package egovframework.com.cmm;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class ExcelView extends AbstractExcelView {

	protected void buildExcelDocument(Map<String, Object> ModelMap, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

		@SuppressWarnings("unchecked") String sheetName = ModelMap.get("sheetName").toString();
		@SuppressWarnings("unchecked") String[] rowTitleList = (String[]) ModelMap.get("rowTitleList");
		@SuppressWarnings("unchecked") ArrayList<String[]> dataList = (ArrayList<String[]>) ModelMap.get("dataList");

		HSSFSheet worksheet = null;
		HSSFRow row = null;

		worksheet = workbook.createSheet(sheetName);

		row = worksheet.createRow(0);

		//해더부분셀에 스타일을 주기위한 인스턴스 생성
		HSSFCellStyle headCellStyle = workbook.createCellStyle();
		headCellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		headCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		headCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		headCellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		headCellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		headCellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		headCellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		HSSFFont font = workbook.createFont();
		font.setBoldweight((short) 700);
		headCellStyle.setFont(font);

		for (int i = 0; i < rowTitleList.length; i++) {
			worksheet.autoSizeColumn((short)i);
			HSSFCell cell = row.createCell(i);
			cell.setCellValue(rowTitleList[i].toString());
			cell.setCellStyle(headCellStyle);
		}

		//얇은 테두리를 위한 스타일 인스턴스 생성
		HSSFCellStyle dataCellStyle = workbook.createCellStyle();
		dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

		for (int i = 1; i < dataList.size() + 1; i++) {
			row = worksheet.createRow(i);
			for (int y = 0; y < dataList.get(i - 1).length; y++) {
				String val = null;
				if (("").equals(dataList.get(i - 1)[y]) || dataList.get(i - 1)[y] == null) {
					val = "";
				} else {
					val = dataList.get(i - 1)[y].toString();
				}
				HSSFCell cell = row.createCell(y);
				cell.setCellValue(val);
				cell.setCellStyle(dataCellStyle);
			}
		}

		try{
			//컬럼 자동 width 맞추기
			for (int i = 0; i < rowTitleList.length; i++) {
				worksheet.autoSizeColumn((short)i);
				worksheet.setColumnWidth(i, (worksheet.getColumnWidth(i)) + 512 );
			}
		}catch (Exception e){
			//e.printStackTrace();
		}

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		sheetName = sheetName + "_" + dateFormat.format(calendar.getTime());
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", FileUtil.getFileName(sheetName, request) + ".xls");
	}
}