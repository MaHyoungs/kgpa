package egovframework.com.cmm;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

public class FileUtil {

	static final char FILE_SEPARATOR = File.separatorChar;

	/**
	 * 폴더 생성
	 * @param path
	 */
	public static void makeBasePath(String path) {
		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();
	}

	/**
	 * 파일 경로 검사
	 * @param value
	 * @return
	 */
	public static String filePathBlackList(String value) {
		String returnValue = value;
		if (returnValue == null || returnValue.trim().equals("")) {
			return "";
		}

		returnValue = returnValue.replaceAll("\\.\\./", "");
		returnValue = returnValue.replaceAll("\\.\\.\\\\", "");

		return returnValue;
	}

	/**
	 * 파일 복사
	 * @param file
	 * @param uploadPath
	 * @param request
	 * @return
	 * @throws IllegalStateException
	 * @throws java.io.IOException
	 */
	public static String[] parseFileInf(MultipartFile file, String uploadPath, HttpServletRequest request) throws IllegalStateException, IOException {
		if(!file.getOriginalFilename().endsWith(".exe") || !file.getOriginalFilename().endsWith(".sh") || !file.getOriginalFilename().endsWith(".jsp") || !file.getOriginalFilename().endsWith(".c") || !file.getOriginalFilename().endsWith(".xml") || !file.getOriginalFilename().endsWith(".js")){
			String[] fileInfo=new String[4];
			String savePath = "";
			String realPath = request.getSession().getServletContext().getRealPath("/");

			realPath = realPath.replace('\\', FILE_SEPARATOR).replace('/', FILE_SEPARATOR);
			realPath = realPath.substring(0, realPath.length()-1);
			uploadPath = uploadPath.replace('\\', FILE_SEPARATOR).replace('/', FILE_SEPARATOR);

			makeBasePath(realPath + uploadPath);
			String ori_name = file.getOriginalFilename();
			String fileExt = ori_name.substring(ori_name.lastIndexOf(".") + 1);
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String save_name = dateFormat.format(calendar.getTime()) + "." + fileExt;
			savePath = uploadPath + save_name;
			file.transferTo(new File(filePathBlackList(realPath+savePath)));
			fileInfo[0] = ori_name;
			fileInfo[1] = save_name;
			fileInfo[2] = new MimetypesFileTypeMap().getContentType(save_name);
			return fileInfo;
		}else{
			return null;
		}
	}

	/**
	 * 브라우저 구분 얻기.
	 *
	 * @param request
	 * @return
	 */
	public static String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if(header.indexOf("Firefox") > -1) {
			return "Firefox";
		}else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		}else if (header.indexOf("Opera") > -1) {
			return "Opera";
		}else if (header.indexOf("MSIE 11") > -1) {
			return "MSIE 11";
		}else{
			return "MSIE";
		}
	}

	/**
	 * Disposition 지정하기.
	 *
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static HttpServletResponse setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);
		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;
		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		}else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			// throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
		return response;
	}

	public static String getFileName(String filename, HttpServletRequest request) throws Exception {
		String browser = getBrowser(request);
		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;
		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		}else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			// throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}
		return dispositionPrefix + encodedFilename;
	}

	/**
	 * 파일 삭제
	 * @param name
	 */
	public static void FileDelete(HttpServletRequest request, String name , String path) {
		String oriFile = request.getSession().getServletContext().getRealPath("/") + path + name;
		File o = new File(oriFile);
		o.delete();
	}

	/**
	 * 파일명 타입 체크
	 * @param filename
	 * @return
	 */
	public static String fileTypeCheck(String filename){
		String return_val = "etc";
		String[] filenames = filename.split("\\."); // 확장자명을 얻기위한 split
		String ext = filenames[filenames.length - 1];
		ext = ext.toLowerCase();
		if(ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("bmp")){
			return_val = "img";
		}else if(ext.contains("xls") || ext.contains("xlsx") || ext.contains("doc") || ext.contains("docx") || ext.contains("hwp") || ext.contains("pptx") || ext.contains("ppt") || ext.contains("pdf")){
			return_val = "doc";
		}
		return return_val;
	}
}
