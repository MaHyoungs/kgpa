package egovframework.com.cmm.mail;

import javax.mail.Session;

public class MailVo {

	private String to_mail;

	private String subject;

	private String content;

	public String getTo_mail() {
		return to_mail;
	}

	public void setTo_mail(String to_mail) {
		this.to_mail = to_mail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
