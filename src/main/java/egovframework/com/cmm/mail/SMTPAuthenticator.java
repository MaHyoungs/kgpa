package egovframework.com.cmm.mail;
import javax.mail.Authenticator;

public class SMTPAuthenticator extends Authenticator {
	private String id;
	private String pw;

	public SMTPAuthenticator(String id, String pw) {
		this.id = id;
		this.pw = pw;
	}

	protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
		return new javax.mail.PasswordAuthentication(id, pw);
	}
}
