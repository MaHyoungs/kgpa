package egovframework.com.cmm.mail;


import egovframework.com.cmm.service.EgovProperties;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.mail.MessagingException;
import javax.mail.Message.RecipientType;

import java.util.Properties;

public class SendMail {


    public static void sendEmail(String from, String to, String subject, String text) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        MimeMessage message = mailSender.createMimeMessage();
        try {
            // Properties 설정
            Properties props = new Properties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.quitwait", "false");
            props.put("mail.debug", "false");                // 디버그 모드 : 개발이 완료되면 false

            mailSender.setJavaMailProperties(props);
            mailSender.setProtocol("smtp");
            mailSender.setHost(EgovProperties.getProperty("Globals.EmailHost"));
            mailSender.setPort(25);
            mailSender.setUsername(EgovProperties.getProperty("Globals.EmailAdress"));
            mailSender.setPassword(EgovProperties.getProperty("Globals.SMTPUserPw"));
            mailSender.setDefaultEncoding("UTF-8");

            message.setSubject(subject);
            message.setText(text, "UTF-8", "html");
            message.setFrom(new InternetAddress(from));
            message.addRecipient(RecipientType.TO, new InternetAddress(to));

            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public static boolean sendEmail(MailVo mailVo) {
        boolean rs = false;
        try {
            // 프로퍼티 값 인스턴스 생성과 기본세션(SMTP 서버 호스트 지정)
            Properties props = new Properties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.host", EgovProperties.getProperty("Globals.EmailHost"));
            props.put("mail.smtp.port", "25");
            props.put("mail.from", EgovProperties.getProperty("Globals.EmailAdress"));
            props.put("mail.smtp.auth", "true");

            SMTPAuthenticator auth = new SMTPAuthenticator(EgovProperties.getProperty("Globals.SMTPUserId"), EgovProperties.getProperty("Globals.SMTPUserPw"));

            //props.put("mail.smtp.socketFactory.port", 465);
            //props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
            //props.put("mail.smtp.socketFactory.fallback", "false");
            //System.out.println(auth.getPasswordAuthentication());

            Session mailSession = Session.getDefaultInstance(props, auth);
            MimeMessage msg = createMessage(mailVo, mailSession);
            Transport.send(msg); // 메일 보내기
            rs = true;
        } catch (MessagingException ex) {
            System.out.println("mail send error : " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("error : " + e.getMessage());
        }
        return rs;
    }


    public static MimeMessage createMessage(MailVo mailVo, Session mailSession) {
        MimeMessage msg = null;
        try {
            msg = new MimeMessage(mailSession);

            msg.setHeader("X-Mailer", "sendMessage");
            // 받는 사람설정
            InternetAddress address = new InternetAddress(mailVo.getTo_mail());
            msg.setRecipient(Message.RecipientType.TO, address);

            // 제목 설정
            msg.setSubject(mailVo.getSubject());

            // 보내는 날짜 설정
            msg.setSentDate(new java.util.Date());
            MimeBodyPart bodypart = new MimeBodyPart();
            bodypart.setContent(mailVo.getContent(), "text/html;charset=utf-8");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodypart);
            msg.setContent(multipart);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }
}
