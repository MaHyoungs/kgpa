package egovframework.com.cmm.service;

import java.io.Serializable;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : FileDetailDownLogVO.java
 * @Description : 파일다운로드로그 정보 처리를 위한 VO 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2014. 12. 13.     서용식
 *
 * @author 프로그램팀 서용식
 * @since 2014. 12. 13
 * @version
 * @see
 *
 */
@SuppressWarnings("serial")
public class FileDetailDownLogVO extends ComDefaultVO implements Serializable {

	/**
	 * 다운로드로그ID
	 */
	public String downloadId = "";
	
    /**
	 * 첨부파일 아이디
	 */
    public String atchFileId = "";
    
    /**
	 * 파일연번
	 */
    public String fileSn = "";
    
    /**
     * 파일 타입(IMG, MOV, DOC)
     */
    public String fileTy = ""; 
    
    /**
	 * 다운로드시점
	 */
    public String userId = "";
    
    /**
	 * 다운로드시점
	 */
    public java.util.Date downloadPnttm;

	public String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public String getFileSn() {
		return fileSn;
	}

	public void setFileSn(String fileSn) {
		this.fileSn = fileSn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public java.util.Date getDownloadPnttm() {
		return downloadPnttm;
	}

	public void setDownloadPnttm(java.util.Date downloadPnttm) {
		this.downloadPnttm = downloadPnttm;
	}

	public String getDownloadId() {
		return downloadId;
	}

	public void setDownloadId(String downloadId) {
		this.downloadId = downloadId;
	}

	public String getFileTy() {
		return fileTy;
	}

	public void setFileTy(String fileTy) {
		this.fileTy = fileTy;
	}
    
}
