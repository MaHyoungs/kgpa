package egovframework.com.cmm.web;

import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileDetailDownLogVO;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.utl.fcc.service.EgovFormBasedFileUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 파일 조회, 삭제, 다운로드 처리를 위한 컨트롤러 클래스
 *
 * @author 공통서비스개발팀 이삼섭
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.3.25  이삼섭          최초 생성
 *
 * </pre>
 * @since 2009.06.01
 */
@Controller
public class EgovFileMngController {

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "egovFileIdGnrService")
	private EgovIdGnrService fileIdgenService;
	
	Logger log = LogManager.getLogger(this.getClass());

	/**
	 * 첨부파일에 대한 목록을 조회한다.
	 *
	 * @param fileVO
	 * @param atchFileId
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfs.do")
	public String selectFileInfs(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");

		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectFileInfs(fileVO);

		model.addAttribute("fileList", result);
		model.addAttribute("updateFlag", "N");
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);

		return "cmm/fms/EgovFileList";
	}

	/**
	 * 자료요청 완료 첨부파일에 대한 목록을 조회한다.
	 *
	 * @param fileVO
	 * @param atchFileId
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectDtaResFileInfs.do")
	public String selectDtaResFileInfs(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");

		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectFileInfs(fileVO);

		model.addAttribute("fileList", result);
		model.addAttribute("updateFlag", "N");
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);

		return "cmm/fms/EgovDtaResFileList";
	}

	/**
	 * 첨부파일 변경을 위한 수정페이지로 이동한다.
	 *
	 * @param fileVO
	 * @param atchFileId
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate.do")
	public String selectFileInfsForUpdate(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		model.addAttribute("updateFlag", "Y");
		return "cmm/fms/EgovFileList";
	}

	/**
	 * 첨부파일 변경을 위한 수정페이지 (게시판 연동 해제)
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate2.do")
	public String selectFileInfsForUpdate2(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		return "cmm/fms/EgovFileList2";
	}

	/**
	 * 지도점검표 파일 업로드 페이지
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate3.do")
	public String selectFileInfsForUpdate3(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		return "cmm/fms/EgovChckList";
	}

	/**
	 * 지도점검 이미지 파일 업로드 페이지
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate4.do")
	public String selectFileInfsForUpdate4(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		return "cmm/fms/EgovChckList2";
	}

	/**
	 * (관리자) 지도점검표 파일 업로드 페이지
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate5.do")
	public String selectFileInfsForUpdate5(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		return "cmm/fms/EgovChckList3";
	}

	/**
	 * (관리자) 지도점검 이미지 파일 업로드 페이지
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileInfsForUpdate6.do")
	public String selectFileInfsForUpdate6(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);
			List<FileVO> result = fileService.selectFileInfs(fileVO);
			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}
		return "cmm/fms/EgovChckList4";
	}

	/**
	 * 첨부파일에 대한 삭제를 처리한다.
	 * @param fileVO
	 * @param returnUrl
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/deleteFileInfs.do")
	public String deleteFileInf(@ModelAttribute("searchVO") FileVO fileVO, @RequestParam("returnUrl") String returnUrl, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		Boolean isAuthenticated = true;//EgovUserDetailsHelper.isAuthenticated(request, response);

		if (isAuthenticated) {
			fileService.deleteFileInf(fileVO);
		}


		if (!EgovStringUtil.isEmpty(returnUrl)) {
			return "redirect:" + returnUrl;
		} else {
			return "redirect:/";
		}

	}

	/**
	 * 첨부파일에 대한 삭제를 처리한다.
	 * @param fileVO
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/deleteFileInfByAjax.do")
	public void deleteFileInfByJson(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		int iCount = fileService.deleteFileInf(fileVO);

		JSONObject jObj = new JSONObject();

		if (iCount != 0) {
			jObj.put("delCount", iCount);
			jObj.put("atchFileId", fileVO.getAtchFileId());
			jObj.put("fileSn", fileVO.getFileSn());
		} else {
			jObj.put("delCount", "0");
		}

		FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(fileVO);
		jObj.put("totalFileMg", totalInfoVO.getTotalFileMg());
		jObj.put("totalFileCount", totalInfoVO.getTotalFileCount());

		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jObj.toString());
		printwriter.flush();
		printwriter.close();

	}

	/**
	 * 이미지 첨부파일에 대한 목록을 조회한다.
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectImageFileInfs.do")
	public String selectImageFileInfs(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap,
			//SessionVO sessionVO,
			ModelMap model) throws Exception {

		String atchFileId = (String) commandMap.get("atchFileId");

		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectImageFileList(fileVO);

		model.addAttribute("fileList", result);

		return "cmm/fms/EgovImgFileList";
	}

	/**
	 * 대용량파일을 Upload 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/uploadStreamLongFiles.do")
	public void uploadStreamLongFiles(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String siteId = (String) commandMap.get("siteId");
		if (EgovStringUtil.isEmpty(siteId)) {
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			siteId = siteVO.getSiteId();
		}

		//String uploadType = (String)commandMap.get("uploadType");
		String pathKey = (String) commandMap.get("pathKey");
		String appendPath = (String) commandMap.get("appendPath");
		String first = (String) commandMap.get("first");
		String last = (String) commandMap.get("last");

		String isEdit = (String) commandMap.get("isEdit");
		String isTempEdit = (String) commandMap.get("isTempEdit");

		boolean firstChunk = Boolean.parseBoolean(first);
		boolean lastChunk = Boolean.parseBoolean(last);
		boolean edit = Boolean.parseBoolean(isEdit);
		boolean tempEdit = Boolean.parseBoolean(isTempEdit);

		String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
		wasReal = wasReal.substring(0, wasReal.length() - 1);

		fileVO.setFileStreCours(wasReal + propertiesService.getString(pathKey) + "/" + siteId + "/" + appendPath);

		FileVO resultVO = fileUtil.uploadStreamLongFiles(request.getInputStream(), fileVO, firstChunk, lastChunk);

		if (lastChunk) {

			FileVO dbVO = fileMngService.insertTempFileInf(resultVO);

			if (edit) {
				if (tempEdit) {
					FileVO delVO = fileMngService.selectTempFileInfByAtchFileIdAndFileSn(resultVO);
					if (delVO != null) {
						fileMngService.deleteTempFileInf(delVO);
					}
				} else {
					fileMngService.deleteFileInf(resultVO);
				}
			}

			JSONObject jObj = new JSONObject();
			jObj.put("SiteId", siteId);
			jObj.put("AppendPath", appendPath);
			jObj.put("AtchFileId", dbVO.getAtchFileId());
			jObj.put("TmprFileId", dbVO.getTmprFileId());
			jObj.put("FileName", dbVO.getOrignlFileNm());
			jObj.put("StreFileNm", dbVO.getStreFileNm() + "." + dbVO.getFileExtsn());
			jObj.put("FileSn", dbVO.getFileSn());
			jObj.put("OriginFileSn", resultVO.getFileSn());
			jObj.put("FileSize", dbVO.getFileMg());
			jObj.put("IsEdit", isEdit);
			jObj.put("IsTempEdit", isTempEdit);

			FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(dbVO);
			jObj.put("TotalFileMg", totalInfoVO.getTotalFileMg());
			jObj.put("TotalFileCount", totalInfoVO.getTotalFileCount());

			response.setContentType("text/javascript; charset=utf-8");
			PrintWriter printwriter = response.getWriter();
			printwriter.println(jObj.toString());
			printwriter.flush();
			printwriter.close();
		}
	}

	/**
	 * 게시판 사용자 Ajax Form을 이용하여 파일을 Upload 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/ajaxBoardUploadFile.do")
	public ModelAndView ajaxUploadStreamLongFiles(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, MultipartHttpServletRequest multiRequest, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");

		String siteId = (String) commandMap.get("siteId");
		if (EgovStringUtil.isEmpty(siteId)) {
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			siteId = siteVO.getSiteId();
		}

		if ("".equals(fileVO.getAtchFileId())) {
			fileVO.setAtchFileId(fileIdgenService.getNextStringId());
		}

		MultipartFile uploadFile = multiRequest.getFile("uploadfile");

		if (!uploadFile.isEmpty()) {

			int currSize = Integer.parseInt(request.getParameter("currSize"));
			int currCount = Integer.parseInt(request.getParameter("currCount"));
			int maxSize = Integer.parseInt(request.getParameter("maxSize"));
			int maxCount = Integer.parseInt(request.getParameter("maxCount"));

			if (maxCount < (currCount + 1)) {
				mav.addObject("rs", "countOver");
				return mav;
			}

			if (maxSize < uploadFile.getSize()) {
				mav.addObject("rs", "overflow");
				return mav;
			} else if (maxSize < uploadFile.getSize() + currSize) {
				mav.addObject("rs", "overflow");
				return mav;
			}

			String pathKey = request.getParameter("pathKey");
			if("".equals(pathKey) || pathKey == null || pathKey == ""){
				pathKey = "Board.fileStorePath";
			}

			String appendPath = (String) commandMap.get("appendPath");
			String first = "True";
			String last = "True";
			String isEdit = "False";
			String isTempEdit = "False";

			boolean firstChunk = Boolean.parseBoolean(first);
			boolean lastChunk = Boolean.parseBoolean(last);
			boolean edit = Boolean.parseBoolean(isEdit);
			boolean tempEdit = Boolean.parseBoolean(isTempEdit);

			fileVO.setFileStreCours(propertiesService.getString(pathKey) + "/" + siteId + "/" + appendPath);
			FileVO resultVO = fileUtil.parseFileInf(request, fileVO, uploadFile);

			String fMg = resultVO.getFileMg();

			if("".equals(fMg)) {
				mav.addObject("rs", "denyExt");
				return mav;
			}

			if (lastChunk) {
				FileVO dbVO = fileMngService.insertTempFileInf(resultVO);

				String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
				wasReal = wasReal.substring(0, wasReal.length() - 1);
				EgovFormBasedFileUtil.renameFile(wasReal + dbVO.fileStreCours, dbVO.getTmprFileId(), dbVO.getStreFileNm());

				if (edit) {
					if (tempEdit) {
						FileVO delVO = fileMngService.selectTempFileInfByAtchFileIdAndFileSn(resultVO);
						if (delVO != null) {
							fileMngService.deleteTempFileInf(delVO);
						}
					} else {
						fileMngService.deleteFileInf(resultVO);
					}
				}

				mav.addObject("fileVo", dbVO);
				FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(dbVO);
				mav.addObject("totalFileMg", totalInfoVO.getTotalFileMg());
				mav.addObject("totalFileCount", totalInfoVO.getTotalFileCount());
			}

		}
		return mav;
	}

	/**
	 * 대용량파일을 삭제 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/deleteStreamLongFileByAjax.do")
	public void deleteStreamLongFileByAjax(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

		FileVO dbVO = fileMngService.deleteTempFileInf(fileVO);

		JSONObject jObj = new JSONObject();

		if (dbVO != null) {
			jObj.put("delCount", "1");
			jObj.put("atchFileId", dbVO.getAtchFileId());
			jObj.put("fileSn", dbVO.getFileSn());

			FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(dbVO);
			jObj.put("totalFileMg", totalInfoVO.getTotalFileMg());
			jObj.put("totalFileCount", totalInfoVO.getTotalFileCount());
		} else {
			jObj.put("delCount", "0");
		}

		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jObj.toString());
		printwriter.flush();
		printwriter.close();
	}

	/**
	 * Ajax 대용량파일을 삭제 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/ajaxBoardFileDelete.do")
	public ModelAndView deleteStreamLongFileByAjax(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
		wasReal = wasReal.substring(0, wasReal.length() - 1);

		fileVO = fileMngService.selectTempFileInf(fileVO);
		if(fileVO != null){
			File tempFile = new File(wasReal + fileVO.getFileStreCours(), fileVO.getStreFileNm());
			if(tempFile.exists()){
				tempFile.delete();
			}
		}

		FileVO dbVO = fileMngService.deleteTempFileInf(fileVO);
		if (dbVO != null) {
			FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(dbVO);
			mav.addObject("totalInfoVO", totalInfoVO);
		}
		return mav;
	}

	@RequestMapping("/cmm/fms/ajaxDeleteFileInfs.do")
	public ModelAndView ajaxDeleteFileInfs(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");

		String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
		wasReal = wasReal.substring(0, wasReal.length() - 1);

		fileVO = fileMngService.selectFileInf(fileVO);
		if(fileVO != null){
			File tempFile = new File(wasReal + fileVO.getFileStreCours(), fileVO.getStreFileNm());
			if(tempFile.exists()){
				tempFile.delete();
			}
		}

		int rs = fileService.deleteFileInf(fileVO);
		if (rs > 0) {
			FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(fileVO);
			mav.addObject("totalInfoVO", totalInfoVO);
		}
		return mav;
	}

	/**
	 * 파일ID를 생성환다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileIdByAjax.do")
	public void selectFileIdByAjax(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(fileIdgenService.getNextStringId());
		printwriter.flush();
		printwriter.close();
	}

	/**
	 * 파일ID Json으로 가져오기
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/fileIdByAjax.do")
	public ModelAndView fileIdByAjax() throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", fileIdgenService.getNextStringId());
		return mav;
	}

	/**
	 * 한반도산림복원자료실 사용자 Ajax Form을 이용하여 파일을 Upload 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/ajaxNkrefoUploadFile.do")
	public ModelAndView ajaxUploadNkrefoStreamLongFiles(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, MultipartHttpServletRequest multiRequest, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");

		//String siteId = (String)commandMap.get("siteId");
		//if(EgovStringUtil.isEmpty(siteId)){
		//	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		//	siteId = siteVO.getSiteId();
		//}

		if ("".equals(fileVO.getAtchFileId())) {
			fileVO.setAtchFileId(fileIdgenService.getNextStringId());
		}

		MultipartFile uploadFile = multiRequest.getFile("uploadfile");

		if (!uploadFile.isEmpty()) {

			int currSize = Integer.parseInt(request.getParameter("currSize"));
			int currCount = Integer.parseInt(request.getParameter("currCount"));
			int maxSize = Integer.parseInt(request.getParameter("maxSize"));
			int maxCount = Integer.parseInt(request.getParameter("maxCount"));

			if (maxCount < (currCount + 1)) {
				mav.addObject("rs", "countOver");
				return mav;
			}

			if (maxSize < uploadFile.getSize()) {
				mav.addObject("rs", "overflow");
				return mav;
			} else if (maxSize < uploadFile.getSize() + currSize) {
				mav.addObject("rs", "overflow");
				return mav;
			}

			String pathKey = "Nkrefo.fileStoreWebPath";
			String appendPath = (String) commandMap.get("appendPath");
			String first = "True";
			String last = "True";
			String isEdit = "False";
			String isTempEdit = "False";

			boolean firstChunk = Boolean.parseBoolean(first);
			boolean lastChunk = Boolean.parseBoolean(last);
			boolean edit = Boolean.parseBoolean(isEdit);
			boolean tempEdit = Boolean.parseBoolean(isTempEdit);

			// 저장경로가 존재하지 않으면 생성처리
			String fileStreCours = propertiesService.getString(pathKey) + "/" + appendPath;

			File scDir = new File(fileStreCours);
			if (scDir.exists() == false) scDir.mkdirs();

			fileVO.setFileStreCours(fileStreCours);
			FileVO resultVO = fileUtil.parseFileInf(request, fileVO, uploadFile);

			if (lastChunk) {
				FileVO dbVO = fileMngService.insertTempFileInf(resultVO);

				String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
				wasReal = wasReal.substring(0, wasReal.length() - 1);
				EgovFormBasedFileUtil.renameFile(wasReal + dbVO.fileStreCours, dbVO.getTmprFileId(), dbVO.getStreFileNm());
				
				if (edit) {
					if (tempEdit) {
						FileVO delVO = fileMngService.selectTempFileInfByAtchFileIdAndFileSn(resultVO);
						if (delVO != null) {
							fileMngService.deleteTempFileInf(delVO);
						}
					} else {
						fileMngService.deleteFileInf(resultVO);
					}
				}

				mav.addObject("fileVo", dbVO);
				FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(dbVO);
				mav.addObject("totalFileMg", totalInfoVO.getTotalFileMg());
				mav.addObject("totalFileCount", totalInfoVO.getTotalFileCount());
			}

		}

		return mav;
	}

	/**
	 * 한반도산림복원자료실 자료의 첨부파일 변경을 위한 수정페이지로 이동한다.
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/nkrefo/selectFileInfsForUpdate.do")
	public String selectNkrefoFileInfsForUpdate(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {

		String atchFileId = (String) commandMap.get("param_atchFileId");

		if (!EgovStringUtil.isEmpty(atchFileId)) {
			fileVO.setAtchFileId(atchFileId);

			List<FileVO> result = fileService.selectFileInfs(fileVO);

			model.addAttribute("fileList", result);
			model.addAttribute("fileListCnt", result.size());
			model.addAttribute("atchFileId", atchFileId);
		}

		model.addAttribute("updateFlag", "Y");

		return "cmm/fms/EgovNkrefoFileList";
	}

	/**
	 * 한반도산림복원자료실 자료의 첨부파일에 대한 목록을 조회한다.
	 * @param fileVO
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/nkrefo/selectFileInfs.do")
	public String selectNkrefoFileInfs(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");
		String userId = (String) commandMap.get("userId");
		String userSe = (String) commandMap.get("userSe");

		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectFileInfs(fileVO);

		// 첨부파일의 확장자 정보로 파일유형 설정
		for (FileVO tmpFile : result) {

			String fileExt = tmpFile.getFileExtsn().toUpperCase(); // 파일확장자는 대문자로 변환 후 비교하기 위함			
			String fileType = "DOC"; // 기본값은 DOC 유형으로 판단

			if ("JPG".equals(fileExt) || "JPEG".equals(fileExt) || "GIF".equals(fileExt) ||
					"BMP".equals(fileExt) || "PNG".equals(fileExt) || "TIF".equals(fileExt) || "TIFF".equals(fileExt)) { // IMG 유형으로 판단
				fileType = "IMG";
			} else if ("AVI".equals(fileExt) || "MPG".equals(fileExt) || "MPEG".equals(fileExt) ||
					"WMV".equals(fileExt) || "ASF".equals(fileExt) || "FLV".equals(fileExt) ||
					"RM".equals(fileExt) || "MOV".equals(fileExt) || "VOB".equals(fileExt) ||
					"M2V".equals(fileExt) || "MKV".equals(fileExt) || "MP4".equals(fileExt)) { // MOV 유형으로 판단
				fileType = "MOV";
			}

			tmpFile.setFileType(fileType);
		}

		model.addAttribute("fileList", result);
		model.addAttribute("updateFlag", "N");
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);
		model.addAttribute("userId", userId);
		model.addAttribute("userSeCode", userSe);

		return "cmm/fms/EgovNkrefoFileList";
	}

	/**
	 * 한반도산림복원자료실 자료의 임시첨부파일에 대한 다운로드 여부를 변경한다.
	 *
	 * @param fileVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/updateTempFileInfDynByAjax.do")
	public void updateTempFileInfByAtchFileIdAndFileSnForDownloadYn(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		int iCount = fileService.updateTempFileInfByAtchFileIdAndFileSnForDownloadYn(fileVO);

		JSONObject jObj = new JSONObject();

		if (iCount != 0) {
			jObj.put("updCount", iCount);
			jObj.put("atchFileId", fileVO.getAtchFileId());
			jObj.put("fileSn", fileVO.getFileSn());
		} else {
			jObj.put("updCount", "0");
		}

		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jObj.toString());
		printwriter.flush();
		printwriter.close();

	}

	/**
	 * 한반도산림복원자료실 자료의 첨부파일에 대한 다운로드 여부를 변경한다.
	 *
	 * @param fileVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/updateFileInfDynByAjax.do")
	public void updateFileInfByAtchFileIdAndFileSnForDownloadYn(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		int iCount = fileService.updateFileInfByAtchFileIdAndFileSnForDownloadYn(fileVO);

		JSONObject jObj = new JSONObject();

		if (iCount != 0) {
			jObj.put("updCount", iCount);
			jObj.put("atchFileId", fileVO.getAtchFileId());
			jObj.put("fileSn", fileVO.getFileSn());
		} else {
			jObj.put("updCount", "0");
		}

		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jObj.toString());
		printwriter.flush();
		printwriter.close();

	}

	/**
	 * 한반도산림복원자료실 자료의 첨부파일 다운로드 이력 조회 화면으로 이동한다.
	 * @param fdlvo
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/nkrefo/EgovNkrefoFiledownlogList.do")
	public String EgovNkrefoFiledownlogList(@ModelAttribute("searchVO") FileDetailDownLogVO fdlvo, Map<String, Object> commandMap, ModelMap model) throws Exception {

		String atchFileId = (String) commandMap.get("param_atchFileId");
		String fileSn = (String) commandMap.get("param_fileSn");

		if (!EgovStringUtil.isEmpty(atchFileId) && !EgovStringUtil.isEmpty(fileSn)) {
			
			fdlvo.setAtchFileId(atchFileId);
			fdlvo.setFileSn(fileSn);
			
			PaginationInfo paginationInfo = new PaginationInfo();
			// 페이징 정보 설정
			fdlvo.setPageUnit(propertiesService.getInt("pageUnit"));
			fdlvo.setPageSize(propertiesService.getInt("pageSize"));
			
			paginationInfo.setCurrentPageNo(fdlvo.getPageIndex());
			paginationInfo.setRecordCountPerPage(fdlvo.getPageUnit());
			paginationInfo.setPageSize(fdlvo.getPageSize());
			
			fdlvo.setFirstIndex(paginationInfo.getFirstRecordIndex());
			fdlvo.setLastIndex(paginationInfo.getLastRecordIndex());
			fdlvo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

			List<FileDetailDownLogVO> resultList = fileService.selectFiledetailDownlogList(fdlvo);
			int totCnt = fileService.selectFiledetailDownlogListTotCnt(fdlvo);
			
			paginationInfo.setTotalRecordCount(totCnt);

			model.addAttribute("fileDownlogList", resultList);
			model.addAttribute("fileDownlogListCnt", totCnt);
			model.addAttribute("paginationInfo", paginationInfo);
			
			model.addAttribute("param_atchFileId", atchFileId);
			model.addAttribute("param_fileSn", fileSn);
			
		}

		return "cmm/fms/EgovNkrefoFiledownlogList";
	}

	/**
	 * 지도점검표 파일을 Upload 처리한다.
	 *
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/ajaxchckUploadFile.do")
	public ModelAndView ajaxchckUploadFile(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, MultipartHttpServletRequest multiRequest, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");

		String siteId = (String) commandMap.get("siteId");
		if (EgovStringUtil.isEmpty(siteId)) {
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			siteId = siteVO.getSiteId();
		}
		
		String atchFileId2 = request.getParameter("ctt_atch_file_id");
		if(EgovStringUtil.isEmpty(atchFileId2)){ // 첨부파일ID가 없으면 신규 발급
			fileVO.setAtchFileId(fileIdgenService.getNextStringId());
		}else{
			fileVO.setAtchFileId(atchFileId2); // 첨부파일ID가 있으면 기존ID 재사용
		}
		
		MultipartFile uploadFile = multiRequest.getFile("uploadfile2");

		if (!uploadFile.isEmpty()) {

			int currSize = Integer.parseInt(request.getParameter("currSize2"));
			int currCount = Integer.parseInt(request.getParameter("currCount2"));
			int maxSize = Integer.parseInt(request.getParameter("maxSize2"));
			int maxCount = Integer.parseInt(request.getParameter("maxCount2"));

			if (maxCount < (currCount + 1)) {
				mav.addObject("rs", "countOver");
				return mav;
			}

			if (maxSize < uploadFile.getSize()) {
				mav.addObject("rs", "overflow");
				return mav;
			} else if (maxSize < uploadFile.getSize() + currSize) {
				mav.addObject("rs", "overflow");
				return mav;
			}

			String pathKey = request.getParameter("pathKey2");
			if("".equals(pathKey) || pathKey == null){
				pathKey = "Board.fileStorePath";
			}

			String appendPath = (String) commandMap.get("appendPath2");
			String first = "True";
			String last = "True";
			String isEdit = "False";
			String isTempEdit = "False";

			boolean firstChunk = Boolean.parseBoolean(first);
			boolean lastChunk = Boolean.parseBoolean(last);
			boolean edit = Boolean.parseBoolean(isEdit);
			boolean tempEdit = Boolean.parseBoolean(isTempEdit);

			fileVO.setFileStreCours(propertiesService.getString(pathKey) + "/" + siteId + "/" + appendPath);
			FileVO resultVO = fileUtil.parseFileInf(request, fileVO, uploadFile);

			if (lastChunk) {
				FileVO dbVO = fileMngService.insertTempFileInf(resultVO);


				String wasReal = request.getSession().getServletContext().getRealPath("/").replace("\\", "/");
				wasReal = wasReal.substring(0, wasReal.length() - 1);
				EgovFormBasedFileUtil.renameFile(wasReal + dbVO.fileStreCours, dbVO.getTmprFileId(), dbVO.getStreFileNm());
				
				if (edit) {
					if (tempEdit) {
						FileVO delVO = fileMngService.selectTempFileInfByAtchFileIdAndFileSn(resultVO);
						if (delVO != null) {
							fileMngService.deleteTempFileInf(delVO);
						}
					} else {
						fileMngService.deleteFileInf(resultVO);
					}
				}

				mav.addObject("fileVo", dbVO);
				FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(dbVO);
				mav.addObject("totalFileMg", totalInfoVO.getTotalFileMg());
				mav.addObject("totalFileCount", totalInfoVO.getTotalFileCount());
			}

		}
		return mav;
	}

	/**
	 * 첨부파일에 대한 확장자를 조회한다.
	 *
	 * @param fileVO
	 * @param atchFileId
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/selectFileExtsn.do")
	public String selectFileExtsn(@ModelAttribute("searchVO") FileVO fileVO, Map<String, Object> commandMap, ModelMap model) throws Exception {
		String atchFileId = (String) commandMap.get("param_atchFileId");

		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectFileInfs(fileVO);

		model.addAttribute("fileList", result);
		model.addAttribute("updateFlag", "N");
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);

		return "cmm/fms/EgovFileList";
	}

	/**
	 * dext5 파일 업로더
	 * @param fileVO
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmm/fms/ajaxInsertFileInfo.do")
	public ModelAndView ajaxInsertFileInfo(HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");

		List<FileVO> fileInfo = new ArrayList<FileVO>();
		List<FileVO> fileDelfo = new ArrayList<FileVO>();
		String atchUseYn = request.getParameter("atchUseYn");
		String atchFileId = request.getParameter("atchFileId");
		int fileSn = Integer.parseInt(request.getParameter("fileSn").toString());
		String[] arr3 = request.getParameter("fileStreCours").split("\\$");
		String[] arr4 = request.getParameter("streFileNm").split("\\$");
		String[] arr5 = request.getParameter("orignlFileNm").split("\\$");
		String[] arrDel = request.getParameter("delSn").split("\\$");
		String[] arr6 = new String[arr3.length];

		if(arrDel[0] != "") {
			for(int j=0; j<arrDel.length; j++) {
				FileVO delvo = new FileVO();
				delvo.setAtchFileId(atchFileId);
				delvo.setFileSn(arrDel[j]);
				fileDelfo.add(j, delvo);
			}
			fileMngService.delFilesInfo(fileDelfo);
		}

		if(arr5[0] != "") {
			for(int j=0; j<arr5.length; j++) {
				arr3[j] = arr3[j].substring(0, arr3[j].lastIndexOf("/")) + "/";
				arr4[j] = arr4[j].substring(0, arr4[j].lastIndexOf("."));
				arr6[j] = arr5[j].substring(arr5[j].lastIndexOf(".") + 1);
			}
			String[] arr7 = request.getParameter("fileMg").split(",");

			for (int i = 0; i < arr4.length; i++) {
				FileVO vo = new FileVO();
				vo.setAtchFileId(atchFileId);
				vo.setFileSn(String.valueOf(fileSn));
				vo.setFileStreCours(arr3[i]);
				vo.setStreFileNm(arr4[i]);
				vo.setOrignlFileNm(arr5[i]);
				vo.setFileExtsn(arr6[i]);
				vo.setFileMg(arr7[i]);
				vo.setDownloadYn("Y");
				vo.setFileCn(null);
				fileSn++;
				fileInfo.add(i, vo);
			}
			if(atchUseYn.equals("Y")) {
				fileMngService.updateFileInfs(fileInfo);
			} else {
				fileMngService.insertFileInfs(fileInfo);
			}
		}
		mav.addObject("result", "success");

		return mav;
	}
}
