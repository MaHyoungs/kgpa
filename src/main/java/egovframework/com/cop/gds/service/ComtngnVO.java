/**
 * 개요
 * - 2006 ~ 2009년 삼림자료에 대한 ServiceImpl 클래스를 정의한다.
 * - 2006 ~ 2009년 삼림자료에 대한 model 클래스를 정의한다.
 * 
 * 상세내용
 * - 2006 ~ 2009년 삼림자료의 일련번호, 카테고리명, 타이틀, 이미지 등 항목을 관리한다.
 * @author 마형민
 * @version 1.0
 * @created 21-11-2014 오후 2:32:10
 */

package egovframework.com.cop.gds.service;

import egovframework.com.cmm.service.FileVO;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ComtngnVO extends CmmnDetailCode implements Serializable {

	/** 녹색자료ID **/
	private String gn_id;
	/** 시스템구분(공통코드) **/
	private String sys_ty_code;
	/** 사이트ID **/
	private String site_id;
	/** 기관분류(공통코드) **/
	private String org_code;
	/** 게시글제목 **/
	private String title;
	/** 기관분류별게시글순번 **/
	private String title_sn;
	/** 이미지저장명 **/
	private String img_stre_snm;
	/** 이미지원본명 **/
	private String img_stre_rnm;
	/** 이미지확장자 **/
	private String img_extsn;
	/** 이미지크기 **/
	private String img_mg;
	/** 파일저장경로 **/
	private String file_stre_cours;
	/** 파일저장명 **/
	private String file_stre_snm;
	/** 파일원본명 **/
	private String file_stre_rnm;
	/** 파일확장자 **/
	private String file_extsn;
	/** 파일크기 **/
	private String file_mg;
	/** 최초등록시점 **/
	private String register_date;
	/** 최초등록자ID **/
	private String register_id;
	/** 최초등록시점 **/
	private String last_updusr_pnttm;
	/** 최초등록자ID **/
	private String last_updusr_id;

	public String getGn_id(){
		return gn_id;
	}

	public void setGn_id(String gn_id){
		this.gn_id = gn_id;
	}

	public String getSys_ty_code(){
		return sys_ty_code;
	}

	public void setSys_ty_code(String sys_ty_code){
		this.sys_ty_code = sys_ty_code;
	}

	public String getSite_id(){
		return site_id;
	}

	public void setSite_id(String site_id){
		this.site_id = site_id;
	}

	public String getOrg_code(){
		return org_code;
	}

	public void setOrg_code(String org_code){
		this.org_code = org_code;
	}

	public String getTitle(){
		return title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle_sn(){
		return title_sn;
	}

	public void setTitle_sn(String title_sn){
		this.title_sn = title_sn;
	}

	public String getImg_stre_snm(){
		return img_stre_snm;
	}

	public void setImg_stre_snm(String img_stre_snm){
		this.img_stre_snm = img_stre_snm;
	}

	public String getImg_stre_rnm(){
		return img_stre_rnm;
	}

	public void setImg_stre_rnm(String img_stre_rnm){
		this.img_stre_rnm = img_stre_rnm;
	}

	public String getImg_extsn(){
		return img_extsn;
	}

	public void setImg_extsn(String img_extsn){
		this.img_extsn = img_extsn;
	}

	public String getImg_mg(){
		return img_mg;
	}

	public void setImg_mg(String img_mg){
		this.img_mg = img_mg;
	}

	public String getFile_stre_cours(){
		return file_stre_cours;
	}

	public void setFile_stre_cours(String file_stre_cours){
		this.file_stre_cours = file_stre_cours;
	}

	public String getFile_stre_snm(){
		return file_stre_snm;
	}

	public void setFile_stre_snm(String file_stre_snm){
		this.file_stre_snm = file_stre_snm;
	}

	public String getFile_stre_rnm(){
		return file_stre_rnm;
	}

	public void setFile_stre_rnm(String file_stre_rnm){
		this.file_stre_rnm = file_stre_rnm;
	}

	public String getFile_extsn(){
		return file_extsn;
	}

	public void setFile_extsn(String file_extsn){
		this.file_extsn = file_extsn;
	}

	public String getFile_mg(){
		return file_mg;
	}

	public void setFile_mg(String file_mg){
		this.file_mg = file_mg;
	}

	public String getRegister_date(){
		return register_date;
	}

	public void setRegister_date(String register_date){
		this.register_date = register_date;
	}

	public String getRegister_id(){
		return register_id;
	}

	public void setRegister_id(String register_id){
		this.register_id = register_id;
	}

	public String getLast_updusr_pnttm(){
		return last_updusr_pnttm;
	}

	public void setLast_updusr_pnttm(String last_updusr_pnttm){
		this.last_updusr_pnttm = last_updusr_pnttm;
	}

	public String getLast_updusr_id(){
		return last_updusr_id;
	}

	public void setLast_updusr_id(String last_updusr_id){
		this.last_updusr_id = last_updusr_id;
	}

	public void setFileValue(List<FileVO> files) {
		if(files != null) {
			for(int index=0; index < files.size(); index++) {
				FileVO file = files.get(index);
				if(file.getFormNm().startsWith("img")) {
					this.img_stre_rnm = file.getOrignlFileNm();
					this.img_stre_snm = file.getStreFileNm();
					this.img_extsn = file.getFileExtsn();
					this.img_mg = file.getFileMg();
				} else if(file.getFormNm().startsWith("cont")) {
					this.file_stre_rnm = file.getOrignlFileNm();
					this.file_stre_snm = file.getStreFileNm();
					this.file_stre_cours = file.getFileStreCours();
					this.file_extsn = file.getFileExtsn();
					this.file_mg = file.getFileMg();
				}
			}
		}
	}
}
