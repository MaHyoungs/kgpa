/**
 * 개요
 * - 2006 ~ 2009년 삼림자료에 대한 Service Interface를 정의한다.
 *
 * 상세내용
 * - 2006 ~ 2009년 삼림자료에 대한 등록, 수정, 삭제, 조회 기능을 제공한다.
 * - 2006 ~ 2009년 삼림자료의 조회기능은 목록조회, 상세조회로 구분된다.
 * @author 마형민
 * @version 1.0
 * @created 21-11-2014 오후 2:45:12
 */

package egovframework.com.cop.gds.service;

import java.util.List;

public interface EgovComtngnService {

	/**
	 * 2006~2009년 산림 자료를 등록 한다.
	 *
	 * @param comtngnVO
	 * @throws Exception
	 */
	public void insertComtngn(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 산림 자료 수를 카운트 한다.
	 *
	 * @param comtngnVO
	 * @return
	 * @throws Exception
	 */
	public int selectComtngnListCnt(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 산림 자료를 조회 한다.
	 *
	 * @param comtngnVO
	 * @return
	 * @throws Exception
	 */
	public List<ComtngnVO> selectComtngnList(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 하나의 산림 자료에 대하여 상세 내용을 조회 한다.
	 *
	 * @param comtngnVO
	 * @return
	 * @throws Exception
	 */
	public ComtngnVO selectComtngnInfo(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 하나의 산림 자료에 대하여 수정 한다.
	 *
	 * @param comtngnVO
	 * @throws Exception
	 */
	public void updateComtngn(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 하나의 산림 자료에 대하여 삭제 한다.
	 *
	 * @param comtngnVO
	 * @throws Exception
	 */
	public void deleteComtngn(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 공통코드 목록
	 *
	 * @param comtngnVO
	 * @return
	 * @throws Exception
	 */
	public List<ComtngnVO> selectCode(ComtngnVO comtngnVO) throws Exception;

	/**
	 * 2006~2009년 마지막 제목번호 값
	 *
	 * @param comtngnVO
	 * @return
	 * @throws Exception
	 */
	public int selectMaxTitle(ComtngnVO comtngnVO) throws Exception;
}
