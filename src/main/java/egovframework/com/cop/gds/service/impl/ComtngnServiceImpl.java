/**
 * 개요
 * - 2006~2009년 산림 자료에 대한 ServiceImpl 클래스를 정의한다.
 *
 * 상세내용
 * - 2006~2009년 산림 자료에 대한 등록, 수정, 삭제, 조회 기능을 제공한다.
 * - 2006~2009년 산림 자료는 목록조회, 상세조회로 구분된다.
 * @author 마형민
 * @version 1.0
 * @created 21-11-2014 오후 3:03:12
 */

package egovframework.com.cop.gds.service.impl;

import egovframework.com.cop.gds.service.ComtngnVO;
import egovframework.com.cop.gds.service.EgovComtngnService;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("EgovComtngnService")
public class ComtngnServiceImpl extends AbstractServiceImpl implements EgovComtngnService{

	@Resource(name="comtngnDAO")
    private ComtngnDAO comtngnDAO;

	public void insertComtngn(ComtngnVO comtngnVO) throws Exception{
		comtngnDAO.insertComtngn(comtngnVO);
	}

	public int selectComtngnListCnt(ComtngnVO comtngnVO) throws Exception {
		return comtngnDAO.selectComtngnListCnt(comtngnVO);
	}

	public List<ComtngnVO> selectComtngnList(ComtngnVO comtngnVO) throws Exception {
		return comtngnDAO.selectComtngnList(comtngnVO);
	}

	public ComtngnVO selectComtngnInfo(ComtngnVO comtngnVO) throws Exception {
		return comtngnDAO.selectComtngnInfo(comtngnVO);
	}

	public void updateComtngn(ComtngnVO comtngnVO) throws Exception {
		comtngnDAO.updateComtngn(comtngnVO);
	}

	public void deleteComtngn(ComtngnVO comtngnVO) throws Exception {
		comtngnDAO.deleteComtngn(comtngnVO);
	}

	public List<ComtngnVO> selectCode(ComtngnVO comtngnVO) throws Exception {
		return comtngnDAO.selectCode(comtngnVO);
	}

	public int selectMaxTitle(ComtngnVO comtngnVO) throws Exception {
		return comtngnDAO.selectMaxTitle(comtngnVO);
	}

}