package egovframework.com.cop.gds.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.gds.service.ComtngnVO;
import egovframework.com.cop.gds.service.EgovComtngnService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;


/**
 * 2006 ~ 2009년 산림자료를 위한 컨트롤러 클래스
 *
 * @author 프로그램개발팀 마형민
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------       --------    ---------------------------
 *   2014.11.21  마형민          최초 생성
 *
 * </pre>
 * @since 2014.11.21
 */

@Controller
public class EgovComtngnController{

	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "EgovComtngnService")
	private EgovComtngnService egovComtngnService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	@Resource(name = "EgovComtngnGnIdService")
	private EgovIdGnrService egovIdGnrService2;

	@Resource(name = "EgovComtngnFileIdService")
	private EgovIdGnrService egovIdGnrService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	Logger log = LogManager.getLogger(this.getClass());

	/**
	 * 2006 ~ 2009년 산림자료에 대한 목록을 조회한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/gds/selectComtngnList.do")
	public String selectComtngnList(ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		//LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);


		// 공통코드 리스트
		model.addAttribute("commonCode", egovComtngnService.selectCode(comtngnVO));

		// 자료 리스트
		model.addAttribute("comtngnList", egovComtngnService.selectComtngnList(comtngnVO));

		return "cop/gds/comtngnList.tiles";
	}

	/**
	 * 2006 ~ 2009년 산림자료에 대한 상세 조회한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/gds/selectComtngnInfo.do")
	public String selectComtngnInfo(ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// 상세 목록 리스트
		List comtngnInfo = egovComtngnService.selectComtngnList(comtngnVO);

		model.addAttribute("comtngnInfo", comtngnInfo);

		return "cop/gds/comtngnList";
	}

	/**
	 * ******  관리자영역  ******
	 * 2006 ~ 2009년 산림자료에 대한 목록을 조회한다.
	 *
	 * @param comtngnVO,commonCodeVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/selectMngComtngnList.do")
	public String selectMngComtngnList(ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{


		// 공통코드 리스트
		model.addAttribute("commonCode", egovComtngnService.selectCode(comtngnVO));

		// 자료 리스트
		model.addAttribute("comtngnList", egovComtngnService.selectComtngnList(comtngnVO));


		return "mng/cop/gds/MngcomtngnList";
	}

	/**
	 * 2006 ~ 2009년 산림자료에 대한 상세 조회한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/selectComtngnInfo.do")
	public String selectMngComtngnInfo(@ModelAttribute("comtngnVO") ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		model.addAttribute("comtngnInfo", egovComtngnService.selectComtngnInfo(comtngnVO));

		return "mng/cop/gds/MngcomtngnRegt";
	}

	/**
	 * 2006 ~ 2009년 산림자료 등록페이지로 이동한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/addComtngn.do")
	public String addComtngn(ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		comtngnVO.setFrstRegisterId(user.getId());

		return "mng/cop/gds/MngcomtngnAdd";
	}


	/**
	 * 2006 ~ 2009년 산림자료 등록한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/insertComtngn.do")
	public String insertComtngn(@ModelAttribute("comtngnVO") ComtngnVO comtngnVO, MultipartHttpServletRequest multiRequest, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		String forwardUrl = "forward:/mng/gds/selectMngComtngnList.do";

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		comtngnVO.setFrstRegisterId(user.getId());

		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		comtngnVO.setSite_id(siteVO.getSiteId());
		comtngnVO.setSys_ty_code("EFC");

		// 제목번호 마지막 값
/*		int maxtTitle = egovComtngnService.selectMaxTitle(comtngnVO);
		model.addAttribute("maxtTitle", maxtTitle);*/

		List<FileVO> result = null;

		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		if(!files.isEmpty()){

			result = fileUtil.directParseFileInf(request, files, "GNFILE_", 0, "Comtngn.fileStoreWebPath", "");
			comtngnVO.setFileValue(result);
		}
/*		MultipartFile imgfile = multiRequest.getFile("imgfile");
		MultipartFile contfile = multiRequest.getFile("contfile");

		if(!imgfile.isEmpty()){
			FileVO imgResult = fileUtil.directParseFileInf(request, imgfile, "GNFILE_", 0, "Comtngn.imgStoreWebPath", "");
			comtngnVO = setImgValue(imgResult, comtngnVO);
		}

		if(!contfile.isEmpty()){
			FileVO fileResult = fileUtil.directParseFileInf(request, contfile, "GNFILE_", 0, "Comtngn.fileStoreWebPath", "");
			comtngnVO = setImgValue(fileResult, comtngnVO);
		}*/

		comtngnVO.setGn_id(egovIdGnrService2.getNextStringId());

		egovComtngnService.insertComtngn(comtngnVO);

		return forwardUrl;
	}

	/**
	 * 2006 ~ 2009년 산림자료 수정
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/updateComtngn.do")
	public String updateComtngn(@ModelAttribute("comtngnVO") ComtngnVO comtngnVO, MultipartHttpServletRequest multiRequest, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		String forwardUrl = "forward:/mng/gds/selectMngComtngnList.do";

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		comtngnVO.setLast_updusr_id(user.getId());

		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		comtngnVO.setSite_id(siteVO.getSiteId());
		comtngnVO.setSys_ty_code("EFC");

		List<FileVO> result = null;

		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		if(!files.isEmpty()){

			result = fileUtil.directParseFileInf(request, files, "GNFILE_", 0, "Comtngn.fileStoreWebPath", "");
			comtngnVO.setFileValue(result);
		}

		egovComtngnService.updateComtngn(comtngnVO);

		return forwardUrl;
	}

	/**
	 * 2006 ~ 2009년 산림자료를 삭제한다.
	 *
	 * @param comtngnVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gds/deleteComtngn.do")
	public String deleteComtngn(@ModelAttribute("comtngnVO") ComtngnVO comtngnVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		String forwardUrl = "forward:/mng/gds/selectMngComtngnList.do";

		egovComtngnService.deleteComtngn(comtngnVO);

		return forwardUrl;
	}
}