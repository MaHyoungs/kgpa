package egovframework.com.diet.service;

import java.io.Serializable;

/**
 * 휴일 모델 클래스
 * @author 공통서비스 개발팀 이중호
 * @since 2009.04.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.04.01  이중호          최초 생성
 *
 * </pre>
 */
public class Diet implements Serializable {

	private String dietId;
	private String rice;
	private String stew;
	private String side1;
	private String side2;
	private String side3;
	private String side4;
	private String side5;
	private String side6;
	private String dietDate;
    private java.sql.Date frstRegisterPnttm;
    private java.lang.String frstRegisterId;
    private java.sql.Date lastUpdusrPnttm;
    private java.lang.String lastUpdusrId;
    private String year           = "";
    private String month          = "";
    private String day            = "";
    private String restdeAt       = "";
	private int    cellNum        = 0;
    private int    weeks          = 0;
    private int maxWeeks = 0;
    private int    week           = 0;
    private int    startWeekMonth = 0;
    private int    lastDayMonth   = 0;
    private String writerNm;
    private String division;
    private String searchDay;
    
	public String getSearchDay() {
		return searchDay;
	}

	public void setSearchDay(String searchDay) {
		this.searchDay = searchDay;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWriterNm() {
		return writerNm;
	}

	public void setWriterNm(String writerNm) {
		this.writerNm = writerNm;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getRestdeAt() {
		return restdeAt;
	}

	public void setRestdeAt(String restdeAt) {
		this.restdeAt = restdeAt;
	}

	public int getCellNum() {
		return cellNum;
	}

	public void setCellNum(int cellNum) {
		this.cellNum = cellNum;
	}

	public int getWeeks() {
		return weeks;
	}

	public void setWeeks(int weeks) {
		this.weeks = weeks;
	}

	public int getMaxWeeks() {
		return maxWeeks;
	}

	public void setMaxWeeks(int maxWeeks) {
		this.maxWeeks = maxWeeks;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public int getStartWeekMonth() {
		return startWeekMonth;
	}

	public void setStartWeekMonth(int startWeekMonth) {
		this.startWeekMonth = startWeekMonth;
	}

	public int getLastDayMonth() {
		return lastDayMonth;
	}

	public void setLastDayMonth(int lastDayMonth) {
		this.lastDayMonth = lastDayMonth;
	}

	public String getDietId() {
		return dietId;
	}

	public void setDietId(String dietId) {
		this.dietId = dietId;
	}

	public String getRice() {
		return rice;
	}

	public void setRice(String rice) {
		this.rice = rice;
	}

	public String getStew() {
		return stew;
	}

	public void setStew(String stew) {
		this.stew = stew;
	}

	public String getSide1() {
		return side1;
	}

	public void setSide1(String side1) {
		this.side1 = side1;
	}

	public String getSide2() {
		return side2;
	}

	public void setSide2(String side2) {
		this.side2 = side2;
	}

	public String getSide3() {
		return side3;
	}

	public void setSide3(String side3) {
		this.side3 = side3;
	}

	public String getSide4() {
		return side4;
	}

	public void setSide4(String side4) {
		this.side4 = side4;
	}

	public String getSide5() {
		return side5;
	}

	public void setSide5(String side5) {
		this.side5 = side5;
	}

	public String getSide6() {
		return side6;
	}

	public void setSide6(String side6) {
		this.side6 = side6;
	}

	public String getDietDate() {
		return dietDate;
	}

	public void setDietDate(String dietDate) {
		this.dietDate = dietDate;
	}

	public java.sql.Date getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(java.sql.Date frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public java.lang.String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(java.lang.String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.sql.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.sql.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public java.lang.String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(java.lang.String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}
    
    
    
}
