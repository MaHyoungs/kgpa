package egovframework.com.gfund.biz.ancmt.service;

import java.sql.SQLException;
import java.util.List;

public interface AnnouncementService {

	/**
	 * 사업공고 등록
	 *
	 * @param announcementVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertAnnouncement(AnnouncementVo announcementVo) throws SQLException;

	/**
	 * 사업공고 수정
	 *
	 * @param announcementVo
	 * @return
	 * @throws SQLException
	 */
	public int updateAnnouncement(AnnouncementVo announcementVo) throws SQLException;

	/**
	 * 사업공고 조회수 증가
	 *
	 * @param announcementVo
	 * @return
	 * @throws SQLException
	 */
	public int updateAnnouncementReadCountIncrease(AnnouncementVo announcementVo) throws SQLException;

	/**
	 * 사업공고 삭제
	 *
	 * @param announcementVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteAnnouncement(AnnouncementVo announcementVo) throws SQLException;

	/**
	 * 사업공고 조회
	 *
	 * @param announcementVo
	 * @return
	 * @throws SQLException
	 */
	public AnnouncementVo selectAnnouncement(AnnouncementVo announcementVo) throws SQLException;

	/**
	 * 사업공고 목록 조회
	 *
	 * @param announcementVo
	 * @return
	 * @throws SQLException
	 */
	public List<AnnouncementVo> selectAnnouncements(AnnouncementVo announcementVo) throws SQLException;
}
