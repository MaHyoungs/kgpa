package egovframework.com.gfund.biz.ancmt.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * 사업공고
 */
public class AnnouncementVo extends ComDefaultVO {

	//년도
	private String year;

	//사업분류(공통코드)
	private String biz_ty_code;

	//사업분류명(공통코드)
	private String biz_ty_code_nm;

	//제안서 접수 시작일시
	private String propse_papers_rcept_bgnde;

	//제안서 접수 종료일시
	private String propse_papers_rcept_endde;

	//제안서 접수 종료일시
	private String last_biz_plan_regist_bgnde;

	//최종 사업 계획서 등록 종료일시
	private String last_biz_plan_regist_endde;

	//사업내용
	private String biz_cn;

	//제안서접수여부
	private String propse_rcept_at;

	//사용여부
	private String use_at;

	//조회수
	private String rdcnt;

	//최초등록시점
	private String frst_regist_pnttm;

	//최초등록자ID
	private String frst_register_id;

	//최종수정시점
	private String last_updusr_pnttm;

	//최종수정자ID
	private String last_updusr_id;

	//첨부파일ID
	private String atch_file_id;

	//사업공고 사용중인 갯수
	private String use_cnt;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getBiz_ty_code() {
		return biz_ty_code;
	}

	public void setBiz_ty_code(String biz_ty_code) {
		this.biz_ty_code = biz_ty_code;
	}

	public String getBiz_ty_code_nm() {
		return biz_ty_code_nm;
	}

	public void setBiz_ty_code_nm(String biz_ty_code_nm) {
		this.biz_ty_code_nm = biz_ty_code_nm;
	}

	public String getPropse_papers_rcept_bgnde() {
		return propse_papers_rcept_bgnde;
	}

	public void setPropse_papers_rcept_bgnde(String propse_papers_rcept_bgnde) {
		this.propse_papers_rcept_bgnde = propse_papers_rcept_bgnde;
	}

	public String getPropse_papers_rcept_endde() {
		return propse_papers_rcept_endde;
	}

	public void setPropse_papers_rcept_endde(String propse_papers_rcept_endde) {
		this.propse_papers_rcept_endde = propse_papers_rcept_endde;
	}

	public String getLast_biz_plan_regist_bgnde() {
		return last_biz_plan_regist_bgnde;
	}

	public void setLast_biz_plan_regist_bgnde(String last_biz_plan_regist_bgnde) {
		this.last_biz_plan_regist_bgnde = last_biz_plan_regist_bgnde;
	}

	public String getLast_biz_plan_regist_endde() {
		return last_biz_plan_regist_endde;
	}

	public void setLast_biz_plan_regist_endde(String last_biz_plan_regist_endde) {
		this.last_biz_plan_regist_endde = last_biz_plan_regist_endde;
	}

	public String getBiz_cn() {
		return biz_cn;
	}

	public void setBiz_cn(String biz_cn) {
		this.biz_cn = biz_cn;
	}

	public String getPropse_rcept_at() {
		return propse_rcept_at;
	}

	public void setPropse_rcept_at(String propse_rcept_at) {
		this.propse_rcept_at = propse_rcept_at;
	}

	public String getUse_at() {
		return use_at;
	}

	public void setUse_at(String use_at) {
		this.use_at = use_at;
	}

	public String getRdcnt() {
		return rdcnt;
	}

	public void setRdcnt(String rdcnt) {
		this.rdcnt = rdcnt;
	}

	public String getFrst_regist_pnttm() {
		return frst_regist_pnttm;
	}

	public void setFrst_regist_pnttm(String frst_regist_pnttm) {
		this.frst_regist_pnttm = frst_regist_pnttm;
	}

	public String getFrst_register_id() {
		return frst_register_id;
	}

	public void setFrst_register_id(String frst_register_id) {
		this.frst_register_id = frst_register_id;
	}

	public String getLast_updusr_pnttm() {
		return last_updusr_pnttm;
	}

	public void setLast_updusr_pnttm(String last_updusr_pnttm) {
		this.last_updusr_pnttm = last_updusr_pnttm;
	}

	public String getLast_updusr_id() {
		return last_updusr_id;
	}

	public void setLast_updusr_id(String last_updusr_id) {
		this.last_updusr_id = last_updusr_id;
	}

	public String getAtch_file_id() {
		return atch_file_id;
	}

	public void setAtch_file_id(String atch_file_id) {
		this.atch_file_id = atch_file_id;
	}

	public String getUse_cnt() {
		return use_cnt;
	}

	public void setUse_cnt(String use_cnt) {
		this.use_cnt = use_cnt;
	}
}
