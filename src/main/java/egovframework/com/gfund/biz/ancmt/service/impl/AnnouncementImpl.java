package egovframework.com.gfund.biz.ancmt.service.impl;

import egovframework.com.gfund.biz.ancmt.service.AnnouncementService;
import egovframework.com.gfund.biz.ancmt.service.AnnouncementVo;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

@Service("announcementService")
public class AnnouncementImpl extends AbstractServiceImpl implements AnnouncementService {

	@Resource(name = "announcementDao")
	private AnnouncementDao announcementDao;
	
	public int insertAnnouncement(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.insertAnnouncement(announcementVo);
	}

	public int updateAnnouncement(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.updateAnnouncement(announcementVo);
	}

	public int updateAnnouncementReadCountIncrease(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.updateAnnouncementReadCountIncrease(announcementVo);
	}

	public int deleteAnnouncement(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.deleteAnnouncement(announcementVo);
	}

	public AnnouncementVo selectAnnouncement(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.selectAnnouncement(announcementVo);
	}

	public List<AnnouncementVo> selectAnnouncements(AnnouncementVo announcementVo) throws SQLException {
		return announcementDao.selectAnnouncements(announcementVo);
	}
}
