package egovframework.com.gfund.biz.ancmt.web;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.ancmt.service.AnnouncementService;
import egovframework.com.gfund.biz.ancmt.service.AnnouncementVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@Controller
public class AnnouncementController {

	@Resource(name = "announcementService")
	private AnnouncementService announcementService;

	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	/**
	 * 사업공고관리 목록 조회
	 * @param announcementVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/announcementList.do")
	public String announcementList(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws SQLException {
		//년도 파라미터 없을시, 기준녀도 조회하여 대입
		if("".equals(announcementVo.getYear()) || announcementVo.getYear() == null){
			GreenFundYearVo defaultYear = greenFundYearService.selectGreenFundYearDefault();
			if(defaultYear != null){
				announcementVo.setYear(defaultYear.getYear());
			}
		}
		model.addAttribute("announcementVo", announcementVo);
		model.addAttribute("yearList", greenFundYearService.selectGreenFundYears());
		model.addAttribute("resultList", announcementService.selectAnnouncements(announcementVo));
		return "mng/gfund/biz/ancmt/announcementList";
	}

	/**
	 * 사업공고관리 기준년도 목록 조회
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/msi/announcementService.do")
	public String announcementService(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws SQLException {
		announcementVo.setBiz_ty_code("");
		model.addAttribute("resultList", announcementService.selectAnnouncements(announcementVo));
		return "msi/svc/announcementService";
	}

	/**
	 * 사업공고관리 조회
	 * @param announcementVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping({"/mng/gfund/biz/ancmt/announcementView.do", "/gfund/biz/ancmt/announcementView.do"})
	public String announcementView(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws SQLException {
		announcementVo = announcementService.selectAnnouncement(announcementVo);
		model.addAttribute("announcementVo", announcementVo);

		//관리자 조회
		if(request.getRequestURI().contains("mng")){
			return "mng/gfund/biz/ancmt/announcementView";
		}
		//사용자 조회
		else{
			//조횟수 업데이트
			announcementService.updateAnnouncementReadCountIncrease(announcementVo);
			LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
			if(loginVO != null){
				BasicInformationVo basicInformationVo=new BasicInformationVo();
				basicInformationVo.setYear(announcementVo.getYear());
				basicInformationVo.setBiz_ty_code(announcementVo.getBiz_ty_code());
				basicInformationVo.setUser_id(loginVO.getId());
				basicInformationVo.setIsNotPaging("true");
				model.addAttribute("bsifList", basicInformationService.selectBusinessBasicInformations(basicInformationVo));
			}
			return "gfund/biz/ancmt/announcementView.tiles";
		}
	}

	/**
	 * 사업공고관리 Ajax 중복체크
	 * @param announcementVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/ajaxAnnouncementOverlapCheck.do")
	public ModelAndView ajaxAnnouncementOverlapCheck(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		announcementVo = announcementService.selectAnnouncement(announcementVo);
		if(announcementVo != null){
			mav.addObject("rs", "false");
		}else{
			mav.addObject("rs", "true");
		}
		return mav;
	}

	/**
	 * 사업공고관리 등록 페이지 조회
	 * @param announcementVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/announcementForm.do")
	public String announcementForm(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws SQLException {
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		model.addAttribute("announcementVo", announcementService.selectAnnouncement(announcementVo));
		model.addAttribute("yearList", greenFundYearService.selectGreenFundYears());
		return "mng/gfund/biz/ancmt/announcementForm";
	}

	/**
	 * 사업공고관리 등록
	 * @param announcementVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/announcementInsert.do")
	public String announcementInsert(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws Exception {
		int insertRs = announcementService.insertAnnouncement(announcementVo);
		if(announcementVo.getAtch_file_id() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(announcementVo.getAtch_file_id());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		model.addAttribute("year", announcementVo.getYear());
		model.addAttribute("biz_ty_code", announcementVo.getBiz_ty_code());
		model.addAttribute("insertRs", insertRs);
		return "redirect:/mng/gfund/biz/ancmt/announcementView.do";
	}

	/**
	 * 사업공고관리 수정
	 * @param announcementVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/announcementUpdate.do")
	public String announcementUpdate(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws Exception {
		int updateRs = announcementService.updateAnnouncement(announcementVo);
		if(announcementVo.getAtch_file_id() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(announcementVo.getAtch_file_id());
			/*fvo.setAtchFileId(announcementVo.getAtch_file_id());*/
			fileMngService.insertFileInfsByTemp(fvo);
		}
		model.addAttribute("year", announcementVo.getYear());
		model.addAttribute("biz_ty_code", announcementVo.getBiz_ty_code());
		model.addAttribute("updateRs", updateRs);
		return "redirect:/mng/gfund/biz/ancmt/announcementView.do";
	}

	/**
	 * 사업공고관리 삭제
	 * @param announcementVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/ancmt/announcementDelete.do")
	public String announcementDelete(@ModelAttribute("announcementVo") AnnouncementVo announcementVo, ModelMap model) throws SQLException {
		int deleteRs = announcementService.deleteAnnouncement(announcementVo);
		model.addAttribute("deleteRs", deleteRs);
		return "redirect:/mng/gfund/biz/ancmt/announcementList.do";
	}

}