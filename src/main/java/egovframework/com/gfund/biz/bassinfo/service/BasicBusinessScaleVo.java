package egovframework.com.gfund.biz.bassinfo.service;

public class BasicBusinessScaleVo {
	private String biz_scale_id;
	private String biz_id;
	private String rcvfvr_trget;
	private String rcvfvr_nmpr_co;
	private String detail_event_scale;
	private String co;
	private String rslt_cn_mnfct_co;
	private String rslt_cn_wdtb_co;
	private String etc;

	public String getBiz_scale_id() {
		return biz_scale_id;
	}

	public void setBiz_scale_id(String biz_scale_id) {
		this.biz_scale_id = biz_scale_id;
	}

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public String getRcvfvr_trget() {
		return rcvfvr_trget;
	}

	public void setRcvfvr_trget(String rcvfvr_trget) {
		this.rcvfvr_trget = rcvfvr_trget;
	}

	public String getRcvfvr_nmpr_co() {
		return rcvfvr_nmpr_co;
	}

	public void setRcvfvr_nmpr_co(String rcvfvr_nmpr_co) {
		this.rcvfvr_nmpr_co = rcvfvr_nmpr_co;
	}

	public String getDetail_event_scale() {
		return detail_event_scale;
	}

	public void setDetail_event_scale(String detail_event_scale) {
		this.detail_event_scale = detail_event_scale;
	}

	public String getCo() {
		return co;
	}

	public void setCo(String co) {
		this.co = co;
	}

	public String getRslt_cn_mnfct_co() {
		return rslt_cn_mnfct_co;
	}

	public void setRslt_cn_mnfct_co(String rslt_cn_mnfct_co) {
		this.rslt_cn_mnfct_co = rslt_cn_mnfct_co;
	}

	public String getRslt_cn_wdtb_co() {
		return rslt_cn_wdtb_co;
	}

	public void setRslt_cn_wdtb_co(String rslt_cn_wdtb_co) {
		this.rslt_cn_wdtb_co = rslt_cn_wdtb_co;
	}

	public String getEtc() {
		return etc;
	}

	public void setEtc(String etc) {
		this.etc = etc;
	}
}
