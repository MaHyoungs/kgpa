package egovframework.com.gfund.biz.bassinfo.service;

public class BasicContentsSequenceVo {

	private String biz_ctnu_id;
	private String biz_id;
	private String year;
	private String biz_nm;
	private String green_fund;
	private String rcvfvr_year_nmpr;
	private String rcvfvr_year_nmpr_goal;
	private String rcvfvr_trget;
	private String rcvfvr_scale;
	private String evl_result;

	public String getBiz_ctnu_id() {
		return biz_ctnu_id;
	}

	public void setBiz_ctnu_id(String biz_ctnu_id) {
		this.biz_ctnu_id = biz_ctnu_id;
	}

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getBiz_nm() {
		return biz_nm;
	}

	public void setBiz_nm(String biz_nm) {
		this.biz_nm = biz_nm;
	}

	public String getGreen_fund() {
		return green_fund;
	}

	public void setGreen_fund(String green_fund) {
		this.green_fund = green_fund;
	}

	public String getRcvfvr_year_nmpr() {
		return rcvfvr_year_nmpr;
	}

	public void setRcvfvr_year_nmpr(String rcvfvr_year_nmpr) {
		this.rcvfvr_year_nmpr = rcvfvr_year_nmpr;
	}

	public String getRcvfvr_year_nmpr_goal() {
		return rcvfvr_year_nmpr_goal;
	}

	public void setRcvfvr_year_nmpr_goal(String rcvfvr_year_nmpr_goal) {
		this.rcvfvr_year_nmpr_goal = rcvfvr_year_nmpr_goal;
	}

	public String getRcvfvr_trget() {
		return rcvfvr_trget;
	}

	public void setRcvfvr_trget(String rcvfvr_trget) {
		this.rcvfvr_trget = rcvfvr_trget;
	}

	public String getRcvfvr_scale() {
		return rcvfvr_scale;
	}

	public void setRcvfvr_scale(String rcvfvr_scale) {
		this.rcvfvr_scale = rcvfvr_scale;
	}

	public String getEvl_result() {
		return evl_result;
	}

	public void setEvl_result(String evl_result) {
		this.evl_result = evl_result;
	}
}
