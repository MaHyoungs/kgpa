package egovframework.com.gfund.biz.bassinfo.service;

@SuppressWarnings("serial")
public class BasicInformationAddInfoVo extends BasicInformationVo{
	
	/* 지도점검체크리스트(com_tn_gf_cch_chck_list) */
	private String comp_at;
	private String chck_frst_regist_pnttm;
	private String chck_frst_register_id;
	private String chck_last_updusr_pnttm;
	private String chck_last_updusr_id;
    
    /* 정상(com_tn_gf_excclc) */
    private String decsn_amount;
    private String excut_amount;
    private String rturn_resn;
    private String dpst_intr;
    private String dpst_intr_resn;
    private String trmnat_intr;
    private String trmnat_intr_resn;
    private String excclc_at;
    
    /* 공통상세코드(comtccmmndetailcode) */
    private String codeId;
    private String codeNm;
    private String codeDc;
    
	public String getComp_at() {
		return comp_at;
	}
	public void setComp_at(String comp_at) {
		this.comp_at = comp_at;
	}
	public String getDecsn_amount() {
		return decsn_amount;
	}
	public void setDecsn_amount(String decsn_amount) {
		this.decsn_amount = decsn_amount;
	}
	public String getExcut_amount() {
		return excut_amount;
	}
	public void setExcut_amount(String excut_amount) {
		this.excut_amount = excut_amount;
	}
	public String getRturn_resn() {
		return rturn_resn;
	}
	public void setRturn_resn(String rturn_resn) {
		this.rturn_resn = rturn_resn;
	}
	public String getDpst_intr() {
		return dpst_intr;
	}
	public void setDpst_intr(String dpst_intr) {
		this.dpst_intr = dpst_intr;
	}
	public String getDpst_intr_resn() {
		return dpst_intr_resn;
	}
	public void setDpst_intr_resn(String dpst_intr_resn) {
		this.dpst_intr_resn = dpst_intr_resn;
	}
	public String getTrmnat_intr() {
		return trmnat_intr;
	}
	public void setTrmnat_intr(String trmnat_intr) {
		this.trmnat_intr = trmnat_intr;
	}
	public String getTrmnat_intr_resn() {
		return trmnat_intr_resn;
	}
	public void setTrmnat_intr_resn(String trmnat_intr_resn) {
		this.trmnat_intr_resn = trmnat_intr_resn;
	}
	public String getExcclc_at() {
		return excclc_at;
	}
	public void setExcclc_at(String excclc_at) {
		this.excclc_at = excclc_at;
	}
	public String getChck_frst_regist_pnttm() {
		return chck_frst_regist_pnttm;
	}
	public void setChck_frst_regist_pnttm(String chck_frst_regist_pnttm) {
		this.chck_frst_regist_pnttm = chck_frst_regist_pnttm;
	}
	public String getChck_frst_register_id() {
		return chck_frst_register_id;
	}
	public void setChck_frst_register_id(String chck_frst_register_id) {
		this.chck_frst_register_id = chck_frst_register_id;
	}
	public String getCodeId() {
		return codeId;
	}
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}
	public String getCodeNm() {
		return codeNm;
	}
	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}
	public String getCodeDc() {
		return codeDc;
	}
	public void setCodeDc(String codeDc) {
		this.codeDc = codeDc;
	}
	public String getChck_last_updusr_pnttm() {
		return chck_last_updusr_pnttm;
	}
	public void setChck_last_updusr_pnttm(String chck_last_updusr_pnttm) {
		this.chck_last_updusr_pnttm = chck_last_updusr_pnttm;
	}
	public String getChck_last_updusr_id() {
		return chck_last_updusr_id;
	}
	public void setChck_last_updusr_id(String chck_last_updusr_id) {
		this.chck_last_updusr_id = chck_last_updusr_id;
	}
}
