package egovframework.com.gfund.biz.bassinfo.service;

import java.sql.SQLException;
import java.util.List;

public interface BasicInformationService {
	/**
	 * 기본정보 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBasicInformation(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformation(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 진행단계(공통코드) 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationStep(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 최종평가결과 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationForEvlRst(BasicInformationVo basicInformationVo) throws SQLException ;

	/**
	 * 기본정보 > 발표자료 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updatePresentationsFile(BasicInformationVo basicInformationVo) throws SQLException ;

	/**
	 * 기본정보 > 최종사업계획서제출여부 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateFinalBusinessPlan(BasicInformationVo basicInformationVo) throws SQLException ;

	/**
	 * 기본정보 > 제출구분 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationSubmitPresence(BasicInformationVo basicInformationVo) throws SQLException ;

	/**
	 * 기본정보 > 지도점검대상여부 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationCchchckTargetAt(BasicInformationVo basicInformationVo) throws SQLException ;
	
	/**
	 * 기본정보 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBasicInformation(BasicInformationVo basicInformationVo) throws SQLException;










	/**
	 * 기본정보 > 기관정보 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertOrganization(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 기관정보 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateOrganization(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 기관정보 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteOrganization(BasicInformationVo basicInformationVo) throws SQLException;












	/**
	 * 기본정보 > 사업대상 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업대상 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업대상 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException;











	/**
	 * 기본정보 > 사업비내역 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업비내역 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업비내역 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException;










	/**
	 * 기본정보 > 사업내용 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessContents(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업내용 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessContents(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 사업내용 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContents(BasicInformationVo basicInformationVo) throws SQLException;











	/**
	 * 기본정보 > 사업연속성 등록
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;

	/**
	 * 기본정보 > 사업연속성 수정
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;

	/**
	 * 기본정보 > 사업연속성 삭제
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;

	/**
	 * 기본정보 > 사업연속성 전체삭제
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;

	/**
	 * 기본정보 > 사업연속성 조회
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public BasicContentsSequenceVo selectBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;

	/**
	 * 기본정보 > 사업연속성 목록 조회
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicContentsSequenceVo> selectBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException;











	/**
	 * 기본정보 > 운영프로그램 등록
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;

	/**
	 * 기본정보 > 운영프로그램 수정
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;

	/**
	 * 기본정보 > 운영프로그램 삭제
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;

	/**
	 * 기본정보 > 운영프로그램 전체삭제
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;

	/**
	 * 기본정보 > 운영프로그램 조회
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public BasicOperationalProgrammeVo selectBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;

	/**
	 * 기본정보 > 운영프로그램 목록 조회
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicOperationalProgrammeVo> selectBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException;











	/**
	 * 기본정보 > 사업규모 등록
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;

	/**
	 * 기본정보 > 사업규모 수정
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;

	/**
	 * 기본정보 > 사업규모 삭제
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;

	/**
	 * 기본정보 > 사업규모 전체삭제
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;

	/**
	 * 기본정보 > 사업규모 조회
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public BasicBusinessScaleVo selectBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;

	/**
	 * 기본정보 > 사업규모 목록 조회
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicBusinessScaleVo> selectBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException;











	/**
	 * 기본정보 > 통합 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public BasicInformationVo selectBusinessBasicInformation(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 통합 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 통합 목록 조회 (모바일)
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectmBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 통합 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectReviewBasicInformations(BasicInformationVo basicInformationVo) throws SQLException;

	/**
	 * 기본정보 > 통합 목록 카운트 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int selectBusinessBasicInformationsCount(BasicInformationVo basicInformationVo) throws SQLException;
	
	/**
	 * 기본+부가정보 > 통합 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationAddInfoVo> selectBusinessBasicInformationAddInfos(BasicInformationVo basicInformationVo) throws SQLException ;
}
