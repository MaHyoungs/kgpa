package egovframework.com.gfund.biz.bassinfo.service;

import egovframework.com.cmm.ComDefaultVO;

import java.util.List;

public class BasicInformationVo extends ComDefaultVO {

	//녹색자금사업 기본정보(com_tn_gf_biz_bass_info)
	private String biz_id;
	private String propse_biz_id;
	private String user_id;
	private String year;
	private String biz_ty_code;
	private String biz_ty_code_nm;
	private String prst_ty_cc;
	private String prst_ty_cc_nm;
	private String cnsrtm_at;
	private String cnsrtm_entrps_nm;
	private String biz_nm;
	private String aceptnc_psncpa;
	private String aceptnc_nownmpr;
	private String user_qy;
	private String mngr_qy;
	private String ground_trplant_ar;
	private String rf_trplant_ar;
	private String tot_trplant_ar;
	private String wct_trplant_div;
	private String wct_make_div;
	private String trplant_ar_type;
	private String frt_ty_spcl;
	private String frt_ty_spcl_nm;
	private String frt_ty_area;
	private String frt_ty_area_nm;
	private String make_ar;
	private String fclty_all_ar;
	private String fclty_instrctr_sttus;
	private String make_ty;
	private String make_ty_nm;
	private String biz_zip;
	private String biz_adres;
	private String biz_adres_detail;
	private String biz_adres2;
	private String biz_adres_detail2;
	private String main_wdpt_01;
	private String main_wdpt_02;
	private String main_wdpt_03;
	private String main_wdpt_04;
	private String tp_drng_at;
	private String tp_remvl_at;
	private String tp_splemnt_at;
	private String tp_use_sttus;
	private String tp_now_sttus;
	private String biz_purps;
	private String ud_bf_dstnct_method;
	private String expc_effect;
	private String adhrnc_rcrit_manage;
	private String biz_bgnde;
	private String biz_endde;
	private String tot_wct;
	private String edc_place;
	private String prtnfx_dc_dt;
	private String prtnfx_dsgn_bgnde;
	private String prtnfx_dsgn_endde;
	private String prtnfx_cntrwk_bgnde;
	private String prtnfx_cntrwk_endde;
	private String prtnfx_sc_de;
	private String prtnfx_bizprpare_bgnde;
	private String prtnfx_bizprpare_endde;
	private String prtnfx_rcritpblanc_bgnde;
	private String prtnfx_rcritpblanc_endde;
	private String prtnfx_bizprogrs_bgnde;
	private String prtnfx_bizprogrs_endde;
	private String prtnfx_evlreport_bgnde;
	private String prtnfx_evlreport_endde;
	private String aftfat_manage;
	private String biz_se;
	private String edc_stle;
	private String edc_pd;

	private String propse_flag;
	private String propse_flag_nm;

	private String last_biz_plan_presentn_at;
	private String cch_chck_trget_at;
	private String last_evl_result;
	private String last_evl_result_nm;
	private String atch_file_id;
	private String gatch_file_id;
	private String presnatn_atch_file_id;
	private String frst_regist_pnttm;
	private String frst_register_id;
	private String last_updusr_pnttm;
	private String last_updusr_id;

	private String build_st_cc;
	private String build_st_cc_nm;
	private String build_st_dt;
	private String build_lc_cc;
	private String build_lc_cc_nm;
	private String build_lc_dt;
	private String build_fond_year;
	private String build_fond_month;
	private String own_ar1;
	private String own_ar2;
	private String resi_ty_cc;
	private String resi_ty_cc_nm;
	private String resi_ty_dt;
	private String make_place_num;
	private String typ_ch_cc;
	private String typ_ch_cc_nm;
	private String fclty_user_qy;
	private String make_user_qy;
	private String biz_contents;
	private String fclty_pudn_bgnde;
	private String fclty_pudn_endde;
	private String construction_bgnde;
	private String construction_endde;
	private String etc_purps_contents;
	private String alt_space_plan;

	//기관정보(com_tn_gf_biz_instt_info)
	private String area;
	private String instt_nm;
	private String fond_ty_cc;
	private String fond_ty_cc_nm;
	private String fclty_ty_cc;
	private String fclty_ty_cc_nm;
	private String fclty_dv_cc;
	private String fclty_dv_cc_nm;
	private String trobl_ty_cc;
	private String trobl_ty_cc_nm;
	private String rprsntv;
	private String fond_year;
	private String fte_nmpr;
	private String fnanc_idpdc;
	private String fnanc_idpdc_rank;
	private String cpr_prmisn_miryfc;
	private String user_nm;
	private String email_adres;
	private String tlphon_no;
	private String moblphon_no;
	private String faxphon_no;
	private String zip;
	private String adres;
	private String adres_detail;
	private String r_adres;
	private String r_adres_detail;
	private String department;
	private String position;
	private String fond_ty_gi;

	//프로그램참가대상(com_tn_gf_biz_trget)
	private String pj_01;
	private String pj_02;
	private String pj_03;
	private String pj_04;
	private String pj_05;
	private String pj_06;
	private String pj_07;

	//대상특성(com_tn_gf_biz_trget)
	private String oc_01;
	private String oc_02;
	private String oc_03;
	private String oc_04;
	private String oc_05;
	private String oc_06;
	private String oc_07;
	private String oc_08;

	//사업대상(com_tn_gf_biz_trget)
	private String ud_01_co;
	private String ud_02_co;
	private String ud_03_co;
	private String ud_04_co;
	private String ut_01_co;
	private String ut_02_co;
	private String ut_03_co;
	private String ut_04_co;
	private String at_01_co;
	private String at_02_co;
	private String at_03_co;
	private String cs_01_co;
	private String cs_02_co;
	private String cs_03_co;
	private String cs_04_co;
	private String cs_05_ct;
	private String cs_06_ct;
	private String ef_01_co;
	private String ef_02_co;
	private String ef_03_co;
	private String ef_04_co;
	private String ef_05_ct;
	private String ef_06_ct;
	private String fl_01_co;
	private String fl_02_co;
	private String fl_03_co;
	private String fl_04_co;
	private String fl_05_ct;
	private String fl_06_ct;
	private String etc_01;
	private String etc_02_co;
	private String etc_03_co;
	private String etc_04_ct;
	private String etc_05_ct;
	private String year_co;
	private String cs_type;
	private String ud_co_avg;
	private String one_edu_mny;
	private String ud_tot_co;
	private String ut_tot_co;
	private String at_tot_co;


	//사업비내역(com_tn_gf_biz_ct_dtls)
	private String gf_01_ct;
	private String gf_02_ct;
	private String gf_03_ct;
	private String gf_04_ct;
	private String gf_05_ct;
	private String gf_06_ct;
	private String gf_07_ct;
	private String gf_08_ct;
	private String gf_09_ct;
	private String gf_10_ct;
	private String gf_11_ct;
	private String gf_12_ct;
	private String gf_13_ct;
	private String gf_14_ct;
	private String gf_15_ct;
	private String gf_16_ct;
	private String gf_17_ct;
	private String gf_18_ct;
	private String gf_19_ct;
	private String gf_20_ct;
	private String gf_21_ct;
	private String gf_22_ct;
	private String gf_23_ct;
	private String sp_01_ct;
	private String sp_02_ct;
	private String sp_03_ct;
	private String sp_04_ct;
	private String sp_05_ct;
	private String sp_06_ct;
	private String sp_07_ct;
	private String sp_08_ct;
	private String sp_09_ct;


	//사업내용(com_tn_gf_biz_cn)
	private String plt_ct;
	private String fnd_ct;
	private String fct_ct;
	private String etc_ct;
	private String biz_ct_tot;
	private String biz_ct_tot2;
	private String pcp_qy;
	private String psf_qy;
	private String isw_cn;
	private String fcw_cn;
	private String plt_ct_avg;
	private String fnd_ct_avg;
	private String fct_ct_avg;
	private String etc_ct_avg;
	private String etc_ct_avg2;
	private String biz_ct_avg;
	private String biz_ct_avg2;

	//20150707 추가됨
	private String cc_ct;
	private String rsc_ct;
	private String tprc_ct;
	private String trc_ct;
	private String fclts_ct;
	private String cc_ct_avg;
	private String rsc_ct_avg;
	private String tprc_ct_avg;
	private String trc_ct_avg;
	private String fclts_ct_avg;


	private String gf_7_9_ct_tot;
	private String gf_10_20_ct_tot;
	private String gf_7_20_ct_tot;
	private String gf_7_21_ct_tot;
	private String gf_7_9_ct_avg;
	private String gf_21_ct_avg;
	private String gf_10_20_ct_avg;
	private String gf_7_21_ct_avg;
	private String green_tot;
	private String jabudam_tot;
	private String green_avg;
	private String jabudam_avg;
	private String oper_progrm_cnt;
	private String gf_09_ct_tot;

	private String propse_papers_rcept_bgnde;
	private String propse_papers_rcept_endde;

	private List<BasicContentsSequenceVo> bcslist;

	private List<BasicOperationalProgrammeVo> boplist;

	private List<BasicBusinessScaleVo> bbslist;

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public String getPropse_biz_id() {
		return propse_biz_id;
	}

	public void setPropse_biz_id(String propse_biz_id) {
		this.propse_biz_id = propse_biz_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getBiz_ty_code() {
		return biz_ty_code;
	}

	public void setBiz_ty_code(String biz_ty_code) {
		this.biz_ty_code = biz_ty_code;
	}

	public String getBiz_ty_code_nm() {
		return biz_ty_code_nm;
	}

	public void setBiz_ty_code_nm(String biz_ty_code_nm) {
		this.biz_ty_code_nm = biz_ty_code_nm;
	}

	public String getPrst_ty_cc() {
		return prst_ty_cc;
	}

	public void setPrst_ty_cc(String prst_ty_cc) {
		this.prst_ty_cc = prst_ty_cc;
	}

	public String getPrst_ty_cc_nm() {
		return prst_ty_cc_nm;
	}

	public void setPrst_ty_cc_nm(String prst_ty_cc_nm) {
		this.prst_ty_cc_nm = prst_ty_cc_nm;
	}

	public String getCnsrtm_at() {
		return cnsrtm_at;
	}

	public void setCnsrtm_at(String cnsrtm_at) {
		this.cnsrtm_at = cnsrtm_at;
	}

	public String getCnsrtm_entrps_nm() {
		return cnsrtm_entrps_nm;
	}

	public void setCnsrtm_entrps_nm(String cnsrtm_entrps_nm) {
		this.cnsrtm_entrps_nm = cnsrtm_entrps_nm;
	}

	public String getBiz_nm() {
		return biz_nm;
	}

	public void setBiz_nm(String biz_nm) {
		this.biz_nm = biz_nm;
	}

	public String getAceptnc_psncpa() {
		return aceptnc_psncpa;
	}

	public void setAceptnc_psncpa(String aceptnc_psncpa) {
		this.aceptnc_psncpa = aceptnc_psncpa;
	}

	public String getAceptnc_nownmpr() {
		return aceptnc_nownmpr;
	}

	public void setAceptnc_nownmpr(String aceptnc_nownmpr) {
		this.aceptnc_nownmpr = aceptnc_nownmpr;
	}

	public String getUser_qy() {
		return user_qy;
	}

	public void setUser_qy(String user_qy) {
		this.user_qy = user_qy;
	}

	public String getMngr_qy() {
		return mngr_qy;
	}

	public void setMngr_qy(String mngr_qy) {
		this.mngr_qy = mngr_qy;
	}

	public String getGround_trplant_ar() {
		return ground_trplant_ar;
	}

	public void setGround_trplant_ar(String ground_trplant_ar) {
		this.ground_trplant_ar = ground_trplant_ar;
	}

	public String getRf_trplant_ar() {
		return rf_trplant_ar;
	}

	public void setRf_trplant_ar(String rf_trplant_ar) {
		this.rf_trplant_ar = rf_trplant_ar;
	}

	public String getTot_trplant_ar(){
		return tot_trplant_ar;
	}

	public void setTot_trplant_ar(String tot_trplant_ar){
		this.tot_trplant_ar = tot_trplant_ar;
	}

	public String getWct_trplant_div(){
		return wct_trplant_div;
	}

	public void setWct_trplant_div(String wct_trplant_div){
		this.wct_trplant_div = wct_trplant_div;
	}

	public String getWct_make_div(){
		return wct_make_div;
	}

	public void setWct_make_div(String wct_make_div){
		this.wct_make_div = wct_make_div;
	}

	public String getTrplant_ar_type() {
		return trplant_ar_type;
	}

	public void setTrplant_ar_type(String trplant_ar_type) {
		this.trplant_ar_type = trplant_ar_type;
	}

	public String getFrt_ty_spcl() {
		return frt_ty_spcl;
	}

	public void setFrt_ty_spcl(String frt_ty_spcl) {
		this.frt_ty_spcl = frt_ty_spcl;
	}

	public String getFrt_ty_spcl_nm() {
		return frt_ty_spcl_nm;
	}

	public void setFrt_ty_spcl_nm(String frt_ty_spcl_nm) {
		this.frt_ty_spcl_nm = frt_ty_spcl_nm;
	}

	public String getFrt_ty_area() {
		return frt_ty_area;
	}

	public void setFrt_ty_area(String frt_ty_area) {
		this.frt_ty_area = frt_ty_area;
	}

	public String getFrt_ty_area_nm() {
		return frt_ty_area_nm;
	}

	public void setFrt_ty_area_nm(String frt_ty_area_nm) {
		this.frt_ty_area_nm = frt_ty_area_nm;
	}

	public String getMake_ar() {
		return make_ar;
	}

	public void setMake_ar(String make_ar) {
		this.make_ar = make_ar;
	}

	public String getFclty_all_ar() {
		return fclty_all_ar;
	}

	public void setFclty_all_ar(String fclty_all_ar) {
		this.fclty_all_ar = fclty_all_ar;
	}

	public String getFclty_instrctr_sttus() {
		return fclty_instrctr_sttus;
	}

	public void setFclty_instrctr_sttus(String fclty_instrctr_sttus) {
		this.fclty_instrctr_sttus = fclty_instrctr_sttus;
	}

	public String getMake_ty() {
		return make_ty;
	}

	public void setMake_ty(String make_ty) {
		this.make_ty = make_ty;
	}

	public String getMake_ty_nm() {
		return make_ty_nm;
	}

	public void setMake_ty_nm(String make_ty_nm) {
		this.make_ty_nm = make_ty_nm;
	}

	public String getBiz_zip() {
		return biz_zip;
	}

	public void setBiz_zip(String biz_zip) {
		this.biz_zip = biz_zip;
	}

	public String getBiz_adres() {
		return biz_adres;
	}

	public void setBiz_adres(String biz_adres) {
		this.biz_adres = biz_adres;
	}

	public String getBiz_adres_detail() {
		return biz_adres_detail;
	}

	public void setBiz_adres_detail(String biz_adres_detail) {
		this.biz_adres_detail = biz_adres_detail;
	}

	public String getBiz_adres2() {
		return biz_adres2;
	}

	public void setBiz_adres2(String biz_adres2) {
		this.biz_adres2 = biz_adres2;
	}

	public String getBiz_adres_detail2() {
		return biz_adres_detail2;
	}

	public void setBiz_adres_detail2(String biz_adres_detail2) {
		this.biz_adres_detail2 = biz_adres_detail2;
	}

	public String getMain_wdpt_01() {
		return main_wdpt_01;
	}

	public void setMain_wdpt_01(String main_wdpt_01) {
		this.main_wdpt_01 = main_wdpt_01;
	}

	public String getMain_wdpt_02() {
		return main_wdpt_02;
	}

	public void setMain_wdpt_02(String main_wdpt_02) {
		this.main_wdpt_02 = main_wdpt_02;
	}

	public String getMain_wdpt_03() {
		return main_wdpt_03;
	}

	public void setMain_wdpt_03(String main_wdpt_03) {
		this.main_wdpt_03 = main_wdpt_03;
	}

	public String getMain_wdpt_04() {
		return main_wdpt_04;
	}

	public void setMain_wdpt_04(String main_wdpt_04) {
		this.main_wdpt_04 = main_wdpt_04;
	}

	public String getTp_drng_at() {
		return tp_drng_at;
	}

	public void setTp_drng_at(String tp_drng_at) {
		this.tp_drng_at = tp_drng_at;
	}

	public String getTp_remvl_at() {
		return tp_remvl_at;
	}

	public void setTp_remvl_at(String tp_remvl_at) {
		this.tp_remvl_at = tp_remvl_at;
	}

	public String getTp_splemnt_at() {
		return tp_splemnt_at;
	}

	public void setTp_splemnt_at(String tp_splemnt_at) {
		this.tp_splemnt_at = tp_splemnt_at;
	}

	public String getTp_use_sttus() {
		return tp_use_sttus;
	}

	public void setTp_use_sttus(String tp_use_sttus) {
		this.tp_use_sttus = tp_use_sttus;
	}

	public String getBiz_purps() {
		return biz_purps;
	}

	public void setBiz_purps(String biz_purps) {
		this.biz_purps = biz_purps;
	}

	public String getUd_bf_dstnct_method() {
		return ud_bf_dstnct_method;
	}

	public void setUd_bf_dstnct_method(String ud_bf_dstnct_method) {
		this.ud_bf_dstnct_method = ud_bf_dstnct_method;
	}

	public String getExpc_effect() {
		return expc_effect;
	}

	public void setExpc_effect(String expc_effect) {
		this.expc_effect = expc_effect;
	}

	public String getAdhrnc_rcrit_manage() {
		return adhrnc_rcrit_manage;
	}

	public void setAdhrnc_rcrit_manage(String adhrnc_rcrit_manage) {
		this.adhrnc_rcrit_manage = adhrnc_rcrit_manage;
	}

	public String getBiz_bgnde() {
		return biz_bgnde;
	}

	public void setBiz_bgnde(String biz_bgnde) {
		this.biz_bgnde = biz_bgnde;
	}

	public String getBiz_endde() {
		return biz_endde;
	}

	public void setBiz_endde(String biz_endde) {
		this.biz_endde = biz_endde;
	}

	public String getTot_wct() {
		return tot_wct;
	}

	public void setTot_wct(String tot_wct) {
		this.tot_wct = tot_wct;
	}

	public String getEdc_place() {
		return edc_place;
	}

	public void setEdc_place(String edc_place) {
		this.edc_place = edc_place;
	}

	public String getPrtnfx_dc_dt() {
		return prtnfx_dc_dt;
	}

	public void setPrtnfx_dc_dt(String prtnfx_dc_dt) {
		this.prtnfx_dc_dt = prtnfx_dc_dt;
	}

	public String getPrtnfx_dsgn_bgnde() {
		return prtnfx_dsgn_bgnde;
	}

	public void setPrtnfx_dsgn_bgnde(String prtnfx_dsgn_bgnde) {
		this.prtnfx_dsgn_bgnde = prtnfx_dsgn_bgnde;
	}

	public String getPrtnfx_dsgn_endde() {
		return prtnfx_dsgn_endde;
	}

	public void setPrtnfx_dsgn_endde(String prtnfx_dsgn_endde) {
		this.prtnfx_dsgn_endde = prtnfx_dsgn_endde;
	}

	public String getPrtnfx_cntrwk_bgnde() {
		return prtnfx_cntrwk_bgnde;
	}

	public void setPrtnfx_cntrwk_bgnde(String prtnfx_cntrwk_bgnde) {
		this.prtnfx_cntrwk_bgnde = prtnfx_cntrwk_bgnde;
	}

	public String getPrtnfx_cntrwk_endde() {
		return prtnfx_cntrwk_endde;
	}

	public void setPrtnfx_cntrwk_endde(String prtnfx_cntrwk_endde) {
		this.prtnfx_cntrwk_endde = prtnfx_cntrwk_endde;
	}

	public String getPrtnfx_sc_de() {
		return prtnfx_sc_de;
	}

	public void setPrtnfx_sc_de(String prtnfx_sc_de) {
		this.prtnfx_sc_de = prtnfx_sc_de;
	}

	public String getPrtnfx_bizprpare_bgnde() {
		return prtnfx_bizprpare_bgnde;
	}

	public void setPrtnfx_bizprpare_bgnde(String prtnfx_bizprpare_bgnde) {
		this.prtnfx_bizprpare_bgnde = prtnfx_bizprpare_bgnde;
	}

	public String getPrtnfx_bizprpare_endde() {
		return prtnfx_bizprpare_endde;
	}

	public void setPrtnfx_bizprpare_endde(String prtnfx_bizprpare_endde) {
		this.prtnfx_bizprpare_endde = prtnfx_bizprpare_endde;
	}

	public String getPrtnfx_rcritpblanc_bgnde() {
		return prtnfx_rcritpblanc_bgnde;
	}

	public void setPrtnfx_rcritpblanc_bgnde(String prtnfx_rcritpblanc_bgnde) {
		this.prtnfx_rcritpblanc_bgnde = prtnfx_rcritpblanc_bgnde;
	}

	public String getPrtnfx_rcritpblanc_endde() {
		return prtnfx_rcritpblanc_endde;
	}

	public void setPrtnfx_rcritpblanc_endde(String prtnfx_rcritpblanc_endde) {
		this.prtnfx_rcritpblanc_endde = prtnfx_rcritpblanc_endde;
	}

	public String getPrtnfx_bizprogrs_bgnde() {
		return prtnfx_bizprogrs_bgnde;
	}

	public void setPrtnfx_bizprogrs_bgnde(String prtnfx_bizprogrs_bgnde) {
		this.prtnfx_bizprogrs_bgnde = prtnfx_bizprogrs_bgnde;
	}

	public String getPrtnfx_bizprogrs_endde() {
		return prtnfx_bizprogrs_endde;
	}

	public void setPrtnfx_bizprogrs_endde(String prtnfx_bizprogrs_endde) {
		this.prtnfx_bizprogrs_endde = prtnfx_bizprogrs_endde;
	}

	public String getPrtnfx_evlreport_bgnde() {
		return prtnfx_evlreport_bgnde;
	}

	public void setPrtnfx_evlreport_bgnde(String prtnfx_evlreport_bgnde) {
		this.prtnfx_evlreport_bgnde = prtnfx_evlreport_bgnde;
	}

	public String getPrtnfx_evlreport_endde() {
		return prtnfx_evlreport_endde;
	}

	public void setPrtnfx_evlreport_endde(String prtnfx_evlreport_endde) {
		this.prtnfx_evlreport_endde = prtnfx_evlreport_endde;
	}

	public String getAftfat_manage() {
		return aftfat_manage;
	}

	public void setAftfat_manage(String aftfat_manage) {
		this.aftfat_manage = aftfat_manage;
	}

	public String getPropse_flag() {
		return propse_flag;
	}

	public void setPropse_flag(String propse_flag) {
		this.propse_flag = propse_flag;
	}

	public String getPropse_flag_nm() {
		return propse_flag_nm;
	}

	public void setPropse_flag_nm(String propse_flag_nm) {
		this.propse_flag_nm = propse_flag_nm;
	}

	public String getLast_biz_plan_presentn_at() {
		return last_biz_plan_presentn_at;
	}

	public void setLast_biz_plan_presentn_at(String last_biz_plan_presentn_at) {
		this.last_biz_plan_presentn_at = last_biz_plan_presentn_at;
	}

	public String getCch_chck_trget_at() {
		return cch_chck_trget_at;
	}

	public void setCch_chck_trget_at(String cch_chck_trget_at) {
		this.cch_chck_trget_at = cch_chck_trget_at;
	}

	public String getLast_evl_result() {
		return last_evl_result;
	}

	public void setLast_evl_result(String last_evl_result) {
		this.last_evl_result = last_evl_result;
	}

	public String getLast_evl_result_nm() {
		return last_evl_result_nm;
	}

	public void setLast_evl_result_nm(String last_evl_result_nm) {
		this.last_evl_result_nm = last_evl_result_nm;
	}

	public String getAtch_file_id() {
		return atch_file_id;
	}

	public void setAtch_file_id(String atch_file_id) {
		this.atch_file_id = atch_file_id;
	}

	public String getGatch_file_id() {
		return gatch_file_id;
	}

	public void setGatch_file_id(String gatch_file_id) {
		this.gatch_file_id = gatch_file_id;
	}

	public String getPresnatn_atch_file_id() {
		return presnatn_atch_file_id;
	}

	public void setPresnatn_atch_file_id(String presnatn_atch_file_id) {
		this.presnatn_atch_file_id = presnatn_atch_file_id;
	}

	public String getFrst_regist_pnttm() {
		return frst_regist_pnttm;
	}

	public void setFrst_regist_pnttm(String frst_regist_pnttm) {
		this.frst_regist_pnttm = frst_regist_pnttm;
	}

	public String getFrst_register_id() {
		return frst_register_id;
	}

	public void setFrst_register_id(String frst_register_id) {
		this.frst_register_id = frst_register_id;
	}

	public String getLast_updusr_pnttm() {
		return last_updusr_pnttm;
	}

	public void setLast_updusr_pnttm(String last_updusr_pnttm) {
		this.last_updusr_pnttm = last_updusr_pnttm;
	}

	public String getLast_updusr_id() {
		return last_updusr_id;
	}

	public void setLast_updusr_id(String last_updusr_id) {
		this.last_updusr_id = last_updusr_id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInstt_nm() {
		return instt_nm;
	}

	public void setInstt_nm(String instt_nm) {
		this.instt_nm = instt_nm;
	}

	public String getFond_ty_cc() {
		return fond_ty_cc;
	}

	public void setFond_ty_cc(String fond_ty_cc) {
		this.fond_ty_cc = fond_ty_cc;
	}

	public String getFond_ty_cc_nm() {
		return fond_ty_cc_nm;
	}

	public void setFond_ty_cc_nm(String fond_ty_cc_nm) {
		this.fond_ty_cc_nm = fond_ty_cc_nm;
	}

	public String getFclty_ty_cc() {
		return fclty_ty_cc;
	}

	public void setFclty_ty_cc(String fclty_ty_cc) {
		this.fclty_ty_cc = fclty_ty_cc;
	}

	public String getFclty_ty_cc_nm() {
		return fclty_ty_cc_nm;
	}

	public void setFclty_ty_cc_nm(String fclty_ty_cc_nm) {
		this.fclty_ty_cc_nm = fclty_ty_cc_nm;
	}

	public String getFclty_dv_cc() { return fclty_dv_cc; }

	public void setFclty_dv_cc(String fclty_dv_cc) { this.fclty_dv_cc = fclty_dv_cc; }

	public String getFclty_dv_cc_nm() {	return fclty_dv_cc_nm; }

	public void setFclty_dv_cc_nm(String fclty_dv_cc_nm) { this.fclty_dv_cc_nm = fclty_dv_cc_nm; }

	public String getTrobl_ty_cc() {
		return trobl_ty_cc;
	}

	public void setTrobl_ty_cc(String trobl_ty_cc) {
		this.trobl_ty_cc = trobl_ty_cc;
	}

	public String getTrobl_ty_cc_nm() {
		return trobl_ty_cc_nm;
	}

	public void setTrobl_ty_cc_nm(String trobl_ty_cc_nm) {
		this.trobl_ty_cc_nm = trobl_ty_cc_nm;
	}

	public String getRprsntv() {
		return rprsntv;
	}

	public void setRprsntv(String rprsntv) {
		this.rprsntv = rprsntv;
	}

	public String getFond_year() {
		return fond_year;
	}

	public void setFond_year(String fond_year) {
		this.fond_year = fond_year;
	}

	public String getFte_nmpr() {
		return fte_nmpr;
	}

	public void setFte_nmpr(String fte_nmpr) {
		this.fte_nmpr = fte_nmpr;
	}

	public String getFnanc_idpdc() {
		return fnanc_idpdc;
	}

	public void setFnanc_idpdc(String fnanc_idpdc) {
		this.fnanc_idpdc = fnanc_idpdc;
	}

	public String getFnanc_idpdc_rank() {
		return fnanc_idpdc_rank;
	}

	public void setFnanc_idpdc_rank(String fnanc_idpdc_rank) {
		this.fnanc_idpdc_rank = fnanc_idpdc_rank;
	}

	public String getCpr_prmisn_miryfc() {
		return cpr_prmisn_miryfc;
	}

	public void setCpr_prmisn_miryfc(String cpr_prmisn_miryfc) {
		this.cpr_prmisn_miryfc = cpr_prmisn_miryfc;
	}

	public String getUser_nm() {
		return user_nm;
	}

	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}

	public String getEmail_adres() {
		return email_adres;
	}

	public void setEmail_adres(String email_adres) {
		this.email_adres = email_adres;
	}

	public String getTlphon_no() {
		return tlphon_no;
	}

	public void setTlphon_no(String tlphon_no) {
		this.tlphon_no = tlphon_no;
	}

	public String getMoblphon_no() {
		return moblphon_no;
	}

	public void setMoblphon_no(String moblphon_no) {
		this.moblphon_no = moblphon_no;
	}

	public String getFaxphon_no() {
		return faxphon_no;
	}

	public void setFaxphon_no(String faxphon_no) {
		this.faxphon_no = faxphon_no;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getAdres_detail() {
		return adres_detail;
	}

	public void setAdres_detail(String adres_detail) {
		this.adres_detail = adres_detail;
	}

	public String getR_adres() {
		return r_adres;
	}

	public void setR_adres(String r_adres) {
		this.r_adres = r_adres;
	}

	public String getR_adres_detail() {
		return r_adres_detail;
	}

	public void setR_adres_detail(String r_adres_detail) {
		this.r_adres_detail = r_adres_detail;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPj_01() { return pj_01; }

	public void setPj_01(String pj_01) { this.pj_01 = pj_01; }

	public String getPj_02() { return pj_02; }

	public void setPj_02(String pj_02) { this.pj_02 = pj_02; }

	public String getPj_03() { return pj_03; }

	public void setPj_03(String pj_03) { this.pj_03 = pj_03; }

	public String getPj_04() { return pj_04; }

	public void setPj_04(String pj_04) { this.pj_04 = pj_04; }

	public String getPj_05() { return pj_05; }

	public void setPj_05(String pj_05) { this.pj_05 = pj_05; }

	public String getPj_06() { return pj_06; }

	public void setPj_06(String pj_06) { this.pj_06 = pj_06; }

	public String getPj_07() { return pj_07; }

	public void setPj_07(String pj_07) { this.pj_07 = pj_07; }

	public String getOc_01() { return oc_01; }

	public void setOc_01(String oc_01) { this.oc_01 = oc_01; }

	public String getOc_02() { return oc_02; }

	public void setOc_02(String oc_02) { this.oc_02 = oc_02; }

	public String getOc_03() { return oc_03; }

	public void setOc_03(String oc_03) { this.oc_03 = oc_03; }

	public String getOc_04() { return oc_04; }

	public void setOc_04(String oc_04) { this.oc_04 = oc_04; }

	public String getOc_05() { return oc_05; }

	public void setOc_05(String oc_05) { this.oc_05 = oc_05; }

	public String getOc_06() { return oc_06; }

	public void setOc_06(String oc_06) { this.oc_06 = oc_06; }

	public String getOc_07() { return oc_07; }

	public void setOc_07(String oc_07) { this.oc_07 = oc_07; }

	public String getOc_08() { return oc_08; }

	public void setOc_08(String oc_08) { this.oc_08 = oc_08; }

	public String getUd_01_co() {
		return ud_01_co;
	}

	public void setUd_01_co(String ud_01_co) {
		this.ud_01_co = ud_01_co;
	}

	public String getUd_02_co() {
		return ud_02_co;
	}

	public void setUd_02_co(String ud_02_co) {
		this.ud_02_co = ud_02_co;
	}

	public String getUd_03_co() {
		return ud_03_co;
	}

	public void setUd_03_co(String ud_03_co) {
		this.ud_03_co = ud_03_co;
	}

	public String getUd_04_co(){
		return ud_04_co;
	}

	public void setUd_04_co(String ud_04_co){
		this.ud_04_co = ud_04_co;
	}

	public String getUt_01_co() {
		return ut_01_co;
	}

	public void setUt_01_co(String ut_01_co) {
		this.ut_01_co = ut_01_co;
	}

	public String getUt_02_co() {
		return ut_02_co;
	}

	public void setUt_02_co(String ut_02_co) {
		this.ut_02_co = ut_02_co;
	}

	public String getUt_03_co() { return ut_03_co; }

	public void setUt_03_co(String ut_03_co) { this.ut_03_co = ut_03_co; }

	public String getUt_04_co() { return ut_04_co; }

	public void setUt_04_co(String ut_04_co) { this.ut_04_co = ut_04_co; }

	public String getAt_01_co() {
		return at_01_co;
	}

	public void setAt_01_co(String at_01_co) {
		this.at_01_co = at_01_co;
	}

	public String getAt_02_co() {
		return at_02_co;
	}

	public void setAt_02_co(String at_02_co) {
		this.at_02_co = at_02_co;
	}

	public String getAt_03_co() {
		return at_03_co;
	}

	public void setAt_03_co(String at_03_co) {
		this.at_03_co = at_03_co;
	}

	public String getCs_01_co() {
		return cs_01_co;
	}

	public void setCs_01_co(String cs_01_co) {
		this.cs_01_co = cs_01_co;
	}

	public String getCs_02_co() {
		return cs_02_co;
	}

	public void setCs_02_co(String cs_02_co) {
		this.cs_02_co = cs_02_co;
	}

	public String getCs_03_co() {
		return cs_03_co;
	}

	public void setCs_03_co(String cs_03_co) {
		this.cs_03_co = cs_03_co;
	}

	public String getCs_04_co() {
		return cs_04_co;
	}

	public void setCs_04_co(String cs_04_co) {
		this.cs_04_co = cs_04_co;
	}

	public String getCs_05_ct() {
		return cs_05_ct;
	}

	public void setCs_05_ct(String cs_05_ct) {
		this.cs_05_ct = cs_05_ct;
	}

	public String getCs_06_ct() {
		return cs_06_ct;
	}

	public void setCs_06_ct(String cs_06_ct) {
		this.cs_06_ct = cs_06_ct;
	}

	public String getEf_01_co() {
		return ef_01_co;
	}

	public void setEf_01_co(String ef_01_co) {
		this.ef_01_co = ef_01_co;
	}

	public String getEf_02_co() {
		return ef_02_co;
	}

	public void setEf_02_co(String ef_02_co) {
		this.ef_02_co = ef_02_co;
	}

	public String getEf_03_co() {
		return ef_03_co;
	}

	public void setEf_03_co(String ef_03_co) {
		this.ef_03_co = ef_03_co;
	}

	public String getEf_04_co() {
		return ef_04_co;
	}

	public void setEf_04_co(String ef_04_co) {
		this.ef_04_co = ef_04_co;
	}

	public String getEf_05_ct() {
		return ef_05_ct;
	}

	public void setEf_05_ct(String ef_05_ct) {
		this.ef_05_ct = ef_05_ct;
	}

	public String getEf_06_ct() {
		return ef_06_ct;
	}

	public void setEf_06_ct(String ef_06_ct) {
		this.ef_06_ct = ef_06_ct;
	}

	public String getFl_01_co() {
		return fl_01_co;
	}

	public void setFl_01_co(String fl_01_co) {
		this.fl_01_co = fl_01_co;
	}

	public String getFl_02_co() {
		return fl_02_co;
	}

	public void setFl_02_co(String fl_02_co) {
		this.fl_02_co = fl_02_co;
	}

	public String getFl_03_co() {
		return fl_03_co;
	}

	public void setFl_03_co(String fl_03_co) {
		this.fl_03_co = fl_03_co;
	}

	public String getFl_04_co() {
		return fl_04_co;
	}

	public void setFl_04_co(String fl_04_co) {
		this.fl_04_co = fl_04_co;
	}

	public String getFl_05_ct() {
		return fl_05_ct;
	}

	public void setFl_05_ct(String fl_05_ct) {
		this.fl_05_ct = fl_05_ct;
	}

	public String getFl_06_ct() {
		return fl_06_ct;
	}

	public void setFl_06_ct(String fl_06_ct) {
		this.fl_06_ct = fl_06_ct;
	}

	public String getEtc_01() {
		return etc_01;
	}

	public void setEtc_01(String etc_01) {
		this.etc_01 = etc_01;
	}

	public String getEtc_02_co() {
		return etc_02_co;
	}

	public void setEtc_02_co(String etc_02_co) {
		this.etc_02_co = etc_02_co;
	}

	public String getEtc_03_co() {
		return etc_03_co;
	}

	public void setEtc_03_co(String etc_03_co) {
		this.etc_03_co = etc_03_co;
	}

	public String getEtc_04_ct() {
		return etc_04_ct;
	}

	public void setEtc_04_ct(String etc_04_ct) {
		this.etc_04_ct = etc_04_ct;
	}

	public String getEtc_05_ct() {
		return etc_05_ct;
	}

	public void setEtc_05_ct(String etc_05_ct) {
		this.etc_05_ct = etc_05_ct;
	}

	public String getYear_co() {
		return year_co;
	}

	public void setYear_co(String year_co) {
		this.year_co = year_co;
	}

	public String getCs_type() {
		return cs_type;
	}

	public void setCs_type(String cs_type) {
		this.cs_type = cs_type;
	}

	public String getUd_co_avg() {
		return ud_co_avg;
	}

	public void setUd_co_avg(String ud_co_avg) {
		this.ud_co_avg = ud_co_avg;
	}

	public String getOne_edu_mny() {
		return one_edu_mny;
	}

	public void setOne_edu_mny(String one_edu_mny) {
		this.one_edu_mny = one_edu_mny;
	}

	public String getUd_tot_co(){
		return ud_tot_co;
	}

	public void setUd_tot_co(String ud_tot_co){
		this.ud_tot_co = ud_tot_co;
	}

	public String getUt_tot_co(){
		return ut_tot_co;
	}

	public void setUt_tot_co(String ut_tot_co){
		this.ut_tot_co = ut_tot_co;
	}

	public String getAt_tot_co(){
		return at_tot_co;
	}

	public void setAt_tot_co(String at_tot_co){
		this.at_tot_co = at_tot_co;
	}

	public String getGf_01_ct() {
		return gf_01_ct;
	}

	public void setGf_01_ct(String gf_01_ct) {
		this.gf_01_ct = gf_01_ct;
	}

	public String getGf_02_ct() {
		return gf_02_ct;
	}

	public void setGf_02_ct(String gf_02_ct) {
		this.gf_02_ct = gf_02_ct;
	}

	public String getGf_03_ct() {
		return gf_03_ct;
	}

	public void setGf_03_ct(String gf_03_ct) {
		this.gf_03_ct = gf_03_ct;
	}

	public String getGf_04_ct() {
		return gf_04_ct;
	}

	public void setGf_04_ct(String gf_04_ct) {
		this.gf_04_ct = gf_04_ct;
	}

	public String getGf_05_ct() {
		return gf_05_ct;
	}

	public void setGf_05_ct(String gf_05_ct) {
		this.gf_05_ct = gf_05_ct;
	}

	public String getGf_06_ct() {
		return gf_06_ct;
	}

	public void setGf_06_ct(String gf_06_ct) {
		this.gf_06_ct = gf_06_ct;
	}

	public String getGf_07_ct() {
		return gf_07_ct;
	}

	public void setGf_07_ct(String gf_07_ct) {
		this.gf_07_ct = gf_07_ct;
	}

	public String getGf_08_ct() {
		return gf_08_ct;
	}

	public void setGf_08_ct(String gf_08_ct) {
		this.gf_08_ct = gf_08_ct;
	}

	public String getGf_09_ct() {
		return gf_09_ct;
	}

	public void setGf_09_ct(String gf_09_ct) {
		this.gf_09_ct = gf_09_ct;
	}

	public String getGf_10_ct() {
		return gf_10_ct;
	}

	public void setGf_10_ct(String gf_10_ct) {
		this.gf_10_ct = gf_10_ct;
	}

	public String getGf_11_ct() {
		return gf_11_ct;
	}

	public void setGf_11_ct(String gf_11_ct) {
		this.gf_11_ct = gf_11_ct;
	}

	public String getGf_12_ct() {
		return gf_12_ct;
	}

	public void setGf_12_ct(String gf_12_ct) {
		this.gf_12_ct = gf_12_ct;
	}

	public String getGf_13_ct() {
		return gf_13_ct;
	}

	public void setGf_13_ct(String gf_13_ct) {
		this.gf_13_ct = gf_13_ct;
	}

	public String getGf_14_ct() {
		return gf_14_ct;
	}

	public void setGf_14_ct(String gf_14_ct) {
		this.gf_14_ct = gf_14_ct;
	}

	public String getGf_15_ct() {
		return gf_15_ct;
	}

	public void setGf_15_ct(String gf_15_ct) {
		this.gf_15_ct = gf_15_ct;
	}

	public String getGf_16_ct() {
		return gf_16_ct;
	}

	public void setGf_16_ct(String gf_16_ct) {
		this.gf_16_ct = gf_16_ct;
	}

	public String getGf_17_ct() {
		return gf_17_ct;
	}

	public void setGf_17_ct(String gf_17_ct) {
		this.gf_17_ct = gf_17_ct;
	}

	public String getGf_18_ct() {
		return gf_18_ct;
	}

	public void setGf_18_ct(String gf_18_ct) {
		this.gf_18_ct = gf_18_ct;
	}

	public String getGf_19_ct() {
		return gf_19_ct;
	}

	public void setGf_19_ct(String gf_19_ct) {
		this.gf_19_ct = gf_19_ct;
	}

	public String getGf_20_ct() {
		return gf_20_ct;
	}

	public void setGf_20_ct(String gf_20_ct) {
		this.gf_20_ct = gf_20_ct;
	}

	public String getGf_21_ct() {
		return gf_21_ct;
	}

	public void setGf_21_ct(String gf_21_ct) {
		this.gf_21_ct = gf_21_ct;
	}

	public String getGf_22_ct() {
		return gf_22_ct;
	}

	public void setGf_22_ct(String gf_22_ct) {
		this.gf_22_ct = gf_22_ct;
	}

	public String getGf_23_ct() {
		return gf_23_ct;
	}

	public void setGf_23_ct(String gf_23_ct) {
		this.gf_23_ct = gf_23_ct;
	}

	public String getSp_01_ct() {
		return sp_01_ct;
	}

	public void setSp_01_ct(String sp_01_ct) {
		this.sp_01_ct = sp_01_ct;
	}

	public String getSp_02_ct() {
		return sp_02_ct;
	}

	public void setSp_02_ct(String sp_02_ct) {
		this.sp_02_ct = sp_02_ct;
	}

	public String getSp_03_ct() {
		return sp_03_ct;
	}

	public void setSp_03_ct(String sp_03_ct) {
		this.sp_03_ct = sp_03_ct;
	}

	public String getSp_04_ct() {
		return sp_04_ct;
	}

	public void setSp_04_ct(String sp_04_ct) {
		this.sp_04_ct = sp_04_ct;
	}

	public String getSp_05_ct() {
		return sp_05_ct;
	}

	public void setSp_05_ct(String sp_05_ct) {
		this.sp_05_ct = sp_05_ct;
	}

	public String getSp_06_ct() {
		return sp_06_ct;
	}

	public void setSp_06_ct(String sp_06_ct) {
		this.sp_06_ct = sp_06_ct;
	}

	public String getSp_07_ct() {
		return sp_07_ct;
	}

	public void setSp_07_ct(String sp_07_ct) {
		this.sp_07_ct = sp_07_ct;
	}

	public String getSp_08_ct() {
		return sp_08_ct;
	}

	public void setSp_08_ct(String sp_08_ct) {
		this.sp_08_ct = sp_08_ct;
	}

	public String getSp_09_ct() {
		return sp_09_ct;
	}

	public void setSp_09_ct(String sp_09_ct) {
		this.sp_09_ct = sp_09_ct;
	}

	public String getPlt_ct() {
		return plt_ct;
	}

	public void setPlt_ct(String plt_ct) {
		this.plt_ct = plt_ct;
	}

	public String getFnd_ct() {
		return fnd_ct;
	}

	public void setFnd_ct(String fnd_ct) {
		this.fnd_ct = fnd_ct;
	}

	public String getFct_ct() {
		return fct_ct;
	}

	public void setFct_ct(String fct_ct) {
		this.fct_ct = fct_ct;
	}

	public String getEtc_ct() {
		return etc_ct;
	}

	public void setEtc_ct(String etc_ct) {
		this.etc_ct = etc_ct;
	}

	public String getBiz_ct_tot(){
		return biz_ct_tot;
	}

	public void setBiz_ct_tot(String biz_ct_tot){
		this.biz_ct_tot = biz_ct_tot;
	}

	public String getBiz_ct_tot2(){
		return biz_ct_tot2;
	}

	public void setBiz_ct_tot2(String biz_ct_tot2){
		this.biz_ct_tot2 = biz_ct_tot2;
	}

	public String getPcp_qy() {
		return pcp_qy;
	}

	public void setPcp_qy(String pcp_qy) {
		this.pcp_qy = pcp_qy;
	}

	public String getPsf_qy() {
		return psf_qy;
	}

	public void setPsf_qy(String psf_qy) {
		this.psf_qy = psf_qy;
	}

	public String getIsw_cn() {
		return isw_cn;
	}

	public void setIsw_cn(String isw_cn) {
		this.isw_cn = isw_cn;
	}

	public String getFcw_cn() {
		return fcw_cn;
	}

	public void setFcw_cn(String fcw_cn) {
		this.fcw_cn = fcw_cn;
	}

	public String getPlt_ct_avg() {
		return plt_ct_avg;
	}

	public void setPlt_ct_avg(String plt_ct_avg) {
		this.plt_ct_avg = plt_ct_avg;
	}

	public String getFnd_ct_avg() {
		return fnd_ct_avg;
	}

	public void setFnd_ct_avg(String fnd_ct_avg) {
		this.fnd_ct_avg = fnd_ct_avg;
	}

	public String getFct_ct_avg() {
		return fct_ct_avg;
	}

	public void setFct_ct_avg(String fct_ct_avg) {
		this.fct_ct_avg = fct_ct_avg;
	}

	public String getEtc_ct_avg() {
		return etc_ct_avg;
	}

	public void setEtc_ct_avg(String etc_ct_avg) {
		this.etc_ct_avg = etc_ct_avg;
	}

	public String getEtc_ct_avg2(){
		return etc_ct_avg2;
	}

	public void setEtc_ct_avg2(String etc_ct_avg2){
		this.etc_ct_avg2 = etc_ct_avg2;
	}

	public String getBiz_ct_avg(){
		return biz_ct_avg;
	}

	public void setBiz_ct_avg(String biz_ct_avg){
		this.biz_ct_avg = biz_ct_avg;
	}

	public String getBiz_ct_avg2(){
		return biz_ct_avg2;
	}

	public void setBiz_ct_avg2(String biz_ct_avg2){
		this.biz_ct_avg2 = biz_ct_avg2;
	}

	public String getGf_7_9_ct_tot() {
		return gf_7_9_ct_tot;
	}

	public void setGf_7_9_ct_tot(String gf_7_9_ct_tot) {
		this.gf_7_9_ct_tot = gf_7_9_ct_tot;
	}

	public String getGf_10_20_ct_tot() {
		return gf_10_20_ct_tot;
	}

	public void setGf_10_20_ct_tot(String gf_10_20_ct_tot) {
		this.gf_10_20_ct_tot = gf_10_20_ct_tot;
	}

	public String getGf_7_20_ct_tot() {
		return gf_7_20_ct_tot;
	}

	public void setGf_7_20_ct_tot(String gf_7_20_ct_tot) {
		this.gf_7_20_ct_tot = gf_7_20_ct_tot;
	}

	public String getGf_7_21_ct_tot() {
		return gf_7_21_ct_tot;
	}

	public void setGf_7_21_ct_tot(String gf_7_21_ct_tot) {
		this.gf_7_21_ct_tot = gf_7_21_ct_tot;
	}

	public String getGf_7_9_ct_avg() {
		return gf_7_9_ct_avg;
	}

	public void setGf_7_9_ct_avg(String gf_7_9_ct_avg) {
		this.gf_7_9_ct_avg = gf_7_9_ct_avg;
	}

	public String getGf_21_ct_avg() {
		return gf_21_ct_avg;
	}

	public void setGf_21_ct_avg(String gf_21_ct_avg) {
		this.gf_21_ct_avg = gf_21_ct_avg;
	}

	public String getGf_10_20_ct_avg() {
		return gf_10_20_ct_avg;
	}

	public void setGf_10_20_ct_avg(String gf_10_20_ct_avg) {
		this.gf_10_20_ct_avg = gf_10_20_ct_avg;
	}

	public String getGf_7_21_ct_avg(){
		return gf_7_21_ct_avg;
	}

	public void setGf_7_21_ct_avg(String gf_7_21_ct_avg){
		this.gf_7_21_ct_avg = gf_7_21_ct_avg;
	}

	public String getGreen_tot() {
		return green_tot;
	}

	public void setGreen_tot(String green_tot) {
		this.green_tot = green_tot;
	}

	public String getJabudam_tot() {
		return jabudam_tot;
	}

	public void setJabudam_tot(String jabudam_tot) {
		this.jabudam_tot = jabudam_tot;
	}

	public String getGreen_avg(){
		return green_avg;
	}

	public void setGreen_avg(String green_avg){
		this.green_avg = green_avg;
	}

	public String getJabudam_avg(){
		return jabudam_avg;
	}

	public void setJabudam_avg(String jabudam_avg){
		this.jabudam_avg = jabudam_avg;
	}

	public String getOper_progrm_cnt() {
		return oper_progrm_cnt;
	}

	public void setOper_progrm_cnt(String oper_progrm_cnt) {
		this.oper_progrm_cnt = oper_progrm_cnt;
	}

	public List<BasicContentsSequenceVo> getBcslist() {
		return bcslist;
	}

	public void setBcslist(List<BasicContentsSequenceVo> bcslist) {
		this.bcslist = bcslist;
	}

	public List<BasicOperationalProgrammeVo> getBoplist() {
		return boplist;
	}

	public void setBoplist(List<BasicOperationalProgrammeVo> boplist) {
		this.boplist = boplist;
	}

	public List<BasicBusinessScaleVo> getBbslist() {
		return bbslist;
	}

	public void setBbslist(List<BasicBusinessScaleVo> bbslist) {
		this.bbslist = bbslist;
	}

	public String getFclts_ct_avg(){
		return fclts_ct_avg;
	}

	public void setFclts_ct_avg(String fclts_ct_avg){
		this.fclts_ct_avg = fclts_ct_avg;
	}

	public String getCc_ct(){
		return cc_ct;
	}

	public void setCc_ct(String cc_ct){
		this.cc_ct = cc_ct;
	}

	public String getRsc_ct(){
		return rsc_ct;
	}

	public void setRsc_ct(String rsc_ct){
		this.rsc_ct = rsc_ct;
	}

	public String getTprc_ct(){
		return tprc_ct;
	}

	public void setTprc_ct(String tprc_ct){
		this.tprc_ct = tprc_ct;
	}

	public String getTrc_ct(){
		return trc_ct;
	}

	public void setTrc_ct(String trc_ct){
		this.trc_ct = trc_ct;
	}

	public String getFclts_ct(){
		return fclts_ct;
	}

	public void setFclts_ct(String fclts_ct){
		this.fclts_ct = fclts_ct;
	}

	public String getCc_ct_avg(){
		return cc_ct_avg;
	}

	public void setCc_ct_avg(String cc_ct_avg){
		this.cc_ct_avg = cc_ct_avg;
	}

	public String getRsc_ct_avg(){
		return rsc_ct_avg;
	}

	public void setRsc_ct_avg(String rsc_ct_avg){
		this.rsc_ct_avg = rsc_ct_avg;
	}

	public String getTprc_ct_avg(){
		return tprc_ct_avg;
	}

	public void setTprc_ct_avg(String tprc_ct_avg){
		this.tprc_ct_avg = tprc_ct_avg;
	}

	public String getTrc_ct_avg(){
		return trc_ct_avg;
	}

	public void setTrc_ct_avg(String trc_ct_avg){
		this.trc_ct_avg = trc_ct_avg;
	}

	public String getTp_now_sttus(){
		return tp_now_sttus;
	}

	public void setTp_now_sttus(String tp_now_sttus){
		this.tp_now_sttus = tp_now_sttus;
	}

	public String getBiz_se(){
		return biz_se;
	}

	public void setBiz_se(String biz_se){
		this.biz_se = biz_se;
	}

	public String getEdc_stle(){
		return edc_stle;
	}

	public void setEdc_stle(String edc_stle){
		this.edc_stle = edc_stle;
	}

	public String getEdc_pd(){
		return edc_pd;
	}

	public void setEdc_pd(String edc_pd){
		this.edc_pd = edc_pd;
	}

	public String getFond_ty_gi(){
		return fond_ty_gi;
	}

	public void setFond_ty_gi(String fond_ty_gi){
		this.fond_ty_gi = fond_ty_gi;
	}

	public String getPropse_papers_rcept_bgnde(){
		return propse_papers_rcept_bgnde;
	}

	public void setPropse_papers_rcept_bgnde(String propse_papers_rcept_bgnde){
		this.propse_papers_rcept_bgnde = propse_papers_rcept_bgnde;
	}

	public String getPropse_papers_rcept_endde(){
		return propse_papers_rcept_endde;
	}

	public void setPropse_papers_rcept_endde(String propse_papers_rcept_endde){
		this.propse_papers_rcept_endde = propse_papers_rcept_endde;
	}

	public String getBuild_st_cc() { return build_st_cc; }

	public void setBuild_st_cc(String build_st_cc) { this.build_st_cc = build_st_cc; }

	public String getBuild_st_cc_nm() { return build_st_cc_nm; }

	public void setBuild_st_cc_nm(String build_st_cc_nm) { this.build_st_cc_nm = build_st_cc_nm; }

	public String getBuild_lc_cc_nm() { return build_lc_cc_nm; }

	public void setBuild_lc_cc_nm(String build_lc_cc_nm) { this.build_lc_cc_nm = build_lc_cc_nm; }

	public String getResi_ty_cc_nm() { return resi_ty_cc_nm; }

	public void setResi_ty_cc_nm(String resi_ty_cc_nm) { this.resi_ty_cc_nm = resi_ty_cc_nm; }

	public String getBuild_st_dt() { return build_st_dt; }

	public void setBuild_st_dt(String build_st_dt) { this.build_st_dt = build_st_dt; }

	public String getBuild_lc_cc() { return build_lc_cc; }

	public void setBuild_lc_cc(String build_lc_cc) { this.build_lc_cc = build_lc_cc; }

	public String getBuild_lc_dt() { return build_lc_dt; }

	public void setBuild_lc_dt(String build_lc_dt) { this.build_lc_dt = build_lc_dt; }

	public String getBuild_fond_year() { return build_fond_year; }

	public void setBuild_fond_year(String build_fond_year) { this.build_fond_year = build_fond_year; }

	public String getBuild_fond_month() { return build_fond_month;}

	public void setBuild_fond_month(String build_fond_month) { this.build_fond_month = build_fond_month; }

	public String getOwn_ar1() { return own_ar1; }

	public void setOwn_ar1(String own_ar1) { this.own_ar1 = own_ar1; }

	public String getOwn_ar2() { return own_ar2; }

	public void setOwn_ar2(String own_ar2) { this.own_ar2 = own_ar2; }

	public String getResi_ty_cc() { return resi_ty_cc; }

	public void setResi_ty_cc(String resi_ty_cc) { this.resi_ty_cc = resi_ty_cc; }

	public String getResi_ty_dt() { return resi_ty_dt; }

	public void setResi_ty_dt(String resi_ty_dt) { this.resi_ty_dt = resi_ty_dt; }

	public String getMake_place_num() { return make_place_num; }

	public void setMake_place_num(String make_place_num) { this.make_place_num = make_place_num; }

	public String getTyp_ch_cc() { return typ_ch_cc; }

	public void setTyp_ch_cc(String typ_ch_cc) { this.typ_ch_cc = typ_ch_cc; }

	public String getTyp_ch_cc_nm() { return typ_ch_cc_nm; }

	public void setTyp_ch_cc_nm(String typ_ch_cc_nm) { this.typ_ch_cc_nm = typ_ch_cc_nm; }

	public String getFclty_user_qy() { return fclty_user_qy; }

	public void setFclty_user_qy(String fclty_user_qy) { this.fclty_user_qy = fclty_user_qy; }

	public String getMake_user_qy() { return make_user_qy; }

	public void setMake_user_qy(String make_user_qy) { this.make_user_qy = make_user_qy; }

	public String getBiz_contents() { return biz_contents; }

	public void setBiz_contents(String biz_contents) { this.biz_contents = biz_contents; }

	public String getFclty_pudn_bgnde() { return fclty_pudn_bgnde; }

	public void setFclty_pudn_bgnde(String fclty_pudn_bgnde) { this.fclty_pudn_bgnde = fclty_pudn_bgnde; }

	public String getFclty_pudn_endde() { return fclty_pudn_endde; }

	public void setFclty_pudn_endde(String fclty_pudn_endde) { this.fclty_pudn_endde = fclty_pudn_endde; }

	public String getConstruction_bgnde() { return construction_bgnde; }

	public void setConstruction_bgnde(String construction_bgnde) { this.construction_bgnde = construction_bgnde; }

	public String getConstruction_endde() { return construction_endde; }

	public void setConstruction_endde(String construction_endde) { this.construction_endde = construction_endde; }

	public String getEtc_purps_contents() { return etc_purps_contents; }

	public void setEtc_purps_contents(String etc_purps_contents) { this.etc_purps_contents = etc_purps_contents; }

	public String getAlt_space_plan() { return alt_space_plan; }

	public void setAlt_space_plan(String alt_space_plan) { this.alt_space_plan = alt_space_plan; }

	public String getGf_09_ct_tot() { return gf_09_ct_tot; }

	public void setGf_09_ct_tot(String gf_09_ct_tot) { this.gf_09_ct_tot = gf_09_ct_tot; }
}