package egovframework.com.gfund.biz.bassinfo.service;

public class BasicOperationalProgrammeVo {

	private String biz_progrm_id;
	private String biz_id;
	private String progrm_nm;
	private String bgnde;
	private String endde;
	private String tot_co;
	private String ctnu_at;
	private String edc_co;
	private String round_edcde_co;
	private String round_instrctr_co;
	private String edc_trget_01_co;
	private String edc_trget_02_co;
	private String edc_trget_03_co;
	private String edc_trget_04_co;
	private String edc_trget_05_co;
	private String edc_trget_06_co;
	private String edc_place;

	public String getBiz_progrm_id() {
		return biz_progrm_id;
	}

	public void setBiz_progrm_id(String biz_progrm_id) {
		this.biz_progrm_id = biz_progrm_id;
	}

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public String getProgrm_nm() {
		return progrm_nm;
	}

	public void setProgrm_nm(String progrm_nm) {
		this.progrm_nm = progrm_nm;
	}

	public String getBgnde() {
		return bgnde;
	}

	public void setBgnde(String bgnde) {
		this.bgnde = bgnde;
	}

	public String getEndde() {
		return endde;
	}

	public void setEndde(String endde) {
		this.endde = endde;
	}

	public String getTot_co() {
		return tot_co;
	}

	public void setTot_co(String tot_co) {
		this.tot_co = tot_co;
	}

	public String getCtnu_at() {
		return ctnu_at;
	}

	public void setCtnu_at(String ctnu_at) {
		this.ctnu_at = ctnu_at;
	}

	public String getEdc_co() {
		return edc_co;
	}

	public void setEdc_co(String edc_co) {
		this.edc_co = edc_co;
	}

	public String getRound_edcde_co() {
		return round_edcde_co;
	}

	public void setRound_edcde_co(String round_edcde_co) {
		this.round_edcde_co = round_edcde_co;
	}

	public String getRound_instrctr_co() {
		return round_instrctr_co;
	}

	public void setRound_instrctr_co(String round_instrctr_co) {
		this.round_instrctr_co = round_instrctr_co;
	}

	public String getEdc_trget_01_co() {
		return edc_trget_01_co;
	}

	public void setEdc_trget_01_co(String edc_trget_01_co) {
		this.edc_trget_01_co = edc_trget_01_co;
	}

	public String getEdc_trget_02_co() {
		return edc_trget_02_co;
	}

	public void setEdc_trget_02_co(String edc_trget_02_co) {
		this.edc_trget_02_co = edc_trget_02_co;
	}

	public String getEdc_trget_03_co() {
		return edc_trget_03_co;
	}

	public void setEdc_trget_03_co(String edc_trget_03_co) {
		this.edc_trget_03_co = edc_trget_03_co;
	}

	public String getEdc_trget_04_co() {
		return edc_trget_04_co;
	}

	public void setEdc_trget_04_co(String edc_trget_04_co) {
		this.edc_trget_04_co = edc_trget_04_co;
	}

	public String getEdc_trget_05_co() {
		return edc_trget_05_co;
	}

	public void setEdc_trget_05_co(String edc_trget_05_co) {
		this.edc_trget_05_co = edc_trget_05_co;
	}

	public String getEdc_trget_06_co() {
		return edc_trget_06_co;
	}

	public void setEdc_trget_06_co(String edc_trget_06_co) {
		this.edc_trget_06_co = edc_trget_06_co;
	}

	public String getEdc_place() {
		return edc_place;
	}

	public void setEdc_place(String edc_place) {
		this.edc_place = edc_place;
	}
}
