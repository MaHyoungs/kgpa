package egovframework.com.gfund.biz.bassinfo.service.impl;

import egovframework.com.gfund.biz.bassinfo.service.*;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository("basicInformationDao")
public class BasicInformationDao extends EgovAbstractDAO {


	/**
	 * 기본정보 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.insertBasicInformation", basicInformationVo);
	}

	/**
	 * 기본정보 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBasicInformation", basicInformationVo);
	}

	/**
	 * 기본정보 진행단계(공통코드) 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationStep(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBasicInformationStep", basicInformationVo);
	}

	/**
	 * 기본정보 > 최종평가결과 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationForEvlRst(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBasicInformationForEvlRst", basicInformationVo);
	}

	/**
	 * 기본정보 > 발표자료 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updatePresentationsFile(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updatePresentationsFile", basicInformationVo);
	}

	/**
	 * 기본정보 > 최종사업계획서제출여부 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateFinalBusinessPlan(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateFinalBusinessPlan", basicInformationVo);
	}

	/**
	 * 기본정보 > 제출구분 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationSubmitPresence(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBasicInformationSubmitPresence", basicInformationVo);
	}
	
	/**
	 * 기본정보 > 지도점검대상여부 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBasicInformationCchchckTargetAt(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBasicInformationCchchckTargetAt", basicInformationVo);
	}

	/**
	 * 기본정보 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return delete("basicInformationDao.deleteBasicInformation", basicInformationVo);
	}









	/**
	 * 기본정보 > 기관정보 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.insertOrganization", basicInformationVo);
	}

	/**
	 * 기본정보 > 기관정보 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateOrganization", basicInformationVo);
	}

	/**
	 * 기본정보 > 기관정보 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return delete("basicInformationDao.deleteOrganization", basicInformationVo);
	}











	/**
	 * 기본정보 > 사업대상 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.insertBusinessTarget", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업대상 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBusinessTarget", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업대상 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessTarget", basicInformationVo);
	}











	/**
	 * 기본정보 > 사업비내역 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.insertBusinessExpenses", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업비내역 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBusinessExpenses", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업비내역 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessExpenses", basicInformationVo);
	}










	/**
	 * 기본정보 > 사업내용 등록
	 * @param basicInformationVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.insertBusinessContents", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업내용 수정
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return update("basicInformationDao.updateBusinessContents", basicInformationVo);
	}

	/**
	 * 기본정보 > 사업내용 삭제
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessContents", basicInformationVo);
	}











	/**
	 * 기본정보 > 사업연속성 등록
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return update("basicInformationDao.insertBusinessContentsSequence", basicContentsSequenceVo);
	}

	/**
	 * 기본정보 > 사업연속성 수정
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return update("basicInformationDao.updateBusinessContentsSequence", basicContentsSequenceVo);
	}

	/**
	 * 기본정보 > 사업연속성 삭제
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessContentsSequence", basicContentsSequenceVo);
	}

	/**
	 * 기본정보 > 사업연속성 전체삭제
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessContentsSequences", basicContentsSequenceVo);
	}

	/**
	 * 기본정보 > 사업연속성 조회
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public BasicContentsSequenceVo selectBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return (BasicContentsSequenceVo)selectByPk("basicInformationDao.selectBusinessContentsSequence", basicContentsSequenceVo);
	}

	/**
	 * 기본정보 > 사업연속성 목록 조회
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicContentsSequenceVo> selectBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return list("basicInformationDao.selectBusinessContentsSequences", basicContentsSequenceVo);
	}











	/**
	 * 기본정보 > 운영프로그램 등록
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return update("basicInformationDao.insertBusinessOperationalProgramme", basicOperationalProgrammeVo);
	}

	/**
	 * 기본정보 > 운영프로그램 수정
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return update("basicInformationDao.updateBusinessOperationalProgramme", basicOperationalProgrammeVo);
	}

	/**
	 * 기본정보 > 운영프로그램 삭제
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessOperationalProgramme", basicOperationalProgrammeVo);
	}

	/**
	 * 기본정보 > 운영프로그램 전체삭제
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessOperationalProgrammes", basicOperationalProgrammeVo);
	}

	/**
	 * 기본정보 > 운영프로그램 조회
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public BasicOperationalProgrammeVo selectBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return (BasicOperationalProgrammeVo)selectByPk("basicInformationDao.selectBusinessOperationalProgramme", basicOperationalProgrammeVo);
	}

	/**
	 * 기본정보 > 운영프로그램 목록 조회
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicOperationalProgrammeVo> selectBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return list("basicInformationDao.selectBusinessOperationalProgrammes", basicOperationalProgrammeVo);
	}











	/**
	 * 기본정보 > 사업규모 등록
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return update("basicInformationDao.insertBusinessScale", basicBusinessScaleVo);
	}

	/**
	 * 기본정보 > 사업규모 수정
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return update("basicInformationDao.updateBusinessScale", basicBusinessScaleVo);
	}

	/**
	 * 기본정보 > 사업규모 삭제
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessScale", basicBusinessScaleVo);
	}

	/**
	 * 기본정보 > 사업규모 전체삭제
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return delete("basicInformationDao.deleteBusinessScales", basicBusinessScaleVo);
	}

	/**
	 * 기본정보 > 사업규모 조회
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public BasicBusinessScaleVo selectBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return (BasicBusinessScaleVo)selectByPk("basicInformationDao.selectBusinessScale", basicBusinessScaleVo);
	}

	/**
	 * 기본정보 > 사업규모 목록 조회
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicBusinessScaleVo> selectBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return list("basicInformationDao.selectBusinessScales", basicBusinessScaleVo);
	}











	/**
	 * 기본정보 > 통합 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public BasicInformationVo selectBusinessBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return (BasicInformationVo)selectByPk("basicInformationDao.selectBusinessBasicInformation", basicInformationVo);
	}

	/**
	 * 기본정보 > 통합 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return list("basicInformationDao.selectBusinessBasicInformations", basicInformationVo);
	}

	/**
	 * 기본정보 > 통합 목록 조회 (모바일)
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectmBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return list("basicInformationDao.selectmBusinessBasicInformations", basicInformationVo);
	}

	/**
	 * 기본정보 > 사전분석자료 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationVo> selectReviewBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return list("basicInformationDao.selectReviewBasicInformations", basicInformationVo);
	}

	/**
	 * 기본정보 > 통합 목록 카운트 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public int selectBusinessBasicInformationsCount(BasicInformationVo basicInformationVo) throws SQLException {
		return (Integer)selectByPk("basicInformationDao.selectBusinessBasicInformationsCount", basicInformationVo);
	}
	
	/**
	 * 기본+부가정보 > 통합 목록 조회
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	public List<BasicInformationAddInfoVo> selectBusinessBasicInformationAddInfos(BasicInformationVo basicInformationVo) throws SQLException {
		return list("basicInformationDao.selectBusinessBasicInformationAddInfos", basicInformationVo);
	}
}
