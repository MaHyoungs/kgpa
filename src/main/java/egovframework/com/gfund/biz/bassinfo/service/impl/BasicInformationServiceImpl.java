package egovframework.com.gfund.biz.bassinfo.service.impl;


import egovframework.com.gfund.biz.bassinfo.service.*;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.util.List;

@Service("basicInformationService")
public class BasicInformationServiceImpl extends AbstractServiceImpl implements BasicInformationService{

	@Resource(name = "basicInformationDao")
	private BasicInformationDao basicInformationDao;

	public int insertBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.insertBasicInformation(basicInformationVo);
	}

	public int updateBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBasicInformation(basicInformationVo);
	}

	public int updateBasicInformationStep(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBasicInformationStep(basicInformationVo);
	}

	public int updateBasicInformationForEvlRst(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBasicInformationForEvlRst(basicInformationVo);
	}

	public int updatePresentationsFile(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updatePresentationsFile(basicInformationVo);
	}

	public int updateFinalBusinessPlan(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updatePresentationsFile(basicInformationVo);
	}

	public int updateBasicInformationSubmitPresence(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updatePresentationsFile(basicInformationVo);
	}
	
	public int updateBasicInformationCchchckTargetAt(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBasicInformationCchchckTargetAt(basicInformationVo);
	}

	public int deleteBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.deleteBasicInformation(basicInformationVo);
	}

	public int insertOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.insertOrganization(basicInformationVo);
	}

	public int updateOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateOrganization(basicInformationVo);
	}

	public int deleteOrganization(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.deleteOrganization(basicInformationVo);
	}

	public int insertBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.insertBusinessTarget(basicInformationVo);
	}

	public int updateBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBusinessTarget(basicInformationVo);
	}

	public int deleteBusinessTarget(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.deleteBusinessTarget(basicInformationVo);
	}

	public int insertBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.insertBusinessExpenses(basicInformationVo);
	}

	public int updateBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBusinessExpenses(basicInformationVo);
	}

	public int deleteBusinessExpenses(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.deleteBusinessExpenses(basicInformationVo);
	}

	public int insertBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.insertBusinessContents(basicInformationVo);
	}

	public int updateBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.updateBusinessContents(basicInformationVo);
	}

	public int deleteBusinessContents(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.deleteBusinessContents(basicInformationVo);
	}

	public int insertBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.insertBusinessContentsSequence(basicContentsSequenceVo);
	}

	public int updateBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.updateBusinessContentsSequence(basicContentsSequenceVo);
	}

	public int deleteBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.deleteBusinessContentsSequence(basicContentsSequenceVo);
	}

	public int deleteBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.deleteBusinessContentsSequences(basicContentsSequenceVo);
	}

	public BasicContentsSequenceVo selectBusinessContentsSequence(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.selectBusinessContentsSequence(basicContentsSequenceVo);
	}

	public List<BasicContentsSequenceVo> selectBusinessContentsSequences(BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		return basicInformationDao.selectBusinessContentsSequences(basicContentsSequenceVo);
	}

	public int insertBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.insertBusinessOperationalProgramme(basicOperationalProgrammeVo);
	}

	public int updateBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.updateBusinessOperationalProgramme(basicOperationalProgrammeVo);
	}

	public int deleteBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.deleteBusinessOperationalProgramme(basicOperationalProgrammeVo);
	}

	public int deleteBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.deleteBusinessOperationalProgrammes(basicOperationalProgrammeVo);
	}

	public BasicOperationalProgrammeVo selectBusinessOperationalProgramme(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.selectBusinessOperationalProgramme(basicOperationalProgrammeVo);
	}

	public List<BasicOperationalProgrammeVo> selectBusinessOperationalProgrammes(BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		return basicInformationDao.selectBusinessOperationalProgrammes(basicOperationalProgrammeVo);
	}

	public int insertBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.insertBusinessScale(basicBusinessScaleVo);
	}

	public int updateBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.updateBusinessScale(basicBusinessScaleVo);
	}

	public int deleteBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.deleteBusinessScale(basicBusinessScaleVo);
	}

	public int deleteBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.deleteBusinessScales(basicBusinessScaleVo);
	}

	public BasicBusinessScaleVo selectBusinessScale(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.selectBusinessScale(basicBusinessScaleVo);
	}

	public List<BasicBusinessScaleVo> selectBusinessScales(BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		return basicInformationDao.selectBusinessScales(basicBusinessScaleVo);
	}

	public BasicInformationVo selectBusinessBasicInformation(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectBusinessBasicInformation(basicInformationVo);
	}

	public List<BasicInformationVo> selectBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectBusinessBasicInformations(basicInformationVo);
	}

	public List<BasicInformationVo> selectmBusinessBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectmBusinessBasicInformations(basicInformationVo);
	}

	public List<BasicInformationVo> selectReviewBasicInformations(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectReviewBasicInformations(basicInformationVo);
	}

	public int selectBusinessBasicInformationsCount(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectBusinessBasicInformationsCount(basicInformationVo);
	}
	
	public List<BasicInformationAddInfoVo> selectBusinessBasicInformationAddInfos(BasicInformationVo basicInformationVo) throws SQLException {
		return basicInformationDao.selectBusinessBasicInformationAddInfos(basicInformationVo);
	}
}
