package egovframework.com.gfund.biz.bassinfo.web;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.*;
import egovframework.com.gfund.biz.selection.service.BizSelectionMngService;
import egovframework.com.gfund.biz.selection.service.BizSelectionMngVo;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.fdl.cmmn.exception.FdlException;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class BasicInformationController {

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	@Resource(name = "bizSelectionMngService")
	private BizSelectionMngService bizSelectionMngService;

	//제안서 시퀸스 서비스
	@Resource(name = "egovBusinessBasicInformationService")
	private EgovIdGnrService basicInformationSeq;

	//사업연속성 시퀸스 서비스
	@Resource(name = "egovBusinessContentsSequenceService")
	private EgovIdGnrService egovBusinessContentsSequenceService;

	//운영프로그램 시퀸스 서비스
	@Resource(name = "egovBusinessOperationalProgrammeService")
	private EgovIdGnrService egovBusinessOperationalProgrammeService;

	//사업규모 시퀸스 서비스
	@Resource(name = "egovBusinessScaleService")
	private EgovIdGnrService egovBusinessScaleService;

	/**
	 * (사용자)제안서 목록(사용자 사업관리)
	 *
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/basicInformationList.do")
	public String basicInformationList(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GreenFundYearVo defaultYear = greenFundYearService.selectGreenFundYearDefault();

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		BasicInformationVo basicInformationVo = new BasicInformationVo();
		basicInformationVo.setUser_id(loginVO.getId());
		basicInformationVo.setIsNotPaging("true");
		basicInformationVo.setOrder_by("listTbl.biz_ty_code ASC, listTbl.year DESC");
		model.addAttribute("bsifList", basicInformationService.selectBusinessBasicInformations(basicInformationVo));
		model.addAttribute("defaultYear", defaultYear);
		return "gfund/biz/bassinfo/basicInformationList.tiles";
	}


	/**
	 * (사용자)제안서 신규 등록 Form
	 *
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/basicInformationNewForm.do")
	public String basicInformationNewForm(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);
		model.addAttribute("bsifVo", bsifVo);

		//사업분류 공통코드
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("bizTyCc", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		return "gfund/biz/bassinfo/basicInformationForm.tiles";
	}


	/**
	 * (사용자)제안서 등록 Form & View
	 *
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/gfund/biz/bassinfo/basicInformationForm.do", "/gfund/biz/bassinfo/basicInformationView.do", "/gfund/biz/bassinfo/basicInformationLastView.do"})
	public String basicInformationForm(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);

		//사업분류 공통코드
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("bizTyCc", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));

		//제안서
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
		model.addAttribute("bsifVo", bsifVo);

		if (bsifVo != null) {
			//사업연속성
			BasicContentsSequenceVo bcsVo = new BasicContentsSequenceVo();
			bcsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bcsList", basicInformationService.selectBusinessContentsSequences(bcsVo));

			//운영프로그램
			BasicOperationalProgrammeVo bopVo = new BasicOperationalProgrammeVo();
			bopVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bopList", basicInformationService.selectBusinessOperationalProgrammes(bopVo));

			//사업규모
			BasicBusinessScaleVo bbsVo = new BasicBusinessScaleVo();
			bbsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bbsList", basicInformationService.selectBusinessScales(bbsVo));
		}

		if (request.getRequestURI().contains("Form")) {
			return "gfund/biz/bassinfo/basicInformationForm.tiles";
		} else {

			BasicInformationVo lastVo=new BasicInformationVo();
			lastVo.setPropse_biz_id(bsifVo.getBiz_id());
			model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));

			return "gfund/biz/bassinfo/basicInformationView.tiles";
		}
	}

	/**
	 * (사용자)제안서 등록 Form & View
	 *
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/basicInformationLastForm.do")
	public String basicInformationLastForm(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);

		//사업분류 공통코드
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("bizTyCc", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));

		//제안서
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
		model.addAttribute("bsifVo", bsifVo);

		if (bsifVo != null) {
			//사업연속성
			BasicContentsSequenceVo bcsVo = new BasicContentsSequenceVo();
			bcsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bcsList", basicInformationService.selectBusinessContentsSequences(bcsVo));

			//운영프로그램
			BasicOperationalProgrammeVo bopVo = new BasicOperationalProgrammeVo();
			bopVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bopList", basicInformationService.selectBusinessOperationalProgrammes(bopVo));

			//사업규모
			BasicBusinessScaleVo bbsVo = new BasicBusinessScaleVo();
			bbsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bbsList", basicInformationService.selectBusinessScales(bbsVo));
		}

		BizSelectionMngVo bsVo=new BizSelectionMngVo();
		bsVo.setBiz_id(bsifVo.getBiz_id());
		model.addAttribute("bsList", bizSelectionMngService.selectBizSelectionStepStates(bsVo));

		return "gfund/biz/bassinfo/basicInformationLastForm.tiles";
	}

	/**
	 * (사용자)사후관리 사업조회 Ajax
	 * @param bsifVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/ajaxSelectBusinessBasicInformation.do")
	public ModelAndView selectBusinessBasicInformation(@ModelAttribute("bsifVo") BasicInformationVo bsifVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.selectBusinessBasicInformation(bsifVo));
		return mav;
	}


	/**
	 * (관리자)제안서 등록 Form & View 조회
	 *
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/selection/proposalForm.do", "/mng/gfund/biz/selection/proposalView.do", "/mng/gfund/biz/selection/finalProposalView.do", "/mng/gfund/biz/businessmng/finalProposalView.do", "/mng/gfund/biz/businessmng/finalProposalForm.do" })
	public String mngBasicInformationForm(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request) throws Exception {
		//제안서
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
		model.addAttribute("bsifVo", bsifVo);

		//사용자 정보 조회
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(bsifVo.getUser_id());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);

		if (bsifVo != null) {
			//사업연속성
			BasicContentsSequenceVo bcsVo = new BasicContentsSequenceVo();
			bcsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bcsList", basicInformationService.selectBusinessContentsSequences(bcsVo));

			//운영프로그램
			BasicOperationalProgrammeVo bopVo = new BasicOperationalProgrammeVo();
			bopVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bopList", basicInformationService.selectBusinessOperationalProgrammes(bopVo));

			//사업규모
			BasicBusinessScaleVo bbsVo = new BasicBusinessScaleVo();
			bbsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bbsList", basicInformationService.selectBusinessScales(bbsVo));
		}

		//선정전형 제안서 Form
		if(request.getRequestURI().contains("selection/proposalForm")) {
			return "mng/gfund/biz/bassinfo/selection/proposalForm";
		}

		//선정전형 제안서 View
		else if(request.getRequestURI().contains("selection/proposalView")) {
			BizSelectionMngVo bsVo=new BizSelectionMngVo();
			bsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bsList", bizSelectionMngService.selectBizSelectionStepStates(bsVo));
			return "mng/gfund/biz/bassinfo/selection/proposalView";
		}

		//선정전형 최종제안서 View
		else if(request.getRequestURI().contains("selection/finalProposalView")) {
			BasicInformationVo lastVo=new BasicInformationVo();
			lastVo.setPropse_biz_id(bsifVo.getBiz_id());
			model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));
			return "mng/gfund/biz/bassinfo/selection/finalProposalView";
		}

		//사업관리 최종제안서 View
		else if(request.getRequestURI().contains("businessmng/finalProposalView")) {
			BasicInformationVo lastVo=new BasicInformationVo();
			lastVo.setPropse_biz_id(bsifVo.getBiz_id());
			model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));

			// 최종사업계획서의 정보로 제안서의 정보 조회
			BasicInformationVo propseVo=new BasicInformationVo();
			propseVo.setBiz_id(bsifVo.getPropse_biz_id());
			model.addAttribute("propseVo", basicInformationService.selectBusinessBasicInformation(propseVo));
			return "mng/gfund/biz/businessmng/finalProposalView";
		}

		//사업관리 최종제안서 Form
		else if(request.getRequestURI().contains("businessmng/finalProposalForm")) {
			BasicInformationVo lastVo=new BasicInformationVo();
			lastVo.setPropse_biz_id(bsifVo.getBiz_id());
			model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));
			return "mng/gfund/biz/businessmng/finalProposalForm";
		}

		else{
			return "";
		}
	}

	/**
	 * (사용자)제안서 임시 저장 및 제출
	 *
	 * @param bsifVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/basicInformationSave.do")
	public String basicInformationSave(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		int saveRs = 0;
		if (bsifVo.getYear() == null) {
			bsifVo.setYear(greenFundYearService.selectGreenFundYearDefault().getYear());
		}

		//최종제안서 제출시 biz_id를 null로 비워 최종 제안서를 새로 insert
		if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST01").equals(bsifVo.getPrst_ty_cc()) && ("N").equals(bsifVo.getLast_biz_plan_presentn_at())){
			bsifVo.setBiz_id("");
			bsifVo.setPrst_ty_cc("PRST02");
		}else if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST01").equals(bsifVo.getPrst_ty_cc()) && ("Y").equals(bsifVo.getLast_biz_plan_presentn_at())){
			bsifVo.setBiz_id("");
			bsifVo.setPrst_ty_cc("PRST02");
		}

		if ("".equals(bsifVo.getBiz_id()) || bsifVo.getBiz_id() == null) {
			bsifVo.setBiz_id(basicInformationSeq.getNextStringId());
			bsifVo.setFrst_register_id(loginVO.getId());
			bsifVo.setUser_id(loginVO.getId());

			//기본정보 등록
			saveRs = basicInformationService.insertBasicInformation(bsifVo);

			if (saveRs > 0) {
				//기본정보 > 기관정보 등록
				basicInformationService.insertOrganization(bsifVo);

				//기본정보 > 사업대상 등록
				basicInformationService.insertBusinessTarget(bsifVo);

				//기본정보 > 사업비내역 등록
				basicInformationService.insertBusinessExpenses(bsifVo);

				//기본정보 > 사업내용 등록
				basicInformationService.insertBusinessContents(bsifVo);

				//사업연속성, 운영프로그램, 사업규모 insert & update
				basicInformationChildSave(bsifVo);

				//파일 업로드
				basicInformationFileUpload(bsifVo);
			}
		} else {
			bsifVo.setLast_updusr_id(loginVO.getId());

			//기본정보 등록
			saveRs = basicInformationService.updateBasicInformation(bsifVo);

			//기본정보 > 기관정보 등록
			basicInformationService.updateOrganization(bsifVo);

			//기본정보 > 사업대상 등록
			basicInformationService.updateBusinessTarget(bsifVo);

			//기본정보 > 사업비내역 등록
			basicInformationService.updateBusinessExpenses(bsifVo);

			//기본정보 > 사업내용 등록
			basicInformationService.updateBusinessContents(bsifVo);

			//사업연속성, 운영프로그램, 사업규모 insert & update
			basicInformationChildSave(bsifVo);

			//파일 업로드
			basicInformationFileUpload(bsifVo);
		}

		//제안단계 임시저장
		if (bsifVo.getPropse_flag().equals("STEP01")) {
			model.addAttribute("saveRs", "1");
			model.addAttribute("year", bsifVo.getYear());
			model.addAttribute("biz_ty_code", bsifVo.getBiz_ty_code());
			model.addAttribute("biz_id", bsifVo.getBiz_id());
			return "redirect:/gfund/biz/bassinfo/basicInformationForm.do";
		}

		//최종사업계획서 임시저장
		else if (bsifVo.getPropse_flag().equals("STEP12") && bsifVo.getPrst_ty_cc().equals("PRST02") && bsifVo.getLast_biz_plan_presentn_at().equals("N")) {
			model.addAttribute("saveRs", "1");
			model.addAttribute("year", bsifVo.getYear());
			model.addAttribute("biz_ty_code", bsifVo.getBiz_ty_code());
			model.addAttribute("biz_id", bsifVo.getBiz_id());
			return "redirect:/gfund/biz/bassinfo/basicInformationLastForm.do";
		}

		//최종사업계획서 최종제출
		else if (bsifVo.getPropse_flag().equals("STEP12") && bsifVo.getPrst_ty_cc().equals("PRST02") && bsifVo.getLast_biz_plan_presentn_at().equals("Y")) {
			model.addAttribute("year", bsifVo.getYear());
			model.addAttribute("biz_ty_code", bsifVo.getBiz_ty_code());
			model.addAttribute("biz_id", bsifVo.getBiz_id());
			return "redirect:/gfund/biz/bassinfo/selection/selectionCompletion.do";
		} else {
			model.addAttribute("saveRs", "2");
			model.addAttribute("year", bsifVo.getYear());
			model.addAttribute("biz_ty_code", bsifVo.getBiz_ty_code());
			model.addAttribute("biz_id", bsifVo.getBiz_id());
			return "redirect:/gfund/biz/bassinfo/basicInformationView.do";
		}
	}

	/**
	 * (관리자)제안서 임시 저장 및 제출
	 *
	 * @param bsifVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/bassinfo/basicInformationSave.do")
	public String mngBasicInformationSave(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		int saveRs = 0;
		if (bsifVo.getYear() == null) {
			bsifVo.setYear(greenFundYearService.selectGreenFundYearDefault().getYear());
		}

		//기본정보 등록
		basicInformationService.updateBasicInformation(bsifVo);

		//기본정보 > 기관정보 등록
		basicInformationService.updateOrganization(bsifVo);

		//기본정보 > 사업대상 등록
		basicInformationService.updateBusinessTarget(bsifVo);

		//기본정보 > 사업비내역 등록
		basicInformationService.updateBusinessExpenses(bsifVo);

		//기본정보 > 사업내용 등록
		basicInformationService.updateBusinessContents(bsifVo);

		//사업연속성, 운영프로그램, 사업규모 insert & update
		basicInformationChildSave(bsifVo);

		//파일 업로드
		basicInformationFileUpload(bsifVo);

		//제안단계 임시저장

		model.addAttribute("saveRs", "1");
		model.addAttribute("biz_id", bsifVo.getBiz_id());

		//사업관리 최종사업계획서 View
		if(bsifVo.getPrst_ty_cc().equals("PRST02") && bsifVo.getLast_biz_plan_presentn_at().equals("Y")){
			return "redirect:/mng/gfund/biz/businessmng/finalProposalView.do";
		}

		//선정전형 제안서 View
		else{
			return "redirect:/mng/gfund/biz/selection/proposalView.do";
		}
	}

	/**
	 * 사업연속성, 운영프로그램, 사업규모 insert & update
	 * @param bsifVo
	 * @throws SQLException
	 * @throws FdlException
	 */
	public void basicInformationChildSave(BasicInformationVo bsifVo) throws SQLException, FdlException {
		//사업연속성 등록,수정
		if (bsifVo.getBcslist() != null) {
			for (BasicContentsSequenceVo bcsVo : bsifVo.getBcslist()) {
				bcsVo.setBiz_id(bsifVo.getBiz_id());
				//최종제안서 제출시 모든 pk null로 비워 새로 insert
				if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("N").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bcsVo.setBiz_ctnu_id("");
				}else if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("Y").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bcsVo.setBiz_ctnu_id("");
				}

				if (!("").equals(bcsVo.getBiz_ctnu_id()) || bcsVo.getBiz_ctnu_id() != "") {
					basicInformationService.updateBusinessContentsSequence(bcsVo);
				} else {
					bcsVo.setBiz_ctnu_id(egovBusinessContentsSequenceService.getNextStringId());
					basicInformationService.insertBusinessContentsSequence(bcsVo);
				}
			}
		}

		//운영프로그램 등록,수정
		if (bsifVo.getBoplist() != null) {
			for (BasicOperationalProgrammeVo bopVo : bsifVo.getBoplist()) {
				bopVo.setBiz_id(bsifVo.getBiz_id());
				//최종제안서 제출시 모든 pk null로 비워 새로 insert
				if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("N").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bopVo.setBiz_progrm_id("");
				}else if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("Y").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bopVo.setBiz_progrm_id("");
				}

				if (!("").equals(bopVo.getBiz_progrm_id()) || bopVo.getBiz_progrm_id() != "") {
					basicInformationService.updateBusinessOperationalProgramme(bopVo);
				} else {
					bopVo.setBiz_progrm_id(egovBusinessOperationalProgrammeService.getNextStringId());
					basicInformationService.insertBusinessOperationalProgramme(bopVo);
				}
			}
		}

		//사업규모 등록,수정
		if (bsifVo.getBbslist() != null) {
			for (BasicBusinessScaleVo bbsVo : bsifVo.getBbslist()) {
				bbsVo.setBiz_id(bsifVo.getBiz_id());
				//최종제안서 제출시 모든 pk null로 비워 새로 insert
				if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("N").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bbsVo.setBiz_scale_id("");
				}else if(!"".equals(bsifVo.getPropse_biz_id()) && ("PRST02").equals(bsifVo.getPrst_ty_cc()) && ("Y").equals(bsifVo.getLast_biz_plan_presentn_at())){
					bbsVo.setBiz_scale_id("");
				}

				if (!("").equals(bbsVo.getBiz_scale_id()) || bbsVo.getBiz_scale_id() != "") {
					basicInformationService.updateBusinessScale(bbsVo);
				} else {
					bbsVo.setBiz_scale_id(egovBusinessScaleService.getNextStringId());
					basicInformationService.insertBusinessScale(bbsVo);
				}
			}
		}
	}

	/**
	 * 제안서 2차 심사 발표자료 업로드
	 * @param bsifVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/updatePresentationsFile.do")
	public String updatePresentationsFile(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model) throws Exception {
		int updateRs = basicInformationService.updatePresentationsFile(bsifVo);
		if (!("").equals(bsifVo.getPresnatn_atch_file_id()) && updateRs > 0) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(bsifVo.getPresnatn_atch_file_id());
			if(!("").equals(bsifVo.getGatch_file_id())) {
				fvo.setAtchFileId(bsifVo.getGatch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);

		model.addAttribute("year", bsifVo.getYear());
		model.addAttribute("biz_ty_code", bsifVo.getBiz_ty_code());
		model.addAttribute("biz_id", bsifVo.getBiz_id());
		model.addAttribute("step_cl", "STEP3");
		model.addAttribute("saveRs", "3");
		return "redirect:/gfund/biz/bassinfo/selection/selectionStepView.do";
	}

	/**
	 * 파일 업로드
	 * @param bsifVo
	 * @throws Exception
	 */
	public void basicInformationFileUpload(BasicInformationVo bsifVo) throws Exception {
		if (!("").equals(bsifVo.getAtch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(bsifVo.getAtch_file_id());
			if(!("").equals(bsifVo.getGatch_file_id())) {
				fvo.setAtchFileId(bsifVo.getGatch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}


	/**
	 * 사업연속성 삭제 Ajax
	 *
	 * @param basicContentsSequenceVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/ajaxDeleteBusinessContentsSequence.do")
	public ModelAndView deleteBusinessContentsSequence(@ModelAttribute("basicContentsSequenceVo") BasicContentsSequenceVo basicContentsSequenceVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.deleteBusinessContentsSequence(basicContentsSequenceVo));
		return mav;
	}

	/**
	 * 운영프로그램 삭제 Ajax
	 *
	 * @param basicOperationalProgrammeVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/ajaxDeleteBusinessOperationalProgramme.do")
	public ModelAndView deleteBusinessOperationalProgramme(@ModelAttribute("basicOperationalProgrammeVo") BasicOperationalProgrammeVo basicOperationalProgrammeVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.deleteBusinessOperationalProgramme(basicOperationalProgrammeVo));
		return mav;
	}

	/**
	 * 사업규모 삭제 Ajax
	 *
	 * @param basicBusinessScaleVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/ajaxDeleteBusinessScale.do")
	public ModelAndView deleteBusinessScale(@ModelAttribute("basicBusinessScaleVo") BasicBusinessScaleVo basicBusinessScaleVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.deleteBusinessScale(basicBusinessScaleVo));
		return mav;
	}


	/**
	 * 사전심사자료 엑셀 다운로드
	 * @param modelMap
	 * @param bsifVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/biz/bassinfo/recordExcelDownload.do")
	public String recordExcelDownload(Map<String,Object> modelMap, @ModelAttribute("bsifVo") BasicInformationVo bsifVo) throws Exception{
		String sheetName = null;
		String[] rowTitleList = null;
		ArrayList<String[]> dataList = new ArrayList<String[]>();

		bsifVo.setIsNotPaging("aa");

		List<BasicInformationVo> list = basicInformationService.selectReviewBasicInformations(bsifVo);
		int idx = 0;
		if(bsifVo.getBiz_ty_code().equals("BTC01")){
			sheetName = "복지시설나눔숲(사회복지시설)";
			rowTitleList=new String[] {"번호", "응모지역", "응모기관", "사업명", "응모액", "조성유형", "수용인원(현원)", "설립유형","시설유형", "조성면적(㎡)", "㎡ 단가", "식재공사(%)", "기반조성(%)", "시설물공사(%)", "기타(%)", "공사원가(%)", "담당자", "전화번호", "이메일"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getTot_wct(), vo.getTrplant_ar_type(), vo.getAceptnc_psncpa()+"("+vo.getAceptnc_nownmpr()+")", vo.getFond_ty_cc_nm(), vo.getFclty_ty_cc_nm(), vo.getTot_trplant_ar(), vo.getWct_trplant_div(), vo.getPlt_ct()+"("+vo.getPlt_ct_avg()+"%)", vo.getFnd_ct()+"("+vo.getFnd_ct_avg()+"%)", vo.getFct_ct()+"("+vo.getFct_ct_avg()+"%)", vo.getEtc_ct()+"("+vo.getEtc_ct_avg()+"%)", vo.getBiz_ct_tot()+"("+vo.getBiz_ct_avg()+"%)",vo.getUser_nm(), vo.getTlphon_no(), vo.getEmail_adres()};
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC02")){
			sheetName = "복지시설나눔숲(특수교육시설)";
			rowTitleList=new String[] {"번호", "응모지역", "응모기관", "사업명", "응모액", "조성유형", "이용자(관리자)", "설립유형", "장애유형", "조성면적(㎡)", "식재공사(%)", "기반조성(%)", "시설물공사(%)", "기타(%)", "담당자", "전화번호", "이메일"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getTot_wct(), vo.getTrplant_ar_type(), vo.getUser_qy()+"("+vo.getMngr_qy()+")", vo.getFond_ty_cc_nm(), vo.getTrobl_ty_cc_nm(), vo.getMake_ar(), vo.getPlt_ct_avg()+"%", vo.getFnd_ct_avg()+"%", vo.getFct_ct_avg()+"%", vo.getEtc_ct_avg()+"%", vo.getUser_nm(), vo.getTlphon_no(), vo.getEmail_adres()};
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC03")){
			sheetName = "지역사회나눔숲";
			rowTitleList=new String[] {"번호", "응모지역", "응모기관", "사업명", "응모액", "조성면적(㎡)", "㎡ 단가", "식재공사(%)", "기반조성(%)", "시설물공사(%)", "기타(%)", "공사원가(%)", "녹색자금(%)", "자부담(%)", "담당자", "전화번호", "이메일"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getTot_wct(), vo.getMake_ar(), vo.getWct_make_div(), vo.getPlt_ct()+"("+vo.getPlt_ct_avg()+"%)", vo.getFnd_ct()+"("+vo.getFnd_ct_avg()+"%)", vo.getFct_ct()+"("+vo.getFct_ct_avg()+"%)", vo.getEtc_ct()+"("+vo.getEtc_ct_avg()+"%)", vo.getBiz_ct_tot()+"("+vo.getBiz_ct_avg()+"%)",  vo.getGreen_tot()+"("+vo.getGreen_avg()+"%)", vo.getJabudam_tot()+"("+vo.getJabudam_avg()+"%)", vo.getUser_nm(), vo.getTlphon_no(), vo.getEmail_adres()};
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC05")){
			sheetName = "숲체험_교육(체험교육사업)";
			rowTitleList=new String[] {"번호", "응모지역", "기관 및 단체명", "제안사업명", "설립유형", "교육형태(일회, 연속)", "교육기간","응모금액", "세부사업분야", "과년도 지원여부(년/백만원/평가결과)", "컨소시엄(협약자수)", "프로그램 참가대상", "대상특성", "연인원", "다문화", "저소득", "장애인", "기타", "소외계층", "유아", "아동", "중학생", "고등학생", "청소년", "대학생", "일반인", "선주", "일반인", "소외계층 비율", "1인당 교육비(원)", "교육장소", "운영프로그램수/횟수", "담당자", "전화번호(사무실)", "전화번호(이동전화)", "이메일", "인건비", "직접비", "일반관리비", "합계", "인건비(%)", "직접비(%)", "일반관리비(%)", "합(%)"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getFond_ty_gi(), vo.getEdc_stle(), vo.getEdc_pd(), vo.getTot_wct(), vo.getBiz_se(), "", vo.getCnsrtm_entrps_nm(), vo.getPj_01() + " " + vo.getPj_02() + " " + vo.getPj_03() + " " + vo.getPj_04() + " " + vo.getPj_05() + " " + vo.getPj_06() + " " + vo.getPj_07(), vo.getOc_01() + " " + vo.getOc_02() + " " + vo.getOc_03() + " " + vo.getOc_04() + " " + vo.getOc_05() + " " + vo.getOc_06() + " " + vo.getOc_07() + " " + vo.getOc_08(), vo.getYear_co(), vo.getUd_01_co(), vo.getUd_02_co(), vo.getUd_03_co(), vo.getUd_04_co(), vo.getUd_tot_co(), vo.getUt_03_co(), vo.getUt_01_co(), vo.getUt_04_co(), vo.getUt_02_co(), vo.getUt_tot_co(), vo.getAt_01_co(), vo.getAt_02_co(), vo.getAt_03_co(), vo.getAt_tot_co(), vo.getUd_co_avg()+"%", vo.getOne_edu_mny(), vo.getEdc_place() ,vo.getOper_progrm_cnt(), vo.getUser_nm(), vo.getTlphon_no(), vo.getMoblphon_no(),vo.getEmail_adres(), vo.getGf_7_9_ct_tot(), vo.getGf_10_20_ct_tot(), vo.getGf_21_ct(), vo.getGf_7_21_ct_tot(), vo.getGf_7_9_ct_avg()+"%", vo.getGf_10_20_ct_avg()+"%", vo.getGf_21_ct_avg()+"%", vo.getGf_7_21_ct_avg()+"%"};
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC06")){
			sheetName = "숲체험_교육(휴양문화사업)";
			rowTitleList=new String[] {"번호", "응모지역", "기관 및 단체명", "제안사업명", "응모금액", "세부사업분야", "과년도 지원여부(년/백만원/평가결과)", "컨소시엄(협약자수)", "연인원", "소외계층 비율", "1인당 교육비(원)", "교육장소", "운영프로그램수/횟수", "담당자", "전화번호(사무실)", "전화번호(이동전화)", "이메일", "인건비", "직접비", "일반관리비", "합계", "인건비(%)", "직접비(%)", "일반관리비(%)", "합(%)"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getTot_wct(), vo.getCs_type(), "", vo.getCnsrtm_entrps_nm(), vo.getYear_co(),vo.getUd_co_avg()+"%", vo.getOne_edu_mny(), vo.getEdc_place() ,vo.getOper_progrm_cnt(), vo.getUser_nm(), vo.getTlphon_no(), vo.getMoblphon_no(),vo.getEmail_adres(), vo.getGf_7_9_ct_tot(), vo.getGf_10_20_ct_tot(), vo.getGf_21_ct(), vo.getGf_7_21_ct_tot(), vo.getGf_7_9_ct_avg()+"%", vo.getGf_10_20_ct_avg()+"%", vo.getGf_10_20_ct_avg()+"%" };
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC08")){
			sheetName = "다함께(무장애) 나눔길";
			rowTitleList=new String[] {"번호", "응모지역", "응모기관", "사업명", "응모액", "나눔길길이(Km)", "공통공사비", "노면공사비", "녹화복원공사비", "지형복원공사비", "시설공사비", "기타비용", "공사원가", "녹색자금(%)", "자부담(%)", "담당자", "전화번호", "이메일"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getBiz_nm(), vo.getTot_wct(), vo.getFclty_all_ar(), vo.getCc_ct()+"("+vo.getCc_ct_avg()+"%)", vo.getRsc_ct()+"("+vo.getRsc_ct_avg()+"%)", vo.getTprc_ct()+"("+vo.getTprc_ct_avg()+"%)", vo.getTrc_ct()+"("+vo.getTrc_ct_avg()+"%)", vo.getFclts_ct()+"("+vo.getFclts_ct_avg()+"%)", vo.getEtc_ct()+"("+vo.getEtc_ct_avg2()+"%)", vo.getBiz_ct_tot2()+"("+vo.getBiz_ct_avg2()+"%)", vo.getGreen_tot()+"("+vo.getGreen_avg()+"%)", vo.getJabudam_tot()+"("+vo.getJabudam_avg()+"%)", vo.getUser_nm(), vo.getTlphon_no(), vo.getEmail_adres()};
				dataList.add(valArr);
			}
		}else if(bsifVo.getBiz_ty_code().equals("BTC09")){
			sheetName = "복지시설환경개선";
			rowTitleList=new String[] {"번호", "지역", "복지시설명", "설립유형", "제안사업명", "총사업비", "시설구분", "시설유형", "인건비", "회의비", "여비교통비", "지급수수료", "기본조사비", "설계비", "공사원가", "감리비", "일반관리비", "합계", "건물현황", "건물위치", "건물준공일", "전용면적", "거주유형", "조성면적 및 조성공간 개소수", "5년이내 용도변경 예정여부", "시설생활 이용인수", "조성공간 이용자수", "사업지(주소)", "담당자", "전화번호(사무실)", "전화번호(이동전화)", "이메일"};
			for(BasicInformationVo vo : list){
				idx++;
				String[] valArr = {Integer.toString(idx), vo.getArea(), vo.getInstt_nm(), vo.getFond_ty_gi(), vo.getBiz_nm(), vo.getTot_wct(), vo.getFclty_dv_cc_nm(), vo.getFclty_ty_cc_nm(), vo.getGf_07_ct(), vo.getGf_14_ct(), vo.getGf_16_ct(), vo.getGf_13_ct(), vo.getGf_17_ct(), vo.getGf_18_ct(), vo.getSp_04_ct(), vo.getGf_19_ct(), vo.getGf_21_ct(), vo.getGf_09_ct_tot(), vo.getBuild_st_cc_nm(), vo.getBuild_lc_cc_nm(), vo.getBuild_fond_year(), vo.getOwn_ar1(), vo.getResi_ty_cc_nm(), vo.getMake_place_num(), vo.getTyp_ch_cc_nm(), vo.getFclty_user_qy(), vo.getMake_user_qy(), vo.getZip() + " " + vo.getAdres() + " " + vo.getAdres_detail(), vo.getUser_nm(), vo.getTlphon_no(), vo.getMoblphon_no(), vo.getEmail_adres()                 };
				dataList.add(valArr);
			}
		}

		modelMap.put("sheetName", sheetName);
		modelMap.put("rowTitleList", rowTitleList);
		modelMap.put("dataList", dataList);
		return "excelView";
	}
}
