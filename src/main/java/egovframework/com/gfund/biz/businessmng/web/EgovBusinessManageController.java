/**
 * 녹색자금통합관리시스템 > 사업관리 > 사업관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.businessmng.web;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.gfund.biz.bassinfo.service.BasicBusinessScaleVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicContentsSequenceVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationAddInfoVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicOperationalProgrammeVo;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovBusinessManageController {
	
	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;

	/**
	 * 사업관리 > 사업관리 목록 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/businessmng/EgovBusinessManageList.do")
	public String EgovBusinessManageList(@ModelAttribute("biVo") BasicInformationVo biVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		PaginationInfo paginationInfo = new PaginationInfo();

		paginationInfo.setCurrentPageNo(biVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(biVo.getPageUnit());
		paginationInfo.setPageSize(biVo.getPageSize());

		biVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		biVo.setLastIndex(paginationInfo.getLastRecordIndex());
		biVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		biVo.setPrst_ty_cc("PRST02");
		biVo.setLast_biz_plan_presentn_at("Y"); // 최종사업계획서제출여부 설정

		List<BasicInformationAddInfoVo> bsifList = basicInformationService.selectBusinessBasicInformationAddInfos(biVo);
		int totCnt = basicInformationService.selectBusinessBasicInformationsCount(biVo);

		paginationInfo.setTotalRecordCount(totCnt);

		model.addAttribute("bsifList", bsifList);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("yearlist", greenFundYearService.selectGreenFundYears());
		
		return "mng/gfund/biz/businessmng/EgovBusinessManageList";
	}
	
	/**
	 * 지도점검대상여부 수정 Ajax
	 *
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/businessmng/ajaxUpdateBasicInformationCchchckTargetAt.do")
	public ModelAndView deleteBusinessScale(@ModelAttribute("basicInformationVo") BasicInformationVo basicInformationVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.updateBasicInformationCchchckTargetAt(basicInformationVo));
		return mav;
	}
	
	/**
	 * 사업관리 > 사업관리 > 기본정보 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/businessmng/EgovBusinessInfoView.do")	
	public String EgovBusinessInfoView(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request) throws Exception {
		//제안서
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
		model.addAttribute("bsifVo", bsifVo);

		//사용자 정보 조회
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(bsifVo.getUser_id());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);
		
		if (bsifVo != null) {
			//사업연속성
			BasicContentsSequenceVo bcsVo = new BasicContentsSequenceVo();
			bcsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bcsList", basicInformationService.selectBusinessContentsSequences(bcsVo));

			//운영프로그램
			BasicOperationalProgrammeVo bopVo = new BasicOperationalProgrammeVo();
			bopVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bopList", basicInformationService.selectBusinessOperationalProgrammes(bopVo));

			//사업규모
			BasicBusinessScaleVo bbsVo = new BasicBusinessScaleVo();
			bbsVo.setBiz_id(bsifVo.getBiz_id());
			model.addAttribute("bbsList", basicInformationService.selectBusinessScales(bbsVo));
		}
		
		BasicInformationVo lastVo = new BasicInformationVo();
		lastVo.setPropse_biz_id(bsifVo.getBiz_id());
		model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));
		
		// 선정전형에서 최종서류제출을 하지 않은 경우 사업관리, 결과산출 하위 메뉴에 접근하지 못하도록 처리
		model.addAttribute("lastBizPlanPresentnAt", bsifVo.getLast_biz_plan_presentn_at()); // 최종사업계획서제출여부 저장
		model.addAttribute("PropseBizId", bsifVo.getPropse_biz_id());
		
		return "gfund/biz/businessmng/EgovBusinessInfoView.tiles";
	}
	
}
