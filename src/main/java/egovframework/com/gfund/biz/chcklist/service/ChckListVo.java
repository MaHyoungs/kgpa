/**
 * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트 관리에 관한 VO 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.chcklist.service;

import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChckListVo extends BasicInformationVo implements Serializable {
	
	/** BIZ_ID, 사업ID */
	private String bizId;
	
	/** ATCH_FILE_ID, 첨부파일ID */
	private String atch_file_id;

	/** CTT_ATCH_FILE_ID, 지도점검표첨부파일ID */
	private String ctt_atch_file_id;

	/** EIPP0101, 수혜대상자가 녹색자금 지원취지와 부합하는가? */
	private String eipp0101;
	
	/** EIPP0102, 수혜대상자를 위한 맞춤형 조성공간으로 구성되었는가? */
	private String eipp0102;

	/** EIPP0103, 수혜대상자를 위한 맞춤형 프로그램으로 구성되었는가? */
	private String eipp0103;
	
	/** EIPP0201, 주민설명회, 구조안전진단 실시 등을 실시하였는가? */
	private String eipp0201;
	
	/** EIPP0202, 수혜자선정의 적정성 */
	private String eipp0202;
	
	/** EIPP0203, 1인당 교육비 산정의 타당성(교육횟수, 교육인원) */
	private String eipp0203;
	
	/** EIPP0204, 교육커리큐럼의 품질(독창성 및 다양성) */
	private String eipp0204;
	
	/** EIPP0205, 강의의 적정성(강사진, 강사비 선정) */
	private String eipp0205;
	
	/** EIPP0206, 홍보의 효과성 */
	private String eipp0206;
	
	/** EIPP0207, 홍보(행사) 참여확대를 위한 노력 정도 */
	private String eipp0207;
	
	/** EIPP0208, 캠페인 노출효과(노출빈도 포함) */
	private String eipp0208;
	
	/** EIPP0209, 행사장소 선정의 적정성 */
	private String eipp0209;
	
	/** EIPP0301, 기술자문위원게 설계자문을 의뢰하였는가? */
	private String eipp0301;
	
	/** EIPP0401, 수종, 시설물 등에 대한 설계변경이 필요한가? */
	private String eipp0401;
	
	/** EIEM0101, 사업계획 대비 일정을 준수하고 있는가? */
	private String eiem0101;
	
	/** EIEM0102, 사업예산 계획 대비 집행실적은 적절한가? */
	private String eiem0102;
	
	/** EIEM0103, 지적 및 권고사항을 조치·완료하였는가? */
	private String eiem0103;
	
	/** EIEM0201, 유관기관·사업과의 연계협조체계를 구축하였는가? */
	private String eiem0201;
	
	/** EIEM0202, 사업별 통장관리를 하고 있는가? */
	private String eiem0202;
	
	/** EIEM0203. 예산절감노력을 하고 있는가? */
	private String eiem0203;
	
	/** EIEM0204, 지출·증빙 자료의 관리·비치하였는가? */
	private String eiem0204;
	
	/** EIEM0205, 민원, 안전사고예방을 위하여 적절한 조치를 하였는가? */
	private String eiem0205;
	
	/** EIEM0206, 사업단 요구자료에 대한 제출시기를 준수하였는가? */
	private String eiem0206;
	
	/**	EIEM0301, 사업단계별 홍보를 실시하였는가? */
	private String eiem0301;
	
	/** EIEM0302, 표지석, 수목표찰을 설치하였는가? */
	private String eiem0302;
	
	/** EIEM0303, 홍보문구 준수하였는가? */
	private String eiem0303;
	
	/** EIAR0101, 사업계획 대비 연간 성과목표를 달성하였는가? */
	private String eiar0101;
	
	/** EIAR0102, 조성공간의 접근성 및 상시 개방하고 있는가? */
	private String eiar0102;
	
	/** EIAR0201, 문제점 및 민원(지적)사항에 대한 적절한 대응을 하였는가? */
	private String eiar0201;
	
	/** GISR01, 적절ㆍ부적절ㆍ해당없음 */
	private String gisr01;

	/** GISR01_RM, 비고 */
	private String gisr01Rm;

	/** GISSI01, 적절ㆍ부적절ㆍ해당없음 */
	private String gissi01;

	/** GISSI01_RM, 비고 */
	private String gissi01Rm;

	/** GISC01, 적절ㆍ부적절ㆍ해당없음 */
	private String gisc01;

	/** GISC01_RM, 비고 */
	private String gisc01Rm;
	
	/** GISC02, 적절ㆍ부적절ㆍ해당없음 */
	private String gisc02;

	/** GISC02_RM, 비고 */
	private String gisc02Rm;
	
	/** GIPL01, 적절ㆍ부적절ㆍ해당없음 */
	private String gipl01;

	/** GIPL01_RM, 비고 */
	private String gipl01Rm;

	/**	GIPL02, 적절ㆍ부적절ㆍ해당없음 */
	private String gipl02;

	/** GIPL02_RM, 비고 */
	private String gipl02Rm;

	/** GIPL03, 적절ㆍ부적절ㆍ해당없음 */
	private String gipl03;

	/**GIPL03_RM, 비고 */
	private String gipl03Rm;

	/** GIMSL01, 적절ㆍ부적절ㆍ해당없음 */
	private String gimsl01;

	/**GIMSL01_RM, 비고 */
	private String gimsl01Rm;

	/** GIMSL02, 적절ㆍ부적절ㆍ해당없음 */
	private String gimsl02;

	/**GIMSL02_RM, 비고 */
	private String gimsl02Rm;

	/** GIMSL03, 적절ㆍ부적절ㆍ해당없음 */
	private String gimsl03;

	/**GIMSL03_RM, 비고 */
	private String gimsl03Rm;

	/** GIFC01, 적절ㆍ부적절ㆍ해당없음 */
	private String gifc01;

	/**GIFC01_RM, 비고 */
	private String gifc01Rm;

	/** GIFC02, 적절ㆍ부적절ㆍ해당없음 */
	private String gifc02;

	/**GIFC02_RM, 비고 */
	private String gifc02Rm;

	/** GIEE01, 적절ㆍ부적절ㆍ해당없음 */
	private String giee01;

	/**GIEE01_RM, 비고 */
	private String giee01Rm;

	/** GIEE02, 적절ㆍ부적절ㆍ해당없음 */
	private String giee02;

	/**GIEE02_RM, 비고 */
	private String giee02Rm;

	/** GIEE03, 적절ㆍ부적절ㆍ해당없음 */
	private String giee03;

	/**GIEE03_RM, 비고 */
	private String giee03Rm;

	/** GIEE04, 적절ㆍ부적절ㆍ해당없음 */
	private String giee04;

	/**GIEE04_RM, 비고 */
	private String giee04Rm;

	/** GIEE05, 적절ㆍ부적절ㆍ해당없음 */
	private String giee05;

	/**GIEE05_RM, 비고 */
	private String giee05Rm;

	/** GIEE06, 적절ㆍ부적절ㆍ해당없음 */
	private String giee06;

	/**GIEE06_RM, 비고 */
	private String giee06Rm;

	/** GIPF01, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf01;

	/**GIPF01_RM, 비고 */
	private String gipf01Rm;

	/** GIPF02, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf02;

	/**GIPF02_RM, 비고 */
	private String gipf02Rm;

	/** GIPF03, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf03;

	/**GIPF03_RM, 비고 */
	private String gipf03Rm;

	/** GIPF04, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf04;

	/**GIPF04_RM, 비고 */
	private String gipf04Rm;

	/** GIPF05, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf05;

	/**GIPF05_RM, 비고 */
	private String gipf05Rm;

	/** GIPF06, 적절ㆍ부적절ㆍ해당없음 */
	private String gipf06;

	/**GIPF06_RM, 비고 */
	private String gipf06Rm;

	/** GIEMS01, 적절ㆍ부적절ㆍ해당없음 */
	private String giems01;

	/**GIEMS01_RM, 비고 */
	private String giems01Rm;

	/** GIEMS02, 적절ㆍ부적절ㆍ해당없음 */
	private String giems02;

	/**GIEMS02_RM, 비고 */
	private String giems02Rm;

	/** GIEMS03, 적절ㆍ부적절ㆍ해당없음 */
	private String giems03;

	/**GIEMS03_RM, 비고 */
	private String giems03Rm;

	/** GIEMS04, 적절ㆍ부적절ㆍ해당없음 */
	private String giems04;

	/**GIEMS04_RM, 비고 */
	private String giems04Rm;

	/** GIEMS05, 적절ㆍ부적절ㆍ해당없음 */
	private String giems05;

	/**GIEMS05_RM, 비고 */
	private String giems05Rm;

	/** GIIS01, 적절ㆍ부적절ㆍ해당없음 */
	private String giis01;

	/**GIIS01_RM, 비고 */
	private String giis01Rm;

	/** GIETC_T01, 기타 질의 1 */
	private String gietcT01;

	/** GIETC_T02, 기타 질의 2 */
	private String gietcT02;

	/** GIETC_T03, 기타 질의 3 */
	private String gietcT03;

	/** GIETC_T04, 기타 질의 4 */
	private String gietcT04;

	/** GIETC01, 적절ㆍ부적절ㆍ해당없음 */
	private String gietc01;
	
	/** GIETC02, 적절ㆍ부적절ㆍ해당없음 */
	private String gietc02;
	
	/** GIETC03, 적절ㆍ부적절ㆍ해당없음 */
	private String gietc03;
	
	/** GIETC04, 적절ㆍ부적절ㆍ해당없음 */
	private String gietc04;

	/**GIETC_T01_RM, 비고 */
	private String gietcT01Rm;

	/**GIETC_T02_RM, 비고 */
	private String gietcT02Rm;

	/**GIETC_T03_RM, 비고 */
	private String gietcT03Rm;

	/**GIETC_T04_RM, 비고 */
	private String gietcT04Rm;

	/** COMP_AT, 완료여부 */
	private String compAt;
	
	/** FRST_REGIST_PNTTM, 최초등록시점 */
	private String frstRegistPnttm;
	
	/** FRST_REGISTER_ID, 최초등록자ID */
	private String frstRegisterId;
	
	/** LAST_UPDUSR_PNTTM, 최종수정시점 */
	private String lastUpdusrPnttm;
	
	/** LAST_UPDUSR_ID, 최종수정자ID */
	private String lastUpdusrId;

	/** LAST_UPDUSR_ID, 코드번호 */
	private String codeId;

	/** LAST_UPDUSR_ID, 코드이름 */
	private String codeNm;

	/** LAST_UPDUSR_ID, 코드명d */
	private String codeDc;

	/** AREA 지역 */
	private String area;

	/** INSTT_NM, 시설명 */
	private String insttNm;

	/** RPRSNTV, 대표자 */
	private String rprsntv;

	/** SIDOGUNGU, 시도군구 */
	private String sidogungu;
	
	/** 임시 지도점검표첨부파일ID */
	private String gctt_atch_file_id;

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getAtch_file_id(){
		return atch_file_id;
	}

	public void setAtch_file_id(String atch_file_id){
		this.atch_file_id = atch_file_id;
	}

	public String getCtt_atch_file_id(){
		return ctt_atch_file_id;
	}

	public void setCtt_atch_file_id(String ctt_atch_file_id){
		this.ctt_atch_file_id = ctt_atch_file_id;
	}

	public String getEipp0101() {
		return eipp0101;
	}

	public void setEipp0101(String eipp0101) {
		this.eipp0101 = eipp0101;
	}

	public String getEipp0102() {
		return eipp0102;
	}

	public void setEipp0102(String eipp0102) {
		this.eipp0102 = eipp0102;
	}

	public String getEipp0103(){
		return eipp0103;
	}

	public void setEipp0103(String eipp0103){
		this.eipp0103 = eipp0103;
	}

	public String getEipp0201() {
		return eipp0201;
	}

	public void setEipp0201(String eipp0201) {
		this.eipp0201 = eipp0201;
	}

	public String getEipp0202() {
		return eipp0202;
	}

	public void setEipp0202(String eipp0202) {
		this.eipp0202 = eipp0202;
	}

	public String getEipp0203() {
		return eipp0203;
	}

	public void setEipp0203(String eipp0203) {
		this.eipp0203 = eipp0203;
	}

	public String getEipp0204() {
		return eipp0204;
	}

	public void setEipp0204(String eipp0204) {
		this.eipp0204 = eipp0204;
	}

	public String getEipp0205() {
		return eipp0205;
	}

	public void setEipp0205(String eipp0205) {
		this.eipp0205 = eipp0205;
	}

	public String getEipp0206() {
		return eipp0206;
	}

	public void setEipp0206(String eipp0206) {
		this.eipp0206 = eipp0206;
	}

	public String getEipp0207() {
		return eipp0207;
	}

	public void setEipp0207(String eipp0207) {
		this.eipp0207 = eipp0207;
	}

	public String getEipp0208() {
		return eipp0208;
	}

	public void setEipp0208(String eipp0208) {
		this.eipp0208 = eipp0208;
	}

	public String getEipp0209() {
		return eipp0209;
	}

	public void setEipp0209(String eipp0209) {
		this.eipp0209 = eipp0209;
	}

	public String getEipp0301() {
		return eipp0301;
	}

	public void setEipp0301(String eipp0301) {
		this.eipp0301 = eipp0301;
	}

	public String getEipp0401() {
		return eipp0401;
	}

	public void setEipp0401(String eipp0401) {
		this.eipp0401 = eipp0401;
	}

	public String getEiem0101() {
		return eiem0101;
	}

	public void setEiem0101(String eiem0101) {
		this.eiem0101 = eiem0101;
	}

	public String getEiem0102() {
		return eiem0102;
	}

	public void setEiem0102(String eiem0102) {
		this.eiem0102 = eiem0102;
	}

	public String getEiem0103() {
		return eiem0103;
	}

	public void setEiem0103(String eiem0103) {
		this.eiem0103 = eiem0103;
	}

	public String getEiem0201() {
		return eiem0201;
	}

	public void setEiem0201(String eiem0201) {
		this.eiem0201 = eiem0201;
	}

	public String getEiem0202() {
		return eiem0202;
	}

	public void setEiem0202(String eiem0202) {
		this.eiem0202 = eiem0202;
	}

	public String getEiem0203() {
		return eiem0203;
	}

	public void setEiem0203(String eiem0203) {
		this.eiem0203 = eiem0203;
	}

	public String getEiem0204() {
		return eiem0204;
	}

	public void setEiem0204(String eiem0204) {
		this.eiem0204 = eiem0204;
	}

	public String getEiem0205() {
		return eiem0205;
	}

	public void setEiem0205(String eiem0205) {
		this.eiem0205 = eiem0205;
	}

	public String getEiem0206() {
		return eiem0206;
	}

	public void setEiem0206(String eiem0206) {
		this.eiem0206 = eiem0206;
	}

	public String getEiem0301() {
		return eiem0301;
	}

	public void setEiem0301(String eiem0301) {
		this.eiem0301 = eiem0301;
	}

	public String getEiem0302() {
		return eiem0302;
	}

	public void setEiem0302(String eiem0302) {
		this.eiem0302 = eiem0302;
	}

	public String getEiem0303() {
		return eiem0303;
	}

	public void setEiem0303(String eiem0303) {
		this.eiem0303 = eiem0303;
	}

	public String getEiar0101() {
		return eiar0101;
	}

	public void setEiar0101(String eiar0101) {
		this.eiar0101 = eiar0101;
	}

	public String getEiar0102() {
		return eiar0102;
	}

	public void setEiar0102(String eiar0102) {
		this.eiar0102 = eiar0102;
	}

	public String getEiar0201() {
		return eiar0201;
	}

	public void setEiar0201(String eiar0201) {
		this.eiar0201 = eiar0201;
	}

	public String getGisr01() {
		return gisr01;
	}

	public void setGisr01(String gisr01) {
		this.gisr01 = gisr01;
	}

	public String getGissi01() {
		return gissi01;
	}

	public void setGissi01(String gissi01) {
		this.gissi01 = gissi01;
	}

	public String getGisc01() {
		return gisc01;
	}

	public void setGisc01(String gisc01) {
		this.gisc01 = gisc01;
	}

	public String getGisc02() {
		return gisc02;
	}

	public void setGisc02(String gisc02) {
		this.gisc02 = gisc02;
	}

	public String getGipl01() {
		return gipl01;
	}

	public void setGipl01(String gipl01) {
		this.gipl01 = gipl01;
	}

	public String getGipl02() {
		return gipl02;
	}

	public void setGipl02(String gipl02) {
		this.gipl02 = gipl02;
	}

	public String getGipl03() {
		return gipl03;
	}

	public void setGipl03(String gipl03) {
		this.gipl03 = gipl03;
	}

	public String getGimsl01() {
		return gimsl01;
	}

	public void setGimsl01(String gimsl01) {
		this.gimsl01 = gimsl01;
	}

	public String getGimsl02() {
		return gimsl02;
	}

	public void setGimsl02(String gimsl02) {
		this.gimsl02 = gimsl02;
	}

	public String getGimsl03() {
		return gimsl03;
	}

	public void setGimsl03(String gimsl03) {
		this.gimsl03 = gimsl03;
	}

	public String getGifc01() {
		return gifc01;
	}

	public void setGifc01(String gifc01) {
		this.gifc01 = gifc01;
	}

	public String getGifc02() {
		return gifc02;
	}

	public void setGifc02(String gifc02) {
		this.gifc02 = gifc02;
	}

	public String getGiee01() {
		return giee01;
	}

	public void setGiee01(String giee01) {
		this.giee01 = giee01;
	}

	public String getGiee02() {
		return giee02;
	}

	public void setGiee02(String giee02) {
		this.giee02 = giee02;
	}

	public String getGiee03() {
		return giee03;
	}

	public void setGiee03(String giee03) {
		this.giee03 = giee03;
	}

	public String getGiee04() {
		return giee04;
	}

	public void setGiee04(String giee04) {
		this.giee04 = giee04;
	}

	public String getGiee05() {
		return giee05;
	}

	public void setGiee05(String giee05) {
		this.giee05 = giee05;
	}

	public String getGiee06() {
		return giee06;
	}

	public void setGiee06(String giee06) {
		this.giee06 = giee06;
	}

	public String getGipf01() {
		return gipf01;
	}

	public void setGipf01(String gipf01) {
		this.gipf01 = gipf01;
	}

	public String getGipf02() {
		return gipf02;
	}

	public void setGipf02(String gipf02) {
		this.gipf02 = gipf02;
	}

	public String getGipf03() {
		return gipf03;
	}

	public void setGipf03(String gipf03) {
		this.gipf03 = gipf03;
	}

	public String getGipf04() {
		return gipf04;
	}

	public void setGipf04(String gipf04) {
		this.gipf04 = gipf04;
	}

	public String getGipf05() {
		return gipf05;
	}

	public void setGipf05(String gipf05) {
		this.gipf05 = gipf05;
	}

	public String getGiems01() {
		return giems01;
	}

	public void setGiems01(String giems01) {
		this.giems01 = giems01;
	}

	public String getGiems02() {
		return giems02;
	}

	public void setGiems02(String giems02) {
		this.giems02 = giems02;
	}

	public String getGiems03() {
		return giems03;
	}

	public void setGiems03(String giems03) {
		this.giems03 = giems03;
	}

	public String getGiems04() {
		return giems04;
	}

	public void setGiems04(String giems04) {
		this.giems04 = giems04;
	}

	public String getGiems05() {
		return giems05;
	}

	public void setGiems05(String giems05) {
		this.giems05 = giems05;
	}

	public String getGiis01() {
		return giis01;
	}

	public void setGiis01(String giis01) {
		this.giis01 = giis01;
	}

	public String getGietcT01() {
		return gietcT01;
	}

	public void setGietcT01(String gietcT01) {
		this.gietcT01 = gietcT01;
	}

	public String getGietcT02() {
		return gietcT02;
	}

	public void setGietcT02(String gietcT02) {
		this.gietcT02 = gietcT02;
	}

	public String getGietcT03() {
		return gietcT03;
	}

	public void setGietcT03(String gietcT03) {
		this.gietcT03 = gietcT03;
	}

	public String getGietcT04() {
		return gietcT04;
	}

	public void setGietcT04(String gietcT04) {
		this.gietcT04 = gietcT04;
	}

	public String getGietc01() {
		return gietc01;
	}

	public void setGietc01(String gietc01) {
		this.gietc01 = gietc01;
	}

	public String getGietc02() {
		return gietc02;
	}

	public void setGietc02(String gietc02) {
		this.gietc02 = gietc02;
	}

	public String getGietc03() {
		return gietc03;
	}

	public void setGietc03(String gietc03) {
		this.gietc03 = gietc03;
	}

	public String getGietc04() {
		return gietc04;
	}

	public void setGietc04(String gietc04) {
		this.gietc04 = gietc04;
	}

	public String getCompAt() {
		return compAt;
	}

	public void setCompAt(String compAt) {
		this.compAt = compAt;
	}

	public String getFrstRegistPnttm() {
		return frstRegistPnttm;
	}

	public void setFrstRegistPnttm(String frstRegistPnttm) {
		this.frstRegistPnttm = frstRegistPnttm;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public String getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(String lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public String getGisr01Rm(){
		return gisr01Rm;
	}

	public void setGisr01Rm(String gisr01Rm){
		this.gisr01Rm = gisr01Rm;
	}

	public String getGissi01Rm(){
		return gissi01Rm;
	}

	public void setGissi01Rm(String gissi01Rm){
		this.gissi01Rm = gissi01Rm;
	}

	public String getGisc01Rm(){
		return gisc01Rm;
	}

	public void setGisc01Rm(String gisc01Rm){
		this.gisc01Rm = gisc01Rm;
	}

	public String getGisc02Rm(){
		return gisc02Rm;
	}

	public void setGisc02Rm(String gisc02Rm){
		this.gisc02Rm = gisc02Rm;
	}

	public String getGipl01Rm(){
		return gipl01Rm;
	}

	public void setGipl01Rm(String gipl01Rm){
		this.gipl01Rm = gipl01Rm;
	}

	public String getGipl02Rm(){
		return gipl02Rm;
	}

	public void setGipl02Rm(String gipl02Rm){
		this.gipl02Rm = gipl02Rm;
	}

	public String getGipl03Rm(){
		return gipl03Rm;
	}

	public void setGipl03Rm(String gipl03Rm){
		this.gipl03Rm = gipl03Rm;
	}

	public String getGimsl01Rm(){
		return gimsl01Rm;
	}

	public void setGimsl01Rm(String gimsl01Rm){
		this.gimsl01Rm = gimsl01Rm;
	}

	public String getGimsl02Rm(){
		return gimsl02Rm;
	}

	public void setGimsl02Rm(String gimsl02Rm){
		this.gimsl02Rm = gimsl02Rm;
	}

	public String getGimsl03Rm(){
		return gimsl03Rm;
	}

	public void setGimsl03Rm(String gimsl03Rm){
		this.gimsl03Rm = gimsl03Rm;
	}

	public String getGifc01Rm(){
		return gifc01Rm;
	}

	public void setGifc01Rm(String gifc01Rm){
		this.gifc01Rm = gifc01Rm;
	}

	public String getGifc02Rm(){
		return gifc02Rm;
	}

	public void setGifc02Rm(String gifc02Rm){
		this.gifc02Rm = gifc02Rm;
	}

	public String getGiee01Rm(){
		return giee01Rm;
	}

	public void setGiee01Rm(String giee01Rm){
		this.giee01Rm = giee01Rm;
	}

	public String getGiee02Rm(){
		return giee02Rm;
	}

	public void setGiee02Rm(String giee02Rm){
		this.giee02Rm = giee02Rm;
	}

	public String getGiee03Rm(){
		return giee03Rm;
	}

	public void setGiee03Rm(String giee03Rm){
		this.giee03Rm = giee03Rm;
	}

	public String getGiee04Rm(){
		return giee04Rm;
	}

	public void setGiee04Rm(String giee04Rm){
		this.giee04Rm = giee04Rm;
	}

	public String getGiee05Rm(){
		return giee05Rm;
	}

	public void setGiee05Rm(String giee05Rm){
		this.giee05Rm = giee05Rm;
	}

	public String getGiee06Rm(){
		return giee06Rm;
	}

	public void setGiee06Rm(String giee06Rm){
		this.giee06Rm = giee06Rm;
	}

	public String getGipf01Rm(){
		return gipf01Rm;
	}

	public void setGipf01Rm(String gipf01Rm){
		this.gipf01Rm = gipf01Rm;
	}

	public String getGipf02Rm(){
		return gipf02Rm;
	}

	public void setGipf02Rm(String gipf02Rm){
		this.gipf02Rm = gipf02Rm;
	}

	public String getGipf03Rm(){
		return gipf03Rm;
	}

	public void setGipf03Rm(String gipf03Rm){
		this.gipf03Rm = gipf03Rm;
	}

	public String getGipf04Rm(){
		return gipf04Rm;
	}

	public void setGipf04Rm(String gipf04Rm){
		this.gipf04Rm = gipf04Rm;
	}

	public String getGipf05Rm(){
		return gipf05Rm;
	}

	public void setGipf05Rm(String gipf05Rm){
		this.gipf05Rm = gipf05Rm;
	}

	public String getGipf06(){
		return gipf06;
	}

	public void setGipf06(String gipf06){
		this.gipf06 = gipf06;
	}

	public String getGipf06Rm(){
		return gipf06Rm;
	}

	public void setGipf06Rm(String gipf06Rm){
		this.gipf06Rm = gipf06Rm;
	}

	public String getGiems01Rm(){
		return giems01Rm;
	}

	public void setGiems01Rm(String giems01Rm){
		this.giems01Rm = giems01Rm;
	}

	public String getGiems02Rm(){
		return giems02Rm;
	}

	public void setGiems02Rm(String giems02Rm){
		this.giems02Rm = giems02Rm;
	}

	public String getGiems03Rm(){
		return giems03Rm;
	}

	public void setGiems03Rm(String giems03Rm){
		this.giems03Rm = giems03Rm;
	}

	public String getGiems04Rm(){
		return giems04Rm;
	}

	public void setGiems04Rm(String giems04Rm){
		this.giems04Rm = giems04Rm;
	}

	public String getGiems05Rm(){
		return giems05Rm;
	}

	public void setGiems05Rm(String giems05Rm){
		this.giems05Rm = giems05Rm;
	}

	public String getGiis01Rm(){
		return giis01Rm;
	}

	public void setGiis01Rm(String giis01Rm){
		this.giis01Rm = giis01Rm;
	}

	public String getGietcT01Rm(){
		return gietcT01Rm;
	}

	public void setGietcT01Rm(String gietcT01Rm){
		this.gietcT01Rm = gietcT01Rm;
	}

	public String getGietcT02Rm(){
		return gietcT02Rm;
	}

	public void setGietcT02Rm(String gietcT02Rm){
		this.gietcT02Rm = gietcT02Rm;
	}

	public String getGietcT03Rm(){
		return gietcT03Rm;
	}

	public void setGietcT03Rm(String gietcT03Rm){
		this.gietcT03Rm = gietcT03Rm;
	}

	public String getGietcT04Rm(){
		return gietcT04Rm;
	}

	public void setGietcT04Rm(String gietcT04Rm){
		this.gietcT04Rm = gietcT04Rm;
	}

	public String getCodeId(){
		return codeId;
	}

	public void setCodeId(String codeId){
		this.codeId = codeId;
	}

	public String getCodeNm(){
		return codeNm;
	}

	public void setCodeNm(String codeNm){
		this.codeNm = codeNm;
	}

	public String getCodeDc(){
		return codeDc;
	}

	public void setCodeDc(String codeDc){
		this.codeDc = codeDc;
	}

	public String getArea(){
		return area;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getInsttNm(){
		return insttNm;
	}

	public void setInsttNm(String insttNm){
		this.insttNm = insttNm;
	}

	public String getRprsntv(){
		return rprsntv;
	}

	public void setRprsntv(String rprsntv){
		this.rprsntv = rprsntv;
	}

	public String getSidogungu(){
		return sidogungu;
	}

	public void setSidogungu(String sidogungu){
		this.sidogungu = sidogungu;
	}

	public String getGctt_atch_file_id() {
		return gctt_atch_file_id;
	}

	public void setGctt_atch_file_id(String gctt_atch_file_id) {
		this.gctt_atch_file_id = gctt_atch_file_id;
	}

}
