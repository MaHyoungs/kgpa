package egovframework.com.gfund.biz.chcklist.service;

public interface EgovChckListService {
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param chckListVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertChckList(ChckListVo chckListVo) throws Exception ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param chckListVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateChckList(ChckListVo chckListVo) throws Exception ;
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 자료 한건을 삭제한다.
     * @param chckListVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deleteChckList(ChckListVo chckListVo) throws Exception ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param chckListVo 자료 기본정보
     * @return chckListVo 자료 상제정보
     * @throws Exception
     */
    public ChckListVo selectChckList(ChckListVo chckListVo) throws Exception ;

	/**
	 * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 사업선택에 대한 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param chckListVo 자료 기본정보
	 * @return chckListVo 자료 상제정보
	 * @throws Exception
	 */
	public ChckListVo selectBusinessInfo(ChckListVo chckListVo) throws Exception ;

	/**
	 * 녹색자금통합관리시스템 > 사업관리 > 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트에서 특정 자료의 값 존재 유무 판단
	 * @param chckListVo 자료 기본정보
	 * @return chckListVo 자료 상제정보
	 * @throws Exception
	 */
	public int selectChckListDupl(ChckListVo chckListVo) throws Exception;

	/**
	 * 사업선택 > 통합검색(사업선택 > 확인버튼)
	 * @param chckListVo
	 * @return
	 * @throws Exception
	 */
	public ChckListVo selectDoChckList(ChckListVo chckListVo) throws Exception;

}
