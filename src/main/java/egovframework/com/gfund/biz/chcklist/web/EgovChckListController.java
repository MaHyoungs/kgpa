/**
 * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트 관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 마형민
 * @since 2014.12.21
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일       수정자               수정내용
 *  -------      --------    ---------------------------
 *   2014.12.21  마형민               최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.chcklist.web;

import java.util.Map;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.chcklist.service.ChckListVo;
import egovframework.com.gfund.biz.chcklist.service.EgovChckListService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class EgovChckListController {

	@Resource(name = "EgovChckListService")
	private EgovChckListService egovChckListService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "loginService")
	private EgovLoginService loginService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	Logger log = LogManager.getLogger(this.getClass());
	
	/**
	 * 사업관리 > 사업현황 > 지도점검 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return mng/gfund/biz/businessmng/EgovChckListModify
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/businessmng/EgovChckListUpdtView.do")
	public String EgovChckListView(@ModelAttribute("chckListVo") ChckListVo chckListVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())){

		}
		
		String bizId = (String) commandMap.get("biz_id");
		
		chckListVo.setBizId(bizId);
		model.addAttribute("loginId", loginVO.getId());
		model.addAttribute("selectBusinessInfo", egovChckListService.selectBusinessInfo(chckListVo));
		chckListVo = egovChckListService.selectChckList(chckListVo);
		model.addAttribute("selectChckList", chckListVo);
		model.addAttribute("bizId", bizId);
		
		return "mng/gfund/biz/businessmng/EgovChckListForm";
	}


	/**
	 * 지도점검체크리스트 관리 > 사업선택 > 지도점검 > 정보등록
	 * @param chckListVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/businessmng/EgovChckListSelectUpdt.do")
	public String EgovChckListInsert(@ModelAttribute("chckListVo") ChckListVo chckListVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())){

		}
		
		String forwardUrl = "forward:/mng/gfund/biz/businessmng/EgovChckListUpdtView.do";
		int totcnt = egovChckListService.selectChckListDupl(chckListVo);
		if(totcnt == 0 ){
			chckListVo.setCompAt("N");
			egovChckListService.insertChckList(chckListVo);
			mngchckImgUpload(chckListVo);
			mngchckFileUpload(chckListVo);
		} else{
			egovChckListService.updateChckList(chckListVo);
			mngchckImgUpload(chckListVo);
			mngchckFileUpload(chckListVo);
		}
				
		return forwardUrl;
	}


	/**
	 * 파일 업로드
	 * @param chckListVo
	 * @throws Exception
	 */
	public void mngchckFileUpload(ChckListVo chckListVo) throws Exception {
		if (!("").equals(chckListVo.getCtt_atch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(chckListVo.getCtt_atch_file_id());
			if(!("").equals(chckListVo.getGctt_atch_file_id())) {
				fvo.setAtchFileId(chckListVo.getGctt_atch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}


	/**
	 * 파일 이미지 업로드
	 * @param chckListVo
	 * @throws Exception
	 */
	public void mngchckImgUpload(ChckListVo chckListVo) throws Exception {
		if (!("").equals(chckListVo.getAtch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(chckListVo.getAtch_file_id());
			if(!("").equals(chckListVo.getGatch_file_id())) {
				fvo.setAtchFileId(chckListVo.getGatch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}
	
}
