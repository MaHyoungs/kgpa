/**
 * 녹색자금통합관리시스템 > 사업관리 > 지도점검체크리스트 관리에 관한 모바일용 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 마형민
 * @since 2014.12.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일       수정자              수정내용
 * ----------   --------    ---------------------------
 * 2014.12.12    마형민              최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.chcklist.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.chcklist.service.ChckListVo;
import egovframework.com.gfund.biz.chcklist.service.EgovChckListService;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@Controller
public class EgovChckListMobileController {

	@Resource(name = "EgovChckListService")
	private EgovChckListService egovChckListService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	@Resource(name = "loginService")
	private EgovLoginService loginService;

	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	Logger log = LogManager.getLogger(this.getClass());

	/**
	 * 지도점검체크리스트 관리 > 로그인페이지
	 * @param loginVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/uat/uia/EgovmChckLoginUsr.do")
	public String EgovmChckLoginUsr(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		if (loginVO != null )
			EgovSessionCookieUtil.setSessionAttribute(request, "returnUrl", loginVO.getUrl());

		return "gfund/biz/mbusinmng/EgovmChckLoginUsr";
	}


	/**
	 * 지도점검체크리스트 관리 > 로그인 처리
	 * @param loginVO
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/uat/uia/EgovChckListMoibleLogin.do")
	public String EgovChckListMoibleLogin(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, ModelMap model) throws Exception{

		String loginUrl = "/gfund/biz/mbusinmng/EgovmChckLoginUsr";
		String forwardUrl = "forward:/gfund/biz/mbusinmng/EgovChckListBusinSelect.do";

		boolean loginSuccess = false;
		LoginVO resultVO = null;
		String userSeCode = "";
		String comfmAt = "";
		if (loginVO.getId() != null && loginVO.getPassword() != null){
			// 1. 일반 로그인 처리
			UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
			if(userVO != null) {
				comfmAt = userVO.getConfmAt();
				resultVO = loginService.actionNkrefoLogin(loginVO);
				if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {	//로그인 성공
					loginSuccess = true;
					userSeCode = resultVO.getUserSe();
				}
			}
		}
		if ("N".equals(comfmAt)) {
			model.addAttribute("frdbmessage", "관리자 승인 후 로그인 할 수 있습니다.");
			return loginUrl;
		}
		// 아이디, 패스워드 및 권한 체크를 한다.
		if ( loginSuccess ) {
			if ( ("06".equals(userSeCode) || "07".equals(userSeCode) || "99".equals(userSeCode)) ) {
				// 산림자료DB 전용 세션 저장
				EgovSessionCookieUtil.setSessionAttribute(request, "isChckcerLogin", "Y");
				request.getSession().setAttribute("loginID", loginVO.getId());
				request.getSession().setAttribute("isChckcerLogin", "Y");
				return forwardUrl;
			} else {
				model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.select"));
				return loginUrl;
			}
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.login"));
			return loginUrl;
		}
	}


	/**
	 * 지도점검체크리스트 관리 > 로그인처리 > 사업선택
	 * @param chckListVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovChckListBusinSelect.do")
	public String EgovChckListBusinSelect(@ModelAttribute("searchVO") ChckListVo chckListVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if ( "Y".equals( EgovSessionCookieUtil.getSessionAttribute(request, "isChckcerLogin") ) == false ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/gfund/biz/businmng/EgovChckListMoibleLogin.do";
		}
		return "gfund/biz/mbusinmng/EgovChckListBusinSelect";
	}


	/**
	 * 지도점검체크리스트 관리 > 사업선택 > 지도점검
	 * @param chckListVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovChckListGuide.do")
	public String EgovChckListGuide(@ModelAttribute("chckListVo") ChckListVo chckListVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if ( "Y".equals( EgovSessionCookieUtil.getSessionAttribute(request, "isChckcerLogin") ) == false ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/gfund/biz/businmng/EgovChckListMoibleLogin.do";
		}
			String bizId = request.getParameter("bizId");
			chckListVo.setBizId(bizId);
			model.addAttribute("loginId", request.getSession().getAttribute("loginID"));
			model.addAttribute("selectBusinessInfo", egovChckListService.selectBusinessInfo(chckListVo));
			chckListVo = egovChckListService.selectChckList(chckListVo);
			model.addAttribute("selectChckList", chckListVo);
			model.addAttribute("bizId", bizId);

		return "gfund/biz/mbusinmng/EgovChckListGuide";
	}


	/**
	 * 지도점검체크리스트 관리 > 사업선택 > 지도점검 > 정보등록
	 * @param chckListVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovChckListInfoInsert.do")
	public String EgovChckListInfoInsert(@ModelAttribute("chckListVo") ChckListVo chckListVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if ( "Y".equals( EgovSessionCookieUtil.getSessionAttribute(request, "isChckcerLogin") ) == false ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/gfund/biz/businmng/EgovChckListMoibleLogin.do";
		}

		String forwardUrl = "forward:/gfund/biz/mbusinmng/EgovChckListGuide.do";
		int totcnt = egovChckListService.selectChckListDupl(chckListVo);
		if(totcnt == 0 ){
			egovChckListService.insertChckList(chckListVo);
			chckImgUpload(chckListVo);
			chckFileUpload(chckListVo);
		} else{
			egovChckListService.updateChckList(chckListVo);
			chckImgUpload(chckListVo);
			chckFileUpload(chckListVo);
		}

		if(chckListVo.getCompAt().equals('Y')) {
			return "/gfund/biz/mbusinmng/EgovChckListGuide.do";
		}

		return forwardUrl;
	}


	/**
	 * 파일 업로드
	 * @param chckListVo
	 * @throws Exception
	 */
	public void chckFileUpload(ChckListVo chckListVo) throws Exception {
		if (!("").equals(chckListVo.getCtt_atch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(chckListVo.getCtt_atch_file_id());
			if(!("").equals(chckListVo.getGatch_file_id())) {
				fvo.setAtchFileId(chckListVo.getCtt_atch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}


	/**
	 * 파일 이미지 업로드
	 * @param chckListVo
	 * @throws Exception
	 */
	public void chckImgUpload(ChckListVo chckListVo) throws Exception {
		if (!("").equals(chckListVo.getAtch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(chckListVo.getAtch_file_id());
			if(!("").equals(chckListVo.getGatch_file_id())) {
				fvo.setAtchFileId(chckListVo.getGatch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}

	/**
	 * 지도점검체크리스트 관리 > 로그아웃 처리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/gfund/uat/uia/EgovmChckLogout.do")
	public String EgovmChckLogout(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		String forwardUrl = "forward:/gfund/uat/uia//EgovmChckLogoutMain.do";
		if ( "Y".equals( EgovSessionCookieUtil.getSessionAttribute(request, "isChckcerLogin") ) == false ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/gfund/uat/uia/EgovmChckLoginUsr.do";
		}
		// 지도점검 세션 삭제
		EgovSessionCookieUtil.removeSessionAttribute(request, "isChckcerLogin");
		request.getSession().removeAttribute("isChckcerLogin");
		request.getSession().removeAttribute("loginID");

		return forwardUrl;
	}


	/**
	 * 지도점검체크리스트 관리 > 로그아웃 >로그인페이지
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/gfund/uat/uia/EgovmChckLogoutMain.do")
	public String EgovmChckLogoutMain(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		String returnUrl = (String)EgovSessionCookieUtil.getSessionAttribute(request, "returnUrl");
		String main_page = "forward:/gfund/uat/uia/EgovmChckLoginUsr.do";
		String modelUrl = main_page;
		if(returnUrl == null || "".equals(returnUrl)){
			if (main_page.startsWith("/")) {
				modelUrl =  "redirect:" + main_page;
			}
		}else{
			modelUrl = "redirect:" + returnUrl;
		}
		EgovSessionCookieUtil.removeSessionAttribute(request, "returnUrl");

		return modelUrl;
	}


	/**
	 * 지도점검체크리스트 관리 > 지도점검 > 사업선택 > 확인버튼 Ajax
	 * @param chckListVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovbizzNmSearchAjax.do")
	public ModelAndView fnChckSearchBizNm(@ModelAttribute("chckListVo") ChckListVo chckListVo) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", egovChckListService.selectDoChckList(chckListVo));
		return mav;
	}


	/**
	 * 지도점검체크리스트 관리 > 지도점검 > 사업선택 > 기관명선택 Ajax
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovOrganSearchAjax.do")
	public ModelAndView EgovOrganSearchAjax(@ModelAttribute("basicInformationVo") BasicInformationVo basicInformationVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.selectBusinessBasicInformation(basicInformationVo));
		return mav;
	}


	/**
	 * 지도점검체크리스트 관리 > 지도점검 > 사업선택 > 시설검색 Ajax
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/mbusinmng/EgovbizNmSearchAjax.do")
	public ModelAndView EgovinsttNmSearchAjax(@ModelAttribute("basicInformationVo") BasicInformationVo basicInformationVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		// 기준년도 조회
		GreenFundYearVo defaultyear = greenFundYearService.selectGreenFundYearDefault();
		String year = defaultyear.getYear();
		basicInformationVo.setYear(year);
		mav.addObject("rs", basicInformationService.selectmBusinessBasicInformations(basicInformationVo));
		return mav;
	}

}
