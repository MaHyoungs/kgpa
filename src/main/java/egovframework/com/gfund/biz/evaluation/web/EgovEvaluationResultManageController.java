/**
 * 녹색자금통합관리시스템 > 결과산출 > 평가결과에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.evaluation.web;

import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
public class EgovEvaluationResultManageController {
	
	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	/**
	 * 결과산출 > 결과산출 > 평가결과 수정 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovEvaluationResultUpdtView.do", "/gfund/biz/productmng/EgovEvaluationResultUpdtView.do"})
	public String EgovEvaluationResultRegist(@ModelAttribute("searchVO") BasicInformationVo basicInformationVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		basicInformationVo.setBiz_id(bizId);
		BasicInformationVo result = basicInformationService.selectBusinessBasicInformation(basicInformationVo);
		
		model.addAttribute("basicInformationVo", result);
		
		request.getSession().setAttribute("sessionVO", basicInformationVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovEvaluationResultForm";
		}else{
			return "gfund/biz/productmng/EgovEvaluationResultForm.tiles";
		}
	}
	
	/**
	 * 결과산출 > 결과산출 > 평가결과 수정 후 이전화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovEvaluationResultSelectUpdt.do", "/gfund/biz/productmng/EgovEvaluationResultSelectUpdt.do"})
	public String EgovEvaluationResultSelectUpdt(@ModelAttribute("searchVO") BasicInformationVo basicInformationVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			model.addAttribute("basicInformationVo", basicInformationService.selectBusinessBasicInformation(selectVo));
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/productmng/EgovEvaluationResultForm";
			}else{
				return "gfund/biz/productmng/EgovEvaluationResultForm.tiles";
			}
		}
		
		if ( basicInformationService.updateBasicInformationForEvlRst(basicInformationVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovEvaluationResultUpdtView.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovEvaluationResultUpdtView.do";
			}
		}
		
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovEvaluationResultUpdtView.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovEvaluationResultUpdtView.do";
		}
	}
	
	/**
	 * 최종평가결과 상태 변경 Ajax
	 * @param bsifVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/productmng/updateBasicInformationEvlRst.do")
	public ModelAndView updateBasicInformationEvlRst(@ModelAttribute("bsifVo") BasicInformationVo bsifVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.updateBasicInformationForEvlRst(bsifVo));
		return mav;
	}
	
}
