/**
 * 녹색자금통합관리시스템 > 사업관리 > 정산 자료관리에 관한 VO 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.excclc.service;

import java.io.Serializable;

import egovframework.com.cmm.ComDefaultVO;

@SuppressWarnings("serial")
public class ExcclcVo extends ComDefaultVO implements Serializable {
	
	/** BIZ_ID, 사업ID */
	private String bizId; 
	
	/** DECSN_AMOUNT, 사업비 */
	private String decsnAmount;
	
	/** EXCUT_AMOUNT, 집행액 */
	private String excutAmount;
	
	/** RTURN_RESN, 반납사유 */
	private String rturnResn;
	
	/** DPST_INTR, 예금이자 */
	private String dpstIntr;
	
	/** DPST_INTR_RESN, 예금이자사유 */
	private String dpstIntrResn;
	
	/** TRMNAT_INTR, 해지이자 */
	private String trmnatIntr;
	
	/** TRMNAT_INTR_RESN, 해지이자사유  */
	private String trmnatIntrResn;
	
	/** EXCCLC_AT, 정산여부 */
	private String excclcAt;

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getDecsnAmount() {
		return decsnAmount;
	}

	public void setDecsnAmount(String decsnAmount) {
		this.decsnAmount = decsnAmount;
	}

	public String getExcutAmount() {
		return excutAmount;
	}

	public void setExcutAmount(String excutAmount) {
		this.excutAmount = excutAmount;
	}

	public String getRturnResn() {
		return rturnResn;
	}

	public void setRturnResn(String rturnResn) {
		this.rturnResn = rturnResn;
	}

	public String getDpstIntr() {
		return dpstIntr;
	}

	public void setDpstIntr(String dpstIntr) {
		this.dpstIntr = dpstIntr;
	}

	public String getDpstIntrResn() {
		return dpstIntrResn;
	}

	public void setDpstIntrResn(String dpstIntrResn) {
		this.dpstIntrResn = dpstIntrResn;
	}

	public String getTrmnatIntr() {
		return trmnatIntr;
	}

	public void setTrmnatIntr(String trmnatIntr) {
		this.trmnatIntr = trmnatIntr;
	}

	public String getTrmnatIntrResn() {
		return trmnatIntrResn;
	}

	public void setTrmnatIntrResn(String trmnatIntrResn) {
		this.trmnatIntrResn = trmnatIntrResn;
	}

	public String getExcclcAt() {
		return excclcAt;
	}

	public void setExcclcAt(String excclcAt) {
		this.excclcAt = excclcAt;
	}
	
}
