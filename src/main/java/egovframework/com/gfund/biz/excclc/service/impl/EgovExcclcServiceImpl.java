package egovframework.com.gfund.biz.excclc.service.impl;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.gfund.biz.excclc.service.EgovExcclcService;
import egovframework.com.gfund.biz.excclc.service.ExcclcVo;

@Service("excclcService")
public class EgovExcclcServiceImpl implements EgovExcclcService{
	
	@Resource(name = "excclcDao")
	private ExcclcDao excclcDao;
	
	/**
     * 녹색자금통합관리시스템 > 결과산출 > 최종정산에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param excclcVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertExcclc(ExcclcVo excclcVo) throws SQLException {
    	return excclcDao.insertExcclc(excclcVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 결과산출 > 최종정산에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param excclcVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateExcclc(ExcclcVo excclcVo) throws SQLException {
    	return excclcDao.updateExcclc(excclcVo);
    }
	
    /**
     * 녹색자금통합관리시스템 > 결과산출 > 최종정산에서 자료 한건을 삭제한다.
     * @param excclcVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deleteExcclc(ExcclcVo excclcVo) throws SQLException {
    	return excclcDao.deleteExcclc(excclcVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 결과산출 > 최종정산에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param excclcVo 자료 기본정보
     * @return excclcVo 자료 상제정보
     * @throws Exception
     */
    public ExcclcVo selectExcclc(ExcclcVo excclcVo) throws SQLException {
    	return excclcDao.selectExcclc(excclcVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 결과산출 > 최종정산에서 특정사업에 대한 자료 목록의 전체개수를 가져와 출력한다.
     * @param excclcVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public int selectExcclcListTotCnt(ExcclcVo excclcVo) throws SQLException {
    	return excclcDao.selectExcclcListTotCnt(excclcVo);
    }
	
}
