/**
 * 녹색자금통합관리시스템 > 사업관리 > 정산 자료관리에 관한 사용자용 컨트롤러 클래스를 정의한다.
 * 사용자는 최초 1회만 저장할 수 있고, 저장 이후에는 조회만 가능하다.
 * 관리자는 조회만 가능하며, 정산 초기화 버튼을 클릭하여 레코드를 삭제할 수 있다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.excclc.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.excclc.service.EgovExcclcService;
import egovframework.com.gfund.biz.excclc.service.ExcclcVo;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
public class EgovExcclcController {

	Logger log = LogManager.getLogger(this.getClass());

	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "excclcService")
	private EgovExcclcService excclcService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;


	/**
	 * 사업관리 > 결과산출 > 최종정산 작성화면으로 이동한다.
	 *
	 * @param excclcVo
	 * @param model 화면모델
	 * @param request
	 * @param response
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovExcclcAddView.do", "/gfund/biz/productmng/EgovExcclcAddView.do"})
	public String EgovExcclcAddView(@ModelAttribute("searchVO") ExcclcVo excclcVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		excclcVo.setBizId(bizId);

		BasicInformationVo bassVo = new BasicInformationVo();
		bassVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(bassVo);
		
		// 최종정산 존재여부 판단
		int totCnt = excclcService.selectExcclcListTotCnt(excclcVo);
		if (totCnt > 0) { // 존재한다면 기존 레코드 조회			
			ExcclcVo result = excclcService.selectExcclc(excclcVo);
			model.addAttribute("excclcVo", result);
		} else {
			// 총 사업비 조회결과 저장
			model.addAttribute("decsnAmount", basicInformationVo.getTot_wct()); // 화면에서 사업비의 경우 '녹색자금사업 기본정보' 테이블의 총 사업비(녹색자금)을 조회하여 사용
		}
		
		model.addAttribute("basicInformationVo", basicInformationVo);

		request.getSession().setAttribute("sessionVO", excclcVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovExcclcForm";
		}else{
			return "gfund/biz/productmng/EgovExcclcForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 결과산출 > 최종정산 삽입 후 조회화면으로 이동한다.
	 * 
	 * @param excclcVo
	 * @param model 화면모델
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovExcclcInsert.do", "/gfund/biz/productmng/EgovExcclcInsert.do"})
	public String insertEgovExcclc(@ModelAttribute("searchVO") ExcclcVo excclcVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/productmng/EgovExcclcForm";
			}else{
				return "gfund/biz/productmng/EgovExcclcForm.tiles";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");		
		excclcVo.setBizId(bizId);
		
		// 최종정산 존재여부 판단 - 추가 삽입 방지용도
		int totCnt = excclcService.selectExcclcListTotCnt(excclcVo);
		if (totCnt < 1) {
			// XSS 방지처리
			excclcVo.setRturnResn( NkrefoUtil.descript(excclcVo.getRturnResn()) );
			excclcVo.setDpstIntrResn( NkrefoUtil.descript(excclcVo.getDpstIntrResn()) );
			excclcVo.setTrmnatIntrResn( NkrefoUtil.descript(excclcVo.getTrmnatIntrResn()) );
			
			// 신규 레코드 삽입
			excclcVo.setExcclcAt("Y");
			excclcService.insertExcclc(excclcVo);
			
			// 화면에서는 최초 1회 저장 후 '저장' 버튼을 비활성화 처리해야 함
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovExcclcAddView.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovExcclcAddView.do";
		}
	}
	
	/**
	 * 사업관리 > 결과산출 > 최종정산 화면에서 현재 조회된 자료 1건을 삭제 처리를 한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/productmng/EgovExcclcDelete.do")
	public String deleteEgovExcclc(@ModelAttribute("searchVO") ExcclcVo excclcVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(excclcVo.getBizId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovExcclcAddView.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovExcclcAddView.do";
			}
		}
		
		// 레코드 삭제처리
		if (excclcService.deleteExcclc(excclcVo) > 0) {
			model.addAttribute("excclcmessage", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("excclcmessage", egovMessageSource.getMessage("fail.common.delete"));
		}

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovExcclcAddView.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovExcclcAddView.do";
		}
	}
	
}
