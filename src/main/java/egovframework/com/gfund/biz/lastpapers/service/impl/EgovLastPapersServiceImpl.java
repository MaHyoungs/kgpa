package egovframework.com.gfund.biz.lastpapers.service.impl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.gfund.biz.lastpapers.service.EgovLastPapersService;
import egovframework.com.gfund.biz.lastpapers.service.LastPapersVo;

@Service("lastPapersService")
public class EgovLastPapersServiceImpl implements EgovLastPapersService {

	@Resource(name = "lastPapersDao")
	private LastPapersDao lastPapersDao;
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param lastPapersVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertLastPapers(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.insertLastPapers(lastPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param lastPapersVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateLastPapers(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.updateLastPapers(lastPapersVo);
    }
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 자료 한건을 삭제한다.
     * @param lastPapersVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deleteLastPapers(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.deleteLastPapers(lastPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param lastPapersVo 자료 기본정보
     * @return lastPapersVo 자료 상제정보
     * @throws Exception
     */
    public LastPapersVo selectLastPapers(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.selectLastPapers(lastPapersVo);
    }

    /**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param lastPapersVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public List<?> selectLastPapersList(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.selectLastPapersList(lastPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 최종서류에서 특정 사업에 대한 자료 목록의 전체 개수를 가져온다.
     * @param lastPapersVo 검색조건
     * @return int 특정 사업에 대한 자료 목록의 전체 개수
     * @throws SQLException
     */
    public int selectLastPapersListTotCnt(LastPapersVo lastPapersVo) throws SQLException {
    	return lastPapersDao.selectLastPapersListTotCnt(lastPapersVo);
    }
	
}
