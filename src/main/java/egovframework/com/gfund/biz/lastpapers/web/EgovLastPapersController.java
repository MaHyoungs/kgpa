/**
 * 녹색자금통합관리시스템 > 사업관리 > 최종서류 자료관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.lastpapers.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.lastpapers.service.EgovLastPapersService;
import egovframework.com.gfund.biz.lastpapers.service.LastPapersVo;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovLastPapersController {

	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "lastPapersService")
	private EgovLastPapersService lastPapersService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	@Resource(name = "egovGFLastPapersGnrService")
	private EgovIdGnrService idgenService;
	
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	/**
	 * 결과산출 > 최종서류 제출 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersList.do", "/gfund/biz/productmng/EgovLastPapersList.do"})
	public String EgovLastPapersList(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
	
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		PaginationInfo paginationInfo = new PaginationInfo();
		
		// 페이징 정보 설정
		lastPapersVo.setPageUnit(propertiesService.getInt("pageUnit"));
		lastPapersVo.setPageSize(propertiesService.getInt("pageSize"));
		
		paginationInfo.setCurrentPageNo(lastPapersVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(lastPapersVo.getPageUnit());
		paginationInfo.setPageSize(lastPapersVo.getPageSize());
		
		lastPapersVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		lastPapersVo.setLastIndex(paginationInfo.getLastRecordIndex());
		lastPapersVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		lastPapersVo.setBizId(bizId);
		
		List<LastPapersVo> resultList = (List<LastPapersVo>) lastPapersService.selectLastPapersList(lastPapersVo);
		int totCnt = lastPapersService.selectLastPapersListTotCnt(lastPapersVo);
		
		paginationInfo.setTotalRecordCount(totCnt);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		//LPP01	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업추진실적보고서
		//LPP02	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업결과물
		//LPP03	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업완료정산보고서
		//LPP04	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 정산관련(사업비 집행액 반납액)
		//LPP05	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 통장해지사본
		//LPP06	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 기타서류
		comDefaultCodeVO.setCodeId("COM254");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		// 선정전형에서 최종서류제출을 하지 않은 경우 사업관리, 결과산출 하위 메뉴에 접근하지 못하도록 처리
		BasicInformationVo bassVo = new BasicInformationVo(); 
		bassVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(bassVo);
		model.addAttribute("lastBizPlanPresentnAt", basicInformationVo.getLast_biz_plan_presentn_at()); // 최종사업계획서제출여부 저장
		model.addAttribute("PropseBizId", basicInformationVo.getPropse_biz_id());
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovLastPapersList";
		}else{
			return "gfund/biz/productmng/EgovLastPapersList.tiles";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 등록화면으로 이동한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */               
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersAddView.do", "/gfund/biz/productmng/EgovLastPapersAddView.do"})
	public String EgovLastPapersAddView(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		//LPP01	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업추진실적보고서
		//LPP02	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업결과물
		//LPP03	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업완료정산보고서
		//LPP04	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 정산관련(사업비 집행액 반납액)
		//LPP05	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 통장해지사본
		//LPP06	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 기타서류
		comDefaultCodeVO.setCodeId("COM254");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", lastPapersVo); // YS1: 등록화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovLastPapersForm";
		}else{
			return "gfund/biz/productmng/EgovLastPapersForm.tiles";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 등록처리 후 목록화면으로 이동한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersInsert.do", "/gfund/biz/productmng/EgovLastPapersInsert.do"})
	public String insertEgovLastPapers(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 등록처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");
		
		lastPapersVo.setBizId(bizId);
		// XSS 방지처리
		lastPapersVo.setNttSj( NkrefoUtil.descript(lastPapersVo.getNttSj()) );
		lastPapersVo.setNttCn( NkrefoUtil.descript(lastPapersVo.getNttCn()) );
		
		// 신규 관련증빙서ID 발급 및 레코드 삽입
		lastPapersVo.setLastPapersId(idgenService.getNextStringId());
		lastPapersService.insertLastPapers(lastPapersVo);
		
		if(lastPapersVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(lastPapersVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		
		// YS3: 등록처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 목록화면에서 자료 1건을 삭제 처리를 한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersDelete.do", "/gfund/biz/productmng/EgovLastPapersDelete.do"})
	public String deleteEgovLastPapers(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(lastPapersVo.getLastPapersId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
			}
		}
		
		LastPapersVo papersVo = lastPapersService.selectLastPapers(lastPapersVo);
		String atchFileId = papersVo.getAtchFileId();
		// 첨부파일 삭제처리
		if (!EgovStringUtil.isEmpty(atchFileId)) fileMngService.deleteFileInfs(atchFileId);		
		
		// 레코드 삭제처리
		if (lastPapersService.deleteLastPapers(papersVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.delete"));
		}

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 수정 화면으로 이동한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersUpdtView.do", "/gfund/biz/productmng/EgovLastPapersUpdtView.do"})
	public String EgovLastPapersUpdtView(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(lastPapersVo.getLastPapersId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
			}
		}
		
		LastPapersVo result = lastPapersService.selectLastPapers(lastPapersVo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		//LPP01	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업추진실적보고서
		//LPP02	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업결과물
		//LPP03	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업완료정산보고서
		//LPP04	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 정산관련(사업비 집행액 반납액)
		//LPP05	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 통장해지사본
		//LPP06	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 기타서류
		comDefaultCodeVO.setCodeId("COM254");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		// enscript 처리
		result.setNttSj( NkrefoUtil.enscript(result.getNttSj()) );
		result.setNttCn( NkrefoUtil.enscript(result.getNttCn()) );
		
		model.addAttribute("lastPapersVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", lastPapersVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovLastPapersForm";
		}else{
			return "gfund/biz/productmng/EgovLastPapersForm.tiles";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 수정 후 목록화면으로 이동한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersSelectUpdt.do", "/gfund/biz/productmng/EgovLastPapersSelectUpdt.do"})
	public String EgovLastPapersSelectUpdt(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
			}
		}
		
		// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
		LastPapersVo uptLastPapersVo = lastPapersService.selectLastPapers(lastPapersVo);
		
		// XSS 방지처리
		uptLastPapersVo.setNttSj( NkrefoUtil.descript(lastPapersVo.getNttSj()) );
		uptLastPapersVo.setNttCn( NkrefoUtil.descript(lastPapersVo.getNttCn()) );
		
		uptLastPapersVo.setLastPapersCl(lastPapersVo.getLastPapersCl());
		uptLastPapersVo.setLastUpdusrId(lastPapersVo.getLastUpdusrId());
		
		// 첨부파일 정보 갱신 처리
		if(lastPapersVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(lastPapersVo.getFileGroupId());
			fvo.setAtchFileId(lastPapersVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		
		if (lastPapersService.updateLastPapers(uptLastPapersVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
			
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/productmng/EgovLastPapersForm";
			}else{
				return "gfund/biz/productmng/EgovLastPapersForm.tiles";
			}
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
		}else{
			return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
		}
	}
	
	/**
	 * 결과산출 > 최종서류 제출 조회 화면으로 이동한다.
	 * 
	 * @param lastPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/productmng/EgovLastPapersSelectView.do", "/gfund/biz/productmng/EgovLastPapersSelectView.do"})
	public String EgovLastPapersSelectView(@ModelAttribute("searchVO") LastPapersVo lastPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		if (EgovStringUtil.isEmpty(lastPapersVo.getLastPapersId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/productmng/EgovLastPapersList.do";
			}else{
				return "forward:/gfund/biz/productmng/EgovLastPapersList.do";
			}
		}
		
		LastPapersVo result = lastPapersService.selectLastPapers(lastPapersVo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		//LPP01	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업추진실적보고서
		//LPP02	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업결과물
		//LPP03	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 사업완료정산보고서
		//LPP04	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 정산관련(사업비 집행액 반납액)
		//LPP05	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 통장해지사본
		//LPP06	녹색자금통합관리시스템 - 결과산출 > 최종서류 제출 > 최종서류 분류목록 > 기타서류
		comDefaultCodeVO.setCodeId("COM254");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));				
		model.addAttribute("lastPapersVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/productmng/EgovLastPapersInquire";
		}else{
			return "gfund/biz/productmng/EgovLastPapersInquire.tiles";
		}
	}
	
}
