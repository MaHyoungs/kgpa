package egovframework.com.gfund.biz.nmpracmslt.service;

import java.sql.SQLException;
import java.util.List;

public interface EgovNmprAcmsltService {
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param nmprAcmsltVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param nmprAcmsltVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 자료 한건을 삭제한다.
     * @param nmprAcmsltVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deleteNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param nmprAcmsltVo 자료 기본정보
     * @return nmprAcmsltVo 자료 상제정보
     * @throws Exception
     */
    public NmprAcmsltVo selectNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정사업에 대한 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param nmprAcmsltVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public List<?> selectNmprAcmsltList(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정사업에 대한 자료 목록의 전체개수를 가져와 출력한다.
     * @param nmprAcmsltVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public int selectNmprAcmsltListTotCnt(NmprAcmsltVo nmprAcmsltVo) throws SQLException ;
}
