/**
 * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적 관리에 관한 VO 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.nmpracmslt.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import egovframework.com.cmm.ComDefaultVO;

@SuppressWarnings("serial")
public class NmprAcmsltVo extends ComDefaultVO implements Serializable {

	/** MENA_ID, 월별교육인원실적ID */
	private String menaId;
	
	/** BIZ_ID, 사업ID */
	private String bizId;
	
	/** MT_CL, 월분류(공통코드) */
	private String mtCl;
	
	/** EDC_CO, 교육횟수 */
	private int edcCo;
	
	/** EDC_NMPR, 교육인원 */
	private int edcNmpr;
	
	/** FRGLTY_SCLSRT_01, 취약계층 장애인 */
	private int frgltySclsrt01;
	
	/** FRGLTY_SCLSRT_02, 취약계층 저소득 */
	private int frgltySclsrt02;
	
	/** FRGLTY_SCLSRT_03, 취약계층 다문화 */
	private int frgltySclsrt03;
	
	/** FRGLTY_SCLSRT_04, 취약계층 한부모 */
	private int frgltySclsrt04;
	
	/** FRGLTY_SCLSRT_05, 취약계층 장기실업 */
	private int frgltySclsrt05;
	
	/** FRGLTY_SCLSRT_06, 취약계층 부적응 */
	private int frgltySclsrt06;
	
	/** FRGLTY_SCLSRT_07, 취약계층 어르신 */
	private int frgltySclsrt07;
	
	/** FRGLTY_SCLSRT_08, 취약계층 북한이탈 가족피해자(숲길체험) */
	private int frgltySclsrt08;
	
	/** YNGBGS, 청소년 */
	private int yngbgs;
	
	/** GNRL_PERSON, 일반인 */
	private int gnrlPerson;
	
	/** ETC, 기타 */
	private int etc;
	
	/** FRST_REGIST_PNTTM, 최초등록시점 */
	private java.util.Date frstRegistPnttm;
	
	/** FRST_REGISTER_ID, 최초등록자ID */
	private String frstRegisterId;
	
	/** LAST_UPDUSR_PNTTM, 최종수정시점 */
	private java.util.Date lastUpdusrPnttm;
	
	/** LAST_UPDUSR_ID, 최종수정자ID */
	private String lastUpdusrId;
	
	
	/** MT_CL, 월분류(공통코드) */
	private List mtClList = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** EDC_CO, 교육횟수 */
	private List edcCoList = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** EDC_NMPR, 교육인원 */
	private List edcNmprList = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_01, 취약계층 장애인 */
	private List frgltySclsrt01List = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_02, 취약계층 저소득 */
	private List frgltySclsrt02List = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_03, 취약계층 다문화 */
	private List frgltySclsrt03List = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_04, 취약계층 한부모 */
	private List frgltySclsrt04List = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_05, 취약계층 장기실업 */
	private List frgltySclsrt05List = ListUtils.lazyList(new ArrayList(), new Factory() {
		public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_06, 취약계층 부적응 */
	private List frgltySclsrt06List = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_07, 취약계층 어르신 */
	private List frgltySclsrt07List = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** FRGLTY_SCLSRT_08, 취약계층 북한이탈 가족피해자(숲길체험) */
	private List frgltySclsrt08List = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** YNGBGS, 청소년 */
	private List yngbgsList = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** GNRL_PERSON, 일반인 */
	private List gnrlPersonList = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
	
	/** ETC, 기타 */
	private List etcList = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new String();
	    }
	});
		
	/** FRST_REGISTER_ID, 최초등록자ID */
	private List frstRegisterIdList = ListUtils.lazyList(new ArrayList(), new Factory() {
    public Object create() {
	      return new String();
	    }
	});
		
	/** LAST_UPDUSR_ID, 최종수정자ID */
	private List lastUpdusrIdList = ListUtils.lazyList(new ArrayList(), new Factory() {
    public Object create() {
	      return new String();
	    }
	});
	
	public String getMenaId() {
		return menaId;
	}

	public void setMenaId(String menaId) {
		this.menaId = menaId;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getMtCl() {
		return mtCl;
	}

	public void setMtCl(String mtCl) {
		this.mtCl = mtCl;
	}

	public int getEdcCo() {
		return edcCo;
	}

	public void setEdcCo(int edcCo) {
		this.edcCo = edcCo;
	}

	public int getEdcNmpr() {
		return edcNmpr;
	}

	public void setEdcNmpr(int edcNmpr) {
		this.edcNmpr = edcNmpr;
	}

	public int getFrgltySclsrt01() {
		return frgltySclsrt01;
	}

	public void setFrgltySclsrt01(int frgltySclsrt01) {
		this.frgltySclsrt01 = frgltySclsrt01;
	}

	public int getFrgltySclsrt02() {
		return frgltySclsrt02;
	}

	public void setFrgltySclsrt02(int frgltySclsrt02) {
		this.frgltySclsrt02 = frgltySclsrt02;
	}

	public int getFrgltySclsrt03() {
		return frgltySclsrt03;
	}

	public void setFrgltySclsrt03(int frgltySclsrt03) {
		this.frgltySclsrt03 = frgltySclsrt03;
	}

	public int getFrgltySclsrt04() {
		return frgltySclsrt04;
	}

	public void setFrgltySclsrt04(int frgltySclsrt04) {
		this.frgltySclsrt04 = frgltySclsrt04;
	}

	public int getFrgltySclsrt05() {
		return frgltySclsrt05;
	}

	public void setFrgltySclsrt05(int frgltySclsrt05) {
		this.frgltySclsrt05 = frgltySclsrt05;
	}

	public int getFrgltySclsrt06() {
		return frgltySclsrt06;
	}

	public void setFrgltySclsrt06(int frgltySclsrt06) {
		this.frgltySclsrt06 = frgltySclsrt06;
	}

	public int getFrgltySclsrt07() {
		return frgltySclsrt07;
	}

	public void setFrgltySclsrt07(int frgltySclsrt07) {
		this.frgltySclsrt07 = frgltySclsrt07;
	}

	public int getFrgltySclsrt08() {
		return frgltySclsrt08;
	}

	public void setFrgltySclsrt08(int frgltySclsrt08) {
		this.frgltySclsrt08 = frgltySclsrt08;
	}

	public int getYngbgs() {
		return yngbgs;
	}

	public void setYngbgs(int yngbgs) {
		this.yngbgs = yngbgs;
	}

	public int getGnrlPerson() {
		return gnrlPerson;
	}

	public void setGnrlPerson(int gnrlPerson) {
		this.gnrlPerson = gnrlPerson;
	}

	public int getEtc() {
		return etc;
	}

	public void setEtc(int etc) {
		this.etc = etc;
	}

	public java.util.Date getFrstRegistPnttm() {
		return frstRegistPnttm;
	}

	public void setFrstRegistPnttm(java.util.Date frstRegistPnttm) {
		this.frstRegistPnttm = frstRegistPnttm;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public List getMtClList() {
		return mtClList;
	}

	public void setMtClList(List mtClList) {
		this.mtClList = mtClList;
	}

	public List getEdcCoList() {
		return edcCoList;
	}

	public void setEdcCoList(List edcCoList) {
		this.edcCoList = edcCoList;
	}

	public List getEdcNmprList() {
		return edcNmprList;
	}

	public void setEdcNmprList(List edcNmprList) {
		this.edcNmprList = edcNmprList;
	}

	public List getFrgltySclsrt01List() {
		return frgltySclsrt01List;
	}

	public void setFrgltySclsrt01List(List frgltySclsrt01List) {
		this.frgltySclsrt01List = frgltySclsrt01List;
	}

	public List getFrgltySclsrt02List() {
		return frgltySclsrt02List;
	}

	public void setFrgltySclsrt02List(List frgltySclsrt02List) {
		this.frgltySclsrt02List = frgltySclsrt02List;
	}

	public List getFrgltySclsrt03List() {
		return frgltySclsrt03List;
	}

	public void setFrgltySclsrt03List(List frgltySclsrt03List) {
		this.frgltySclsrt03List = frgltySclsrt03List;
	}

	public List getFrgltySclsrt04List() {
		return frgltySclsrt04List;
	}

	public void setFrgltySclsrt04List(List frgltySclsrt04List) {
		this.frgltySclsrt04List = frgltySclsrt04List;
	}

	public List getFrgltySclsrt05List() {
		return frgltySclsrt05List;
	}

	public void setFrgltySclsrt05List(List frgltySclsrt05List) {
		this.frgltySclsrt05List = frgltySclsrt05List;
	}

	public List getFrgltySclsrt06List() {
		return frgltySclsrt06List;
	}

	public void setFrgltySclsrt06List(List frgltySclsrt06List) {
		this.frgltySclsrt06List = frgltySclsrt06List;
	}

	public List getFrgltySclsrt07List() {
		return frgltySclsrt07List;
	}

	public void setFrgltySclsrt07List(List frgltySclsrt07List) {
		this.frgltySclsrt07List = frgltySclsrt07List;
	}

	public List getFrgltySclsrt08List() {
		return frgltySclsrt08List;
	}

	public void setFrgltySclsrt08List(List frgltySclsrt08List) {
		this.frgltySclsrt08List = frgltySclsrt08List;
	}

	public List getYngbgsList() {
		return yngbgsList;
	}

	public void setYngbgsList(List yngbgsList) {
		this.yngbgsList = yngbgsList;
	}

	public List getGnrlPersonList() {
		return gnrlPersonList;
	}

	public void setGnrlPersonList(List gnrlPersonList) {
		this.gnrlPersonList = gnrlPersonList;
	}

	public List getEtcList() {
		return etcList;
	}

	public void setEtcList(List etcList) {
		this.etcList = etcList;
	}

	public List getFrstRegisterIdList() {
		return frstRegisterIdList;
	}

	public void setFrstRegisterIdList(List frstRegisterIdList) {
		this.frstRegisterIdList = frstRegisterIdList;
	}

	public List getLastUpdusrIdList() {
		return lastUpdusrIdList;
	}

	public void setLastUpdusrIdList(List lastUpdusrIdList) {
		this.lastUpdusrIdList = lastUpdusrIdList;
	}
	
}
