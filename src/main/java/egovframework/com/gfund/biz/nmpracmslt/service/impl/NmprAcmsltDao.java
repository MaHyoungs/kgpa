/**
 * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적 관리에 관한 데이터 접근 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.nmpracmslt.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.gfund.biz.nmpracmslt.service.NmprAcmsltVo;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

@Repository("nmprAcmsltDao")
public class NmprAcmsltDao extends EgovAbstractDAO {
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param nmprAcmsltVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return (String)insert("nmprAcmsltDao.insertNmprAcmslt", nmprAcmsltVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param nmprAcmsltVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return update("nmprAcmsltDao.updateNmprAcmslt", nmprAcmsltVo);
    }
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 자료 한건을 삭제한다.
     * @param nmprAcmsltVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deleteNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return update("nmprAcmsltDao.deleteNmprAcmslt", nmprAcmsltVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param nmprAcmsltVo 자료 기본정보
     * @return nmprAcmsltVo 자료 상제정보
     * @throws Exception
     */
    public NmprAcmsltVo selectNmprAcmslt(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return (NmprAcmsltVo) selectByPk("nmprAcmsltDao.selectNmprAcmslt", nmprAcmsltVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정사업에 대한 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param nmprAcmsltVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public List<?> selectNmprAcmsltList(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return list("nmprAcmsltDao.selectNmprAcmsltList", nmprAcmsltVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적에서 특정사업에 대한 자료 목록의 전체개수를 가져와 출력한다.
     * @param nmprAcmsltVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public int selectNmprAcmsltListTotCnt(NmprAcmsltVo nmprAcmsltVo) throws SQLException {
        return (Integer) getSqlMapClientTemplate().queryForObject("nmprAcmsltDao.selectNmprAcmsltListTotCnt", nmprAcmsltVo);
    }

}
