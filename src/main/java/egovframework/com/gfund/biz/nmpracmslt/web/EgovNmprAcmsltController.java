/**
 * 녹색자금통합관리시스템 > 사업관리 > 월별교육인원실적 자료관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.nmpracmslt.web;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.nmpracmslt.service.EgovNmprAcmsltService;
import egovframework.com.gfund.biz.nmpracmslt.service.NmprAcmsltVo;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
public class EgovNmprAcmsltController {

	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "nmprAcmsltService")
	private EgovNmprAcmsltService nmprAcmsltService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	@Resource(name = "egovGFMnbyEdcNmprAcmsltGnrService")
	private EgovIdGnrService idgenService;
	
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	/**
	 * 사업관리 > 사업현황 > 월별인원실적 수정화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do", "/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do"})
	public String EgovNmprAcmsltView(@ModelAttribute("searchVO") NmprAcmsltVo nmprAcmsltVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		String bizType = "EDU";
		if ("BTC01".equals(bizTyCode) || "BTC02".equals(bizTyCode) || "BTC03".equals(bizTyCode)) { // 나눔숲 3가지 인 경우는 조성사업으로 처리
			bizType = "BIZ";
		} else { // 숲체헙교육 2가지 인 경우는 비조성사업(체험교육)으로 처리
			bizType = "EDU";
		}
		
		if (bizType.equals("BIZ")) {
			model.addAttribute("message", "조성사업은 월별사업추진계획 및 실적만 접근 가능합니다");
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
			}
		}

		nmprAcmsltVo.setBizId(bizId);

		// 월별인원실적  존재여부 판단
		int totCnt = nmprAcmsltService.selectNmprAcmsltListTotCnt(nmprAcmsltVo);
		if (totCnt == 12) { // 존재한다면 기존 레코드 조회
			List<NmprAcmsltVo> resultList = (List<NmprAcmsltVo>)nmprAcmsltService.selectNmprAcmsltList(nmprAcmsltVo);
			
			model.addAttribute("resultList", resultList);
			model.addAttribute("resultCnt", totCnt);
		}
		
		request.getSession().setAttribute("sessionVO", nmprAcmsltVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovNmprAcmsltForm";
		}else{
			return "gfund/biz/businessmng/EgovNmprAcmsltForm.tiles";
		}		
	}
	
	/**
	 * 사업관리 > 사업현황 > 월별인원실적 삽입, 수정 후 이전화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovNmprAcmsltSelectUpdt.do", "/gfund/biz/businessmng/EgovNmprAcmsltSelectUpdt.do"})
	public String EgovNmprAcmsltSelectUpdt(@ModelAttribute("searchVO") NmprAcmsltVo nmprAcmsltVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/businessmng/EgovNmprAcmsltForm";
			}else{
				return "gfund/biz/businessmng/EgovNmprAcmsltForm.tiles";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		//NmprAcmsltVo nmprAcmsltVo = new NmprAcmsltVo();
		nmprAcmsltVo.setBizId(bizId);
		
		// 입력폼으로부터 전송된 여러개의 파라미터를 name 별로 가져온다.
		// "edcCo", "edcNmpr", "frgltySclsrt01", "frgltySclsrt02", "frgltySclsrt03", "frgltySclsrt04", "frgltySclsrt05", "frgltySclsrt06", "frgltySclsrt07", "frgltySclsrt08", "yngbgs", "gnrlPerson", "etc", "mtCl", "frstRegisterId", "lastUpdusrId"
		String[] edcCo = request.getParameterValues("edcCoList");
		String[] edcNmpr = request.getParameterValues("edcNmprList");		
		String[] frgltySclsrt01 = request.getParameterValues("frgltySclsrt01List");
		String[] frgltySclsrt02 = request.getParameterValues("frgltySclsrt02List");
		String[] frgltySclsrt03 = request.getParameterValues("frgltySclsrt03List");
		String[] frgltySclsrt04 = request.getParameterValues("frgltySclsrt04List");
		String[] frgltySclsrt05 = request.getParameterValues("frgltySclsrt05List");
		String[] frgltySclsrt06 = request.getParameterValues("frgltySclsrt06List");
		String[] frgltySclsrt07 = request.getParameterValues("frgltySclsrt07List");
		String[] frgltySclsrt08 = request.getParameterValues("frgltySclsrt08List");
		String[] yngbgs = request.getParameterValues("yngbgsList");
		String[] gnrlPerson = request.getParameterValues("gnrlPersonList");
		String[] etc = request.getParameterValues("etcList");
		String[] mtCl = request.getParameterValues("mtClList");
		String[] frstRegisterId = request.getParameterValues("frstRegisterIdList");
		String[] lastUpdusrId = request.getParameterValues("lastUpdusrIdList");

		// 변경 처리
		int formNameMax = mtCl.length;
		int successCount = 0;

		// 월별인원실적  존재여부 판단
		int totCnt = nmprAcmsltService.selectNmprAcmsltListTotCnt(nmprAcmsltVo);
		if (totCnt == 12) { // 존재한다면 기존 레코드 조회 후 변경 처리
			// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
			List<NmprAcmsltVo> resultList = (List<NmprAcmsltVo>)nmprAcmsltService.selectNmprAcmsltList(nmprAcmsltVo);
			
			Hashtable nmprAcmsltTable = new Hashtable();
			for (NmprAcmsltVo tmpVo : resultList) {
				String key = tmpVo.getBizId() + tmpVo.getMtCl();
				nmprAcmsltTable.put(key, tmpVo);
			}
			
			for (int idx=0; idx<formNameMax; idx++) {
				
				try {
					String key = bizId + mtCl[idx];
					NmprAcmsltVo orgVo = (NmprAcmsltVo)nmprAcmsltTable.get(key);
					orgVo.setEdcCo(Integer.parseInt(edcCo[idx])); // 교육횟수
					orgVo.setEdcNmpr(Integer.parseInt(edcNmpr[idx])); // 교육인원
					orgVo.setFrgltySclsrt01(Integer.parseInt(frgltySclsrt01[idx])); // 취약계층 장애인
					orgVo.setFrgltySclsrt02(Integer.parseInt(frgltySclsrt02[idx])); // 취약계층 저소득
					orgVo.setFrgltySclsrt03(Integer.parseInt(frgltySclsrt03[idx])); // 취약계층 다문화
					orgVo.setFrgltySclsrt04(Integer.parseInt(frgltySclsrt04[idx])); // 취약계층 한부모
					orgVo.setFrgltySclsrt05(Integer.parseInt(frgltySclsrt05[idx])); // 취약계층 장기실업
					orgVo.setFrgltySclsrt06(Integer.parseInt(frgltySclsrt06[idx])); // 취약계층 부적응
					orgVo.setFrgltySclsrt07(Integer.parseInt(frgltySclsrt07[idx])); // 취약계층 어르신
					orgVo.setFrgltySclsrt08(Integer.parseInt(frgltySclsrt08[idx])); // 취약계층 북한이탈 가족피해자(숲길체럼)
					orgVo.setYngbgs(Integer.parseInt(yngbgs[idx])); // 청소년
					orgVo.setGnrlPerson(Integer.parseInt(gnrlPerson[idx])); // 일반인
					orgVo.setEtc(Integer.parseInt(etc[idx])); // 기타
					orgVo.setLastUpdusrId(lastUpdusrId[idx]); // 최종수정자
					
					nmprAcmsltService.updateNmprAcmslt(orgVo); // 업데이트 성공 개수 기록
					
					successCount++;
				
				} catch (Exception ex) {
					continue;
				}
			}
		} else { // 신규레코드로 삽입 처리
			
			for (int idx=0; idx<formNameMax; idx++) {
			
				try {
					NmprAcmsltVo newVo = new NmprAcmsltVo();
					newVo.setEdcCo(Integer.parseInt(edcCo[idx])); // 교육횟수
					newVo.setEdcNmpr(Integer.parseInt(edcNmpr[idx])); // 교육인원
					newVo.setFrgltySclsrt01(Integer.parseInt(frgltySclsrt01[idx])); // 취약계층 장애인
					newVo.setFrgltySclsrt02(Integer.parseInt(frgltySclsrt02[idx])); // 취약계층 저소득
					newVo.setFrgltySclsrt03(Integer.parseInt(frgltySclsrt03[idx])); // 취약계층 다문화
					newVo.setFrgltySclsrt04(Integer.parseInt(frgltySclsrt04[idx])); // 취약계층 한부모
					newVo.setFrgltySclsrt05(Integer.parseInt(frgltySclsrt05[idx])); // 취약계층 장기실업
					newVo.setFrgltySclsrt06(Integer.parseInt(frgltySclsrt06[idx])); // 취약계층 부적응
					newVo.setFrgltySclsrt07(Integer.parseInt(frgltySclsrt07[idx])); // 취약계층 어르신
					newVo.setFrgltySclsrt08(Integer.parseInt(frgltySclsrt08[idx])); // 취약계층 북한이탈 가족피해자(숲길체럼)
					newVo.setYngbgs(Integer.parseInt(yngbgs[idx])); // 청소년
					newVo.setGnrlPerson(Integer.parseInt(gnrlPerson[idx])); // 일반인
					newVo.setEtc(Integer.parseInt(etc[idx])); // 기타
					newVo.setFrstRegisterId(frstRegisterId[idx]); // 최초 등록자
					newVo.setBizId(bizId);
					newVo.setMtCl(mtCl[idx]);
					newVo.setMenaId(idgenService.getNextStringId());
					nmprAcmsltService.insertNmprAcmslt(newVo);
					
					successCount++;
				
				} catch (Exception ex) {
					continue;
				}
			}
		}
		
		if (successCount < 12) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
		}		

		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");

		// 다시 조회하여 리스트 갱신처리
		List<NmprAcmsltVo> resultList = (List<NmprAcmsltVo>)nmprAcmsltService.selectNmprAcmsltList(nmprAcmsltVo);
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do";
		}
	}
	
}
