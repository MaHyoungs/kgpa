package egovframework.com.gfund.biz.photo.service.impl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.gfund.biz.photo.service.EgovPhotoInfoService;
import egovframework.com.gfund.biz.photo.service.PhotoInfoVo;

@Service("photoInfoService")
public class EgovPhotoInfoServiceImpl implements EgovPhotoInfoService {
	
	@Resource(name = "photoInfoDao")
	private PhotoInfoDao photoInfoDao;
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param photoInfoVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertPhotoinfo(PhotoInfoVo photoInfoVo) throws SQLException {
    	
    	//System.out.println("===================================================================");
    	//System.out.println("photoInfoVo.getPhotoInfoId() = " + photoInfoVo.getPhotoInfoId());
    	//System.out.println("photoInfoVo.getBizId() = " + photoInfoVo.getBizId());
    	//System.out.println("photoInfoVo.getAtchFileId() = " + photoInfoVo.getAtchFileId());
    	//System.out.println("photoInfoVo.getMakeBizCl() = " + photoInfoVo.getMakeBizCl());
    	//System.out.println("photoInfoVo.getFrtExprnCl() = " + photoInfoVo.getFrtExprnCl());
    	//System.out.println("photoInfoVo.getNttSj() = " + photoInfoVo.getNttSj());
    	//System.out.println("photoInfoVo.getNttCn() = " + photoInfoVo.getNttCn());
    	//System.out.println("photoINfoVo.getFrstRegistPnttm() = " + photoInfoVo.getFrstRegistPnttm());
    	//System.out.println("photoINfoVo.getFrstRegisterId() = " + photoInfoVo.getFrstRegisterId());
    	//System.out.println("photoINfoVo.getLastUpdusrPnttm() = " + photoInfoVo.getLastUpdusrPnttm());
    	//System.out.println("photoINfoVo.getLastUpdusrId() = " + photoInfoVo.getLastUpdusrId());
    	//System.out.println("===================================================================");
    	
    	return photoInfoDao.insertPhotoinfo(photoInfoVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param photoInfoVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updatePhotoinfo(PhotoInfoVo photoInfoVo) throws SQLException {
    	return photoInfoDao.updatePhotoinfo(photoInfoVo);
    }
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 자료 한건을 삭제한다.
     * @param photoInfoVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deletePhotoinfo(PhotoInfoVo photoInfoVo) throws SQLException {
    	return photoInfoDao.deletePhotoinfo(photoInfoVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param photoInfoVo 자료 기본정보
     * @return photoInfoVo 자료 상제정보
     * @throws Exception
     */
    public PhotoInfoVo selectPhotoinfo(PhotoInfoVo photoInfoVo) throws SQLException {
    	return photoInfoDao.selectPhotoinfo(photoInfoVo);
    }

    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param photoInfoVo 검색조건
     * @return List 사진 목록정보
     * @throws Exception
     */
    public List<?> selectPhotoinfoList(PhotoInfoVo photoInfoVo) throws SQLException {
    	return photoInfoDao.selectPhotoinfoList(photoInfoVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사진정보에서 특정 사업에 대한 자료 목록의 전체 개수를 가져온다.
     * @param photoInfoVo 검색조건
     * @return int 특정 사업에 대한 자료 목록의 전체 개수
     * @throws Exception
     */
    public int selectPhotoinfoListTotCnt(PhotoInfoVo photoInfoVo) throws SQLException {
    	return photoInfoDao.selectPhotoinfoListTotCnt(photoInfoVo);
    }
}
