/**
 * 녹색자금통합관리시스템 > 사업관리 > 사진정보 자료관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.photo.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.photo.service.EgovPhotoInfoService;
import egovframework.com.gfund.biz.photo.service.PhotoInfoVo;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovPhotoInfoController {
	
	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "photoInfoService")
	private EgovPhotoInfoService photoInfoService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	@Resource(name = "egovGFPhotoInfoGnrService")
	private EgovIdGnrService idgenService;
	
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	/**
	 * 사업관리 > 사진정보 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoList.do", "/gfund/biz/businessmng/EgovPhotoInfoList.do"})
	public String EgovPhotoInfoList(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		PaginationInfo paginationInfo = new PaginationInfo();
		
		// 페이징 정보 설정
		//photoInfoVo.setPageUnit(propertiesService.getInt("pageUnit"));
		//photoInfoVo.setPageSize(propertiesService.getInt("pageSize"));
		photoInfoVo.setPageUnit(3);
		photoInfoVo.setPageSize(3);
		
		paginationInfo.setCurrentPageNo(photoInfoVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(photoInfoVo.getPageUnit());
		paginationInfo.setPageSize(photoInfoVo.getPageSize());
		
		photoInfoVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		photoInfoVo.setLastIndex(paginationInfo.getLastRecordIndex());
		photoInfoVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		photoInfoVo.setBizId(bizId);
		
		List<PhotoInfoVo> resultList = (List<PhotoInfoVo>) photoInfoService.selectPhotoinfoList(photoInfoVo);
		int totCnt = photoInfoService.selectPhotoinfoListTotCnt(photoInfoVo);
		
		paginationInfo.setTotalRecordCount(totCnt);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		if ("BTC01".equals(bizTyCode) || "BTC02".equals(bizTyCode) || "BTC03".equals(bizTyCode)) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			photoInfoVo.setBizType("BIZ");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			photoInfoVo.setBizType("EDU");
		}
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// BTC01: 복지시설(사회복지시설)나눔숲
		// BTC02: 복지시설(특수교육시설)나눔숲
		// BTC03: 지역사회 나눔숲
		// BTC04: 체험교육 나눔숲
		// BTC05: 숲 체험교육		
		if ("BIZ".equals(photoInfoVo.getBizType())) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			comDefaultCodeVO.setCodeId("COM250");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			comDefaultCodeVO.setCodeId("COM251");
		}
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		model.addAttribute("bizType", photoInfoVo.getBizType());
		
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPhotoInfoList";
		}else{
			return "gfund/biz/businessmng/EgovPhotoInfoList.tiles";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 등록화면으로 이동한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoAddView.do", "/gfund/biz/businessmng/EgovPhotoInfoAddView.do"})
	public String EgovPhotoInfoAddView(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
				
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// BTC01: 복지시설(사회복지시설)나눔숲
		// BTC02: 복지시설(특수교육시설)나눔숲
		// BTC03: 지역사회 나눔숲
		// BTC04: 체험교육 나눔숲
		// BTC05: 숲 체험교육		
		if ("BIZ".equals(photoInfoVo.getBizType())) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			comDefaultCodeVO.setCodeId("COM250");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			comDefaultCodeVO.setCodeId("COM251");
		}
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		model.addAttribute("bizId", photoInfoVo.getBizId());
		model.addAttribute("bizType", photoInfoVo.getBizType());
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", photoInfoVo); // YS1: 등록화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPhotoInfoForm";
		}else{
			return "gfund/biz/businessmng/EgovPhotoInfoForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 등록처리 후 목록화면으로 이동한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoInsert.do", "/gfund/biz/businessmng/EgovPhotoInfoInsert.do"})
	public String insertEgovPhotoInfo(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 등록처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");
		
		photoInfoVo.setBizId(bizId);
		// XSS 방지처리
		photoInfoVo.setNttSj( NkrefoUtil.descript(photoInfoVo.getNttSj()) );
		photoInfoVo.setNttCn( NkrefoUtil.descript(photoInfoVo.getNttCn()) );
		
		// 신규 사진정보ID 발급 및 레코드 삽입
		photoInfoVo.setPhotoInfoId(idgenService.getNextStringId());
		photoInfoService.insertPhotoinfo(photoInfoVo);
		
		if(photoInfoVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(photoInfoVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
				
		// YS3: 등록처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 목록화면에서 자료 1건을 삭제 처리를 한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoDelete.do", "/gfund/biz/businessmng/EgovPhotoInfoDelete.do"})
	public String deleteEgovPhotoInfo(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(photoInfoVo.getPhotoInfoId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}
		}
		
		PhotoInfoVo photoVo = photoInfoService.selectPhotoinfo(photoInfoVo);
		String atchFileId = photoVo.getAtchFileId();
		// 첨부파일 삭제처리
		if (!EgovStringUtil.isEmpty(atchFileId)) fileMngService.deleteFileInfs(atchFileId);		
		
		// 레코드 삭제처리
		if (photoInfoService.deletePhotoinfo(photoInfoVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.delete"));
		}

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 수정 화면으로 이동한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoUpdtView.do", "/gfund/biz/businessmng/EgovPhotoInfoUpdtView.do"})
	public String EgovPhotoInfoUpdtView(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(photoInfoVo.getPhotoInfoId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}
		}
		
		PhotoInfoVo result = photoInfoService.selectPhotoinfo(photoInfoVo);
				
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		if ("BTC01".equals(bizTyCode) || "BTC02".equals(bizTyCode) || "BTC03".equals(bizTyCode)) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			result.setBizType("BIZ");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			result.setBizType("EDU");
		}
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// BTC01: 복지시설(사회복지시설)나눔숲
		// BTC02: 복지시설(특수교육시설)나눔숲
		// BTC03: 지역사회 나눔숲
		// BTC04: 체험교육 나눔숲
		// BTC05: 숲 체험교육		
		if ("BIZ".equals(result.getBizType())) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			comDefaultCodeVO.setCodeId("COM250");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			comDefaultCodeVO.setCodeId("COM251");
		}
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		// enscript 처리
		result.setNttSj( NkrefoUtil.enscript(result.getNttSj()) );
		result.setNttCn( NkrefoUtil.enscript(result.getNttCn()) );
		
		model.addAttribute("photoInfoVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", photoInfoVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPhotoInfoForm";
		}else{
			return "gfund/biz/businessmng/EgovPhotoInfoForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 수정 후 목록화면으로 이동한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoSelectUpdt.do", "/gfund/biz/businessmng/EgovPhotoInfoSelectUpdt.do"})
	public String EgovPhotoInfoSelectUpdt(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}
		}
		
		// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
		PhotoInfoVo uptPhotoInfoVo = photoInfoService.selectPhotoinfo(photoInfoVo);
		
		// XSS 방지처리
		uptPhotoInfoVo.setNttSj( NkrefoUtil.descript(photoInfoVo.getNttSj()) );
		uptPhotoInfoVo.setNttCn( NkrefoUtil.descript(photoInfoVo.getNttCn()) );
		
		uptPhotoInfoVo.setMakeBizCl(photoInfoVo.getMakeBizCl());
		uptPhotoInfoVo.setFrtExprnCl(photoInfoVo.getFrtExprnCl());
		
		uptPhotoInfoVo.setLastUpdusrId(photoInfoVo.getLastUpdusrId());
		
		// 첨부파일 정보 갱신 처리
		if(photoInfoVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(photoInfoVo.getFileGroupId());
			fvo.setAtchFileId(photoInfoVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		
		if (photoInfoService.updatePhotoinfo(uptPhotoInfoVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
			
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/businessmng/EgovPhotoInfoForm";
			}else{
				return "gfund/biz/businessmng/EgovPhotoInfoForm.tiles";
			}
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
		}
	}
	
	/**
	 * 사업관리 > 사진정보 조회 화면으로 이동한다.
	 * 
	 * @param photoInfoVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPhotoInfoSelectView.do", "/gfund/biz/businessmng/EgovPhotoInfoSelectView.do"})
	public String EgovPhotoInfoSelectView(@ModelAttribute("searchVO") PhotoInfoVo photoInfoVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		if (EgovStringUtil.isEmpty(photoInfoVo.getPhotoInfoId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPhotoInfoList.do";
			}
		}
		
		PhotoInfoVo result = photoInfoService.selectPhotoinfo(photoInfoVo);
		
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		if ("BTC01".equals(bizTyCode) || "BTC02".equals(bizTyCode) || "BTC03".equals(bizTyCode)) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			result.setBizType("BIZ");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			result.setBizType("EDU");
		}
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// BTC01: 복지시설(사회복지시설)나눔숲
		// BTC02: 복지시설(특수교육시설)나눔숲
		// BTC03: 지역사회 나눔숲
		// BTC04: 체험교육 나눔숲
		// BTC05: 숲 체험교육		
		if ("BIZ".equals(result.getBizType())) { // 나눔숲 3가지 인 경우는 조성사업으로 조회
			comDefaultCodeVO.setCodeId("COM250");
		} else { // 숲체헙교육 2가지 인 경우는 체험교육으로 조회
			comDefaultCodeVO.setCodeId("COM251");
		}
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));		
		model.addAttribute("photoInfoVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPhotoInfoInquire";
		}else{
			return "gfund/biz/businessmng/EgovPhotoInfoInquire.tiles";
		}
	}

}
