package egovframework.com.gfund.biz.planacmslt.service;

import java.sql.SQLException;

public interface EgovPlanAcmsltService {
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param planAcmsltVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertPlanAcmslt(PlanAcmsltVo planAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param planAcmsltVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updatePlanAcmslt(PlanAcmsltVo planAcmsltVo) throws SQLException ;
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에서 자료 한건을 삭제한다.
     * @param planAcmsltVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deletePlanAcmslt(PlanAcmsltVo planAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param planAcmsltVo 자료 기본정보
     * @return prufPapersVo 자료 상제정보
     * @throws Exception
     */
    public PlanAcmsltVo selectPlanAcmslt(PlanAcmsltVo planAcmsltVo) throws SQLException ;
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에서 특정 사업에 대한 자료 목록의 전체 개수를 가져온다.
     * @param photoInfoVo 검색조건
     * @return int 특정 사업에 대한 자료 목록의 전체 개수
     * @throws Exception
     */
    public int selectPlanAcmsltTotCnt(PlanAcmsltVo planAcmsltVo) throws SQLException ;
    
}
