/**
 * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에 관한 VO 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.planacmslt.service;

import java.io.Serializable;

import egovframework.com.cmm.ComDefaultVO;

@SuppressWarnings("serial")
public class PlanAcmsltVo extends ComDefaultVO implements Serializable {
	
	/** BIZ_ID, 사업ID */
	private String bizId;
		
	/** MT_1_PRTN_PLAN, 1월 추진계획 */
	private String mt1PrtnPlan;
	
	/** MT_2_PRTN_PLAN, 2월 추진계획 */
	private String mt2PrtnPlan;
	
	/** MT_3_PRTN_PLAN, 3월 추진계획 */
	private String mt3PrtnPlan;
	
	/** MT_4_PRTN_PLAN, 4월 추진계획 */
	private String mt4PrtnPlan;
	
	/** MT_5_PRTN_PLAN, 5월 추진계획 */
	private String mt5PrtnPlan; 
	
	/** MT_6_PRTN_PLAN, 6월 추진계획 */
	private String mt6PrtnPlan;
	
	/** MT_7_PRTN_PLAN, 7월 추진계획 */
	private String mt7PrtnPlan;
		
	/** MT_8_PRTN_PLAN, 8월 추진계획 */
	private String mt8PrtnPlan;
	
	/** MT_9_PRTN_PLAN, 9월 추진계획 */
	private String mt9PrtnPlan;
	
	/** MT_10_PRTN_PLAN, 10월 추진계획 */
	private String mt10PrtnPlan;
	
	/** MT_11_PRTN_PLAN, 11월 추진계획 */
	private String mt11PrtnPlan;
	
	/** MT_12_PRTN_PLAN, 12월 추진계획 */
	private String mt12PrtnPlan;
	
	/** MT_1_PRTN_ACMSLT, 1월 추진실적 */
	private String mt1PrtnAcmslt;
	
	/** MT_2_PRTN_ACMSLT, 2월 추진실적 */
	private String mt2PrtnAcmslt;
	
	/** MT_3_PRTN_ACMSLT, 3월 추진실적 */
	private String mt3PrtnAcmslt;
	
	/** MT_4_PRTN_ACMSLT, 4월 추진실적 */
	private String mt4PrtnAcmslt;
	
	/** MT_5_PRTN_ACMSLT, 5월 추진실적 */
	private String mt5PrtnAcmslt;
	
	/** MT_6_PRTN_ACMSLT, 6월 추진실적 */
	private String mt6PrtnAcmslt;
	
	/** MT_7_PRTN_ACMSLT, 7월 추진실적 */
	private String mt7PrtnAcmslt;
	
	/** MT_8_PRTN_ACMSLT, 8월 추진실적 */
	private String mt8PrtnAcmslt;
	
	/** MT_9_PRTN_ACMSLT, 9월 추진실적 */
	private String mt9PrtnAcmslt;
	
	/** MT_10_PRTN_ACMSLT, 10월 추진실적 */
	private String mt10PrtnAcmslt;
	
	/** MT_11_PRTN_ACMSLT, 11월 추진실적 */
	private String mt11PrtnAcmslt;
	
	/** MT_12_PRTN_ACMSLT, 12월 추진실적 */
	private String mt12PrtnAcmslt;
	
	/** FRST_REGIST_PNTTM, 최초등록시점 */
	private java.util.Date frstRegistPnttm;
	
	/** FRST_REGISTER_ID, 최초등록자ID */
	private String frstRegisterId;
	
	/** LAST_UPDUSR_PNTTM, 최종수정시점 */
	private java.util.Date lastUpdusrPnttm;
	
	/** LAST_UPDUSR_ID, 최종수정자ID */
	private String lastUpdusrId;

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getMt1PrtnPlan() {
		return mt1PrtnPlan;
	}

	public void setMt1PrtnPlan(String mt1PrtnPlan) {
		this.mt1PrtnPlan = mt1PrtnPlan;
	}

	public String getMt2PrtnPlan() {
		return mt2PrtnPlan;
	}

	public void setMt2PrtnPlan(String mt2PrtnPlan) {
		this.mt2PrtnPlan = mt2PrtnPlan;
	}

	public String getMt3PrtnPlan() {
		return mt3PrtnPlan;
	}

	public void setMt3PrtnPlan(String mt3PrtnPlan) {
		this.mt3PrtnPlan = mt3PrtnPlan;
	}

	public String getMt4PrtnPlan() {
		return mt4PrtnPlan;
	}

	public void setMt4PrtnPlan(String mt4PrtnPlan) {
		this.mt4PrtnPlan = mt4PrtnPlan;
	}

	public String getMt5PrtnPlan() {
		return mt5PrtnPlan;
	}

	public void setMt5PrtnPlan(String mt5PrtnPlan) {
		this.mt5PrtnPlan = mt5PrtnPlan;
	}

	public String getMt6PrtnPlan() {
		return mt6PrtnPlan;
	}

	public void setMt6PrtnPlan(String mt6PrtnPlan) {
		this.mt6PrtnPlan = mt6PrtnPlan;
	}

	public String getMt7PrtnPlan() {
		return mt7PrtnPlan;
	}

	public void setMt7PrtnPlan(String mt7PrtnPlan) {
		this.mt7PrtnPlan = mt7PrtnPlan;
	}

	public String getMt8PrtnPlan() {
		return mt8PrtnPlan;
	}

	public void setMt8PrtnPlan(String mt8PrtnPlan) {
		this.mt8PrtnPlan = mt8PrtnPlan;
	}

	public String getMt9PrtnPlan() {
		return mt9PrtnPlan;
	}

	public void setMt9PrtnPlan(String mt9PrtnPlan) {
		this.mt9PrtnPlan = mt9PrtnPlan;
	}

	public String getMt10PrtnPlan() {
		return mt10PrtnPlan;
	}

	public void setMt10PrtnPlan(String mt10PrtnPlan) {
		this.mt10PrtnPlan = mt10PrtnPlan;
	}

	public String getMt11PrtnPlan() {
		return mt11PrtnPlan;
	}

	public void setMt11PrtnPlan(String mt11PrtnPlan) {
		this.mt11PrtnPlan = mt11PrtnPlan;
	}

	public String getMt12PrtnPlan() {
		return mt12PrtnPlan;
	}

	public void setMt12PrtnPlan(String mt12PrtnPlan) {
		this.mt12PrtnPlan = mt12PrtnPlan;
	}

	public String getMt1PrtnAcmslt() {
		return mt1PrtnAcmslt;
	}

	public void setMt1PrtnAcmslt(String mt1PrtnAcmslt) {
		this.mt1PrtnAcmslt = mt1PrtnAcmslt;
	}

	public String getMt2PrtnAcmslt() {
		return mt2PrtnAcmslt;
	}

	public void setMt2PrtnAcmslt(String mt2PrtnAcmslt) {
		this.mt2PrtnAcmslt = mt2PrtnAcmslt;
	}

	public String getMt3PrtnAcmslt() {
		return mt3PrtnAcmslt;
	}

	public void setMt3PrtnAcmslt(String mt3PrtnAcmslt) {
		this.mt3PrtnAcmslt = mt3PrtnAcmslt;
	}

	public String getMt4PrtnAcmslt() {
		return mt4PrtnAcmslt;
	}

	public void setMt4PrtnAcmslt(String mt4PrtnAcmslt) {
		this.mt4PrtnAcmslt = mt4PrtnAcmslt;
	}

	public String getMt5PrtnAcmslt() {
		return mt5PrtnAcmslt;
	}

	public void setMt5PrtnAcmslt(String mt5PrtnAcmslt) {
		this.mt5PrtnAcmslt = mt5PrtnAcmslt;
	}

	public String getMt6PrtnAcmslt() {
		return mt6PrtnAcmslt;
	}

	public void setMt6PrtnAcmslt(String mt6PrtnAcmslt) {
		this.mt6PrtnAcmslt = mt6PrtnAcmslt;
	}

	public String getMt7PrtnAcmslt() {
		return mt7PrtnAcmslt;
	}

	public void setMt7PrtnAcmslt(String mt7PrtnAcmslt) {
		this.mt7PrtnAcmslt = mt7PrtnAcmslt;
	}

	public String getMt8PrtnAcmslt() {
		return mt8PrtnAcmslt;
	}

	public void setMt8PrtnAcmslt(String mt8PrtnAcmslt) {
		this.mt8PrtnAcmslt = mt8PrtnAcmslt;
	}

	public String getMt9PrtnAcmslt() {
		return mt9PrtnAcmslt;
	}

	public void setMt9PrtnAcmslt(String mt9PrtnAcmslt) {
		this.mt9PrtnAcmslt = mt9PrtnAcmslt;
	}

	public String getMt10PrtnAcmslt() {
		return mt10PrtnAcmslt;
	}

	public void setMt10PrtnAcmslt(String mt10PrtnAcmslt) {
		this.mt10PrtnAcmslt = mt10PrtnAcmslt;
	}

	public String getMt11PrtnAcmslt() {
		return mt11PrtnAcmslt;
	}

	public void setMt11PrtnAcmslt(String mt11PrtnAcmslt) {
		this.mt11PrtnAcmslt = mt11PrtnAcmslt;
	}

	public String getMt12PrtnAcmslt() {
		return mt12PrtnAcmslt;
	}

	public void setMt12PrtnAcmslt(String mt12PrtnAcmslt) {
		this.mt12PrtnAcmslt = mt12PrtnAcmslt;
	}

	public java.util.Date getFrstRegistPnttm() {
		return frstRegistPnttm;
	}

	public void setFrstRegistPnttm(java.util.Date frstRegistPnttm) {
		this.frstRegistPnttm = frstRegistPnttm;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}	
	
}
