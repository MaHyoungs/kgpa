/**
 * 녹색자금통합관리시스템 > 사업관리 > 사업 추진계획 및 실적 자료관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.planacmslt.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.planacmslt.service.EgovPlanAcmsltService;
import egovframework.com.gfund.biz.planacmslt.service.PlanAcmsltVo;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class EgovPlanAcmsltController {
	
	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "planAcmsltService")
	private EgovPlanAcmsltService planAcmsltService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	/**
	 * 사업관리 > 사업현황 > 월별사업추진계획 및 실적 수정화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do", "/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do"})
	public String EgovPlanAcmsltView(@ModelAttribute("searchVO") PlanAcmsltVo planAcmsltVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		planAcmsltVo.setBizId(bizId);

		// 월별사업추진계획 및 실적 존재여부 판단
		int totCnt = planAcmsltService.selectPlanAcmsltTotCnt(planAcmsltVo);
		if (totCnt > 0) { // 존재한다면 기존 레코드 조회
			PlanAcmsltVo result = planAcmsltService.selectPlanAcmslt(planAcmsltVo);
			
			// enscript 처리
			// 월별 사업추진계획
			result.setMt1PrtnPlan( NkrefoUtil.enscript(result.getMt1PrtnPlan()) );
			result.setMt2PrtnPlan( NkrefoUtil.enscript(result.getMt2PrtnPlan()) );
			result.setMt3PrtnPlan( NkrefoUtil.enscript(result.getMt3PrtnPlan()) );
			result.setMt4PrtnPlan( NkrefoUtil.enscript(result.getMt4PrtnPlan()) );
			result.setMt5PrtnPlan( NkrefoUtil.enscript(result.getMt5PrtnPlan()) );
			result.setMt6PrtnPlan( NkrefoUtil.enscript(result.getMt6PrtnPlan()) );
			result.setMt7PrtnPlan( NkrefoUtil.enscript(result.getMt7PrtnPlan()) );
			result.setMt8PrtnPlan( NkrefoUtil.enscript(result.getMt8PrtnPlan()) );
			result.setMt9PrtnPlan( NkrefoUtil.enscript(result.getMt9PrtnPlan()) );
			result.setMt10PrtnPlan( NkrefoUtil.enscript(result.getMt10PrtnPlan()) );
			result.setMt11PrtnPlan( NkrefoUtil.enscript(result.getMt11PrtnPlan()) );
			result.setMt12PrtnPlan( NkrefoUtil.enscript(result.getMt12PrtnPlan()) );
			// 월별 사업추진실적
			result.setMt1PrtnAcmslt( NkrefoUtil.enscript(result.getMt1PrtnAcmslt()) );
			result.setMt2PrtnAcmslt( NkrefoUtil.enscript(result.getMt2PrtnAcmslt()) );
			result.setMt3PrtnAcmslt( NkrefoUtil.enscript(result.getMt3PrtnAcmslt()) );
			result.setMt4PrtnAcmslt( NkrefoUtil.enscript(result.getMt4PrtnAcmslt()) );
			result.setMt5PrtnAcmslt( NkrefoUtil.enscript(result.getMt5PrtnAcmslt()) );
			result.setMt6PrtnAcmslt( NkrefoUtil.enscript(result.getMt6PrtnAcmslt()) );
			result.setMt7PrtnAcmslt( NkrefoUtil.enscript(result.getMt7PrtnAcmslt()) );
			result.setMt8PrtnAcmslt( NkrefoUtil.enscript(result.getMt8PrtnAcmslt()) );
			result.setMt9PrtnAcmslt( NkrefoUtil.enscript(result.getMt9PrtnAcmslt()) );
			result.setMt10PrtnAcmslt( NkrefoUtil.enscript(result.getMt10PrtnAcmslt()) );
			result.setMt11PrtnAcmslt( NkrefoUtil.enscript(result.getMt11PrtnAcmslt()) );
			result.setMt12PrtnAcmslt( NkrefoUtil.enscript(result.getMt12PrtnAcmslt()) );
					
			model.addAttribute("planAcmsltVo", result);
		}
		
		if ("BTC01".equals(bizTyCode) || "BTC02".equals(bizTyCode) || "BTC03".equals(bizTyCode) || "BTC08".equals(bizTyCode)) { // 나눔숲 3가지 인 경우는 조성사업으로 처리
			model.addAttribute("bizType", "BIZ");
		} else { // 숲체헙교육 2가지 인 경우는 비조성사업(체험교육)으로 처리
			model.addAttribute("bizType", "EDU");
		}
		
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", planAcmsltVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPlanAcmsltForm";
		}else{
			return "gfund/biz/businessmng/EgovPlanAcmsltForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서  삽입, 수정 후 이전화면으로 이동한다.
	 * 
	 * @param planAcmsltVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPlanAcmsltSelectUpdt.do", "/gfund/biz/businessmng/EgovPlanAcmsltSelectUpdt.do"})
	public String EgovPlanAcmsltSelectUpdt(@ModelAttribute("searchVO") PlanAcmsltVo planAcmsltVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/businessmng/EgovPlanAcmsltForm";
			}else{
				return "gfund/biz/businessmng/EgovPlanAcmsltForm.tiles";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		PlanAcmsltVo uptplanAcmsltVo = null;
		
		// 월별사업추진계획 및 실적 존재여부 판단
		planAcmsltVo.setBizId(bizId);
		int totCnt = planAcmsltService.selectPlanAcmsltTotCnt(planAcmsltVo);
		if (totCnt > 0) { // 존재한다면 기존 레코드 조회
			// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
			uptplanAcmsltVo = planAcmsltService.selectPlanAcmslt(planAcmsltVo);
			uptplanAcmsltVo.setLastUpdusrId(planAcmsltVo.getLastUpdusrId());
		} else {
			uptplanAcmsltVo = planAcmsltVo;
		}
		
		// XSS 방지처리
		// 월별 사업추진계획
		uptplanAcmsltVo.setMt1PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt1PrtnPlan()) );
		uptplanAcmsltVo.setMt2PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt2PrtnPlan()) );
		uptplanAcmsltVo.setMt3PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt3PrtnPlan()) );
		uptplanAcmsltVo.setMt4PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt4PrtnPlan()) );
		uptplanAcmsltVo.setMt5PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt5PrtnPlan()) );
		uptplanAcmsltVo.setMt6PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt6PrtnPlan()) );
		uptplanAcmsltVo.setMt7PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt7PrtnPlan()) );
		uptplanAcmsltVo.setMt8PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt8PrtnPlan()) );
		uptplanAcmsltVo.setMt9PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt9PrtnPlan()) );
		uptplanAcmsltVo.setMt10PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt10PrtnPlan()) );
		uptplanAcmsltVo.setMt11PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt11PrtnPlan()) );
		uptplanAcmsltVo.setMt12PrtnPlan( NkrefoUtil.descript(planAcmsltVo.getMt12PrtnPlan()) );
		// 월별 사업추진실적
		uptplanAcmsltVo.setMt1PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt1PrtnAcmslt()) );
		uptplanAcmsltVo.setMt2PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt2PrtnAcmslt()) );
		uptplanAcmsltVo.setMt3PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt3PrtnAcmslt()) );
		uptplanAcmsltVo.setMt4PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt4PrtnAcmslt()) );
		uptplanAcmsltVo.setMt5PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt5PrtnAcmslt()) );
		uptplanAcmsltVo.setMt6PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt6PrtnAcmslt()) );
		uptplanAcmsltVo.setMt7PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt7PrtnAcmslt()) );
		uptplanAcmsltVo.setMt8PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt8PrtnAcmslt()) );
		uptplanAcmsltVo.setMt9PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt9PrtnAcmslt()) );
		uptplanAcmsltVo.setMt10PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt10PrtnAcmslt()) );
		uptplanAcmsltVo.setMt11PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt11PrtnAcmslt()) );
		uptplanAcmsltVo.setMt12PrtnAcmslt( NkrefoUtil.descript(planAcmsltVo.getMt12PrtnAcmslt()) );
		
		if (totCnt > 0) { // 기존 레코드가 존재한다면
			// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
			if (planAcmsltService.updatePlanAcmslt(uptplanAcmsltVo) > 0) {
				model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
			} else {
				model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
				if(request.getRequestURI().contains("/mng/")){
					return "forward:/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
				}else{
					return "forward:/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
				}
			}
		} else {
			// 신규 레코드 삽입
			planAcmsltService.insertPlanAcmslt(uptplanAcmsltVo);
		}		
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
				
		model.addAttribute("planAcmsltVo", uptplanAcmsltVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do";
		}
	}
	
}
