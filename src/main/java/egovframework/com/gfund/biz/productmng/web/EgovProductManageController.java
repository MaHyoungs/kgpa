/**
 * 녹색자금통합관리시스템 > 결과산출 > 결과산출에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.productmng.web;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.gfund.biz.bassinfo.service.BasicInformationAddInfoVo;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovProductManageController {
	
	Logger log = LogManager.getLogger(this.getClass());

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;
	
	/**
	 * 최종평가결과 수정 Ajax
	 *
	 * @param basicInformationVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/productmng/ajaxUpdateBasicInformationLastEvlResult.do")
	public ModelAndView deleteBusinessScale(@ModelAttribute("basicInformationVo") BasicInformationVo basicInformationVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.updateBasicInformationForEvlRst(basicInformationVo));
		return mav;
	}
	
	/**
	 * 결과산출 > 결과산출 목록 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/productmng/EgovProductManageList.do")
	public String EgovProductManageList(@ModelAttribute("biVo") BasicInformationVo biVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		PaginationInfo paginationInfo = new PaginationInfo();

		paginationInfo.setCurrentPageNo(biVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(biVo.getPageUnit());
		paginationInfo.setPageSize(biVo.getPageSize());

		biVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		biVo.setLastIndex(paginationInfo.getLastRecordIndex());
		biVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		biVo.setPrst_ty_cc("PRST02");
		biVo.setLast_biz_plan_presentn_at("Y"); // 최종사업계획서제출여부 설정

		List<BasicInformationAddInfoVo> bsifList = basicInformationService.selectBusinessBasicInformationAddInfos(biVo);
		int totCnt = basicInformationService.selectBusinessBasicInformationsCount(biVo);

		paginationInfo.setTotalRecordCount(totCnt);

		model.addAttribute("bsifList", bsifList);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("yearlist", greenFundYearService.selectGreenFundYears());
				
		return "mng/gfund/biz/productmng/EgovProductManageList";
	}
	
}
