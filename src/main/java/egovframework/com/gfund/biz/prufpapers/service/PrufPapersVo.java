/**
 * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서 자료관리에 관한 VO 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.prufpapers.service;

import java.io.Serializable;

import egovframework.com.cmm.ComDefaultVO;

@SuppressWarnings("serial")
public class PrufPapersVo  extends ComDefaultVO implements Serializable {
	
	/** RPP_ID, 관련증빙서ID */
	private String rppId;
	
	/** BIZ_ID, 사업ID */
	private String bizId;
	
	/** ATCH_FILE_ID, 첨부파일ID */
	private String atchFileId;
	
	/** PRUF_PAPERS_CL, 증빙서분류(공통코드) */
	private String prufPapersCl; 
	
	/** NTT_SJ, 제목 */
	private String nttSj;
	
	/** NTT_CN, 내용 */
	private String nttCn;
	
	/** FRST_REGIST_PNTTM, 최초등록시점 */
	private java.util.Date frstRegistPnttm;
	
	/** FRST_REGISTER_ID, 최초등록자ID */
	private String frstRegisterId;
	
	/** LAST_UPDUSR_PNTTM, 최종수정시점 */
	private java.util.Date lastUpdusrPnttm;	
	
	/** LAST_UPDUSR_ID, 최종수정자ID */
	private String lastUpdusrId;
	
	/** 임시첨부파일 그룹아이디  */
	private String fileGroupId = "";

	public String getRppId() {
		return rppId;
	}

	public void setRppId(String rppId) {
		this.rppId = rppId;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public String getPrufPapersCl() {
		return prufPapersCl;
	}

	public void setPrufPapersCl(String prufPapersCl) {
		this.prufPapersCl = prufPapersCl;
	}

	public String getNttSj() {
		return nttSj;
	}

	public void setNttSj(String nttSj) {
		this.nttSj = nttSj;
	}

	public String getNttCn() {
		return nttCn;
	}

	public void setNttCn(String nttCn) {
		this.nttCn = nttCn;
	}

	public java.util.Date getFrstRegistPnttm() {
		return frstRegistPnttm;
	}

	public void setFrstRegistPnttm(java.util.Date frstRegistPnttm) {
		this.frstRegistPnttm = frstRegistPnttm;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public String getFileGroupId() {
		return fileGroupId;
	}

	public void setFileGroupId(String fileGroupId) {
		this.fileGroupId = fileGroupId;
	}

}
