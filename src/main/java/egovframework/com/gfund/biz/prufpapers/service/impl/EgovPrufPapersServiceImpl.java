package egovframework.com.gfund.biz.prufpapers.service.impl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.gfund.biz.prufpapers.service.EgovPrufPapersService;
import egovframework.com.gfund.biz.prufpapers.service.PrufPapersVo;

@Service("prufPapersService")
public class EgovPrufPapersServiceImpl implements EgovPrufPapersService {

	@Resource(name = "prufPapersDao")
	private PrufPapersDao prufPapersDao;
	
	/**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 자료의 정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param prufPapersVo 자료 등록정보
     * @return String result 등록결과
     */
    public String insertPrufPapers(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.insertPrufPapers(prufPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 화면에 조회된 자료의 정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param prufPapersVo 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updatePrufPapers(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.updatePrufPapers(prufPapersVo);
    }
	
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 자료 한건을 삭제한다.
     * @param prufPapersVo 자로 삭제정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int deletePrufPapers(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.deletePrufPapers(prufPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param prufPapersVo 자료 기본정보
     * @return prufPapersVo 자료 상제정보
     * @throws Exception
     */
    public PrufPapersVo selectPrufPapers(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.selectPrufPapers(prufPapersVo);
    }

    /**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param prufPapersVo 검색조건
     * @return List 자료 목록정보
     * @throws Exception
     */
    public List<?> selectPrufPapersList(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.selectPrufPapersList(prufPapersVo);
    }
    
    /**
     * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서에서 특정 사업에 대한 자료 목록의 전체 개수를 가져온다.
     * @param prufPapersVo 검색조건
     * @return int 특정 사업에 대한 자료 목록의 전체 개수
     * @throws SQLException
     */
    public int selectPrufPapersListTotCnt(PrufPapersVo prufPapersVo) throws SQLException {
    	return prufPapersDao.selectPrufPapersListTotCnt(prufPapersVo);
    }

}
