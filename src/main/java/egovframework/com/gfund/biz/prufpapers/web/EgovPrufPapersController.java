/**
 * 녹색자금통합관리시스템 > 사업관리 > 관련증빙서 자료관리에 관한 사용자용 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.prufpapers.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.prufpapers.service.EgovPrufPapersService;
import egovframework.com.gfund.biz.prufpapers.service.PrufPapersVo;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovPrufPapersController {
	
	Logger log = LogManager.getLogger(this.getClass());
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "prufPapersService")
	private EgovPrufPapersService prufPapersService;
	
	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService EgovCmmUseService;
	
	@Resource(name = "egovGFRelatePrufPapersGnrService")
	private EgovIdGnrService idgenService;
	
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	/**
	 * 사업관리 > 관련증빙서 목록 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersList.do", "/gfund/biz/businessmng/EgovPrufPapersList.do"})
	public String EgovPrufPapersList(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		String bizId = (String) commandMap.get("biz_id");
		String bizTyCode = (String) commandMap.get("biz_ty_code");
		
		PaginationInfo paginationInfo = new PaginationInfo();
		
		// 페이징 정보 설정
		prufPapersVo.setPageUnit(propertiesService.getInt("pageUnit"));
		prufPapersVo.setPageSize(propertiesService.getInt("pageSize"));
		
		paginationInfo.setCurrentPageNo(prufPapersVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(prufPapersVo.getPageUnit());
		paginationInfo.setPageSize(prufPapersVo.getPageSize());
		
		prufPapersVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		prufPapersVo.setLastIndex(paginationInfo.getLastRecordIndex());
		prufPapersVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		prufPapersVo.setBizId(bizId);
		
		List<PrufPapersVo> resultList = (List<PrufPapersVo>) prufPapersService.selectPrufPapersList(prufPapersVo);
		int totCnt = prufPapersService.selectPrufPapersListTotCnt(prufPapersVo);
		
		paginationInfo.setTotalRecordCount(totCnt);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// PRUF01: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계도면
		// PRUF02: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계내역서
		// PRUF03: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 기술자문의견서(설계) 
		// PRUF04: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 준공계
		// PRUF05: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 지도점검표
		// PRUF06: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 체크리스트
		// PRUF07: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 회계증빙서류(e-호조, 영수증사본 등)
		// PRUF08: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비 통장 내역 사본
		// PRUF09: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비관리시스템 내역
		// PRUF10: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 홍보자료
		// PRUF11: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사후관리협약서
		comDefaultCodeVO.setCodeId("COM252");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPrufPapersList";
		}else{
			return "gfund/biz/businessmng/EgovPrufPapersList.tiles";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서 등록화면으로 이동한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersAddView.do", "/gfund/biz/businessmng/EgovPrufPapersAddView.do"})
	public String EgovPrufPapersAddView(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// PRUF01: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계도면
		// PRUF02: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계내역서
		// PRUF03: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 기술자문의견서(설계) 
		// PRUF04: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 준공계
		// PRUF05: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 지도점검표
		// PRUF06: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 체크리스트
		// PRUF07: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 회계증빙서류(e-호조, 영수증사본 등)
		// PRUF08: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비 통장 내역 사본
		// PRUF09: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비관리시스템 내역
		// PRUF10: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 홍보자료
		// PRUF11: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사후관리협약서
		comDefaultCodeVO.setCodeId("COM252");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		model.addAttribute("bizId", prufPapersVo.getBizId());
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", prufPapersVo); // YS1: 등록화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPrufPapersForm";
		}else{
			return "gfund/biz/businessmng/EgovPrufPapersForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서 등록처리 후 목록화면으로 이동한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersInsert.do", "/gfund/biz/businessmng/EgovPrufPapersInsert.do"})
	public String insertEgovPrufPapers(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 등록처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
			}
		}
		
		String bizId = (String) commandMap.get("biz_id");
		
		prufPapersVo.setBizId(bizId);
		// XSS 방지처리
		prufPapersVo.setNttSj( NkrefoUtil.descript(prufPapersVo.getNttSj()) );
		prufPapersVo.setNttCn( NkrefoUtil.descript(prufPapersVo.getNttCn()) );
		
		// 신규 관련증빙서ID 발급 및 레코드 삽입
		prufPapersVo.setRppId(idgenService.getNextStringId());
		prufPapersService.insertPrufPapers(prufPapersVo);
		
		if(prufPapersVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(prufPapersVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		
		// YS3: 등록처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서 목록화면에서 자료 1건을 삭제 처리를 한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersDelete.do", "/gfund/biz/businessmng/EgovPrufPapersDelete.do"})
	public String deleteEgovPrufPapers(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(prufPapersVo.getRppId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
			}
		}
		
		PrufPapersVo papersVo = prufPapersService.selectPrufPapers(prufPapersVo);
		String atchFileId = papersVo.getAtchFileId();
		// 첨부파일 삭제처리
		if (!EgovStringUtil.isEmpty(atchFileId)) fileMngService.deleteFileInfs(atchFileId);		
		
		// 레코드 삭제처리
		if (prufPapersService.deletePrufPapers(papersVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.delete"));
		}

		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서 수정 화면으로 이동한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersUpdtView.do", "/gfund/biz/businessmng/EgovPrufPapersUpdtView.do"})
	public String EgovPrufPapersUpdtView(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		if (EgovStringUtil.isEmpty(prufPapersVo.getRppId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
			}
		}
		
		PrufPapersVo result = prufPapersService.selectPrufPapers(prufPapersVo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// PRUF01: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계도면
		// PRUF02: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계내역서
		// PRUF03: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 기술자문의견서(설계) 
		// PRUF04: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 준공계
		// PRUF05: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 지도점검표
		// PRUF06: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 체크리스트
		// PRUF07: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 회계증빙서류(e-호조, 영수증사본 등)
		// PRUF08: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비 통장 내역 사본
		// PRUF09: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비관리시스템 내역
		// PRUF10: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 홍보자료
		// PRUF11: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사후관리협약서
		comDefaultCodeVO.setCodeId("COM252");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));
		
		// enscript 처리
		result.setNttSj( NkrefoUtil.enscript(result.getNttSj()) );
		result.setNttCn( NkrefoUtil.enscript(result.getNttCn()) );
		
		model.addAttribute("prufPapersVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		request.getSession().setAttribute("sessionVO", prufPapersVo); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPrufPapersForm";
		}else{
			return "gfund/biz/businessmng/EgovPrufPapersForm.tiles";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서  수정 후 목록화면으로 이동한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersSelectUpdt.do", "/gfund/biz/businessmng/EgovPrufPapersSelectUpdt.do"})
	public String EgovPrufPapersSelectUpdt(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
			}
		}
		
		// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
		PrufPapersVo uptPrufPapersVo = prufPapersService.selectPrufPapers(prufPapersVo);
		
		// XSS 방지처리
		uptPrufPapersVo.setNttSj( NkrefoUtil.descript(prufPapersVo.getNttSj()) );
		uptPrufPapersVo.setNttCn( NkrefoUtil.descript(prufPapersVo.getNttCn()) );
		
		uptPrufPapersVo.setPrufPapersCl(prufPapersVo.getPrufPapersCl());
		
		uptPrufPapersVo.setLastUpdusrId(prufPapersVo.getLastUpdusrId());
		
		// 첨부파일 정보 갱신 처리
		if(prufPapersVo.getAtchFileId() != null){
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(prufPapersVo.getFileGroupId());
			fvo.setAtchFileId(prufPapersVo.getAtchFileId());
			fileMngService.insertFileInfsByTemp(fvo);
		}
		
		if (prufPapersService.updatePrufPapers(uptPrufPapersVo) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.update"));
			
			String bizId = (String) commandMap.get("biz_id");
			BasicInformationVo selectVo = new BasicInformationVo();
			selectVo.setBiz_id(bizId);
			BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
			model.addAttribute("basicInformationVo", basicInformationVo);
			
			if(request.getRequestURI().contains("/mng/")){
				return "mng/gfund/biz/businessmng/EgovPrufPapersForm";
			}else{
				return "gfund/biz/businessmng/EgovPrufPapersForm.tiles";
			}
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		if(request.getRequestURI().contains("/mng/")){
			return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
		}else{
			return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
		}
	}
	
	/**
	 * 사업관리 > 관련증빙서 조회 화면으로 이동한다.
	 * 
	 * @param prufPapersVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/mng/gfund/biz/businessmng/EgovPrufPapersSelectView.do", "/gfund/biz/businessmng/EgovPrufPapersSelectView.do"})
	public String EgovPrufPapersSelectView(@ModelAttribute("searchVO") PrufPapersVo prufPapersVo, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		if (EgovStringUtil.isEmpty(prufPapersVo.getRppId())) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
			
			if(request.getRequestURI().contains("/mng/")){
				return "forward:/mng/gfund/biz/businessmng/EgovPrufPapersList.do";
			}else{
				return "forward:/gfund/biz/businessmng/EgovPrufPapersList.do";
			}
		}
		
		PrufPapersVo result = prufPapersService.selectPrufPapers(prufPapersVo);
		
		// 공통코드정보를 조회한다.
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		
		// PRUF01: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계도면
		// PRUF02: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 설계내역서
		// PRUF03: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 기술자문의견서(설계) 
		// PRUF04: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 준공계
		// PRUF05: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 지도점검표
		// PRUF06: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 체크리스트
		// PRUF07: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 회계증빙서류(e-호조, 영수증사본 등)
		// PRUF08: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비 통장 내역 사본
		// PRUF09: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사업비관리시스템 내역
		// PRUF10: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 홍보자료
		// PRUF11: 녹색자금통합관리시스템 - 관련증빙서 > 증빙서분류코드 > 사후관리협약서
		comDefaultCodeVO.setCodeId("COM252");
		
		model.addAttribute("cmmCodeList", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));				
		model.addAttribute("prufPapersVo", result);
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		if(request.getRequestURI().contains("/mng/")){
			return "mng/gfund/biz/businessmng/EgovPrufPapersInquire";
		}else{
			return "gfund/biz/businessmng/EgovPrufPapersInquire.tiles";
		}
	}
	
}
