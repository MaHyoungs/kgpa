package egovframework.com.gfund.biz.selection.service;


import java.sql.SQLException;
import java.util.List;

public interface BizSelectionMngService {

	/**
	 * 선정전형 심사 등록
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public int insertBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException;

	/**
	 * 선정전형 심사 수정
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException;

	/**
	 * 선정전형 심사 삭제
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException;

	/**
	 * 선정전형 심사 조회
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public BizSelectionMngVo selectBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException;

	/**
	 * 선정전형 심사 목록 조회
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public List<BizSelectionMngVo> selectBizSelectionStepStates(BizSelectionMngVo bizSelectionMngVo) throws SQLException;
}
