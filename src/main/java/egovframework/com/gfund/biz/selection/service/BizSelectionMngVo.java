package egovframework.com.gfund.biz.selection.service;

public class BizSelectionMngVo {
	private String jdgmn_id;
	private String biz_id;
	private String atch_file_id;
	private String gatch_file_id;
	private String step_cl;
	private String step_cl_use;
	private String expert_opinion;
	private String frst_regist_pnttm;
	private String frst_register_id;
	private String last_updusr_pnttm;
	private String last_updusr_id;
	private String step_cl_nm;
	private String propse_flag;

	public String getJdgmn_id() {
		return jdgmn_id;
	}

	public void setJdgmn_id(String jdgmn_id) {
		this.jdgmn_id = jdgmn_id;
	}

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public String getAtch_file_id() {
		return atch_file_id;
	}

	public void setAtch_file_id(String atch_file_id) {
		this.atch_file_id = atch_file_id;
	}

	public String getGatch_file_id() {
		return gatch_file_id;
	}

	public void setGatch_file_id(String gatch_file_id) {
		this.gatch_file_id = gatch_file_id;
	}

	public String getStep_cl() {
		return step_cl;
	}

	public void setStep_cl(String step_cl) {
		this.step_cl = step_cl;
	}

	public String getExpert_opinion() {
		return expert_opinion;
	}

	public void setExpert_opinion(String expert_opinion) {
		this.expert_opinion = expert_opinion;
	}

	public String getFrst_regist_pnttm() {
		return frst_regist_pnttm;
	}

	public void setFrst_regist_pnttm(String frst_regist_pnttm) {
		this.frst_regist_pnttm = frst_regist_pnttm;
	}

	public String getFrst_register_id() {
		return frst_register_id;
	}

	public void setFrst_register_id(String frst_register_id) {
		this.frst_register_id = frst_register_id;
	}

	public String getLast_updusr_pnttm() {
		return last_updusr_pnttm;
	}

	public void setLast_updusr_pnttm(String last_updusr_pnttm) {
		this.last_updusr_pnttm = last_updusr_pnttm;
	}

	public String getLast_updusr_id() {
		return last_updusr_id;
	}

	public void setLast_updusr_id(String last_updusr_id) {
		this.last_updusr_id = last_updusr_id;
	}

	public String getStep_cl_nm() {
		return step_cl_nm;
	}

	public void setStep_cl_nm(String step_cl_nm) {
		this.step_cl_nm = step_cl_nm;
	}

	public String getPropse_flag() {
		return propse_flag;
	}

	public void setPropse_flag(String propse_flag) {
		this.propse_flag = propse_flag;
	}

	public String getStep_cl_use() {
		return step_cl_use;
	}

	public void setStep_cl_use(String step_cl_use) {
		this.step_cl_use = step_cl_use;
	}
}
