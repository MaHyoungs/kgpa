package egovframework.com.gfund.biz.selection.service.impl;

import egovframework.com.gfund.biz.selection.service.BizSelectionMngVo;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository("bizSelectionMngDao")
public class BizSelectionMngDao extends EgovAbstractDAO {

	/**
	 * 선정전형 심사 등록
	 *
	 * @param bizSelectionMngVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return update("bizSelectionMngDao.insertBizSelectionStep", bizSelectionMngVo);
	}

	/**
	 * 선정전형 심사 수정
	 *
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public int updateBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return update("bizSelectionMngDao.updateBizSelectionStep", bizSelectionMngVo);
	}

	/**
	 * 선정전형 심사 삭제
	 *
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return update("bizSelectionMngDao.deleteBizSelectionStep", bizSelectionMngVo);
	}

	/**
	 * 선정전형 심사 조회
	 *
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public BizSelectionMngVo selectBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return (BizSelectionMngVo)selectByPk("bizSelectionMngDao.selectBizSelectionStep", bizSelectionMngVo);
	}

	/**
	 * 선정전형 심사 목록 조회
	 *
	 * @param bizSelectionMngVo
	 * @return
	 * @throws SQLException
	 */
	public List<BizSelectionMngVo> selectBizSelectionStepStates(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return list("bizSelectionMngDao.selectBizSelectionStepStates", bizSelectionMngVo);
	}

}
