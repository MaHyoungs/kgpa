package egovframework.com.gfund.biz.selection.service.impl;

import egovframework.com.gfund.biz.selection.service.BizSelectionMngService;
import egovframework.com.gfund.biz.selection.service.BizSelectionMngVo;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

@Service("bizSelectionMngService")
public class BizSelectionMngServiceImpl extends AbstractServiceImpl implements BizSelectionMngService {

	@Resource(name = "bizSelectionMngDao")
	private BizSelectionMngDao bizSelectionMngDao;

	public int insertBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return bizSelectionMngDao.insertBizSelectionStep(bizSelectionMngVo);
	}

	public int updateBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return bizSelectionMngDao.updateBizSelectionStep(bizSelectionMngVo);
	}

	public int deleteBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return bizSelectionMngDao.deleteBizSelectionStep(bizSelectionMngVo);
	}

	public BizSelectionMngVo selectBizSelectionStep(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return bizSelectionMngDao.selectBizSelectionStep(bizSelectionMngVo);
	}

	public List<BizSelectionMngVo> selectBizSelectionStepStates(BizSelectionMngVo bizSelectionMngVo) throws SQLException {
		return bizSelectionMngDao.selectBizSelectionStepStates(bizSelectionMngVo);
	}
}
