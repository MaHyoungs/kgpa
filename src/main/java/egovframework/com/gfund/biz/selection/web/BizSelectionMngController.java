package egovframework.com.gfund.biz.selection.web;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.gfund.biz.selection.service.BizSelectionMngService;
import egovframework.com.gfund.biz.selection.service.BizSelectionMngVo;
import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

@Controller
public class BizSelectionMngController {

	@Resource(name = "bizSelectionMngService")
	private BizSelectionMngService bizSelectionMngService;

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;

	@Resource(name = "EgovCmmUseService")
	private egovframework.com.cmm.service.EgovCmmUseService EgovCmmUseService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	//선정전형 심사ID 시퀸스 서비스
	@Resource(name = "egovGFBizSelectionService")
	private EgovIdGnrService bizSelectionSeq;

	/**
	 * 선정전형 - 관리자 제안서 목록(사용자 사업관리)
	 *
	 * @param biVo
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/gfund/biz/selection/proposalList.do")
	public String bizSelectionList(@ModelAttribute("bsifVo") BasicInformationVo bsifVo, ModelMap model) throws Exception {

		PaginationInfo paginationInfo = new PaginationInfo();

		paginationInfo.setCurrentPageNo(bsifVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(bsifVo.getPageUnit());
		paginationInfo.setPageSize(bsifVo.getPageSize());

		bsifVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		bsifVo.setLastIndex(paginationInfo.getLastRecordIndex());
		bsifVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		bsifVo.setPrst_ty_cc("PRST01");

		List<BasicInformationVo> bsifList = basicInformationService.selectBusinessBasicInformations(bsifVo);
		int totCnt = basicInformationService.selectBusinessBasicInformationsCount(bsifVo);

		paginationInfo.setTotalRecordCount(totCnt);

		model.addAttribute("bsifList", bsifList);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("yearlist", greenFundYearService.selectGreenFundYears());
		return "mng/gfund/biz/bassinfo/selection/proposalList";
	}

	/**
	 * 선정전형 - 관리자 1차심사,현장심사,2심사 등록 Form & View
	 * @param bsVo
	 * @param model
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping({"/mng/gfund/biz/selection/selectionStepForm.do", "/mng/gfund/biz/selection/selectionStepView.do"})
	public String mngBizSelectionStep(@ModelAttribute("bsVo") BizSelectionMngVo bsVo, ModelMap model, HttpServletRequest request) throws SQLException {
		BasicInformationVo biVo= new BasicInformationVo();
		biVo.setBiz_id(bsVo.getBiz_id());
		model.addAttribute("biVo", basicInformationService.selectBusinessBasicInformation(biVo));
		model.addAttribute("bsVo", bizSelectionMngService.selectBizSelectionStep(bsVo));
		model.addAttribute("bsList", bizSelectionMngService.selectBizSelectionStepStates(bsVo));

		//관리자 심사 등록 Form
		if(request.getRequestURI().contains("selectionStepForm")) {
			return "mng/gfund/biz/bassinfo/selection/selectionStepForm";
		}

		//관리자 심사 조회
		else {
			return "mng/gfund/biz/bassinfo/selection/selectionStepView";
		}
	}

	/**
	 * 선정전형 - 사용자 1차심사,현장심사,2심사 등록 Form & View
	 * @param bsVo
	 * @param model
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/selection/selectionStepView.do")
	public String userBizSelectionStep(@ModelAttribute("biVo") BizSelectionMngVo bsVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);

		//사업분류 공통코드
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("bizTyCc", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));

		if(!"".equals(bsVo.getBiz_id())){
			BasicInformationVo bsifVo= new BasicInformationVo();
			bsifVo.setBiz_id(bsVo.getBiz_id());
			bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
			model.addAttribute("bsifVo", bsifVo);
			model.addAttribute("bsVo", bizSelectionMngService.selectBizSelectionStep(bsVo));
			model.addAttribute("bsList", bizSelectionMngService.selectBizSelectionStepStates(bsVo));

			BasicInformationVo lastVo=new BasicInformationVo();
			lastVo.setPropse_biz_id(bsifVo.getBiz_id());
			model.addAttribute("lastVo", basicInformationService.selectBusinessBasicInformation(lastVo));
		}else{
			model.addAttribute("bsVo", null);
		}
		return "gfund/biz/selection/selectionStepView.tiles";
	}

	/**
	 * 선정전형 - 최종발표 완료
	 * @param bsVo
	 * @param model
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/gfund/biz/bassinfo/selection/selectionCompletion.do")
	public String selectionCompletion(@ModelAttribute("biVo") BizSelectionMngVo bsVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		//사용자 정보 조회
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		UserManageVO userManageVO = new UserManageVO();
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);

		//사업분류 공통코드
		ComDefaultCodeVO comDefaultCodeVO = new ComDefaultCodeVO();
		comDefaultCodeVO.setCodeId("COM210");
		model.addAttribute("bizTyCc", EgovCmmUseService.selectCmmCodeDetail(comDefaultCodeVO));

		BasicInformationVo bsifVo= new BasicInformationVo();
		bsifVo.setBiz_id(bsVo.getBiz_id());
		bsifVo = basicInformationService.selectBusinessBasicInformation(bsifVo);
		model.addAttribute("bsifVo", bsifVo);
		model.addAttribute("bsList", bizSelectionMngService.selectBizSelectionStepStates(bsVo));
		return "gfund/biz/selection/selectionCompletion.tiles";
	}

	/**
	 * 선정전형 - 관리자 1차심사,현장심사,2심사 등록&수정
	 *
	 * @param bsVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/selection/selectionStepSave.do")
	public String selectionStepSave(@ModelAttribute("bsVo") BizSelectionMngVo bsVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		int saveRs = 0;
		if("".equals(bsVo.getJdgmn_id())){
			bsVo.setJdgmn_id(bizSelectionSeq.getNextStringId());
			bsVo.setFrst_register_id(loginVO.getId());
			saveRs = bizSelectionMngService.insertBizSelectionStep(bsVo);
		}else{
			bsVo.setLast_updusr_id(loginVO.getId());
			saveRs = bizSelectionMngService.updateBizSelectionStep(bsVo);
		}
		if(saveRs > 0){
			bizSelectionFileUpload(bsVo);
			BasicInformationVo biVo=new BasicInformationVo();
			biVo.setPropse_flag(bsVo.getPropse_flag());
			biVo.setBiz_id(bsVo.getBiz_id());
			basicInformationService.updateBasicInformationStep(biVo);
			model.addAttribute("biVo", basicInformationService.selectBusinessBasicInformation(biVo));
		}

		model.addAttribute("bsVo", bizSelectionMngService.selectBizSelectionStep(bsVo));
		return "mng/gfund/biz/bassinfo/selection/selectionStepView";
	}

	/**
	 * 제안서 관리자 진행상태 변경 Ajax
	 * @param biVo
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/selection/updateBasicInformationStep.do")
	public ModelAndView updateBasicInformationStep(@ModelAttribute("biVo") BasicInformationVo biVo) throws SQLException {
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("rs", basicInformationService.updateBasicInformationStep(biVo));
		return mav;
	}

	/**
	 * 파일 업로드
	 * @param bsVo
	 * @throws Exception
	 */
	public void bizSelectionFileUpload(BizSelectionMngVo bsVo) throws Exception {
		if (!("").equals(bsVo.getAtch_file_id())) {
			FileVO fvo = new FileVO();
			fvo.setFileGroupId(bsVo.getAtch_file_id());
			if(!("").equals(bsVo.getGatch_file_id())) {
				fvo.setAtchFileId(bsVo.getGatch_file_id());
			}else{
				fvo.setAtchFileId(null);
			}
			fileMngService.insertFileInfsByTemp(fvo);
		}
	}
}
