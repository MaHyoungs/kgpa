/**
 * 녹색자금통합관리시스템 > 결과산출 > 설문조사에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.12.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일                   수정자                    수정내용
 *  -------      --------    ---------------------------
 *   2014.12.10  서용식                   최초 생성
 *
 * </pre>
 */

package egovframework.com.gfund.biz.survey.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.evt.service.ComtneventadhrncVO;
import egovframework.com.evt.service.ComtneventaswperVO;
import egovframework.com.evt.service.ComtneventcnsrVO;
import egovframework.com.evt.service.ComtneventqesitmVO;
import egovframework.com.evt.service.ComtneventqesitmexVO;
import egovframework.com.evt.service.ComtnschdulinfoDefaultVO;
import egovframework.com.evt.service.ComtnschdulinfoService;
import egovframework.com.evt.service.ComtnschdulinfoVO;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class EgovSurveyManageController {
	
	Logger log = LogManager.getLogger(this.getClass());

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;
	
	@Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;
	
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
	@Resource(name = "comtnschdulinfoService")
	private ComtnschdulinfoService comtnschdulinfoService;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
    
    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	@Autowired	
	private DefaultBeanValidator beanValidator;
	
	/**
	 * 결과산출 > 결과산출 > 설문조사 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/biz/productmng/EgovSurveyList.do")
	public String EgovSurveyListView(@ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, Map<String, Object> commandMap, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String bizId = (String) commandMap.get("biz_id");
		BasicInformationVo selectVo = new BasicInformationVo();
		selectVo.setBiz_id(bizId);
		BasicInformationVo basicInformationVo = basicInformationService.selectBusinessBasicInformation(selectVo);
		model.addAttribute("basicInformationVo", basicInformationVo);
		
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		
		//공통코드 일정종류
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("COM020");
    	model.addAttribute("schdulSe", cmmUseService.selectCmmCodeDetail(voComCode));
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		//이벤트 웹경로.
	    model.addAttribute("EventFileStoreWebPath", propertyService.getString("Event.fileStoreWebPath"));
		
		searchVO.setSiteId(siteVO.getSiteId());
		
		 String menuId = (String)commandMap.get("menuId");
	    if(!(menuId==null || "".equals(menuId))){
	    	searchVO.setSearchCondition("99");
	    }

        model.addAttribute("resultList", comtnschdulinfoService.selectComtnschdulinfoList_D(searchVO));

        int totCnt = comtnschdulinfoService.selectComtnschdulinfoListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
		
		return "gfund/biz/productmng/EgovSurveyList.tiles";
	}
	
	 /**
	 * 이벤트 상세내용을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 ComtnschdulinfoDefaultVO
	 * @return 
	 * @exception Exception
	 */
    @RequestMapping("/gfund/biz/productmng/selectSurveyInfo.do")
    public String selectSurveyInfo(@ModelAttribute("searchVO") ComtnschdulinfoVO comtnschdulinfoVO, Map<String, Object> commandMap, Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

	    SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);

	    LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	    
    	int evtCheck = 0;

        ComtnschdulinfoVO comtnschdulinfoList = comtnschdulinfoService.selectComtnschdulinfo(comtnschdulinfoVO);
		model.addAttribute("comtnschdulinfoVO", comtnschdulinfoList);

		//이벤트 참여 여부 조회
		if(user != null){
			comtnschdulinfoVO.setUserId(user.getId());
			evtCheck = comtnschdulinfoService.selectComtneventadhrncCnt(comtnschdulinfoVO);
		}
		model.addAttribute("evtCheck", evtCheck);
        return "gfund/biz/productmng/EgovSurveyForm.tiles";
    }
    
    /**
	 * 객관식 또는 주관식 이벤트를 출력한다.
	 * @param searchVO - 조회할 정보가 담긴 ComtnschdulinfoVO
	 * @return 
	 * @exception Exception
	 */
    @RequestMapping("/gfund/biz/productmng/participateSurveyEvent.do")
    public String participateSurveyEvent(ComtnschdulinfoVO comtnschdulinfoVO, @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfoBySiteId("SITE_000000000000001");
  	  	model.addAttribute("siteInfo", siteVO);
  	  	model.addAttribute("LytFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile"));

	    LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

    	comtnschdulinfoVO.setIsError(false);

		if(StringUtils.isEmpty(comtnschdulinfoVO.getSchdulId())) {
			comtnschdulinfoVO.setIsError(true);
			comtnschdulinfoVO.setErrorMessage("올바른 경로로 접근하세요");
		}

		ComtnschdulinfoVO onlineEvent = null;
		if(!comtnschdulinfoVO.getIsError()) {

			onlineEvent = comtnschdulinfoService.selectComtnschduInfo(comtnschdulinfoVO);

			if(onlineEvent != null) {
				if(siteVO.getSiteId().equals("SITE_000000000000001")){
					comtnschdulinfoVO.setUserId(user.getCredtId());
				}else{
					comtnschdulinfoVO.setUserId(user.getId());
				}
				comtnschdulinfoVO.setSchdulId(onlineEvent.getSchdulId());
				comtnschdulinfoVO.setSchdulClCode(onlineEvent.getSchdulClCode());
				comtnschdulinfoVO.setSchdulBgnde(onlineEvent.getSchdulBgnde());
				comtnschdulinfoVO.setSchdulEndde(onlineEvent.getSchdulEndde());
				comtnschdulinfoVO.setPresnatnDe(onlineEvent.getPresnatnDe());
				comtnschdulinfoVO.setSchdulNm(onlineEvent.getSchdulNm());
				comtnschdulinfoVO.setSchdulCn(onlineEvent.getSchdulCn());
				comtnschdulinfoVO.setUpendStreFileNm(onlineEvent.getUpendStreFileNm());
				comtnschdulinfoVO.setMiddleStreFileNm(onlineEvent.getMiddleStreFileNm());
				comtnschdulinfoVO.setLptStreFileNm(onlineEvent.getLptStreFileNm());

				comtnschdulinfoVO.setQuestionList(onlineEvent.getQuestionList());

			} else {
				comtnschdulinfoVO.setIsError(true);
				comtnschdulinfoVO.setErrorMessage("알수없는 이벤트입니다.");
			}
		}
		model.addAttribute("comtnschdulinfoVO", comtnschdulinfoVO);
		request.getSession().setAttribute("sessionVO", comtnschdulinfoVO);
    	return "gfund/biz/productmng/SurveyEventPop";
    }
    
    /**
   	 * 객관식 주관식 이벤트 참여정보를 입력한다.
   	 * @param searchVO - 조회할 정보가 담긴 ComtnschdulinfoDefaultVO
   	 * @return
   	 * @exception Exception
   	 */
	@RequestMapping("/gfund/biz/productmng/addComtnschdulEvent.do")
	public String addComtnschdulEvent(
			@ModelAttribute("searchVO") ComtnschdulinfoDefaultVO comtnschdulinfo,
			ComtnschdulinfoVO comtnschdulinfoVO, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session)
			throws Exception {

		SiteManageVO siteVO = siteManageService.selectSiteServiceInfoBySiteId("SITE_000000000000001");
		model.addAttribute("siteInfo", siteVO);
		model.addAttribute("LytFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile"));

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (user == null) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.user.login"));
			return "gfund/biz/productmng/SurveyEventFinish";
		}

		ComtneventadhrncVO comtneventadhrncVO = new ComtneventadhrncVO();
		comtneventadhrncVO.setUserId(user.getId());		
		comtneventadhrncVO.setFrstRegistIp(EgovClntInfo.getClntIP(request));
		comtneventadhrncVO.setSchdulId(comtnschdulinfoVO.getSchdulId());
		comtneventadhrncVO.setSiteId(siteVO.getSiteId());
		comtneventadhrncVO.setSysTyCode(siteVO.getSysTyCode());

		// 이벤트 중복 참여여부 체크
		if (comtnschdulinfoService.isRegDuplicate(comtneventadhrncVO)) {
			comtnschdulinfoVO.setIsError(true);
			comtnschdulinfoVO.setErrorMessage("이미 참여하셨습니다.");
			return "gfund/biz/productmng/SurveyEventFinish";
		}

		List<ComtneventaswperVO> answerList = new ArrayList<ComtneventaswperVO>();

		if (comtnschdulinfoVO.getQuestionIdList().size() > 0) {
			int essayAnswerPointer = 0;
			for (int index = 0; index < comtnschdulinfoVO.getQuestionIdList().size(); index++) {
				String questionId = comtnschdulinfoVO.getQuestionIdList().get(index).toString();
				Integer type = new Integer(comtnschdulinfoVO.getTypeList().get(index).toString());
				Integer maximum = new Integer(comtnschdulinfoVO.getMaximumList().get(index).toString());

				if (type.intValue() == 1) { // 객관식
					ComtneventaswperVO answer = new ComtneventaswperVO();
					answer.setUserId(user.getId());					
					answer.setSchdulId(comtnschdulinfoVO.getSchdulId());
					answer.setQesitmId(questionId);
					answer.setAswperSn(new Integer(index + 1));
					answer.setChoiceCnsr(new Integer(request.getParameter("choiceAnswer_" + (index + 1))));
					
					answerList.add(answer);
				} else if (type.intValue() == 2) { // 주관식

					for (int index1 = 0; index1 < maximum; index1++) {
						ComtneventaswperVO answer = new ComtneventaswperVO();
						answer.setUserId(user.getId());
						answer.setSchdulId(comtnschdulinfoVO.getSchdulId());
						answer.setQesitmId(questionId);
						answer.setAswperSn(new Integer(index1 + 1));
						answer.setEssayCnsr(comtnschdulinfoVO.getEssayAnswerList().get(index1 + essayAnswerPointer).toString());

						answerList.add(answer);
					}
					essayAnswerPointer += maximum;
				}
			}
			comtnschdulinfoService.addUserAnswerByBatch(comtneventadhrncVO,	answerList);
		}
		request.getSession().removeAttribute("sessionVO");
		return "gfund/biz/productmng/SurveyEventFinish";
	}
		
	/**
	 * 스케쥴 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 ComtnschdulinfoDefaultVO
	 * @return
	 * @exception Exception
	 */
	@RequestMapping("/mng/gfund/biz/productmng/selectSchdulinfoList.do")
    public String selectComtnschdulinfoList(@ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO,  
    		ModelMap model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
		
		searchVO.setSiteId("SITE_000000000000002");
		searchVO.setSearchSe("3");
    	searchVO.setAdminYn("Y");
    	//공통코드 일정종류
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("COM020");
    	model.addAttribute("schdulSe", cmmUseService.selectCmmCodeDetail(voComCode));

        /** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<?> comtnschdulinfoList = comtnschdulinfoService.selectComtnschdulinfoList_D(searchVO);
        model.addAttribute("resultList", comtnschdulinfoList);
        
        int totCnt = comtnschdulinfoService.selectComtnschdulinfoListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
    	
		return "mng/gfund/biz/productmng/ComtnschdulinfoMngList";
    }
    
    /**
     * 스케쥴 등록 화면으로 이동한다.
     * @param model 화면모델
     * @return 
     * @throws Exception
     */
    @RequestMapping("/mng/gfund/biz/productmng/addComtnschdulinfoView.do")
    public String addComtnschdulinfoView(
    		@ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, 
            ComtnschdulinfoVO comtnschdulinfoVO,
            Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	//스케쥴 코드
    	ComDefaultCodeVO vo = new ComDefaultCodeVO();
    	vo.setCodeId("COM020");	//스케쥴 일정분류 코드
 
    	comtnschdulinfoVO.setUseAt("Y");
    	model.addAttribute("codeList",cmmUseService.selectCmmCodeDetail(vo));
    	request.getSession().setAttribute("sessionVO", comtnschdulinfoVO);
        model.addAttribute("comtnschdulinfoVO", comtnschdulinfoVO);
        
		return "mng/gfund/biz/productmng/ComtnschdulinfoMngRegister";
    }
    
    /**
     * 스케쥴 등록/수정 처리후 메인화면으로 이동한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping("/mng/gfund/biz/productmng/processComtnschdulinfo.do")
    public String addComtnschdulinfo(
    		final MultipartHttpServletRequest multiRequest, 
            @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO,
            ComtnschdulinfoVO comtnschdulinfoVO,
            BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
  			return "forward:/mng/gfund/biz/productmng/selectSchdulinfoList.do";
    	}
    	//beanValidator.validate(comtnschdulinfoVO, bindingResult);
        //if(bindingResult.hasErrors()) {
        //	//스케쥴 코드
        //	ComDefaultCodeVO vo = new ComDefaultCodeVO();
        //	vo.setCodeId("COM020");	//스케쥴 일정분류 코드
        //	model.addAttribute("codeList",cmmUseService.selectCmmCodeDetail(vo));
        //	
        //	model.addAttribute("comtnschdulinfoVO", comtnschdulinfoVO);
        //	
        //	return "mng/gfund/biz/productmng/ComtnschdulinfoMngRegister";
        //}

        if (comtnschdulinfoVO.getQuestionTitleList().size() > 0) {
        	List<ComtneventqesitmVO> questionList = new ArrayList<ComtneventqesitmVO>();
        	int examAndanswerPointer = 0;
        	int typeCount = 0;

        	for (int index=0;index < comtnschdulinfoVO.getQuestionTitleList().size();index++) {
        		ComtneventqesitmVO question = new ComtneventqesitmVO();

        		Integer type = new Integer(comtnschdulinfoVO.getTypeList().get(index).toString());
            	Integer maximum = new Integer(comtnschdulinfoVO.getMaximumList().get(index).toString());
            	
            	question.setQesitmId(comtnschdulinfoVO.getQuestionIdList().get(index).toString());
            	question.setQesitmSn(new Integer(index + 1));
            	question.setQesitmTyCode(type.toString());
            	question.setQesitmSj(comtnschdulinfoVO.getQuestionTitleList().get(index).toString());
            	question.setQesitmCn(comtnschdulinfoVO.getQuestionContentsList().get(index).toString());
            	question.setMxmmAnswerCo(maximum);
            	if(type.intValue() == 1) {	//객관식
            		List<ComtneventqesitmexVO> exampleList = new ArrayList<ComtneventqesitmexVO>();
            		for (int index1 = 0; index1 < maximum; index1++) {
            			ComtneventqesitmexVO example = new ComtneventqesitmexVO();
            			example.setExId(comtnschdulinfoVO.getExampleIdList().get(index1 + examAndanswerPointer).toString());
            			example.setExSn(new Integer(index1 + 1));
            			example.setExCn(comtnschdulinfoVO.getExampleList().get(index1 + examAndanswerPointer).toString());
            			
            			exampleList.add(example);
            		}
            		examAndanswerPointer+=maximum;
            		question.setExampleList(exampleList);
            		//객관식정답
            		List<ComtneventcnsrVO> answerList = new ArrayList<ComtneventcnsrVO>();
            		ComtneventcnsrVO answer = new ComtneventcnsrVO();
            		answer.setCnsrId(comtnschdulinfoVO.getAnswerIdList().get(typeCount).toString());
            		answer.setCnsrSn(1);
            		answer.setChoiseCnsr(new Integer(comtnschdulinfoVO.getAnswerList().get(typeCount).toString()));
            		
            		answerList.add(answer);
            		question.setAnswerList(answerList);
            		typeCount++;
            	} else if(type.intValue() == 2) {	//주관식
            		//주관식정답
            		List<ComtneventcnsrVO> answerList = new ArrayList<ComtneventcnsrVO>();
            		for (int index2 = 0; index2 < maximum; index2++) {
            			ComtneventcnsrVO answer = new ComtneventcnsrVO();
            			answer.setCnsrId(comtnschdulinfoVO.getExampleIdList().get(index2 + examAndanswerPointer).toString());
            			answer.setCnsrSn(new Integer(index2 + 1));
            			answer.setSbjctCnsr(comtnschdulinfoVO.getExampleList().get(index2 + examAndanswerPointer).toString());
            			
            			answerList.add(answer);
            		}
            		examAndanswerPointer+=maximum;
            		question.setAnswerList(answerList);
            	}
            	questionList.add(question);
        	}
        	comtnschdulinfoVO.setQuestionList(questionList);
        }
        List<FileVO> result = null;
        String atchFileId ="";
        
        final Map<String, MultipartFile> files = multiRequest.getFileMap();
        
        if(!files.isEmpty()) {
          result = fileUtil.directParseFileInf(request, files, "EVENT_", 0, "Event.fileStorePath", "");
          atchFileId = fileMngService.insertFileInfs(result);
        }
        if(result != null) {
        	comtnschdulinfoVO.setFileValue(result);
        }

        if (StringUtils.isEmpty(comtnschdulinfoVO.getSchdulId())){
        	comtnschdulinfoVO.setFrstRegisterId(user.getId());
        	comtnschdulinfoService.insertComtnschdulinfo(comtnschdulinfoVO);	//등록
        }else{
        	comtnschdulinfoVO.setLastUpdusrId(user.getId());
        	comtnschdulinfoService.updateComtnschdulinfo(comtnschdulinfoVO);	//수정
        }
        request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mng/gfund/biz/productmng/selectSchdulinfoList.do";
    }
    
    /**
     * 스케쥴 수정 화면으로 이동한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
    @RequestMapping("/mng/gfund/biz/productmng/updateComtnschdulinfoView.do")
    public String updateComtnschdulinfoView(
            @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, 
            ComtnschdulinfoVO comtnschdulinfoVO,
            Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	//스케쥴 코드
    	ComDefaultCodeVO vo = new ComDefaultCodeVO();
    	vo.setCodeId("COM020");	//스케쥴 일정분류 코드 
    	comtnschdulinfoVO.setUseAt("Y");
    	model.addAttribute("codeList",cmmUseService.selectCmmCodeDetail(vo));
    	
    	comtnschdulinfoVO.setMngrAt("Y");
    	ComtnschdulinfoVO comtnschdulinfoList = comtnschdulinfoService.selectComtnschdulinfo(comtnschdulinfoVO);
    	model.addAttribute("comtnschdulinfoVO", comtnschdulinfoList);
    	request.getSession().setAttribute("sessionVO", comtnschdulinfoVO);
    	model.addAttribute("comtnschdulItem", comtnschdulinfoService.selectComtnschduInfo(comtnschdulinfoVO));

    	return "mng/gfund/biz/productmng/ComtnschdulinfoMngRegister";
    }
    
    /**
     * 스케쥴 수정처리후 메인화면으로 이동한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping("/mng/gfund/biz/productmng/updateComtnschdulinfo.do")
    public String updateComtnschdulinfo(
    		final MultipartHttpServletRequest multiRequest, 
            ComtnschdulinfoVO comtnschdulinfoVO,
            @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO,
            BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	comtnschdulinfoVO.setLastUpdusrId(user.getId());

    	beanValidator.validate(comtnschdulinfoVO, bindingResult);
        if(bindingResult.hasErrors()) {
        	
        	ComDefaultCodeVO vo = new ComDefaultCodeVO();
        	vo.setCodeId("COM020");	//스케쥴 일정분류 코드 
        	comtnschdulinfoVO.setUseAt("Y");
        	model.addAttribute("codeList",cmmUseService.selectCmmCodeDetail(vo));
        	
        	model.addAttribute("comtnschdulinfoVO", comtnschdulinfoVO);
       		return "mng/gfund/biz/productmng/ComtnschdulinfoMngRegister";
        }
        
    	List<FileVO> result = null;
        
        final Map<String, MultipartFile> files = multiRequest.getFileMap();
        String atchFileId = "";
        
        if(!files.isEmpty()) {
          
        	result = fileUtil.directParseFileInf(request, files, "EVENT_", 0, "Event.fileStorePath", "");

        	atchFileId = fileMngService.insertFileInfs(result);
        }
        
        if(result != null) {
        	comtnschdulinfoVO.setFileValue(result);
        }
    	
        comtnschdulinfoVO.setAtchFileId(atchFileId);
        comtnschdulinfoService.updateComtnschdulinfo(comtnschdulinfoVO);

        return "forward:/mng/gfund/biz/productmng/selectSchdulinfoList.do";
    }
    
    /**
     * 이벤트 응시현황 목록을 조회한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
	@RequestMapping("/mng/gfund/biz/productmng/selectComtneventadhrncList.do")
    public String selectComtneventadhrncList(
    		ComtneventadhrncVO comtneventadhrncVO, 
    		@ModelAttribute("searchVO") ComtnschdulinfoVO comtnschdulinfoVO,
            ModelMap model, HttpServletRequest request)
            throws Exception {

    	/** EgovPropertyService.sample */
    	comtneventadhrncVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	comtneventadhrncVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(comtneventadhrncVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(comtneventadhrncVO.getPageUnit());
		paginationInfo.setPageSize(comtneventadhrncVO.getPageSize());
		
		comtneventadhrncVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		comtneventadhrncVO.setLastIndex(paginationInfo.getLastRecordIndex());
		comtneventadhrncVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		comtnschdulinfoVO.setMngrAt("Y");
		ComtnschdulinfoVO schdulinfo = comtnschdulinfoService.selectComtnschdulinfo(comtnschdulinfoVO);
		model.addAttribute("comtnschdulinfoVO", schdulinfo);
		
        List<ComtneventadhrncVO> comtneventadhrncList = comtnschdulinfoService.selectComtneventadhrncList(comtneventadhrncVO);
        model.addAttribute("resultList", comtneventadhrncList);
        
        int totCnt = comtnschdulinfoService.selectComtneventadhrncListTotCnt(comtneventadhrncVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

        return "/mng/gfund/biz/productmng/ComtneventadhrncMngList";
    }

	/**
     * 이벤트 응시자를 삭제 한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
    @RequestMapping("/mng/gfund/biz/productmng/deleteComtneventadhrnc.do")
    public String deleteComtneventadhrnc(
            ComtnschdulinfoVO comtnschdulinfoVO,
            @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, SessionStatus status, HttpServletRequest request)
            throws Exception {
    	
        comtnschdulinfoService.deleteComtneventadhrnc(comtnschdulinfoVO);
        status.setComplete();
        
        return "forward:/mng/gfund/biz/productmng/selectComtneventadhrncList.do";
    }
    
	/**
     * 이벤트 응시현황 목록을 엑셀 다운받는다.
     * @param model 화면모델
     * @return forward:/mng/evt/selectSchdulinfoList.do
     * @throws Exception
     */
    @RequestMapping("/mng/gfund/biz/productmng/selectComtneventadhrncExcel.do")
    public String selectComtneventadhrncExcel(
    		@ModelAttribute("searchVO") ComtneventadhrncVO comtneventadhrncVO, 
    		ComtnschdulinfoVO comtnschdulinfoVO,
            ModelMap model, HttpServletRequest request)
            throws Exception {

    	comtneventadhrncVO.setFirstIndex(0);
    	comtneventadhrncVO.setRecordCountPerPage(999999999);
    	List<ComtneventadhrncVO> comtneventadhrncList = comtnschdulinfoService.selectComtneventadhrncList(comtneventadhrncVO);

    	model.addAttribute("resultList", comtneventadhrncList);    	
    	return "mng/evt/ComtneventadhrncExcel";
    }
    
    /**
     * 행사 및 이벤트를 삭제 한다.
     * @param model 화면모델
     * @return
     * @throws Exception
     */
    @RequestMapping("/mng/gfund/biz/productmng/deleteComtnschdulinfo.do")
    public String deleteComtnschdulinfo(
            ComtnschdulinfoVO comtnschdulinfoVO,
            @ModelAttribute("searchVO") ComtnschdulinfoDefaultVO searchVO, SessionStatus status, HttpServletRequest request)
            throws Exception {
        comtnschdulinfoService.deleteComtnschdulinfo(comtnschdulinfoVO);
        status.setComplete();
        
        return "forward:/mng/gfund/biz/productmng/selectSchdulinfoList.do";        
    }
    
    /**
   	 * 설문조사 이벤트 결과 보기 화면.
   	 * @param model 화면모델
   	 * @return
   	 * @exception Exception
   	 */
	@RequestMapping("/mng/gfund/biz/productmng/selectComtnschdulResult.do")
	public String selectComtnschdulResult(ComtnschdulinfoVO comtnschdulinfoVO,
			BindingResult bindingResult, ModelMap model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ComtnschdulinfoVO onlineEvent = null;
		onlineEvent = comtnschdulinfoService.selectComtnschduInfoResult(comtnschdulinfoVO);

		if (onlineEvent != null) {
			comtnschdulinfoVO.setSchdulId(onlineEvent.getSchdulId());
			comtnschdulinfoVO.setSchdulClCode(onlineEvent.getSchdulClCode());
			comtnschdulinfoVO.setSchdulBgnde(onlineEvent.getSchdulBgnde());
			comtnschdulinfoVO.setSchdulEndde(onlineEvent.getSchdulEndde());
			comtnschdulinfoVO.setPresnatnDe(onlineEvent.getPresnatnDe());
			comtnschdulinfoVO.setSchdulNm(onlineEvent.getSchdulNm());
			comtnschdulinfoVO.setTotalCo(onlineEvent.getTotalCo());
			comtnschdulinfoVO.setQuestionList(onlineEvent.getQuestionList());
		} else {
			comtnschdulinfoVO.setIsError(true);
			comtnschdulinfoVO.setErrorMessage("알수없는 이벤트입니다.");
		}

		model.addAttribute("comtnschdulinfoVO", comtnschdulinfoVO);

		return "mng/evt/ComtnschdulEventResult";
	}

}
