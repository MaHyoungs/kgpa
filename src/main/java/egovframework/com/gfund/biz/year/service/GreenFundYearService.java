package egovframework.com.gfund.biz.year.service;

import java.sql.SQLException;
import java.util.List;

public interface GreenFundYearService {

	/**
	 * 기준년도 등록
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int insertGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException;

	/**
	 * 기준년도 전체 해제
	 * @return
	 * @throws SQLException
	 */
	public int updateGreenFundYearAllNonUse() throws SQLException;

	/**
	 * 기준년도 적용
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int updateGreenFundYearUse(GreenFundYearVo greenFundYearVo) throws SQLException;

	/**
	 * 기준년도 삭제
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException;

	/**
	 * 기준년도 조회
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public GreenFundYearVo selectGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException;


	/**
	 * 기준년도 적용중인 년도 조회
	 * @return
	 * @throws SQLException
	 */
	public GreenFundYearVo selectGreenFundYearDefault() throws SQLException;

	/**
	 * 기준년도 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<GreenFundYearVo> selectGreenFundYears() throws SQLException;
}
