package egovframework.com.gfund.biz.year.service;

/**
 * Created by ByungYeol on 2014-12-05.
 */
public class GreenFundYearVo {

	//년도
	private String year;

	//사용유무
	private String use_at;

	//해당 년도 사용중인 갯수
	private String use_cnt;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getUse_at() {
		return use_at;
	}

	public void setUse_at(String use_at) {
		this.use_at = use_at;
	}

	public String getUse_cnt() {
		return use_cnt;
	}

	public void setUse_cnt(String use_cnt) {
		this.use_cnt = use_cnt;
	}
}
