package egovframework.com.gfund.biz.year.service.impl;

import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository("greenFundYearDao")
public class GreenFundYearDao extends EgovAbstractDAO {

	/**
	 * 기준년도 등록
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int insertGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return update("greenFundYearDao.insertGreenFundYear", greenFundYearVo);
	}

	/**
	 * 기준년도 전체 해제
	 * @return
	 * @throws SQLException
	 */
	public int updateGreenFundYearAllNonUse() throws SQLException {
		return update("greenFundYearDao.updateGreenFundYearAllNonUse", null);
	}

	/**
	 * 기준년도 적용
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int updateGreenFundYearUse(GreenFundYearVo greenFundYearVo) throws SQLException {
		return update("greenFundYearDao.updateGreenFundYearUse", greenFundYearVo);
	}

	/**
	 * 기준년도 삭제
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return delete("greenFundYearDao.deleteGreenFundYear", greenFundYearVo);
	}

	/**
	 * 기준년도 조회
	 * @param greenFundYearVo
	 * @return
	 * @throws SQLException
	 */
	public GreenFundYearVo selectGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return (GreenFundYearVo)selectByPk("greenFundYearDao.selectGreenFundYear", greenFundYearVo);
	}

	/**
	 * 기준년도 적용중인 년도 조회
	 * @return
	 * @throws SQLException
	 */
	public GreenFundYearVo selectGreenFundYearDefault() throws SQLException {
		return (GreenFundYearVo)selectByPk("greenFundYearDao.selectGreenFundYearDefault", null);
	}

	/**
	 * 기준년도 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<GreenFundYearVo> selectGreenFundYears() throws SQLException {
		return list("greenFundYearDao.selectGreenFundYears", null);
	}
}
