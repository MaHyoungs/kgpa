package egovframework.com.gfund.biz.year.service.impl;

import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

@Service("greenFundYearService")
public class GreenFundYearServiceImpl extends AbstractServiceImpl implements GreenFundYearService{

	@Resource(name = "greenFundYearDao")
	private GreenFundYearDao greenFundYearDao;

	public int insertGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return greenFundYearDao.insertGreenFundYear(greenFundYearVo);
	}

	public int updateGreenFundYearAllNonUse() throws SQLException {
		return greenFundYearDao.updateGreenFundYearAllNonUse();
	}

	public int updateGreenFundYearUse(GreenFundYearVo greenFundYearVo) throws SQLException {
		return greenFundYearDao.updateGreenFundYearUse(greenFundYearVo);
	}

	public int deleteGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return greenFundYearDao.deleteGreenFundYear(greenFundYearVo);
	}

	public GreenFundYearVo selectGreenFundYear(GreenFundYearVo greenFundYearVo) throws SQLException {
		return greenFundYearDao.selectGreenFundYear(greenFundYearVo);
	}

	public GreenFundYearVo selectGreenFundYearDefault() throws SQLException {
		return greenFundYearDao.selectGreenFundYearDefault();
	}

	public List<GreenFundYearVo> selectGreenFundYears() throws SQLException {
		return greenFundYearDao.selectGreenFundYears();
	}
}
