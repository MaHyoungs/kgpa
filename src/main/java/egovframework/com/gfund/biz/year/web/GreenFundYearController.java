package egovframework.com.gfund.biz.year.web;

import egovframework.com.gfund.biz.year.service.GreenFundYearService;
import egovframework.com.gfund.biz.year.service.GreenFundYearVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.sql.SQLException;

@Controller
public class GreenFundYearController {

	@Resource(name = "greenFundYearService")
	private GreenFundYearService greenFundYearService;

	/**
	 * 년도 목록
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/year/yearList.do")
	public String yearList(ModelMap model) throws SQLException {
		model.addAttribute("yearList", greenFundYearService.selectGreenFundYears());
		return "mng/gfund/biz/year/yearList";
	}

	/**
	 * 년도 등록 페이지
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/year/yearForm.do")
	public String yearForm(ModelMap model) throws SQLException {
		model.addAttribute("yearList", greenFundYearService.selectGreenFundYears());
		return "mng/gfund/biz/year/yearForm";
	}

	/**
	 * 년도 등록
	 * @param greenFundYearVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/year/yearInsert.do")
	public String yearInsert(@ModelAttribute("greenFundYearVo") GreenFundYearVo greenFundYearVo, ModelMap model) throws SQLException {
		if(greenFundYearVo.getUse_at().equals("Y")){
			greenFundYearService.updateGreenFundYearAllNonUse();
		}
		int insertRs =greenFundYearService.insertGreenFundYear(greenFundYearVo);
		model.addAttribute("greenFundYearVo", greenFundYearVo);
		model.addAttribute("insertRs", insertRs);
		return "forward:/mng/gfund/biz/year/yearList.do";
	}

	/**
	 * 년도 적용여부 수정
	 * @param greenFundYearVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/year/yearUseUpdate.do")
	public String yearUpdate(@ModelAttribute("greenFundYearVo") GreenFundYearVo greenFundYearVo, ModelMap model) throws SQLException {
		greenFundYearService.updateGreenFundYearAllNonUse();
		int updateRs = greenFundYearService.updateGreenFundYearUse(greenFundYearVo);
		model.addAttribute("greenFundYearVo", greenFundYearVo);
		model.addAttribute("updateRs", updateRs);
		return "forward:/mng/gfund/biz/year/yearList.do";
	}

	/**
	 * 년도 삭제
	 * @param greenFundYearVo
	 * @param model
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/gfund/biz/year/yearDelete.do")
	public String yearDelete(@ModelAttribute("greenFundYearVo") GreenFundYearVo greenFundYearVo, ModelMap model) throws SQLException {
		int deleteRs = greenFundYearService.deleteGreenFundYear(greenFundYearVo);
		model.addAttribute("greenFundYearVo", greenFundYearVo);
		model.addAttribute("deleteRs", deleteRs);
		return "forward:/mng/gfund/biz/year/yearList.do";
	}
}
