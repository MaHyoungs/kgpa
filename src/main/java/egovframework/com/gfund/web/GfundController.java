package egovframework.com.gfund.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GfundController {

	@RequestMapping(value = "/gfund/cmm/privacy.do")
	public String privacy() {
		return "gfund/cmm/privacy.tiles";
	}
	@RequestMapping(value = "/gfund/cmm/mailDeny.do")
	public String mailDeny() {
		return "gfund/cmm/mailDeny.tiles";
	}

	@RequestMapping(value = "/gfund/cmm/agoPrject.do")
	public String agoPrject() {
		return "gfund/cmm/agoPrject.tiles";
	}
}
