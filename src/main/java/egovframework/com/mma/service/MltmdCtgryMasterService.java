package egovframework.com.mma.service;

import java.util.List;

/**
 * @Class Name : MltmdCtgryMasterService.java
 * @Description : MltmdCtgryMasterService Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 20110907
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdCtgryMasterService {
	
	/**
	 * COMTNMLTMDCTGRYMASTER을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRYMASTER을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRYMASTER을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRYMASTER을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 조회한 COMTNMLTMDCTGRYMASTER
	 * @exception Exception
	 */
    MltmdCtgryMasterVO selectMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRYMASTER 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRYMASTER 목록
	 * @exception Exception
	 */
    List<MltmdCtgryMasterVO> selectMltmdCtgryMasterList(MltmdCtgryMasterVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRYMASTER 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRYMASTER 총 갯수
	 * @exception
	 */
    int selectMltmdCtgryMasterListTotCnt(MltmdCtgryMasterVO searchVO);
    
}
