package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdCtgryVO;

/**
 * @Class Name : MltmdCtgryService.java
 * @Description : MltmdCtgry Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdCtgryService {
	
	/**
	 * COMTNMLTMDCTGRY을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdCtgry(MltmdCtgryVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRY을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdCtgry(MltmdCtgryVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRY을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdCtgry(MltmdCtgryVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRY을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryVO
	 * @return 조회한 COMTNMLTMDCTGRY
	 * @exception Exception
	 */
    MltmdCtgryVO selectMltmdCtgry(MltmdCtgryVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRY 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRY 목록
	 * @exception Exception
	 */
    List<MltmdCtgryVO> selectMltmdCtgryList(MltmdCtgryVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDCTGRY 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    int selectMltmdCtgryListTotCnt(MltmdCtgryVO searchVO);
    
    /**
	 * COMTNMLTMDCTGRY DEPTH를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryLevel(MltmdCtgryVO searchVO) ;
    
    /**
     * 이동할 대상카테고리 정보를 조회 한다.
     * 
     * @param searchVO
     */
    public MltmdCtgryVO selectTargetSortOrdr(MltmdCtgryVO searchVO) throws Exception ;
    
    /**
     * 정렬순서를 수정한다.
     * 
     * @param searchVO
     */
    public void updateSortOrdr(MltmdCtgryVO searchVO) throws Exception;
    
}
