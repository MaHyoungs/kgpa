package egovframework.com.mma.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : MltmdCtgryVO.java
 * @Description : MltmdCtgryVO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class MltmdCtgryVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** CTGRY_ID */
    private java.lang.String ctgryId;
    
    /** CTGRYMASTER_ID */
    private java.lang.String ctgrymasterId;
    
    /** UPPER_CTGRY_ID */
    private java.lang.String upperCtgryId;
    
    /** CTGRY_NM */
    private java.lang.String ctgryNm = "";
    
    /** SORT_ORDR */
    private int sortOrdr = 0;
        
    /** USE_AT */
    private java.lang.String useAt;
    
    /**
	 * 경로 - 명
	 */
    private java.lang.String ctgryPathByName = "";
    
    /**
	 * 경로 - ID
	 */
    private java.lang.String ctgryPathById = "";
    
    /**
	 * 레벨
	 */
    private int ctgryLevel = 1;
    
    /**
	 * 마지막노드여부
	 */
    private java.lang.String lastNodeAt = "";
    
    /**
	 * 동영상목록
	 */
    private List<MltmdMvpInfoVO> mvpInfoList = null;

	public java.lang.String getCtgryId() {
        return this.ctgryId;
    }
    
    public void setCtgryId(java.lang.String ctgryId) {
        this.ctgryId = ctgryId;
    }
    
    public java.lang.String getCtgrymasterId() {
        return this.ctgrymasterId;
    }
    
    public void setCtgrymasterId(java.lang.String ctgrymasterId) {
        this.ctgrymasterId = ctgrymasterId;
    }
    
    public java.lang.String getUpperCtgryId() {
		return upperCtgryId;
	}

	public void setUpperCtgryId(java.lang.String upperCtgryId) {
		this.upperCtgryId = upperCtgryId;
	}

	public java.lang.String getCtgryNm() {
        return this.ctgryNm;
    }
    
    public void setCtgryNm(java.lang.String ctgryNm) {
        this.ctgryNm = ctgryNm;
    }
        
    public int getSortOrdr() {
		return sortOrdr;
	}

	public void setSortOrdr(int sortOrdr) {
		this.sortOrdr = sortOrdr;
	}

	public java.lang.String getUseAt() {
        return this.useAt;
    }
    
    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }

	public String getCtgryPathByName() {
		return ctgryPathByName;
	}

	public void setCtgryPathByName(String ctgryPathByName) {
		this.ctgryPathByName = ctgryPathByName;
	}

	public String getCtgryPathById() {
		return ctgryPathById;
	}

	public void setCtgryPathById(String ctgryPathById) {
		this.ctgryPathById = ctgryPathById;
	}

	public int getCtgryLevel() {
		return ctgryLevel;
	}

	public void setCtgryLevel(int ctgryLevel) {
		this.ctgryLevel = ctgryLevel;
	}

	public java.lang.String getLastNodeAt() {
		return lastNodeAt;
	}

	public void setLastNodeAt(java.lang.String lastNodeAt) {
		this.lastNodeAt = lastNodeAt;
	}

	public List<MltmdMvpInfoVO> getMvpInfoList() {
		return mvpInfoList;
	}

	public void setMvpInfoList(List<MltmdMvpInfoVO> mvpInfoList) {
		this.mvpInfoList = mvpInfoList;
	}
    
}
