package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.sym.sit.service.SiteManageDefaultVO;

/**
 * @Class Name : MltmdEnvrnSetupService.java
 * @Description : MltmdEnvrnSetup Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdEnvrnSetupService {
	
	/**
	 * COMTNMLTMDENVRNSETUP을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDENVRNSETUP을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDENVRNSETUP을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDENVRNSETUP을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 조회한 COMTNMLTMDENVRNSETUP
	 * @exception Exception
	 */
    MltmdEnvrnSetupVO selectMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDENVRNSETUP 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDENVRNSETUP 목록
	 * @exception Exception
	 */
    List selectMltmdEnvrnSetupList(SiteManageDefaultVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDENVRNSETUP 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDENVRNSETUP 총 갯수
	 * @exception
	 */
    int selectMltmdEnvrnSetupListTotCnt(SiteManageDefaultVO searchVO);
    
}
