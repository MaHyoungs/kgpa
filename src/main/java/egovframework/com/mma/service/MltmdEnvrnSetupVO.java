package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdenvrnsetupVO.java
 * @Description : Comtnmltmdenvrnsetup VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class MltmdEnvrnSetupVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** SITE_ID */
    private java.lang.String siteId;
    
    /** CTGRYMASTER_ID */
    private java.lang.String ctgrymasterId;
        
    /** WM_ORIGNL_FILE_NM */
    private java.lang.String wmOrignlFileNm;
    
    /** WM_STRE_FILE_NM */
    private java.lang.String wmStreFileNm;
    
    /** WM_TRNSPRC_NCL */
    private java.math.BigDecimal wmTrnsprcNcl;
    
    /** WM_LC_WIDTH */
    private java.lang.String wmLcWidth;
    
    /** WM_LC_VRTICL */
    private java.lang.String wmLcVrticl;
    
    /** ATMC_CONFM_AT */
    private java.lang.String atmcConfmAt = "Y";
    
    public java.lang.String getSiteId() {
        return this.siteId;
    }
    
    public void setSiteId(java.lang.String siteId) {
        this.siteId = siteId;
    }
    
    public java.lang.String getCtgrymasterId() {
		return ctgrymasterId;
	}

	public void setCtgrymasterId(java.lang.String ctgrymasterId) {
		this.ctgrymasterId = ctgrymasterId;
	}

	public java.lang.String getWmOrignlFileNm() {
        return this.wmOrignlFileNm;
    }
    
    public void setWmOrignlFileNm(java.lang.String wmOrignlFileNm) {
        this.wmOrignlFileNm = wmOrignlFileNm;
    }
    
    public java.lang.String getWmStreFileNm() {
        return this.wmStreFileNm;
    }
    
    public void setWmStreFileNm(java.lang.String wmStreFileNm) {
        this.wmStreFileNm = wmStreFileNm;
    }
    
    public java.math.BigDecimal getWmTrnsprcNcl() {
        return this.wmTrnsprcNcl;
    }
    
    public void setWmTrnsprcNcl(java.math.BigDecimal wmTrnsprcNcl) {
        this.wmTrnsprcNcl = wmTrnsprcNcl;
    }
    
    public java.lang.String getWmLcWidth() {
        return this.wmLcWidth;
    }
    
    public void setWmLcWidth(java.lang.String wmLcWidth) {
        this.wmLcWidth = wmLcWidth;
    }
    
    public java.lang.String getWmLcVrticl() {
        return this.wmLcVrticl;
    }
    
    public void setWmLcVrticl(java.lang.String wmLcVrticl) {
        this.wmLcVrticl = wmLcVrticl;
    }

	public java.lang.String getAtmcConfmAt() {
		return atmcConfmAt;
	}

	public void setAtmcConfmAt(java.lang.String atmcConfmAt) {
		this.atmcConfmAt = atmcConfmAt;
	}
    
}
