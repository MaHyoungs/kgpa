package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdFileDetailVO;

/**
 * @Class Name : MltmdFileDetailService.java
 * @Description : MltmdFileDetail Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdFileDetailService {
	
	/**
	 * COMTNMLTMDFILEDETAIL을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileDetailVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdFileDetail(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILEDETAIL을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileDetailVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdFileDetail(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILEDETAIL을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileDetailVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdFileDetail(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILEDETAIL을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileDetailVO
	 * @return 조회한 COMTNMLTMDFILEDETAIL
	 * @exception Exception
	 */
    MltmdFileDetailVO selectMltmdFileDetail(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILEDETAIL 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILEDETAIL 목록
	 * @exception Exception
	 */
    List<MltmdFileDetailVO> selectMltmdFileDetailList(MltmdFileVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDFILEDETAIL 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILEDETAIL 총 갯수
	 * @exception
	 */
    int selectMltmdFileDetailListTotCnt(MltmdFileVO searchVO);
    
}
