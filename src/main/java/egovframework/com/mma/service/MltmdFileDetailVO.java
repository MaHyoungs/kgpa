package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdfiledetailVO.java
 * @Description : Comtnmltmdfiledetail VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdFileDetailVO extends ComDefaultVO{
    
    /** MLTMD_FILE_DETAIL_ID */
    private java.lang.String mltmdFileDetailId;
    
    /** MLTMD_FILE_ID */
    private java.lang.String mltmdFileId;
    
    /** MLTMD_SN */
    private java.math.BigDecimal mltmdSn;
    
    /** FILE_STRE_COURS */
    private java.lang.String fileStreCours;
    
    /** LGCLFL_NM */
    private java.lang.String lgclflNm;
    
    /** PHYSCL_NM */
    private java.lang.String physclNm;
    
    /** FILE_EXTSN */
    private java.lang.String fileExtsn;
    
    /** FILE_MG */
    private java.math.BigDecimal fileMg;
    
    /** CNVR_CL_CODE */
    private java.lang.String cnvrClCode = "READY";
    
    /** CNVR_RT */
    private java.math.BigDecimal cnvrRt;
    
    /** CNVR_PNTTM */
    private java.sql.Date cnvrPnttm;
    
    /** MEMO */
    private java.lang.String memo;
    
    public java.lang.String getMltmdFileDetailId() {
        return this.mltmdFileDetailId;
    }
    
    public void setMltmdFileDetailId(java.lang.String mltmdFileDetailId) {
        this.mltmdFileDetailId = mltmdFileDetailId;
    }
    
    public java.lang.String getMltmdFileId() {
        return this.mltmdFileId;
    }
    
    public void setMltmdFileId(java.lang.String mltmdFileId) {
        this.mltmdFileId = mltmdFileId;
    }
    
    public java.math.BigDecimal getMltmdSn() {
        return this.mltmdSn;
    }
    
    public void setMltmdSn(java.math.BigDecimal mltmdSn) {
        this.mltmdSn = mltmdSn;
    }
    
    public java.lang.String getFileStreCours() {
        return this.fileStreCours;
    }
    
    public void setFileStreCours(java.lang.String fileStreCours) {
        this.fileStreCours = fileStreCours;
    }
    
    public java.lang.String getLgclflNm() {
        return this.lgclflNm;
    }
    
    public void setLgclflNm(java.lang.String lgclflNm) {
        this.lgclflNm = lgclflNm;
    }
    
    public java.lang.String getPhysclNm() {
        return this.physclNm;
    }
    
    public void setPhysclNm(java.lang.String physclNm) {
        this.physclNm = physclNm;
    }
    
    public java.lang.String getFileExtsn() {
        return this.fileExtsn;
    }
    
    public void setFileExtsn(java.lang.String fileExtsn) {
        this.fileExtsn = fileExtsn;
    }
    
    public java.math.BigDecimal getFileMg() {
        return this.fileMg;
    }
    
    public void setFileMg(java.math.BigDecimal fileMg) {
        this.fileMg = fileMg;
    }
    
    public java.lang.String getCnvrClCode() {
        return this.cnvrClCode;
    }
    
    public void setCnvrClCode(java.lang.String cnvrClCode) {
        this.cnvrClCode = cnvrClCode;
    }
    
    public java.math.BigDecimal getCnvrRt() {
        return this.cnvrRt;
    }
    
    public void setCnvrRt(java.math.BigDecimal cnvrRt) {
        this.cnvrRt = cnvrRt;
    }
    
    public java.sql.Date getCnvrPnttm() {
        return this.cnvrPnttm;
    }
    
    public void setCnvrPnttm(java.sql.Date cnvrPnttm) {
        this.cnvrPnttm = cnvrPnttm;
    }
    
    public java.lang.String getMemo() {
        return this.memo;
    }
    
    public void setMemo(java.lang.String memo) {
        this.memo = memo;
    }
    
}
