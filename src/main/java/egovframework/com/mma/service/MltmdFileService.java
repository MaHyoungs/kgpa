package egovframework.com.mma.service;

import java.util.List;
import java.util.Map;

import egovframework.com.mma.service.MltmdFileVO;

/**
 * @Class Name : MltmdFileService.java
 * @Description : MltmdFile Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdFileService {
	
	/**
	 * COMTNMLTMDFILE을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdFile(MltmdFileVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILE을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdFile(MltmdFileVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILE을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdFile(MltmdFileVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILE을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileVO
	 * @return 조회한 COMTNMLTMDFILE
	 * @exception Exception
	 */
    MltmdFileVO selectMltmdFile(MltmdFileVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDFILE 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILE 목록
	 * @exception Exception
	 */
    List<MltmdFileVO> selectMltmdFileList(MltmdFileVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDFILE 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILE 총 갯수
	 * @exception
	 */
    int selectMltmdFileListTotCnt(MltmdFileVO searchVO);
 
    /**
     *  기본저장경로  :  "/년/월/일" ,  최종파일저장경로 : "/년/월/일/contentsId"
     * @return
     */
    public String selectMltmdFileBassCours();
    
    public Map<String, String> saveData(MltmdFileVO mltmdFileVO) throws Exception ;
    
    public String selectMediaMovieJsonData(MltmdFileVO mvpFileVO) throws Exception;
}
