package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdfileVO.java
 * @Description : Comtnmltmdfile VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdFileVO extends ComDefaultVO{
    
    /** MLTMD_FILE_ID */
    private java.lang.String mltmdFileId;
    
    /** SERVER_ID */
    private java.lang.String serverId;
    
    /** MLTMD_CL_CODE */
    private java.lang.String mltmdClCode;
    
    /** CNVR_CL_CODE */
    private java.lang.String cnvrClCode = "READY";
    
    /** CNVR_RT */
    private java.math.BigDecimal cnvrRt = java.math.BigDecimal.ZERO;
    
    /** CNVR_PNTTM */
    private java.sql.Date cnvrPnttm;
    
    /** MEMO */
    private java.lang.String memo;
    
    /** BASS_COURS */
    private java.lang.String bassCours;
    
    /** CREAT_PNTTM */
    private java.sql.Date creatPnttm;
    
    /** USE_AT */
    private java.lang.String useAt;
    
    private java.lang.String mvpFileId;
    
    private java.lang.String mvpBassCours;
    
    private java.lang.String mvpJsonData;
    
    private java.lang.String mvpChangeAt;
    
    private java.lang.String isReset = "N";
    
    public java.lang.String getMltmdFileId() {
        return this.mltmdFileId;
    }
    
    public void setMltmdFileId(java.lang.String mltmdFileId) {
        this.mltmdFileId = mltmdFileId;
    }
    
    public java.lang.String getServerId() {
        return this.serverId;
    }
    
    public void setServerId(java.lang.String serverId) {
        this.serverId = serverId;
    }
    
    public java.lang.String getMltmdClCode() {
        return this.mltmdClCode;
    }
    
    public void setMltmdClCode(java.lang.String mltmdClCode) {
        this.mltmdClCode = mltmdClCode;
    }
    
    public java.lang.String getCnvrClCode() {
        return this.cnvrClCode;
    }
    
    public void setCnvrClCode(java.lang.String cnvrClCode) {
        this.cnvrClCode = cnvrClCode;
    }
    
    public java.math.BigDecimal getCnvrRt() {
        return this.cnvrRt;
    }
    
    public void setCnvrRt(java.math.BigDecimal cnvrRt) {
        this.cnvrRt = cnvrRt;
    }
    
    public java.sql.Date getCnvrPnttm() {
        return this.cnvrPnttm;
    }
    
    public void setCnvrPnttm(java.sql.Date cnvrPnttm) {
        this.cnvrPnttm = cnvrPnttm;
    }
    
    public java.lang.String getMemo() {
        return this.memo;
    }
    
    public void setMemo(java.lang.String memo) {
        this.memo = memo;
    }
    
    public java.lang.String getBassCours() {
        return this.bassCours;
    }
    
    public void setBassCours(java.lang.String bassCours) {
        this.bassCours = bassCours;
    }
    
    public java.sql.Date getCreatPnttm() {
        return this.creatPnttm;
    }
    
    public void setCreatPnttm(java.sql.Date creatPnttm) {
        this.creatPnttm = creatPnttm;
    }
    
    public java.lang.String getUseAt() {
        return this.useAt;
    }
    
    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }

	public java.lang.String getMvpFileId() {
		return mvpFileId;
	}

	public void setMvpFileId(java.lang.String mvpFileId) {
		this.mvpFileId = mvpFileId;
	}

	public java.lang.String getMvpBassCours() {
		return mvpBassCours;
	}

	public void setMvpBassCours(java.lang.String mvpBassCours) {
		this.mvpBassCours = mvpBassCours;
	}

	public java.lang.String getMvpJsonData() {
		return mvpJsonData;
	}

	public void setMvpJsonData(java.lang.String mvpJsonData) {
		this.mvpJsonData = mvpJsonData;
	}

	public java.lang.String getMvpChangeAt() {
		return mvpChangeAt;
	}

	public void setMvpChangeAt(java.lang.String mvpChangeAt) {
		this.mvpChangeAt = mvpChangeAt;
	}

	public java.lang.String getIsReset() {
		return isReset;
	}

	public void setIsReset(java.lang.String isReset) {
		this.isReset = isReset;
	}
    
}
