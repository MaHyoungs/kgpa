package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdInqireLogVO;

/**
 * @Class Name : MltmdInqireLogService.java
 * @Description : MltmdInqireLog Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdInqireLogService {
	
	/**
	 * COMTNMLTMDINQIRELOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdInqireLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
	int insertMltmdInqireLog(MltmdInqireLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDINQIRELOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdInqireLogVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdInqireLog(MltmdInqireLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDINQIRELOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdInqireLogVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdInqireLog(MltmdInqireLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDINQIRELOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdInqireLogVO
	 * @return 조회한 COMTNMLTMDINQIRELOG
	 * @exception Exception
	 */
    MltmdInqireLogVO selectMltmdInqireLog(MltmdInqireLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDINQIRELOG 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDINQIRELOG 목록
	 * @exception Exception
	 */
    List<MltmdInqireLogVO> selectMltmdInqireLogList(MltmdInqireLogVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDINQIRELOG 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDINQIRELOG 총 갯수
	 * @exception
	 */
    int selectMltmdInqireLogListTotCnt(MltmdInqireLogVO searchVO);
    
}
