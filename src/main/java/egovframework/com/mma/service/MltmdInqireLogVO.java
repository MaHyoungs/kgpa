package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdinqirelogVO.java
 * @Description : Comtnmltmdinqirelog VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdInqireLogVO extends ComDefaultVO{
    
	
	/** INQIRELOG_ID */
    private java.lang.String inqirelogId;
	
    /** MLTMD_ID */
    private java.lang.String mltmdId;
    
    /** IQRRSN_ID */
    private java.lang.String iqrrsnId;
    
    /** INQIRE_DE */
    private java.lang.String inqireDe;
    
    /** INQIRE_PNTTM */
    private java.util.Date inqirePnttm;
    
	public java.lang.String getInqirelogId() {
		return inqirelogId;
	}

	public void setInqirelogId(java.lang.String inqirelogId) {
		this.inqirelogId = inqirelogId;
	}

	public java.lang.String getMltmdId() {
        return this.mltmdId;
    }
    
    public void setMltmdId(java.lang.String mltmdId) {
        this.mltmdId = mltmdId;
    }
    
    public java.lang.String getIqrrsnId() {
        return this.iqrrsnId;
    }
    
    public void setIqrrsnId(java.lang.String iqrrsnId) {
        this.iqrrsnId = iqrrsnId;
    }
    
    public java.lang.String getInqireDe() {
        return this.inqireDe;
    }
    
    public void setInqireDe(java.lang.String inqireDe) {
        this.inqireDe = inqireDe;
    }
    
    public java.util.Date getInqirePnttm() {
        return this.inqirePnttm;
    }
    
    public void setInqirePnttm(java.util.Date inqirePnttm) {
        this.inqirePnttm = inqirePnttm;
    }
    
}
