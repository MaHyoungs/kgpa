package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdMvpCnvrInfoVO;

/**
 * @Class Name : MltmdMvpCnvrInfoService.java
 * @Description : MltmdMvpCnvrInfo Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdMvpCnvrInfoService {
	
	/**
	 * COMTNMLTMDMVPCNVRINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPCNVRINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPCNVRINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdMvpCnvrInfo(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPCNVRINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 조회한 COMTNMLTMDMVPCNVRINFO
	 * @exception Exception
	 */
    MltmdMvpCnvrInfoVO selectMltmdMvpCnvrInfo(MltmdFileVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPCNVRINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPCNVRINFO 목록
	 * @exception Exception
	 */
    List<MltmdMvpCnvrInfoVO> selectMltmdMvpCnvrInfoList(MltmdMvpCnvrInfoVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDMVPCNVRINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPCNVRINFO 총 갯수
	 * @exception
	 */
    int selectMltmdMvpCnvrInfoListTotCnt(MltmdMvpCnvrInfoVO searchVO);
    
}
