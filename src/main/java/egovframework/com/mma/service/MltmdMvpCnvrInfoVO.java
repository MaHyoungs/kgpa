package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdmvpcnvrinfoVO.java
 * @Description : Comtnmltmdmvpcnvrinfo VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdMvpCnvrInfoVO extends ComDefaultVO{
    
    /** MLTMD_FILE_DETAIL_ID */
    private java.lang.String mltmdFileDetailId;
    
    /** FILE_STRE_COURS */
    private java.lang.String fileStreCours;
    
    /** PHYSCL_NM */
    private java.lang.String physclNm;
    
    /** PLAY_TIME */
    private java.math.BigDecimal playTime;
    
    public java.lang.String getMltmdFileDetailId() {
        return this.mltmdFileDetailId;
    }
    
    public void setMltmdFileDetailId(java.lang.String mltmdFileDetailId) {
        this.mltmdFileDetailId = mltmdFileDetailId;
    }
    
    public java.lang.String getFileStreCours() {
        return this.fileStreCours;
    }
    
    public void setFileStreCours(java.lang.String fileStreCours) {
        this.fileStreCours = fileStreCours;
    }
    
    public java.lang.String getPhysclNm() {
        return this.physclNm;
    }
    
    public void setPhysclNm(java.lang.String physclNm) {
        this.physclNm = physclNm;
    }
    
    public java.math.BigDecimal getPlayTime() {
        return this.playTime;
    }
    
    public void setPlayTime(java.math.BigDecimal playTime) {
        this.playTime = playTime;
    }
    
}
