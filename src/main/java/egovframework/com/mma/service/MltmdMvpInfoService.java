package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdMvpInfoVO;

/**
 * @Class Name : MltmdMvpInfoService.java
 * @Description : MltmdMvpInfo Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdMvpInfoService {
	
	/**
	 * MltmdMvpId를 생성한다.
	 * @return MltmdMvpId를
	 * @exception Exception
	 */
    public String selectMltmdMvpId() throws Exception;
    
	/**
	 * COMTNMLTMDMVPINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    MltmdMvpInfoVO selectMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * 추천수를 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    int selectMltmdMvpInfoRecomend(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    List<MltmdMvpInfoVO> selectMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDMVPINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    int selectMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO);
    
    /**
	 * 조회수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpInqire(MltmdMvpInfoVO vo) throws Exception ;

    /**
	 * 추천수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpRecomend(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * 승인정보를 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpConfirm(MltmdMvpInfoVO vo) throws Exception;
    
    /**
	 * 카테고리별 최신 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForCtgryNew(MltmdMvpInfoVO searchVO) throws Exception ;
    
    /**
	 * 추천 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForRecomendBest(MltmdMvpInfoVO searchVO) throws Exception ;
    
    /**
	 * 조회 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForInqireBest(MltmdMvpInfoVO searchVO) throws Exception;
    
    /**
	 * 통합검색 목록
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectSearchMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception ;

    /**
	 * 통합검색 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    public int selectSearchMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO);
}
