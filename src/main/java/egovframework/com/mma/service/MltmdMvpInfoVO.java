package egovframework.com.mma.service;

import java.math.BigDecimal;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdmvpinfoVO.java
 * @Description : Comtnmltmdmvpinfo VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdMvpInfoVO extends ComDefaultVO{
    
	/**
	 * 임시아이디
	 */
    private java.lang.String takeMvpId = "";
    
    /** MLTMD_MVP_ID */
    private java.lang.String mltmdMvpId;
    
    /** MLTMD_FILE_ID */
    private java.lang.String mltmdFileId;
    
    /** CTGRY_ID */
    private java.lang.String ctgryId;
    
    /** SITE_ID */
    private java.lang.String siteId;
    
    /** MVP_SJ */
    private java.lang.String mvpSj;
    
    /** MVP_CN */
    private java.lang.String mvpCn;
    
    /** CONFM_AT */
    private java.lang.String confmAt = "Y";
    
    /** CONFMER_ID */
    private java.lang.String confmerId;
    
    /** CONFMER_PNTTM */
    private java.util.Date confmerPnttm;
    
    /** USE_AT */
    private java.lang.String useAt = "Y";
    
    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;
    
    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;
    
    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;
    
    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;
    
    /** WRTER_NM */
    private java.lang.String wrterNm;
    
    /** SEARCH_KWRD */
    private java.lang.String searchKwrd;
    
    /** INQIRE_CO */
    private java.math.BigDecimal inqireCo = BigDecimal.ZERO;
    
    /** RECOMEND_CO */
    private java.math.BigDecimal recomendCo = BigDecimal.ZERO;
    
    /** RECOMEND_SCORE */
    private java.math.BigDecimal recomendScore = BigDecimal.ZERO;
    
    /** CTGRYMASTER_ID */
    private java.lang.String ctgrymasterId;    
    
    /** CTGRY_NM */
    private java.lang.String ctgryNm = "";
    
    /** 카테고리경로 - ID */
    private java.lang.String ctgryPathById = "";
    
    /** CNVR_CL_CODE */
    private java.lang.String cnvrClCode = "";
    
    /** THUMB_FILE_PATH */
    private java.lang.String thumbFilePath = "";
    
    /** SITE NM */
    private java.lang.String siteNm = "";
    
    /** SITE URL */
    private java.lang.String siteUrl = "";

	public java.lang.String getTakeMvpId() {
		return takeMvpId;
	}

	public void setTakeMvpId(java.lang.String takeMvpId) {
		this.takeMvpId = takeMvpId;
	}

	public java.lang.String getMltmdMvpId() {
        return this.mltmdMvpId;
    }
    
    public void setMltmdMvpId(java.lang.String mltmdMvpId) {
        this.mltmdMvpId = mltmdMvpId;
    }
    
    public java.lang.String getMltmdFileId() {
        return this.mltmdFileId;
    }
    
    public void setMltmdFileId(java.lang.String mltmdFileId) {
        this.mltmdFileId = mltmdFileId;
    }
    
    public java.lang.String getCtgryId() {
        return this.ctgryId;
    }
    
    public void setCtgryId(java.lang.String ctgryId) {
        this.ctgryId = ctgryId;
    }
    
    public java.lang.String getSiteId() {
        return this.siteId;
    }
    
    public void setSiteId(java.lang.String siteId) {
        this.siteId = siteId;
    }
    
    public java.lang.String getMvpSj() {
        return this.mvpSj;
    }
    
    public void setMvpSj(java.lang.String mvpSj) {
        this.mvpSj = mvpSj;
    }
    
    public java.lang.String getMvpCn() {
        return this.mvpCn;
    }
    
    public void setMvpCn(java.lang.String mvpCn) {
        this.mvpCn = mvpCn;
    }
    
    public java.lang.String getConfmAt() {
        return this.confmAt;
    }
    
    public void setConfmAt(java.lang.String confmAt) {
        this.confmAt = confmAt;
    }
    
    public java.lang.String getConfmerId() {
        return this.confmerId;
    }
    
    public void setConfmerId(java.lang.String confmerId) {
        this.confmerId = confmerId;
    }
    
    public java.util.Date getConfmerPnttm() {
        return this.confmerPnttm;
    }
    
    public void setConfmerPnttm(java.util.Date confmerPnttm) {
        this.confmerPnttm = confmerPnttm;
    }
    
    public java.lang.String getUseAt() {
        return this.useAt;
    }
    
    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }
    
    public java.lang.String getFrstRegisterId() {
        return this.frstRegisterId;
    }
    
    public void setFrstRegisterId(java.lang.String frstRegisterId) {
        this.frstRegisterId = frstRegisterId;
    }
    
    public java.util.Date getFrstRegisterPnttm() {
        return this.frstRegisterPnttm;
    }
    
    public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
        this.frstRegisterPnttm = frstRegisterPnttm;
    }
    
    public java.lang.String getLastUpdusrId() {
        return this.lastUpdusrId;
    }
    
    public void setLastUpdusrId(java.lang.String lastUpdusrId) {
        this.lastUpdusrId = lastUpdusrId;
    }
    
    public java.util.Date getLastUpdusrPnttm() {
        return this.lastUpdusrPnttm;
    }
    
    public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
        this.lastUpdusrPnttm = lastUpdusrPnttm;
    }

	public java.lang.String getWrterNm() {
		return wrterNm;
	}

	public void setWrterNm(java.lang.String wrterNm) {
		this.wrterNm = wrterNm;
	}

	public java.lang.String getSearchKwrd() {
		return searchKwrd;
	}

	public void setSearchKwrd(java.lang.String searchKwrd) {
		this.searchKwrd = searchKwrd;
	}

	public java.math.BigDecimal getInqireCo() {
		return inqireCo;
	}

	public void setInqireCo(java.math.BigDecimal inqireCo) {
		this.inqireCo = inqireCo;
	}

	public java.math.BigDecimal getRecomendCo() {
		return recomendCo;
	}

	public void setRecomendCo(java.math.BigDecimal recomendCo) {
		this.recomendCo = recomendCo;
	}

	public java.math.BigDecimal getRecomendScore() {
		return recomendScore;
	}

	public void setRecomendScore(java.math.BigDecimal recomendScore) {
		this.recomendScore = recomendScore;
	}

	public java.lang.String getCtgrymasterId() {
		return ctgrymasterId;
	}

	public void setCtgrymasterId(java.lang.String ctgrymasterId) {
		this.ctgrymasterId = ctgrymasterId;
	}

	public java.lang.String getCtgryNm() {
		return ctgryNm;
	}

	public void setCtgryNm(java.lang.String ctgryNm) {
		this.ctgryNm = ctgryNm;
	}

	public java.lang.String getCtgryPathById() {
		return ctgryPathById;
	}

	public void setCtgryPathById(java.lang.String ctgryPathById) {
		this.ctgryPathById = ctgryPathById;
	}

	public java.lang.String getCnvrClCode() {
		return cnvrClCode;
	}

	public void setCnvrClCode(java.lang.String cnvrClCode) {
		this.cnvrClCode = cnvrClCode;
	}

	public java.lang.String getThumbFilePath() {
		return thumbFilePath;
	}

	public void setThumbFilePath(java.lang.String thumbFilePath) {
		this.thumbFilePath = thumbFilePath;
	}

	public java.lang.String getSiteNm() {
		return siteNm;
	}

	public void setSiteNm(java.lang.String siteNm) {
		this.siteNm = siteNm;
	}

	public java.lang.String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(java.lang.String siteUrl) {
		this.siteUrl = siteUrl;
	}
    
}
