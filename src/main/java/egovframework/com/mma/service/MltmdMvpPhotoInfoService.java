package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdMvpPhotoInfoVO;

/**
 * @Class Name : MltmdMvpPhotoInfoService.java
 * @Description : MltmdMvpPhotoInfo Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdMvpPhotoInfoService {
	
	/**
	 * COMTNMLTMDMVPPHOTOINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPPHOTOINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPPHOTOINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdMvpPhotoInfo(MltmdFileDetailVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPPHOTOINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 조회한 COMTNMLTMDMVPPHOTOINFO
	 * @exception Exception
	 */
    MltmdMvpPhotoInfoVO selectMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDMVPPHOTOINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPPHOTOINFO 목록
	 * @exception Exception
	 */
    List<MltmdMvpPhotoInfoVO> selectMltmdMvpPhotoInfoList(MltmdFileVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDMVPPHOTOINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPPHOTOINFO 총 갯수
	 * @exception
	 */
    int selectMltmdMvpPhotoInfoListTotCnt(MltmdFileVO searchVO);
    
}
