package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdmvpphotoinfoVO.java
 * @Description : Comtnmltmdmvpphotoinfo VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdMvpPhotoInfoVO extends ComDefaultVO{
    
    /** MLTMD_FILE_DETAIL_ID */
    private java.lang.String mltmdFileDetailId;
    
    /** PLAY_TIME */
    private java.math.BigDecimal playTime;
    
    /** FILE_STRE_COURS */
    private java.lang.String fileStreCours;
    
    /** PHYSCL_NM */
    private java.lang.String physclNm;
    
    public java.lang.String getMltmdFileDetailId() {
        return this.mltmdFileDetailId;
    }
    
    public void setMltmdFileDetailId(java.lang.String mltmdFileDetailId) {
        this.mltmdFileDetailId = mltmdFileDetailId;
    }
    
    public java.math.BigDecimal getPlayTime() {
        return this.playTime;
    }
    
    public void setPlayTime(java.math.BigDecimal playTime) {
        this.playTime = playTime;
    }
    
    public java.lang.String getFileStreCours() {
        return this.fileStreCours;
    }
    
    public void setFileStreCours(java.lang.String fileStreCours) {
        this.fileStreCours = fileStreCours;
    }
    
    public java.lang.String getPhysclNm() {
        return this.physclNm;
    }
    
    public void setPhysclNm(java.lang.String physclNm) {
        this.physclNm = physclNm;
    }
    
}
