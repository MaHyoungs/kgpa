package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdRecomendLogVO;

/**
 * @Class Name : MltmdRecomendLogService.java
 * @Description : MltmdRecomendLog Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdRecomendLogService {
	
	/**
	 * COMTNMLTMDRECOMENDLOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdRecomendLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
	int insertMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDRECOMENDLOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDRECOMENDLOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDRECOMENDLOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdRecomendLogVO
	 * @return 조회한 COMTNMLTMDRECOMENDLOG
	 * @exception Exception
	 */
    MltmdRecomendLogVO selectMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDRECOMENDLOG 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDRECOMENDLOG 목록
	 * @exception Exception
	 */
    List<MltmdRecomendLogVO> selectMltmdRecomendLogList(MltmdRecomendLogVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDRECOMENDLOG 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDRECOMENDLOG 총 갯수
	 * @exception
	 */
    int selectMltmdRecomendLogListTotCnt(MltmdRecomendLogVO searchVO);
    
}
