package egovframework.com.mma.service;

import java.math.BigDecimal;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdrecomendlogVO.java
 * @Description : Comtnmltmdrecomendlog VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdRecomendLogVO extends ComDefaultVO{
    
	/** RECOMENDLOG_ID */
    private java.lang.String recomendlogId;
    
    /** MLTMD_ID */
    private java.lang.String mltmdId;
    
    /** RECOMENDR_ID */
    private java.lang.String recomendrId;
    
    /** RECOMEND_CO */
    private java.math.BigDecimal recomendCo = BigDecimal.ZERO;
    
    /** RECOMEND_SCORE */
    private java.math.BigDecimal recomendScore;
    
    /** RECOMEND_DE */
    private java.lang.String recomendDe;
    
    /** RECOMEND_PNTTM */
    private java.util.Date recomendPnttm;
    
    public java.lang.String getRecomendlogId() {
		return recomendlogId;
	}

	public void setRecomendlogId(java.lang.String recomendlogId) {
		this.recomendlogId = recomendlogId;
	}

	public java.lang.String getMltmdId() {
        return this.mltmdId;
    }
    
    public void setMltmdId(java.lang.String mltmdId) {
        this.mltmdId = mltmdId;
    }
    
    public java.lang.String getRecomendrId() {
        return this.recomendrId;
    }
    
    public void setRecomendrId(java.lang.String recomendrId) {
        this.recomendrId = recomendrId;
    }
    
    public java.math.BigDecimal getRecomendCo() {
		return recomendCo;
	}

	public void setRecomendCo(java.math.BigDecimal recomendCo) {
		this.recomendCo = recomendCo;
	}

	public java.math.BigDecimal getRecomendScore() {
        return this.recomendScore;
    }
    
    public void setRecomendScore(java.math.BigDecimal recomendScore) {
        this.recomendScore = recomendScore;
    }
    
    public java.lang.String getRecomendDe() {
        return this.recomendDe;
    }
    
    public void setRecomendDe(java.lang.String recomendDe) {
        this.recomendDe = recomendDe;
    }
    
    public java.util.Date getRecomendPnttm() {
        return this.recomendPnttm;
    }
    
    public void setRecomendPnttm(java.util.Date recomendPnttm) {
        this.recomendPnttm = recomendPnttm;
    }
    
}
