package egovframework.com.mma.service;

import java.util.List;
import egovframework.com.mma.service.MltmdServerVO;

/**
 * @Class Name : MltmdServerService.java
 * @Description : MltmdServer Business class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface MltmdServerService {
	
	/**
	 * COMTNMLTMDSERVER을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdServerVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertMltmdServer(MltmdServerVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDSERVER을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdServerVO
	 * @return void형
	 * @exception Exception
	 */
    void updateMltmdServer(MltmdServerVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDSERVER을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdServerVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteMltmdServer(MltmdServerVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDSERVER을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdServerVO
	 * @return 조회한 COMTNMLTMDSERVER
	 * @exception Exception
	 */
    MltmdServerVO selectMltmdServer(MltmdServerVO vo) throws Exception;
    
    /**
	 * COMTNMLTMDSERVER 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDSERVER 목록
	 * @exception Exception
	 */
    List<MltmdServerVO> selectMltmdServerList(MltmdServerVO searchVO) throws Exception;
    
    /**
	 * COMTNMLTMDSERVER 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDSERVER 총 갯수
	 * @exception
	 */
    int selectMltmdServerListTotCnt(MltmdServerVO searchVO);
    
}
