package egovframework.com.mma.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : ComtnmltmdserverVO.java
 * @Description : Comtnmltmdserver VO class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class MltmdServerVO extends ComDefaultVO{
    
    /** SERVER_ID */
    private java.lang.String serverId;
    
    /** SERVER_NM */
    private java.lang.String serverNm;
    
    /** SERVER_DC */
    private java.lang.String serverDc;
    
    public java.lang.String getServerId() {
        return this.serverId;
    }
    
    public void setServerId(java.lang.String serverId) {
        this.serverId = serverId;
    }
    
    public java.lang.String getServerNm() {
        return this.serverNm;
    }
    
    public void setServerNm(java.lang.String serverNm) {
        this.serverNm = serverNm;
    }
    
    public java.lang.String getServerDc() {
        return this.serverDc;
    }
    
    public void setServerDc(java.lang.String serverDc) {
        this.serverDc = serverDc;
    }
    
}
