package egovframework.com.mma.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.mma.service.EgovMltmdCommentService;
import egovframework.com.mma.service.MltmdComment;
import egovframework.com.mma.service.MltmdCommentVO;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

/**
 * 댓글관리를 위한 서비스 구현 클래스
 * @author 공통컴포넌트개발팀 한성곤
 * @since 2009.06.29
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.06.29  한성곤          최초 생성
 *
 * </pre>
 */
@Service("EgovMltmdCommentService")
public class EgovMltmdCommentServiceImpl extends AbstractServiceImpl implements EgovMltmdCommentService {

    @Resource(name = "MltmdCommentDAO")
    private MltmdCommentDAO mltmdCommentDAO;

    @Resource(name = "egovMltmdCommentNoGnrService")
    private EgovIdGnrService idgenService;
        
    /**
     * 댓글에 대한 목록을 조회 한다.
     */
    public Map<String, Object> selectCommentList(MltmdCommentVO commentVO) throws Exception {
	List<MltmdCommentVO> result = mltmdCommentDAO.selectCommentList(commentVO);
	int cnt = mltmdCommentDAO.selectCommentListCnt(commentVO);
	
	Map<String, Object> map = new HashMap<String, Object>();
	
	map.put("resultList", result);
	map.put("resultCnt", Integer.toString(cnt));

	return map;
    }
    
    /**
     * 댓글을 등록한다.
     */
    public void insertComment(MltmdComment comment) throws Exception {
    	comment.setCommentNo(idgenService.getNextBigDecimalId());
    	mltmdCommentDAO.insertComment(comment);
    	
    }
    
    /**
     * 댓글의 댓글을 등록한다
     */
    public void insertReplyComment(MltmdCommentVO commentVO) throws Exception {
    	commentVO.setCommentNo(idgenService.getNextBigDecimalId());
    	commentVO.setPrntOrdrCode(commentVO.getOrdrCode());
    	commentVO.setOrdrCodeDp(commentVO.getOrdrCodeDp() + 1);
    	mltmdCommentDAO.insertReplyComment(commentVO);
    }
    
    /**
     * 댓글을 삭제한다.
     */
    public void deleteComment(MltmdCommentVO commentVO) throws Exception {
	mltmdCommentDAO.deleteComment(commentVO);
	
    }
    
    /**
     * 모든 댓글을 삭제한다.
     * 
     * @param commentVO
     * @throws Exception
     */
    public void deleteAllComment(MltmdCommentVO commentVO) throws Exception {
    mltmdCommentDAO.deleteAllComment(commentVO);
    }
    
    /**
     * 댓글에 대한 내용을 조회한다.
     */
    public MltmdComment selectComment(MltmdCommentVO commentVO) throws Exception {
	return mltmdCommentDAO.selectComment(commentVO);
    }
    
    /**
     * 댓글에 대한 내용을 수정한다.
     */
    public void updateComment(MltmdComment comment) throws Exception {
	mltmdCommentDAO.updateComment(comment);
    }
    
    /**
     * 댓글 패스워드를 가져온다.
     */
    public String getCommentPassword(MltmdComment comment) throws Exception {
	return mltmdCommentDAO.getCommentPassword(comment);
    }
    
}
