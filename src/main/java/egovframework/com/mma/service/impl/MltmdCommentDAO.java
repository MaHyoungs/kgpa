package egovframework.com.mma.service.impl;

import java.util.List;
import org.springframework.stereotype.Repository;
import egovframework.com.mma.service.MltmdComment;
import egovframework.com.mma.service.MltmdCommentVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 댓글관리를 위한 데이터 접근 클래스
 * @author 공통컴포넌트개발팀 한성곤
 * @since 2009.06.29
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.06.29  한성곤          최초 생성
 *
 * </pre>
 */
@Repository("MltmdCommentDAO")
public class MltmdCommentDAO extends EgovAbstractDAO {

    /**
     * 댓글에 대한 목록을 조회 한다.
     * 
     * @param commentVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<MltmdCommentVO> selectCommentList(MltmdCommentVO commentVO) throws Exception {
	return list("MltmdCommentDAO.selectCommentList", commentVO);
    }
    
    /**
     * 댓글에 대한 목록 건수를 조회 한다.
     * 
     * @param commentVO
     * @return
     * @throws Exception
     */
    public int selectCommentListCnt(MltmdCommentVO commentVO) throws Exception {
	return (Integer)getSqlMapClientTemplate().queryForObject("MltmdCommentDAO.selectCommentListCnt", commentVO);
    }
    
    /**
     * 댓글을 등록한다.
     * 
     * @param comment
     * @throws Exception
     */
    public void insertComment(MltmdComment comment) throws Exception {	
	insert("MltmdCommentDAO.insertComment", comment);
    }
    
    /**
     * 댓글의 댓글을 등록한다
     * @param commentVO
     * @throws Exception
     */
    public void insertReplyComment(MltmdCommentVO commentVO) throws Exception {
    	insert("MltmdCommentDAO.insertReplyComment", commentVO);
    }
    
    /**
     * 댓글을 삭제한다.
     * 
     * @param commentVO
     * @throws Exception
     */
    public void deleteComment(MltmdCommentVO commentVO) throws Exception {
	update("MltmdCommentDAO.deleteComment", commentVO);
    }
    
    /**
     * 모든 댓글을 삭제한다.
     * 
     * @param commentVO
     * @throws Exception
     */
    public void deleteAllComment(MltmdCommentVO commentVO) throws Exception {
	delete("MltmdCommentDAO.deleteAllComment", commentVO);
    }
    
    
    /**
     * 댓글에 대한 내용을 조회한다.
     * 
     * @param commentVO
     * @return
     * @throws Exception
     */
    public MltmdComment selectComment(MltmdCommentVO commentVO) throws Exception {
	return (MltmdComment)selectByPk("MltmdCommentDAO.selectComment", commentVO);
    }
    
    /**
     * 댓글에 대한 내용을 수정한다.
     * 
     * @param comment
     * @throws Exception
     */
    public void updateComment(MltmdComment comment) throws Exception {	
	insert("MltmdCommentDAO.updateComment", comment);
    }
    
    /**
     * 댓글에 대한 패스워드를 조회 한다.
     * 
     * @param comment
     * @return
     * @throws Exception
     */
    public String getCommentPassword(MltmdComment comment) throws Exception {
	return (String)getSqlMapClientTemplate().queryForObject("MltmdCommentDAO.getCommentPassword", comment);
    }
    
}
