package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdCtgryVO;

/**
 * @Class Name : MltmdCtgryDAO.java
 * @Description : MltmdCtgry DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdCtgryDAO")
public class MltmdCtgryDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDCTGRY을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public int insertMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        return (Integer)insert("mltmdCtgryDAO.insertMltmdCtgry", vo);
    }

    /**
	 * COMTNMLTMDCTGRY을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        update("mltmdCtgryDAO.updateMltmdCtgry", vo);
    }

    /**
	 * COMTNMLTMDCTGRY을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        delete("mltmdCtgryDAO.deleteMltmdCtgry", vo);
    }

    /**
	 * COMTNMLTMDCTGRY을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryVO
	 * @return 조회한 COMTNMLTMDCTGRY
	 * @exception Exception
	 */
    public MltmdCtgryVO selectMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        return (MltmdCtgryVO) selectByPk("mltmdCtgryDAO.selectMltmdCtgry", vo);
    }

    /**
	 * COMTNMLTMDCTGRY 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRY 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdCtgryVO> selectMltmdCtgryList(MltmdCtgryVO searchVO) throws Exception {
        return list("mltmdCtgryDAO.selectMltmdCtgryList", searchVO);
    }

    /**
	 * COMTNMLTMDCTGRY 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryListTotCnt(MltmdCtgryVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdCtgryDAO.selectMltmdCtgryListTotCnt", searchVO);
    }

    /**
	 * COMTNMLTMDCTGRY DEPTH를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryLevel(MltmdCtgryVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdCtgryDAO.selectMltmdCtgryLevel", searchVO);
    }
    
    /**
     * 이동할 대상카테고리 정보를 조회 한다.
     * 
     * @param searchVO
     */
    public MltmdCtgryVO selectTargetSortOrdr(MltmdCtgryVO searchVO) throws Exception {
    	return (MltmdCtgryVO)selectByPk("mltmdCtgryDAO.selectTargetSortOrdr", searchVO);
    }
    
    /**
     * 정렬순서를 수정한다.
     * 
     * @param searchVO
     */
    public void updateSortOrdr(MltmdCtgryVO searchVO) throws Exception {
    	update("mltmdCtgryDAO.updateSortOrdr", searchVO);
    }	
}
