package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdCtgryMasterVO;

/**
 * @Class Name : MltmdCtgryMasterDAO.java
 * @Description : MltmdCtgryMaster DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 20110907
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdCtgryMasterDAO")
public class MltmdCtgryMasterDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDCTGRYMASTER을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        return (String)insert("mltmdCtgryMasterDAO.insertMltmdCtgryMaster", vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        update("mltmdCtgryMasterDAO.updateMltmdCtgryMaster", vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        delete("mltmdCtgryMasterDAO.deleteMltmdCtgryMaster", vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 조회한 COMTNMLTMDCTGRYMASTER
	 * @exception Exception
	 */
    public MltmdCtgryMasterVO selectMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        return (MltmdCtgryMasterVO) selectByPk("mltmdCtgryMasterDAO.selectMltmdCtgryMaster", vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRYMASTER 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
	public List<MltmdCtgryMasterVO> selectMltmdCtgryMasterList(MltmdCtgryMasterVO searchVO) throws Exception {
        return list("mltmdCtgryMasterDAO.selectMltmdCtgryMasterList", searchVO);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRYMASTER 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryMasterListTotCnt(MltmdCtgryMasterVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdCtgryMasterDAO.selectMltmdCtgryMasterListTotCnt", searchVO);
    }

}
