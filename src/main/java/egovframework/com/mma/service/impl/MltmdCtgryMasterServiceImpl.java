package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdCtgryMasterService;
import egovframework.com.mma.service.MltmdCtgryMasterVO;
import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;

/**
 * @Class Name : MltmdCtgryMasterServiceImpl.java
 * @Description : MltmdCtgryMaster Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 20110907
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdCtgryMasterService")
public class MltmdCtgryMasterServiceImpl extends AbstractServiceImpl implements
	MltmdCtgryMasterService {

	@Resource(name="mltmdCtgryMasterDAO")
    private MltmdCtgryMasterDAO mltmdCtgryMasterDAO;
    
    @Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
    
    /** ID Generation */
    @Resource(name="egovMltmdCtgryMstrIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * COMTNMLTMDCTGRYMASTER 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
    	
    	String id = egovIdGnrService.getNextStringId();
    	vo.setCtgrymasterId(id);
    	
    	mltmdCtgryMasterDAO.insertMltmdCtgryMaster(vo);
    	
    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
    	ctgry.setCtgrymasterId(id);
    	ctgry.setCtgryNm("대분류");
    	mltmdCtgryService.insertMltmdCtgry(ctgry);
    	
    	//TODO 해당 테이블 정보에 맞게 수정    	
        return null;
    }

    /**
	 * COMTNMLTMDCTGRYMASTER 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        mltmdCtgryMasterDAO.updateMltmdCtgryMaster(vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryMasterVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
        mltmdCtgryMasterDAO.deleteMltmdCtgryMaster(vo);
    }

    /**
	 * COMTNMLTMDCTGRYMASTER 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryMasterVO
	 * @return 조회한 COMTNBBSCTGRYMASTER
	 * @exception Exception
	 */
    public MltmdCtgryMasterVO selectMltmdCtgryMaster(MltmdCtgryMasterVO vo) throws Exception {
    	MltmdCtgryMasterVO resultVO = mltmdCtgryMasterDAO.selectMltmdCtgryMaster(vo);
        //if (resultVO == null)
        //    throw processException("info.nodata.msg");
        return resultVO;
    }

    /**
	 * COMTNBBSCTGRYMASTER 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNBBSCTGRYMASTER 목록
	 * @exception Exception
	 */
    public List<MltmdCtgryMasterVO> selectMltmdCtgryMasterList(MltmdCtgryMasterVO searchVO) throws Exception {
        return mltmdCtgryMasterDAO.selectMltmdCtgryMasterList(searchVO);
    }

    /**
	 * COMTNBBSCTGRYMASTER 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNBBSCTGRYMASTER 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryMasterListTotCnt(MltmdCtgryMasterVO searchVO) {
		return mltmdCtgryMasterDAO.selectMltmdCtgryMasterListTotCnt(searchVO);
	}
    
}
