package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.com.mma.service.impl.MltmdCtgryDAO;

/**
 * @Class Name : MltmdCtgryServiceImpl.java
 * @Description : MltmdCtgry Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdCtgryService")
public class MltmdCtgryServiceImpl extends AbstractServiceImpl implements
        MltmdCtgryService {

    @Resource(name="mltmdCtgryDAO")
    private MltmdCtgryDAO mltmdCtgryDAO;
    
    /** ID Generation */
    @Resource(name="egovMltmdCtgryIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * COMTNMLTMDCTGRY을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdCtgryVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdCtgry(MltmdCtgryVO vo) throws Exception {
    	
    	vo.setCtgryId(egovIdGnrService.getNextStringId());
    	
    	mltmdCtgryDAO.insertMltmdCtgry(vo);
    	
        return null;
    }

    /**
	 * COMTNMLTMDCTGRY을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdCtgryVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        mltmdCtgryDAO.updateMltmdCtgry(vo);
    }

    /**
	 * COMTNMLTMDCTGRY을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdCtgryVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdCtgry(MltmdCtgryVO vo) throws Exception {
        mltmdCtgryDAO.deleteMltmdCtgry(vo);
    }

    /**
	 * COMTNMLTMDCTGRY을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdCtgryVO
	 * @return 조회한 COMTNMLTMDCTGRY
	 * @exception Exception
	 */
    public MltmdCtgryVO selectMltmdCtgry(MltmdCtgryVO vo) throws Exception {
    	return mltmdCtgryDAO.selectMltmdCtgry(vo);
    }

    /**
	 * COMTNMLTMDCTGRY 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRY 목록
	 * @exception Exception
	 */
    public List<MltmdCtgryVO> selectMltmdCtgryList(MltmdCtgryVO searchVO) throws Exception {
        return mltmdCtgryDAO.selectMltmdCtgryList(searchVO);
    }

    /**
	 * COMTNMLTMDCTGRY 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryListTotCnt(MltmdCtgryVO searchVO) {
		return mltmdCtgryDAO.selectMltmdCtgryListTotCnt(searchVO);
	}
    
    /**
	 * COMTNMLTMDCTGRY DEPTH를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDCTGRY 총 갯수
	 * @exception
	 */
    public int selectMltmdCtgryLevel(MltmdCtgryVO searchVO) {
        return mltmdCtgryDAO.selectMltmdCtgryLevel(searchVO);
    }
    
    /**
     * 이동할 대상카테고리 정보를 조회 한다.
     * 
     * @param searchVO
     */
    public MltmdCtgryVO selectTargetSortOrdr(MltmdCtgryVO searchVO) throws Exception {
    	return mltmdCtgryDAO.selectTargetSortOrdr(searchVO);
    }
    
    /**
     * 정렬순서를 수정한다.
     * 
     * @param searchVO
     */
    public void updateSortOrdr(MltmdCtgryVO searchVO) throws Exception {
    	int sourceSortOrdr = searchVO.getSortOrdr();
    	MltmdCtgryVO targetCtgry = mltmdCtgryDAO.selectTargetSortOrdr(searchVO);
    	
    	if(targetCtgry != null) {
    		searchVO.setSortOrdr(targetCtgry.getSortOrdr());
    		mltmdCtgryDAO.updateSortOrdr(searchVO);
    		
    		searchVO.setCtgryId(targetCtgry.getCtgryId());
    		searchVO.setSortOrdr(sourceSortOrdr);
    		mltmdCtgryDAO.updateSortOrdr(searchVO);
    	}
    }	
    
}
