package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.sym.sit.service.SiteManageDefaultVO;

/**
 * @Class Name : MltmdEnvrnSetupDAO.java
 * @Description : MltmdEnvrnSetup DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdEnvrnSetupDAO")
public class MltmdEnvrnSetupDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDENVRNSETUP을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        return (String)insert("mltmdEnvrnSetupDAO.insertMltmdEnvrnSetup", vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        update("mltmdEnvrnSetupDAO.updateMltmdEnvrnSetup", vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        delete("mltmdEnvrnSetupDAO.deleteMltmdEnvrnSetup", vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 조회한 COMTNMLTMDENVRNSETUP
	 * @exception Exception
	 */
    public MltmdEnvrnSetupVO selectMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        return (MltmdEnvrnSetupVO) selectByPk("mltmdEnvrnSetupDAO.selectMltmdEnvrnSetup", vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDENVRNSETUP 목록
	 * @exception Exception
	 */
    public List selectMltmdEnvrnSetupList(SiteManageDefaultVO searchVO) throws Exception {
        return list("mltmdEnvrnSetupDAO.selectMltmdEnvrnSetupList", searchVO);
    }

    /**
	 * COMTNMLTMDENVRNSETUP 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDENVRNSETUP 총 갯수
	 * @exception
	 */
    public int selectMltmdEnvrnSetupListTotCnt(SiteManageDefaultVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdEnvrnSetupDAO.selectMltmdEnvrnSetupListTotCnt", searchVO);
    }

}
