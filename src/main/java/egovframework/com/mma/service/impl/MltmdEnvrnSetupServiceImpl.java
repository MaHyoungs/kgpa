package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.mma.service.impl.MltmdEnvrnSetupDAO;
import egovframework.com.sym.sit.service.SiteManageDefaultVO;

/**
 * @Class Name : MltmdEnvrnSetupServiceImpl.java
 * @Description : MltmdEnvrnSetup Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdEnvrnSetupService")
public class MltmdEnvrnSetupServiceImpl extends AbstractServiceImpl implements
        MltmdEnvrnSetupService {

    @Resource(name="mltmdEnvrnSetupDAO")
    private MltmdEnvrnSetupDAO mltmdEnvrnSetupDAO;
    
	/**
	 * COMTNMLTMDENVRNSETUP을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
    	    	
    	mltmdEnvrnSetupDAO.insertMltmdEnvrnSetup(vo);
    	    	
        return null;
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        mltmdEnvrnSetupDAO.updateMltmdEnvrnSetup(vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
        mltmdEnvrnSetupDAO.deleteMltmdEnvrnSetup(vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return 조회한 COMTNMLTMDENVRNSETUP
	 * @exception Exception
	 */
    public MltmdEnvrnSetupVO selectMltmdEnvrnSetup(MltmdEnvrnSetupVO vo) throws Exception {
    	return mltmdEnvrnSetupDAO.selectMltmdEnvrnSetup(vo);
    }

    /**
	 * COMTNMLTMDENVRNSETUP 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDENVRNSETUP 목록
	 * @exception Exception
	 */
    public List selectMltmdEnvrnSetupList(SiteManageDefaultVO searchVO) throws Exception {
        return mltmdEnvrnSetupDAO.selectMltmdEnvrnSetupList(searchVO);
    }

    /**
	 * COMTNMLTMDENVRNSETUP 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDENVRNSETUP 총 갯수
	 * @exception
	 */
    public int selectMltmdEnvrnSetupListTotCnt(SiteManageDefaultVO searchVO) {
		return mltmdEnvrnSetupDAO.selectMltmdEnvrnSetupListTotCnt(searchVO);
	}
    
}
