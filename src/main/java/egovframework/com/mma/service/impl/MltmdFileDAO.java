package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdFileVO;

/**
 * @Class Name : MltmdFileDAO.java
 * @Description : MltmdFile DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdFileDAO")
public class MltmdFileDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDFILE을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdFile(MltmdFileVO vo) throws Exception {
        return (String)insert("mltmdFileDAO.insertMltmdFile", vo);
    }

    /**
	 * COMTNMLTMDFILE을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdFile(MltmdFileVO vo) throws Exception {
        update("mltmdFileDAO.updateMltmdFile", vo);
    }

    /**
	 * COMTNMLTMDFILE을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdFile(MltmdFileVO vo) throws Exception {
        delete("mltmdFileDAO.deleteMltmdFile", vo);
    }

    /**
	 * COMTNMLTMDFILE을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileVO
	 * @return 조회한 COMTNMLTMDFILE
	 * @exception Exception
	 */
    public MltmdFileVO selectMltmdFile(MltmdFileVO vo) throws Exception {
        return (MltmdFileVO) selectByPk("mltmdFileDAO.selectMltmdFile", vo);
    }

    /**
	 * COMTNMLTMDFILE 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDFILE 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdFileVO> selectMltmdFileList(MltmdFileVO searchVO) throws Exception {
        return list("mltmdFileDAO.selectMltmdFileList", searchVO);
    }

    /**
	 * COMTNMLTMDFILE 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDFILE 총 갯수
	 * @exception
	 */
    public int selectMltmdFileListTotCnt(MltmdFileVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdFileDAO.selectMltmdFileListTotCnt", searchVO);
    }

}
