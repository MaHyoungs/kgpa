package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;

/**
 * @Class Name : MltmdFileDetailDAO.java
 * @Description : MltmdFileDetail DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdFileDetailDAO")
public class MltmdFileDetailDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDFILEDETAIL을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileDetailVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        return (String)insert("mltmdFileDetailDAO.insertMltmdFileDetail", vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileDetailVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        update("mltmdFileDetailDAO.updateMltmdFileDetail", vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileDetailVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        delete("mltmdFileDetailDAO.deleteMltmdFileDetail", vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileDetailVO
	 * @return 조회한 COMTNMLTMDFILEDETAIL
	 * @exception Exception
	 */
    public MltmdFileDetailVO selectMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        return (MltmdFileDetailVO) selectByPk("mltmdFileDetailDAO.selectMltmdFileDetail", vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDFILEDETAIL 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdFileDetailVO> selectMltmdFileDetailList(MltmdFileVO searchVO) throws Exception {
        return list("mltmdFileDetailDAO.selectMltmdFileDetailList", searchVO);
    }

    /**
	 * COMTNMLTMDFILEDETAIL 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDFILEDETAIL 총 갯수
	 * @exception
	 */
    public int selectMltmdFileDetailListTotCnt(MltmdFileVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdFileDetailDAO.selectMltmdFileDetailListTotCnt", searchVO);
    }

}
