package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdFileDetailService;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.impl.MltmdFileDetailDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : MltmdFileDetailServiceImpl.java
 * @Description : MltmdFileDetail Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdFileDetailService")
public class MltmdFileDetailServiceImpl extends AbstractServiceImpl implements
        MltmdFileDetailService {

    @Resource(name="mltmdFileDetailDAO")
    private MltmdFileDetailDAO mltmdFileDetailDAO;
    
    /** ID Generation */
    @Resource(name="egovMltmdFileDetailIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * COMTNMLTMDFILEDETAIL을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileDetailVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
    	
    	if(EgovStringUtil.isEmpty(vo.getMltmdFileDetailId())) {
    		vo.setMltmdFileDetailId(egovIdGnrService.getNextStringId());
    	}
    	
    	mltmdFileDetailDAO.insertMltmdFileDetail(vo);
    	    	
        return null;
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileDetailVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        mltmdFileDetailDAO.updateMltmdFileDetail(vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileDetailVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
        mltmdFileDetailDAO.deleteMltmdFileDetail(vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileDetailVO
	 * @return 조회한 COMTNMLTMDFILEDETAIL
	 * @exception Exception
	 */
    public MltmdFileDetailVO selectMltmdFileDetail(MltmdFileDetailVO vo) throws Exception {
    	return mltmdFileDetailDAO.selectMltmdFileDetail(vo);
    }

    /**
	 * COMTNMLTMDFILEDETAIL 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILEDETAIL 목록
	 * @exception Exception
	 */
    public List<MltmdFileDetailVO> selectMltmdFileDetailList(MltmdFileVO searchVO) throws Exception {
        return mltmdFileDetailDAO.selectMltmdFileDetailList(searchVO);
    }

    /**
	 * COMTNMLTMDFILEDETAIL 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILEDETAIL 총 갯수
	 * @exception
	 */
    public int selectMltmdFileDetailListTotCnt(MltmdFileVO searchVO) {
		return mltmdFileDetailDAO.selectMltmdFileDetailListTotCnt(searchVO);
	}
    
}
