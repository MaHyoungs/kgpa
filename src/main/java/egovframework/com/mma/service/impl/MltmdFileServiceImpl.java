package egovframework.com.mma.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdFileDetailService;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileService;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoService;
import egovframework.com.mma.service.MltmdMvpPhotoInfoService;
import egovframework.com.mma.service.impl.MltmdFileDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : MltmdFileServiceImpl.java
 * @Description : MltmdFile Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdFileService")
public class MltmdFileServiceImpl extends AbstractServiceImpl implements
        MltmdFileService {

    @Resource(name="mltmdFileDAO")
    private MltmdFileDAO mltmdFileDAO;
    
    @Resource(name="mltmdFileDetailService")
    private MltmdFileDetailService mltmdFileDetailService;
    
    @Resource(name="mltmdMvpPhotoInfoService")
    private MltmdMvpPhotoInfoService mltmdMvpPhotoInfoService;
    
    @Resource(name = "mltmdMvpCnvrInfoService")
    private MltmdMvpCnvrInfoService mltmdMvpCnvrInfoService;
    
    @Resource(name="egovMltmdFileIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * COMTNMLTMDFILE을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdFileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdFile(MltmdFileVO vo) throws Exception {
    	
    	if(EgovStringUtil.isEmpty(vo.getMltmdFileId())) {
    		vo.setMltmdFileId(egovIdGnrService.getNextStringId());
    	}
    	
    	mltmdFileDAO.insertMltmdFile(vo);
    	
        return null;
    }

    /**
	 * COMTNMLTMDFILE을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdFileVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdFile(MltmdFileVO vo) throws Exception {
        mltmdFileDAO.updateMltmdFile(vo);
    }

    /**
	 * COMTNMLTMDFILE을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdFileVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdFile(MltmdFileVO vo) throws Exception {
        mltmdFileDAO.deleteMltmdFile(vo);
    }

    /**
	 * COMTNMLTMDFILE을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdFileVO
	 * @return 조회한 COMTNMLTMDFILE
	 * @exception Exception
	 */
    public MltmdFileVO selectMltmdFile(MltmdFileVO vo) throws Exception {
        MltmdFileVO resultVO = mltmdFileDAO.selectMltmdFile(vo);
        if (resultVO == null)
            throw processException("info.nodata.msg");
        return resultVO;
    }

    /**
	 * COMTNMLTMDFILE 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILE 목록
	 * @exception Exception
	 */
    public List<MltmdFileVO> selectMltmdFileList(MltmdFileVO searchVO) throws Exception {
        return mltmdFileDAO.selectMltmdFileList(searchVO);
    }

    /**
	 * COMTNMLTMDFILE 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDFILE 총 갯수
	 * @exception
	 */
    public int selectMltmdFileListTotCnt(MltmdFileVO searchVO) {
		return mltmdFileDAO.selectMltmdFileListTotCnt(searchVO);
	}
    
    /**
     *  기본저장경로  :  "/년/월/일" ,  최종파일저장경로 : "/년/월/일/contentsId"
     * @return
     */
    public String selectMltmdFileBassCours() {
		java.util.Calendar cal = java.util.Calendar.getInstance();		
		int iYear = cal.get(java.util.Calendar.YEAR);
		int iMonth = cal.get(java.util.Calendar.MONTH);
		int iDate = cal.get(java.util.Calendar.DATE);		
		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth+1).length() == 1 ? "0" + Integer.toString(iMonth+1) : Integer.toString(iMonth+1); 
		String sDay = Integer.toString(iDate).length() == 1 ? "0" + Integer.toString(iDate) : Integer.toString(iDate); 
		
		return "/" + sYear + "/" + sMonth + "/" + sDay;
    }
    
    public Map<String, String> saveData(MltmdFileVO mltmdFileVO) throws Exception {  
    	boolean mvpChangeAt = ("Y".equals( mltmdFileVO.getMvpChangeAt()) ? true : false);
    	
    	Map<String, Object> mediaDataList = this.getMediaDataList(mltmdFileVO, mvpChangeAt);
    	Map<String, String> returnData = insertMltmdFile(mediaDataList);
    	
    	//파일변경사항은 없으나.. 만약 변환에러일경우 다시 변환하게 하기 위한 코드추가
    	MltmdFileVO resetVO = new MltmdFileVO();				
		resetVO.setMltmdFileId(resetVO.getMvpFileId());
		resetVO.setCnvrClCode("READY");
		resetVO.setIsReset("Y");
		updateMltmdFile(resetVO);
		
    	return returnData;
    }
    
  //미디어 파일정보를 가져온다.
    public Map<String, Object> getMediaDataList(MltmdFileVO mediaVO, boolean mvpChangeAt) throws Exception {    	
    	MltmdFileVO mvpFileVO = null;
    	List<MltmdFileDetailVO> mvpDetailList = null;
    	MltmdFileDetailVO mvpDetailVO = null;
    	
    	mvpFileVO = new MltmdFileVO();
    	mvpFileVO.setMltmdFileId(mediaVO.getMvpFileId());
    	mvpFileVO.setMltmdClCode("MVP");
    	mvpFileVO.setMvpBassCours(mediaVO.getMvpBassCours());
    	mvpFileVO.setServerId(mediaVO.getServerId());
    	
    	mvpDetailList = new ArrayList<MltmdFileDetailVO>();    
    	
    	JSONArray mvpJArray =  JSONArray.fromObject(mediaVO.getMvpJsonData());
		for(int i=0; i < mvpJArray.size(); i++) {
			
			JSONObject jObj = (JSONObject)mvpJArray.get(i);
			
			mvpDetailVO = new MltmdFileDetailVO();
			mvpDetailVO.setMltmdFileId(mediaVO.getMvpFileId());
			mvpDetailVO.setMltmdFileDetailId((String)jObj.get("MltmdDetailId"));
			mvpDetailVO.setMltmdSn(java.math.BigDecimal.valueOf(Long.parseLong((String)jObj.get("FileSeq"))));
			mvpDetailVO.setFileStreCours((String)jObj.get("FileSavePath"));
			mvpDetailVO.setLgclflNm((String)jObj.get("FileName"));
			mvpDetailVO.setPhysclNm((String)jObj.get("UploadFileName"));
			mvpDetailVO.setFileExtsn((String)jObj.get("FileExtention"));
			mvpDetailVO.setFileMg(java.math.BigDecimal.valueOf(Long.parseLong((String)jObj.get("FileSize"))));
			
			mvpDetailList.add(mvpDetailVO);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mvpChangeAt", mvpChangeAt);
    	map.put("mvpFileVO", mvpFileVO);
    	map.put("mvpDetailList", mvpDetailList);
    	
    	return map;
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, String> insertMltmdFile(Map<String, Object> mediaDataList) throws Exception {
    	
    	Map<String, String> mediaInfo = new HashMap<String, String>();
    	
    	Boolean mvpChangeAt = (Boolean)mediaDataList.get("mvpChangeAt");
    	
    	if(mvpChangeAt.booleanValue()) {
    		
    		MltmdFileVO mvpFileVO = (MltmdFileVO)mediaDataList.get("mvpFileVO");
        	List<MltmdFileDetailVO> mvpDetailList = (List<MltmdFileDetailVO>)mediaDataList.get("mvpDetailList");
        	
    		List<MltmdFileDetailVO>  detailList = mltmdFileDetailService.selectMltmdFileDetailList(mvpFileVO);
    		
    		//db 와 비교하여.. 사용자가 지운 데이터를 삭제한다.
        	if(!EgovStringUtil.isEmpty(mvpFileVO.getMltmdFileId())) {
        		
        		if(detailList != null && detailList.size() > 0) {
        			MltmdFileDetailVO dbDetailVO = null;
        			MltmdFileDetailVO delVO = new MltmdFileDetailVO();
        			boolean idDelTarget = true;
        			for(int i=0; i < detailList.size(); i++) {
        				dbDetailVO = detailList.get(i);
        				
        				idDelTarget = true;
        				for(int di=0; di < mvpDetailList.size(); di++) {
        					if(mvpDetailList.get(di).getMltmdFileDetailId().equals(dbDetailVO.getMltmdFileDetailId())) {
        						idDelTarget = false;
        						break;
        					}
        				}
        				//지워졌다면..
        				if(idDelTarget) {
        					//상세파일아이디로 지워야하기에.. 대표파일아이디를 지운다.
        					delVO.setMltmdFileDetailId(dbDetailVO.getMltmdFileDetailId());
        					//동영상썸네일정보 삭제
        					mltmdMvpPhotoInfoService.deleteMltmdMvpPhotoInfo(delVO);
    			    		//동영상변환정보 삭제
        					mltmdMvpCnvrInfoService.deleteMltmdMvpCnvrInfo(delVO);
    			    		//파일상세 삭제
        					mltmdFileDetailService.deleteMltmdFileDetail(delVO);	
        				}
        			}
        		}
		    	
        	}
	    	
        	if(mvpDetailList.size() > 0) {
	        	//신규등록..
		    	if(EgovStringUtil.isEmpty(mvpFileVO.getMltmdFileId())) {
		    		//파일마스터 등록
		    		insertMltmdFile(mvpFileVO);
	    		}
		    	
		    	//파일상세 등록
		    	MltmdFileDetailVO mvpDetailVO = null;
		    	for(int i=0; i < mvpDetailList.size(); i++) {
		    		mvpDetailVO = mvpDetailList.get(i);
		    		if(EgovStringUtil.isEmpty(mvpDetailVO.getMltmdFileDetailId())) {
		    			mvpDetailVO.setMltmdFileId(mvpFileVO.getMltmdFileId());
				    	mvpDetailVO.setMltmdSn(java.math.BigDecimal.valueOf(i));
				    	mltmdFileDetailService.insertMltmdFileDetail(mvpDetailVO);	
		    		} else {
		    			mvpDetailVO.setMltmdSn(java.math.BigDecimal.valueOf(i));
		    			mltmdFileDetailService.updateMltmdFileDetail(mvpDetailVO);
		    		}
		    	}
        	} else {
        		//파일마스터 삭제
        		deleteMltmdFile(mvpFileVO);
        		mvpFileVO.setMltmdFileId("");        		
        	}
	    	
	    	mediaInfo.put("mvpFileId", mvpFileVO.getMltmdFileId());
	    	mediaInfo.put("mvpDetailListCount", String.valueOf(mvpDetailList.size()));
	    	
	    	log.debug(mvpFileVO.toString());
    	}else {
    		List<MltmdFileDetailVO> mvpDetailList = (List<MltmdFileDetailVO>)mediaDataList.get("mvpDetailList");
    		if(mvpDetailList != null) {
    			MltmdFileDetailVO mvpDetailVO = null;	    	
		    	for(int i=0; i < mvpDetailList.size(); i++) {
		    		mvpDetailVO = mvpDetailList.get(i);	    		
		    		mltmdFileDetailService.updateMltmdFileDetail(mvpDetailVO);
		    	}
    		}
    	}
    	
        return mediaInfo;
    }
    
    public String selectMediaMovieJsonData(MltmdFileVO mvpFileVO) throws Exception {
		//동영상파일 상세정보
    	List<MltmdFileDetailVO> mvpDetailList = mltmdFileDetailService.selectMltmdFileDetailList(mvpFileVO);
    	MltmdFileDetailVO mvpDetailVO = null;
    	JSONObject jObj = null;
    	JSONArray mvpJArray = new JSONArray();
    	for(int i=0; i < mvpDetailList.size(); i++) {
    		mvpDetailVO = mvpDetailList.get(i);
    		
    		jObj = new JSONObject();
    		//jObj.put("BaseSavePath", null);
    		jObj.put("MltmdDetailId", EgovStringUtil.isNullToString(mvpDetailVO.getMltmdFileDetailId()));
    		jObj.put("FileSavePath", EgovStringUtil.isNullToString(mvpDetailVO.getFileStreCours()));
    		jObj.put("FileSeq", EgovStringUtil.isNullToString(mvpDetailVO.getMltmdSn()));
    		jObj.put("FileExtention", EgovStringUtil.isNullToString(mvpDetailVO.getFileExtsn()));
    		jObj.put("FileName", EgovStringUtil.isNullToString(mvpDetailVO.getLgclflNm()));
    		jObj.put("UploadFileName", EgovStringUtil.isNullToString(mvpDetailVO.getPhysclNm()));
    		jObj.put("FileSize", EgovStringUtil.isNullToString(mvpDetailVO.getFileMg()));   
    		
    		mvpJArray.add(jObj);
    	}
    	
    	return mvpJArray.toString();
    }
}
