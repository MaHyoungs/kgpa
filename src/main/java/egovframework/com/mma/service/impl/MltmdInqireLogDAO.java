package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdInqireLogVO;

/**
 * @Class Name : MltmdInqireLogDAO.java
 * @Description : MltmdInqireLog DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdInqireLogDAO")
public class MltmdInqireLogDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDINQIRELOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdInqireLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public void insertMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        insert("mltmdInqireLogDAO.insertMltmdInqireLog", vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdInqireLogVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        update("mltmdInqireLogDAO.updateMltmdInqireLog", vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdInqireLogVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        delete("mltmdInqireLogDAO.deleteMltmdInqireLog", vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdInqireLogVO
	 * @return 조회한 COMTNMLTMDINQIRELOG
	 * @exception Exception
	 */
    public MltmdInqireLogVO selectMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        return (MltmdInqireLogVO) selectByPk("mltmdInqireLogDAO.selectMltmdInqireLog", vo);
    }
    
    /**
	 * COMTNMLTMDINQIRELOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdInqireLogVO
	 * @return 조회한 COMTNMLTMDINQIRELOG
	 * @exception Exception
	 */
    public int selectMltmdInqireLogExists(MltmdInqireLogVO vo) throws Exception {
        return (Integer) selectByPk("mltmdInqireLogDAO.selectMltmdInqireLogExists", vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDINQIRELOG 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdInqireLogVO> selectMltmdInqireLogList(MltmdInqireLogVO searchVO) throws Exception {
        return list("mltmdInqireLogDAO.selectMltmdInqireLogList", searchVO);
    }

    /**
	 * COMTNMLTMDINQIRELOG 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDINQIRELOG 총 갯수
	 * @exception
	 */
    public int selectMltmdInqireLogListTotCnt(MltmdInqireLogVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdInqireLogDAO.selectMltmdInqireLogListTotCnt", searchVO);
    }

}
