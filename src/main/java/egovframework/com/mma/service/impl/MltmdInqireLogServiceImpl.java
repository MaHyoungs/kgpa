package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdInqireLogService;
import egovframework.com.mma.service.MltmdInqireLogVO;
import egovframework.com.mma.service.impl.MltmdInqireLogDAO;

/**
 * @Class Name : MltmdInqireLogServiceImpl.java
 * @Description : MltmdInqireLog Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdInqireLogService")
public class MltmdInqireLogServiceImpl extends AbstractServiceImpl implements
        MltmdInqireLogService {

    @Resource(name="mltmdInqireLogDAO")
    private MltmdInqireLogDAO mltmdInqireLogDAO;
    
    @Resource(name="egovMltmdInqireIdGnrService")    
    private EgovIdGnrService egovIdGnrService;
    
	/**
	 * COMTNMLTMDINQIRELOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdInqireLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public int insertMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
    	vo.setInqirelogId(egovIdGnrService.getNextStringId());
    	mltmdInqireLogDAO.insertMltmdInqireLog(vo);
    	
    	return mltmdInqireLogDAO.selectMltmdInqireLogExists(vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdInqireLogVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        mltmdInqireLogDAO.updateMltmdInqireLog(vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdInqireLogVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
        mltmdInqireLogDAO.deleteMltmdInqireLog(vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdInqireLogVO
	 * @return 조회한 COMTNMLTMDINQIRELOG
	 * @exception Exception
	 */
    public MltmdInqireLogVO selectMltmdInqireLog(MltmdInqireLogVO vo) throws Exception {
    	return mltmdInqireLogDAO.selectMltmdInqireLog(vo);
    }

    /**
	 * COMTNMLTMDINQIRELOG 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDINQIRELOG 목록
	 * @exception Exception
	 */
    public List<MltmdInqireLogVO> selectMltmdInqireLogList(MltmdInqireLogVO searchVO) throws Exception {
        return mltmdInqireLogDAO.selectMltmdInqireLogList(searchVO);
    }

    /**
	 * COMTNMLTMDINQIRELOG 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDINQIRELOG 총 갯수
	 * @exception
	 */
    public int selectMltmdInqireLogListTotCnt(MltmdInqireLogVO searchVO) {
		return mltmdInqireLogDAO.selectMltmdInqireLogListTotCnt(searchVO);
	}
    
}
