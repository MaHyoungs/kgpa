package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoVO;

/**
 * @Class Name : MltmdMvpCnvrInfoDAO.java
 * @Description : MltmdMvpCnvrInfo DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdMvpCnvrInfoDAO")
public class MltmdMvpCnvrInfoDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDMVPCNVRINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception {
        return (String)insert("mltmdMvpCnvrInfoDAO.insertMltmdMvpCnvrInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception {
        update("mltmdMvpCnvrInfoDAO.updateMltmdMvpCnvrInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpCnvrInfo(MltmdFileDetailVO vo) throws Exception {
        delete("mltmdMvpCnvrInfoDAO.deleteMltmdMvpCnvrInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 조회한 COMTNMLTMDMVPCNVRINFO
	 * @exception Exception
	 */
    public MltmdMvpCnvrInfoVO selectMltmdMvpCnvrInfo(MltmdFileVO vo) throws Exception {
        return (MltmdMvpCnvrInfoVO) selectByPk("mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPCNVRINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpCnvrInfoVO> selectMltmdMvpCnvrInfoList(MltmdMvpCnvrInfoVO searchVO) throws Exception {
        return list("mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfoList", searchVO);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPCNVRINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpCnvrInfoListTotCnt(MltmdMvpCnvrInfoVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfoListTotCnt", searchVO);
    }

}
