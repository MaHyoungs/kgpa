package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoService;
import egovframework.com.mma.service.MltmdMvpCnvrInfoVO;
import egovframework.com.mma.service.impl.MltmdMvpCnvrInfoDAO;

/**
 * @Class Name : MltmdMvpCnvrInfoServiceImpl.java
 * @Description : MltmdMvpCnvrInfo Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdMvpCnvrInfoService")
public class MltmdMvpCnvrInfoServiceImpl extends AbstractServiceImpl implements
        MltmdMvpCnvrInfoService {

    @Resource(name="mltmdMvpCnvrInfoDAO")
    private MltmdMvpCnvrInfoDAO mltmdMvpCnvrInfoDAO;
    
	/**
	 * COMTNMLTMDMVPCNVRINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception {
    	
    	mltmdMvpCnvrInfoDAO.insertMltmdMvpCnvrInfo(vo);
    	
        return null;
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpCnvrInfo(MltmdMvpCnvrInfoVO vo) throws Exception {
        mltmdMvpCnvrInfoDAO.updateMltmdMvpCnvrInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpCnvrInfo(MltmdFileDetailVO vo) throws Exception {
        mltmdMvpCnvrInfoDAO.deleteMltmdMvpCnvrInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpCnvrInfoVO
	 * @return 조회한 COMTNMLTMDMVPCNVRINFO
	 * @exception Exception
	 */
    public MltmdMvpCnvrInfoVO selectMltmdMvpCnvrInfo(MltmdFileVO vo) throws Exception {
    	return mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPCNVRINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpCnvrInfoVO> selectMltmdMvpCnvrInfoList(MltmdMvpCnvrInfoVO searchVO) throws Exception {
        return mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfoList(searchVO);
    }

    /**
	 * COMTNMLTMDMVPCNVRINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPCNVRINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpCnvrInfoListTotCnt(MltmdMvpCnvrInfoVO searchVO) {
		return mltmdMvpCnvrInfoDAO.selectMltmdMvpCnvrInfoListTotCnt(searchVO);
	}
    
}
