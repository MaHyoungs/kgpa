package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdMvpInfoVO;

/**
 * @Class Name : MltmdMvpInfoDAO.java
 * @Description : MltmdMvpInfo DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdMvpInfoDAO")
public class MltmdMvpInfoDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDMVPINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        return (String)insert("mltmdMvpInfoDAO.insertMltmdMvpInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        update("mltmdMvpInfoDAO.updateMltmdMvpInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        delete("mltmdMvpInfoDAO.deleteMltmdMvpInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    public MltmdMvpInfoVO selectMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        return (MltmdMvpInfoVO) selectByPk("mltmdMvpInfoDAO.selectMltmdMvpInfo", vo);
    }
    
    /**
	 * 추천수를 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    public int selectMltmdMvpInfoRecomend(MltmdMvpInfoVO vo) throws Exception {
        return (Integer) selectByPk("mltmdMvpInfoDAO.selectMltmdMvpInfoRecomend", vo);
    }

    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception {
        return list("mltmdMvpInfoDAO.selectMltmdMvpInfoList", searchVO);
    }

    /**
	 * COMTNMLTMDMVPINFO 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdMvpInfoDAO.selectMltmdMvpInfoListTotCnt", searchVO);
    }
    
    /**
	 * 조회수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpInqire(MltmdMvpInfoVO vo) throws Exception {
        update("mltmdMvpInfoDAO.updateMltmdMvpInqire", vo);
    }

    /**
	 * 추천수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpRecomend(MltmdMvpInfoVO vo) throws Exception {
        update("mltmdMvpInfoDAO.updateMltmdMvpRecomend", vo);
    }
    
    /**
	 * 승인정보를 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpConfirm(MltmdMvpInfoVO vo) throws Exception {
        update("mltmdMvpInfoDAO.updateMltmdMvpConfirm", vo);
    }
    
    /**
	 * 카테고리별 최신 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForCtgryNew(MltmdMvpInfoVO searchVO) throws Exception {
        return list("mltmdMvpInfoDAO.selectMltmdMvpInfoListForCtgryNew", searchVO);
    }
    
    /**
	 * 추천 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForRecomendBest(MltmdMvpInfoVO searchVO) throws Exception {
        return list("mltmdMvpInfoDAO.selectMltmdMvpInfoListForRecomendBest", searchVO);
    }
    
    /**
	 * 조회 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForInqireBest(MltmdMvpInfoVO searchVO) throws Exception {
        return list("mltmdMvpInfoDAO.selectMltmdMvpInfoListForInqireBest", searchVO);
    }
    
    /**
	 * 통합검색 목록
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpInfoVO> selectSearchMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception {
        return list("mltmdMvpInfoDAO.selectSearchMltmdMvpInfoList", searchVO);
    }

    /**
	 * 통합검색 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    public int selectSearchMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdMvpInfoDAO.selectSearchMltmdMvpInfoListTotCnt", searchVO);
    }
    
}
