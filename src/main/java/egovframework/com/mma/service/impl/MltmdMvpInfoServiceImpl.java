package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdMvpInfoService;
import egovframework.com.mma.service.MltmdMvpInfoVO;
import egovframework.com.mma.service.impl.MltmdMvpInfoDAO;

/**
 * @Class Name : MltmdMvpInfoServiceImpl.java
 * @Description : MltmdMvpInfo Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdMvpInfoService")
public class MltmdMvpInfoServiceImpl extends AbstractServiceImpl implements
        MltmdMvpInfoService {

    @Resource(name="mltmdMvpInfoDAO")
    private MltmdMvpInfoDAO mltmdMvpInfoDAO;
    
    @Resource(name="egovMltmdMvpInfoIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

    /**
	 * MltmdMvpId를 생성한다.
	 * @return MltmdMvpId를
	 * @exception Exception
	 */
    public String selectMltmdMvpId() throws Exception {
        return egovIdGnrService.getNextStringId();
    }

	/**
	 * COMTNMLTMDMVPINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
    	
    	mltmdMvpInfoDAO.insertMltmdMvpInfo(vo);
    		
        return null;
    }

    /**
	 * COMTNMLTMDMVPINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        mltmdMvpInfoDAO.updateMltmdMvpInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
        mltmdMvpInfoDAO.deleteMltmdMvpInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    public MltmdMvpInfoVO selectMltmdMvpInfo(MltmdMvpInfoVO vo) throws Exception {
    	return mltmdMvpInfoDAO.selectMltmdMvpInfo(vo);
    }

    /**
	 * 추천수를 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @return 조회한 COMTNMLTMDMVPINFO
	 * @exception Exception
	 */
    public int selectMltmdMvpInfoRecomend(MltmdMvpInfoVO vo) throws Exception {
        return mltmdMvpInfoDAO.selectMltmdMvpInfoRecomend(vo);
    }
    
    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception {
        return mltmdMvpInfoDAO.selectMltmdMvpInfoList(searchVO);
    }

    /**
	 * COMTNMLTMDMVPINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO) {
		return mltmdMvpInfoDAO.selectMltmdMvpInfoListTotCnt(searchVO);
	}
    
    /**
	 * 조회수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpInqire(MltmdMvpInfoVO vo) throws Exception {
    	mltmdMvpInfoDAO.updateMltmdMvpInqire(vo);
    }

    /**
	 * 추천수를 증가시킨다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpRecomend(MltmdMvpInfoVO vo) throws Exception {
    	mltmdMvpInfoDAO.updateMltmdMvpRecomend(vo);
    }
    
    /**
	 * 승인정보를 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpConfirm(MltmdMvpInfoVO vo) throws Exception {
    	mltmdMvpInfoDAO.updateMltmdMvpConfirm(vo);
    }
    
    /**
	 * 카테고리별 최신 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForCtgryNew(MltmdMvpInfoVO searchVO) throws Exception {
        return mltmdMvpInfoDAO.selectMltmdMvpInfoListForCtgryNew(searchVO);
    }
    
    /**
	 * 추천 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForRecomendBest(MltmdMvpInfoVO searchVO) throws Exception {
        return mltmdMvpInfoDAO.selectMltmdMvpInfoListForRecomendBest(searchVO);
    }
    
    /**
	 * 조회 베스트 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectMltmdMvpInfoListForInqireBest(MltmdMvpInfoVO searchVO) throws Exception {
        return mltmdMvpInfoDAO.selectMltmdMvpInfoListForInqireBest(searchVO);
    }
    
    /**
	 * 통합검색 목록
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpInfoVO> selectSearchMltmdMvpInfoList(MltmdMvpInfoVO searchVO) throws Exception {
        return mltmdMvpInfoDAO.selectSearchMltmdMvpInfoList(searchVO);
    }

    /**
	 * 통합검색 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPINFO 총 갯수
	 * @exception
	 */
    public int selectSearchMltmdMvpInfoListTotCnt(MltmdMvpInfoVO searchVO) {
        return mltmdMvpInfoDAO.selectSearchMltmdMvpInfoListTotCnt(searchVO);
    }
}
