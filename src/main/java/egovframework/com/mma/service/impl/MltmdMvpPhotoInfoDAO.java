package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpPhotoInfoVO;

/**
 * @Class Name : MltmdMvpPhotoInfoDAO.java
 * @Description : MltmdMvpPhotoInfo DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdMvpPhotoInfoDAO")
public class MltmdMvpPhotoInfoDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDMVPPHOTOINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
        return (String)insert("mltmdMvpPhotoInfoDAO.insertMltmdMvpPhotoInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
        update("mltmdMvpPhotoInfoDAO.updateMltmdMvpPhotoInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpPhotoInfo(MltmdFileDetailVO vo) throws Exception {
        delete("mltmdMvpPhotoInfoDAO.deleteMltmdMvpPhotoInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 조회한 COMTNMLTMDMVPPHOTOINFO
	 * @exception Exception
	 */
    public MltmdMvpPhotoInfoVO selectMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
        return (MltmdMvpPhotoInfoVO) selectByPk("mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfo", vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPPHOTOINFO 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdMvpPhotoInfoVO> selectMltmdMvpPhotoInfoList(MltmdFileVO searchVO) throws Exception {
        return list("mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfoList", searchVO);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDMVPPHOTOINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpPhotoInfoListTotCnt(MltmdFileVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfoListTotCnt", searchVO);
    }

}
