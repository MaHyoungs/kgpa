package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.com.mma.service.MltmdFileDetailVO;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpPhotoInfoService;
import egovframework.com.mma.service.MltmdMvpPhotoInfoVO;
import egovframework.com.mma.service.impl.MltmdMvpPhotoInfoDAO;

/**
 * @Class Name : MltmdMvpPhotoInfoServiceImpl.java
 * @Description : MltmdMvpPhotoInfo Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdMvpPhotoInfoService")
public class MltmdMvpPhotoInfoServiceImpl extends AbstractServiceImpl implements
        MltmdMvpPhotoInfoService {

    @Resource(name="mltmdMvpPhotoInfoDAO")
    private MltmdMvpPhotoInfoDAO mltmdMvpPhotoInfoDAO;
    
	/**
	 * COMTNMLTMDMVPPHOTOINFO을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
    	
    	mltmdMvpPhotoInfoDAO.insertMltmdMvpPhotoInfo(vo);
    	  	
        return null;
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
        mltmdMvpPhotoInfoDAO.updateMltmdMvpPhotoInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdMvpPhotoInfo(MltmdFileDetailVO vo) throws Exception {
        mltmdMvpPhotoInfoDAO.deleteMltmdMvpPhotoInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdMvpPhotoInfoVO
	 * @return 조회한 COMTNMLTMDMVPPHOTOINFO
	 * @exception Exception
	 */
    public MltmdMvpPhotoInfoVO selectMltmdMvpPhotoInfo(MltmdMvpPhotoInfoVO vo) throws Exception {
    	return mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfo(vo);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPPHOTOINFO 목록
	 * @exception Exception
	 */
    public List<MltmdMvpPhotoInfoVO> selectMltmdMvpPhotoInfoList(MltmdFileVO searchVO) throws Exception {
        return mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfoList(searchVO);
    }

    /**
	 * COMTNMLTMDMVPPHOTOINFO 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDMVPPHOTOINFO 총 갯수
	 * @exception
	 */
    public int selectMltmdMvpPhotoInfoListTotCnt(MltmdFileVO searchVO) {
		return mltmdMvpPhotoInfoDAO.selectMltmdMvpPhotoInfoListTotCnt(searchVO);
	}
    
}
