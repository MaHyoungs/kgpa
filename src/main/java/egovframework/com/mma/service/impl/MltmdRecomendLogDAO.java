package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdRecomendLogVO;

/**
 * @Class Name : MltmdRecomendLogDAO.java
 * @Description : MltmdRecomendLog DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdRecomendLogDAO")
public class MltmdRecomendLogDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDRECOMENDLOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdRecomendLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public void insertMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        insert("mltmdRecomendLogDAO.insertMltmdRecomendLog", vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        update("mltmdRecomendLogDAO.updateMltmdRecomendLog", vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        delete("mltmdRecomendLogDAO.deleteMltmdRecomendLog", vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdRecomendLogVO
	 * @return 조회한 COMTNMLTMDRECOMENDLOG
	 * @exception Exception
	 */
    public MltmdRecomendLogVO selectMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        return (MltmdRecomendLogVO) selectByPk("mltmdRecomendLogDAO.selectMltmdRecomendLog", vo);
    }
    
    /**
	 * COMTNMLTMDRECOMENDLOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdRecomendLogVO
	 * @return 조회한 COMTNMLTMDRECOMENDLOG
	 * @exception Exception
	 */
    public int selectMltmdRecomendLogExists(MltmdRecomendLogVO vo) throws Exception {
        return (Integer)selectByPk("mltmdRecomendLogDAO.selectMltmdRecomendLogExists", vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDRECOMENDLOG 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdRecomendLogVO> selectMltmdRecomendLogList(MltmdRecomendLogVO searchVO) throws Exception {
        return list("mltmdRecomendLogDAO.selectMltmdRecomendLogList", searchVO);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDRECOMENDLOG 총 갯수
	 * @exception
	 */
    public int selectMltmdRecomendLogListTotCnt(MltmdRecomendLogVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdRecomendLogDAO.selectMltmdRecomendLogListTotCnt", searchVO);
    }

}
