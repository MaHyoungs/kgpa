package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdRecomendLogService;
import egovframework.com.mma.service.MltmdRecomendLogVO;
import egovframework.com.mma.service.impl.MltmdRecomendLogDAO;

/**
 * @Class Name : MltmdRecomendLogServiceImpl.java
 * @Description : MltmdRecomendLog Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdRecomendLogService")
public class MltmdRecomendLogServiceImpl extends AbstractServiceImpl implements
        MltmdRecomendLogService {

    @Resource(name="mltmdRecomendLogDAO")
    private MltmdRecomendLogDAO mltmdRecomendLogDAO;
    
    @Resource(name="egovMltmdRecomendIdGnrService")    
    private EgovIdGnrService egovIdGnrService;
    
	/**
	 * COMTNMLTMDRECOMENDLOG을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdRecomendLogVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public int insertMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
    	vo.setRecomendlogId(egovIdGnrService.getNextStringId());
    	mltmdRecomendLogDAO.insertMltmdRecomendLog(vo);
    	return mltmdRecomendLogDAO.selectMltmdRecomendLogExists(vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        mltmdRecomendLogDAO.updateMltmdRecomendLog(vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdRecomendLogVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
        mltmdRecomendLogDAO.deleteMltmdRecomendLog(vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdRecomendLogVO
	 * @return 조회한 COMTNMLTMDRECOMENDLOG
	 * @exception Exception
	 */
    public MltmdRecomendLogVO selectMltmdRecomendLog(MltmdRecomendLogVO vo) throws Exception {
    	return mltmdRecomendLogDAO.selectMltmdRecomendLog(vo);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDRECOMENDLOG 목록
	 * @exception Exception
	 */
    public List<MltmdRecomendLogVO> selectMltmdRecomendLogList(MltmdRecomendLogVO searchVO) throws Exception {
        return mltmdRecomendLogDAO.selectMltmdRecomendLogList(searchVO);
    }

    /**
	 * COMTNMLTMDRECOMENDLOG 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDRECOMENDLOG 총 갯수
	 * @exception
	 */
    public int selectMltmdRecomendLogListTotCnt(MltmdRecomendLogVO searchVO) {
		return mltmdRecomendLogDAO.selectMltmdRecomendLogListTotCnt(searchVO);
	}
    
}
