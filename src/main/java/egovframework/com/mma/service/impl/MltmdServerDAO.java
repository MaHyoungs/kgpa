package egovframework.com.mma.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.mma.service.MltmdServerVO;

/**
 * @Class Name : MltmdServerDAO.java
 * @Description : MltmdServer DAO Class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("mltmdServerDAO")
public class MltmdServerDAO extends EgovAbstractDAO {

	/**
	 * COMTNMLTMDSERVER을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdServerVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdServer(MltmdServerVO vo) throws Exception {
        return (String)insert("mltmdServerDAO.insertMltmdServer", vo);
    }

    /**
	 * COMTNMLTMDSERVER을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdServerVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdServer(MltmdServerVO vo) throws Exception {
        update("mltmdServerDAO.updateMltmdServer", vo);
    }

    /**
	 * COMTNMLTMDSERVER을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdServerVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdServer(MltmdServerVO vo) throws Exception {
        delete("mltmdServerDAO.deleteMltmdServer", vo);
    }

    /**
	 * COMTNMLTMDSERVER을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdServerVO
	 * @return 조회한 COMTNMLTMDSERVER
	 * @exception Exception
	 */
    public MltmdServerVO selectMltmdServer(MltmdServerVO vo) throws Exception {
        return (MltmdServerVO) selectByPk("mltmdServerDAO.selectMltmdServer", vo);
    }

    /**
	 * COMTNMLTMDSERVER 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDSERVER 목록
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
    public List<MltmdServerVO> selectMltmdServerList(MltmdServerVO searchVO) throws Exception {
        return list("mltmdServerDAO.selectMltmdServerList", searchVO);
    }

    /**
	 * COMTNMLTMDSERVER 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return COMTNMLTMDSERVER 총 갯수
	 * @exception
	 */
    public int selectMltmdServerListTotCnt(MltmdServerVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("mltmdServerDAO.selectMltmdServerListTotCnt", searchVO);
    }

}
