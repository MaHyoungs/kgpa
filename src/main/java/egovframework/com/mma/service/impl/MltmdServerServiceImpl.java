package egovframework.com.mma.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.mma.service.MltmdServerService;
import egovframework.com.mma.service.MltmdServerVO;
import egovframework.com.mma.service.impl.MltmdServerDAO;

/**
 * @Class Name : MltmdServerServiceImpl.java
 * @Description : MltmdServer Business Implement class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("mltmdServerService")
public class MltmdServerServiceImpl extends AbstractServiceImpl implements
        MltmdServerService {

    @Resource(name="mltmdServerDAO")
    private MltmdServerDAO mltmdServerDAO;
    
    @Resource(name="egovMltmdServerIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * COMTNMLTMDSERVER을 등록한다.
	 * @param vo - 등록할 정보가 담긴 MltmdServerVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertMltmdServer(MltmdServerVO vo) throws Exception {
    	    	
    	vo.setServerId(egovIdGnrService.getNextStringId());
    	
    	mltmdServerDAO.insertMltmdServer(vo);
    	
        return null;
    }

    /**
	 * COMTNMLTMDSERVER을 수정한다.
	 * @param vo - 수정할 정보가 담긴 MltmdServerVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateMltmdServer(MltmdServerVO vo) throws Exception {
        mltmdServerDAO.updateMltmdServer(vo);
    }

    /**
	 * COMTNMLTMDSERVER을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 MltmdServerVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteMltmdServer(MltmdServerVO vo) throws Exception {
        mltmdServerDAO.deleteMltmdServer(vo);
    }

    /**
	 * COMTNMLTMDSERVER을 조회한다.
	 * @param vo - 조회할 정보가 담긴 MltmdServerVO
	 * @return 조회한 COMTNMLTMDSERVER
	 * @exception Exception
	 */
    public MltmdServerVO selectMltmdServer(MltmdServerVO vo) throws Exception {
    	return mltmdServerDAO.selectMltmdServer(vo);
    }

    /**
	 * COMTNMLTMDSERVER 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDSERVER 목록
	 * @exception Exception
	 */
    public List<MltmdServerVO> selectMltmdServerList(MltmdServerVO searchVO) throws Exception {
        return mltmdServerDAO.selectMltmdServerList(searchVO);
    }

    /**
	 * COMTNMLTMDSERVER 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNMLTMDSERVER 총 갯수
	 * @exception
	 */
    public int selectMltmdServerListTotCnt(MltmdServerVO searchVO) {
		return mltmdServerDAO.selectMltmdServerListTotCnt(searchVO);
	}
    
}
