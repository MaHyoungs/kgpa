package egovframework.com.mma.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.mma.service.MltmdFileService;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoService;
import egovframework.com.mma.service.MltmdMvpPhotoInfoService;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name : EgovMltmdCtgryController.java
 * @Description : EgovMltmdCtgryController class
 * @Modification Information
 *
 * @author 이엠티
 * @since 2011.12.15
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdCommonController {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	@Resource(name = "mltmdEnvrnSetupService")
    private MltmdEnvrnSetupService mltmdEnvrnSetupService;
	
	@Resource(name = "mltmdFileService")
    private MltmdFileService mltmdFileService;
	
	@Resource(name = "mltmdMvpPhotoInfoService")
    private MltmdMvpPhotoInfoService mltmdMvpPhotoInfoService;
	
	@Resource(name = "mltmdMvpCnvrInfoService")
    private MltmdMvpCnvrInfoService mltmdMvpCnvrInfoService;
	
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	    
    @RequestMapping("/mma/uploaderInfo.do")
    public String uploaderInfo(@RequestParam("mltmdClCode") String mltmdClCode, Model model) throws Exception {
        
    	if("MVP".equals(mltmdClCode)) {
    		model.addAttribute("UploadWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
    		model.addAttribute("UploadHandlerName", propertiesService.getString("mediaMovieUploderHandlerUrl"));
    	} else if("PHOTO".equals(mltmdClCode)) {
    		model.addAttribute("UploadWebUrl", propertiesService.getString("mediaPhotoImageWebUrl"));
    		model.addAttribute("UploadHandlerName", propertiesService.getString("mediaPhotoUploderHandlerUrl"));
    	} else if("AUDIO".equals(mltmdClCode)) {
    		model.addAttribute("UploadWebUrl", propertiesService.getString("mediaAudioWebUrl"));
    		model.addAttribute("UploadHandlerName", propertiesService.getString("mediaAudioUploderHandlerUrl"));
    	} else if("DOC".equals(mltmdClCode)) {
    		model.addAttribute("UploadWebUrl", propertiesService.getString("mediaDocWebUrl"));
    		model.addAttribute("UploadHandlerName", propertiesService.getString("mediaDocUploderHandlerUrl"));
    	}
    	
    	return "/mma/uploaderInfo";
    }
    
    @RequestMapping("/mma/mvpMediaInfo.do")
    public String movieMediaInfo(Map<String, Object> commandMap, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	String siteId = (String)commandMap.get("siteId");
    	String mltmdFileId = (String)commandMap.get("mltmdFileId");
    	//String mltmdFileDetailId = (String)commandMap.get("mltmdFileDetailId");

    	if(EgovStringUtil.isEmpty(siteId)) {
    		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    		siteId = siteVO.getSiteId();
    	}
    	
    	if(!EgovStringUtil.isEmpty(mltmdFileId)) {
    		
    		MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
        	setupVO.setSiteId(siteId);    	
        	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
        	model.addAttribute("mltmdSetup", setupVO);
        	
    		MltmdFileVO mltmdFileVO = new MltmdFileVO();
        	mltmdFileVO.setMltmdFileId(mltmdFileId);
        	
        	//파일마스터 정보(변환정보)
        	model.addAttribute("mltmdFile", mltmdFileService.selectMltmdFile(mltmdFileVO));
        	//변환 파일정보
        	model.addAttribute("mltmdMvpCnvrInfo", mltmdMvpCnvrInfoService.selectMltmdMvpCnvrInfo(mltmdFileVO));    		
        	//동영상 사진정보
	    	model.addAttribute("moviePhotoList" , mltmdMvpPhotoInfoService.selectMltmdMvpPhotoInfoList(mltmdFileVO));
	    	//동영상 이미지 웹경로.
	    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));	    	
	    	//동영상 서비스 경로
	    	model.addAttribute("mediaMovieServiceWebUrl", propertiesService.getString("mediaMovieServiceWebUrl"));
	    	//워터마크 이미지 웹경로.
	    	model.addAttribute("SiteFileStoreWebPath", propertiesService.getString("Site.fileStoreWebPath"));
	    	
    	}
    	
    	return "/mma/mvpMediaInfo";
    }
    
    @RequestMapping("/mma/mvpProgress.do")
    public String mvpProgress(MltmdFileVO vo, Model model) throws Exception {
        
    	//파일 마스터 정보
    	model.addAttribute("result" , mltmdFileService.selectMltmdFile(vo));
    	
    	return "/mma/mediaProgress";
    }

}
