package egovframework.com.mma.web;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name : EgovMltmdCtgryController.java
 * @Description : EgovMltmdCtgryController class
 * @Modification Information
 *
 * @author 이엠티
 * @since 2011.12.15
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdCtgryController {

	@Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
	    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
     * 하위카테로리를 조회한다.
     * 
     * @param MltmdCtgryVO
     * @param searchVO
     * @throws Exception
     */
    @RequestMapping("/mma/selectCtgryListForAjax.do")
    public void selectCtgryListForAjax(@ModelAttribute("searchVO") MltmdCtgryVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
      
  	  JSONArray ja = new JSONArray();
  	  JSONObject jo = null;
  	  List<MltmdCtgryVO> list = mltmdCtgryService.selectMltmdCtgryList(searchVO);
  	  if(list != null && list.size() > 0) {
  		  for(int i = 0; i < list.size(); i++) {
  			  jo = new JSONObject();
  			  jo.put("upperCtgryId", list.get(i).getUpperCtgryId());
  			  jo.put("ctgryId", list.get(i).getCtgryId());
  			  jo.put("ctgryNm", list.get(i).getCtgryNm());
  			  
  			  ja.add(jo);
  		  }
  	  }
  	  
  	  response.setContentType("text/javascript; charset=utf-8");
  	  PrintWriter printwriter = response.getWriter();
  	  printwriter.println(ja.toString());
  	  printwriter.flush();
  	  printwriter.close();
    }
    
}
