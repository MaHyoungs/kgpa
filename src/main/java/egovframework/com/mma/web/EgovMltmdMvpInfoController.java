package egovframework.com.mma.web;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.mma.service.MltmdFileService;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdInqireLogService;
import egovframework.com.mma.service.MltmdInqireLogVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoService;
import egovframework.com.mma.service.MltmdMvpInfoService;
import egovframework.com.mma.service.MltmdMvpInfoVO;
import egovframework.com.mma.service.MltmdMvpPhotoInfoService;
import egovframework.com.mma.service.MltmdRecomendLogService;
import egovframework.com.mma.service.MltmdRecomendLogVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : EgovMltmdMvpInfoController.java
 * @Description : MltmdMvpInfo Controller class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdMvpInfoController {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
    @Resource(name = "mltmdMvpInfoService")
    private MltmdMvpInfoService mltmdMvpInfoService;
    
    @Resource(name = "mltmdEnvrnSetupService")
    private MltmdEnvrnSetupService mltmdEnvrnSetupService;
    
    @Resource(name = "mltmdMvpCnvrInfoService")
    private MltmdMvpCnvrInfoService mltmdMvpCnvrInfoService;
    
    @Resource(name = "mltmdFileService")
    private MltmdFileService mltmdFileService;
    
    @Resource(name = "mltmdInqireLogService")
    private MltmdInqireLogService mltmdInqireLogService;
    
    @Resource(name = "mltmdRecomendLogService")
    private MltmdRecomendLogService mltmdRecomendLogService;
    
    @Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
    
    @Resource(name = "mltmdMvpPhotoInfoService")
    private MltmdMvpPhotoInfoService mltmdMvpPhotoInfoService;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
    
    @Autowired
	private DefaultBeanValidator beanValidator;
	
    /**
     * XSS 방지 처리.
     * 
     * @param data
     * @return
     */
    protected String unscript(String data) {
      if(data == null || data.trim().equals("")) {
        return "";
      }
      
      String ret = data;
      
      ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
      ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");
      
      //ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
      //ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");
      
      ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
      ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");
      
      // ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
      // ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
      
      ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      
      return ret;
    }
        
    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @exception Exception
	 */
    @RequestMapping(value="/mma/MltmdMvpInfoList.do")
    public String selectMltmdMvpInfoList(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
		
    	searchVO.setPageUnit(propertiesService.getInt("mmaPageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("mmaPageSize"));
    	
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
    	
    	List<MltmdMvpInfoVO> mltmdMvpInfoList = mltmdMvpInfoService.selectMltmdMvpInfoList(searchVO);
        model.addAttribute("resultList", mltmdMvpInfoList);
        
        int totCnt = mltmdMvpInfoService.selectMltmdMvpInfoListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
		
        model.addAttribute("paginationInfo", paginationInfo);
        
        //동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
        
        return "/mma/EgovMltmdMvpInfoList";
    } 
    
    @RequestMapping("/mma/selectMltmdMvpInfo.do")
    public String selectMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
		
		MltmdMvpInfoVO mvpInfo = mltmdMvpInfoService.selectMltmdMvpInfo(searchVO);
    	model.addAttribute("mltmdMvpInfoVO", mvpInfo);
    	
    	MltmdFileVO mltmdFileVO = new MltmdFileVO();
    	mltmdFileVO.setMltmdFileId(mvpInfo.getMltmdFileId());
    	model.addAttribute("mltmdMvpCnvrInfo", mltmdMvpCnvrInfoService.selectMltmdMvpCnvrInfo(mltmdFileVO));
    	
    	model.addAttribute("mediaMovieServiceWebUrl", propertiesService.getString("mediaMovieServiceWebUrl"));
    	//동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
    	
    	//조회 로그 기록
    	MltmdInqireLogVO inqireLogVO = new MltmdInqireLogVO();
    	inqireLogVO.setMltmdId(searchVO.getMltmdMvpId());
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	inqireLogVO.setIqrrsnId(user != null ? user.getId() : request.getSession().getId());
    	if(mltmdInqireLogService.insertMltmdInqireLog(inqireLogVO) > 0) {
    		mltmdMvpInfoService.updateMltmdMvpInqire(searchVO);
    	}
    	//조회 로그 기록
    	
        return "/mma/EgovMltmdMvpInfoInqire";
    }
    
    @RequestMapping("/mma/addMltmdMvpInfo.do")
    public String addMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  	  	if(user == null) {
  	  		return "forward:/mma/index.do";
  	  	}
  	  
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
    	
    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());
		ctgry.setSearchLevel("1");
		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
    	
		searchVO.setTakeMvpId(mltmdMvpInfoService.selectMltmdMvpId());
    	model.addAttribute("mltmdMvpInfoVO", searchVO);
    	
    	MltmdFileVO mvpFileVO = new MltmdFileVO();
    	mvpFileVO.setMvpBassCours(mltmdFileService.selectMltmdFileBassCours());
    	model.addAttribute("mvpFileVO", mvpFileVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mma/EgovMltmdMvpInfoRegister";
    }
    
    @RequestMapping("/mma/insertMltmdMvpInfo.do")
    public String insertMltmdMvpInfo(MltmdFileVO mltmdFileVO, MltmdMvpInfoVO mltmdMvpInfoVO, BindingResult bindingResult, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	beanValidator.validate(mltmdMvpInfoVO, bindingResult);
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
  	  	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
  	  	setupVO.setSiteId(searchVO.getSiteId());    	
  	  	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
	  	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
	  	}
  	
    	if(bindingResult.hasErrors()) {
    		
    		MltmdCtgryVO ctgry = new MltmdCtgryVO();
    		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());
    		ctgry.setSearchLevel("1");
    		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
    		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
    		
    		return "/mma/EgovMltmdMvpInfoRegister";
    	}
    	
    	
    	Map<String, String> fileIds  = mltmdFileService.saveData(mltmdFileVO);
    	if(fileIds != null) {
    		if(fileIds.get("mvpFileId") != null) {
    			mltmdFileVO.setMvpFileId(fileIds.get("mvpFileId"));
	    	}
    	}
    	mltmdMvpInfoVO.setMltmdFileId(mltmdFileVO.getMvpFileId());
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);    
    	if(setupVO != null) {
	    	mltmdMvpInfoVO.setConfmAt(setupVO.getAtmcConfmAt());
	    	if("Y".equals(setupVO.getAtmcConfmAt())) {
	    		mltmdMvpInfoVO.setConfmerId("SYSTEM");
	    	}
    	}
    	mltmdMvpInfoVO.setFrstRegisterId(user.getId());
    	mltmdMvpInfoVO.setWrterNm(user.getName());
    	mltmdMvpInfoVO.setMvpCn(unscript(mltmdMvpInfoVO.getMvpCn()));
    	
    	mltmdMvpInfoService.insertMltmdMvpInfo(mltmdMvpInfoVO);
    	
    	request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mma/forUpdateMltmdMvpInfo.do")
    public String forUpdateMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  	  	if(user == null) {
  	  		return "forward:/mma/index.do";
  	  	}
  	  	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
    	
    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());
		ctgry.setSearchLevel("1");
		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
    	
		MltmdMvpInfoVO mvpInfo = mltmdMvpInfoService.selectMltmdMvpInfo(searchVO);
    	model.addAttribute("mltmdMvpInfoVO", mvpInfo);
    	
    	MltmdFileVO mvpFileVO = new MltmdFileVO();
    	mvpFileVO.setMltmdFileId(mvpInfo.getMltmdFileId());
    	mvpFileVO = mltmdFileService.selectMltmdFile(mvpFileVO);
    	mvpFileVO.setMvpFileId(mvpFileVO.getMltmdFileId());
    	mvpFileVO.setMvpBassCours(mvpFileVO.getBassCours());
    	mvpFileVO.setMvpJsonData(mltmdFileService.selectMediaMovieJsonData(mvpFileVO));
    	model.addAttribute("mvpFileVO", mvpFileVO);
    	
    	request.getSession().setAttribute("sessionVO", searchVO);
    	
        return "/mma/EgovMltmdMvpInfoRegister";
    }

    @RequestMapping("/mma/updateMltmdMvpInfo.do")
    public String updateMltmdMvpInfo(MltmdMvpInfoVO mltmdMvpInfoVO, BindingResult bindingResult, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	beanValidator.validate(mltmdMvpInfoVO, bindingResult);
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
    	if(bindingResult.hasErrors()) {
    		
    		MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
        	setupVO.setSiteId(searchVO.getSiteId());    	
        	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
        	if(setupVO != null) {
    			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
        	}
        	
        	MltmdCtgryVO ctgry = new MltmdCtgryVO();
    		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());
    		ctgry.setSearchLevel("1");
    		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
    		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
        	
    		model.addAttribute("mltmdMvpInfoVO", mltmdMvpInfoService.selectMltmdMvpInfo(searchVO));
    		
    		return "/mma/EgovMltmdMvpInfoRegister";
    	}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);  
    	if("10".equals(user.getUserSe())) {
	  		mltmdMvpInfoVO.setAdminAt("Y");
		}
    	mltmdMvpInfoVO.setLastUpdusrId(user.getId());
    	mltmdMvpInfoVO.setMvpCn(unscript(mltmdMvpInfoVO.getMvpCn()));
    	mltmdMvpInfoService.updateMltmdMvpInfo(mltmdMvpInfoVO);
    	
    	request.getSession().removeAttribute("sessionVO");
                
        return "forward:/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mma/deleteMltmdMvpInfo.do")
    public String deleteMltmdMvpInfo(MltmdMvpInfoVO mltmdMvpInfoVO, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  	  	if(user == null) {
  	  		return "forward:/mma/index.do";
  	  	}
  	  	
	  	if("10".equals(user.getUserSe())) {
	  		mltmdMvpInfoVO.setAdminAt("Y");
		}
  	  	
    	mltmdMvpInfoVO.setLastUpdusrId(user.getId());
    	mltmdMvpInfoService.deleteMltmdMvpInfo(mltmdMvpInfoVO);
        
        return "forward:/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mma/insertMltmdMvpRecomend.do")
    public String insertMltmdMvpRecomend(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
    	insertMltmdMvpRecomend(searchVO, request, response);
    	
    	
        return "forward:/mma/selectMltmdMvpInfo.do";
    }
    
    @RequestMapping("/mma/insertMltmdMvpRecomendForAjax.do")
    public void insertMltmdMvpRecomendForAjax(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
    	int iResult = insertMltmdMvpRecomend(searchVO, request, response);
    	
    	JSONObject jo = new JSONObject();
    	jo.put("applyAt", iResult == 0 ? "N" : "Y");
    	jo.put("recomendCo", mltmdMvpInfoService.selectMltmdMvpInfoRecomend(searchVO));

    	response.setContentType("text/javascript; charset=utf-8");
    	PrintWriter printwriter = response.getWriter();
    	printwriter.println(jo.toString());
    	printwriter.flush();
    	printwriter.close();
    }
    
    private int insertMltmdMvpRecomend(MltmdMvpInfoVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	int iResult = 0;
    	//추천 로그 기록
    	MltmdRecomendLogVO recomendLogVO = new MltmdRecomendLogVO();
    	recomendLogVO.setMltmdId(searchVO.getMltmdMvpId());
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	recomendLogVO.setRecomendrId(user != null ? user.getId() : request.getSession().getId());
    	iResult = mltmdRecomendLogService.insertMltmdRecomendLog(recomendLogVO);
    	if(iResult > 0) {
    		mltmdMvpInfoService.updateMltmdMvpRecomend(searchVO);
    	}
    	//추천 로그 기록
    	
    	return iResult;
    }
    
    
    
    
    
    
    
    
    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @exception Exception
	 */
    @RequestMapping(value="/mma/MblMltmdMvpInfoList.do")
    public String selectMblMltmdMvpInfoList(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
		
    	searchVO.setPageUnit(propertiesService.getInt("mmaPageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("mmaPageSize"));
    	
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
    	
    	List<MltmdMvpInfoVO> mltmdMvpInfoList = mltmdMvpInfoService.selectMltmdMvpInfoList(searchVO);
        model.addAttribute("resultList", mltmdMvpInfoList);
        
        int totCnt = mltmdMvpInfoService.selectMltmdMvpInfoListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
		
        model.addAttribute("paginationInfo", paginationInfo);
		
        //동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
    	
    	
    	//카테고리
        MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());		
		List<MltmdCtgryVO> cateList = mltmdCtgryService.selectMltmdCtgryList(ctgry);
		model.addAttribute("mltmdCateList", cateList);
        
        return "/mma/EgovMblMltmdMvpInfoList";
    } 
    
    @RequestMapping("/mma/selectMblMltmdMvpInfo.do")
    public String selectMblMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
       
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	searchVO.setSiteId(siteVO.getSiteId());
  	  	
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	if(setupVO != null) {
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
    	}
		
		MltmdMvpInfoVO mvpInfo = mltmdMvpInfoService.selectMltmdMvpInfo(searchVO);
    	model.addAttribute("mltmdMvpInfoVO", mvpInfo);
    	
    	MltmdFileVO mltmdFileVO = new MltmdFileVO();
    	mltmdFileVO.setMltmdFileId(mvpInfo.getMltmdFileId());
    	model.addAttribute("mltmdMvpCnvrInfo", mltmdMvpCnvrInfoService.selectMltmdMvpCnvrInfo(mltmdFileVO));
    	//동영상 사진정보
    	model.addAttribute("moviePhotoList" , mltmdMvpPhotoInfoService.selectMltmdMvpPhotoInfoList(mltmdFileVO));
    	
    	model.addAttribute("mediaMovieServiceWebUrl", propertiesService.getString("mediaMovieServiceWebUrl"));
    	//동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
    	
    	//조회 로그 기록
    	MltmdInqireLogVO inqireLogVO = new MltmdInqireLogVO();
    	inqireLogVO.setMltmdId(searchVO.getMltmdMvpId());
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	inqireLogVO.setIqrrsnId(user != null ? user.getId() : request.getSession().getId());
    	if(mltmdInqireLogService.insertMltmdInqireLog(inqireLogVO) > 0) {
    		mltmdMvpInfoService.updateMltmdMvpInqire(searchVO);
    	}
    	//조회 로그 기록
    	
    	
    	
    	//카테고리
        MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());		
		List<MltmdCtgryVO> cateList = mltmdCtgryService.selectMltmdCtgryList(ctgry);
		model.addAttribute("mltmdCateList", cateList);
		
		if(!EgovStringUtil.isEmpty(searchVO.getSearchCate())) {
			MltmdCtgryVO currCtgry = null;
			if(cateList != null && cateList.size() > 0) {
				for(int i = 0; i < cateList.size(); i++) {
					if(cateList.get(i).getCtgryId().equals(searchVO.getSearchCate())) {
						currCtgry = cateList.get(i);
						break;
					}
				}
			}
			if(currCtgry != null) {
				model.addAttribute("currCtgry", currCtgry);
			}
			
			MltmdCtgryVO currRootCtgry = null;			
			if(currCtgry != null) {
				if(cateList != null && cateList.size() > 0) {
					for(int i = 0; i < cateList.size(); i++) {
						if(cateList.get(i).getCtgryLevel() == 1 && cateList.get(i).getCtgryId().equals(currCtgry.getCtgryPathById().split(">")[1])) {
							currRootCtgry = cateList.get(i);
							break;
						}
					}
				}
	    	}
			if(currRootCtgry != null) {
				model.addAttribute("currRootCtgry", currRootCtgry);
			}
		}
		
        return "/mma/EgovMblMltmdMvpInfoInqire";
    }
}
