package egovframework.com.mma.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.rte.fdl.property.EgovPropertyService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.mma.service.MltmdMvpInfoService;
import egovframework.com.mma.service.MltmdMvpInfoVO;
import egovframework.com.sym.mpm.service.Mpm;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : EgovMltmdMvpInfoController.java
 * @Description : MltmdMvpInfo Controller class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller("mmaTemplateServiceController")
public class TemplateServiceController {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
    @Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
    
    @Resource(name = "mltmdEnvrnSetupService")
    private MltmdEnvrnSetupService mltmdEnvrnSetupService;
    
    @Resource(name = "mltmdMvpInfoService")
    private MltmdMvpInfoService mltmdMvpInfoService;
    
    @Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;
    
    @Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;
    
    @Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;
    
    @Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
        
    /**
	 * 메인화면을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @exception Exception
	 */
    @RequestMapping(value="/mma/index.do")
    public String index(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));
  	  	
  	  	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
	  	setupVO.setSiteId(siteVO.getSiteId());    	
	  	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
	  	model.addAttribute("mltmdSetup", setupVO);
	  	
	  	if(setupVO != null) {
	  	  	MltmdCtgryVO ctgry = new MltmdCtgryVO();
	  	  	ctgry.setSearchLevel("1");
			ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());			
			List<MltmdCtgryVO> cateList = mltmdCtgryService.selectMltmdCtgryList(ctgry);
			
			MltmdMvpInfoVO mvpInfoVO = new MltmdMvpInfoVO();
			mvpInfoVO.setSiteId(siteVO.getSiteId());
			mvpInfoVO.setCtgrymasterId(setupVO.getCtgrymasterId());
			mvpInfoVO.setFirstIndex(0);
			mvpInfoVO.setRecordCountPerPage(4);
			if(cateList != null && cateList.size() > 0) {
				for(int i = 0; i < cateList.size(); i++) {
					mvpInfoVO.setSearchCate(cateList.get(i).getCtgryId());
					cateList.get(i).setMvpInfoList(mltmdMvpInfoService.selectMltmdMvpInfoListForCtgryNew(mvpInfoVO));
				}
				//카테고리별 동영상
				model.addAttribute("mltmdCateList", cateList);
			}
			
			
			//인기동영상
			List<MltmdMvpInfoVO> inqireBestList = mltmdMvpInfoService.selectMltmdMvpInfoListForInqireBest(mvpInfoVO);
			MltmdMvpInfoVO inqire = null;
			for(int i = 0; i < inqireBestList.size(); i++) {
				inqire = inqireBestList.get(i);
				inqire.setMvpCn(EgovStringUtil.getNoneHtml(inqire.getMvpCn()));
        	}
			model.addAttribute("inqireBestList", inqireBestList);
			//추천동영상
			mvpInfoVO.setRecordCountPerPage(12);
			model.addAttribute("recomendBestList", mltmdMvpInfoService.selectMltmdMvpInfoListForRecomendBest(mvpInfoVO));
			
			model.addAttribute("mediaMovieImageWebUrl", propertyService.getString("mediaMovieImageWebUrl"));
			
	  	}
	  	
	  	BoardMasterVO boardMasterVO = new BoardMasterVO();
	  	boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(9999999);
		boardMasterVO.setSiteId(siteVO.getSiteId());
		boardMasterVO.setTrgetId("MMAMVP_SERVICE_BOARD");
		List<BoardMasterVO> bbsList = bbsAttrbService.selectBBSMasterList(boardMasterVO);
		/*
		if(bbsList != null && bbsList.size() > 0) {
			BoardVO boardVO = new BoardVO();
			for(int i = 0; i < bbsList.size(); i++) {
				boardVO.setBbsId(bbsList.get(i).getBbsId());
				bbsList.get(i).setNttList(bbsMngService.selectBoardArticles(boardVO));
			}
		}*/
		
		if(bbsList != null && bbsList.size() > 0) {
			BoardVO boardVO = new BoardVO();
			boardVO.setBbsId(bbsList.get(0).getBbsId());
			boardVO.setFirstIndex(0);
			boardVO.setRecordCountPerPage(5);
			List<BoardVO> bbsResultList = bbsMngService.selectBoardArticles(boardVO);
			BoardVO resetVO = null;
			for(int i = 0; i < bbsResultList.size(); i++) {
        		resetVO = bbsResultList.get(i);
        		resetVO.setNttCn(EgovStringUtil.getNoneHtml(resetVO.getNttCn()));
        	}
			bbsList.get(0).setNttList(bbsResultList);
			model.addAttribute("bbsList", bbsList);
		}
		
		
    	return "/msi/mma/index";
    }
    
    @RequestMapping(value = "/mma/tmplatHead.do")
	public String tmplatUpper(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, BoardMasterVO bbsVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  
  	  	model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));
  	  	
  	  	if(EgovStringUtil.isEmpty(searchVO.getCtgrymasterId())) {
	  	  	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
	    	setupVO.setSiteId(siteVO.getSiteId());    	
	    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
	    	if(setupVO != null) {
				searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
	    	}
  	  	}
  	  	
  	  	MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(searchVO.getCtgrymasterId());
		
		List<MltmdCtgryVO> cateList = mltmdCtgryService.selectMltmdCtgryList(ctgry);
		model.addAttribute("mltmdCateList", cateList);
		
		//인기동영상
		searchVO.setSiteId(siteVO.getSiteId());
		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(5);
		List<MltmdMvpInfoVO> inqireBestList = mltmdMvpInfoService.selectMltmdMvpInfoListForInqireBest(searchVO);
		MltmdMvpInfoVO inqire = null;
		for(int i = 0; i < inqireBestList.size(); i++) {
			inqire = inqireBestList.get(i);
			inqire.setMvpCn(EgovStringUtil.getNoneHtml(inqire.getMvpCn()));
    	}
		model.addAttribute("inqireBestList", inqireBestList);
		
		if(!EgovStringUtil.isEmpty(searchVO.getSearchCate())) {
			MltmdCtgryVO currCtgry = null;
			if(cateList != null && cateList.size() > 0) {
				for(int i = 0; i < cateList.size(); i++) {
					if(cateList.get(i).getCtgryId().equals(searchVO.getSearchCate())) {
						currCtgry = cateList.get(i);
						break;
					}
				}
			}
			if(currCtgry != null) {
				model.addAttribute("currCtgry", currCtgry);
			}
			
			MltmdCtgryVO currRootCtgry = null;			
			if(currCtgry != null) {
				if(cateList != null && cateList.size() > 0) {
					for(int i = 0; i < cateList.size(); i++) {
						if(cateList.get(i).getCtgryLevel() == 1 && cateList.get(i).getCtgryId().equals(currCtgry.getCtgryPathById().split(">")[1])) {
							currRootCtgry = cateList.get(i);
							break;
						}
					}
				}
	    	}
			if(currRootCtgry != null) {
				model.addAttribute("currRootCtgry", currRootCtgry);
			}
		}
	  	
	  	BoardMasterVO boardMasterVO = new BoardMasterVO();
	  	boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(9999999);
		boardMasterVO.setSiteId(siteVO.getSiteId());
		boardMasterVO.setTrgetId("MMAMVP_SERVICE_BOARD");
		List<BoardMasterVO> bbsList = bbsAttrbService.selectBBSMasterList(boardMasterVO);
	  	model.addAttribute("bbsList", bbsList);
		
	  	if(!EgovStringUtil.isEmpty(bbsVO.getBbsId())) {
	  		BoardMasterVO currBBS = null;
			if(bbsList != null && bbsList.size() > 0) {
				for(int i = 0; i < bbsList.size(); i++) {
					if(bbsList.get(i).getBbsId().equals(bbsVO.getBbsId())) {
						currBBS = bbsList.get(i);
						break;
					}
				}
				model.addAttribute("currBBS", currBBS);
			}
	  	}
	  	model.addAttribute("mediaMovieImageWebUrl", propertyService.getString("mediaMovieImageWebUrl"));
	  	
    	return "/msi/mma/tmplatHead";
    }
    
    @RequestMapping(value = "/mma/tmplatBottom.do")
	public String tmplatBottom(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);  	  
  	  	model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));
  	  	
    	return "/msi/mma/tmplatBottom";
    }
}
