package egovframework.com.mng.cop.bbs.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import egovframework.com.gfund.biz.bassinfo.service.BasicInformationService;
import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Board;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.mpm.service.MpmVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 게시물 관리를 위한 컨트롤러 클래스
 *
 * @author 공통서비스개발팀 이삼섭
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------       --------    ---------------------------
 *   2009.3.19  이삼섭          최초 생성
 *   2009.06.29	한성곤
 *
 * </pre>
 * @since 2009.06.01
 */
@Controller("mngEgovBBSManageController")
public class EgovBBSManageController{
	@Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;

	@Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;

	@Resource(name = "EgovBBSCtgryService")
	private EgovBBSCtgryService ctgryService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "EgovMpmService")
	private EgovMpmService egovMpmService;

	@Autowired
	private DefaultBeanValidator beanValidator;

	@Resource(name = "basicInformationService")
	private BasicInformationService basicInformationService;

	Logger log = LogManager.getLogger(this.getClass());

	/**
	 * XSS 방지 처리.
	 *
	 * @param data
	 * @return
	 */
	protected String unscript(String data){
		if(data == null || data.trim().equals("")){
			return "";
		}

		String ret = data;

		ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
		ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

		//ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
		//ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

		ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
		ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

		// ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
		// ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

		ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
		ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

		return ret;
	}

	/**
	 * 정보공개 목록 게시판
	 * @return
	 */
	@RequestMapping("/mng/cop/bbs/selectOpenBoardList.do")
	public String selectOpenBoard(){
		return "mng/cop/bbs/EgovBoardOpen";
	}

	/**
	 * 녹색자금통합관리 - 공지사항 게시판
	 * @return
	 */
	@RequestMapping("/mng/cop/bbs/gfundNoticeList.do")
	public String gfundNoticeList(){
		return "mng/gfund/bbs/gfundNoticeList";
	}

	/**
	 * 녹색자금통합관리 - Q&A 게시판
	 * @return
	 */
	@RequestMapping("/mng/cop/bbs/gfundQnAList.do")
	public String gfundQnAList(){
		return "mng/gfund/bbs/gfundQnAList";
	}

	/**
	 * 녹색자금통합관리 - Q&A 게시판
	 * @return
	 */
	@RequestMapping("/mng/cop/bbs/gfundPostManagement.do")
	public String gfundPostManagement(){
		return "mng/gfund/bbs/postManagement";
	}

	/**
	 * 게시물 목록
	 *
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/selectBoardList.do")
	public String selectBoardArticles(@ModelAttribute("searchVO") BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if(EgovUserDetailsHelper.isAuthenticated(request, response)){
			LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

			boardVO.setFrstRegisterId(user.getId());
			boardVO.setAdminAt("Y");

			model.addAttribute("sessionUniqId", user.getId());
		}

		PaginationInfo paginationInfo = new PaginationInfo();

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			// 페이징 정보 설정
			boardVO.setPageUnit(propertyService.getInt("pageUnit"));
			boardVO.setPageSize(propertyService.getInt("pageSize"));
			boardVO.setCtgrymasterId(master.getCtgrymasterId());
			if("BBSA02".equals(master.getBbsAttrbCode())){
				// 페이징 정보 설정
				boardVO.setPageUnit(propertyService.getInt("photoPageUnit"));
				boardVO.setPageSize(propertyService.getInt("photoPageSize"));
			}else{
				//공지게시물 가져오기
				BoardVO noticeVO = new BoardVO();
				noticeVO.setBbsId(boardVO.getBbsId());
				noticeVO.setTmplatId(master.getTmplatId());
				noticeVO.setCommentUseAt(master.getCommentUseAt());
				noticeVO.setSearchNoticeAt("Y");
				noticeVO.setFirstIndex(0);
				noticeVO.setRecordCountPerPage(9999);

				model.addAttribute("noticeList", bbsMngService.selectBoardArticles(noticeVO));
			}

			paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
			paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
			paginationInfo.setPageSize(boardVO.getPageSize());

			boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
			boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
			boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

			boardVO.setCommentUseAt(master.getCommentUseAt());
			boardVO.setTmplatId(master.getTmplatId());
			boardVO.setBbsAttrbCode(master.getBbsAttrbCode());

			List<BoardVO> resultList = bbsMngService.selectBoardArticles(boardVO);
			int totCnt = bbsMngService.selectBoardArticlesCnt(boardVO);

			paginationInfo.setTotalRecordCount(totCnt);

			if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
				Ctgry ctgry = new Ctgry();
				ctgry.setCtgrymasterId(master.getCtgrymasterId());
				model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));
				model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
			}

			model.addAttribute("resultList", resultList);
			model.addAttribute("resultCnt", totCnt);
			model.addAttribute("brdMstrVO", master);
		}

		model.addAttribute("paginationInfo", paginationInfo);

		if(master.getBbsId().equals("BBSMSTR_000000000038") || master.getBbsId().equals("BBSMSTR_000000000039") || master.getBbsId().equals("BBSMSTR_000000000040") || master.getBbsId().equals("BBSMSTR_000000000041")){
			return "mng/cop/bbs/ppbctifm/EgovNoticeList";
		}else{
			return "mng/cop/bbs/default/EgovNoticeList";
		}
	}


	/**
	 * 게시물 읽기
	 *
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/selectBoardArticle.do")
	public String selectBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		boardVO.setAdminAt("Y");

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			model.addAttribute("brdMstrVO", master);

			// 조회수 증가 여부 지정
			boardVO.setPlusCount(false);
			boardVO.setCtgrymasterId(master.getCtgrymasterId());
			BoardVO board = bbsMngService.selectBoardArticle(boardVO);
			model.addAttribute("board", board);

			if(user != null){
				model.addAttribute("sessionUniqId", user.getId());
			}

			//사후관리 게시판
			if("BBSA07".equals(master.getBbsAttrbCode())){
				if(board.getTmp01() != "" || !"".equals(board.getTmp01())){
					BasicInformationVo bsifVo=new BasicInformationVo();
					bsifVo.setBiz_id(board.getTmp01());
					model.addAttribute("bsifVo", basicInformationService.selectBusinessBasicInformation(bsifVo));
				}
			}

			MpmVO mpmVO = new MpmVO();
			mpmVO.setSiteId(boardVO.getSiteId());
			mpmVO.setSysTyCode(boardVO.getSysTyCode());
			mpmVO.setProgrmId(master.getBbsId());
			model.addAttribute("menuId", egovMpmService.selectMpmByProgrmId(mpmVO));
		}

		if(master.getBbsAttrbCode().equals("BBSA05")){
			return "mng/cop/bbs/ppbctifm/EgovNoticeInqire";
		}else{
			return "mng/cop/bbs/default/EgovNoticeInqire";
		}
	}


	/**
	 * 게시물 등록 폼
	 *
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/addBoardArticle.do")
	public String addBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
				Ctgry ctgry = new Ctgry();
				ctgry.setCtgrymasterId(master.getCtgrymasterId());
				model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));

				model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
			}

			model.addAttribute("brdMstrVO", master);

			Board board = new Board();
			model.addAttribute("board", board);

			request.getSession().setAttribute("sessionVO", boardVO);
		}

		if(master.getBbsAttrbCode().equals("BBSA05")){
			return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
		}else{
			return "mng/cop/bbs/default/EgovNoticeRegist";
		}
	}


	/**
	 * 게시물 등록 실행
	 *
	 * @param multiRequest
	 * @param boardVO
	 * @param board
	 * @param bindingResult
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/mng/cop/bbs/insertBoardArticle.do")
	public String insertBoardArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO, Board board, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		if(request.getSession().getAttribute("sessionVO") == null){
			return "forward:/mng/cop/bbs/selectBoardList.do";
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		beanValidator.validate(board, bindingResult);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(bindingResult.hasErrors()){
			if(master != null){
				if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
					Ctgry ctgry = new Ctgry();
					ctgry.setCtgrymasterId(master.getCtgrymasterId());
					model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));

					model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
				}

				model.addAttribute("brdMstrVO", master);
			}

			if(master.getBbsAttrbCode().equals("BBSA05")){
				return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
			}else{
				return "mng/cop/bbs/default/EgovNoticeRegist";
			}
		}

		if(master != null){
			String atchFileId = "";

			List<FileVO> result = null;

			final Map<String, MultipartFile> files = multiRequest.getFileMap();
			if(!files.isEmpty()){
				result = fileUtil.parseBoardFileInf(request, files, 0, "", boardVO.getSiteId(), boardVO.getBbsId());
				atchFileId = fileMngService.insertFileInfs(result);
			}

			boardVO.setAtchFileId(atchFileId);
			boardVO.setFrstRegisterId(user.getId());
			boardVO.setNtcrNm(user.getName());
			boardVO.setNttCn(unscript(boardVO.getNttCn())); // XSS 방지
			boardVO.setCreatIp(EgovClntInfo.getClntIP(request));
			boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));

			bbsMngService.insertBoardArticle(boardVO, master);

			request.getSession().removeAttribute("sessionVO");
		}

		return "forward:/mng/cop/bbs/selectBoardList.do";
	}


	@RequestMapping("/mng/cop/bbs/insertBoardArticlesExcel.do")
	public String insertBoardArticlesExcel(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO, Board board, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		beanValidator.validate(board, bindingResult);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		MultipartFile uploadFile = multiRequest.getFile("uploadfile");
		if(!uploadFile.isEmpty() && uploadFile.getSize() > 0){
			String tempUploadPath = request.getSession().getServletContext().getRealPath("/")+"upload/temp/excelTemp.xls";
			File dir = new File(tempUploadPath);
			if (!dir.exists())
				dir.mkdirs();

			uploadFile.transferTo(new File(tempUploadPath));
			File excelUploadFile = new File(tempUploadPath);

			List<BoardVO> excelList = null;
			if(uploadFile.getOriginalFilename().endsWith(".xlsx") || uploadFile.getOriginalFilename().endsWith(".XLSX")){
				excelList = xlsxExcelRead(excelUploadFile);
			}

			if(excelList != null && excelList.size() > 0){
				try{
					for(BoardVO v : excelList){
						if(master != null){
							v.setFrstRegisterId(user.getId());
							v.setNtcrNm(user.getName());
							v.setNttCn(unscript(v.getNttCn())); // XSS 방지
							v.setCreatIp(EgovClntInfo.getClntIP(request));
							v.setEstnData(EgovHttpUtil.getEstnParseData(request));
							bbsMngService.insertBoardArticle(v, master);
						}
					}
					model.addAttribute("resultMsg", "일괄등록이 완료 되었습니다.");
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				model.addAttribute("resultMsg", "일괄등록이 실패 되었습니다.\\n데이터가 누락되었는지 확인해주십시오.");
			}
		}else{
			model.addAttribute("resultMsg", "일괄등록이 실패 되었습니다.");
		}

		return "forward:/mng/cop/bbs/selectBoardList.do";
	}


	/**
	 * 게시물 답변 등록 폼
	 *
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/addReplyBoardArticle.do")
	public String addReplyBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			model.addAttribute("brdMstrVO", master);

			BoardVO selectVO = new BoardVO();
			selectVO.setBbsId(master.getBbsId());
			selectVO.setNttNo(boardVO.getNttNo());
			selectVO.setAdminAt("Y");
			selectVO.setCtgrymasterId(master.getCtgrymasterId());
			selectVO = bbsMngService.selectBoardArticle(selectVO);

			Board board = new Board();
			board.setCtgryId(selectVO.getCtgryId());
			board.setOrdrCode(selectVO.getOrdrCode());
			board.setOrdrCodeDp(BigDecimal.valueOf(selectVO.getOrdrCodeDp().longValue() + 1));

			if("BBSA11".equals(master.getBbsAttrbCode()) || "BBSA07".equals(master.getBbsAttrbCode())){
				board.setNttNo(selectVO.getNttNo());
				board.setNttSj(selectVO.getNttSj());
				if(!EgovStringUtil.isEmpty(selectVO.getEstnData())){
					board.setNttCn(selectVO.getEstnParseData().getString("cn"));
				}
				board.setAtchFileId(selectVO.getEstnAtchFileId());
				board.setProcessSttusCode(selectVO.getProcessSttusCode());

				ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
				codeVO.setCodeId("COM108");
				model.addAttribute("qaCodeList", cmmUseService.selectCmmCodeDetail(codeVO));
			}

			model.addAttribute("board", board);

			request.getSession().setAttribute("sessionVO", boardVO);
		}

		if(master.getBbsAttrbCode().equals("BBSA05")){
			return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
		}else{
			return "mng/cop/bbs/default/EgovNoticeRegist";
		}
	}


	/**
	 * 게시물 답변 등록 실행
	 *
	 * @param multiRequest
	 * @param boardVO
	 * @param board
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/mng/cop/bbs/replyBoardArticle.do")
	public String replyBoardArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO, Board board, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if(request.getSession().getAttribute("sessionVO") == null){
			return "forward:/mng/cop/bbs/selectBoardList.do";
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		beanValidator.validate(board, bindingResult);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		boardVO.setAdminAt("Y");

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(bindingResult.hasErrors()){

			if(master != null){

				model.addAttribute("brdMstrVO", master);

				if("BBSA11".equals(master.getBbsAttrbCode()) || "BBSA07".equals(master.getBbsAttrbCode())){
					ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
					codeVO.setCodeId("COM108");
					model.addAttribute("qaCodeList", cmmUseService.selectCmmCodeDetail(codeVO));
				}
			}

			if(master.getBbsAttrbCode().equals("BBSA05")){
				return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
			}else{
				return "mng/cop/bbs/default/EgovNoticeRegist";
			}
		}

		if(master != null){
			String atchFileId = boardVO.getAtchFileId();

			final Map<String, MultipartFile> files = multiRequest.getFileMap();

			if("BBSA11".equals(master.getBbsAttrbCode()) || "BBSA07".equals(master.getBbsAttrbCode())){
				if(!files.isEmpty()){
					if(EgovStringUtil.isEmpty(atchFileId)){
						List<FileVO> result = fileUtil.parseBoardFileInf(request, files, 0, atchFileId, boardVO.getSiteId(), boardVO.getBbsId());
						atchFileId = fileMngService.insertFileInfs(result);
					}else{
						FileVO fvo = new FileVO();
						fvo.setAtchFileId(atchFileId);
						int cnt = fileMngService.getMaxFileSN(fvo);
						List<FileVO> _result = fileUtil.parseBoardFileInf(request, files, cnt, atchFileId, boardVO.getSiteId(), boardVO.getBbsId());
						fileMngService.updateFileInfs(_result);
					}
					boardVO.setEstnAtchFileId(atchFileId);
				}

				boardVO.setLastAnswrrId(user.getId());
				boardVO.setLastAnswrrNm(user.getName());

				JSONObject jObj = new JSONObject();
				jObj.put("cn", boardVO.getNttCn());
				boardVO.setEstnData(jObj.toString());

				bbsMngService.updateBoardArticle(boardVO, master, true);
			}else{
				if(!files.isEmpty()){
					List<FileVO> result = fileUtil.parseBoardFileInf(request, files, 0, "", boardVO.getSiteId(), boardVO.getBbsId());
					atchFileId = fileMngService.insertFileInfs(result);
				}

				boardVO.setAtchFileId(atchFileId);
				boardVO.setFrstRegisterId(user.getId());
				boardVO.setNtcrNm(user.getName());
				boardVO.setNttCn(unscript(boardVO.getNttCn())); // XSS 방지
				boardVO.setCreatIp(EgovClntInfo.getClntIP(request));

				boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));
				bbsMngService.replyBoardArticle(boardVO, master);
			}

			request.getSession().removeAttribute("sessionVO");
		}

		return "forward:/mng/cop/bbs/selectBoardList.do";
	}

	/**
	 * 게시물 수정 폼
	 *
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/forUpdateBoardArticle.do")
	public String selectBoardArticleForUpdt(@ModelAttribute("searchVO") BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());
		boardVO.setAdminAt("Y");

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
				Ctgry ctgry = new Ctgry();
				ctgry.setCtgrymasterId(master.getCtgrymasterId());
				model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));

				model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
			}

			if("BBSA06".equals(master.getBbsAttrbCode())){
				ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
				codeVO.setCodeId("COM900");
				model.addAttribute("statusCodeList", cmmUseService.selectCmmCodeDetail(codeVO));
			}

			boardVO.setCtgrymasterId(master.getCtgrymasterId());
			BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);


			//사후관리 게시판
			if("BBSA07".equals(master.getBbsAttrbCode())){
				if(dataVO.getTmp01() != "" || !"".equals(dataVO.getTmp01())){
					BasicInformationVo bsifVo=new BasicInformationVo();
					bsifVo.setBiz_id(dataVO.getTmp01());
					model.addAttribute("bsifVo", basicInformationService.selectBusinessBasicInformation(bsifVo));
				}
			}

			model.addAttribute("brdMstrVO", master);
			model.addAttribute("board", dataVO);

			request.getSession().setAttribute("sessionVO", boardVO);
		}

		if(master.getBbsAttrbCode().equals("BBSA05")){
			return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
		}else{
			return "mng/cop/bbs/default/EgovNoticeRegist";
		}
	}


	/**
	 * 게시물 수정 실행
	 *
	 * @param multiRequest
	 * @param board
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/mng/cop/bbs/updateBoardArticle.do")
	public String updateBoardArticle(final MultipartHttpServletRequest multiRequest, BoardVO board, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if(request.getSession().getAttribute("sessionVO") == null){
			return "forward:/mng/cop/bbs/selectBoardList.do";
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		String atchFileId = board.getAtchFileId();

		beanValidator.validate(board, bindingResult);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(board.getSiteId());
		vo.setSysTyCode(board.getSysTyCode());
		vo.setBbsId(board.getBbsId());
		vo.setTrgetId(board.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(bindingResult.hasErrors()){
			if(master != null){
				if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
					Ctgry ctgry = new Ctgry();
					ctgry.setCtgrymasterId(master.getCtgrymasterId());
					model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));

					model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
				}

				board.setCtgrymasterId(master.getCtgrymasterId());
				BoardVO dataVO = bbsMngService.selectBoardArticle(board);

				model.addAttribute("brdMstrVO", master);
				model.addAttribute("board", dataVO);
			}

			if(master.getBbsAttrbCode().equals("BBSA05")){
				return "mng/cop/bbs/ppbctifm/EgovNoticeRegist";
			}else{
				return "mng/cop/bbs/default/EgovNoticeRegist";
			}
		}

		if(master != null){
			final Map<String, MultipartFile> files = multiRequest.getFileMap();
			if(!files.isEmpty()){
				if(EgovStringUtil.isEmpty(atchFileId)){
					List<FileVO> result = fileUtil.parseBoardFileInf(request, files, 0, atchFileId, board.getSiteId(), board.getBbsId());
					atchFileId = fileMngService.insertFileInfs(result);
					board.setAtchFileId(atchFileId);
				}else{
					FileVO fvo = new FileVO();
					fvo.setAtchFileId(atchFileId);
					int cnt = fileMngService.getMaxFileSN(fvo);
					List<FileVO> _result = fileUtil.parseBoardFileInf(request, files, cnt, atchFileId, board.getSiteId(), board.getBbsId());
					fileMngService.updateFileInfs(_result);
				}
			}

			board.setAdminAt("Y");
			board.setLastUpdusrId(user.getId());
			board.setNttCn(unscript(board.getNttCn())); // XSS 방지
			board.setEstnData(EgovHttpUtil.getEstnParseData(request));

			bbsMngService.updateBoardArticle(board, master, false);

			request.getSession().removeAttribute("sessionVO");
		}

		return "forward:/mng/cop/bbs/selectBoardList.do";
	}


	/**
	 * 게시물  삭제 실행
	 *
	 * @param boardVO
	 * @param board
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/deleteBoardArticle.do")
	public String deleteBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, BoardVO board, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setSiteId(boardVO.getSiteId());
		vo.setSysTyCode(boardVO.getSysTyCode());
		vo.setBbsId(boardVO.getBbsId());
		vo.setTrgetId(boardVO.getTrgetId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			board.setAdminAt("Y");
			board.setLastUpdusrId(user.getId());
			bbsMngService.deleteBoardArticle(board, master);
		}

		return "forward:/mng/cop/bbs/selectBoardList.do";
	}


	/**
	 * 공표목록 게시판 상태처리 변경 Ajax
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do")
	public ModelAndView updateBoardArticleByOpenProcessSttusCode(HttpServletRequest request) throws Exception{
		ModelAndView mav = new ModelAndView("jsonView");
		BoardVO boardVO = new BoardVO();
		boardVO.setProcessSttusCode(request.getParameter("processSttusCode"));
		boardVO.setLastAnswrrId(request.getParameter("lastAnswrrId"));
		boardVO.setLastAnswrrNm(request.getParameter("lastAnswrrNm"));
		boardVO.setBbsId(request.getParameter("bbsId"));
		BigDecimal nttNo = new BigDecimal(request.getParameter("nttNo").trim());
		boardVO.setNttNo(nttNo);
		boardVO.setAdminAt(request.getParameter("adminAt"));
		boardVO.setLastUpdusrId(request.getParameter("lastUpdusrId"));
		boardVO.setTmp01(request.getParameter("tmp01"));
		boardVO.setTmp02(request.getParameter("tmp02"));
		int rs = bbsMngService.updateBoardArticleByOpenProcessSttusCode(boardVO);
		mav.addObject("rs", rs);

		return mav;
	}

	/**
	 * 업로드되는 xlsxExcel 파일 읽기
	 * @param excelFile
	 * @return
	 * @throws Exception
	 */
	public ArrayList<BoardVO> xlsxExcelRead(File excelFile){
		ArrayList<BoardVO> list = new ArrayList<BoardVO>();
		int lows = 0;
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if( !excelFile.exists() || !excelFile.isFile() || !excelFile.canRead() ) {
				throw new IOException("파일없음");
			}
			XSSFWorkbook work = new XSSFWorkbook(new FileInputStream(excelFile));
			XSSFSheet sheet = work.getSheetAt(0);
			lows = sheet.getLastRowNum();
			if(lows > 0){
				//XSSRow는 1개 부족하게 가저온다. 그러니 꼭 반복문에 1을 하나 더해라..
				for(int i = 1; i < lows+1; i++){
					Date to = null;
					XSSFRow row = sheet.getRow(i);
					BoardVO vo = new BoardVO();
					if(row.getCell(0) != null)
						vo.setBbsId(row.getCell(0).getStringCellValue());
					if(row.getCell(1) != null)
						vo.setNtcrNm(row.getCell(1).getStringCellValue());
					if(row.getCell(2) != null)
						vo.setNttSj(row.getCell(2).getStringCellValue());
					if(row.getCell(3) != null)
						vo.setNttCn(row.getCell(3).getStringCellValue());
					if(row.getCell(4) != null)
						vo.setTmp01(row.getCell(4).getStringCellValue());
					if(row.getCell(5) != null)
						vo.setInqireCo(new BigDecimal(row.getCell(5).getNumericCellValue()));
					if(row.getCell(6) != null)
						vo.setCreatIp(row.getCell(6).getStringCellValue());
					if(row.getCell(7) != null)
						to = transFormat.parse(row.getCell(7).getStringCellValue());
						vo.setFrstRegisterPnttm(to);
					if(row.getCell(8) != null)
						vo.setFrstRegisterId("admin");
					list.add(vo);
				}
			}else{
				throw new Exception("Read 할 데이터가 엑셀에 존재하지 않습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}