package egovframework.com.mng.forest.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 산림자료DB 전용 자료관리에 관한 인터페이스클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.11.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2014.11.26  서용식          최초 생성
 *
 * </pre>
 */
public interface EgovNkrefoManageService  {
	
	/**
     * 산림자료DB에서 자료 한 건을 삭제 한다.
     * 
     * @param Nkrefo
     * @throws Exception
     */
    public int deleteNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception ;

    /**
     * 산림자료DB에서 자료 한 건을 등록한다.
     * 
     * @param Nkrefo
     * @throws Exception
     */
    public void insertNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception ;
    
    /**
     * 산림자료DB에서 자료 한 건에 대하여 상세 내용을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public NkrefoManageVO selectNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception ;
    
    /**
     * 산림자료DB에서 단일 조건에 맞는 자료 목록을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public List<?> selectNkrefoArticles(NkrefoManageVO nkrefoManageVO) throws Exception ;
	
	/**
     * 산림자료DB에서 단일 조건에 맞는 자료 목록건수를 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public int selectNkrefoArticlesCnt(NkrefoManageVO nkrefoManageVO) throws Exception ;

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료 목록을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public List<?> selectSearchNkrefoArticleList(NkrefoManageVO nkrefoManageVO) throws Exception ;

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료 목록에 대한 전체 건수를 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public int selectSearchNkrefoArticleListCnt(NkrefoManageVO nkrefoManageVO) throws Exception ;
    
    /**
     * 산림자료DB에서 자료 한 건의 내용을 수정 한다.
     * 
     * @param nkrefoManageVO
     * @throws Exception
     */
    public int updateNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception ;
    
    /**
     * 산림자료DB에서 자료 한 건의 조회수를 +1 증가시킨다.
     * 
     * @param nkrefoManageVO
     * @throws Exception
     */
    public void updateNkrefoArticleForHit(NkrefoManageVO nkrefoManageVO) throws Exception ;
    
    /** 
	 * 산림자료 엑셀파일을 일괄 업로드한다.
	 * 
	 * @param  vo NkrefoManageVO  
	 * @param  inputStream InputStream  
	 * @exception Exception
	 */
	public Map<String, Object> insertNkrefoExcelUpload(NkrefoManageVO vo, String fileExt, InputStream inputStream, String frstRegisterId) throws Exception ;
	
	/**
	 * 검색항목 중 자료형태, 본문언어의 경우 '상위 코드 id', '하위 코드 명' 를 매개변수로 하여 '하위 코드' 를 찾아 반환한다.
	 * 쿼리문 예시) SELECT CODE FROM COMTCCMMNDETAILCODE WHERE CODE_ID = 'COM401' AND CODE_NM = '발표자료'; // PRESENTATION
	 * 
	 * @param codeId 상위 코드 id
	 * @param codeNm 하위 코드 명
	 * @return code 하위 코드
	 * @throws Exception
	 */
	public String selectNkrefoCode(String codeId, String codeNm) throws Exception ;
	
	/**
	 * '상위 코드 id' 에서 '하위 코드 명', '하위 코드 id' 리스트를 찾아 반환한다.
	 * 쿼리문 예시) SELECT CODE, CODE_NM FROM COMTCCMMNDETAILCODE WHERE CODE_ID = 'COM401'; // PRESENTATION, 발표자료
	 * 
	 * @param codeId 상위 코드 id
	 * @return 
	 * @throws Exception
	 */
	public List<?> selectNkrefoCodeList(String codeId) throws Exception ;
}