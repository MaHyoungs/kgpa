package egovframework.com.mng.forest.service;

import java.io.Serializable;

/******************************************************
 * @Class Name : NkrefoManageVO.java
 * @Program name : egovframework.com.mng.forest.service
 * @Descriptopn :
 * @version : 1.0.0
 * @author : 서용식
 * @created date : 2014. 11. 26. Modification log
 *          ===================================================== date name
 *          description -----------------------------------------------------
 *          2014. 11. 26. 서용식 first generated
 *********************************************************/

@SuppressWarnings("serial")
public class NkrefoManageVO extends NkrefoDefaultVO implements Serializable {

	/** FR_ID */
	private String frId;

	/** SYS_TY_CODE */
	private String sysTyCode;

	/** SITE_ID */
	private String siteId;

	/** TITLE */
	private String title;

	/** AUTHOR */
	private String author;

	/** PUBLISHER */
	private String publisher;

	/** TAG */
	private String tag;

	/** DATA_FORM */
	private String dataForm;

	/** BODY_LANG */
	private String bodyLang;

	/** SUMMARY */
	private String summary;

	/** PUB_YEAR */
	private java.util.Date pubYear;

	/** USE_AT */
	private String useAt;

	/** HIT */
	private int hit;

	/** ATCH_FILE_ID */
	private String atchFileId;

	/** FRST_REGISTER_PNTTM */
	private java.util.Date frstRegisterPnttm;

	/** FRST_REGISTER_ID */
	private String frstRegisterId;

	/** LAST_UPDUSR_PNTTM */
	private java.util.Date lastUpdusrPnttm;

	/** LAST_UPDUSR_ID */
	private String lastUpdusrId;

	/**
	 * 엑셀일괄 업로드 등에서의 오류메시지 보관
	 */
	private String message;

	/**
	 * 임시첨부파일 그룹아이디
	 */
	private String fileGroupId = "";

	public String getFrId() {
		return frId;
	}

	public void setFrId(String frId) {
		this.frId = frId;
	}

	public String getSysTyCode() {
		return sysTyCode;
	}

	public void setSysTyCode(String sysTyCode) {
		this.sysTyCode = sysTyCode;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDataForm() {
		return dataForm;
	}

	public void setDataForm(String dataForm) {
		this.dataForm = dataForm;
	}

	public String getBodyLang() {
		return bodyLang;
	}

	public void setBodyLang(String bodyLang) {
		this.bodyLang = bodyLang;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public java.util.Date getPubYear() {
		return pubYear;
	}

	public void setPubYear(java.util.Date pubYear) {
		this.pubYear = pubYear;
	}

	public String getUseAt() {
		return useAt;
	}

	public void setUseAt(String useAt) {
		this.useAt = useAt;
	}

	public int getHit() {
		return hit;
	}

	public void setHit(int hit) {
		this.hit = hit;
	}

	public String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public java.util.Date getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFileGroupId() {
		return fileGroupId;
	}

	public void setFileGroupId(String fileGroupId) {
		this.fileGroupId = fileGroupId;
	}

}
