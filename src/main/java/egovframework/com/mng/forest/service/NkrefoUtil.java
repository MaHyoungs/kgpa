package egovframework.com.mng.forest.service;

public class NkrefoUtil {
	/**
	 * XSS 방지 처리.
	 * "&"	=> "&amp;"
	 * "<"	=> "&lt;"
	 * ">"	=> "&gt;"
	 * "\"\""	=> "&quot;"
	 * 
	 * @param data
	 * @return
	 */
	public static String descript(String data) {
		if (data == null || "null".equals(data) || data.trim().equals("")) {
			return "";
		}

		String ret = data;

		ret = ret.replace("&", "&amp;");
		ret = ret.replace("<", "&lt;");
		ret = ret.replace(">", "&gt;");		
		ret = ret.replace("\"\"", "&quot;");
		
		return ret;
	}
	
	/**
	 * 자료 조회 시 descript() 처리된 내역을 원복처리
	 * "&amp;"	=> "&"
	 * "&lt;"	=> "<"
	 * "&gt;"	=> ">"		
	 * "&quot;"	=> "\"\""
	 * 
	 * @param data
	 * @return
	 */
	public static String enscript(String data) {
		if (data == null || "null".equals(data) || data.trim().equals("")) {
			return "";
		}

		String ret = data;

		ret = ret.replace("&amp;", "&");
		ret = ret.replace("&lt;", "<");
		ret = ret.replace("&gt;", ">");		
		ret = ret.replace("&quot;", "\"\"");
		
		return ret;
	}
}
