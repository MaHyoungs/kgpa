package egovframework.com.mng.forest.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.sms.service.EgovSmsInfoService;
import egovframework.com.mng.forest.service.EgovNkrefoManageService;
import egovframework.com.mng.forest.service.NkrefoManageVO;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

/**
 * 산림자료DB 전용 자료관리에 관한 비지니스 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.11.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2014.11.26  서용식          최초 생성
 *
 * </pre>
 */
@Service("nkrefoManageService")
public class EgovNkrefoManageServiceImpl extends AbstractServiceImpl implements EgovNkrefoManageService {

	@Resource(name="nkrefoManageDAO")
	private NkrefoManageDAO nkrefoManageDAO;

	@Resource(name = "EgovSmsInfoService")
    private EgovSmsInfoService egovSmsInfoService;
	
	@Resource(name = "egovForestNkrefoGnrService")
    private EgovIdGnrService idgenService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	/**
     * 산림자료DB에서 자료 한 건을 삭제 한다.
     * 
     * @param Nkrefo
     * @throws Exception
     */
    public int deleteNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception {
    	return nkrefoManageDAO.deleteNkrefo(nkrefoManageVO);
	}

    /**
     * 산림자료DB에서 자료 한 건을 등록한다.
     * 
     * @param Nkrefo
     * @throws Exception
     */
    public void insertNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception {
    	
    	String frId = idgenService.getNextStringId();
	    nkrefoManageVO.setFrId(frId);
    	
    	nkrefoManageVO.setAtchFileId(this.insertFileInfsByTemp(nkrefoManageVO));
    	    	
    	nkrefoManageDAO.insertNkrefo(nkrefoManageVO);
	}
    
	/**
	 * 임시첨부파일을 정식으로 등록 한다.
	 * 
	 */
	public String insertFileInfsByTemp(NkrefoManageVO nkrefoManageVO) throws Exception {
		FileVO fvo = new FileVO();
		fvo.setAtchFileId(nkrefoManageVO.getAtchFileId());
		fvo.setFileGroupId(nkrefoManageVO.getFileGroupId());
		return fileMngService.insertFileInfsByTemp(fvo).getAtchFileId();
	}
    
    /**
     * 산림자료DB에서 자료 한 건에 대하여 상세 내용을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public NkrefoManageVO selectNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception {
    	return nkrefoManageDAO.selectNkrefo(nkrefoManageVO);
	}
    
    /**
     * 산림자료DB에서 단일 조건에 맞는 자료 목록을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public List<?> selectNkrefoArticles(NkrefoManageVO nkrefoSearchVO) throws Exception {
    	List<?> result = nkrefoManageDAO.selectNkrefoList(nkrefoSearchVO);
    	return result;
	}
	
	/**
     * 산림자료DB에서 단일 조건에 맞는 자료 목록건수를 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public int selectNkrefoArticlesCnt(NkrefoManageVO nkrefoSearchVO) throws Exception {
    	return nkrefoManageDAO.selectNkrefoListTotCnt(nkrefoSearchVO);
	}

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료 목록을 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public List<?> selectSearchNkrefoArticleList(NkrefoManageVO nkrefoSearchVO) throws Exception {
    	List<?> result = nkrefoManageDAO.selectSearchNkrefoList(nkrefoSearchVO);
    	return result;
	}

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료 목록에 대한 전체 건수를 조회 한다.
     * 
     * @param nkrefoManageVO
     * @return
     * @throws Exception
     */
    public int selectSearchNkrefoArticleListCnt(NkrefoManageVO nkrefoSearchVO) throws Exception {
    	return nkrefoManageDAO.selectSearchNkrefoListTotCnt(nkrefoSearchVO);
	}
    
    /**
     * 산림자료DB에서 자료 한 건의 내용을 수정 한다.
     * 
     * @param nkrefoManageVO
     * @throws Exception
     */
    public int updateNkrefoArticle(NkrefoManageVO nkrefoManageVO) throws Exception {
    	
    	nkrefoManageVO.setAtchFileId(this.insertFileInfsByTemp(nkrefoManageVO));
    	    	
    	return nkrefoManageDAO.updateNkrefo(nkrefoManageVO);
	}
    
    /**
     * 산림자료DB에서 자료 한 건의 조회수를 +1 증가시킨다.
     * 
     * @param nkrefoManageVO
     * @throws Exception
     */
    public void updateNkrefoArticleForHit(NkrefoManageVO nkrefoManageVO) throws Exception {
    	nkrefoManageDAO.updateNkrefoHit(nkrefoManageVO);
    }

    /** 
	 * 산림자료 자료 엑셀파일을 일괄 업로드한다.
	 * @param  nkrefoManageVO
	 * @param  fileExt  
	 * @param  inputStream  
	 * @exception Exception
	 */
	public Map<String, Object> insertNkrefoExcelUpload(NkrefoManageVO nkrefoManageVO, String fileExt, InputStream inputStream, String frstRegisterId) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		
		if(EgovStringUtil.isEmpty(nkrefoManageVO.getSiteId())) {
			map.put("message", "사이트가 선택되지 않았습니다.");
			return map;
        }
		
		if(!("XLS".equals(fileExt) || "XLSX".equals(fileExt))) {
			map.put("message", "파일형식이 올바르지 않습니다('.xls','.xlsx')");
			return map;
        }
        
		String baseMessage = null;
		List<NkrefoManageVO> articleList = new ArrayList<NkrefoManageVO>();
	    try {
	    	Workbook wb = null;
	    	if("XLS".equals(fileExt)) {
	    		wb = new HSSFWorkbook(inputStream);
	    	} else if("XLSX".equals(fileExt)) {
	    		wb = new XSSFWorkbook(inputStream);
	    	}
	    	//int sheetNum = wb.getNumberOfSheets(); //시트갯수 가져오기
	    	if(wb != null) {
		    	Sheet sheet = wb.getSheetAt(0);
		    	int rows = sheet.getPhysicalNumberOfRows(); //행 갯수 가져오기
	
	            for(int r = 1; r < rows; r++){ //row 루프            	
	            	Row row = sheet.getRow(r); //row 가져오기
	                if(row != null) {
	                	NkrefoManageVO nkrefoVO = new NkrefoManageVO();
	                	int cells = row.getPhysicalNumberOfCells();
	                	
		                for(int c = 0; c < cells; c++) {	//cell 가져오기
		                	Cell cell = row.getCell(c);
		                	String value = "";
		                	if(cell != null) {
			                	switch(cell.getCellType()) {
				                	case Cell.CELL_TYPE_FORMULA:
				                		value = "" + cell.getCellFormula();
				                		break;
				                	case Cell.CELL_TYPE_NUMERIC:
				                		value = "" + (long)cell.getNumericCellValue();
				                		break;
				                	case Cell.CELL_TYPE_STRING:
				                		value = "" + cell.getRichStringCellValue();
				                		break;
				                	case Cell.CELL_TYPE_BLANK:
				                		value = "";
				                		break;
				                	case Cell.CELL_TYPE_ERROR:
				                		value = "" + cell.getErrorCellValue();
				                		break;
				                	case Cell.CELL_TYPE_BOOLEAN:
				                		value = "" + cell.getBooleanCellValue();
				                		break;
				                	default:
				                		break;
			                	}
		                	}
		                	
		                	if(!EgovStringUtil.isEmpty(value)) {
		                		value = value.trim();
		                	}
		                	
		                	switch(c) {
		                		case 0 : nkrefoVO.setTitle(NkrefoUtil.descript(value));		break;	// 표제
		                		case 1 : nkrefoVO.setAuthor(NkrefoUtil.descript(value));		break;	// 저자
		                		case 2 : nkrefoVO.setPublisher(NkrefoUtil.descript(value));	break;	// 발행자
		                		case 3 : nkrefoVO.setTag(NkrefoUtil.descript(value));		break;	// 태그(키워드)
		                		case 4 : nkrefoVO.setDataForm(value);	break;	// 자료형태
		                		case 5 : nkrefoVO.setBodyLang(value);	break;	// 본문언어
		                		case 6 : nkrefoVO.setSummary(NkrefoUtil.descript(value));	break;	// 요약내용
		                		case 7 : java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
				                		 java.util.Date pubYear = format.parse(value);
				                		 nkrefoVO.setPubYear(pubYear);	break;	// 발행일자
		                		case 8 : nkrefoVO.setUseAt(value);		break;	// 사용여부
				                default : break;
		                	}
		                }
		                nkrefoVO.setFrId(idgenService.getNextStringId());		// 자료id		                
		                nkrefoVO.setSiteId(nkrefoManageVO.getSiteId());			// 사이트id
		                nkrefoVO.setHit(0);										// 조회수
		                nkrefoVO.setFrstRegisterId(frstRegisterId);
		                
		                //Validation 1차
		                nkrefoVO.setMessage("");
		                boolean validateErrored = false;
		                if (EgovStringUtil.isEmpty(nkrefoVO.getTitle())) {
		                	nkrefoVO.setMessage(nkrefoVO.getMessage() + "표제가 공란입니다"); validateErrored = true;
		                }

		                if (nkrefoVO.getTitle().equalsIgnoreCase("END")) break;

		                articleList.add(nkrefoVO);

		                if (validateErrored) {
		                	map.put("message", "[포탈-문제가 발생하였습니다(00)]  데이터에 문제가 발견되었습니다. 데이터의 메세지를 확인해 주세요");
		            		map.put("dataList", articleList);
		            		return map;
		                }
	                }
	            }
	    	}
        } catch(Exception e){
        	baseMessage = "엑셀 읽기 실패";
        	e.printStackTrace(); 
        } finally {
        	if(inputStream != null) {
        		try {
        			inputStream.close();
        		} catch(Exception ex) {}
        	}
        }
        
        if (baseMessage!= null && articleList.size() == 0) {
        	map.put("message", "데이터가 0건 입니다. " + (baseMessage!= null ? "서버메세지 : " + baseMessage : ""));
        	return map;
        }
        
        //임시 자료정보 일괄 입력
    	//Validation 2차
    	nkrefoManageDAO.deleteNkrefoTmp(nkrefoManageVO);
    	
    	try {
    		nkrefoManageDAO.insertNkrefoTmpBatch(articleList);
    	} catch(Exception ex) {
    		map.put("message", "[포탈-문제가 발생하였습니다(01)] 서버메세지 : " + "임시자료저장");
    		map.put("dataList", articleList);
    		return map;
    	}
    	
		try {
    		nkrefoManageDAO.insertNkrefoForTmp(nkrefoManageVO);
    	} catch(Exception ex) {
    		map.put("message", "[포탈-문제가 발생하였습니다(02)] 서버메세지 : " + "임시 > 자료");
    		map.put("dataList", articleList);
    		return map;
    	}
    	
    	map.put("message", "성공적으로 작업을 완료하였습니다. 처리된 데이터건수 :" + articleList.size());
    	map.put("dataList", articleList);
    	
		return map;
	}

	
	/**
	 * 검색항목 중 자료형태, 본문언어의 경우 '상위 코드 id', '하위 코드 명' 를 매개변수로 하여 '하위 코드' 를 찾아 반환한다.
	 * 쿼리문 예시) SELECT CODE FROM COMTCCMMNDETAILCODE WHERE CODE_ID = 'COM401' AND CODE_NM = '발표자료'; // PRESENTATION
	 * 
	 * @param codeId 상위 코드 id
	 * @param codeNm 하위 코드 명
	 * @return code 하위 코드
	 * @throws Exception
	 */
	public String selectNkrefoCode(String codeId, String codeNm) throws Exception {
		
		ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
		codeVO.setCodeId(codeId);
		codeVO.setCodeNm(codeNm);
		
		String code = nkrefoManageDAO.selectNkrefoCode(codeVO);
		if (code == null || "null".equals(code) || code.trim().equals("")) code = "";
		
		return code;
	}
		
	/**
	 * '상위 코드 id' 에서 '하위 코드 명', '하위 코드 id' 리스트를 찾아 반환한다.
	 * 쿼리문 예시) SELECT CODE, CODE_NM FROM COMTCCMMNDETAILCODE WHERE CODE_ID = 'COM401'; // PRESENTATION, 발표자료
	 * 
	 * @param codeId 상위 코드 id
	 * @return 
	 * @throws Exception
	 */
	public List<?> selectNkrefoCodeList(String codeId) throws Exception {
		
		ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
		codeVO.setCodeId(codeId);
		
		List<?> result = nkrefoManageDAO.selectNkrefoCodeList(codeVO);
		return result;
	}
}