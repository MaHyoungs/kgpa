package egovframework.com.mng.forest.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapExecutor;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.mng.forest.service.NkrefoManageVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 산림자료DB 전용 자료관리에 관한 데이터 접근 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.11.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2014.11.26  서용식          최초 생성
 *
 * </pre>
 */
@Repository("nkrefoManageDAO")
public class NkrefoManageDAO extends EgovAbstractDAO{
	
    protected Log log = LogFactory.getLog(this.getClass());

    /**
     * 산림자료DB에서 자료 한 건을 삭제 한다.
     * @param nkrefoManageVO
     * @throws Exception
     */
    public int deleteNkrefo(NkrefoManageVO nkrefoManageVO) throws Exception {
        return update("nkrefoManageDAO.deleteNkrefo", nkrefoManageVO);
    }

    /**
     * 산림자료DB에서 자료의 기본정보를 화면에서 입력하여 항목의 정합성을 체크하고 데이터베이스에 저장
     * @param nkrefoManageVO 자료 등록정보
     * @return String result 등록결과 
     */
    public String insertNkrefo(NkrefoManageVO nkrefoManageVO) throws Exception {
        return (String)insert("nkrefoManageDAO.insertNkrefo", nkrefoManageVO);
    }

    /**
     * 산림자료DB에서 특정 자료의 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param nkrefoManageVO 자료 조회정보
     * @return NkrefoManageVO 자료 상세정보
     * @throws Exception
     */
    public NkrefoManageVO selectNkrefo(NkrefoManageVO nkrefoManageVO) throws Exception {
        return (NkrefoManageVO) selectByPk("nkrefoManageDAO.selectNkrefo", nkrefoManageVO);
    }

    /**
     * 산림자료DB에서 단일 조건에 맞는 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param nkrefoSearchVO 검색조건
     * @return List 사용자 목록정보
     * @throws Exception
     */
    public List<?> selectNkrefoList(NkrefoManageVO nkrefoSearchVO) throws Exception {
        return list("nkrefoManageDAO.selectNkrefoList", nkrefoSearchVO);
    }

    /**
     * 산림자료DB에서 단일 조건에 맞는 자료의 총 갯수를 조회한다.
     * @param nkrefoSearchVO 검색조건
     * @return int 자료 총갯수
     * @throws Exception
     */
    public int selectNkrefoListTotCnt(NkrefoManageVO nkrefoSearchVO) throws Exception {
        return (Integer)getSqlMapClientTemplate().queryForObject("nkrefoManageDAO.selectNkrefoListTotCnt", nkrefoSearchVO);
    }
    

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료 목록을 데이터베이스에서 읽어와 화면에 출력
     * @param nkrefoSearchVO 검색조건
     * @return List 사용자 목록정보
     * @throws Exception
     */
    public List<?> selectSearchNkrefoList(NkrefoManageVO nkrefoSearchVO) throws Exception {
        return list("nkrefoManageDAO.selectSearchNkrefoList", nkrefoSearchVO);
    }

    /**
     * 산림자료DB에서 복수 조건에 맞는 자료의 총 갯수를 조회한다.
     * @param nkrefoSearchVO 검색조건
     * @return int 자료 총갯수
     * @throws Exception
     */
    public int selectSearchNkrefoListTotCnt(NkrefoManageVO nkrefoSearchVO) throws Exception {
        return (Integer)getSqlMapClientTemplate().queryForObject("nkrefoManageDAO.selectSearchNkrefoListTotCnt", nkrefoSearchVO);
    }

    /**
     * 산림자료DB에서 화면에 조회된 자료의 상세정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
     * @param nkrefoManageVO 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateNkrefo(NkrefoManageVO nkrefoManageVO) throws Exception {
        return update("nkrefoManageDAO.updateNkrefo", nkrefoManageVO);
    }
    
    /**
     * 
     * @param nkrefoManageVO 자료 수정정보
     * @return int 영향을 받은 레코드 수
     * @throws Exception
     */
    public int updateNkrefoHit(NkrefoManageVO nkrefoManageVO) throws Exception {
    	return update("nkrefoManageDAO.updateNkrefoHit", nkrefoManageVO);
    }
    
    /**
     * code_id, code_nm을 조건으로 code 를 반환한다.
     * 
     * @param codeVO 조회할 코드정보
     * @return
     * @throws Exception
     */
    public String selectNkrefoCode(ComDefaultCodeVO codeVO) throws Exception {
    	return (String) getSqlMapClientTemplate().queryForObject("nkrefoManageDAO.selectNkrefoCode", codeVO);
    }
    
    /**
     * code_id 조건으로 code, code_nm 을 반환한다.
     * 
     * @param codeVO 조회할 코드정보
     * @return
     * @throws Exception
     */
    public List<?> selectNkrefoCodeList(ComDefaultCodeVO codeVO) throws Exception {
    	return list("nkrefoManageDAO.selectNkrefoCodeList", codeVO);
    }
    
    /**
     * 임시산림자료 테이블의 내용을 삭제한다. 
     *  
     * @throws Exception
     */
    public void deleteNkrefoTmp(NkrefoManageVO nkrefoManageVO) throws Exception {
    	update("nkrefoManageDAO.deleteNkrefoTmp", nkrefoManageVO);
    }
    
    /**
     * 임시산림자료 데이터를 데이터베이스에 저장
     * 
     * @param nkrefoTmpList 산림자료 정보
     * @throws Exception
     */
    public void insertNkrefoTmpBatch(final List<NkrefoManageVO> nkrefoTmpList) throws Exception {
    	if(nkrefoTmpList != null && nkrefoTmpList.size() > 0) {
	    	getSqlMapClientTemplate().execute( new SqlMapClientCallback() {
		          public Object doInSqlMapClient( SqlMapExecutor excutor ) throws SQLException {
		              excutor.startBatch();
		              
		              for(int i = 0; i < nkrefoTmpList.size(); i++) {
		            	  insert("nkrefoManageDAO.insertNkrefoTmp", nkrefoTmpList.get(i));
		              }
	
		              excutor.executeBatch();
		              return null;
		          }
		    });
    	}
    }
    
    /**
     * 임시산림자료 데이터를 산림자료 데이터베이스에 저장
     * @param nkrefoManageVO 자료등록정보
     * @return String result 등록결과 
     */
    public void insertNkrefoForTmp(NkrefoManageVO nkrefoManageVO) throws Exception {
    	insert("nkrefoManageDAO.insertNkrefoForTmp", nkrefoManageVO);
    }
    
}
