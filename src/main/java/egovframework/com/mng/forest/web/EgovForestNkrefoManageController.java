package egovframework.com.mng.forest.web;

import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.mng.forest.service.EgovNkrefoManageService;
import egovframework.com.mng.forest.service.NkrefoDefaultVO;
import egovframework.com.mng.forest.service.NkrefoManageVO;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 산림자료DB 전용 자료관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.11.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2014.11.26  서용식          최초 생성
 *
 * </pre>
 */

@Controller
public class EgovForestNkrefoManageController {
	/**
	 * EgovMessageSource
	 */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/**
	 * EgovNkrefoManageService
	 */
	@Resource(name = "nkrefoManageService")
	private EgovNkrefoManageService nkrefoManageService;

	/**
	 * EgovPropertyService
	 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	/**
	 * DefaultBeanValidator
	 */
	@Autowired
	private DefaultBeanValidator beanValidator;
	
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService cmmUseService;
		
	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	Logger log = LogManager.getLogger(this.getClass());
	
	/**
	 * 산림자료DB 자료 목록을 조회한다. - 단일 조건
	 *
	 * @param model 화면모델
	 * @return mng/forest/EgovNkrefoList
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefoManage.do")
	public String EgovNkrefoManage(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())){
			nkrefoManageVO.setSiteId(loginVO.getSiteId());
		}
		
		PaginationInfo paginationInfo = new PaginationInfo();
		
		nkrefoManageVO.setSearchCnd("OR"); // 검색방식은 LIKE 연산을 위해 OR로 설정
		
		// 페이징 정보 설정
		nkrefoManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		nkrefoManageVO.setPageSize(propertiesService.getInt("pageSize"));
		
		paginationInfo.setCurrentPageNo(nkrefoManageVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(nkrefoManageVO.getPageUnit());
		paginationInfo.setPageSize(nkrefoManageVO.getPageSize());
		
		nkrefoManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		nkrefoManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
		nkrefoManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		String searchCondition = nkrefoManageVO.getSearchCondition();
		String searchKeyword = nkrefoManageVO.getSearchKeyword(); // 검색어
		
		String decodedKeyword = NkrefoUtil.descript(URLDecoder.decode(searchKeyword, "UTF-8").toUpperCase()); // 검색어 입력란에도 <,>,& 등을 입력할 수 있으므로 임시로 치환처리
		nkrefoManageVO.setDecodedKeyword(decodedKeyword);
		if ( !(searchKeyword == null || "null".equals(searchKeyword) || searchKeyword.trim().equals("")) ) {
			if ("4".equals(searchCondition)) { // 자료형태
				String codeValue = nkrefoManageService.selectNkrefoCode("COM401", decodedKeyword);
				nkrefoManageVO.setSearchKeyword(codeValue);
			} else if ("5".equals(searchCondition)) { // 본문언어
				String codeValue = nkrefoManageService.selectNkrefoCode("COM402", decodedKeyword);
				nkrefoManageVO.setSearchKeyword(codeValue);
			} else {
				nkrefoManageVO.setSearchKeyword(decodedKeyword); // 검색어 입력란에도 <,>,& 등을 입력할 수 있으므로 임시로 치환처리
			}
		}

		List<NkrefoManageVO> resultList = (List<NkrefoManageVO>) nkrefoManageService.selectNkrefoArticles(nkrefoManageVO); 
		int totCnt = nkrefoManageService.selectNkrefoArticlesCnt(nkrefoManageVO);
		
		paginationInfo.setTotalRecordCount(totCnt);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");
		
		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);
		
		nkrefoManageVO.setSearchKeyword(searchKeyword); // searchKeyword 원복처리

		return "mng/forest/EgovForestNkrefoList";
	}
	
	/**
	 * 산림자료 등록화면으로 이동한다.
	 * 
	 * @param searchVO
	 * @param nkrefoManageVO
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefoAddView.do")
	public String EgovNkrefoAddView(@ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, HttpServletRequest request, ModelMap model) throws Exception {

		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");
		
		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);
		
		request.getSession().setAttribute("sessionVO", nkrefoManageVO); // YS1: 등록화면으로 이동할 때 세션에 저장하고
		
		return "mng/forest/EgovForestNkrefoIndt";
	}
	
	/**
	 * 산림자료DB에 자료 등록처리후 메인화면으로 이동한다.
	 *
	 * @param nkrefoManageVO  자료등록정보
	 * @param bindingResult 입력값검증용 bindingResult
	 * @param model         화면모델
	 * @return forward:/uss/umt/user/EgovUserManage.do
	 * @throws Exception
	 */
	@RequestMapping("/mng/forest/nkrefo/EgovNkrefInsert.do")
	public String insertEgovNkref(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// YS2: 등록처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
		}
		
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO != null) {
			nkrefoManageVO.setFrstRegisterId(loginVO.getId());
		}
		
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		nkrefoManageVO.setSiteId(siteVO.getSiteId());
		nkrefoManageVO.setSysTyCode(siteVO.getSysTyCode());

		// XSS 방지 처리
		nkrefoManageVO.setTitle(unscript(nkrefoManageVO.getTitle()));
		nkrefoManageVO.setAuthor(unscript(nkrefoManageVO.getAuthor()));
		nkrefoManageVO.setPublisher(unscript(nkrefoManageVO.getPublisher()));
		nkrefoManageVO.setTag(unscript(nkrefoManageVO.getTag()));
		nkrefoManageVO.setSummary(unscript(nkrefoManageVO.getSummary()));

		//String atchFileId = "";
		//List<FileVO> result = null;

		//SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); // 첨부파일의 하위 저장경로는 년도로 생성한다.
		//String appendPath = sdf.format(new Date());
		
		//final Map<String, MultipartFile> files = multiRequest.getFileMap();
		//if(!files.isEmpty()){
		//	result = fileUtil.parseNkrefoFileInf(request, files, 0, "", appendPath);
		//	atchFileId = fileMngService.insertFileInfs(result);
		//}
				
		//nkrefoManageVO.setAtchFileId(atchFileId);

		nkrefoManageService.insertNkrefoArticle(nkrefoManageVO);
		
		// YS3: 등록처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
	}
	
	
	/**
	 * 산림자료DBd에서 특정 자료 1건을 삭제 처리를 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/mng/forest/nkrefo/EgovNkrefoManage.do
	 * @throws Exception
	 */	
	@RequestMapping("/mng/forest/nkrefo/EgovNkrefoManageDelete.do")
	public String deleteEgovMber(@ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// top.jsp 에서도 message 를 검사하여 alert를 하므로 forest 패키지에서는 message 대신 frdbmessage 를 사용
		
		if (EgovStringUtil.isEmpty(nkrefoManageVO.getFrId())) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}
		
		NkrefoManageVO nkrefoVO = nkrefoManageService.selectNkrefoArticle(nkrefoManageVO);
		String atchFileId = nkrefoVO.getAtchFileId();
		if (!EgovStringUtil.isEmpty(atchFileId)) fileMngService.deleteFileInfs(atchFileId);		
		if (nkrefoManageService.deleteNkrefoArticle(nkrefoManageVO) > 0) {			
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.delete"));			
		}

		return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";		
	}
			
	/**
	 * 산림자료DB 자료 수정 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return mng/forest/EgovForestNkrefoUpdt
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefoSelectUpdtView.do")
	public String EgovUserSelectUpdtView(@ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, HttpServletRequest request, ModelMap model) throws Exception{

		if ( EgovStringUtil.isEmpty(nkrefoManageVO.getFrId()) ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
		}
		
		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");
		
		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);
		
		NkrefoManageVO result = nkrefoManageService.selectNkrefoArticle(nkrefoManageVO);

		model.addAttribute("nkrefoManageVO", result);
		
		request.getSession().setAttribute("sessionVO", nkrefoManageVO); // YS1: 수정화면으로 이동할 때 세션에 저장하고
		
		return "mng/forest/EgovForestNkrefoUpdt";
	}

	/**
	 * 산림자료DB 자료정보 수정 처리 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/mng/forest/nkrefo/EgovNkrefoManage.do
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefSelectUpdt.do")
	public String EgovNkrefSelectUpdt(@ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
		}
		
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		if ( loginVO == null || EgovStringUtil.isEmpty(loginVO.getId()) ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "/mng/forest/nkrefo/EgovNkrefoManage.do";
		}

		// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리
		NkrefoManageVO uptNkrefoManageVO = nkrefoManageService.selectNkrefoArticle(nkrefoManageVO);

		// XSS 방지 처리
		uptNkrefoManageVO.setTitle(unscript(nkrefoManageVO.getTitle()));
		uptNkrefoManageVO.setAuthor(unscript(nkrefoManageVO.getAuthor()));
		uptNkrefoManageVO.setPublisher(unscript(nkrefoManageVO.getPublisher()));
		uptNkrefoManageVO.setTag(unscript(nkrefoManageVO.getTag()));
		uptNkrefoManageVO.setDataForm(nkrefoManageVO.getDataForm());
		uptNkrefoManageVO.setBodyLang(nkrefoManageVO.getBodyLang());
		uptNkrefoManageVO.setSummary(unscript(nkrefoManageVO.getSummary()));
		uptNkrefoManageVO.setPubYear(nkrefoManageVO.getPubYear());
		uptNkrefoManageVO.setUseAt(nkrefoManageVO.getUseAt());
		uptNkrefoManageVO.setLastUpdusrId(loginVO.getId());
		uptNkrefoManageVO.setFileGroupId(nkrefoManageVO.getFileGroupId());
		
		if (nkrefoManageService.updateNkrefoArticle(uptNkrefoManageVO) > 0) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.update"));
			return "mng/forest/EgovForestNkrefoUpdt";
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
	}
	
	/**
	 * 산림자료DB에 자료 엑셀파일을 일괄 업로드하는 화면으로 이동한다.
	 * @param nkrefoManageVO
	 * @param request
	 * @param model
	 * @return "mng/usr/EgovMberExcelUpload"
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefoExcelUploadView.do")
	public String EgovNkrefoExcelUploadView(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, HttpServletRequest request, ModelMap model) throws Exception {
				
		return "mng/forest/EgovNkrefoExcelUpload";
	}
	
	/**
	 * 산림자료DB에 자료 엑셀파일을 일괄 업로드한다.
	 * @param userManageVO
	 * @param request
	 * @param commandMap
	 * @param model
	 * @return "mng/usr/EgovMberExcelUpload"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/forest/nkrefo/EgovMberExcelUpload.do")
	public String EgovMberExcelUpload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		if ( loginVO == null || EgovStringUtil.isEmpty(loginVO.getId()) ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "/mng/forest/nkrefo/EgovNkrefoManage.do";
		}
		
		Map<String, Object> resultList = null;
		
		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;

		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();
		
			file = entry.getValue();

			if (!"".equals(file.getOriginalFilename())) {
				int index = file.getOriginalFilename().lastIndexOf(".");
				String fileExt = "";
			    if(index != -1) {
			    	fileExt = file.getOriginalFilename().substring(index + 1).toUpperCase();
			    }
				resultList = nkrefoManageService.insertNkrefoExcelUpload(nkrefoManageVO, (fileExt == null ? "" : fileExt), file.getInputStream(), loginVO.getId());
				model.addAttribute("message", resultList.get("message"));
				if (resultList.containsKey("dataList")) {
					model.addAttribute("dataList", resultList.get("dataList"));
					
					// 자료형태 공통코드정보를 조회한다.
					List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
					// 본문언어 공통코드정보를 조회한다.
					List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");
					
					model.addAttribute("dataFormList", dataFormList);
					model.addAttribute("bodyLangList", bodyLangList);
				}
			}
		}
		
        return "mng/forest/EgovNkrefoExcelUpload";
	}
	
	/**
	 * 산림자료DB 자료 조회 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return mng/forest/EgovForestNkrefoInquire
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/nkrefo/EgovNkrefoSelectView.do")
	public String EgovUserSelectView(@ModelAttribute("searchVO") NkrefoDefaultVO searchVO, NkrefoManageVO nkrefoManageVO, HttpServletRequest request, ModelMap model) throws Exception{

		if ( EgovStringUtil.isEmpty(nkrefoManageVO.getFrId()) ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/nkrefo/EgovNkrefoManage.do";
		}
		
		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");
		
		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);

		NkrefoManageVO result = nkrefoManageService.selectNkrefoArticle(nkrefoManageVO);

		model.addAttribute("nkrefoManageVO", result);

		return "mng/forest/EgovForestNkrefoInquire";
	}

	/**
	 * XSS 방지 처리.
	 *
	 * @param data
	 * @return
	 */
	protected String unscript(String data) {
		if(data == null || data.trim().equals("")) {
			return "";
		}

		String ret = data;

		ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
		ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

		ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
		ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

		ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
		ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

		// ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
		// ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

		ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
		ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

		return ret;
	}

}
