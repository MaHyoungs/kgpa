package egovframework.com.mng.forest.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 산림자료DB 전용 사용자관리에 관한 컨트롤러 클래스를 정의한다.
 * @author 프로그램팀 서용식
 * @since 2014.11.25
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2014.11.25  서용식          최초 생성
 *
 * </pre>
 */

@Controller
public class EgovForestUserManageController {
	/**
	 * EgovMessageSource
	 */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/**
	 * EgovUserManageService
	 */
	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	/**
	 * EgovPropertyService
	 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/**
	 * DefaultBeanValidator
	 */
	@Autowired
	private DefaultBeanValidator beanValidator;
	
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

	/**
	 * 산림자료DB 전용 사용자목록을 조회한다.
	 *
	 * @param model 화면모델
	 * @return mng/forest/EgovForestMemberList
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/usr/EgovMberManage.do")
	public String selectEgovMberManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())){
			userManageVO.setSiteId(loginVO.getSiteId());
		}

		userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		userManageVO.setPageSize(propertiesService.getInt("pageSize"));

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
		paginationInfo.setPageSize(userManageVO.getPageSize());

		userManageVO.setSearchSe("06"); // 산림자료DB 열람가능 회원권한은 06 으 설정
		userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
		userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", userManageService.selectUserList(userManageVO));

		int totCnt = userManageService.selectUserListTotCnt(userManageVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		return "mng/forest/EgovForestMemberList";
	}
	
	/**
	 * 산림자료DB 전용 사용자 등록화면으로 이동한다.
	 *
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "mng/usr/EgovForestMemberIndt"
	 * @throws Exception
	 */

	@RequestMapping(value = "/mng/forest/usr/EgovMberAddView.do")
	public String EgovMberAddView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{

		request.getSession().setAttribute("sessionVO", userManageVO); // YS1: 등록화면으로 이동할 때 세션에 저장하고
		
		return "mng/forest/EgovForestMemberIndt";
	}
	
	/**
	 * 산림자료DB 전용 사용자등록처리후 메인화면으로 이동한다.
	 *
	 * @param userManageVO  사용자등록정보
	 * @param bindingResult 입력값검증용 bindingResult
	 * @param model         화면모델
	 * @return forward:/uss/umt/user/EgovUserManage.do
	 * @throws Exception
	 */
	@RequestMapping("/mng/forest/usr/EgovMberInsert.do")
	public String insertEgovMber(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// YS2: 등록처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}
		
		// top.jsp 에서도 message 를 검사하여 alert를 하므로 forest 패키지에서는 message 대신 frdbmessage 를 사용
		if (userManageService.checkIdDplct(userManageVO.getUserId()) > 0){
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("common.isExist.msg"));
			return "mng/forest/EgovForestMemberIndt";
		}
		
		userManageVO.setUserSeCode("06"); // 산림자료DB 자료를 열람할 수 있는 권한 부여

		// 접속자의 IP 정보 얻기
		String clientIp = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (null == clientIp || clientIp.length() == 0 || clientIp.toLowerCase().equals("unknown")) {
			clientIp = request.getHeader("REMOTE_ADDR");
		}
		if (null == clientIp || clientIp.length() == 0 || clientIp.toLowerCase().equals("unknown")) {
			clientIp = request.getRemoteAddr();
		}
		userManageVO.setFrstRegistIp(clientIp); // 접속 IP 등록
		
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO != null) {
			userManageVO.setFrstRegisterId(loginVO.getId());
		}

		if(EgovStringUtil.isEmpty(userManageVO.getSiteId())){
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			userManageVO.setSiteId(siteVO.getSiteId());
		}
		
		//System.out.println("userManageVO.getUserId() = " + userManageVO.getUserId());
		//System.out.println("userManageVO.getPassword() = " + userManageVO.getPassword());
		//System.out.println("userManageVO.getUserNm() = " + userManageVO.getUserNm()); 
		//System.out.println("userManageVO.getOrganNm() = " + userManageVO.getOrganNm()); 
		//System.out.println("userManageVO.getConfmAt() = " + userManageVO.getConfmAt());
		//System.out.println("userManageVO.getFrstRegistIp() = " + userManageVO.getFrstRegistIp());
		//System.out.println("userManageVO.getFrstRegisterId() = " + userManageVO.getFrstRegisterId());
		
		userManageService.insertFRDBUser(userManageVO);
		
		// YS3: 등록처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");
		
		return "forward:/mng/forest/usr/EgovMberManage.do";
	}
	
	
	/**
	 * 산림자료DB 전용 사용자를 삭제 처리를 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/mng/forest/usr/EgovMberManage.do
	 * @throws Exception
	 */
	@RequestMapping("/mng/forest/usr/EgovMberManageDelete.do")
	public String deleteEgovMber(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// top.jsp 에서도 message 를 검사하여 alert를 하므로 forest 패키지에서는 message 대신 frdbmessage 를 사용
		
		if (EgovStringUtil.isEmpty(userManageVO.getUserId())) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}
		
		if (userManageService.deleteFRDBUser(userManageVO) > 0) {			
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.delete"));
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.delete"));			
		}

		return "forward:/mng/forest/usr/EgovMberManage.do";		
	}
	
			
	/**
	 * 산림자료DB 전용 사용자정보 수정 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return mng/usr/EgovMemberUpdt
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/usr/EgovUserSelectUpdtView.do")
	public String EgovUserSelectUpdtView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{

		if (EgovStringUtil.isEmpty(userManageVO.getUserId())) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}
		
		model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));
		
		request.getSession().setAttribute("sessionVO", userManageVO); // YS1: 수정화면으로 이동할 때 세션에 저장하고

		return "mng/forest/EgovForestMemberUpdt";
	}


	/**
	 * 산림자료DB 전용 사용자정보 수정 처리 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/mng/usr/EgovUserSelectUpdtView.do
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/usr/EgovUserSelectUpdt.do")
	public String EgovUserSelectUpdt(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// YS2: 수정처리 핸들러에서는 세션 체크를 해서 없으면 리스트 페이지로 다시 이동처리, forward 처리를 하므로 새로고침하면 레코드 삽입이 반복된다.
		if (request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		
		if ( user == null || EgovStringUtil.isEmpty(userManageVO.getUserId()) ) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.access"));
			
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}

		// 원본 레코드 조회 후 입력 폼으로부터 전송된 값만 변경 처리 
		UserManageVO uptUserManageVO = userManageService.selectUser(userManageVO);

		uptUserManageVO.setUserId(userManageVO.getUserId());
		uptUserManageVO.setPassword(userManageVO.getPassword());
		uptUserManageVO.setUserNm(userManageVO.getUserNm());
		uptUserManageVO.setOrganNm(userManageVO.getOrganNm());
		uptUserManageVO.setConfmAt(userManageVO.getConfmAt());
		uptUserManageVO.setLastUpdusrId(user.getId());
		uptUserManageVO.setTargetId(uptUserManageVO.getUserId());
		
		//System.out.println("userManageVO.getUserId() = " + uptUserManageVO.getUserId());
		//System.out.println("userManageVO.getPassword() = " + uptUserManageVO.getPassword());
		//System.out.println("userManageVO.getUserNm() = " + uptUserManageVO.getUserNm()); 
		//System.out.println("userManageVO.getOrganNm() = " + uptUserManageVO.getOrganNm()); 
		//System.out.println("userManageVO.getConfmAt() = " + uptUserManageVO.getConfmAt());
		//System.out.println("userManageVO.getFrstRegistIp() = " + uptUserManageVO.getFrstRegistIp());
		//System.out.println("userManageVO.getFrstRegisterId() = " + uptUserManageVO.getFrstRegisterId());
		//System.out.println("userManageVO.getLastUpdusrId() = " + uptUserManageVO.getLastUpdusrId());
		
		if (userManageService.updateManageUser(uptUserManageVO) > 0) {			
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.update"));
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.update"));
			return "mng/forest/EgovForestMemberUpdt";
		}
		
		// YS3: 수정처리 핸들러에서는 세션을 제거해서 새로고침 되더라도 메소드 맨 앞에서 리스트페이지로 이동되도록 한다.
		request.getSession().removeAttribute("sessionVO");

		return "forward:/mng/forest/usr/EgovMberManage.do";
	}
	
	/**
	 * 산림DB 전용 사용자의 패스워드를 재발급한다.
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "mng/forest/EgovForestMemberUpdt"
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/forest/usr/InitPassword.do")
	public String InitPassword(@ModelAttribute("searchVO") UserManageVO userManageVO, final HttpServletRequest request, Model model) throws Exception{

		String userId = userManageVO.getUserId();
		userManageVO.setPassword(userId);
		
		if (userManageService.updatePassword(userManageVO) > 0) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.process"));
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.request.msg"));
		}
		
		model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));

		return "mng/forest/EgovForestMemberUpdt";
	}
	
}
