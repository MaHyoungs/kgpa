package egovframework.com.mng.mma.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name : EgovMltmdCtgryController.java
 * @Description : EgovMltmdCtgryController class
 * @Modification Information
 *
 * @author 이엠티
 * @since 2011.12.15
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller("mngEgovMltmdCtgryController")
public class EgovMltmdCtgryController {

	@Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
	 * COMTNMLTMDCTGRY 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 ctgry
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/mma/selectMltmdCtgryList.do")
    public String selectMltmdCtgryList(@ModelAttribute("searchVO") MltmdCtgryVO searchVO, ModelMap model) throws Exception {
    	
        model.addAttribute("resultList", mltmdCtgryService.selectMltmdCtgryList(searchVO));
        
        return "/mng/mma/EgovMltmdCtgryList";
    } 
    
    @RequestMapping("/mng/mma/addMltmdCtgry.do")
    public String addMltmdCtgryView(@ModelAttribute("searchVO") MltmdCtgryVO searchVO, Model model, HttpServletRequest request) throws Exception {
    	
    	model.addAttribute("ctgryList", mltmdCtgryService.selectMltmdCtgryList(searchVO));
    	
    	model.addAttribute("mltmdCtgry", searchVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdCtgryRegist";
    }
    
    @RequestMapping("/mng/mma/insertMltmdCtgry.do")
    public String insertMltmdCtgry(MltmdCtgryVO ctgry, @ModelAttribute("searchVO") MltmdCtgryVO searchVO, HttpServletRequest request) throws Exception {
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryList.do";
		}
    	
    	mltmdCtgryService.insertMltmdCtgry(ctgry);
    	
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/mma/selectMltmdCtgryList.do";
    }
    
    @RequestMapping("/mng/mma/selectMltmdCtgry.do")
    public String selectMltmdCtgry(@ModelAttribute("searchVO") MltmdCtgryVO searchVO, Model model, HttpServletRequest request) throws Exception {
    	
    	model.addAttribute("ctgryList", mltmdCtgryService.selectMltmdCtgryList(searchVO));
    	
    	model.addAttribute("mltmdCtgry", mltmdCtgryService.selectMltmdCtgry(searchVO));
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdCtgryRegist";
    }

    @RequestMapping("/mng/mma/updateMltmdCtgry.do")
    public String updateMltmdCtgry(MltmdCtgryVO ctgry, @ModelAttribute("searchVO") MltmdCtgryVO searchVO, HttpServletRequest request) throws Exception {
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryList.do";
		}
    	
    	mltmdCtgryService.updateMltmdCtgry(ctgry);
    	
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/mma/selectMltmdCtgryList.do";
    }
    
    @RequestMapping("/mng/mma/deleteMltmdCtgry.do")
    public String deleteMltmdCtgry(MltmdCtgryVO mltmdCtgryVO, @ModelAttribute("searchVO") MltmdCtgryVO searchVO) throws Exception {
    	mltmdCtgryService.deleteMltmdCtgry(mltmdCtgryVO);
        return "forward:/mng/mma/selectMltmdCtgryList.do";
    }
    
    /**
     * 카테고리를 이동한다.
     * 
     * @param searchVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/mng/mma/updateMltmdCtgrySortOrdr.do")
    public String updateMltmdCtgrySortOrdr(@ModelAttribute("searchVO") MltmdCtgryVO searchVO, ModelMap model) throws Exception {
  	  
    	mltmdCtgryService.updateSortOrdr(searchVO);

    	return "forward:/mng/mma/selectMltmdCtgryList.do";
    }

}
