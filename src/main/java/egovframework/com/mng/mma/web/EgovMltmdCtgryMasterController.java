package egovframework.com.mng.mma.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import egovframework.com.mma.service.MltmdCtgryMasterService;
import egovframework.com.mma.service.MltmdCtgryMasterVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : EgovMltmdCtgryMasterController.java
 * @Description : EgovMltmdCtgryMaster Controller class
 * @Modification Information
 *
 * @author 이엠티
 * @since 2011.12.15
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdCtgryMasterController {

	@Resource(name = "mltmdCtgryMasterService")
    private MltmdCtgryMasterService mltmdCtgryMasterService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
	 * COMTNMLTMDCTGRYMASTER 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 CtgryMaster
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/mma/selectMltmdCtgryMasterList.do")
    public String selectMltmdCtgryMasterList(@ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			searchVO.setSiteId(loginVO.getSiteId());
		}
		
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		if(!EgovStringUtil.isEmpty(searchVO.getSiteId())) {
	        model.addAttribute("resultList", mltmdCtgryMasterService.selectMltmdCtgryMasterList(searchVO));
	        
	        int totCnt = mltmdCtgryMasterService.selectMltmdCtgryMasterListTotCnt(searchVO);
			paginationInfo.setTotalRecordCount(totCnt);
		}
        model.addAttribute("paginationInfo", paginationInfo);
        
        return "/mng/mma/EgovMltmdCtgryMasterList";
    } 
    
    @RequestMapping("/mng/mma/addMltmdCtgryMaster.do")
    public String addMltmdCtgryMasterView(@ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO, Model model, HttpServletRequest request) throws Exception {
    	
		//model.addAttribute("siteList", siteManageService.selectSiteSimpleList());
		
    	model.addAttribute("mltmdCtgryMaster", searchVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdCtgryMasterRegist";
    }
    
    @RequestMapping("/mng/mma/insertMltmdCtgryMaster.do")
    public String addMltmdCtgryMaster(MltmdCtgryMasterVO ctgryMaster, @ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	mltmdCtgryMasterService.insertMltmdCtgryMaster(ctgryMaster);
    	
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
    }
    
    @RequestMapping("/mng/mma/selectMltmdCtgryMaster.do")
    public String insertMltmdCtgryMaster(@ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO, Model model, HttpServletRequest request) throws Exception {
        
    	//model.addAttribute("siteList", siteManageService.selectSiteSimpleList());
    	
    	model.addAttribute("mltmdCtgryMaster", mltmdCtgryMasterService.selectMltmdCtgryMaster(searchVO));
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdCtgryMasterRegist";
    }


    @RequestMapping("/mng/mma/updateMltmdCtgryMaster.do")
    public String updateMltmdCtgryMaster(MltmdCtgryMasterVO ctgryMaster, @ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	mltmdCtgryMasterService.updateMltmdCtgryMaster(ctgryMaster);
    	
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
    }
    
    @RequestMapping("/mng/mma/deleteMltmdCtgryMaster.do")
    public String deleteMltmdCtgryMaster(MltmdCtgryMasterVO ctgryMaster, @ModelAttribute("searchVO") MltmdCtgryMasterVO searchVO) throws Exception {
        mltmdCtgryMasterService.deleteMltmdCtgryMaster(ctgryMaster);
        return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
    }

}
