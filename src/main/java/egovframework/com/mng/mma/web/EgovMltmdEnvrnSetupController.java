package egovframework.com.mng.mma.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.mma.service.MltmdCtgryMasterService;
import egovframework.com.mma.service.MltmdCtgryMasterVO;
import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.SiteManageDefaultVO;
import egovframework.com.uat.uia.service.LoginVO;

/**
 * @Class Name : EgovMltmdEnvrnSetupController.java
 * @Description : MltmdEnvrnSetup Controller class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdEnvrnSetupController {

    @Resource(name = "mltmdEnvrnSetupService")
    private MltmdEnvrnSetupService mltmdEnvrnSetupService;
    
    @Resource(name = "mltmdCtgryMasterService")
    private MltmdCtgryMasterService mltmdCtgryMasterService;
    
    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
	 * COMTNMLTMDENVRNSETUP 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 MltmdEnvrnSetupVO
	 * @return "/mng/mma/EgovMltmdEnvrnSetupList"
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/mma/MltmdEnvrnSetupList.do")
    public String selectMltmdEnvrnSetupList(@ModelAttribute("searchVO") SiteManageDefaultVO searchVO, 
    		ModelMap model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			searchVO.setSiteId(loginVO.getSiteId());
		}
		
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		if(!EgovStringUtil.isEmpty(searchVO.getSiteId())) {
	        model.addAttribute("resultList", mltmdEnvrnSetupService.selectMltmdEnvrnSetupList(searchVO));
	        
	        int totCnt = mltmdEnvrnSetupService.selectMltmdEnvrnSetupListTotCnt(searchVO);
			paginationInfo.setTotalRecordCount(totCnt);
		}
        model.addAttribute("paginationInfo", paginationInfo);
        
        return "/mng/mma/EgovMltmdEnvrnSetupList";
    } 
    
    @RequestMapping("/mng/mma/addMltmdEnvrnSetup.do")
    public String addMltmdEnvrnSetupView(@ModelAttribute("searchVO") MltmdEnvrnSetupVO searchVO, Model model, HttpServletRequest request) throws Exception {
        
    	MltmdCtgryMasterVO ctgrymasterVO = new MltmdCtgryMasterVO();
	    ctgrymasterVO.setSiteId(searchVO.getSiteId());
	    ctgrymasterVO.setFirstIndex(0);
	    ctgrymasterVO.setRecordCountPerPage(999999999);
	    model.addAttribute("ctgrymasterList", mltmdCtgryMasterService.selectMltmdCtgryMasterList(ctgrymasterVO));
	    
    	model.addAttribute("mltmdEnvrnSetupVO", searchVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdEnvrnSetupRegister";
    }
    
    @RequestMapping("/mng/mma/insertMltmdEnvrnSetup.do")
    public String addMltmdEnvrnSetup(final MultipartHttpServletRequest multiRequest, MltmdEnvrnSetupVO mltmdEnvrnSetupVO, @ModelAttribute("searchVO") MltmdEnvrnSetupVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/MltmdEnvrnSetupList.do";
		}
    	
    	List<FileVO> result = null;    
        final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
        	result = fileUtil.directParseFileInf(request, files, "MLTMD_", 0, "Site.fileStorePath", mltmdEnvrnSetupVO.getSiteId());
        	
        	if(result != null) {
    	    	for(int index=0; index < result.size(); index++) {
    	    		FileVO file = result.get(index);
    	    		if(file.getFormNm().startsWith("wmImage")) {
    	    			mltmdEnvrnSetupVO.setWmStreFileNm(file.getStreFileNm());
    	    			mltmdEnvrnSetupVO.setWmOrignlFileNm(file.getOrignlFileNm());
    	    		}
    	    	}
        	}
        }
        
    	mltmdEnvrnSetupService.insertMltmdEnvrnSetup(mltmdEnvrnSetupVO);
    	
    	request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mng/mma/MltmdEnvrnSetupList.do";
    }
    
    @RequestMapping("/mng/mma/selectMltmdEnvrnSetup.do")
    public String selectMltmdEnvrnSetup(MltmdEnvrnSetupVO mltmdEnvrnSetupVO, @ModelAttribute("searchVO") MltmdEnvrnSetupVO searchVO, Model model, HttpServletRequest request) throws Exception {
    	
    	MltmdCtgryMasterVO ctgrymasterVO = new MltmdCtgryMasterVO();
	    ctgrymasterVO.setSiteId(mltmdEnvrnSetupVO.getSiteId());
	    ctgrymasterVO.setFirstIndex(0);
	    ctgrymasterVO.setRecordCountPerPage(999999999);
	    model.addAttribute("ctgrymasterList", mltmdCtgryMasterService.selectMltmdCtgryMasterList(ctgrymasterVO));
	    
    	model.addAttribute("mltmdEnvrnSetupVO", mltmdEnvrnSetupService.selectMltmdEnvrnSetup(mltmdEnvrnSetupVO));
    	
    	model.addAttribute("SiteFileStoreWebPath", propertiesService.getString("Site.fileStoreWebPath"));
    	
    	request.getSession().setAttribute("sessionVO", searchVO);
    	
    	return "/mng/mma/EgovMltmdEnvrnSetupRegister";
    }

    @RequestMapping("/mng/mma/updateMltmdEnvrnSetup.do")
    public String updateMltmdEnvrnSetup(final MultipartHttpServletRequest multiRequest, MltmdEnvrnSetupVO mltmdEnvrnSetupVO, @ModelAttribute("searchVO") MltmdEnvrnSetupVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/MltmdEnvrnSetupList.do";
		}
    	
    	List<FileVO> result = null;    
        final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
        	result = fileUtil.directParseFileInf(request, files, "MLTMD_", 0, "Site.fileStorePath", mltmdEnvrnSetupVO.getSiteId());
        	
        	if(result != null) {
    	    	for(int index=0; index < result.size(); index++) {
    	    		FileVO file = result.get(index);
    	    		if(file.getFormNm().startsWith("wmImage")) {
    	    			mltmdEnvrnSetupVO.setWmStreFileNm(file.getStreFileNm());
    	    			mltmdEnvrnSetupVO.setWmOrignlFileNm(file.getOrignlFileNm());
    	    		}
    	    	}
        	}
        }
        
    	mltmdEnvrnSetupService.updateMltmdEnvrnSetup(mltmdEnvrnSetupVO);
        
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/mma/MltmdEnvrnSetupList.do";
    }
    
    @RequestMapping("/mng/mma/deleteMltmdEnvrnSetup.do")
    public String deleteMltmdEnvrnSetup(MltmdEnvrnSetupVO mltmdEnvrnSetupVO, @ModelAttribute("searchVO") MltmdEnvrnSetupVO searchVO) throws Exception {
        
    	mltmdEnvrnSetupService.deleteMltmdEnvrnSetup(mltmdEnvrnSetupVO);
        
        return "forward:/mng/mma/MltmdEnvrnSetupList.do";
    }

}
