package egovframework.com.mng.mma.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.mma.service.MltmdCtgryService;
import egovframework.com.mma.service.MltmdCtgryVO;
import egovframework.com.mma.service.MltmdEnvrnSetupService;
import egovframework.com.mma.service.MltmdEnvrnSetupVO;
import egovframework.com.mma.service.MltmdFileService;
import egovframework.com.mma.service.MltmdFileVO;
import egovframework.com.mma.service.MltmdMvpCnvrInfoService;
import egovframework.com.mma.service.MltmdMvpInfoService;
import egovframework.com.mma.service.MltmdMvpInfoVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;

/**
 * @Class Name : EgovMltmdMvpInfoController.java
 * @Description : MltmdMvpInfo Controller class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller("mngEgovMltmdMvpInfoController")
public class EgovMltmdMvpInfoController {

    @Resource(name = "mltmdMvpInfoService")
    private MltmdMvpInfoService mltmdMvpInfoService;
    
    @Resource(name = "mltmdCtgryService")
    private MltmdCtgryService mltmdCtgryService;
    
    @Resource(name = "mltmdEnvrnSetupService")
    private MltmdEnvrnSetupService mltmdEnvrnSetupService;
    
    @Resource(name = "mltmdMvpCnvrInfoService")
    private MltmdMvpCnvrInfoService mltmdMvpCnvrInfoService;
    
    @Resource(name = "mltmdFileService")
    private MltmdFileService mltmdFileService;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
    
    @Autowired
	private DefaultBeanValidator beanValidator;
	
    /**
     * XSS 방지 처리.
     * 
     * @param data
     * @return
     */
    protected String unscript(String data) {
      if(data == null || data.trim().equals("")) {
        return "";
      }
      
      String ret = data;
      
      ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
      ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");
      
      //ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
      //ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");
      
      ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
      ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");
      
      // ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
      // ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
      
      ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      
      return ret;
    }
    
    /**
	 * COMTNMLTMDMVPINFO 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 MltmdMvpInfoVO
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/mma/MltmdMvpInfoList.do")
    public String selectMltmdMvpInfoList(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			searchVO.setSiteId(loginVO.getSiteId());
		}
		
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		if(!EgovStringUtil.isEmpty(searchVO.getSiteId())) {
			
			MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
	    	setupVO.setSiteId(searchVO.getSiteId());    	
	    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
	    	model.addAttribute("mltmdSetup", setupVO);
	    	
	    	if(setupVO != null) {	    		
		    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
				ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());
				ctgry.setSearchLevel("1");
				model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
				model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
				
				searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
				searchVO.setAdminAt("Y");
		        List<MltmdMvpInfoVO> mltmdMvpInfoList = mltmdMvpInfoService.selectMltmdMvpInfoList(searchVO);
		        model.addAttribute("resultList", mltmdMvpInfoList);
		        
		        int totCnt = mltmdMvpInfoService.selectMltmdMvpInfoListTotCnt(searchVO);
				paginationInfo.setTotalRecordCount(totCnt);
	    	} else {
	    		model.addAttribute("message", egovMessageSource.getMessage("mltmd.config.notFound"));
	    	}
		}
        model.addAttribute("paginationInfo", paginationInfo);
        
        //동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
        
        return "/mng/mma/EgovMltmdMvpInfoList";
    } 
    
    @RequestMapping("/mng/mma/selectMltmdMvpInfo.do")
    public String selectMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request) throws Exception {
       
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	model.addAttribute("mltmdSetup", setupVO);
    	
		searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
		searchVO.setAdminAt("Y");
		MltmdMvpInfoVO mvpInfo = mltmdMvpInfoService.selectMltmdMvpInfo(searchVO);
    	model.addAttribute("mltmdMvpInfoVO", mvpInfo);
    	
    	MltmdFileVO mltmdFileVO = new MltmdFileVO();
    	mltmdFileVO.setMltmdFileId(mvpInfo.getMltmdFileId());
    	model.addAttribute("mltmdMvpCnvrInfo", mltmdMvpCnvrInfoService.selectMltmdMvpCnvrInfo(mltmdFileVO));
    	
    	model.addAttribute("mediaMovieServiceWebUrl", propertiesService.getString("mediaMovieServiceWebUrl"));
    	//동영상 이미지 웹경로.
    	model.addAttribute("mediaMovieImageWebUrl", propertiesService.getString("mediaMovieImageWebUrl"));
    	
        return "/mng/mma/EgovMltmdMvpInfoInqire";
    }
    
    @RequestMapping("/mng/mma/addMltmdMvpInfo.do")
    public String addMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request) throws Exception {

    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	model.addAttribute("mltmdSetup", setupVO);
    	
    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());
		ctgry.setSearchLevel("1");
		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
		
		searchVO.setTakeMvpId(mltmdMvpInfoService.selectMltmdMvpId());
    	model.addAttribute("mltmdMvpInfoVO", searchVO);
    	
    	MltmdFileVO mvpFileVO = new MltmdFileVO();
    	mvpFileVO.setMvpBassCours(mltmdFileService.selectMltmdFileBassCours());
    	model.addAttribute("mvpFileVO", mvpFileVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdMvpInfoRegister";
    }
    
    @RequestMapping("/mng/mma/insertMltmdMvpInfo.do")
    public String insertMltmdMvpInfo(MltmdFileVO mltmdFileVO, MltmdMvpInfoVO mltmdMvpInfoVO, BindingResult bindingResult, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	beanValidator.validate(mltmdMvpInfoVO, bindingResult);
    	
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	
    	if(bindingResult.hasErrors()) {   		
        	
    		model.addAttribute("mltmdSetup", setupVO);
    		
        	MltmdCtgryVO ctgry = new MltmdCtgryVO();
    		ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());
    		ctgry.setSearchLevel("1");
    		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
			model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
    		
    		return "/mng/mma/EgovMltmdMvpInfoRegister";
    	}
    	
    	
    	Map<String, String> fileIds  = mltmdFileService.saveData(mltmdFileVO);
    	if(fileIds != null) {
    		if(fileIds.get("mvpFileId") != null) {
    			mltmdFileVO.setMvpFileId(fileIds.get("mvpFileId"));
	    	}
    	}
    	mltmdMvpInfoVO.setMltmdFileId(mltmdFileVO.getMvpFileId());
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);   
    	mltmdMvpInfoVO.setConfmAt(setupVO.getAtmcConfmAt());
    	if("Y".equals(setupVO.getAtmcConfmAt())) {
    		mltmdMvpInfoVO.setConfmerId("SYSTEM");
    	}
    	mltmdMvpInfoVO.setFrstRegisterId(user.getId());
    	mltmdMvpInfoVO.setWrterNm(user.getName());
    	mltmdMvpInfoVO.setMvpCn(unscript(mltmdMvpInfoVO.getMvpCn()));
    	
    	mltmdMvpInfoService.insertMltmdMvpInfo(mltmdMvpInfoVO);
    	
    	request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mng/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mng/mma/forUpdateMltmdMvpInfo.do")
    public String forUpdateMltmdMvpInfo(@ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request) throws Exception {
       
    	MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
    	setupVO.setSiteId(searchVO.getSiteId());    	
    	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
    	model.addAttribute("mltmdSetup", setupVO);
    	
    	MltmdCtgryVO ctgry = new MltmdCtgryVO();
		ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());
		ctgry.setSearchLevel("1");
		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
		model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
		
		searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
		searchVO.setAdminAt("Y");
		MltmdMvpInfoVO mvpInfo = mltmdMvpInfoService.selectMltmdMvpInfo(searchVO);
    	model.addAttribute("mltmdMvpInfoVO", mvpInfo);
    	
    	MltmdFileVO mvpFileVO = new MltmdFileVO();
    	mvpFileVO.setMltmdFileId(mvpInfo.getMltmdFileId());
    	mvpFileVO = mltmdFileService.selectMltmdFile(mvpFileVO);
    	mvpFileVO.setMvpFileId(mvpFileVO.getMltmdFileId());
    	mvpFileVO.setMvpBassCours(mvpFileVO.getBassCours());
    	mvpFileVO.setMvpJsonData(mltmdFileService.selectMediaMovieJsonData(mvpFileVO));
    	model.addAttribute("mvpFileVO", mvpFileVO);
    	
    	request.getSession().setAttribute("sessionVO", searchVO);
    	
        return "/mng/mma/EgovMltmdMvpInfoRegister";
    }

    @RequestMapping("/mng/mma/updateMltmdMvpInfo.do")
    public String updateMltmdMvpInfo(MltmdMvpInfoVO mltmdMvpInfoVO, BindingResult bindingResult, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/selectMltmdCtgryMasterList.do";
		}
    	
    	beanValidator.validate(mltmdMvpInfoVO, bindingResult);
    	
    	if(bindingResult.hasErrors()) {
    		
    		MltmdEnvrnSetupVO setupVO = new MltmdEnvrnSetupVO();
        	setupVO.setSiteId(searchVO.getSiteId());    	
        	setupVO = mltmdEnvrnSetupService.selectMltmdEnvrnSetup(setupVO);
        	model.addAttribute("mltmdSetup", setupVO);
        	
        	MltmdCtgryVO ctgry = new MltmdCtgryVO();
    		ctgry.setCtgrymasterId(setupVO.getCtgrymasterId());
    		ctgry.setSearchLevel("1");
    		model.addAttribute("mltmdCateList", mltmdCtgryService.selectMltmdCtgryList(ctgry));
			model.addAttribute("mltmdCateLevel", mltmdCtgryService.selectMltmdCtgryLevel(ctgry));
    		
			searchVO.setCtgrymasterId(setupVO.getCtgrymasterId());
			searchVO.setAdminAt("Y");
    		model.addAttribute("mltmdMvpInfoVO", mltmdMvpInfoService.selectMltmdMvpInfo(searchVO));
    		
    		return "/mng/mma/EgovMltmdMvpInfoRegister";
    	}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);   
    	mltmdMvpInfoVO.setAdminAt("Y");
    	mltmdMvpInfoVO.setLastUpdusrId(user.getId());
    	mltmdMvpInfoVO.setMvpCn(unscript(mltmdMvpInfoVO.getMvpCn()));
    	mltmdMvpInfoService.updateMltmdMvpInfo(mltmdMvpInfoVO);
    	
    	request.getSession().removeAttribute("sessionVO");
                
        return "forward:/mng/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mng/mma/deleteMltmdMvpInfo.do")
    public String deleteMltmdMvpInfo(MltmdMvpInfoVO mltmdMvpInfoVO, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);  
    	mltmdMvpInfoVO.setAdminAt("Y");
    	mltmdMvpInfoVO.setLastUpdusrId(user.getId());
    	mltmdMvpInfoService.deleteMltmdMvpInfo(mltmdMvpInfoVO);
        
        return "forward:/mng/mma/MltmdMvpInfoList.do";
    }
    
    @RequestMapping("/mng/mma/updateMltmdMvpConfirm.do")
    public String updateMltmdMvpConfirm(MltmdMvpInfoVO mltmdMvpInfoVO, @ModelAttribute("searchVO") MltmdMvpInfoVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);  
    	mltmdMvpInfoVO.setAdminAt("Y");
    	mltmdMvpInfoVO.setLastUpdusrId(user.getId());
    	mltmdMvpInfoService.updateMltmdMvpConfirm(mltmdMvpInfoVO);
        
        return "forward:/mng/mma/MltmdMvpInfoList.do";
    }

}
