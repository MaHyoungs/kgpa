package egovframework.com.mng.mma.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import egovframework.com.mma.service.MltmdServerService;
import egovframework.com.mma.service.MltmdServerVO;

/**
 * @Class Name : EgovMltmdServerController.java
 * @Description : MltmdServer Controller class
 * @Modification Information
 *
 * @author 정정욱
 * @since 2012.11.14
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class EgovMltmdServerController {

    @Resource(name = "mltmdServerService")
    private MltmdServerService mltmdServerService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
	 * COMTNMLTMDSERVER 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 MltmdServerVO
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/mma/MltmdServerList.do")
    public String selectMltmdServerList(@ModelAttribute("searchVO") MltmdServerVO searchVO, ModelMap model) throws Exception {
    	
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
        List<MltmdServerVO> mltmdServerList = mltmdServerService.selectMltmdServerList(searchVO);
        model.addAttribute("resultList", mltmdServerList);
        
        int totCnt = mltmdServerService.selectMltmdServerListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        return "/mng/mma/EgovMltmdServerList";
    } 
    
    @RequestMapping("/mng/mma/addMltmdServer.do")
    public String addMltmdServerView(@ModelAttribute("searchVO") MltmdServerVO searchVO, Model model, HttpServletRequest request) throws Exception {
        
    	model.addAttribute("mltmdServerVO", searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/mma/EgovMltmdServerRegister";
    }
    
    @RequestMapping("/mng/mma/insertMltmdServer.do")
    public String addMltmdServer(MltmdServerVO mltmdServerVO, @ModelAttribute("searchVO") MltmdServerVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/MltmdEnvrnSetupList.do";
		}
    	
    	mltmdServerService.insertMltmdServer(mltmdServerVO);
    	
    	request.getSession().removeAttribute("sessionVO");
                
        return "forward:/mng/mma/MltmdServerList.do";
    }
    
    @RequestMapping("/mng/mma/selectMltmdServer.do")
    public String selectMltmdServer(MltmdServerVO mltmdServerVO, @ModelAttribute("searchVO") MltmdServerVO searchVO, Model model, HttpServletRequest request) throws Exception {
        
    	model.addAttribute("mltmdServerVO", mltmdServerService.selectMltmdServer(mltmdServerVO));
    	request.getSession().setAttribute("sessionVO", searchVO);
    	
    	return "/mng/mma/EgovMltmdServerRegister";
    }

    @RequestMapping("/mng/mma/updateMltmdServer.do")
    public String updateMltmdServer(MltmdServerVO mltmdServerVO, @ModelAttribute("searchVO") MltmdServerVO searchVO, HttpServletRequest request) throws Exception {
        
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/mma/MltmdEnvrnSetupList.do";
		}
    	
    	mltmdServerService.updateMltmdServer(mltmdServerVO);
    	
    	request.getSession().removeAttribute("sessionVO");
       
        return "forward:/mng/mma/MltmdServerList.do";
    }
    
    @RequestMapping("/mng/mma/deleteMltmdServer.do")
    public String deleteMltmdServer(MltmdServerVO mltmdServerVO, @ModelAttribute("searchVO") MltmdServerVO searchVO) throws Exception {
        mltmdServerService.deleteMltmdServer(mltmdServerVO);
        return "forward:/mng/mma/MltmdServerList.do";
    }

}
