package egovframework.com.mng.msi.web;

import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller("MngEgovTemplateController")
public class EgovTemplateController {

	@Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

	/**
	 * 관리자 메인 포워딩
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mng/index.do")
	public String index(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		//정보공개 권한 > 정보목록 페이지 이동
		if(user.getUserSe().equals("04") || user.getUserSe().equals("05")){
			return "forward:/mng/cop/bbs/selectOpenBoardList.do";
		}

		//한반도산림복원 권한 > 한반도산림복원관리 > 회원관리 페이지 이동
		else if(user.getUserSe().equals("07")){
			return "forward:/mng/forest/usr/EgovMberManage.do";
		}

		//녹색자금통합관리 권한 > 녹색자금통합관리 > 사업공고 페이지 이동
		else if(user.getUserSe().equals("09")){
			return "forward:/mng/gfund/biz/ancmt/announcementList.do";
		}

		//홈페이지관리자 권한 > 시스템관리 > 사이트관리 페이지 이동
		else if(user.getUserSe().equals("10")){
			return "forward:/mng/sym/sit/selectSiteInfoList.do";
		}

		//소외계층관리자 권한 > 소외계층 체험교육 > 소외계층 체험교육 게시판 페이지 이동
		else if(user.getUserSe().equals("11")){
			return "forward:/mng/UnderPrivilegedBoardList.jsp";
		}

		//공모전관리자 권한 > 녹색자금사업 아이디어 공모전 > 녹색자금사업 아이디어 공모전 게시판 페이지 이동
		else if(user.getUserSe().equals("12")){
			return "forward:/mng/UnderContestBoardList.jsp";
		}

		//통합관리자 권한 > 시스템관리 > 사이트관리 페이지 이동
		else{
			return "forward:/mng/sym/sit/selectSiteInfoList.do";
		}
	}

	/**
	 * 관리자 상단 메뉴, 좌측메뉴
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mng/template/top.do")
	public String top(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "mng/template/top";
	}

	/**
	 * 관리자 하단
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mng/template/bottom.do")
	public String bottom(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "mng/template/bottom";
	}

	/**
	 * 관리자 팝업 상단
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mng/template/popTop.do")
	public String popTop(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "mng/template/popTop";
	}

	/**
	 * 관리자 팝업 하단
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mng/template/popBottom.do")
	public String popBottom(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "mng/template/popBottom";
	}
}
