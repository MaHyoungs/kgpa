package egovframework.com.mng.uss.umt.web;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Controller
public class GFndUserManageController {

    /**
     * EgovMessageSource
     */
    @Resource(name = "egovMessageSource")
    EgovMessageSource egovMessageSource;

    /**
     * EgovUserManageService
     */
    @Resource(name = "userManageService")
    private EgovUserManageService userManageService;

    /**
     * EgovPropertyService
     */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    /**
     * DefaultBeanValidator
     */
    @Autowired
    private DefaultBeanValidator beanValidator;

    /**
     * EgovFileMngUtil
     */
    @Resource(name = "EgovFileMngUtil")
    private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovCmmUseService")
    private EgovCmmUseService cmmUseService;

    /**
     * 사용자목록을 조회한다.
     *
     * @param model 화면모델
     * @return mng/gfund/member/EgovMemberList
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovMberManage.do")
    public String selectEgovMberManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        userManageVO.setSiteId("SITE_000000000000002");

        userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
        userManageVO.setPageSize(propertiesService.getInt("pageSize"));

        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
        paginationInfo.setPageSize(userManageVO.getPageSize());

        userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
        userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

        model.addAttribute("resultList", userManageService.selectUserList(userManageVO));

        int totCnt = userManageService.selectUserListTotCnt(userManageVO);
        paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

        return "mng/gfund/member/EgovMemberList";
    }

    /**
     * 사용자 등록화면으로 이동한다.
     *
     * @param userManageVO
     * @param request
     * @param model
     * @return "/mng/gfund/member/EgovMberAddView.do"
     * @throws Exception
     */

    @RequestMapping(value = "/mng/gfund/member/EgovMberAddView.do")
    public String EgovMberAddView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {

        return "mng/gfund/member/EgovMemberIndt";
    }


    /**
     * 사용자 정보를 DB에 입력한다.
     *
     * @param model 화면모델
     * @return forward:/mng/gfund/member/EgovUserSelectIndt.do
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/mng/gfund/member/EgovUserSelectIndt.do")
    public String EgovUserSelectIndt(UserManageVO userManageVO) throws Exception {
        userManageVO.setSiteId("SITE_000000000000002");
        String zip = EgovStringUtil.removeMinusChar(userManageVO.getZip());
        userManageVO.setZip(zip);
        userManageService.insertUser(userManageVO);
        return "forward:/mng/gfund/member/EgovMberManage.do";
    }


    /**
     * 사용자목록을 엑셀로 다운받는다.
     *
     * @param searchVO - 조회할 정보가 담긴 ComtneventprzwnerDefaultVO
     * @return "/mng/evt/ComtneventprzwnerList"
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovMberManageExcel.do")
    public String EgovMberManageExcel(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model) throws Exception {

        int totCnt = userManageService.selectUserListTotCnt(userManageVO);

        userManageVO.setFirstIndex(0);
        userManageVO.setRecordCountPerPage(totCnt);

        model.addAttribute("resultList", userManageService.selectUserList(userManageVO));

        return "mng/gfund/member/EgovMemberListExcel";
    }

    /**
     * 사용자정보 수정 화면으로 이동한다.
     *
     * @param model 화면모델
     * @return mng/gfund/member/EgovMemberUpdt
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovUserSelectUpdtView.do")
    public String EgovUserSelectUpdtView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {

        if ("08".equals(userManageVO.getUserSeCode())) {
            ComDefaultCodeVO vo = new ComDefaultCodeVO();
            vo.setCodeId("DEPT01");
            List<CmmnDetailCode> nameList = cmmUseService.selectCmmCodeDetail(vo);
            model.addAttribute("codeList", nameList);
        }

        model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));

        model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

        return "mng/gfund/member/EgovMemberUpdt";
    }


    /**
     * 사용자정보 수정 처리 한다.
     *
     * @param model 화면모델
     * @return forward:/mng/gfund/member/EgovUserSelectUpdtView.do
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/mng/gfund/member/EgovUserSelectUpdt.do")
    public String EgovUserSelectUpdt(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {


	   if(userManageService.selectUser(userManageVO) == null) {
		   return "mng/gfund/member/EgovMemberUpdt";
	   }

       List<FileVO> result = null;
       final Map<String, MultipartFile> files = multiRequest.getFileMap();
       if (!files.isEmpty()) {
           result = fileUtil.directParseFileInf(request, files, "MEM_", 0, "Members.fileStorePath", "");
           if (result != null) {
               for (int index = 0; index < result.size(); index++) {
                   FileVO file = result.get(index);
                   if (file.getFormNm().startsWith("user")) {
                       userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
                       userManageVO.setPhotoStreFileNm(file.getStreFileNm());
                   }
                }
           }
       }

       if (!EgovStringUtil.isEmpty(userManageVO.getTel1()) && !EgovStringUtil.isEmpty(userManageVO.getTel2()) && !EgovStringUtil.isEmpty(userManageVO.getTel3())) {
           userManageVO.setTlphonNo(userManageVO.getTel1() + "-" + userManageVO.getTel2() + "-" + userManageVO.getTel3());
       }
       if (!EgovStringUtil.isEmpty(userManageVO.getPhone1()) && !EgovStringUtil.isEmpty(userManageVO.getPhone2()) && !EgovStringUtil.isEmpty(userManageVO.getPhone3())) {
           userManageVO.setMoblphonNo(userManageVO.getPhone1() + "-" + userManageVO.getPhone2() + "-" + userManageVO.getPhone3());
       }
       if (!EgovStringUtil.isEmpty(userManageVO.getEmail1()) && !EgovStringUtil.isEmpty(userManageVO.getEmail2())) {
           userManageVO.setEmailAdres(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
       }
	        LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	        userManageVO.setLastUpdusrId(user.getId());

            userManageVO.setLastUpdusrId(user.getId());
            //userManageVO.setBrthdy(userManageVO.getBrthdy1()+userManageVO.getBrthdy2()+userManageVO.getBrthdy3());
            String zip = EgovStringUtil.removeMinusChar(userManageVO.getZip());
            userManageVO.setZip(zip);

            userManageService.updateManageUser(userManageVO);
            return "forward:/mng/gfund/member/EgovMberManage.do";
    }


    /**
     * 관리자가 패스워드를 임의로 재발급하고 핸드폰으로 전송 한다.
     *
     * @param userManageVO
     * @param request
     * @param commandMap
     * @param model
     * @return "forward:/mng/gfund/member/SendPassword.do"
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/SendPassword.do")
    public String SendPassword(@ModelAttribute("searchVO") UserManageVO userManageVO, final HttpServletRequest request, Model model) throws Exception {

        model.addAttribute("target", "searchPasswordBySms");    //SMS모드로 전송한다.

        // 1. 비밀번호 찾기
        Map<String, Object> resultList = userManageService.updateSendPassword(userManageVO);
        boolean result = (Boolean) resultList.get("sendResult");

        // 2. 결과 리턴
        if (result) {
            model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
        } else {
            model.addAttribute("message", egovMessageSource.getMessage("fail.request.msg"));
        }

        return "mng/gfund/member/SendPassword";
    }

    /**
     * 선택한 사용자 목록을 접속금지/해제 처리 한다.
     *
     * @param userManageVO
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovMberManageConfrm.do")
    public String mberManageConfrm(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model) throws Exception {

        if ("N".equals(userManageVO.getConfmAt())) {
            userManageService.updateUserRhibt(userManageVO);
        } else if ("Y".equals(userManageVO.getConfmAt())) {
            userManageService.updateUserRelis(userManageVO);
        }

        //model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
        return "forward:/mng/gfund/member/EgovMberManage.do";
    }

    /**
     * 선택한 사용자 목록을 접속금지/해제 처리 한다.
     *
     * @param userManageVO
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovMberManageDelete.do")
    public String mberManageDelete(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
        userManageVO.setLastUpdusrId(user.getId());
        userManageService.deleteUser(userManageVO);

        //model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
        return "forward:/mng/gfund/member/EgovMberManage.do";
    }

    /**
     * 사용자 엑셀파일을 일괄 업로드한다.
     *
     * @param userManageVO
     * @param request
     * @param model
     * @return "mng/gfund/member/EgovMberExcelUpload"
     * @throws Exception
     */
    @RequestMapping(value = "/mng/gfund/member/EgovMberExcelUploadView.do")
    public String EgovMberExcelUploadView(@ModelAttribute("searchVO") UserManageVO userManageVO, Model model) throws Exception {
        return "mng/gfund/member/EgovMberExcelUpload";
    }

    /**
     * 사용자 엑셀파일을 일괄 업로드한다.
     *
     * @param userManageVO
     * @param request
     * @param commandMap
     * @param model
     * @return "mng/gfund/member/EgovMberExcelUpload"
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/mng/gfund/member/EgovMberExcelUpload.do")
    public String EgovMberExcelUpload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") UserManageVO userManageVO, Model model) throws Exception {

        Map<String, Object> resultList = null;

        final Map<String, MultipartFile> files = multiRequest.getFileMap();

        Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
        MultipartFile file;

        while (itr.hasNext()) {
            Entry<String, MultipartFile> entry = itr.next();

            file = entry.getValue();

            if (!"".equals(file.getOriginalFilename())) {
                int index = file.getOriginalFilename().lastIndexOf(".");
                String fileExt = "";
                if (index != -1) {
                    fileExt = file.getOriginalFilename().substring(index + 1).toUpperCase();
                }
                resultList = userManageService.insertUserExcelUpload(userManageVO, fileExt, file.getInputStream());
                model.addAttribute("message", resultList.get("message"));
                if (resultList.containsKey("dataList")) {
                    model.addAttribute("dataList", resultList.get("dataList"));
                }
            }
        }

        return "mng/gfund/member/EgovMberExcelUpload";
    }
}
