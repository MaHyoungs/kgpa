package egovframework.com.msi.web;

import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.mpm.service.Mpm;
import egovframework.com.sym.mpm.service.MpmVO;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * ***************************************************
 *
 * @author : 이호영
 * @version : 1.0.0
 * @Class Name : EmtMainTemplateController.java
 * @Program name : egovframework.com.msi.web
 * @Descriptopn :
 * @created date : 2011. 7. 26.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2012. 7. 26.        이호영             first generated
 * 2012. 9. 01.        문동열
 * *******************************************************
 */

@Controller
public class TemplateServiceController{
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovMpmService")
	private EgovMpmService egovMpmService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@RequestMapping(value = "/index.do")
	public String index(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//메인페이지 여부
		model.addAttribute("isMain", "Y");

		//https 접속 시 _WEB_FULL_PATH 설정
		String url = request.getRequestURL().toString();
		String user_protocol = "";
		if (url.startsWith("https://")) {
			user_protocol = "https://";
		} else {
			user_protocol = "http://";
		}

		if(url.contains("gfund")) {
		} else if(url.contains("nkrefo")) {
		} else if(url.contains("mng")) {
		} else {
			return "msi/intro";
		}

		model.addAttribute("user_protocol", user_protocol);
		//System.out.println("user_protocol >>> " + user_protocol);

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//return "msi/cntntsService";
		/*return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcMain" + (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");*/
		return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcMain";
	}


	@RequestMapping(value = "/msi/tmplatHead.do")
	public String tmplatUpper(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		/** EgovPropertyService.SiteList */
		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(siteVO, mnuVO, model, true, true);
		/*return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcHead" + (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");*/
		return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcHead";
	}


	@RequestMapping(value = "/msi/tmplatSub.do")
	public String tmplatSub(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		/** EgovPropertyService.SiteList */
		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(siteVO, mnuVO, model, true, true);
		/*return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcSub" + (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");*/
		return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcSub";
	}


	@RequestMapping(value = "/msi/tmplatBottom.do")
	public String tmplatBottom(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(siteVO, mnuVO, model, true, false);

		/*return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcBottom" + (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");*/
		return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile") + "sit/" + siteVO.getLytSourcId() + "/sourcBottom";
	}


	@RequestMapping(value = "/msi/cntntsService.do")
	public String cntnsService(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.

		String url = request.getRequestURL().toString();
		if(url.contains("gfund")) {
		} else if(url.contains("nkrefo")) {
		} else if(url.contains("mng")) {
		} else {
			return "redirect:http://www.kgpa.or.kr/index.do";
		}

		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(siteVO, mnuVO, model, true, false);

		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));
		return "msi/cntntsService.tiles";
	}


	@RequestMapping(value = "/msi/siteMap.do")
	public String siteMap(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		mnuVO.setSiteId(siteVO.getSiteId());
		List<Mpm> mpmList = egovMpmService.selectMpmServiceList(mnuVO);

		model.addAttribute("mnMnuList", mpmList);

		return "msi/siteMap.tiles";

	}


	@RequestMapping(value = "/msi/indvdlInfoPolicy.do")
	public String indvdlInfoPolicy(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/indvdlInfoPolicy");

		return "msi/cntntsService";

	}


	@RequestMapping(value = "/msi/useStplat.do")
	public String useStplat(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/useStplat");

		return "msi/cntntsService";

	}


	@RequestMapping(value = "/msi/emailColctPolicy.do")
	public String emailColctPolicy(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/emailColctPolicy");

		return "msi/cntntsService";
	}


	@RequestMapping(value = "/msi/cpyrhtSttemntSvc.do")
	public String cpyrhtSttemntSvc(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/cpyrhtSttemntSvc");

		return "msi/cntntsService";
	}


	private void modelMpmDataBinding(SiteManageVO siteVO, MpmVO mnuVO, ModelMap model, boolean publishBinding, boolean mainContentsBinding) throws Exception{
		List<Mpm> mpmList = egovMpmService.selectMpmServiceList(mnuVO);
		model.addAttribute("mpmList", mpmList);

		Mpm currMpm = egovMpmService.selectMpmCurrent(mpmList, mnuVO);

		//미리보기일 경우 
		if("Y".equals(mnuVO.getPreviewYn())){
			if(currMpm != null){
				mnuVO.setMenuPathByName(currMpm.getMenuPathByName());
				mnuVO.setMenuPathById(currMpm.getMenuPathById());
				mnuVO.setMenuLevel(currMpm.getMenuLevel());
				mnuVO.setMenuLastNodeAt(currMpm.getMenuLastNodeAt());
			}else{
				Mpm uppperMpm = egovMpmService.selectMpmFind(mpmList, mnuVO.getUpperMenuId());
				if(uppperMpm != null){
					mnuVO.setMenuPathByName(uppperMpm.getMenuPathByName() + ">" + mnuVO.getMenuNm());
					mnuVO.setMenuPathById(uppperMpm.getMenuPathById() + ">" + mnuVO.getMenuId());
					mnuVO.setMenuLevel(uppperMpm.getMenuLevel() + 1);
					mnuVO.setMenuLastNodeAt("Y");
				}
			}
			currMpm = mnuVO;
		}

		if(mainContentsBinding && "Y".equals(mnuVO.getIsMain())){
			if(siteVO.getMainContentsList() != null){
				Mpm progrmMpm = null;
				for(int i = 0; i < siteVO.getMainContentsList().size(); i++){
					progrmMpm = egovMpmService.selectMpmProgram(mpmList, siteVO.getMainContentsList().get(i).getProgrmId());
					if(progrmMpm != null){
						siteVO.getMainContentsList().get(i).setMenuId(progrmMpm.getMenuId());
					}
				}
			}

		}

		model.addAttribute("currMpm", currMpm);

		model.addAttribute("currRootMpm", egovMpmService.selectMpmCurrentRoot(mpmList, currMpm));

		if(publishBinding){
			model.addAttribute("MenuFileStoreWebPath", propertyService.getString("Menu.fileStoreWebPath"));

			model.addAttribute("MnuFileStoreWebPathByWebFile", propertyService.getString("publish.mnu.fileStoreWebPathByWebFile"));
			model.addAttribute("MnuFileStoreWebPathByJspFile", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile"));

			model.addAttribute("LytFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile"));
			model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
		}
	}


	@RequestMapping(value = "/msi/cmm/tmplatHead.do")
	public String cmmTmplatHead(UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		//SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		//model.addAttribute("siteInfo", siteVO);
		SiteManageVO siteVO = null;
		if(EgovStringUtil.isEmpty(userManageVO.getSiteId())){
			siteVO = siteManageService.selectSiteServiceInfo(request);
		}else{
			siteVO = siteManageService.selectSiteServiceInfoBySiteId(userManageVO.getSiteId());
		}

		if(siteVO != null){
			model.addAttribute("siteInfo", siteVO);
		}

		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/cmm/tmplatHead";
	}


	@RequestMapping(value = "/msi/cmm/tmplatBottom.do")
	public String cmmTmplatBottom(UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		//SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		//model.addAttribute("siteInfo", siteVO);
		SiteManageVO siteVO = null;
		if(EgovStringUtil.isEmpty(userManageVO.getSiteId())){
			siteVO = siteManageService.selectSiteServiceInfo(request);
		}else{
			siteVO = siteManageService.selectSiteServiceInfoBySiteId(userManageVO.getSiteId());
		}

		if(siteVO != null){
			model.addAttribute("siteInfo", siteVO);
		}
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/cmm/tmplatBottom";
	}


	@RequestMapping(value = "/msi/sch/tmplatHead.do")
	public String schTmplatHead(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/sch/tmplatHead";
	}


	@RequestMapping(value = "/msi/sch/tmplatBottom.do")
	public String schTmplatBottom(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		//사이트 설정 웹경로.
		model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/sch/tmplatBottom";
	}
}