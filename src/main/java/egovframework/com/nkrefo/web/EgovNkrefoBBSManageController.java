package egovframework.com.nkrefo.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.*;
import egovframework.com.mng.forest.service.NkrefoManageVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 게시물 관리를 위한 컨트롤러 클래스
 *
 * @author 공통서비스개발팀 이삼섭
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------       --------    ---------------------------
 *   2009.3.19  이삼섭          최초 생성
 *   2009.06.29	한성곤
 *
 * </pre>
 * @since 2009.06.01
 */
@Controller("EgovNkrefoBBSManageController")
public class EgovNkrefoBBSManageController{

	@Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;

	@Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;

	@Resource(name = "EgovBBSCtgryService")
	private EgovBBSCtgryService ctgryService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovBBSCommentService")
	protected EgovBBSCommentService bbsCommentService;

	@Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	@Autowired
	private DefaultBeanValidator beanValidator;

	Logger log = LogManager.getLogger(this.getClass());

	/**
	 * XSS 방지 처리.
	 *
	 * @param data
	 * @return
	 */
	protected String unscript(String data){
		if(data == null || data.trim().equals("")){
			return "";
		}

		String ret = data;

		ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
		ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

		ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
		ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

		ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
		ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

		ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
		ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

		ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
		ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

		return ret;
	}

	/**
	 * 한반도 산림복원 자료실 게시물에 대한 목록을 조회한다.
	 * @param nkrefoManageVO
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/bbs/selectNkrefoBoardList.do")
	public String selectNkrefoBoardArticles(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/egovLoginUsr.do";
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());
		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		if(master != null){
			int SE_CODE = 1;
			if(user != null){
				SE_CODE = Integer.parseInt(user.getUserSe());
				model.addAttribute("USER_INFO", user);
			}

			if(SE_CODE >= 10){
				boardVO.setAdminAt("Y");
			}

			PaginationInfo paginationInfo = new PaginationInfo();

			// 페이징 정보 설정
			boardVO.setPageUnit(propertyService.getInt("pageUnit"));
			boardVO.setPageSize(propertyService.getInt("pageSize"));
			boardVO.setCtgrymasterId(master.getCtgrymasterId());

			if("BBSA02".equals(master.getBbsAttrbCode()) || "BBSA04".equals(master.getBbsAttrbCode())){
				// 페이징 정보 설정
				boardVO.setPageUnit(propertyService.getInt("photoPageUnit"));
				boardVO.setPageSize(propertyService.getInt("photoPageSize"));
			}else if("BBSA11".equals(master.getBbsAttrbCode())){
				if(user == null){
					return "cmm/uss/umt/EgovUserPrivateCertification.tiles";
				}else{
					boardVO.setCredtId(user.getCredtId());
					boardVO.setOldSecId(user.getOldSecId());
				}
			}else if("BBSA07".equals(master.getBbsAttrbCode())){
				boardVO.setFrstRegisterId(user.getId());
				boardVO.setCredtId(user.getCredtId());
			}else{
				//공지게시물 가져오기
				BoardVO noticeVO = new BoardVO();
				noticeVO.setBbsId(boardVO.getBbsId());
				noticeVO.setCommentUseAt(master.getCommentUseAt());
				noticeVO.setCtgrymasterId(master.getCtgrymasterId());
				noticeVO.setSearchNoticeAt("Y");
				noticeVO.setFirstIndex(0);
				noticeVO.setRecordCountPerPage(9999);

				model.addAttribute("noticeList", bbsMngService.selectBoardArticles(noticeVO));
			}

			paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
			paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
			paginationInfo.setPageSize(boardVO.getPageSize());

			boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
			boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
			boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

			boardVO.setCommentUseAt(master.getCommentUseAt());
			boardVO.setBbsAttrbCode(master.getBbsAttrbCode());

			List<BoardVO> resultList = new ArrayList<BoardVO>();
			int totCnt = 0;

			if("BBSA06".equals(master.getBbsAttrbCode())){
				boardVO.setSearchSttus("BBSS03");
			}

			resultList = bbsMngService.selectBoardArticles(boardVO);
			totCnt = bbsMngService.selectBoardArticlesCnt(boardVO);

			paginationInfo.setTotalRecordCount(totCnt);

			if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())){
				Ctgry ctgry = new Ctgry();
				ctgry.setCtgrymasterId(master.getCtgrymasterId());
				model.addAttribute("boardCateList", ctgryService.selectComtnbbsctgryList(ctgry));
				model.addAttribute("boardCateLevel", ctgryService.selectComtnbbsctgryLevel(ctgry));
			}

			model.addAttribute("resultList", resultList);
			model.addAttribute("resultCnt", totCnt);
			model.addAttribute("brdMstrVO", master);
			model.addAttribute("paginationInfo", paginationInfo);

			return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
		}

		model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
		return "forward:/uat/uia/egovLoginUsr.do";
	}

	/**
	 * 한반도 산림복원 자료실 게시물에 대한 상세 정보를 조회한다.
	 * @param nkrefoManageVO
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/bbs/selectNkrefoBoardArticle.do")
	public String selectNkrefoBoardArticle(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/egovLoginUsr.do";
		}

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);


		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			int SE_CODE = 1;
			model.addAttribute("brdMstrVO", master);

			if(loginVO != null) {
				SE_CODE = Integer.parseInt(loginVO.getUserSe());
			}

			if(SE_CODE >= 10){
				boardVO.setAdminAt("Y");
			}

			boardVO.setCtgrymasterId(master.getCtgrymasterId());
			boardVO.setBbsAttrbCode(master.getBbsAttrbCode());
			BoardVO dbVO = bbsMngService.selectBoardArticle(boardVO);
			model.addAttribute("board", dbVO);

			String loginID = loginVO.getId();
			UserManageVO userVO = userManageService.selectLoingNkrefoUser(loginID);

			model.addAttribute("USER_ID", userVO.getUserId());
			model.addAttribute("USER_SE_CODE", loginVO.getUserSeCode());

			return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
		}

		model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}


	/**
	 * 한반도 산림복원 자료실 게시물 등록을 위한 등록페이지로 이동한다.
	 * @param nkrefoManageVO
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/bbs/addNkrefoBoardArticle.do")
	public String addNkrefoBoardArticle(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
			if(user == null){
				return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
			}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
				model.addAttribute("brdMstrVO", master);
				Board board = new Board();
				model.addAttribute("board", board);
				request.getSession().setAttribute("sessionVO", boardVO);
				model.addAttribute("USER_INFO", user);
			return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";

		}

		model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}


	/**
	 * 한반도 산림복원 자료실 게시물을 등록한다.
	 * @param multiRequest
	 * @param boardVO
	 * @param board
	 * @param bindingResult
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cop/bbs/insertNkrefoBoardArticle.do")
	public String insertNkrefoBoardArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO, Board board, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws Exception{
		if(request.getSession().getAttribute("sessionVO") == null){
			return "forward:/cop/bbs/selectNkrefoBoardList.do";
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();

		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());
		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(bindingResult.hasErrors()){
			if(master != null){
				model.addAttribute("siteInfo", siteVO);
				model.addAttribute("brdMstrVO", master);
				model.addAttribute("USER_INFO", user);
				model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
				request.setAttribute("viewType", "bbsList");

				return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
			}
		}

		if(master != null){
			int SE_CODE = Integer.parseInt(user.getUserSe());
			if(SE_CODE >= Integer.parseInt(master.getRegistAuthor())){
				String atchFileId = "";

				List<FileVO> result = null;

				final Map<String, MultipartFile> files = multiRequest.getFileMap();
				if(!files.isEmpty()){
					result = fileUtil.parseBoardFileInf(request, files, 0, "", siteManageService.selectSiteServiceInfo(request).getSiteId(), boardVO.getBbsId());
					atchFileId = fileMngService.insertFileInfs(result);
				}

				boardVO.setAtchFileId(atchFileId);

				if(!"".equals(user.getId())){
					boardVO.setFrstRegisterId(user.getId());
				}
				boardVO.setNtcrNm(user.getName());
				boardVO.setCredtId(user.getCredtId());
				boardVO.setOldSecId(user.getOldSecId());
				boardVO.setNttCn(unscript(boardVO.getNttCn())); // XSS 방지
				boardVO.setCreatIp(EgovClntInfo.getClntIP(request));
				boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));
				bbsMngService.insertBoardArticle(boardVO, master);
			}
		}

		request.getSession().removeAttribute("sessionVO");

		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}

	/**
	 * 한반도 산림복원 자료실 게시물 수정을 위한 수정페이지로 이동한다.
	 * @param nkrefoManageVO
	 * @param boardVO
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/bbs/forUpdateNkrefoBoardArticle.do")
	public String selectNkrefoBoardArticleForUpdt(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, BoardVO boardVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user == null){
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());

		boardVO.setFrstRegisterId(user.getId());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			int SE_CODE = Integer.parseInt(user.getUserSe());
			if(SE_CODE >= 10){
				boardVO.setAdminAt("Y");
			}
			BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);

			model.addAttribute("brdMstrVO", master);
			model.addAttribute("board", dataVO);
			request.getSession().setAttribute("sessionVO", boardVO);
			model.addAttribute("USER_INFO", user);

			return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
		}

		model.addAttribute("message", egovMessageSource.getMessage("fail.auth.access"));
		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}

	/**
	 * 한반도 산림복원 자료실 게시물에 대한 내용을 수정한다.
	 * @param multiRequest
	 * @param boardVO
	 * @param board
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cop/bbs/updateNkrefoBoardArticle.do")
	public String updateNkrefoBoardArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO, Board board, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{

		if(request.getSession().getAttribute("sessionVO") == null){
			return "forward:/cop/bbs/selectBoardList.do";
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();

		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		String atchFileId = boardVO.getAtchFileId();

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());
		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(bindingResult.hasErrors()){

			if(master != null){
				model.addAttribute("siteInfo", siteVO);
				BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);
				model.addAttribute("brdMstrVO", master);
				model.addAttribute("board", dataVO);
				model.addAttribute("USER_INFO", user);
				model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
				request.setAttribute("viewType", "bbsList");

				return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
			}

		}

		if(master != null){
			int SE_CODE = Integer.parseInt(user.getUserSe());
			if(SE_CODE >= Integer.parseInt(master.getRegistAuthor())){

				final Map<String, MultipartFile> files = multiRequest.getFileMap();
				if(!files.isEmpty()){
					if(EgovStringUtil.isEmpty(atchFileId)){
						List<FileVO> result = fileUtil.parseBoardFileInf(request, files, 0, atchFileId, siteManageService.selectSiteServiceInfo(request).getSiteId(), boardVO.getBbsId());
						atchFileId = fileMngService.insertFileInfs(result);
						boardVO.setAtchFileId(atchFileId);
					}else{
						FileVO fvo = new FileVO();
						fvo.setAtchFileId(atchFileId);
						int cnt = fileMngService.getMaxFileSN(fvo);
						List<FileVO> _result = fileUtil.parseBoardFileInf(request, files, cnt, atchFileId, siteManageService.selectSiteServiceInfo(request).getSiteId(), boardVO.getBbsId());
						fileMngService.updateFileInfs(_result);
					}
				}

				if(SE_CODE >= 10){
					boardVO.setAdminAt("Y");
				}
				boardVO.setLastUpdusrId(user.getId());
				boardVO.setNttCn(unscript(boardVO.getNttCn())); // XSS 방지
				boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));

				bbsMngService.updateBoardArticle(boardVO, master, false);
			}
		}
		request.getSession().removeAttribute("sessionVO");

		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}

	/**
	 * 한반도 산림복원 자료실 게시물에 대한 내용을 삭제한다.
	 * @param boardVO
	 * @param board
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cop/bbs/deleteNkrefoBoardArticle.do")
	public String deleteNkrefoBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, BoardVO board, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{

		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		if(user == null){
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		BoardMasterVO vo = new BoardMasterVO();
		vo.setBbsId(boardVO.getBbsId());
		vo.setSiteId(siteVO.getSiteId());
		vo.setSysTyCode(siteVO.getSysTyCode());

		BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

		if(master != null){
			int SE_CODE = Integer.parseInt(user.getUserSe());
			if(SE_CODE >= Integer.parseInt(master.getRegistAuthor())){
				if(SE_CODE >= 10){
					boardVO.setAdminAt("Y");
				}

				if(!"".equals(user.getId())){
					board.setLastUpdusrId(user.getId());
				}
				if(!"".equals(user.getCredtId())){
					board.setCredtId(user.getCredtId());
				}
				if(!"".equals(user.getOldSecId())){
					board.setOldSecId(user.getOldSecId());
				}
				bbsMngService.deleteBoardArticle(board, master);
			}
		}

		return "forward:/cop/bbs/selectNkrefoBoardList.do";
	}


	/**
	 * 한반도 산림복원 자료실 댓글관리 목록 조회를 제공한다.
	 * @param commentVO
	 * @param model
	 * @param urlPrefix
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cop/bbs/selectNkrefoCommentList.do")
	public String selectNkrefoCommentList(@ModelAttribute("searchVO") CommentVO commentVO, ModelMap model, String urlPrefix, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			// 수정 처리된 후 댓글 등록 화면으로 처리되기 위한 구현
			if(commentVO.isModified()) {
				commentVO.setCommentNo(null);
				commentVO.setCommentCn("");
			}

			model.addAttribute("type", commentVO.getType()); // head or body

			if(commentVO.getType().equals("head")) {
				return "nkrefo/EgovCommentList";
			}
			// //----------------------------------------

			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
			if(isAuthenticated) {
				LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

				model.addAttribute("sessionUniqId", user.getId());
				model.addAttribute("sessionUniqNm", user.getName());

				commentVO.setWrterNm(user.getName());
			}

			commentVO.setSubPageUnit(propertyService.getInt("pageUnit"));
			commentVO.setSubPageSize(propertyService.getInt("pageSize"));

			PaginationInfo paginationInfo = new PaginationInfo();
			paginationInfo.setCurrentPageNo(commentVO.getSubPageIndex());
			paginationInfo.setRecordCountPerPage(commentVO.getSubPageUnit());
			paginationInfo.setPageSize(commentVO.getSubPageSize());

			commentVO.setSubFirstIndex(paginationInfo.getFirstRecordIndex());
			commentVO.setSubLastIndex(paginationInfo.getLastRecordIndex());
			commentVO.setSubRecordCountPerPage(paginationInfo.getRecordCountPerPage());

			Map<String, Object> map = bbsCommentService.selectCommentList(commentVO);
			int totCnt = Integer.parseInt((String)map.get("resultCnt"));

			paginationInfo.setTotalRecordCount(totCnt);

			model.addAttribute("resultList", map.get("resultList"));
			model.addAttribute("resultCnt", map.get("resultCnt"));
			model.addAttribute("paginationInfo", paginationInfo);

			commentVO.setCommentCn(""); // 등록 후 댓글 내용 처리
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return "nkrefo/EgovCommentList";
	}


	/**
	 * 한반도 산림복원 자료실 댓글을 등록한다.
	 *
	 * @param commentVO
	 * @param comment
	 * @param bindingResult
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cop/bbs/insertNkrefoComment.do")
	public String insertNkrefoComment(@ModelAttribute("searchVO") CommentVO commentVO, @ModelAttribute("comment") Comment comment, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(request.getSession().getAttribute("sessionCommentVO") != null && comment.getCommentCn().equals(request.getSession().getAttribute("sessionCommentVO"))) {
			return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		beanValidator.validate(comment, bindingResult);
		if(bindingResult.hasErrors()) {
			model.addAttribute("msg", "댓글 작성자 및  내용은 필수 입력값입니다.");

			return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
		}

		comment.setFrstRegisterId(user.getId());
		comment.setWrterNm(user.getName());
		comment.setCommentCn(unscript(comment.getCommentCn()));
		bbsCommentService.insertComment(comment);

		request.getSession().setAttribute("sessionCommentVO", comment.getCommentCn());

		commentVO.setCommentCn("");
		commentVO.setCommentNo(null);


		return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
	}


	/**
	 * 한반도 산림복원 자료실 댓글에 댓글을 등록한다
	 * @param commentVO
	 * @param comment
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cop/bbs/insertNkrefoReplyComment.do")
	public String insertNkrefoReplyComment(@ModelAttribute("searchVO") CommentVO commentVO, @ModelAttribute("comment") Comment comment, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(request.getSession().getAttribute("sessionCommentVO") != null && comment.getCommentCn().equals(request.getSession().getAttribute("sessionCommentVO"))) {
			return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		beanValidator.validate(comment, bindingResult);
		if(bindingResult.hasErrors()) {
			model.addAttribute("msg", "댓글 작성자 및  내용은 필수 입력값입니다.");

			return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
		}

		commentVO.setFrstRegisterId(user.getId());
		commentVO.setWrterNm(user.getName());
		commentVO.setCommentCn(unscript(commentVO.getCommentCn()));

		String ordr_code = bbsCommentService.selectCommentDupl(commentVO);

		if(ordr_code != null) {
			commentVO.setOrdrCode(ordr_code);
		}
		bbsCommentService.insertReplyDuplComment(commentVO);

		request.getSession().setAttribute("sessionCommentVO", comment.getCommentCn());

		commentVO.setCommentCn("");
		commentVO.setCommentNo(null);

		return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
	}


	/**
	 * 한반도 산림복원 자료실 댓글을 삭제한다.
	 *
	 * @param commentVO
	 * @param comment
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cop/bbs/deleteNkrefoComment.do")
	public String deleteNkrefoComment(@ModelAttribute("searchVO") CommentVO commentVO, @ModelAttribute("comment") Comment comment, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);

		if(isAuthenticated) {
			commentVO.setLastUpdusrId(user.getId());
			bbsCommentService.deleteComment(commentVO);
		}

		commentVO.setCommentCn("");
		commentVO.setCommentNo(null);

		return "forward:/cop/bbs/selectNkrefoBoardArticle.do";
	}
}
