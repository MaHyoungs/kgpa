package egovframework.com.nkrefo.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * ***************************************************
 *
 * @author : 서용식
 * @version : 1.0.0
 * @Class Name : EgovNkrefoLoginController.java
 * @Program name : egovframework.com.nkrefo.web
 * @Descriptopn : 일반 로그인, 비밀번호 변경 처리하는 컨트롤러 클래스
 * @created date : 2014. 12. 4.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2014. 12. 4.        서용식             first generated
 * *******************************************************
 */

@Controller
public class EgovNkrefoLoginController{

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "loginService")
	private EgovLoginService loginService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	/**
	 * 산림자료DB 전용 사용자의 로그인 화면으로 들어간다
	 * @param vo - 로그인후 이동할 URL이 담긴 LoginVO
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/nkrefoLoginUsr.do")
	public String loginUsrView(@ModelAttribute("loginVO") LoginVO loginVO,
	                           HttpServletRequest request,
	                           HttpServletResponse response,
	                           ModelMap model)
			throws Exception {

		if ( "Y".equals(EgovSessionCookieUtil.getSessionAttribute(request, "isMasterLogin")) )
			return "redirect:/index.do";

		if (loginVO.getUrl() != null) {
			EgovSessionCookieUtil.setSessionAttribute(request, "returnUrl", loginVO.getUrl());
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcLoginUsr";
	}

	/**
	 * 산림자료DB 전용 사용자의 일반 로그인을 처리한다
	 * @param vo - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/nkrefoActionLogin.do")
	public String actionLogin(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, ModelMap model)throws Exception {

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);

		String loginUrl = LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcLoginUsr";


		boolean loginSuccess = false;
		LoginVO resultVO = null;
		String userSeCode = "";
		String comfmAt = "";
		if (loginVO.getId() != null && loginVO.getPassword() != null){
			// 1. 일반 로그인 처리
			UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
			if(userVO != null) {
				comfmAt = userVO.getConfmAt();
				resultVO = loginService.actionNkrefoLogin(loginVO);
				if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {	//로그인 성공
					loginSuccess = true;
					userSeCode = resultVO.getUserSe();
				}
			}
		}

		if ("N".equals(comfmAt)) {
			model.addAttribute("frdbmessage", "관리자 승인 후 로그인 할 수 있습니다.");
			return loginUrl;
		}

		// 아이디, 패스워드 및 권한 체크를 한다.
		if ( loginSuccess ) {
			if ( ("06".equals(userSeCode) || "07".equals(userSeCode) || "99".equals(userSeCode)) ) {
				// 산림자료DB 전용 세션 저장
				EgovSessionCookieUtil.setSessionAttribute(request, "isMasterLogin", "Y");
				return "redirect:/j_spring_security_check?j_username=" + resultVO.getId() + "&j_password=" + resultVO.getPassword();
			} else {
				model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.auth.select"));
				return loginUrl;
			}
		} else {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.common.login"));
			return loginUrl;
		}
	}

	/**
	 * 산림자료DB 전용 사용자의 로그인 후 메인화면으로 들어간다
	 * @param
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/nkrefoActionMain.do")
	public String actionMain(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/nkrefoLoginUsr.do";
		}

		String returnUrl = (String)EgovSessionCookieUtil.getSessionAttribute(request, "returnUrl");
		String main_page = "/index.do";

		String modelUrl = main_page;
		if(returnUrl == null || "".equals(returnUrl) || "null".equalsIgnoreCase(returnUrl)){
			if (main_page.startsWith("/")) {
				modelUrl =  "redirect:" + main_page;
			}
		}else{
			modelUrl = "redirect:" + returnUrl;
		}

		EgovSessionCookieUtil.removeSessionAttribute(request, "returnUrl");

		return modelUrl;
	}

	/**
	 * 산림자료DB 전용 사용자를 로그아웃한다.
	 * @return String
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/nkrefoActionLogout.do")
	public String actionLogout(HttpSession session) throws Exception {
		session.invalidate();
		return "redirect:/index.do";
	}
}
