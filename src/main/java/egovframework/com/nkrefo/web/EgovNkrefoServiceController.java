package egovframework.com.nkrefo.web;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.mng.forest.service.EgovNkrefoManageService;
import egovframework.com.mng.forest.service.NkrefoManageVO;
import egovframework.com.mng.forest.service.NkrefoUtil;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.List;

/**
 * ***************************************************
 *
 * @author : 서용식
 * @version : 1.0.0
 * @Class Name : EgovNkrefoServiceController
 * @Program name : egovframework.com.nkrefo.web
 * @Descriptopn : 통합검색, 상세검색, 검색결과 리스트, 검색결과 조회 처리하는 컨트롤러 클래스
 * @created date : 2014. 12. 4.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2014. 12. 4.        서용식             first generated
 * *******************************************************
 */

@Controller
public class EgovNkrefoServiceController{

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	/**
	 * EgovPropertyService
	 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/**
	 * EgovNkrefoManageService
	 */
	@Resource(name = "nkrefoManageService")
	private EgovNkrefoManageService nkrefoManageService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	/**
	 * 산림자료DB 조회를 위한 사용자 메인 페이지로 이동한다.
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/nkrefo/index.do")
	public String index(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{




































		// 한반도산림복원자료실 도메인 얻기
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfoBySiteId("SITE_000000000000017");


		return "redirect:http://" + siteVO.getSiteUrl() + "/index.do";
	}

	@RequestMapping(value = "/nkrefoSourcHead.do")
	public String sourcHead(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);


		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcHead";
	}

	@RequestMapping(value = "/nkrefoSourcBottom.do")
	public String sourcBottom(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcBottom";
	}

	/**
	 * 산림자료DB에 대한 통합검색을 처리한다.
	 * @param nkrefoManageVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/nkrefoSch/totalSearch.do")
	public String totalSearch(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO,HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/egovLoginUsr.do";
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);


		PaginationInfo paginationInfo = new PaginationInfo();

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO != null && !EgovStringUtil.isEmpty(loginVO.getSiteId())) {
			nkrefoManageVO.setSiteId(loginVO.getSiteId());
		}

		String pageCondition = nkrefoManageVO.getPageCondition();
		// 페이징 정보 설정
		if (!EgovStringUtil.isEmpty(pageCondition)) {
			nkrefoManageVO.setPageUnit(Integer.parseInt(pageCondition));
			nkrefoManageVO.setPageSize(Integer.parseInt(pageCondition));
		} else {
			nkrefoManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
			nkrefoManageVO.setPageSize(propertiesService.getInt("pageSize"));
		}

		paginationInfo.setCurrentPageNo(nkrefoManageVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(nkrefoManageVO.getPageUnit());
		paginationInfo.setPageSize(nkrefoManageVO.getPageSize());

		nkrefoManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		nkrefoManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
		nkrefoManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		String searchKeyword = nkrefoManageVO.getSearchKeyword(); // 검색어
		String decodedKeyword = NkrefoUtil.descript(URLDecoder.decode(searchKeyword, "UTF-8").toUpperCase()); // 검색어 입력란에도 <,>,& 등을 입력할 수 있으므로 임시로 치환처리
		nkrefoManageVO.setDecodedKeyword(decodedKeyword);
		nkrefoManageVO.setSearchKeyword(decodedKeyword);

		//System.out.println("nkrefoManageVO.getSearchCondition() = " + nkrefoManageVO.getSearchCondition());
		//System.out.println("nkrefoManageVO.getSearchKeyword() = " + nkrefoManageVO.getSearchKeyword());
		//System.out.println("nkrefoManageVO.getsDate() = " + nkrefoManageVO.getsDate());
		//System.out.println("nkrefoManageVO.geteDate() = " + nkrefoManageVO.geteDate());
		//System.out.println("nkrefoManageVO.getDataForm() = " + nkrefoManageVO.getDataForm());
		//System.out.println("nkrefoManageVO.getBodyLang() = " + nkrefoManageVO.getBodyLang());
		//System.out.println("nkrefoManageVO.getFileCondition() = " + nkrefoManageVO.getFileCondition());
		//System.out.println("nkrefoManageVO.getSortCondition() = " + nkrefoManageVO.getSortCondition());
		//System.out.println("nkrefoManageVO.getPageCondition() = " + nkrefoManageVO.getPageCondition());
		//System.out.println("nkrefoManageVO.getPageIndex() = " + nkrefoManageVO.getPageIndex());
		//System.out.println("nkrefoManageVO.getPageSize() = " + nkrefoManageVO.getPageSize());
		//System.out.println("nkrefoManageVO.getPageUnit() = " + nkrefoManageVO.getPageUnit());

		List<NkrefoManageVO> resultList = (List<NkrefoManageVO>) nkrefoManageService.selectSearchNkrefoArticleList(nkrefoManageVO);
		int totCnt = nkrefoManageService.selectSearchNkrefoArticleListCnt(nkrefoManageVO);

		paginationInfo.setTotalRecordCount(totCnt);

		model.addAttribute("nkrefoManage", nkrefoManageVO);
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultCnt", totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");

		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);

		nkrefoManageVO.setSearchKeyword(searchKeyword); // searchKeyword 원복처리

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
	}

	/**
	 * 산림자료DB 자료 1건에 대한 조회를 처리한다.
	 * @param nkrefoManageVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/nkrefoSch/selectView.do")
	public String selectView(@ModelAttribute("searchVO") NkrefoManageVO nkrefoManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/egovLoginUsr.do";
		}

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null || loginVO.getId() == null) {
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.login"));
			return "forward:/uat/uia/egovLoginUsr.do";
		}

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		// 사이트 레이아웃 및 템플릿 설정
		String lytSourcId = siteVO.getLytSourcId();
		String lytTmplatId = siteVO.getLytTmplatId();
		String LytFileStoreWebPathByWebFile = propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile");
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		model.addAttribute("lytSourcId", lytSourcId);
		model.addAttribute("lytTmplatId", lytTmplatId);
		model.addAttribute("LytFileStoreWebPathByWebFile", LytFileStoreWebPathByWebFile);


		//System.out.println("nkrefoManageVO.getSearchCondition() = " + nkrefoManageVO.getSearchCondition());
		//System.out.println("nkrefoManageVO.getSearchKeyword() = " + nkrefoManageVO.getSearchKeyword());
		//System.out.println("nkrefoManageVO.getsDate() = " + nkrefoManageVO.getsDate());
		//System.out.println("nkrefoManageVO.geteDate() = " + nkrefoManageVO.geteDate());
		//System.out.println("nkrefoManageVO.getDataForm() = " + nkrefoManageVO.getDataForm());
		//System.out.println("nkrefoManageVO.getBodyLang() = " + nkrefoManageVO.getBodyLang());
		//System.out.println("nkrefoManageVO.getFileCondition() = " + nkrefoManageVO.getFileCondition());
		//System.out.println("nkrefoManageVO.getSortCondition() = " + nkrefoManageVO.getSortCondition());
		//System.out.println("nkrefoManageVO.getPageCondition() = " + nkrefoManageVO.getPageCondition());

		NkrefoManageVO result = nkrefoManageService.selectNkrefoArticle(nkrefoManageVO);

		String searchKeyword = nkrefoManageVO.getSearchKeyword(); // 검색어
		String decodedKeyword = NkrefoUtil.descript(URLDecoder.decode(searchKeyword, "UTF-8").toUpperCase()); // 검색어 입력란에도 <,>,& 등을 입력할 수 있으므로 임시로 치환처리
		result.setDecodedKeyword(decodedKeyword);

		model.addAttribute("nkrefoManageVO", result);

		nkrefoManageService.updateNkrefoArticleForHit(nkrefoManageVO); // 조회수 증가 처리

		// 자료형태 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> dataFormList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM401");
		// 본문언어 공통코드정보를 조회한다.
		List<ComDefaultCodeVO> bodyLangList = (List<ComDefaultCodeVO>) nkrefoManageService.selectNkrefoCodeList("COM402");


		model.addAttribute("dataFormList", dataFormList);
		model.addAttribute("bodyLangList", bodyLangList);

		// 사용자의 기존 정보를 조회한다.
		String loginID = loginVO.getId();
		UserManageVO userVO = userManageService.selectLoingNkrefoUser(loginID);

		model.addAttribute("USER_ID", userVO.getUserId());
		model.addAttribute("USER_SE_CODE", userVO.getUserSeCode());

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcMain";
	}

}
