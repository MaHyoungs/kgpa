package egovframework.com.nkrefo.web;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ***************************************************
 *
 * @author : 서용식
 * @version : 1.0.0
 * @Class Name : EgovNkrefoLoginController.java
 * @Program name : egovframework.com.nkrefo.web
 * @Descriptopn : 일반 로그인, 비밀번호 변경 처리하는 컨트롤러 클래스
 * @created date : 2014. 12. 4.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2014. 12. 4.        서용식             first generated
 * *******************************************************
 */

@Controller
public class EgovNkrefoUserInfoController{

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "loginService")
	private EgovLoginService loginService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	/**
	 * 산림자료DB 전용 사용자의 비밀번호 변경화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovUserPasswordUpdate
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/NkrefoUserPasswordUpdateView.do")
	public String userPasswordUpdateView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {

			return "forward:/uat/uia/egovLoginUsr.do";
		}

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null || loginVO.getId() == null) {

			return "forward:/uat/uia/egovLoginUsr.do";
		}

		String loginID = loginVO.getId();

		UserManageVO userVO = userManageService.selectLoingNkrefoUser(loginID);
		userManageVO.setUserId(userVO.getUserId());
		userManageVO.setUserNm(userVO.getUserNm());
		userManageVO.setOrganNm(userVO.getOrganNm());

		model.addAttribute("userManageVO", userManageVO);

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);

		String lytSourcId = siteVO.getLytSourcId();
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcUserPasswordUpdate";
	}

	/**
	 * 산림자료DB 전용 사용자의 비밀번호 변경 임시
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/NkrefoUserTempPasswordUpdate.do")
	public String userTempPasswordUpdate(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
		if (!isAuthenticated) {

			return "redirect:/uat/uia/egovLoginUsr.do";
		}

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null || loginVO.getId() == null) {

			return "redirect:/uat/uia/egovLoginUsr.do";
		}

		// 사용자의 기존 정보를 조회한다.
		String loginID = loginVO.getId();

		request.getSession().setAttribute("loginID", loginID);
		request.getSession().setAttribute("userManageVO", userManageVO);

		// 사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		request.getSession().setAttribute("siteVO", siteVO);

		return "redirect:/uss/umt/NkrefoUserPasswordUpdate.do";
	}

	/**
	 * 산림자료DB 전용 사용자의 비밀번호 변경 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/NkrefoUserPasswordUpdate.do")
	public String userPasswordUpdate(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		String loginID = (String)request.getSession().getAttribute("loginID");
		userManageVO = (UserManageVO)request.getSession().getAttribute("userManageVO");

		// 사용자의 기존 정보를 조회한다.
		userManageVO.setUserId(loginID);
		if (userManageService.selectCheckPassword(userManageVO) > 0) {

			userManageVO.setPassword(userManageVO.getPassword2()); // 변경할 비밀번호를 기존 비밀번호로 설정하고 DB 업데이트를 수행한다.
			if (userManageService.updatePassword(userManageVO) > 0) {
				model.addAttribute("result", true);
				model.addAttribute("frdbmessage", egovMessageSource.getMessage("success.common.process"));
			} else {
				model.addAttribute("result", false);
				model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.request.msg"));
			}

		} else { // 현재 비밀번호가 일치하지 않는 경우
			model.addAttribute("result", false);
			model.addAttribute("frdbmessage", egovMessageSource.getMessage("fail.user.passwordUpdate1"));
		}

		UserManageVO userVO = userManageService.selectLoingNkrefoUser(loginID);
		userManageVO.setUserNm(userVO.getUserNm());
		userManageVO.setOrganNm(userVO.getOrganNm());

		model.addAttribute("userManageVO", userManageVO);

		// 사이트설정정보

		SiteManageVO siteVO = (SiteManageVO)request.getSession().getAttribute("siteVO");
		model.addAttribute("siteInfo", siteVO);

		request.removeAttribute("loginID");
		request.removeAttribute("siteVO");

		String lytSourcId = siteVO.getLytSourcId();
		String LytFileStoreWebPathByJspFile = propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile");

		return LytFileStoreWebPathByJspFile + "sit/" + lytSourcId + "/sourcUserPasswordUpdate";
	}

}
