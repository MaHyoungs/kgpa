package egovframework.com.scheduler.gfund;


import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import egovframework.com.cmm.mail.SendMail;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserCamelCaseVo;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class GfundCmmScheduling {

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	/**
	 * 녹색자금통합관리시스템 보존기간 메일 발송 및 회원삭제 스케줄러
	 * 매일 자정 00:01 분 동작
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0/1 0 * * ?")
	public void gfundRetentionPerionUserSendMailScheduling() throws Exception {

		//보존기간 한달 남은 회원 목록
		List<UserCamelCaseVo> monthList = userManageService.selectGfundRetentionPeriodMonthUsers();
		//보존기간 3일 남은 회원 목록
		List<UserCamelCaseVo> threeDayList = userManageService.selectGfundRetentionPeriodThreeDayUsers();
		//보존기간 만료된 회원 목록
		List<UserCamelCaseVo> endList = userManageService.selectGfundRetentionPeriodEndUsers();
		//통합관리자 회원 목록
		List<UserCamelCaseVo> adminList = userManageService.selectSuperManagers();

		//보존기간 한달 남은 회원 메일 발송
		if (monthList != null) {
			gfundRetentionPerionMonthUserSendMail(monthList, adminList);
		}

		//보존기간 3일 남은 회원 메일 발송
		if (threeDayList != null) {
			gfundRetentionPerionThreeDayUserSendMail(threeDayList, adminList);
		}

		//보존기간 만료 회원 메일 발송 및 삭제 및 관리자한테 삭제 정보 발송
		if (endList != null) {
			gfundRetentionPerionEndUserSendMail(endList, adminList);
		}
	}

	/**
	 * 녹색자금통합관리시스템 보존기간 한달전 사용자 메일 발송
	 *
	 * @param monthList
	 */
	public void gfundRetentionPerionMonthUserSendMail(List<UserCamelCaseVo> monthList, List<UserCamelCaseVo> adminList) {
		List<String[]> userList = new ArrayList<String[]>();
		String from = EgovProperties.getProperty("Globals.AdminEmailAdress");
		String subject = "[녹색자금 통합관리시스템] 개인정보 보존기간 만료알림 서비스";
		try {
			for (UserCamelCaseVo userVo : monthList) {
				if (!EgovStringUtil.isEmpty(userVo.getEmail_adres())) {
					String to = userVo.getEmail_adres();
					StringBuffer sb = new StringBuffer();
					sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div><div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\">");
					sb.append("<div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림 1달 전</div>");
					sb.append("<div style=\"border:5px solid #e7e7e7;background:#fff;padding:5px;\"><ul style=\"margin:0;padding:20px;color:#727272;font-size:13px;\">");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">개인정보 보존기간 만료가 1달 전입니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">만료된 후에는 입력하신 개인정보가 잊혀질 권리에 의해서 삭제됩니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">ID가 삭제된 경우 가입 후 활동하신 내용은 마이페이지에서 확인하실 수 없습니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">단, 해당 내용은 녹색사업단에 귀속됩니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">개인정보 보존기간 연장은 회원정보에서 진행하실 수 있습니다</li></ul></div></div></div>");
					SendMail.sendEmail(from, to, subject, sb.toString());
					System.out.println("보존기간 한달 남은 " + userVo.getUser_id() +" 목록 메일 발송 완료");

					String user[] = {userVo.getUser_id(), userVo.getIndvdlinfo_prsrv_pd()};
					userList.add(user);
				}
			}

			//관리자에게 발송
			if(userList.size() > 0){
				for (UserCamelCaseVo adminVo : adminList) {
					if (!EgovStringUtil.isEmpty(adminVo.getEmail_adres())) {
						StringBuffer sb = new StringBuffer();
						sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div>");
						sb.append("<div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\"><div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림 1달 전</div>");
						sb.append("<div style=\"padding:0 0 10px 20px;color:#3b3b3b;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon.gif') 0 3px no-repeat;font-size:16px;\">개인정보 보존기간 만료가 1달 전 계정 목록입니다.</div>");
						sb.append("<table   width=\"100%\" cellspacing=\"0\" cellpadding=\"10\" border=\"0\">");
						sb.append("<tr><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">아이디</th><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">만료일자</th></tr>");
						for (String[] userArr : userList) {
							sb.append("<tr><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + userArr[0] + "</td><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + userArr[1] + "</td></tr>");
						}
						sb.append("</table></div></div>");
						SendMail.sendEmail(from, adminVo.getEmail_adres(), subject, sb.toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 녹색자금통합관리시스템 보존기간 3일전 사용자 메일 발송
	 *
	 * @param threeDayList
	 */
	public void gfundRetentionPerionThreeDayUserSendMail(List<UserCamelCaseVo> threeDayList, List<UserCamelCaseVo> adminList) {
		List<String[]> userList = new ArrayList<String[]>();
		String from = EgovProperties.getProperty("Globals.AdminEmailAdress");
		String subject = "[녹색자금 통합관리시스템] 개인정보 보존기간 만료알림 서비스";
		try {
			for (UserCamelCaseVo userVo : threeDayList) {
				if (!EgovStringUtil.isEmpty(userVo.getEmail_adres())) {
					String to = userVo.getEmail_adres();
					StringBuffer sb = new StringBuffer();
					sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div><div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\">");
					sb.append("<div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림 3일 전</div>");
					sb.append("<div style=\"border:5px solid #e7e7e7;background:#fff;padding:5px;\"><ul style=\"margin:0;padding:20px;color:#727272;font-size:13px;\">");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">개인정보 보존기간 만료가 3일 전입니다</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">만료된 후에는 입력하신 개인정보가 잊혀질 권리에 의해서 삭제됩니다</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">ID가 삭제된 경우 가입 후 활동하신 내용은 마이페이지에서 확인하실 수 없습니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">단, 해당 내용은 녹색사업단에 귀속됩니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">개인정보 보존기간 연장은 회원정보에서 진행하실 수 있습니다</li></ul></div></div></div>");
					SendMail.sendEmail(from, to, subject, sb.toString());
					System.out.println("보존기간 3일 남은 " + userVo.getUser_id() +" 목록 메일 발송 완료");

					String user[] = {userVo.getUser_id(), userVo.getIndvdlinfo_prsrv_pd()};
					userList.add(user);
				}
			}

			//관리자에게 발송
			if(userList.size() > 0){
				for (UserCamelCaseVo adminVo : adminList) {
					if (!EgovStringUtil.isEmpty(adminVo.getEmail_adres())) {
						StringBuffer sb = new StringBuffer();
						sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div>");
						sb.append("<div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\"><div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림 3일 전</div>");
						sb.append("<div style=\"padding:0 0 10px 20px;color:#3b3b3b;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon.gif') 0 3px no-repeat;font-size:16px;\">개인정보 보존기간 만료가 3일 전 계정 목록입니다.</div>");
						sb.append("<table   width=\"100%\" cellspacing=\"0\" cellpadding=\"10\" border=\"0\">");
						sb.append("<tr><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">아이디</th><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">만료일자</th></tr>");
						for (String[] userArr : userList) {
							sb.append("<tr><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + userArr[0] + "</td><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + userArr[1] + "</td></tr>");
						}
						sb.append("</table></div></div>");
						SendMail.sendEmail(from, adminVo.getEmail_adres(), subject, sb.toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 녹색자금통합관리시스템 보존기간 만료 사용자 메일 발송 및 관리자 삭제 알림 메일 발송
	 *
	 * @param endList
	 * @throws Exception
	 */
	public void gfundRetentionPerionEndUserSendMail(List<UserCamelCaseVo> endList, List<UserCamelCaseVo> adminList) throws Exception {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.");
		List<String> userList = new ArrayList<String>();
		String from = EgovProperties.getProperty("Globals.AdminEmailAdress");
		String subject = "[녹색자금 통합관리시스템] 개인정보 보존기간 만료알림 서비스";
		try {

			//사용자에게 발송
			for (UserCamelCaseVo userVo : endList) {
				if (!EgovStringUtil.isEmpty(userVo.getEmail_adres())) {
					String to = userVo.getEmail_adres();
					StringBuffer sb = new StringBuffer();
					sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div><div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\">");
					sb.append("<div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림</div>");
					sb.append("<div style=\"border:5px solid #e7e7e7;background:#fff;padding:5px;\"><ul style=\"margin:0;padding:20px;color:#727272;font-size:13px;\">");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">개인정보 보존기간이 만료되어 고객님의 개인정보가 삭제되었습니다.</li>");
					sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">지금까지 녹색사업단 서비스를 이용해주신 고객님께 진심으로 감사드립니다.</li></ul></div></div></div>");
					SendMail.sendEmail(from, to, subject, sb.toString());
					userList.add(userVo.getUser_id());
					System.out.println("보존기간 만료로 삭제된 ID:" + userVo.getUser_id() + " 메일 발송 완료");
				}

				//메일 발송이 되지 않아도 삭제 해버림
				UserManageVO dUserVo = new UserManageVO();
				dUserVo.setUserId(userVo.getUser_id());
				dUserVo.setLastUpdusrId(EgovProperties.getProperty("Globals.AdminId"));
				dUserVo.setDeleteResn("녹색자금 통합관리시스템 개인정보 보존기간 만료로 인한 삭제");
				userManageService.deleteUser(dUserVo);
			}

			//관리자에게 발송
			if(userList.size() > 0){
				for (UserCamelCaseVo adminVo : adminList) {
					if (!EgovStringUtil.isEmpty(adminVo.getEmail_adres())) {
						StringBuffer sb = new StringBuffer();
						sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div>");
						sb.append("<div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\"><div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">개인정보 보존기간 만료알림</div>");
						sb.append("<div style=\"padding:0 0 10px 20px;color:#3b3b3b;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon.gif') 0 3px no-repeat;font-size:16px;\">개인정보 보존기간이 만료되어 개인정보가 삭제된 계정 목록입니다.</div>");
						sb.append("<table   width=\"100%\" cellspacing=\"0\" cellpadding=\"10\" border=\"0\">");
						sb.append("<tr><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">아이디</th><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;\">만료일자</th><th style=\"color:#fff;font-weight:normal;border:1px solid #ebebeb;background:#ababab;padding:10px 0;;\">계정상태</th></tr>");
						for (String userId : userList) {
							sb.append("<tr><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + userId + "</td><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">" + dateFormat.format(calendar.getTime()) + "</td><td style=\"border:1px solid #ebebeb;text-align:center;padding:10px 0;\">삭제</td></tr>");
						}
						sb.append("</table></div></div>");
						SendMail.sendEmail(from, adminVo.getEmail_adres(), subject, sb.toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
