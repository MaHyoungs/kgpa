package egovframework.com.sec.ram.security.common;

import egovframework.com.sec.ram.security.userdetails.EgovUserDetails;
import egovframework.com.sec.ram.security.userdetails.jdbc.EgovUsersByUsernameMapping;
import egovframework.com.uat.uia.service.LoginVO;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * mapRow 결과를 사용자 EgovUserDetails Object 에 정의한다.
 *
 * @author ByungHun Woo
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2009.03.10  ByungHun Woo    최초 생성
 *   2009.03.20  이문준          UPDATE
 *
 * </pre>
 */

public class EgovSessionMapping extends EgovUsersByUsernameMapping {

	/**
	 * 사용자정보를 테이블에서 조회하여 EgovUsersByUsernameMapping 에 매핑한다.
	 * @param ds DataSource
	 * @param usersByUsernameQuery String
	 */
	public EgovSessionMapping(DataSource ds, String usersByUsernameQuery) {
        super(ds, usersByUsernameQuery);
    }

	/**
	 * mapRow Override
	 * @param rs ResultSet 결과
	 * @param rownum row num
	 * @return Object EgovUserDetails
	 * @exception SQLException
	 */
	@Override
    protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
    	logger.debug("## EgovUsersByUsernameMapping mapRow ##");

        String strUserId            = rs.getString("USER_ID");
        String strSiteId 	        = rs.getString("SITE_ID");
        String strUserSe            = rs.getString("USER_SE_CODE");
        String credtId              = rs.getString("CREDT_ID");
        String strPassWord          = rs.getString("PASSWORD");
        String strUserNm            = rs.getString("USER_NM");
        String strUserEmail         = rs.getString("EMAIL_ADRES");
        String strUserTel           = rs.getString("TLPHON_NO");
        String strUserPhone         = rs.getString("MOBLPHON_NO");
        String strUserFax           = rs.getString("FAXPHON_NO");
        String strUserZip           = rs.getString("ZIP");
        String strUserAdres         = rs.getString("ADRES");
        String strUserAdresDt       = rs.getString("ADRES_DETAIL");
        String strUserRAdres        = rs.getString("R_ADRES");
        String strUserRAdresDt      = rs.getString("R_ADRES_DETAIL");
        String strUserBrthdy        = rs.getString("BRTHDY");
        String strUserSexdstn       = rs.getString("SEXDSTN");
        String strUserOrgan         = rs.getString("ORGAN_NM");
        String strUserEmailAt       = rs.getString("EMAIL_RECPTN_AT");
        String strUserMblAt         = rs.getString("MOBLPHON_RECPTN_AT");
		String indvdlinfoPrsrvPd 	= rs.getString("INDVDLINFO_PRSRV_PD");
		String strSiteNm 	        = rs.getString("SITE_NM");

        boolean strEnabled  = rs.getBoolean("ENABLED");

        // 세션 항목 설정
        LoginVO loginVO = new LoginVO();
        loginVO.setId(strUserId);
        loginVO.setPassword(strPassWord);
        loginVO.setUserSeCode(strUserSe);
        loginVO.setUserSe(strUserSe);
        loginVO.setName(strUserNm);
        loginVO.setEmail(strUserEmail);
        loginVO.setMobileNo(strUserPhone);
        loginVO.setSiteId(strSiteId);
        loginVO.setSiteNm(strSiteNm);
        loginVO.setCredtId(credtId);
        loginVO.setStrUserTel(strUserTel);
        loginVO.setStrUserFax(strUserFax);
        loginVO.setStrUserZip(strUserZip);
        loginVO.setStrUserAdres(strUserAdres);
        loginVO.setStrUserAdresDt(strUserAdresDt);
        loginVO.setStrUserRAdres(strUserRAdres);
        loginVO.setStrUserRAdres(strUserRAdres);
        loginVO.setStrUserRAdresDt(strUserRAdresDt);
        loginVO.setStrUserBrthdy(strUserBrthdy);
        loginVO.setStrUserSexdstn(strUserSexdstn);
        loginVO.setStrUserOrgan(strUserOrgan);
        loginVO.setStrUserEmailAt(strUserEmailAt);
        loginVO.setStrUserMblAt(strUserMblAt);
        loginVO.setIndvdlinfoPrsrvPd(indvdlinfoPrsrvPd);

        return new EgovUserDetails(strUserId, strPassWord, strEnabled, loginVO);
    }
}

	