package egovframework.com.sym.cal.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import egovframework.com.diet.service.Diet;
import egovframework.com.sym.cal.service.EgovCalRestdeManageService;
import egovframework.com.sym.cal.service.Restde;
import egovframework.com.sym.cal.service.RestdeVO;



/**
 * 
 * 휴일에 대한 서비스 구현클래스를 정의한다
 * @author 공통서비스 개발팀 이중호
 * @since 2009.04.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.04.01  이중호          최초 생성
 *
 * </pre>
 */
@Service("RestdeManageService")
public class EgovCalRestdeManageServiceImpl extends AbstractServiceImpl implements EgovCalRestdeManageService {

    /**
	 * @uml.property  name="restdeManageDAO"
	 * @uml.associationEnd  readOnly="true"
	 */
    @Resource(name="RestdeManageDAO")
    private RestdeManageDAO restdeManageDAO;
    
    /** ID Generation */
    @Resource(name="egovDietIdGnrService")    
    private EgovIdGnrService egovIdGnrService;
    
	/**
	 * 일반달력 팝업 정보를 조회한다.
	 */
    public List selectNormalRestdePopup(Diet diet) throws Exception {
		return restdeManageDAO.selectNormalRestdePopup(diet);
	}
    
	/**
	 * 행정달력 팝업 정보를 조회한다.
	 */
    public List selectAdministRestdePopup(Diet diet) throws Exception {
		return restdeManageDAO.selectAdministRestdePopup(diet);
	}

	/**
	 * 일반달력 일간 정보를 조회한다.
	 */
    public List selectNormalDayCal(Restde restde) throws Exception {
		return restdeManageDAO.selectNormalDayCal(restde);
	}

	/**
	 * 일반달력 일간 휴일을 조회한다.
	 */
    public List selectNormalDayRestde(Restde restde) throws Exception {
		return restdeManageDAO.selectNormalDayRestde(restde);
	}
    
	/**
	 * 일반달력 월간 휴일을 조회한다.
	 */
    public List selectNormalMonthRestde(Diet diet) throws Exception {
		return restdeManageDAO.selectNormalMonthRestde(diet);
	}

	/**
	 * 행정달력 일간 정보를 조회한다.
	 */
    public List selectAdministDayCal(Restde restde) throws Exception {
		return restdeManageDAO.selectAdministDayCal(restde);
	}

	/**
	 * 행정달력 일간 휴일을 조회한다.
	 */
    public List selectAdministDayRestde(Restde restde) throws Exception {
		return restdeManageDAO.selectAdministDayRestde(restde);
	}
    
    /**
	 * 행정달력 월간 휴일을 조회한다.
	 */
    public List selectAdministMonthRestde(Diet diet) throws Exception {
		return restdeManageDAO.selectAdministMonthRestde(diet);
	}
    
    /**
	 * 휴일을 삭제한다.
	 */
	public void deleteRestde(Diet diet) throws Exception {
		restdeManageDAO.deleteRestde(diet);
	}

	/**
	 * 휴일을 등록한다.
	 */
	public void insertRestde(Diet diet) throws Exception {
		
		log.debug(diet.toString());
    	
    	/** ID Generation Service */
    	//TODO 해당 테이블 속성에 따라 ID 제너레이션 서비스 사용
    	String id = egovIdGnrService.getNextStringId();
    	diet.setDietId(id);
    	log.debug(diet.toString());
    	
    	restdeManageDAO.insertRestde(diet);    	
	}

	/**
	 * 휴일 상세항목을 조회한다.
	 */
	public Diet selectRestdeDetail(Diet diet) throws Exception {
		Diet ret = restdeManageDAO.selectRestdeDetail(diet);
    	return ret;
	}

	/**
	 * 휴일 목록을 조회한다.
	 */
	public List selectRestdeList(RestdeVO searchVO) throws Exception {
        return restdeManageDAO.selectRestdeList(searchVO);
	}

	/**
	 * 휴일 총 갯수를 조회한다.
	 */
	public int selectRestdeListTotCnt(RestdeVO searchVO) throws Exception {
        return restdeManageDAO.selectRestdeListTotCnt(searchVO);
	}

	/**
	 * 휴일을 수정한다.
	 */
	public void updateRestde(Diet diet) throws Exception {
		restdeManageDAO.updateRestde(diet);
	}

}
