package egovframework.com.sym.cal.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

import egovframework.com.diet.service.Diet;
import egovframework.com.sym.cal.service.Restde;
import egovframework.com.sym.cal.service.RestdeVO;

/**
 * 
 * 휴일에 대한 데이터 접근 클래스를 정의한다
 * @author 공통서비스 개발팀 이중호
 * @since 2009.04.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.04.01  이중호          최초 생성
 *
 * </pre>
 */
@Repository("RestdeManageDAO")
public class RestdeManageDAO extends EgovAbstractDAO {

	/**
	 * 일반달력 팝업 정보를 조회한다.
	 * @param restde
	 * @return List(일반달력 팝업 날짜정보)
	 * @throws Exception
	 */
    public List selectNormalRestdePopup(Diet diet) throws Exception {
        return list("RestdeManageDAO.selectNormalRestdePopup", diet);
	}

	/**
	 * 행정달력 팝업 정보를 조회한다.
	 * @param restde
	 * @return List(행정달력 팝업 날짜정보)
	 * @throws Exception
	 */
    public List selectAdministRestdePopup(Diet diet) throws Exception {
        return list("RestdeManageDAO.selectAdministRestdePopup", diet);
	}

	/**
	 * 일반달력 일간 정보를 조회한다.
	 * @param restde
	 * @return List(일반달력 일간 날짜정보)
	 * @throws Exception
	 */
    public List selectNormalDayCal(Restde restde) throws Exception {
        return list("RestdeManageDAO.selectNormalDayCal", restde);
	}

	/**
	 * 일반달력 일간 휴일을 조회한다.
	 * @param restde
	 * @return List(일반달력 일간 휴일정보)
	 * @throws Exception
	 */
    public List selectNormalDayRestde(Restde restde) throws Exception {
        return list("RestdeManageDAO.selectNormalDayRestde", restde);
	}

	/**
	 * 일반달력 월간 휴일을 조회한다.
	 * @param restde
	 * @return List(일반달력 월간 휴일정보)
	 * @throws Exception
	 */
    public List selectNormalMonthRestde(Diet diet) throws Exception {
        return list("RestdeManageDAO.selectNormalMonthRestde", diet);
	}

	/**
	 * 행정달력 일간 정보를 조회한다.
	 * @param restde
	 * @return List(행정달력 일간 날짜정보)
	 * @throws Exception
	 */
    public List selectAdministDayCal(Restde restde) throws Exception {
        return list("RestdeManageDAO.selectAdministDayCal", restde);
	}

	/**
	 * 행정달력 일간 휴일을 조회한다.
	 * @param restde
	 * @return List(행정달력 일간 휴일정보)
	 * @throws Exception
	 */
    public List selectAdministDayRestde(Restde restde) throws Exception {
        return list("RestdeManageDAO.selectAdministDayRestde", restde);
	}
    
	/**
	 * 행정달력 월간 휴일을 조회한다.
	 * @param restde
	 * @return List(행정달력 월간 휴일정보)
	 * @throws Exception
	 */
    public List selectAdministMonthRestde(Diet diet) throws Exception {
        return list("RestdeManageDAO.selectAdministMonthRestde", diet);
	}
    
	/**
	 * 휴일을 삭제한다.
	 * @param restde
	 * @throws Exception
	 */
	public void deleteRestde(Diet diet) throws Exception {
		delete("RestdeManageDAO.deleteRestde", diet);
	}


	/**
	 * 휴일을 등록한다.
	 * @param restde
	 * @throws Exception
	 */
	public void insertRestde(Diet diet) throws Exception {
        insert("RestdeManageDAO.insertRestde", diet);
	}

	/**
	 * 휴일 상세항목을 조회한다.
	 * @param restde
	 * @return Restde(휴일)
	 * @throws Exception
	 */
	public Diet selectRestdeDetail(Diet diet) throws Exception {
		return (Diet) selectByPk("RestdeManageDAO.selectRestdeDetail", diet);
	}


    /**
     * 휴일 목록을 조회한다.
     * @param searchVO
	 * @return List(휴일 목록)
     * @throws Exception
     */
    public List selectRestdeList(RestdeVO searchVO) throws Exception {
        return list("RestdeManageDAO.selectRestdeList", searchVO);
    }

    /**
     * 글 총 갯수를 조회한다.
     * @param searchVO
     * @return int(휴일 총 갯수)
     * @throws Exception
     */
    public int selectRestdeListTotCnt(RestdeVO searchVO) throws Exception {
        return (Integer)getSqlMapClientTemplate().queryForObject("RestdeManageDAO.selectRestdeListTotCnt", searchVO);
    }

	/**
	 * 휴일을 수정한다.
	 * @param restde
	 * @throws Exception
	 */
	public void updateRestde(Diet diet) throws Exception {
		update("RestdeManageDAO.updateRestde", diet);
	}
	
}
