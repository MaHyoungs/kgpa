package egovframework.com.sym.log.service;

import egovframework.com.cmm.ComDefaultVO;

public class AdminLogVo extends ComDefaultVO {

	private String rn;
	private String log_id;
	private String creat_dt;
	private String login_mthd;
	private String login_ip;
	private String login_id;
	private String user_nm;
	private String author_nm;
	private String fromDate;
	private String toDate;
	private String isPaging;

	public String getRn() {
		return rn;
	}

	public void setRn(String rn) {
		this.rn = rn;
	}

	public String getLog_id() {
		return log_id;
	}

	public void setLog_id(String log_id) {
		this.log_id = log_id;
	}

	public String getCreat_dt() {
		return creat_dt;
	}

	public void setCreat_dt(String creat_dt) {
		this.creat_dt = creat_dt;
	}

	public String getLogin_mthd() {
		return login_mthd;
	}

	public void setLogin_mthd(String login_mthd) {
		this.login_mthd = login_mthd;
	}

	public String getLogin_ip() {
		return login_ip;
	}

	public void setLogin_ip(String login_ip) {
		this.login_ip = login_ip;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getUser_nm() {
		return user_nm;
	}

	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}

	public String getAuthor_nm() {
		return author_nm;
	}

	public void setAuthor_nm(String author_nm) {
		this.author_nm = author_nm;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIsPaging(){
		return isPaging;
	}

	public void setIsPaging(String isPaging){
		this.isPaging = isPaging;
	}
}
