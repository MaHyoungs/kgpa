package egovframework.com.sym.log.service;

import java.sql.SQLException;
import java.util.List;

public interface MngWorkLogService {

	/**
	 * 관리자 작업로그 등록
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int insertMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException;

	/**
	 * 관리자 작업로그 삭제
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException;

	/**
	 * 관리자 작업로그 in 삭제
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int inDeleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException;

	/**
	 * 관리자 작업로그 1차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup1() throws SQLException;

	/**
	 * 관리자 작업로그 2차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup2() throws SQLException;

	/**
	 * 관리자 작업로그 3차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup3() throws SQLException;

	/**
	 * 관리자 작업로그 4차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup4() throws SQLException;

	/**
	 * 관리자 작업로그 5차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup5() throws SQLException;

	/**
	 * 관리자 작업로그 목록 조회
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkLogs(MngWorkLogVo mngWorkLogVo) throws SQLException;

	/**
	 * 관리자 작업로그 목록 카운트 조회
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int selectMngWorkLogsCount(MngWorkLogVo mngWorkLogVo) throws SQLException;

}
