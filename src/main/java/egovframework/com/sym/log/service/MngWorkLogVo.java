package egovframework.com.sym.log.service;

import egovframework.com.cmm.ComDefaultVO;

public class MngWorkLogVo extends ComDefaultVO {

	private String mngrwork_id;
	private String mngrwork_ids;
	private String site_id;
	private String sys_ty_code;
	private String menu_1st_nm;
	private String menu_2nd_nm;
	private String menu_3rd_nm;
	private String menu_4th_nm;
	private String menu_5th_nm;
	private String id_menu_nm;
	private String url;
	private String work_ty;
	private String work_ty_nm;
	private String opertor_ip;
	private String opertor_id;
	private String occrrnc_pnttm;
	private String fromDate;
	private String toDate;
	private String user_nm;
	private String rnum;
	private String isPaging;

	public String getMngrwork_id() {
		return mngrwork_id;
	}

	public void setMngrwork_id(String mngrwork_id) {
		this.mngrwork_id = mngrwork_id;
	}

	public String getMngrwork_ids() {
		return mngrwork_ids;
	}

	public void setMngrwork_ids(String mngrwork_ids) {
		this.mngrwork_ids = mngrwork_ids;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getSys_ty_code() {
		return sys_ty_code;
	}

	public void setSys_ty_code(String sys_ty_code) {
		this.sys_ty_code = sys_ty_code;
	}

	public String getMenu_1st_nm() {
		return menu_1st_nm;
	}

	public void setMenu_1st_nm(String menu_1st_nm) {
		this.menu_1st_nm = menu_1st_nm;
	}

	public String getMenu_2nd_nm() {
		return menu_2nd_nm;
	}

	public void setMenu_2nd_nm(String menu_2nd_nm) {
		this.menu_2nd_nm = menu_2nd_nm;
	}

	public String getMenu_3rd_nm() {
		return menu_3rd_nm;
	}

	public void setMenu_3rd_nm(String menu_3rd_nm) {
		this.menu_3rd_nm = menu_3rd_nm;
	}

	public String getMenu_4th_nm() {
		return menu_4th_nm;
	}

	public void setMenu_4th_nm(String menu_4th_nm) {
		this.menu_4th_nm = menu_4th_nm;
	}

	public String getMenu_5th_nm() {
		return menu_5th_nm;
	}

	public void setMenu_5th_nm(String menu_5th_nm) {
		this.menu_5th_nm = menu_5th_nm;
	}

	public String getId_menu_nm() {
		return id_menu_nm;
	}

	public void setId_menu_nm(String id_menu_nm) {
		this.id_menu_nm = id_menu_nm;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWork_ty() {
		return work_ty;
	}

	public void setWork_ty(String work_ty) {
		this.work_ty = work_ty;
	}

	public String getWork_ty_nm() {
		return work_ty_nm;
	}

	public void setWork_ty_nm(String work_ty_nm) {
		this.work_ty_nm = work_ty_nm;
	}

	public String getOpertor_ip() {
		return opertor_ip;
	}

	public void setOpertor_ip(String opertor_ip) {
		this.opertor_ip = opertor_ip;
	}

	public String getOpertor_id() {
		return opertor_id;
	}

	public void setOpertor_id(String opertor_id) {
		this.opertor_id = opertor_id;
	}

	public String getOccrrnc_pnttm() {
		return occrrnc_pnttm;
	}

	public void setOccrrnc_pnttm(String occrrnc_pnttm) {
		this.occrrnc_pnttm = occrrnc_pnttm;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getUser_nm() {
		return user_nm;
	}

	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}

	public String getRnum() {
		return rnum;
	}

	public void setRnum(String rnum) {
		this.rnum = rnum;
	}

	public String getIsPaging() {
		return isPaging;
	}

	public void setIsPaging(String isPaging) {
		this.isPaging = isPaging;
	}
}
