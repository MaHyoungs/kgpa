package egovframework.com.sym.log.service.impl;


import egovframework.com.sym.log.service.MngWorkLogVo;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository("mngWorkLogDao")
public class MngWorkLogDao extends EgovAbstractDAO {


	/**
	 * 관리자 작업로그 등록
	 * @param mngWorkLogVo
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int insertMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException{
		return update("MngWorkLogDao.insertMngWorkLog", mngWorkLogVo);
	}

	/**
	 * 관리자 작업로그 삭제
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int deleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException{
		return delete("MngWorkLogDao.deleteMngWorkLog", mngWorkLogVo);
	}

	/**
	 * 관리자 작업로그 IN 삭제
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int inDeleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException{
		return delete("MngWorkLogDao.inDeleteMngWorkLog", mngWorkLogVo);
	}

	/**
	 * 관리자 작업로그 1차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup1() throws SQLException{
		return list("MngWorkLogDao.selectMngWorkGroup1", null);
	}

	/**
	 * 관리자 작업로그 2차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup2() throws SQLException{
		return list("MngWorkLogDao.selectMngWorkGroup2", null);
	}

	/**
	 * 관리자 작업로그 3차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup3() throws SQLException{
		return list("MngWorkLogDao.selectMngWorkGroup3", null);
	}

	/**
	 * 관리자 작업로그 4차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup4() throws SQLException{
		return list("MngWorkLogDao.selectMngWorkGroup4", null);
	}

	/**
	 * 관리자 작업로그 5차 메뉴 그룹 목록 조회
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkGroup5() throws SQLException{
		return list("MngWorkLogDao.selectMngWorkGroup5", null);
	}

	/**
	 * 관리자 작업로그 목록 조회
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public List<MngWorkLogVo> selectMngWorkLogs(MngWorkLogVo mngWorkLogVo) throws SQLException{
		return list("MngWorkLogDao.selectMngWorkLogs", mngWorkLogVo);
	}

	/**
	 * 관리자 작업로그 목록 카운트 조회
	 * @param mngWorkLogVo
	 * @return
	 * @throws SQLException
	 */
	public int selectMngWorkLogsCount(MngWorkLogVo mngWorkLogVo) throws SQLException{
		return (Integer)selectByPk("MngWorkLogDao.selectMngWorkLogsCount", mngWorkLogVo);
	}
}
