package egovframework.com.sym.log.service.impl;


import egovframework.com.sym.log.service.MngWorkLogService;
import egovframework.com.sym.log.service.MngWorkLogVo;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

@Service("mngWorkLogService")
public class MngWorkLogImpl extends AbstractServiceImpl implements MngWorkLogService {

	@Resource(name = "mngWorkLogDao")
	private MngWorkLogDao mngWorkLogDao;

	public int insertMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException {
		return mngWorkLogDao.insertMngWorkLog(mngWorkLogVo);
	}

	public int deleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException {
		return mngWorkLogDao.deleteMngWorkLog(mngWorkLogVo);
	}

	public int inDeleteMngWorkLog(MngWorkLogVo mngWorkLogVo) throws SQLException {
		return mngWorkLogDao.inDeleteMngWorkLog(mngWorkLogVo);
	}

	public List<MngWorkLogVo> selectMngWorkGroup1() throws SQLException {
		return mngWorkLogDao.selectMngWorkGroup1();
	}

	public List<MngWorkLogVo> selectMngWorkGroup2() throws SQLException {
		return mngWorkLogDao.selectMngWorkGroup2();
	}

	public List<MngWorkLogVo> selectMngWorkGroup3() throws SQLException {
		return mngWorkLogDao.selectMngWorkGroup3();
	}

	public List<MngWorkLogVo> selectMngWorkGroup4() throws SQLException {
		return mngWorkLogDao.selectMngWorkGroup4();
	}

	public List<MngWorkLogVo> selectMngWorkGroup5() throws SQLException {
		return mngWorkLogDao.selectMngWorkGroup5();
	}

	public List<MngWorkLogVo> selectMngWorkLogs(MngWorkLogVo mngWorkLogVo) throws SQLException {
		return mngWorkLogDao.selectMngWorkLogs(mngWorkLogVo);
	}

	public int selectMngWorkLogsCount(MngWorkLogVo mngWorkLogVo) throws SQLException {
		return mngWorkLogDao.selectMngWorkLogsCount(mngWorkLogVo);
	}
}
