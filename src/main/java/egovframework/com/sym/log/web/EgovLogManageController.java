package egovframework.com.sym.log.web;

import egovframework.com.sym.log.service.AdminLogVo;
import egovframework.com.sym.log.service.EgovLogManageService;
import egovframework.com.sym.log.service.LoginLog;
import egovframework.com.sym.log.service.WebLog;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 공통 서비스 개발팀 이삼섭
 * @Class Name : EgovLogManageController.java
 * @Description : 시스템 로그정보를 관리하기 위한 컨트롤러 클래스
 * @Modification Information
 * <p/>
 * 수정일       수정자         수정내용
 * -------        -------     -------------------
 * 2009. 3. 11.     이삼섭  최초생성
 * @see
 * @since 2009. 3. 11.
 */
@Controller
public class EgovLogManageController {

	/**
	 * @uml.property name="logManageService"
	 * @uml.associationEnd readOnly="true"
	 */
	@Resource(name = "EgovLogManageService")
	private EgovLogManageService logManageService;

	/**
	 * @uml.property name="propertyService"
	 * @uml.associationEnd readOnly="true"
	 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	/**
	 * 로그인 로그 목록 조회
	 *
	 * @param loginLog
	 * @return sym/log/EgovLoginLogList
	 * @throws Exception
	 */
	@RequestMapping(value = "/sym/log/SelectLoginLogList.do")
	public String selectLoginLogInf(@ModelAttribute("searchVO") LoginLog loginLog, ModelMap model) throws Exception {

		loginLog.setPageUnit(propertyService.getInt("pageUnit"));
		loginLog.setPageSize(propertyService.getInt("pageSize"));

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(loginLog.getPageIndex());
		paginationInfo.setRecordCountPerPage(loginLog.getPageUnit());
		paginationInfo.setPageSize(loginLog.getPageSize());

		loginLog.setFirstIndex(paginationInfo.getFirstRecordIndex());
		loginLog.setLastIndex(paginationInfo.getLastRecordIndex());
		loginLog.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		HashMap _map = (HashMap) logManageService.selectLoginLogInf(loginLog);
		int totCnt = Integer.parseInt((String) _map.get("resultCnt"));

		model.addAttribute("resultList", _map.get("resultList"));
		model.addAttribute("resultCnt", _map.get("resultCnt"));

		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		return "sym/log/EgovLoginLogList";
	}

	/**
	 * 로그인 로그 상세 조회
	 *
	 * @param loginLog
	 * @param model
	 * @return sym/log/EgovLoginLogInqire
	 * @throws Exception
	 */
	@RequestMapping(value = "/sym/log/InqireLoginLog.do")
	public String selectLoginLog(@ModelAttribute("searchVO") LoginLog loginLog, @RequestParam("logId") String logId, ModelMap model) throws Exception {

		loginLog.setLogId(logId.trim());

		LoginLog vo = logManageService.selectLoginLog(loginLog);
		model.addAttribute("result", vo);
		return "sym/log/EgovLoginLogInqire";
	}

	/**
	 * 로그인 로그 상세 조회
	 *
	 * @param model
	 * @return sym/log/EgovLoginLogInqire
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/sym/log/adminLoginLogList.do")
	public String mngSelectLoginLogList(@ModelAttribute("adminLogVo") AdminLogVo adminLogVo, ModelMap model) throws SQLException {
		adminLogVo.setPageUnit(propertyService.getInt("pageUnit"));
		adminLogVo.setPageSize(propertyService.getInt("pageSize"));
		adminLogVo.setLogin_mthd("I");

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(adminLogVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(adminLogVo.getPageUnit());
		paginationInfo.setPageSize(adminLogVo.getPageSize());

		adminLogVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		adminLogVo.setLastIndex(paginationInfo.getLastRecordIndex());
		adminLogVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<AdminLogVo> alist = logManageService.selectAdminLoginLogs(adminLogVo);
		int totCnt = logManageService.selectAdminLoginLogsCount(adminLogVo);

		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("alist", alist);
		model.addAttribute("adminLogVo", adminLogVo);
		return "mng/sym/log/adminLoginLogList";
	}


	/**
	 * 웹 로그 목록 조회
	 *
	 * @param webLog
	 * @return sym/log/EgovWebLogList
	 * @throws Exception
	 */
	@RequestMapping(value = "/sym/log/SelectWebLogList.do")
	public String selectWebLogInf(@ModelAttribute("searchVO") WebLog webLog, ModelMap model) throws Exception {

		webLog.setPageUnit(propertyService.getInt("pageUnit"));
		webLog.setPageSize(propertyService.getInt("pageSize"));

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(webLog.getPageIndex());
		paginationInfo.setRecordCountPerPage(webLog.getPageUnit());
		paginationInfo.setPageSize(webLog.getPageSize());

		webLog.setFirstIndex(paginationInfo.getFirstRecordIndex());
		webLog.setLastIndex(paginationInfo.getLastRecordIndex());
		webLog.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		HashMap _map = (HashMap) logManageService.selectWebLogInf(webLog);
		int totCnt = Integer.parseInt((String) _map.get("resultCnt"));

		model.addAttribute("resultList", _map.get("resultList"));
		model.addAttribute("resultCnt", _map.get("resultCnt"));

		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		return "sym/log/EgovWebLogList";
	}

	/**
	 * 웹 로그 상세 조회
	 *
	 * @param webLog
	 * @param model
	 * @return sym/log/EgovWebLogInqire
	 * @throws Exception
	 */
	@RequestMapping(value = "/sym/log/InqireWebLog.do")
	public String selectWebLog(@ModelAttribute("searchVO") WebLog webLog, @RequestParam("requstId") String requstId, ModelMap model) throws Exception {

		webLog.setRequstId(requstId.trim());

		WebLog vo = logManageService.selectWebLog(webLog);
		model.addAttribute("result", vo);
		return "sym/log/EgovWebLogInqire";
	}

	/**
	 * 관리자 접속로그 엑셀 다운로드
	 * @param modelMap
	 * @param adminLogVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/sym/log/mngLoginExcelDownload.do")
	public String loginRecordExcelDownload(Map<String,Object> modelMap, @ModelAttribute("adminLogVo") AdminLogVo adminLogVo) throws Exception{
		String sheetName = "관리자접속로그";
		String[] rowTitleList=new String[] {"번호", "아이디", "이름", "구분", "IP", "접속일자"};
		ArrayList<String[]> dataList = new ArrayList<String[]>();
		adminLogVo.setLogin_mthd("I");
		adminLogVo.setIsPaging("nopage");
		List<AdminLogVo>  list = logManageService.selectAdminLoginLogs(adminLogVo);
		int idx = 0;
		for(AdminLogVo vo : list){
			idx++;

			StringBuffer menuTxt = new StringBuffer();

			String[] valArr = {Integer.toString(idx)+"", vo.getLogin_id()+"", vo.getUser_nm()+"", vo.getAuthor_nm()+"", vo.getLogin_ip()+"", vo.getCreat_dt()+""};
			dataList.add(valArr);
		}
		modelMap.put("sheetName", sheetName);
		modelMap.put("rowTitleList", rowTitleList);
		modelMap.put("dataList", dataList);
		return "excelView";
	}

}
