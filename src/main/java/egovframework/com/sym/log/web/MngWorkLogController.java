package egovframework.com.sym.log.web;

import egovframework.com.gfund.biz.bassinfo.service.BasicInformationVo;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.log.service.MngWorkLogService;
import egovframework.com.sym.log.service.MngWorkLogVo;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class MngWorkLogController {

	@Resource(name = "mngWorkLogService")
	private MngWorkLogService mngWorkLogService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	/**
	 * 관리자 작업로그 목록 조회
	 * @param mngWorkLogVo
	 * @param model
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/sym/log/mngWorkLogs.do")
	public String mngWorkLogs(@ModelAttribute("mngWorkLogVo") MngWorkLogVo mngWorkLogVo, ModelMap model) throws SQLException{
		mngWorkLogVo.setPageUnit(20);
		mngWorkLogVo.setPageSize(propertyService.getInt("pageSize"));

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(mngWorkLogVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(mngWorkLogVo.getPageUnit());
		paginationInfo.setPageSize(mngWorkLogVo.getPageSize());

		mngWorkLogVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		mngWorkLogVo.setLastIndex(paginationInfo.getLastRecordIndex());
		mngWorkLogVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<MngWorkLogVo>  list = mngWorkLogService.selectMngWorkLogs(mngWorkLogVo);
		int totCnt = mngWorkLogService.selectMngWorkLogsCount(mngWorkLogVo);

		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("list", list);
		model.addAttribute("mngWorkLogVo", mngWorkLogVo);

		model.addAttribute("glist1", mngWorkLogService.selectMngWorkGroup1());
		model.addAttribute("glist2", mngWorkLogService.selectMngWorkGroup2());
		model.addAttribute("glist3", mngWorkLogService.selectMngWorkGroup3());
		model.addAttribute("glist4", mngWorkLogService.selectMngWorkGroup4());
		model.addAttribute("glist5", mngWorkLogService.selectMngWorkGroup5());

		return "mng/sym/log/mngWorkLogs";
	}

	/**
	 * 관리자 작업로그 삭제
	 * @param mngWorkLogVo
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/sym/log/inDeleteMngWorkLog.do")
	public String inDeleteMngWorkLog(@ModelAttribute("mngWorkLogVo") MngWorkLogVo mngWorkLogVo) throws SQLException{
		mngWorkLogService.inDeleteMngWorkLog(mngWorkLogVo);
		return "redirect:/mng/sym/log/mngWorkLogs.do";
	}

	/**
	 * 관리자 작업로그 Ajax 등록
	 * @param mngWorkLogVo
	 * @param request
	 * @param response
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value = "/mng/sym/log/ajaxInsertMngWorkLog.do")
	public ModelAndView insertMngWorkLog(@ModelAttribute("mngWorkLogVo") MngWorkLogVo mngWorkLogVo, HttpServletRequest request, HttpServletResponse response) throws SQLException{
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		ModelAndView mav = new ModelAndView("jsonView");
		mngWorkLogVo.setOpertor_ip(request.getRemoteAddr());
		mngWorkLogVo.setOpertor_id(loginVO.getId());
		mav.addObject("rs", mngWorkLogService.insertMngWorkLog(mngWorkLogVo));
		return mav;
	}

	/**
	 * 관리자 작업로그 엑셀 다운로드
	 * @param modelMap
	 * @param mngWorkLogVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/sym/log/mngWorkLogExcelDownload.do")
	public String recordExcelDownload(Map<String,Object> modelMap, @ModelAttribute("mngWorkLogVo") MngWorkLogVo mngWorkLogVo) throws Exception{
		String sheetName = "관리자작업로그";
		String[] rowTitleList=new String[] {"번호", "작업영역", "작업내역", "URL", "ID", "IP", "작업일자"};
		ArrayList<String[]> dataList = new ArrayList<String[]>();
		mngWorkLogVo.setIsPaging("nopage");
		List<MngWorkLogVo>  list = mngWorkLogService.selectMngWorkLogs(mngWorkLogVo);
		int idx = 0;
		for(MngWorkLogVo vo : list){
			idx++;

			StringBuffer menuTxt = new StringBuffer();

			menuTxt.append(vo.getMenu_1st_nm());
			if(!"".equals(vo.getMenu_2nd_nm())){
				menuTxt.append(" > " + vo.getMenu_2nd_nm());
			}else if(!"".equals(vo.getMenu_3rd_nm())){
				menuTxt.append(" > " + vo.getMenu_3rd_nm());
			}else if(!"".equals(vo.getMenu_4th_nm())){
				menuTxt.append(" > " + vo.getMenu_4th_nm());
			}else if(!"".equals(vo.getMenu_5th_nm())){
				menuTxt.append(" > " + vo.getMenu_5th_nm());
			}
			String[] valArr = {Integer.toString(idx)+"", menuTxt.toString()+"", vo.getWork_ty_nm()+"", vo.getUrl()+"", vo.getOpertor_id()+"", vo.getOpertor_ip()+"", vo.getOccrrnc_pnttm()+""};
			dataList.add(valArr);
		}
		modelMap.put("sheetName", sheetName);
		modelMap.put("rowTitleList", rowTitleList);
		modelMap.put("dataList", dataList);
		return "excelView";
	}
}
