package egovframework.com.sym.log.web;

import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.log.service.MngWorkLogService;
import egovframework.com.sym.log.service.MngWorkLogVo;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class MngWorkLogInterceptor extends HandlerInterceptorAdapter {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "mngWorkLogService")
	private MngWorkLogService mngWorkLogService;

	@Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String reqURL = request.getRequestURI();
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		MngWorkLogVo logVo=new MngWorkLogVo();

		if(reqURL.indexOf(".do") > -1){

			//사이트 관리
			if(reqURL.indexOf("/mng/sym/sit/insertSiteInfo.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("사이트관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}else if(reqURL.indexOf("/mng/sym/sit/updateSiteInfo.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("사이트관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//템플릿 관리
			else if(reqURL.indexOf("/mng/cop/com/insertLytTemplate.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("템플릿관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/updateLytTemplate.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("템플릿관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/deleteLytTemplate.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("템플릿관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//레이아웃관리
			else if(reqURL.indexOf("/mng/cop/com/insertLytSourc.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("레이아웃관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/updateLytSourc.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("레이아웃관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/deleteLytSourc.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("레이아웃관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//공통코드관리
			else if(reqURL.indexOf("/mng/sym/ccm/cca/EgovCcmCmmnCodeRegist.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("공통코드관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/sym/ccm/cca/EgovCcmCmmnCodeModify.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("공통코드관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/sym/ccm/cca/EgovCcmCmmnCodeRemove.do") > -1){
				logVo.setMenu_1st_nm("사이트관리");
				logVo.setMenu_2nd_nm("공통코드관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//메뉴관리
			else if(reqURL.indexOf("/mng/sym/mpm/addMpm.do") > -1){
				logVo.setMenu_1st_nm("메뉴관리");
				logVo.setMenu_2nd_nm("메뉴관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/sym/mpm/forUpdateMpm.do") > -1){
				logVo.setMenu_1st_nm("메뉴관리");
				logVo.setMenu_2nd_nm("메뉴관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/sym/mpm/deleteMpm.do") > -1){
				logVo.setMenu_1st_nm("메뉴관리");
				logVo.setMenu_2nd_nm("메뉴관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//관리자관리
			else if(reqURL.indexOf("/mng/usr/EgovUserSelectIndt.do") > -1){
				logVo.setMenu_1st_nm("관리자관리");
				logVo.setMenu_2nd_nm("관리자목록");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/usr/EgovUserSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("관리자관리");
				logVo.setMenu_2nd_nm("관리자목록");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/usr/EgovMberManageDelete") > -1){
				logVo.setMenu_1st_nm("관리자관리");
				logVo.setMenu_2nd_nm("관리자목록");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//게시판관리
			else if(reqURL.indexOf("/mng/cop/bbs/insertBBSMasterInf.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/UpdateBBSMasterInf.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/DeleteBBSMasterInf.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//게시판 게시물 관리
			else if(reqURL.indexOf("/mng/cop/bbs/insertBoardArticle.do") > -1){
				BoardMasterVO vo = new BoardMasterVO();
				vo.setSiteId(request.getParameter("siteId"));
				vo.setSysTyCode(siteVO.getSysTyCode());
				vo.setBbsId(request.getParameter("bbsId"));
				vo.setTrgetId(request.getParameter("trgetId"));
				BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

				String[] bbsNameArr = master.getBbsNm().split(" > ");
				String bbsName = "";
				if(bbsNameArr.length > 1){
					bbsName = bbsNameArr[bbsNameArr.length-1];
				}else{
					bbsName = master.getBbsNm();
				}
				if(master.getSiteId().equals("SITE_000000000000001")){
					logVo.setMenu_1st_nm("게시판관리");
				}else if(master.getSiteId().equals("SITE_000000000000002")){
					logVo.setMenu_1st_nm("녹색자금통합관리");
				}

				logVo.setMenu_2nd_nm(bbsName);

				if(master.getBbsNm().equals("정부3.0 정보공개 > 정보목록")){
					logVo.setMenu_1st_nm("정보공개관리");
					logVo.setMenu_2nd_nm("정보목록");
				}

				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/updateBoardArticle.do") > -1){
				BoardMasterVO vo = new BoardMasterVO();
				vo.setSiteId(request.getParameter("siteId"));
				vo.setSysTyCode(siteVO.getSysTyCode());
				vo.setBbsId(request.getParameter("bbsId"));
				vo.setTrgetId(request.getParameter("trgetId"));
				BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

				String[] bbsNameArr = master.getBbsNm().split(" > ");
				String bbsName = "";
				if(bbsNameArr.length > 1){
					bbsName = bbsNameArr[bbsNameArr.length-1];
				}else{
					bbsName = master.getBbsNm();
				}
				if(master.getSiteId().equals("SITE_000000000000001")){
					logVo.setMenu_1st_nm("게시판관리");
				}else if(master.getSiteId().equals("SITE_000000000000002")){
					logVo.setMenu_1st_nm("녹색자금통합관리");
				}

				logVo.setMenu_2nd_nm(bbsName);

				if(master.getBbsNm().equals("정부3.0 정보공개 > 정보목록")){
					logVo.setMenu_1st_nm("정보공개관리");
					logVo.setMenu_2nd_nm("정보목록");
				}

				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/deleteBoardArticle.do") > -1){
				BoardMasterVO vo = new BoardMasterVO();
				vo.setSiteId(request.getParameter("siteId"));
				vo.setSysTyCode(siteVO.getSysTyCode());
				vo.setBbsId(request.getParameter("bbsId"));
				vo.setTrgetId(request.getParameter("trgetId"));
				BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

				String[] bbsNameArr = master.getBbsNm().split(" > ");
				String bbsName = "";
				if(bbsNameArr.length > 1){
					bbsName = bbsNameArr[bbsNameArr.length-1];
				}else{
					bbsName = master.getBbsNm();
				}
				if(master.getSiteId().equals("SITE_000000000000001")){
					logVo.setMenu_1st_nm("게시판관리");
				}else if(master.getSiteId().equals("SITE_000000000000002")){
					logVo.setMenu_1st_nm("녹색자금통합관리");
				}

				logVo.setMenu_2nd_nm(bbsName);

				if(master.getBbsNm().equals("정부3.0 정보공개 > 정보목록")){
					logVo.setMenu_1st_nm("정보공개관리");
					logVo.setMenu_2nd_nm("정보목록");
				}

				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//카테고리관리
			else if(reqURL.indexOf("/mng/cop/bbs/ctg/insertBBSCtgryMaster.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("카테고리관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/ctg/updateBBSCtgryMaster.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("카테고리관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/bbs/ctg/deleteBBSCtgryMaster.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("카테고리관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//게시판 템플릿 관리
			else if(reqURL.indexOf("/mng/cop/com/insertBbsTemplate.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 템플릿 관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/updateBbsTemplate.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 템플릿 관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/deleteBbsTemplate.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 템플릿 관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//게시판 소스 관리
			else if(reqURL.indexOf("/mng/cop/com/insertBbsSourc.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 소스 관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/updateBbsSourc.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 소스 관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/cop/com/deleteBbsSourc.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("게시판 소스 관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//녹색자료실 06~09
			else if(reqURL.indexOf("/mng/gds/insertComtngn.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("녹색자료실 06~09");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gds/updateComtngn.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("녹색자료실 06~09");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gds/deleteComtngn.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("녹색자료실 06~09");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//통합캘린더관리 > 전체
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") == null){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("전체");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") != null){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("전체");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/deleteComtnschdulinfo.do") > -1){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("전체");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//통합캘린더관리 > 행사및일반일정
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") == null && request.getParameter("searchSe").equals("1")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("행사및일반일정");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") != null && request.getParameter("searchSe").equals("1")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("행사및일반일정");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/deleteComtnschdulinfo.do") > -1 && request.getParameter("searchSe").equals("1")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("행사및일반일정");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//통합캘린더관리 > 설문조사
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") == null && request.getParameter("searchSe").equals("3")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("설문조사");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/processComtnschdulinfo.do") > -1 && request.getParameter("schdulId") != null && request.getParameter("searchSe").equals("3")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("설문조사");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/evt/deleteComtnschdulinfo.do") > -1 && request.getParameter("searchSe").equals("3")){
				logVo.setMenu_1st_nm("게시판관리");
				logVo.setMenu_2nd_nm("통합캘린더관리");
				logVo.setMenu_3rd_nm("설문조사");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//팝업존관리
			else if(reqURL.indexOf("/mng/uss/ion/bnr/addBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN001")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업존관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/updtBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN001")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업존관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/removeBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN001")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업존관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//배너존관리
			else if(reqURL.indexOf("/mng/uss/ion/bnr/addBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN002")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("배너존관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/updtBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN002")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("배너존관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/removeBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN002")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("배너존관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//메인이미지관리
			else if(reqURL.indexOf("/mng/uss/ion/bnr/addBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN007")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("메인이미지관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/updtBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN007")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("메인이미지관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/bnr/removeBanner.do") > -1 && request.getParameter("bannerTyCode").equals("BAN007")){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("메인이미지관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//팝업관리
			else if(reqURL.indexOf("/mng/uss/ion/pwm/registPopup.do") > -1 && (request.getParameter("cmd") != null && request.getParameter("cmd").equals("save"))){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/pwm/updtPopup.do") > -1 && (request.getParameter("cmd") != null && request.getParameter("cmd").equals("save"))){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/uss/ion/pwm/deletePopup.do") > -1){
				logVo.setMenu_1st_nm("기타관리");
				logVo.setMenu_2nd_nm("팝업관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//녹색자금통합시스템 > 회원관리
			else if(reqURL.indexOf("/mng/gfund/member/EgovUserSelectIndt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("회원관리");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gfund/member/EgovUserSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("회원관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gfund/member/EgovMberManageDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("회원관리");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//녹색자금통합시스템 > 사업공고
			else if(reqURL.indexOf("/mng/gfund/biz/ancmt/announcementInsert.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업공고");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gfund/biz/ancmt/announcementUpdate.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업공고");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
			else if(reqURL.indexOf("/mng/gfund/biz/ancmt/announcementDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업공고");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//녹색자금통합시스템 > 선정전형
			else if(reqURL.indexOf("/mng/gfund/biz/selection/selectionStepSave.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("선정전형");
				if(request.getParameter("step_cl").equals("STEP1")){
					logVo.setMenu_3rd_nm("1차심사");
				}else if(request.getParameter("step_cl").equals("STEP2")){
					logVo.setMenu_3rd_nm("현장심사");
				}else if(request.getParameter("step_cl").equals("STEP3")){
					logVo.setMenu_3rd_nm("2차심사");
				}else if(request.getParameter("step_cl").equals("STEP4")){
					logVo.setMenu_3rd_nm("최종심사");
				}
				if(request.getParameter("jdgmn_id") != null){
					logVo.setWork_ty("MWC04");
				}else{
					logVo.setWork_ty("MWC03");
				}
				mngWorkLogInsert(siteVO, user, logVo, request);
			}


			//녹색자금통합시스템 > 사업관리(지도점검대상여부)
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/ajaxUpdateBasicInformationCchchckTargetAt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 기본정보
			else if(reqURL.indexOf("/mng/gfund/biz/bassinfo/basicInformationSave.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("기본정보");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 사진정보
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPhotoInfoInsert.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("사진정보");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 사진정보
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPhotoInfoSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("사진정보");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 사진정보
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPhotoInfoDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("사진정보");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 관련증빙서
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPrufPapersInsert.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("관련증빙서");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 관련증빙서
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPrufPapersSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("관련증빙서");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 관련증빙서
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPrufPapersDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("관련증빙서");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 사업현황
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovPlanAcmsltSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("사업현황");
				if(request.getParameter("frstRegisterId") != null){
					logVo.setWork_ty("MWC03");
				}
				if(request.getParameter("lastUpdusrId") != null){
					logVo.setWork_ty("MWC04");
				}
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 사업관리 > 지도점검
			else if(reqURL.indexOf("/mng/gfund/biz/businessmng/EgovChckListSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("사업관리");
				logVo.setMenu_2nd_nm("지도점검");
				if(request.getParameter("frstRegisterId") != null){
					logVo.setWork_ty("MWC03");
				}
				if(request.getParameter("lastUpdusrId") != null){
					logVo.setWork_ty("MWC04");
				}
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 결과산출 > 최종서류제출
			else if(reqURL.indexOf("/mng/gfund/biz/productmng/EgovLastPapersInsert.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("결과산출");
				logVo.setMenu_2nd_nm("최종서류제출");
				logVo.setWork_ty("MWC03");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 결과산출 > 최종서류제출
			else if(reqURL.indexOf("/mng/gfund/biz/productmng/EgovLastPapersSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("결과산출");
				logVo.setMenu_2nd_nm("최종서류제출");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 결과산출 > 최종서류제출
			else if(reqURL.indexOf("/mng/gfund/biz/productmng/EgovLastPapersDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("결과산출");
				logVo.setMenu_2nd_nm("최종서류제출");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 결과산출 > 최종정산
			else if(reqURL.indexOf("/mng/gfund/biz/productmng/EgovExcclcDelete.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("결과산출");
				logVo.setMenu_2nd_nm("최종정산");
				logVo.setWork_ty("MWC05");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}

			//녹색자금통합시스템 > 결과산출 > 평가결과
			else if(reqURL.indexOf("/mng/gfund/biz/productmng/EgovEvaluationResultSelectUpdt.do") > -1){
				logVo.setMenu_1st_nm("녹색자금통합시스템");
				logVo.setMenu_2nd_nm("결과산출");
				logVo.setMenu_2nd_nm("평가결과");
				logVo.setWork_ty("MWC04");
				mngWorkLogInsert(siteVO, user, logVo, request);
			}
		}
		return true;
	}

	public void mngWorkLogInsert(SiteManageVO siteVO, LoginVO user, MngWorkLogVo logVo, HttpServletRequest request) throws SQLException{
		logVo.setSite_id(siteVO.getSiteId());
		logVo.setOpertor_ip(request.getRemoteAddr());
		logVo.setOpertor_id(user.getId());
		mngWorkLogService.insertMngWorkLog(logVo);
	}
}
