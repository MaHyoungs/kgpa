package egovframework.com.uss.umt.service;

public class UserCamelCaseVo {
	private String user_id;
	private String site_id;
	private String user_se_code;
	private String credt_id;
	private String crtfct_dn;
	private String crtfct_serial;
	private String password;
	private String user_nm;
	private String email_adres;
	private String tlphon_no;
	private String moblphon_no;
	private String faxphon_no;
	private String zip;
	private String adres;
	private String adres_detail;
	private String r_adres;
	private String r_adres_detail;
	private String brthdy;
	private String slrcld_lrr_code;
	private String sexdstn;
	private String organ_nm;
	private String email_recptn_at;
	private String moblphon_recptn_at;
	private String photo_original_file_nm;
	private String photo_stre_file_nm;
	private String code;
	private String indvdlinfo_prsrv_pd;
	private String confm_at;
	private String confm_pnttm;
	private String delete_at;
	private String delete_resn;
	private String delete_pnttm;
	private String frst_regist_ip;
	private String frst_regist_pnttm;
	private String frst_register_id;
	private String last_updusr_pnttm;
	private String last_updusr_id;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getUser_se_code() {
		return user_se_code;
	}

	public void setUser_se_code(String user_se_code) {
		this.user_se_code = user_se_code;
	}

	public String getCredt_id() {
		return credt_id;
	}

	public void setCredt_id(String credt_id) {
		this.credt_id = credt_id;
	}

	public String getCrtfct_dn() {
		return crtfct_dn;
	}

	public void setCrtfct_dn(String crtfct_dn) {
		this.crtfct_dn = crtfct_dn;
	}

	public String getCrtfct_serial() {
		return crtfct_serial;
	}

	public void setCrtfct_serial(String crtfct_serial) {
		this.crtfct_serial = crtfct_serial;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser_nm() {
		return user_nm;
	}

	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}

	public String getEmail_adres() {
		return email_adres;
	}

	public void setEmail_adres(String email_adres) {
		this.email_adres = email_adres;
	}

	public String getTlphon_no() {
		return tlphon_no;
	}

	public void setTlphon_no(String tlphon_no) {
		this.tlphon_no = tlphon_no;
	}

	public String getMoblphon_no() {
		return moblphon_no;
	}

	public void setMoblphon_no(String moblphon_no) {
		this.moblphon_no = moblphon_no;
	}

	public String getFaxphon_no() {
		return faxphon_no;
	}

	public void setFaxphon_no(String faxphon_no) {
		this.faxphon_no = faxphon_no;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getAdres_detail() {
		return adres_detail;
	}

	public void setAdres_detail(String adres_detail) {
		this.adres_detail = adres_detail;
	}

	public String getR_adres() {
		return r_adres;
	}

	public void setR_adres(String r_adres) {
		this.r_adres = r_adres;
	}

	public String getR_adres_detail() {
		return r_adres_detail;
	}

	public void setR_adres_detail(String r_adres_detail) {
		this.r_adres_detail = r_adres_detail;
	}

	public String getBrthdy() {
		return brthdy;
	}

	public void setBrthdy(String brthdy) {
		this.brthdy = brthdy;
	}

	public String getSlrcld_lrr_code() {
		return slrcld_lrr_code;
	}

	public void setSlrcld_lrr_code(String slrcld_lrr_code) {
		this.slrcld_lrr_code = slrcld_lrr_code;
	}

	public String getSexdstn() {
		return sexdstn;
	}

	public void setSexdstn(String sexdstn) {
		this.sexdstn = sexdstn;
	}

	public String getOrgan_nm() {
		return organ_nm;
	}

	public void setOrgan_nm(String organ_nm) {
		this.organ_nm = organ_nm;
	}

	public String getEmail_recptn_at() {
		return email_recptn_at;
	}

	public void setEmail_recptn_at(String email_recptn_at) {
		this.email_recptn_at = email_recptn_at;
	}

	public String getMoblphon_recptn_at() {
		return moblphon_recptn_at;
	}

	public void setMoblphon_recptn_at(String moblphon_recptn_at) {
		this.moblphon_recptn_at = moblphon_recptn_at;
	}

	public String getPhoto_original_file_nm() {
		return photo_original_file_nm;
	}

	public void setPhoto_original_file_nm(String photo_original_file_nm) {
		this.photo_original_file_nm = photo_original_file_nm;
	}

	public String getPhoto_stre_file_nm() {
		return photo_stre_file_nm;
	}

	public void setPhoto_stre_file_nm(String photo_stre_file_nm) {
		this.photo_stre_file_nm = photo_stre_file_nm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIndvdlinfo_prsrv_pd() {
		return indvdlinfo_prsrv_pd;
	}

	public void setIndvdlinfo_prsrv_pd(String indvdlinfo_prsrv_pd) {
		this.indvdlinfo_prsrv_pd = indvdlinfo_prsrv_pd;
	}

	public String getConfm_at() {
		return confm_at;
	}

	public void setConfm_at(String confm_at) {
		this.confm_at = confm_at;
	}

	public String getConfm_pnttm() {
		return confm_pnttm;
	}

	public void setConfm_pnttm(String confm_pnttm) {
		this.confm_pnttm = confm_pnttm;
	}

	public String getDelete_at() {
		return delete_at;
	}

	public void setDelete_at(String delete_at) {
		this.delete_at = delete_at;
	}

	public String getDelete_resn() {
		return delete_resn;
	}

	public void setDelete_resn(String delete_resn) {
		this.delete_resn = delete_resn;
	}

	public String getDelete_pnttm() {
		return delete_pnttm;
	}

	public void setDelete_pnttm(String delete_pnttm) {
		this.delete_pnttm = delete_pnttm;
	}

	public String getFrst_regist_ip() {
		return frst_regist_ip;
	}

	public void setFrst_regist_ip(String frst_regist_ip) {
		this.frst_regist_ip = frst_regist_ip;
	}

	public String getFrst_regist_pnttm() {
		return frst_regist_pnttm;
	}

	public void setFrst_regist_pnttm(String frst_regist_pnttm) {
		this.frst_regist_pnttm = frst_regist_pnttm;
	}

	public String getFrst_register_id() {
		return frst_register_id;
	}

	public void setFrst_register_id(String frst_register_id) {
		this.frst_register_id = frst_register_id;
	}

	public String getLast_updusr_pnttm() {
		return last_updusr_pnttm;
	}

	public void setLast_updusr_pnttm(String last_updusr_pnttm) {
		this.last_updusr_pnttm = last_updusr_pnttm;
	}

	public String getLast_updusr_id() {
		return last_updusr_id;
	}

	public void setLast_updusr_id(String last_updusr_id) {
		this.last_updusr_id = last_updusr_id;
	}
}
