package egovframework.com.uss.umt.web;

import Kisinfo.Check.IPINClient;
import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.mail.MailVo;
import egovframework.com.cmm.mail.SendMail;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.Globals;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.com.utl.sim.service.Sha1Util;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * ***************************************************
 *
 * @author : 이호영
 * @version : 1.0.0
 * @Class Name : EgovUserManageController.java
 * @Program name : egovframework.com.uss.umt.web
 * @Descriptopn :
 * @created date : 2011. 7. 26.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2011. 7. 26.        이호영             first generated
 * *******************************************************
 */

@Controller
public class EgovUserInfoController {

	@Resource(name = "SiteManageService")
	private EgovSiteManageService siteManageService;

	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "userManageService")
	private EgovUserManageService userManageService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	@Resource(name = "loginService")
	private EgovLoginService loginService;

	protected Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private DefaultBeanValidator beanValidator;

	@Resource(name = "EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

	/**
	 * 회원구분 선택
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovSelectMber
	 * @throws Exception
	 */
	@RequestMapping("/uss/umt/cmm/EgovSelectMber.do")
	public String selectMber(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {

		return "cmm/uss/umt/EgovSelectMber";
	}

	/**
	 * 약관확인
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovStplatCnfirm
	 * @throws Exception
	 */
	@RequestMapping("/uss/umt/cmm/EgovStplatCnfirmMber.do")
	public String stplatCnfirmMber(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteVO", siteVO);
		return "cmm/uss/umt/EgovStplatCnfirm";
	}

	/**
	 * 회원 가입 인증 관련
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovCertificate
	 * @throws Exception
	 */
	@RequestMapping("/uss/umt/cmm/EgovCertificate.do")
	public String certificate(HttpServletRequest request, ModelMap model) throws Exception {
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteVO", siteVO);
		return "cmm/uss/umt/EgovCertificate";
	}

	@RequestMapping("/uss/umt/cmm/EgovUserPrivateCertification.do")
	public String userPrivateCertification(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {
		return "cmm/uss/umt/EgovUserPrivateCertification";
	}

	/**
	 * 사용자등록화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovUserInsert
	 * @throws Exception
	 */
	@RequestMapping("/uss/umt/user/EgovUserInsertView.do")
	public String insertUserView(ModelMap model, HttpSession session, HttpServletRequest request) throws Exception {
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteVO", siteVO);
		LoginVO UserVo = (LoginVO) session.getAttribute("UserPrivateCert");
		if (UserVo != null) {
			model.addAttribute("UserVo", UserVo);
			return "cmm/uss/umt/EgovUserInsert";
		} else {
			return "redirect:/uss/umt/cmm/EgovCertificate.do";
		}
	}

	/**
	 * 사용자등록처리 임시 컨트롤러
	 *
	 * @param userManageVO 사용자등록정보
	 * @param model        화면모델
	 * @return redirect:/uss/umt/user/EgovUserInsert.do
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/uss/umt/user/EgovUserTempInsert.do")
	public String insertTempUser(@ModelAttribute("userManageVO") UserManageVO userManageVO, HttpSession session, ModelMap model, HttpServletRequest request) throws Exception {
		LoginVO UserVo = (LoginVO) session.getAttribute("UserPrivateCert");

		if (UserVo == null) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.request.msg"));
			return "forward:/uss/umt/cmm/EgovCertificate.do";
		}

		if (userManageService.checkIdDplct(userManageVO.getUserId()) > 0) {
			model.addAttribute("message", egovMessageSource.getMessage("common.isExist.msg"));
			return "forward:/sec/rnc/EgovRlnmCnfirm.do";
		} else {
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			userManageVO.setSiteId(siteVO.getSiteId());
			userManageVO.setConfmAt("Y");
			userManageService.insertUser(userManageVO);

			//세션 삭제
			session.removeAttribute("UserPrivateCert");

			/*model.addAttribute("siteVO", siteVO);
			model.addAttribute("mberNm", userManageVO.getUserNm());
			model.addAttribute("mberId", userManageVO.getUserId());*/

			request.getSession().setAttribute("siteVO", siteVO);
			request.getSession().setAttribute("userVO", userManageVO);

			return "redirect:/uss/umt/user/EgovUserInsert.do";
		}
	}

	/**
	 * 사용자등록처리후 메인화면으로 이동한다.
	 *
	 * @param userManageVO 사용자등록정보
	 * @param model        화면모델
	 * @return cmm/uss/umt/EgovUserInsertComplete
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/uss/umt/user/EgovUserInsert.do")
	public String insertUser(@ModelAttribute("userManageVO") UserManageVO userManageVO, HttpSession session, ModelMap model, HttpServletRequest request) throws Exception {

		UserManageVO userVO = (UserManageVO)request.getSession().getAttribute("userVO");
		SiteManageVO siteVO = (SiteManageVO)request.getSession().getAttribute("siteVO");

		request.removeAttribute("userVO");
		request.removeAttribute("siteVO");

		model.addAttribute("siteVO", siteVO);
		model.addAttribute("mberNm", userVO.getUserNm());
		model.addAttribute("mberId", userVO.getUserId());

		return "cmm/uss/umt/EgovUserInsertComplete";
	}

	/**
	 * 입력한 사용자아이디의 중복확인화면 이동
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovIdDplctCnfirm
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/EgovIdDplctCnfirmView.do")
	public String checkIdDplctCnfirmView(ModelMap model) throws Exception {
		model.addAttribute("checkId", "");
		model.addAttribute("usedCnt", "-1");
		return "cmm/uss/umt/EgovIdDplctCnfirm";
	}

	/**
	 * 입력한 사용자아이디의 중복여부를 체크하여 사용가능여부를 확인
	 *
	 * @param commandMap 파라메터전달용 commandMap
	 * @param model      화면모델
	 * @return cmm/uss/umt/EgovIdDplctCnfirm
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovIdDplctCnfirm.do")
	public String checkIdDplctCnfirm(Map<String, Object> commandMap, ModelMap model) throws Exception {

		String checkId = (String) commandMap.get("checkId");
		checkId = new String(checkId.getBytes("ISO-8859-1"), "UTF-8");

		if (checkId == null || checkId.equals("")) return "forward:/uss/umt/EgovIdDplctCnfirmView.do";

		int usedCnt = userManageService.checkIdDplct(checkId);
		model.addAttribute("usedCnt", usedCnt);
		model.addAttribute("checkId", checkId);

		return "cmm/uss/umt/EgovIdDplctCnfirm";
	}

	@RequestMapping(value = "/uss/umt/cmm/EgovIdDplctCheck.do")
	public ModelAndView EgovIdDplctCheck(HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		String checkId = request.getParameter("checkId");
		int usedCnt = userManageService.checkIdDplct(checkId);
		mav.addObject("rs", usedCnt);
		return mav;
	}


	/**
	 * 개인정보 보호를 위한 페스워드 인증하면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovUserConfirm
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserConfirmView.do")
	public String userConfirmView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		userManageVO.setUserId(loginVO.getId());

		model.addAttribute("userManageVO", userManageVO);
		return "cmm/uss/umt/EgovUserConfirm";
	}

	/**
	 * 개인정보 보호를 위한 페스워드를 받아 확인한다.
	 *
	 * @param model 화면모델
	 * @return "cmm/uss/umt/EgovUserConfirm"
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserConfirm.do")
	public String userConfirm(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		//입력한 정보가 맞는지 체크
		if (userManageService.selectCheckPassword(userManageVO) > 0) {
			if ("secsn".equals(searchVO.getTrgtPge())) {
				//탈퇴화면으로 이동
				return "forward:/uss/umt/cmm/EgovUserSecsnView.do";
			} else if ("update".equals(searchVO.getTrgtPge())) {
				//사용자정보 수정화면으로 이동
				return "forward:/uss/umt/cmm/EgovUserUpdateView.do";
			} else if ("password".equals(searchVO.getTrgtPge())) {
				//비밀번호 변경화면으로 이동
				return "forward:/uss/umt/cmm/EgovUserPasswordUpdateView.do";
			}
		} else {
			model.addAttribute("userManageVO", userManageVO);
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.idpw"));
			return "cmm/uss/umt/EgovUserConfirm";
		}

		return "forward:" + Globals.MAIN_PAGE;
	}


	/**
	 * 사용자정보 수정 화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovUserUpdate
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserUpdateView.do")
	public String EgovUserSelectUpdtView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:/uat/uia/egovLoginUsr.do";
		}
		userManageVO.setUserId(loginVO.getId());
		userManageVO = userManageService.selectUser(userManageVO);
		model.addAttribute("userManageVO", userManageVO);
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteVO", siteVO);
		return "cmm/uss/umt/EgovUserUpdate";
	}

	/**
	 * 사용자정보 수정 처리 임시
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserTempUpdate.do")
	public String EgovUserTempSelectUpdt(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:/uat/uia/egovLoginUsr.do";
		}

		beanValidator.validate(userManageVO, bindingResult);

		if (!EgovStringUtil.isEmpty(userManageVO.getTel1()) && !EgovStringUtil.isEmpty(userManageVO.getTel2()) && !EgovStringUtil.isEmpty(userManageVO.getTel3())) {
			userManageVO.setTlphonNo(userManageVO.getTel1() + "-" + userManageVO.getTel2() + "-" + userManageVO.getTel3());
		}
		if (!EgovStringUtil.isEmpty(userManageVO.getPhone1()) && !EgovStringUtil.isEmpty(userManageVO.getPhone2()) && !EgovStringUtil.isEmpty(userManageVO.getPhone3())) {
			userManageVO.setMoblphonNo(userManageVO.getPhone1() + "-" + userManageVO.getPhone2() + "-" + userManageVO.getPhone3());
		}
		if (!EgovStringUtil.isEmpty(userManageVO.getEmail1()) && !EgovStringUtil.isEmpty(userManageVO.getEmail2())) {
			userManageVO.setEmailAdres(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
		}

		userManageVO.setUserId(loginVO.getId());
		userManageVO.setLastUpdusrId(loginVO.getId());
		userManageVO.setUserSeCode(loginVO.getUserSeCode());
		if (userManageService.updateUser(userManageVO) > 0) {
			request.getSession().setAttribute("userVO", userManageVO);
		}
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		request.getSession().setAttribute("siteVO", siteVO);

		return "redirect:/uss/umt/cmm/EgovUserUpdate.do";
	}

	/**
	 * 사용자정보 수정 처리 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserUpdate.do")
	public String EgovUserSelectUpdt(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		userManageVO = (UserManageVO)request.getSession().getAttribute("userVO");
		SiteManageVO siteVO = (SiteManageVO)request.getSession().getAttribute("siteVO");

		request.removeAttribute("userVO");
		request.removeAttribute("siteVO");

		model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));
		model.addAttribute("result", true);
		model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		model.addAttribute("siteVO", siteVO);

		return "cmm/uss/umt/EgovUserUpdate";
	}

	/**
	 * 회원탈퇴 회면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return ivp/mpe/ComtnmberSecsn
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserSecsnView.do")
	public String selectComtnmberSecsn(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		return "cmm/uss/umt/EgovUserSecsn";
	}

	/**
	 * 회원탈퇴를 처리 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/uat/uia/actionLogout.do
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserSecsn.do")
	public String updateComtnmberSecsn(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		userManageVO.setUserId(loginVO.getId());

		if (userManageService.deleteUser(userManageVO) > 0) {
			model.addAttribute("result", true);
			model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		}

		return "cmm/uss/umt/EgovUserSecsn";
	}

	/**
	 * 비밀번호 변경화면으로 이동한다.
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovUserPasswordUpdate
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserPasswordUpdateView.do")
	public String userPasswordUpdateView(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		return "cmm/uss/umt/EgovUserPasswordUpdate";
	}

	/**
	 * 비밀번호 변경 한다.
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserPasswordUpdate.do")
	public String userPasswordUpdate(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO == null) {
			return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
		}

		userManageVO.setUserId(loginVO.getId());

		if (userManageService.updatePassword(userManageVO) > 0) {
			model.addAttribute("result", true);
			model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		}
		return "cmm/uss/umt/EgovUserPasswordUpdate";
	}

	@RequestMapping(value = "/uss/umt/cmm/selectSchool")
	public String selectSchool(@RequestParam("stTyCode") String stTyCode, Model model) throws Exception {

		ComDefaultCodeVO codeVO = new ComDefaultCodeVO();

		if ("SCH01".equals(stTyCode)) {
			codeVO.setCodeId("CA0014");
		} else if ("SCH02".equals(stTyCode)) {
			codeVO.setCodeId("CA0015");
		} else if ("SCH03".equals(stTyCode)) {
			codeVO.setCodeId("CA0016");
		}

		model.addAttribute("schoolList", cmmUseService.selectCmmCodeDetail(codeVO));

		return "prg/dat/schoolList";
	}


	/**
	 * Nice 안심체크 검사check
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/NiceNameCheck.do")
	public String niceNameCheck(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		NiceID.Check.CPClient niceCheck = new NiceID.Check.CPClient();

		String sEncodeData = request.getParameter("enc_data");

		String sSiteCode = "G4576";                    // NICE신용평가정보로부터 부여받은 사이트 코드
		String sSitePassword = "NFTNGQXLLC2L";            // NICE신용평가정보부터 부여받은 사이트 패스워드

		String sCipherTime = "";                        // 복호화한 시간
		String sRequestNO = "";                        // 요청 번호
		String sPlainData = "";

		String sMessage = "";
		String sResult = "";

		int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

		if (iReturn == 0) {
			sMessage = "본인인증 성공하였습니다.";
			sPlainData = niceCheck.getPlainData();
			sCipherTime = niceCheck.getCipherDateTime();

			// 데이타를 추출합니다.
			java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

			sRequestNO = (String) mapresult.get("REQ_SEQ");
			sResult = (String) mapresult.get("NC_RESULT");

//			System.out.println("[실명확인결과 : " + mapresult.get("NC_RESULT") + "]<br>");
//			System.out.println("[이름 : " + mapresult.get("NAME") + "]<br>");
//			System.out.println("[안심KEY :" + mapresult.get("SAFEID") + "]<br>");
//			System.out.println("[요청고유번호 : " + sRequestNO + "]<br>");
//			System.out.println("[RESERVED1 : " + mapresult.get("RESERVED1") + "]<br>");
//			System.out.println("[RESERVED2 : " + mapresult.get("RESERVED2") + "]<br>");
//			System.out.println("[RESERVED3 : " + mapresult.get("RESERVED3") + "]<br>");
//			System.out.println("[IPIN_DI : " + mapresult.get("IPIN_DI") + "]<br>");
//			System.out.println("[IPIN_CI : " + mapresult.get("IPIN_CI") + "]<br>");

			String ncResult = (String) mapresult.get("NC_RESULT");
			String name = (String) mapresult.get("NAME");
			String safeId = (String) mapresult.get("SAFEID");
			String birth = safeId.substring(0, 6);
			String MandW = safeId.substring(6, 7);
			String sex = "";
			String foreigner = "";
			if ("1".equals(MandW) || "3".equals(MandW)) {
				sex = "M";
				foreigner = "N";
			} else if ("2".equals(MandW) || "4".equals(MandW)) {
				sex = "W";
				foreigner = "N";
			} else if ("5".equals(MandW) || "7".equals(MandW)) {
				sex = "M";
				foreigner = "Y";
			} else if ("6".equals(MandW) || "8".equals(MandW)) {
				sex = "W";
				foreigner = "Y";
			} else {
			}
			//String srequestNo = sRequestNO;
			//String reserved1 = (String)mapresult.get("RESERVED1");
			//String reserved2 = (String)mapresult.get("RESERVED2");
			//String reserved3 = (String)mapresult.get("RESERVED3");
			String ipinDi = (String) mapresult.get("IPIN_DI");
			//String ipinCi = (String)mapresult.get("IPIN_CI");

			String session_sRequestNumber = (String) request.getAttribute("REQ_SEQ");
			if (!sRequestNO.equals(session_sRequestNumber)) {
				sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
				sRequestNO = "";
			}

			if ("1".equals(ncResult)) {
				/**
				 model.addAttribute("ncResult", ncResult);
				 model.addAttribute("name", name);
				 model.addAttribute("birth", birth);
				 model.addAttribute("ipinDi", ipinDi);
				 model.addAttribute("sex", sex);
				 model.addAttribute("foreigner", foreigner);
				 model.addAttribute("MandW", MandW);

				 EgovSessionCookieUtil.setSessionAttribute(request, "dupInfo", ipinDi);
				 EgovSessionCookieUtil.setSessionAttribute(request, "realName", name);
				 EgovSessionCookieUtil.setSessionAttribute(request, "birthDate", birth);
				 EgovSessionCookieUtil.setSessionAttribute(request, "sex", sex);
				 EgovSessionCookieUtil.setSessionAttribute(request, "foreigner", foreigner);
				 **/

				String oldSecId = "g-" + Sha1Util.getHashHexString(mapresult.get("NAME") + birth + "9999999").substring(1, 18);
				LoginVO UserPrivateCert = new LoginVO();
				UserPrivateCert.setName(name);
				UserPrivateCert.setCredtId(ipinDi);
				UserPrivateCert.setOldSecId(oldSecId);
				UserPrivateCert.setBirthDate(birth);
				UserPrivateCert.setGenDer(sex);
				UserPrivateCert.setUserSe("01");
				//개인인증정보 세션 생성
				EgovSessionCookieUtil.setSessionAttribute(request, "UserPrivateCert", UserPrivateCert);
			}


		} else if (iReturn == -1) {
			sMessage = "복호화 시스템 에러입니다.";
		} else if (iReturn == -4) {
			sMessage = "복호화 처리오류입니다.";
		} else if (iReturn == -5) {
			sMessage = "복호화 해쉬 오류입니다.";
		} else if (iReturn == -6) {
			sMessage = "복호화 데이터 오류입니다.";
		} else if (iReturn == -9) {
			sMessage = "입력 데이터 오류입니다.";
		} else if (iReturn == -12) {
			sMessage = "사이트 패스워드 오류입니다.";
		} else {
			sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
		}

		return "cmm/sec/rnc/EgovRlnmCnfirmChk";
	}


	/**
	 * Nice 본인 인증 서비스 결과 처리
	 *
	 * @param searchVO
	 * @param userManageVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/NicePrivateCert.do")
	public String nicePrivateCert(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws Exception {

		NiceID.Check.CPClient niceCheck = new NiceID.Check.CPClient();

		String sEncodeData = requestReplace(request.getParameter("EncodeData"), "encodeData");
		String sReserved1 = requestReplace(request.getParameter("param_r1"), "");
		String sReserved2 = requestReplace(request.getParameter("param_r2"), "");
		String sReserved3 = requestReplace(request.getParameter("param_r3"), "");

		String sSiteCode = "G4576";                   // NICE로부터 부여받은 사이트 코드
		String sSitePassword = "NFTNGQXLLC2L";             // NICE로부터 부여받은 사이트 패스워드

		String sCipherTime = "";                 // 복호화한 시간
		String sRequestNumber = "";             // 요청 번호
		String sResponseNumber = "";         // 인증 고유번호
		String sAuthType = "";                   // 인증 수단
		String sName = "";                             // 성명
		String sDupInfo = "";                         // 중복가입 확인값 (DI_64 byte)
		String sConnInfo = "";                     // 연계정보 확인값 (CI_88 byte)
		String sBirthDate = "";                     // 생일
		String sGender = "";                         // 성별
		String sNationalInfo = "";       // 내/외국인정보 (개발가이드 참조)
		String sMessage = "";
		String sPlainData = "";

		int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

		if (iReturn == 0) {
			sPlainData = niceCheck.getPlainData();
			sCipherTime = niceCheck.getCipherDateTime();

			// 데이타를 추출합니다.
			java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

			sRequestNumber = (String) mapresult.get("REQ_SEQ");
			sResponseNumber = (String) mapresult.get("RES_SEQ");
			sAuthType = (String) mapresult.get("AUTH_TYPE");
			sName = (String) mapresult.get("NAME");
			sBirthDate = (String) mapresult.get("BIRTHDATE");
			sGender = (String) mapresult.get("GENDER");
			sNationalInfo = (String) mapresult.get("NATIONALINFO");
			sDupInfo = (String) mapresult.get("DI");
			sConnInfo = (String) mapresult.get("CI");
			// 휴대폰 번호 : MOBILE_NO => (String)mapresult.get("MOBILE_NO");
			// 이통사 정보 : MOBILE_CO => (String)mapresult.get("MOBILE_CO");
			// checkplus_success 페이지에서 결과값 null 일 경우, 관련 문의는 관리담당자에게 하시기 바랍니다.
			String session_sRequestNumber = (String) session.getAttribute("REQ_SEQ");
//
//			System.out.println(sName);
//			System.out.println(sBirthDate);
//			System.out.println(session_sRequestNumber);
//			System.out.println(sResponseNumber);

			sMessage = "정상 처리되었습니다.";
			if (!sRequestNumber.equals(session_sRequestNumber)) {
				sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";

				sResponseNumber = "";
				sAuthType = "";
			} else {
				String sex = "";
				if ("1".equals(sGender)) {
					sex = "M";
				} else {
					sex = "W";
				}
				String oldSecId = "g-" + Sha1Util.getHashHexString(mapresult.get("NAME") + sBirthDate + "9999999").substring(1, 18);
				LoginVO UserPrivateCert = new LoginVO();
				UserPrivateCert.setName(sName);
				UserPrivateCert.setCredtId(sDupInfo);
				UserPrivateCert.setOldSecId(oldSecId);
				UserPrivateCert.setBirthDate(sBirthDate);
				UserPrivateCert.setGenDer(sex);
				UserPrivateCert.setUserSe("01");
				//개인인증정보 세션 생성
				EgovSessionCookieUtil.setSessionAttribute(request, "UserPrivateCert", UserPrivateCert);

				String memJoin = (String) session.getAttribute("memJoin");
				if (memJoin != null){
					userManageVO.setCredtId(sDupInfo);
					userManageVO = userManageService.selectUser2(userManageVO);
					if(userManageVO != null){
						sMessage = sName + "님은 이미 가입 되었습니다.";
						model.addAttribute("retUrl", "/uat/uia/egovLoginUsr.do");
					}
					EgovSessionCookieUtil.removeSessionAttribute(request, "memJoin");
				}

				String idFind = (String) session.getAttribute("idFind");
				String pwFind = (String) session.getAttribute("pwFind");
				if (idFind != null || pwFind != null) {
					userManageVO.setCredtId(sDupInfo);
					userManageVO = userManageService.selectUser2(userManageVO);
					if(userManageVO != null && userManageVO.getUserId() != null){
						String userId = userManageVO.getUserId();
						String tempId = "";
						for (int i = 0; i < userId.length(); i++) {
							if (i > 2) {
								tempId += "*";
							} else {
								tempId += Character.toString(userId.charAt(i));
							}
						}
						userManageVO.setTempId(tempId);
						EgovSessionCookieUtil.setSessionAttribute(request, "userVo", userManageVO);
					}else{
						EgovSessionCookieUtil.removeSessionAttribute(request, "userVo");
						EgovSessionCookieUtil.removeSessionAttribute(request, "idFind");
						EgovSessionCookieUtil.removeSessionAttribute(request, "pwFind");
						EgovSessionCookieUtil.removeSessionAttribute(request, "UserPrivateCert");
						sMessage = "죄송합니다. " + sName + "님의 정보로 등록된 회원정보가 없습니다.";
						model.addAttribute("retUrl", "/index.do");
					}
				}
				if (idFind != null) {
					model.addAttribute("retUrl", "/uat/uia/memberIdFind.do");
				} else if (pwFind != null) {
					model.addAttribute("retUrl", "/uat/uia/memberPasswordFind.do");
				}
			}
		} else if (iReturn == -1) {
			sMessage = "복호화 시스템 에러입니다.";
		} else if (iReturn == -4) {
			sMessage = "복호화 처리오류입니다.";
		} else if (iReturn == -5) {
			sMessage = "복호화 해쉬 오류입니다.";
		} else if (iReturn == -6) {
			sMessage = "복호화 데이터 오류입니다.";
		} else if (iReturn == -9) {
			sMessage = "입력 데이터 오류입니다.";
		} else if (iReturn == -12) {
			sMessage = "사이트 패스워드 오류입니다.";
		} else {
			sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
		}
		model.addAttribute("sMessage", sMessage);
		return "cmm/sec/rnc/EgovRlnmCnfirmChk";
	}

	/**
	 * Nice 안심체크 검사check
	 *
	 * @param model 화면모델
	 * @return forward:/
	 * @throws Exception
	 */
	@RequestMapping(value = "/uss/umt/cmm/NiceIpinCheck.do")
	public String niceIpinCheck(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws Exception {


		/********************************************************************************************************************************************
		 NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

		 서비스명 : 가상주민번호서비스 (IPIN) 서비스
		 페이지명 : 가상주민번호서비스 (IPIN) 결과 페이지
		 *********************************************************************************************************************************************/

		String sSiteCode = "H079";            // IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
		String sSitePw = "35416143";            // IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)


		// 사용자 정보 및 CP 요청번호를 암호화한 데이타입니다.
		String sResponseData = requestReplace(request.getParameter("enc_data"), "encodeData");

		// CP 요청번호 : ipin_main.jsp 에서 세션 처리한 데이타
		String sCPRequest = (String) request.getAttribute("CPREQUEST");


		// 객체 생성
		IPINClient pClient = new IPINClient();
	
	
	/*
	┌ 복호화 함수 설명  ──────────────────────────────────────────────────────────
		Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
		
		fnResponse 함수는 결과 데이타를 복호화 하는 함수이며,
		'sCPRequest'값을 추가로 보내시면 CP요청번호 일치여부도 확인하는 함수입니다. (세션에 넣은 sCPRequest 데이타로 검증)
		
		따라서 귀사에서 원하는 함수로 이용하시기 바랍니다.
	└────────────────────────────────────────────────────────────────────
	*/
		int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData);
		//int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData, sCPRequest);

		String sRtnMsg = "";                            // 처리결과 메세지
		String sVNumber = pClient.getVNumber();            // 가상주민번호 (13자리이며, 숫자 또는 문자 포함)
		String sName = pClient.getName();            // 이름
		String sDupInfo = pClient.getDupInfo();            // 중복가입 확인값 (DI - 64 byte 고유값)
		String sAgeCode = pClient.getAgeCode();            // 연령대 코드 (개발 가이드 참조)
		String sGenderCode = pClient.getGenderCode();        // 성별 코드 (개발 가이드 참조)
		String sBirthDate = pClient.getBirthDate();        // 생년월일 (YYYYMMDD)
		String sNationalInfo = pClient.getNationalInfo();    // 내/외국인 정보 (개발 가이드 참조)
		String sCPRequestNum = pClient.getCPRequestNO();        // CP 요청번호

		String birth = sBirthDate.substring(0, 8);

		//Mand
		String MandW = "GPIN";
		String sex = "";
		String foreigner = "";
		// Method 결과값에 따른 처리사항
		if (iRtn == 1) {
		/*
			다음과 같이 사용자 정보를 추출할 수 있습니다.
			사용자에게 보여주는 정보는, '이름' 데이타만 노출 가능합니다.
		
			사용자 정보를 다른 페이지에서 이용하실 경우에는
			보안을 위하여 암호화 데이타(sResponseData)를 통신하여 복호화 후 이용하실것을 권장합니다. (현재 페이지와 같은 처리방식)
			
			만약, 복호화된 정보를 통신해야 하는 경우엔 데이타가 유출되지 않도록 주의해 주세요. (세션처리 권장)
			form 태그의 hidden 처리는 데이타 유출 위험이 높으므로 권장하지 않습니다.
		*/

			// 사용자 인증정보에 대한 변수
			//			System.out.println("가상주민번호 : " + sVNumber + "<BR>");
			//			System.out.println("이름 : " + sName + "<BR>");
			//			System.out.println("중복가입 확인값 (DI) : " + sDupInfo + "<BR>");
			//			System.out.println("연령대 코드 : " + sAgeCode + "<BR>");
			//			System.out.println("성별 코드 : " + sGenderCode + "<BR>");
			//			System.out.println("생년월일 : " + sBirthDate + "<BR>");
			//			System.out.println("내/외국인 정보 : " + sNationalInfo + "<BR>");
			//			System.out.println("CP 요청번호 : " + sCPRequestNum + "<BR>");
			//			System.out.println("***** 복호화 된 정보가 정상인지 확인해 주시기 바랍니다.<BR><BR><BR><BR>");

			if ("1".equals(sGenderCode)) {
				sex = "M";
			} else {
				sex = "W";
			}
			if ("0".equals(sNationalInfo)) {
				foreigner = "N";
			} else {
				foreigner = "Y";
			}


			/**
			 model.addAttribute("ncResult", iRtn);
			 model.addAttribute("name", sName);
			 model.addAttribute("birth", birth);
			 model.addAttribute("ipinDi", sDupInfo);
			 model.addAttribute("sex", sex);
			 model.addAttribute("foreigner", foreigner);
			 model.addAttribute("MandW", MandW);
			 EgovSessionCookieUtil.setSessionAttribute(request, "dupInfo", sDupInfo);
			 EgovSessionCookieUtil.setSessionAttribute(request, "realName", sName);
			 EgovSessionCookieUtil.setSessionAttribute(request, "birthDate", birth);
			 EgovSessionCookieUtil.setSessionAttribute(request, "sex", sex);
			 EgovSessionCookieUtil.setSessionAttribute(request, "foreigner", foreigner);
			 **/

			String oldSecId = "g-" + Sha1Util.getHashHexString(pClient.getName() + birth + "9999999").substring(1, 18);
			LoginVO UserPrivateCert = new LoginVO();
			UserPrivateCert.setName(sName);
			UserPrivateCert.setCredtId(sDupInfo);
			UserPrivateCert.setOldSecId(oldSecId);
			UserPrivateCert.setBirthDate(birth);
			UserPrivateCert.setGenDer(sex);
			UserPrivateCert.setUserSe("01");

			//개인인증정보 세션 생성
			EgovSessionCookieUtil.setSessionAttribute(request, "UserPrivateCert", UserPrivateCert);

			sRtnMsg = "정상 처리되었습니다.";

			String memJoin = (String) session.getAttribute("memJoin");
			if (memJoin != null){
				userManageVO.setCredtId(sDupInfo);
				userManageVO = userManageService.selectUser2(userManageVO);
				if(userManageVO != null){
					sRtnMsg = sName + "님은 이미 가입 되었습니다.";
					model.addAttribute("retUrl", "/uat/uia/egovLoginUsr.do");
				}
				EgovSessionCookieUtil.removeSessionAttribute(request, "memJoin");
			}

			String idFind = (String) session.getAttribute("idFind");
			String pwFind = (String) session.getAttribute("pwFind");

			if (idFind != null || pwFind != null) {
				userManageVO.setCredtId(sDupInfo);
				userManageVO = userManageService.selectUser2(userManageVO);
				if(userManageVO != null && userManageVO.getUserId() != null){
					String userId = userManageVO.getUserId();
					String tempId = "";
					for (int i = 0; i < userId.length(); i++) {
						if (i > 2) {
							tempId += "*";
						} else {
							tempId += Character.toString(userId.charAt(i));
						}
					}
					userManageVO.setTempId(tempId);
					EgovSessionCookieUtil.setSessionAttribute(request, "userVo", userManageVO);
				}else{
					EgovSessionCookieUtil.removeSessionAttribute(request, "userVo");
					EgovSessionCookieUtil.removeSessionAttribute(request, "idFind");
					EgovSessionCookieUtil.removeSessionAttribute(request, "pwFind");
					EgovSessionCookieUtil.removeSessionAttribute(request, "UserPrivateCert");
					sRtnMsg = "죄송합니다. " + sName + "님의 정보로 등록된 회원정보가 없습니다.";
					model.addAttribute("retUrl", "/index.do");
				}
			}
			if (idFind != null) {
				model.addAttribute("retUrl", "/uat/uia/memberIdFind.do");
			} else if (pwFind != null) {
				model.addAttribute("retUrl", "/uat/uia/memberPasswordFind.do");
			}


		} else if (iRtn == -1 || iRtn == -4) {
			sRtnMsg = "iRtn 값, 서버 환경정보를 정확히 확인하여 주시기 바랍니다.";
		} else if (iRtn == -6) {
			sRtnMsg = "당사는 한글 charset 정보를 euc-kr 로 처리하고 있으니, euc-kr 에 대해서 허용해 주시기 바랍니다.<BR>" + "한글 charset 정보가 명확하다면 ..<BR><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
		} else if (iRtn == -9) {
			sRtnMsg = "입력값 오류 : fnResponse 함수 처리시, 필요한 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
		} else if (iRtn == -12) {
			sRtnMsg = "CP 비밀번호 불일치 : IPIN 서비스 사이트 패스워드를 확인해 주시기 바랍니다.";
		} else if (iRtn == -13) {
			sRtnMsg = "CP 요청번호 불일치 : 세션에 넣은 sCPRequest 데이타를 확인해 주시기 바랍니다.";
		} else {
			sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 전산 담당자에게 문의해 주세요.";
		}
		model.addAttribute("sMessage", sRtnMsg);
		return "cmm/sec/rnc/EgovRlnmCnfirmChk";
	}

	/**
	 * 아이디 찾기 결과 화면
	 *
	 * @return
	 */
	@RequestMapping(value = "/uat/uia/memberIdFind.do")
	public String memberIdFind() {
		return "cmm/uat/uia/memberIdFind.tiles";
	}


	/**
	 * 비밀번호 찾기 결과 화면
	 *
	 * @return
	 */
	@RequestMapping(value = "/uat/uia/memberPasswordFind.do")
	public String memberPasswordFind(HttpServletRequest request, HttpSession session, ModelMap model) {
		UserManageVO userLoginVo = (UserManageVO) session.getAttribute("userVo");
		if (userLoginVo != null) {
			return "cmm/uat/uia/memberPasswordFind.tiles";
		} else {
			model.addAttribute("message", "아이핀 및 본인인증을 하셔야 합니다.");
			return "redirect:/uat/uia/egovLoginUsr.do";
		}
	}

	/**
	 * 임시비밀번호 발급
	 *
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uat/uia/memberTempPassword.do")
	public String memberTempPassword(HttpServletRequest request, HttpSession session, ModelMap model) throws Exception {
		UserManageVO userLoginVo = (UserManageVO) session.getAttribute("userVo");
		if (userLoginVo != null) {
			String userId = request.getParameter("userId");

			//인증된 세션 정보와 입력된 ID 비교
			if(userId.equals(userLoginVo.getUserId())){
				LoginVO loginVO = new LoginVO();
				loginVO.setDn(userLoginVo.getCredtId());
				loginVO.setId(userLoginVo.getUserId());
				loginVO = loginService.searchPasswordRe(loginVO);
				EgovSessionCookieUtil.setSessionAttribute(request, "tmpPw", loginVO.getTempPass());

				//임시비밀번호 메일 발송
				MailVo mailVo=new MailVo();
				mailVo.setTo_mail(userLoginVo.getEmailAdres());
				mailVo.setSubject("[녹색자금 통합관리시스템] 임시비밀번호 발급서비스");
				StringBuffer sb=new StringBuffer();
				sb.append("<div style=\"width:650px;font-family:Nanum Gothic, 나눔고딕\"><div style=\"border-bottom:5px solid #81be4c;margin-bottom:10px;\"><img src=\"http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.gif\" alt=\"\" /></div>");
				sb.append("<div style=\"border:1px solid #e7e7e7;padding:20px 20px 40px;\"><div style=\"padding:90px 180px 0 0;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/bg_img.gif') 430px 30px no-repeat;height:147px;color:#000;font-weight:bold;font-size:22px;line-height:28px;\">임시비밀번호를 알려 드립니다.</div>");
				sb.append("<div style=\"padding:0 0 10px 20px;color:#3b3b3b;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon.gif') 0 3px no-repeat;font-size:16px;\">요청하신 <strong style=\"color:#006bb8;font-weight:normal;\">임시비밀번호</strong>가 발급되었습니다.</div>");
				sb.append("<div style=\"border:5px solid #e7e7e7;background:#fff;padding:5px;\"><ul style=\"margin:0;padding:20px;color:#727272;\">");
				sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">아이디 : " + userLoginVo.getUserId() + "</li>");
				sb.append("<li style=\"list-style:none;padding:0 0 0px 15px;margin-bottom:5px;background:url('http://kgpa.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_icon02.gif') 0 center no-repeat;\">비밀번호 : " + loginVO.getTempPass() + "</li></ul></div></div></div>");
				mailVo.setContent(sb.toString());
				SendMail.sendEmail(mailVo);

				return "cmm/uat/uia/memberTempPassword.tiles";
			}else{
				model.addAttribute("message", "입력하신 아이디와 아이핀 및 본인인증으로 조회된 회원정보와 동일하지 않습니다.");
				return "cmm/uat/uia/memberPasswordFind.tiles";
			}
		} else {
			model.addAttribute("message", "아이핀 및 본인인증을 하셔야 합니다.");
			return "redirect:/uat/uia/egovLoginUsr.do";
		}
	}


	public static String requestReplace(String paramValue, String gubun) {
		String result = "";
		if (paramValue != null) {
			paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
			paramValue = paramValue.replaceAll("\\*", "");
			paramValue = paramValue.replaceAll("\\?", "");
			paramValue = paramValue.replaceAll("\\[", "");
			paramValue = paramValue.replaceAll("\\{", "");
			paramValue = paramValue.replaceAll("\\(", "");
			paramValue = paramValue.replaceAll("\\)", "");
			paramValue = paramValue.replaceAll("\\^", "");
			paramValue = paramValue.replaceAll("\\$", "");
			paramValue = paramValue.replaceAll("'", "");
			paramValue = paramValue.replaceAll("@", "");
			paramValue = paramValue.replaceAll("%", "");
			paramValue = paramValue.replaceAll(";", "");
			paramValue = paramValue.replaceAll(":", "");
			paramValue = paramValue.replaceAll("-", "");
			paramValue = paramValue.replaceAll("#", "");
			paramValue = paramValue.replaceAll("--", "");
			paramValue = paramValue.replaceAll("-", "");
			paramValue = paramValue.replaceAll(",", "");

			if (gubun != "encodeData") {
				paramValue = paramValue.replaceAll("\\+", "");
				paramValue = paramValue.replaceAll("/", "");
				paramValue = paramValue.replaceAll("=", "");
			}

			result = paramValue;

		}
		return result;
	}


	/**
	 * 녹색자금통합시스템 사용자 보존기간 체크 팝업
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/gfund/retentionTimeAlerts.do")
	public String retentionTimeAlerts(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if (loginVO != null) {
			UserManageVO userVo=new UserManageVO();
			userVo.setUserId(loginVO.getId());
			int monthRs = userManageService.selectGfundRetentionPeriodMonthUser(userVo);
			if(monthRs > 0){
				model.addAttribute("rs", "true");
			}
		}
		return "gfund/cmm/retentionTimeAlerts";
	}
	@RequestMapping(value = "/gfund/retentionTimeAlertsPopup.do")
	public String retentionTimeAlertsPopup() throws Exception {
		return "gfund/cmm/retentionTimeAlertsPopup";
	}

	/**
	 * 테스트페이지
	 *
	 * @param model 화면모델
	 * @return cmm/uss/umt/EgovTestPage
	 * @throws Exception
	 */

	@RequestMapping("/uss/umt/cmm/userInfoTest.do")
	public String userInfoTest(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {

		//실명인증 회원정보
		String oldSecId = "g-" + Sha1Util.getHashHexString("정병열198710319999999").substring(1, 18);
		LoginVO UserPrivateCert = new LoginVO();
		UserPrivateCert.setName("정병열");
		UserPrivateCert.setCredtId("MC0GCCqGSIb3DQIJAyEA94M8WQBSeJeKH+w8DkSzJeZQ2QoBmz6aa949c5uLvZs=");
		UserPrivateCert.setOldSecId(oldSecId);
		UserPrivateCert.setBirthDate("19871031");
		UserPrivateCert.setGenDer("M");
		UserPrivateCert.setUserSe("01");
		//개인인증정보 세션 생성
		EgovSessionCookieUtil.setSessionAttribute(request, "UserPrivateCert", UserPrivateCert);

		return "cmm/sec/rnc/EgovRlnmCnfirmChk";
	}

}
