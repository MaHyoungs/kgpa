package egovframework.com.utl.fcc.service;


import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public class CurrUrlUtil{
	/**
	 * 현재 접근한 Url 리턴
	 * @param request
	 * @return
	 */
	public static String currRetunUrl(HttpServletRequest request){

		String return_url = request.getRequestURL().toString();

		Enumeration param = request.getParameterNames();
		String strParam = "";
		while(param.hasMoreElements()) {
			String name = (String)param.nextElement();
			String value = request.getParameter(name);
			if(value != null){
				strParam += name + "=" + getParameter(value) + "&";
			}
		}
		if(strParam != ""){
			strParam = strParam.substring(0, (strParam.length() - 1));
			return_url += "?" + strParam;
		}
		return return_url;
	}
	/**
	 * 현재 접근한 Url 파라미턴만 리턴
	 * @param request
	 * @return
	 */
	public static String currRetunUrlParameter(HttpServletRequest request){

		String return_url = "";

		Enumeration param = request.getParameterNames();
		String strParam = "";
		while(param.hasMoreElements()) {
			String name = (String)param.nextElement();
			String value = request.getParameter(name);
			if(value != null){
				strParam += name + "=" + getParameter(value) + "&";
			}
		}
		if(strParam != ""){
			return_url += "?" + strParam.substring(0, strParam.length()-1);
		}
		return return_url;
	}

	public static String getParameter(String parameter) {

		String value = parameter;

		if (value == null) {
			return null;
		}

		StringBuffer strBuff = new StringBuffer();

		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			switch (c) {
				case '<':
					strBuff.append("&lt;");
					break;
				case '>':
					strBuff.append("&gt;");
					break;
				case '&':
					strBuff.append("&amp;");
					break;
				case '"':
					strBuff.append("&quot;");
					break;
				case '\'':
					strBuff.append("&apos;");
					break;
				default:
					strBuff.append(c);
					break;
			}
		}

		value = strBuff.toString();

		return value;
	}
}