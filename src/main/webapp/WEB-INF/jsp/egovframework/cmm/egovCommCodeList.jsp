<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:choose>
    <c:when test="${param.elType eq 'radio'}">
      <c:forEach var="rs" items="${cmmCdList}" varStatus="sts">
        <c:choose>
            <c:when test="${not empty param.elStart and not empty param.elEnd}">
              <c:if test="${param.elStart <= sts.count and param.elEnd >= sts.count}">
                <input type="radio" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq param.elStart}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
              </c:if>
            </c:when>
            <c:otherwise>
                  <input type="radio" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq 1}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
            </c:otherwise>
        </c:choose>
      </c:forEach>
    </c:when>
    <c:when test="${param.elType eq 'checkbox'}">
      <c:forEach var="rs" items="${cmmCdList}" varStatus="sts">
        <c:choose>
            <c:when test="${not empty param.elStart and not empty param.elEnd}">
              <c:if test="${param.elStart <= sts.count and param.elEnd >= sts.count}">
                <input type="checkbox" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq param.elStart}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
              </c:if>
            </c:when>
            <c:otherwise>
                  <input type="radio" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq 1}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
            </c:otherwise>
        </c:choose>
      </c:forEach>
    </c:when>
    <c:when test="${param.elType eq 'select'}">
      <select name="${param.elName}" class="${param.elClass}">
          <c:if test="${not empty param.emptyMsg}">
              <option value="" <c:if test="${empty param.chVal and not empty param.elFast}"> selected="selected"</c:if>>${param.emptyMsg}</option>
          </c:if>
      <c:forEach var="rs" items="${cmmCdList}" varStatus="sts">
        <option value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> selected="selected"</c:if> <c:if test="${empty param.chVal and sts.index eq 0 and empty param.elFast}"> selected="selected"</c:if>>${rs.codeNm}</option>
      </c:forEach>
      </select>
    </c:when>
    <c:when test="${param.elType eq 'codeName'}">
      <c:forEach var="rs" items="${cmmCdList}" varStatus="sts">
          <c:if test="${param.chVal eq rs.code}">
          ${rs.codeNm}
          </c:if>
      </c:forEach>
    </c:when>
    <c:when test="${param.elType eq 'radioWithText'}">
        <c:forEach var="rs" items="${cmmCdList}" varStatus="sts">
            <c:choose>
                <c:when test="${not empty param.elStart and not empty param.elEnd}">
                    <c:if test="${param.elStart <= sts.count and param.elEnd >= sts.count}">
                        <input type="radio" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq param.elStart}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <input type="radio" name="${param.elName}" id="${param.elName}_${sts.count}" value="${rs.code}" <c:if test="${param.chVal eq rs.code}"> checked="checked"</c:if> <c:if test="${empty param.chVal and sts.count eq 1}"> checked="checked"</c:if> ><label for="${param.elName}_${sts.count}">${rs.codeNm}</label>
                    <c:if test="${param.txtPosition eq sts.count}">
                        <c:choose>
                            <c:when test="${param.chVal eq rs.code}">
                                ( <input type="text" name="${param.txtElName}" id="${param.txtElName}" value="${param.txtChVal}" class="${param.classN}inp" title="기타"/> <c:if test="${not empty param.txtEtc}"> ${param.txtEtc}</c:if> )
                            </c:when>
                            <c:otherwise>
                                ( <input type="text" name="${param.txtElName}" id="${param.txtElName}" value="" class="${param.classN}inp" title="기타"/> <c:if test="${not empty param.txtEtc}"> ${param.txtEtc}</c:if> )
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </c:when>
</c:choose>
