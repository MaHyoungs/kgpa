<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
	String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>

<c:if test="${param.formAjaxJs2 eq 'add'}">
	<script type="text/javascript" src="/template/common/js/jquery/jquery.form.min.js"></script>
</c:if>
<script type="text/javascript" src="/template/mobile/js/chckupload.js"></script>

<div class="file_box2">

	<p class="file_info2">
		<strong class="blue">개수 : </strong><span id="lblCurrCount2">0</span>/<span id="lblMaxCount2">0</span> ,
		<strong class="blue">크기 : </strong><span id="lblCurrSize2">0MB</span>/<span id="lblMaxSize2">0MB</span>
	</p>

	<table class="file_list_chart2" summary="지도점검표의 첨부파일 목록을 나타낸표로 파일명, 크기, 삭제 항목을 나타낸표입니다 "  >
		<caption>첨부파일목록</caption>
		<colgroup>
			<col width="*" />
			<col width="150" />
			<col width="30" />
		</colgroup>
		<tbody id="multiFileList">
		<tr id="tr_file_empty" <c:if test="${fn:length(fileList) ne '0'}">style="display:none"</c:if>>
			<c:choose>
				<c:when test="${param.updateFlag2=='Y'}">
					<td colspan="3" align="center">첨부된 파일이 없습니다.</td>
				</c:when>
				<c:otherwise>
					<td colspan="2" align="center">첨부된 파일이 없습니다.</td>
				</c:otherwise>
			</c:choose>
		</tr>
		<c:forEach var="fileVO" items="${fileList}" varStatus="status">
			<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
				<c:choose>
					<c:when test="${param.updateFlag2=='Y'}">
						<c:url var="downLoad" value="/cmm/fms/FileDown.do">
							<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
							<c:param name="fileSn" value="${fileVO.fileSn}"/>
						</c:url>
						<td>
							<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">								
								<c:out value="${fileVO.orignlFileNm}"/>
							</a>
						</td>
						<td class="size"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
						<td class="del">
							<a href="/cmm/fms/ajaxDeleteFileInfs.do" onclick="fnAjaxFileDel2('<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>', this);return false;">
								<img src="${CMMN_IMG }/btn_sdelete.gif"/>
							</a>
						</td>
						<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
						<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
					</c:when>
					<c:otherwise>
						<c:url var="downLoad" value="/cmm/fms/FileDown.do">
							<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
							<c:param name="fileSn" value="${fileVO.fileSn}"/>
						</c:url>
						<td>
							<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
								<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
							</a>
						</td>
						<td class="size" colspan="2"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
		</tbody>
	</table>

	<c:if test="${param.updateFlag2=='Y'}">
		<div class="file_top2">
			<form name="chckFileAjaxForm" id="chckFileAjaxForm" method="post" enctype="multipart/form-data" action="/cmm/fms/ajaxchckUploadFile.do">
				<input type="file" name="uploadfile2" id="uploadfile2" onchange="fnAjaxFileUploadChangeEvent2();" title="첨부파일 찾기" />
				<input type="hidden" name="pathKey2"                             value="${param.pathKey2}" />
				<input type="hidden" name="appendPath2"                          value="${param.appendPath2}" />
				<input type="hidden" name="ctt_atch_file_id"		id="gcttAtchFileId"	value="" />
				<input type="hidden" id="currSize2"  name="currSize2"     value="0" />
				<input type="hidden" id="maxSize2"   name="maxSize2"      value="${param.maxSize2}" />
				<input type="hidden" id="currCount2" name="currCount2"    value="0" />
				<input type="hidden" id="maxCount2"  name="maxCount2"     value="${param.maxCount2}" />
				<span class="cbtn1"><a class="c_click2">파일첨부</a></span>
				<div class="progressBox2">
					<div class="progress2 radius2">
						<div class="bar2">0%</div>
					</div>
				</div>
			</form>
		</div>
	</c:if>

</div>

<input type="hidden" id="fileCurrCount2" name="fileCurrCount2" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
<input type="hidden" id="fileCurrSize2" name="fileCurrSize2" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>