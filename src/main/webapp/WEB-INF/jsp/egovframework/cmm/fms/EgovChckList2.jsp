<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
	String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>

<c:if test="${param.formAjaxJs eq 'add'}">
	<script type="text/javascript" src="/template/common/js/jquery/jquery.form.min.js"></script>
</c:if>
<script type="text/javascript" src="/template/mobile/js/chckupload2.js"></script>
<div class="file_box">
	<c:choose>
		<c:when test="${param.style eq 'gfSelection'}">
			<ul class="file_list">
				<c:forEach var="fileVO" items="${fileList}" varStatus="status">
					<c:url var="downLoad" value="/cmm/fms/FileDown.do">
						<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
						<c:param name="fileSn" value="${fileVO.fileSn}"/>
						<c:choose>
							<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
							<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
							<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
							<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
						</c:choose>
						<c:choose>
							<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
						</c:choose>
					</c:url>
					<li>
						<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
							<c:out value="${fileVO.orignlFileNm}"/>
						</a>
					</li>
				</c:forEach>
				<c:if test="${empty fileList}">
					<li>첨부된 파일이 없습니다</li>
				</c:if>
			</ul>
		</c:when>
		<c:otherwise>
			<p class="file_info">
				<strong class="blue">개수 : </strong><span id="lblCurrCount">0</span>/<span id="lblMaxCount">0</span>
				<strong class="blue">크기 : </strong><span id="lblCurrSize">0MB</span>/<span id="lblMaxSize">0MB</span>
			</p>
			<table class="file_list_chart" summary="첨부파일 목록을 나타낸표로 파일명, 크기, 삭제 항목을 나타낸표입니다 "  >
				<caption>첨부파일목록</caption>
				<colgroup>
					<col width="200" />
					<col width="150" />
				</colgroup>
				<tbody id="multiFileList">
				<tr id="tr_file_empty" <c:if test="${fn:length(fileList) ne '0'}">style="display:none"</c:if>>
					<c:choose>
						<c:when test="${param.updateFlag=='Y'}">
							<td colspan="3" align="center">첨부된 파일이 없습니다.</td>
						</c:when>
						<c:otherwise>
							<td colspan="2" align="center">첨부된 파일이 없습니다.</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<c:forEach var="fileVO" items="${fileList}" varStatus="status">
					<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
						<c:choose>
							<c:when test="${param.updateFlag=='Y'}">
								<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
									<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
									<c:param name="fileSn" value="${fileVO.fileSn}"/>
									<c:param name="returnUrl" value="${CURR_URL}"/>
								</c:url>
								<c:url var="downLoad" value="/cmm/fms/FileDown.do">
									<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
									<c:param name="fileSn" value="${fileVO.fileSn}"/>
									<c:choose>
										<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
										<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
										<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
										<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
									</c:choose>
								</c:url>
								<td><%-- <img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> --%>
									<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
										<img src="<c:out value='${downLoad}'/>" height="60px" width="60px"/>
										<c:out value="${fileVO.orignlFileNm}"/>
									</a>
								</td>
								<td></td>
								<td class="del">
									<a href="/cmm/fms/ajaxDeleteFileInfs.do" onclick="fnAjaxFileDel('<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>', this);return false;">
										<img src="${CMMN_IMG }/btn_sdelete.gif"/>
									</a>
								</td>
								<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
								<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
							</c:when>
							<c:otherwise>
								<c:url var="downLoad" value="/cmm/fms/FileDown.do">
									<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
									<c:param name="fileSn" value="${fileVO.fileSn}"/>
									<c:choose>
										<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
										<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
										<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
										<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
									</c:choose>
								</c:url>
								<td>
									<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
										<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
									</a>
								</td>
								<td class="size" colspan="2"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>

	<c:if test="${param.updateFlag=='Y'}">
		<div class="file_top">
			<form name="boardFileAjaxForm" id="boardFileAjaxForm" method="post" enctype="multipart/form-data" action="/cmm/fms/ajaxBoardUploadFile.do">
				<input type="file" name="uploadfile" id="uploadfile" onchange="fnAjaxFileUploadChangeEvent();" title="첨부파일 찾기" />
				<input type="hidden" name="pathKey"                             value="${param.pathKey}" />
				<input type="hidden" name="appendPath"                          value="${param.appendPath}" />
				<input type="hidden" name="atchFileId"		id="gatchFileId"	value="" />
				<input type="hidden" id="currSize"  name="currSize"     value="0" />
				<input type="hidden" id="maxSize"   name="maxSize"      value="${param.maxSize}" />
				<input type="hidden" id="currCount" name="currCount"    value="0" />
				<input type="hidden" id="maxCount"  name="maxCount"     value="${param.maxCount}" />
				<span class="cbtn1"><a class="c_click">파일첨부</a></span>
				<div class="progressBox">
					<div class="progress radius">
						<div class="bar">0%</div>
					</div>
				</div>
			</form>
		</div>
	</c:if>
</div>

<input type="hidden" id="fileCurrCount" name="fileCurrCount" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
<input type="hidden" id="fileCurrSize" name="fileCurrSize" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>