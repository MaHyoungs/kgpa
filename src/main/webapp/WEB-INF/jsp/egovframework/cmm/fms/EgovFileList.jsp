<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>
<c:choose>
	<c:when test="${param.style eq 'gfund'}">
		<c:forEach var="fileVO" items="${fileList}" varStatus="status">
			<c:url var="downLoad" value="/cmm/fms/FileDown.do">
				<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
				<c:param name="fileSn" value="${fileVO.fileSn}"/>
				<c:choose>
					<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
					<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
					<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
					<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
				</c:choose>
				<c:choose>
					<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
				</c:choose>
			</c:url>
			<li><a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;"><c:out value="${fileVO.orignlFileNm}"/></a></li>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div class="file_box">
			<c:if test="${updateFlag=='Y'}">
				<div class="file_top">
					<c:if test="${param.potalService eq 'Y'}">
						<form name="boardFileAjaxForm" id="boardFileAjaxForm" method="post" enctype="multipart/form-data" action="/cmm/fms/ajaxBoardUploadFile.do">
							<input type="file" name="uploadfile" id="uploadfile" onchange="fnAjaxFileUploadChangeEvent();" title="첨부파일 찾기" />
							<input type="hidden" name="appendPath"                          value="${brdMstrVO.bbsId}" />
							<input type="hidden" name="atchFileId"		id="gatchFileId"	value="" />
							<input type="hidden" id="currSize"  name="currSize"     value="0" />
							<input type="hidden" id="maxSize"   name="maxSize"      value="0" />
							<input type="hidden" id="currCount" name="currCount"    value="0" />
							<input type="hidden" id="maxCount"  name="maxCount"     value="0" />
							<span class="bbtn_bg1"><a class="c_click">+ 파일첨부</a></span>
							<div class="progressBox">
								<div class="progress radius">
									<div class="bar">0%</div>
								</div>
							</div>
						</form>
					</c:if>
					<c:if test="${param.potalService ne 'Y'}">
						<a href="#" class="file_btn" id="btnAddFile_<c:out value="${param.editorId}"/>" style="display:none"><img src="${CMMN_IMG }/btn_add_file.gif" alt="파일첨부"/></a>
					</c:if>
					<p class="file_info">
						<strong class="blue">갯수 : </strong><span id="lblCurrCount_<c:out value="${param.editorId}"/>">0</span>/<span id="lblMaxCount_<c:out value="${param.editorId}"/>">0</span> ,
						<strong class="blue">크기 : </strong><span id="lblCurrSize_<c:out value="${param.editorId}"/>">0MB</span>/<span id="lblMaxSize_<c:out value="${param.editorId}"/>">0MB</span>
					</p>
				</div>
			</c:if>
			<table class="file_list_chart" summary="첨부파일 목록을 나타낸표로 파일명, 크기, 삭제 항목을 나타낸표입니다 "  >
				<caption>첨부파일목록</caption>
				<colgroup>
						<c:choose>
							<c:when test="${updateFlag=='Y'}">
								<col width="60%" />
								<col width="25%" />
								<col width="15%" />
							</c:when>
							<c:otherwise>
								<col width="80%" />
								<col width="20%" />
							</c:otherwise>
						</c:choose>
				</colgroup>
				<thead>
					<tr>
						<th scope="col">파일명</th>
						<c:choose>
							<c:when test="${updateFlag=='Y'}">
								<th class="size"  scope="col">크기</th>
								<th class="del"  scope="col">삭제</th>
							</c:when>
							<c:otherwise>
								<th class="size" colspan="2"  scope="col">크기</th>
							</c:otherwise>
						</c:choose>
					</tr>
				</thead>
				<tbody id="multiFileList_<c:out value="${param.editorId}"/>">
				<tr id="tr_file_empty_<c:out value="${param.editorId}"/>" <c:if test="${fn:length(fileList) ne '0'}">style="display:none"</c:if>>
					<c:choose>
						<c:when test="${updateFlag=='Y'}">
							<td colspan="3" align="center">첨부된 파일이 없습니다.</td>
						</c:when>
						<c:otherwise>
							<td colspan="2" align="center">첨부된 파일이 없습니다.</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<c:forEach var="fileVO" items="${fileList}" varStatus="status">
					<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
						<c:choose>
			               <c:when test="${updateFlag=='Y'}">
				                <c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
				                    <c:param name="atchFileId" value="${fileVO.atchFileId}"/>
									<c:param name="fileSn" value="${fileVO.fileSn}"/>
									<c:param name="returnUrl" value="${CURR_URL}"/>
				                </c:url>
								<td><img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/></td>
								<td class="size"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
								<td class="del">
									<c:if test="${param.potalService ne 'Y'}">
										<a href="<c:out value="${delUrl}"/>" onclick="fn_egov_editor_file_del('<c:out value="${param.editorId}"/>', '<c:out value="${param.estnAt}"/>', '<c:out value="${param.bbsId}"/>', '<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');return false;">
											<img src="${CMMN_IMG }/btn_sdelete.gif" alt="첨부파일 삭제" />
										</a>
									</c:if>
									<c:if test="${param.potalService eq 'Y'}">
										<a href="/cmm/fms/ajaxDeleteFileInfs.do" onclick="fnAjaxFileDel('<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>', this);return false;">
											<img src="${CMMN_IMG }/btn_sdelete.gif"/>
										</a>
									</c:if>
								</td>
								<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
								<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
			               </c:when>
			               <c:otherwise>
			                   <c:url var="downLoad" value="/cmm/fms/FileDown.do">
									<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
									<c:param name="fileSn" value="${fileVO.fileSn}"/>
									<c:choose>
										<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
										<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
										<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
										<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
									</c:choose>
									<c:choose>
										<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
									</c:choose>
			                   </c:url>
								<td>
									<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
										<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
									</a>
								</td>
								<td class="size" colspan="2"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
			               </c:otherwise>
						</c:choose>
					</tr>
		        </c:forEach>
				</tbody>
			</table>
		</div>

		<input type="hidden" id="fileGroupId_<c:out value="${param.editorId}"/>" name="fileGroupId_<c:out value="${param.editorId}"/>" value="<c:out value="${param.param_atchFileId}"/>"/>
		<input type="hidden" id="fileCurrCount_<c:out value="${param.editorId}"/>" name="fileCurrCount_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
		<input type="hidden" id="fileCurrSize_<c:out value="${param.editorId}"/>" name="fileCurrSize_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>
	</c:otherwise>
</c:choose>
