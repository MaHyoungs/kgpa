<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<% 
 /** 
  * @Class Name : EgovImgFileList.jsp
  * @Description : 이미지 파일 조회화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.31  이삼섭          최초 생성
  *
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.31
  *  @version 1.0
  *  @see
  *  
  */
%>
<ul>
	<c:forEach var="fileVO" items="${fileList}" varStatus="status">
		<li style="text-align: center; margin-bottom: 10px;">
			<c:url var="imgUrl" value="/cmm/fms/getImageEx.do">
				<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
				<c:param name="fileSn" value="${fileVO.fileSn}"/>
				<c:param name="siteId" value="${param.siteId}"/>
				<c:param name="fileStorePath" value="${fileStorePath }"/>
				<c:param name="appendPath" value="${param.bbsId }"/>
			</c:url>
			<img src="${imgUrl}" width="500" alt="${fileVO.orignlFileNm}" />
		</li>
	</c:forEach>
</ul>