<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="LoginVO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<%
org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
String appendPath = sdf.format(new java.util.Date());
String userAgent = egovframework.com.cmm.web.EgovFileDownloadController.getBrowserEx(request);
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>
<c:set var="CURR_YEAR" value="<%=appendPath%>"/>
<c:set var="USER_AGENT" value="<%=userAgent%>"/>

<c:choose>
	<c:when test="${USER_AGENT eq 'MSIE'}">
		<c:set var="fileOpt" value="wmv"/>
	</c:when>
	<c:otherwise>
		<c:set var="fileOpt" value="webm"/>
	</c:otherwise>
</c:choose>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>

<c:if test="${LoginVO.userSe > 10}">
	<script type="text/javascript">
		function filelogView(param_atchFileId, param_fileSn) {
			var win = window.open('/cmm/fms/nkrefo/EgovNkrefoFiledownlogList.do?param_atchFileId='+param_atchFileId+'&param_fileSn='+param_fileSn,'log',' scrollbars=yes, resizable=yes, left=0, top=0, width=600,height=400');
			if(win != null) {
				win.focus();
			}
		}
	</script>
</c:if>

<div class="file_box">
	<c:if test="${updateFlag=='Y'}">
		<div class="file_top">
			<c:if test="${param.potalService eq 'Y'}">
				<form name="nkrefoFileAjaxForm" id="nkrefoFileAjaxForm" method="post" enctype="multipart/form-data" action="/cmm/fms/ajaxNkrefoUploadFile.do">
					<input type="file" name="uploadfile" id="uploadfile" onchange="fnAjaxFileUploadChangeEvent();" />
					<input type="hidden" name="appendPath"		id="appendPath"	value="${CURR_YEAR}" />
					<input type="hidden" name="atchFileId"		id="gatchFileId"	value="" />
					<input type="hidden" id="currSize"  name="currSize"     value="0" />
					<input type="hidden" id="maxSize"   name="maxSize"      value="0" />
					<input type="hidden" id="currCount" name="currCount"    value="0" />
					<input type="hidden" id="maxCount"  name="maxCount"     value="0" />
					<span class="bbtn_bg1"><a class="c_click">+ 파일첨부</a></span>
					<div class="progressBox">
						<div class="progress radius">
							<div class="bar">0%</div>
						</div>
					</div>
				</form>
			</c:if>
			<c:if test="${param.potalService ne 'Y'}">
				<a href="#" class="file_btn" id="btnAddFile_<c:out value="${param.editorId}"/>" style="display:none"><img src="${CMMN_IMG }/btn_add_file.gif" alt="파일첨부"/></a>
			</c:if>
			<p class="file_info">
				<strong class="blue">개수 : </strong><span id="lblCurrCount_<c:out value="${param.editorId}"/>">0</span>/<span id="lblMaxCount_<c:out value="${param.editorId}"/>">0</span> ,
				<strong class="blue">크기 : </strong><span id="lblCurrSize_<c:out value="${param.editorId}"/>">0MB</span>/<span id="lblMaxSize_<c:out value="${param.editorId}"/>">0MB</span>
			</p>
		</div>
	</c:if>

	<table class="file_list_chart" summary="첨부파일 목록을 나타낸표입니다">
		<caption>첨부파일목록</caption>
		<colgroup>
				<col width="*" />
				<col width="150" />
				<col width="50" />
				<col width="150" />
		</colgroup>
		<thead>			
			<tr>
				<th>파일명</th>
				<c:choose>
					<c:when test="${updateFlag=='Y'}">
						<th class="size">크기</th>
						<th class="del">삭제</th>
						<th class="down">다운로드</th>
					</c:when>
					<c:otherwise>
						<th class="size" colspan="3">크기</th>
					</c:otherwise>
				</c:choose>
			</tr>
		</thead>
		<tbody id="multiFileList_<c:out value="${param.editorId}"/>">
			<tr id="tr_file_empty_<c:out value="${param.editorId}"/>" <c:if test="${fn:length(fileList) ne '0'}">style="display:none"</c:if>>
				<c:choose>
					<c:when test="${updateFlag=='Y'}">
						<td colspan="4" align="center">첨부된 파일이 없습니다.</td>
					</c:when>
					<c:otherwise>
						<td colspan="3" align="center">첨부된 파일이 없습니다.</td>
					</c:otherwise>
				</c:choose>
			</tr>
			<c:forEach var="fileVO" items="${fileList}" varStatus="status">
				<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
					<c:choose>
					   <c:when test="${updateFlag=='Y'}">
							<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
								<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
								<c:param name="fileSn" value="${fileVO.fileSn}"/>
								<c:param name="returnUrl" value="${CURR_URL}"/>
							</c:url>
							<td><img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/></td>
							<td class="size"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
							<td class="del">
								<c:if test="${param.potalService ne 'Y'}">
									<a href="<c:out value="${delUrl}"/>" onclick="fn_egov_editor_file_del('<c:out value="${param.editorId}"/>', '<c:out value="${param.estnAt}"/>', '<c:out value="${param.bbsId}"/>', '<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');return false;">
										<img src="${CMMN_IMG }/btn_sdelete.gif"/>
									</a>
								</c:if>
								<c:if test="${param.potalService eq 'Y'}">
									<a href="/cmm/fms/ajaxDeleteFileInfs.do" onclick="fnAjaxFileDel('<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>', this);return false;">
										<img src="${CMMN_IMG }/btn_sdelete.gif"/>
									</a>
								</c:if>
							</td>
							<td class="">
								<input type="radio" name="dyn${fileVO.fileSn}" id="dyn${fileVO.fileSn}_yes" value="Y" cssClass="cho" <c:if test="${fileVO.downloadYn eq 'Y'}">checked="checked"</c:if> onclick="fnAjaxFileDyn('${fileVO.atchFileId}','${fileVO.fileSn}','Y',this);return false;" /> <label for="dyn${fileVO.fileSn}_yes">예</label>
								<input type="radio" name="dyn${fileVO.fileSn}" id="dyn${fileVO.fileSn}_no" value="N" cssClass="cho" <c:if test="${fileVO.downloadYn eq 'N'}">checked="checked"</c:if> onclick="fnAjaxFileDyn('${fileVO.atchFileId}','${fileVO.fileSn}','N',this);return false;" /> <label for="dyn${fileVO.fileSn}_no">아니오</label>
							</td>
							<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
							<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
					   </c:when>
					   <c:otherwise>
							<c:url var="downLoad" value="/cmm/fms/nkrefo/FileDown.do">
								<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
								<c:param name="fileSn" value="${fileVO.fileSn}"/>
								<c:param name="userId" value="${userId}"/>
								<c:param name="userSe" value="${userSeCode}"/>
								<c:param name="fileTy" value="${fileVO.fileType}"/>
							</c:url>
							<td>
								<c:choose>
									<c:when test="${LoginVO.userSe > 10  or LoginVO.userSe eq '07'}">
										<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
											<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
										</a>
										<span class="btn_log_pop" onclick="javascript:filelogView('${fileVO.atchFileId}', '${fileVO.fileSn}')">로그확인</span>
									</c:when>
									<c:when test="${userSeCode eq '06' and fileVO.downloadYn eq 'Y' and fileVO.fileType eq 'DOC'}">
										<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
											<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
										</a>
									</c:when>
									<c:otherwise>
										<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="size" colspan="3"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
							<%-- 권한확인용도로 남겨둠 <c:out value="${LoginVO.userSe}"/>, <c:out value="${userSeCode}"/>, <c:out value="${userId}"/> --%>
					   </c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<c:if test="${updateFlag ne 'Y'}"> <%-- 조회 페이지에서만 노출처리 --%>
		<br/>
		<table class="file_list_chart" summary="첨부파일 display를 위한 표입니다">
			<caption>첨부파일 display</caption>
			<colgroup>
					<col width="*" />
					<col width="*" />
			</colgroup>
			<thead>
				<tr>
					<th>파일명</th>
					<th>Display</th>
				</tr>
			</thead>
			<tbody id="multiFileList_<c:out value="${param.editorId}"/>">
				<c:forEach var="fileVO" items="${fileList}" varStatus="status">
					<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
						<c:url var="downLoadUrl" value="/cmm/fms/nkrefo/FileDown.do">
							<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
							<c:param name="fileSn" value="${fileVO.fileSn}"/>
							<c:param name="userId" value="${userId}"/>
							<c:param name="userSe" value="${userSeCode}"/>
							<c:param name="fileTy" value="${fileVO.fileType}"/>
							<c:param name="fileOpt" value="${fileOpt}"/>
						</c:url>
						<td>
							<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
						</td>
						<td>
							<c:choose>
								<c:when test="${fileVO.fileType eq 'IMG'}">
									<c:choose>
										<c:when test="${(LoginVO.userSe > 10 or LoginVO.userSe eq '07') or (LoginVO.userSe eq '06' and fileVO.downloadYn eq 'Y')}">
											<%-- TIFF 파일인 경우 IE에서만 정상적으로 노출가능함 --%>
											<c:choose>
												<c:when test="${(USER_AGENT eq 'MSIE' or USER_AGENT eq 'Safari') and (fn:toLowerCase(fileVO.fileExtsn) eq 'tif' or fn:toLowerCase(fileVO.fileExtsn) eq 'tiff')}">
													<img src='${downLoadUrl}' alt='<c:out value="${fileVO.orignlFileNm}"/>' width="600" />
												</c:when>
												<c:when test="${(USER_AGENT ne 'MSIE' and USER_AGENT ne 'Safari') and (fn:toLowerCase(fileVO.fileExtsn) eq 'tif' or fn:toLowerCase(fileVO.fileExtsn) eq 'tiff')}">
													TIFF (Tagged Image File Format) 파일은 Internet Explorer, Safari 에서만 미리보기가 지원됩니다.	
												</c:when>
												<c:otherwise>
													<img src='${downLoadUrl}' alt='<c:out value="${fileVO.orignlFileNm}"/>' width="600" />
												</c:otherwise>												
											</c:choose>
										</c:when>
										<c:otherwise>
											조회 권한이 없습니다.
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:when test="${fileVO.fileType eq 'MOV'}">
									<c:choose>
										<c:when test="${(LoginVO.userSe > 10 or LoginVO.userSe eq '07') or (LoginVO.userSe eq '06' and fileVO.downloadYn eq 'Y')}">
											<c:choose>
												<c:when test="${USER_AGENT eq 'MSIE'}"> <%-- IE 인 경우 --%>
													<OBJECT ID="_MediaPlayer" NAME="_MediaPlayer" CLASSID="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6" WIDTH="600" HEIGHT="450">
														<PARAM NAME="URL" VALUE="${downLoadUrl}">
														<PARAM NAME="enableContextMenu" value="true">
														<PARAM NAME="ShowControls" VALUE="true">
														<PARAM NAME="ShowPositionControls" value="true">
														<PARAM NAME="ShowStatusBar" value="true">
														<PARAM NAME="autostart" value="true">
														<PARAM NAME="Volume" value="50">
													</OBJECT>
												</c:when>
												<c:otherwise>
													<video id="video1" preload="metadata" controls="controls" poster="" width="600" height="">
														<source src="${downLoadUrl}" type='video/webm; codecs="vp8, vorbis"'> <%-- webm --%>
														<source src="${downLoadUrl}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'> <%-- mp4 --%>
														<source src="${downLoadUrl}" type='video/ogv; codecs="theora, vorbis"'> <%-- ogv --%>
														HTML5 Video 를 지원하지 않는 브라우저 입니다.
													</video>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											조회 권한이 없습니다.
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									사진, 동영상 외에는 미리보기를 지원하지 않습니다.
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
</div>

<input type="hidden" id="fileGroupId_<c:out value="${param.editorId}"/>" name="fileGroupId_<c:out value="${param.editorId}"/>" value="<c:out value="${param.param_atchFileId}"/>"/>
<input type="hidden" id="fileCurrCount_<c:out value="${param.editorId}"/>" name="fileCurrCount_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
<input type="hidden" id="fileCurrSize_<c:out value="${param.editorId}"/>" name="fileCurrSize_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>