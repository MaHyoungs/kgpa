<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_IMG" value="/template/member/images"/>
<c:set var="_CSS" value="/template/member/css"/>
<c:set var="C_JS" value="/template/common/js"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="stylesheet" href="${_CSS}/login.css" type="text/css" charset="utf-8" />
  <link rel="stylesheet" href="/str/cre/lyt/tmplat/sit/LYTTMP_NKREFO0000001/style.css" type="text/css" charset="utf-8" /> 
  <style>
	.total{background:url('/template/common/images/page/board/icon_balloon.gif') 0 center no-repeat;padding-left:18px;font-size:11px;color:#999999;letter-spacing:-0.05em;margin-bottom:10px;}
	.chart_board{margin-top:30px;margin-bottom:50px;width:100%;border-collapse:collapse;color:#666666; background:url('/template/common/images/page/board/bg_board_tit2.gif') repeat-x 0 0;}
	.chart_board img {vertical-align:middle;}
	.chart_board thead th{ text-align:center; padding:10px 0; color:#657591; font-weight:bold; }
	.chart_board thead th.first{background:none;border-left:1px solid #e4e4e4;}
	.chart_board thead th.last{background:none;border-right:1px solid #e4e4e4;}
	.chart_board tbody td{background:none;border-bottom:1px dotted #cbcbcb;padding:5px 0; text-align:center;}
	.chart_board tbody td a{color:#666666;}
	.chart_board tbody td.tit{text-align:left}
	.chart_board tbody tr.deleted a{color:#dfdfdf;text-decoration: line-through;}
  	.org {color: #EE8342;font-size: 11px;}
  </style>
  <script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
<title>한반도산림복원자료  첨부파일 다운로드 내역</title>
<style type="text/css">
	body{background:none;}
</style>
<script type="text/javascript">
</script>
</head>

 <body>

		<div class="pop_tit">
			<h1>한반도산림복원자료  첨부파일 다운로드 내역</h1>
		</div>

		<c:if test="${fn:length(fileDownlogList) > 0}">
			<p class="total">총  건수 ${paginationInfo.totalRecordCount}건ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
			<table class="chart_board" summary="" >
				<caption class="hdn">결과 목록</caption>
				<thead>
					<tr>
						<th colspan="2">총 <c:out value="${fileDownlogListCnt}"/>건 다운로드 / 디스플레이</th>
					</tr>
					<tr>
						<th>순번</th>
						<th>아이디</th>
						<th>시간정보</th>
						<th>비고</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="result" items="${fileDownlogList}" varStatus="status">
						<tr>
							<td><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
							<td><c:out value="${result.userId}"/></td>							
							<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:SS" value="${result.downloadPnttm}"/></td>
							<td>
								<c:choose>
									<c:when test="${result.fileTy eq 'DOC'}">
										다운로드
									</c:when>
									<c:otherwise>
										디스플레이
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<c:if test="${empty fileDownlogList or fileDownlogListCnt < 1}">
			<table class="chart_board" summary="" >
				<caption class="hdn">결과 목록</caption>
				<tbody>
					<tr>
						<th>다운로드 내역이 없습니다.</th>
					</tr>
				</tbody>
			</table>
		</c:if>

		<%-- 최상단에 위치하는 X 버튼 --%>
		<button class="pop_mclose" onclick="window.close(); return false;">닫기</button>
		
		<div id="paging">
			<c:url var="pageUrl" value="/cmm/fms/nkrefo/EgovNkrefoFiledownlogList.do">
				<c:param name="param_atchFileId" value="${param_atchFileId}" />
				<c:param name="param_fileSn" value="${param_fileSn}" />
			</c:url>
			
			<c:if test="${not empty paginationInfo}">
				<ul>
					<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				</ul>
			</c:if>
		</div>

 </body>
</html>
