<%@ page language = "java" contentType = "text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<c:if test="${not empty retUrl}">
	<script language="javascript">
		$(function(){
			var rsStr = '${sMessage}';
			if(rsStr != ''){
				alert(rsStr);
			}
			opener.location.href="${retUrl}";
			self.close();
		});
	</script>
</c:if>
<c:if test="${empty retUrl}">
	<script language="javascript">
		$(function(){
			var rsStr = '${sMessage}';
			if(rsStr != ''){
				alert(rsStr);
			}
			opener.location.reload(true);
			setTimeout(function(){
				self.close();
			}, 100);
		});
	</script>
</c:if>

<%--<body onload="realNameSubmit()">--%>
<%--<script language="javascript">--%>
<%--function realNameSubmit(){--%>
	<%--var ncResult = '${ncResult}';--%>
	<%--var name = '${name}';--%>
	<%--var birth = '${birth}';--%>
	<%--var ipinDi = '${ipinDi}';--%>
	<%--var sex = '${sex}';--%>
	<%--var foreigner = '${foreigner}';--%>
	<%--var MandW = '${MandW}';--%>

	<%--opener.checkReturnCallFn(ncResult, name, birth, ipinDi, sex, foreigner, MandW);--%>
	<%--self.close();--%>
<%--}--%>
<%--</script>--%>
<%--</body>--%>