<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<%-- 
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		 --%>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMain" value="N"/>
</c:import>
<c:set var="year_b3" value="${resultList[0].year-3}"/>
<c:set var="year_b2" value="${resultList[0].year-2}"/>
<c:set var="year_b1" value="${resultList[0].year-1}"/>
<c:set var="year"    value="${resultList[0].year}"  />
<c:set var="year_a1" value="${resultList[0].year+1}"/>
<c:set var="year_a2" value="${resultList[0].year+2}"/>
<c:set var="year_a3" value="${resultList[0].year+3}"/>
<c:set var="month"   value="${resultList[0].month}" />
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/template/common/css/egovframework/cmm/sym/cal/com.css" />
<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 연월변경
 ******************************************************** */
function fnChangeCalendar(year, month){
	var varForm			= document.all["Form"];
	varForm.action      = "<c:url value='/sym/cal/EgovNormalMonthCalendar.do'/>";
	varForm.year.value  = year;
	varForm.month.value = month;
	varForm.submit();
}

/* ********************************************************
* 연월변경
******************************************************** */
function fn_egov_change_Calendar(form){
	form.submit();
}

function fn_delete(id){
	var frm = document.normalMonthCalendar;
	
	if(confirm("해당 식단표를 삭제하시겠습니까?")){
		frm.action="/sym/cal/EgovRestdeRemove.do?dietId="+id;
		frm.submit();
	}
}

</script>
		<!-- ------------------------------------------------------------------ 상단타이틀 -->
		<form name="normalMonthCalendar" action="<c:url value='/sym/cal/EgovNormalMonthCalendar.do'/>" method="post">
			<div>
				* 식단 옆의 번호표시는 알레르기 정보 입니다. 식품알레르기가 있는 학생은 참고하시길 바랍니다.<br/>
				- ①난류, ②우유, ③메밀, ④땅콩, ⑤대두, ⑥밀, ⑦고등어, ⑧게, ⑨새우, ⑩돼지고기, ⑪복숭아, ⑫토마토
			</div>
			<input type="hidden" name="init" value="${init}" /> <input type="hidden" name="day" />
			<input type="hidden" name="menuId" value="${searchVO.menuId }" />
			<table width="700" cellpadding="8" class="table-search" border="0">
				<tr>
						<div style="text-align: right; width: 620px;">
							<select name="year" onchange="fn_egov_change_Calendar(document.normalMonthCalendar);">
								<c:if test="${year_b3 > 0 && year_b3 < 10000}">
									<option value="${year_b3}">${year_b3}</option>
								</c:if>
								<c:if test="${year_b2 > 0 && year_b2 < 10000}">
									<option value="${year_b2}">${year_b2}</option>
								</c:if>
								<c:if test="${year_b1 > 0 && year_b1 < 10000}">
									<option value="${year_b1}">${year_b1}</option>
								</c:if>
								<c:if test="${year    > 0 && year    < 10000}">
									<option value="${year}" selected="selected">${year}</option>
								</c:if>
								<c:if test="${year_a1 > 0 && year_a1 < 10000}">
									<option value="${year_a1}">${year_a1}</option>
								</c:if>
								<c:if test="${year_a2 > 0 && year_a2 < 10000}">
									<option value="${year_a2}">${year_a2}</option>
								</c:if>
								<c:if test="${year_a3 > 0 && year_a3 < 10000}">
									<option value="${year_a3}">${year_a3}</option>
								</c:if>
							</select> 년 &nbsp;&nbsp; <select name="month" onchange="fn_egov_change_Calendar(document.normalMonthCalendar);">
								<option value=1 <c:if test="${month==1 }">selected="selected"</c:if>>01</option>
								<option value=2 <c:if test="${month==2 }">selected="selected"</c:if>>02</option>
								<option value=3 <c:if test="${month==3 }">selected="selected"</c:if>>03</option>
								<option value=4 <c:if test="${month==4 }">selected="selected"</c:if>>04</option>
								<option value=5 <c:if test="${month==5 }">selected="selected"</c:if>>05</option>
								<option value=6 <c:if test="${month==6 }">selected="selected"</c:if>>06</option>
								<option value=7 <c:if test="${month==7 }">selected="selected"</c:if>>07</option>
								<option value=8 <c:if test="${month==8 }">selected="selected"</c:if>>08</option>
								<option value=9 <c:if test="${month==9 }">selected="selected"</c:if>>09</option>
								<option value=10 <c:if test="${month==10}">selected="selected"</c:if>>10</option>
								<option value=11 <c:if test="${month==11}">selected="selected"</c:if>>11</option>
								<option value=12 <c:if test="${month==12}">selected="selected"</c:if>>12</option>
							</select> 월
						</div></td>
				</tr>

			</table>

			<table cellpadding="1" class="table-line">
				<thead>
					<tr>
						<th style="height: 50px; color: red;" class="title" width="20" nowrap>일</th>
						<th class="title" width="100" nowrap>월</th>
						<th class="title" width="100" nowrap>화</th>
						<th class="title" width="100" nowrap>수</th>
						<th class="title" width="100" nowrap>목</th>
						<th class="title" width="100" nowrap>금</th>
						<th style="height: 50px; color: red;" class="title" width="20" nowrap>토</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:choose>
								<c:when test='${result.day == ""}'>
									<c:choose>
										<c:when test='${result.weeks != 6}'>
											<td style="height: 50px;"></td>
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test='${result.restAt == "Y" }'>
											<c:choose>
												<c:when test='${result.week == 7}'>
													<td style="height: 50px; width:70px; text-align: left; vertical-align: top; color: red;" nowrap>${result.day} 
															<c:forEach var="restde" items="${RestdeList}" varStatus="status">
															<c:if test="${result.year eq restde.year && result.month eq restde.month && result.day eq restde.day}">
																<table>
																	<tr>
																		<td style="text-decoration: none;" nowrap><div style='width: 92px; border: solid 0px;'>${restde.restdeNm}</div></td>
																	</tr>
																</table>
															</c:if>
														</c:forEach></td>
													<c:out value="</tr>" escapeXml="false" />
													<c:out value="<tr>" escapeXml="false" />
												</c:when>
												<c:otherwise>
													<td style="height: 50px; width:70px; text-align: left; vertical-align: top; color: red;" nowrap>${result.day} <c:forEach var="restde" items="${RestdeList}" varStatus="status">
															<c:if test="${result.year eq restde.year && result.month eq restde.month && result.day eq restde.day}">
																<table>
																	<tr>
																		<td style="text-decoration: none;" nowrap><div style='width: 92px; border: solid 0px;'>${restde.restdeNm}</div></td>
																	</tr>
																</table>
															</c:if>
														</c:forEach>
													</td>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<td style="height: 50px; width:80px; text-align: left; vertical-align: top; color: black;" nowrap>
												${result.day}
												<c:forEach var="diet" items="${RestdeList}" varStatus="status">
													<c:if test="${result.year eq diet.year && result.month eq diet.month && result.day eq diet.day}">
														<c:choose>
															<c:when test="${(USER_INFO.userSeCode eq '08') or (USER_INFO.userSeCode eq '10') or (USER_INFO.userSeCode eq '99') }">
																<c:url var="detail" value="/sym/cal/EgovRestdeModify.do">
																	<c:param name="menuId" value="${searchVO.menuId }"/>
																	<c:param name="dietId" value="${diet.dietId }"/>
																	<c:param name="division" value=""/>
																</c:url>
																<c:if test="${(USER_INFO.userSeCode eq '08') or (USER_INFO.userSeCode eq '10') or (USER_INFO.userSeCode eq '99') }">
																<a href="#" onclick="fn_delete('${diet.dietId}');">
																	<img src="/template/common/images/egovframework/uss/ivp/mpe/icon/action_delete.gif" alt="삭제" />
																</a>
																</c:if>
																<br />
																<a href="${detail}">
																	<c:if test="${not empty diet.side5 }">
																		${diet.side5 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.rice }">
																		${diet.rice }<br/>
																	</c:if>
																	<c:if test="${not empty diet.stew }">
																		${diet.stew }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side1 }">
																		${diet.side1 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side2 }">
																		${diet.side2 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side3 }">
																		${diet.side3 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side4 }">
																		${diet.side4 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side6 }">
																		${diet.side6 }<br/>
																	</c:if>
																</a>														
															</c:when>
															<c:otherwise>
															<br />
																<c:if test="${not empty diet.side5 }">
																		${diet.side5 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.rice }">
																		${diet.rice }<br/>
																	</c:if>
																	<c:if test="${not empty diet.stew }">
																		${diet.stew }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side1 }">
																		${diet.side1 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side2 }">
																		${diet.side2 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side3 }">
																		${diet.side3 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side4 }">
																		${diet.side4 }<br/>
																	</c:if>
																	<c:if test="${not empty diet.side6 }">
																		${diet.side6 }<br/>
																	</c:if>
															</c:otherwise>
														</c:choose>
													</c:if>
												</c:forEach>
											</td>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</tr>
				</tbody>
			</table>
			<c:if test="${(USER_INFO.userSeCode eq '08') or (USER_INFO.userSeCode eq '10') or (USER_INFO.userSeCode eq '99')}" >
				<div class="fR" style="margin-top:20px; margin-bottom: 20px;">
					<!-- <span class="bbtn_confirm2"><a href="/sym/cal/EgovRestdeList.do?menuId=MNU_0000000000000195" id="btnBbsWrite" title="리스트 목록">리스트 목록</a></span> -->
					<span class="bbtn_confirm2"><a href="/sym/cal/EgovRestdeRegist.do?menuId=MNU_0000000000000195" id="btnBbsWrite" title="식단표 등록하기">식단표 등록</a></span>
				</div>
			</c:if>
		</form>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8" />