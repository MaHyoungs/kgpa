<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMain" value="N"/>
</c:import>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/template/common/css/egovframework/cmm/sym/cal/com.css" />
<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 페이징 처리 함수
 ******************************************************** */
function fn_egov_pageview(pageNo){
	document.listForm.pageIndex.value = pageNo;
	document.listForm.action = "<c:url value='/sym/cal/EgovRestdeList.do'/>";
   	document.listForm.submit();
}
/* ********************************************************
 * 조회 처리 
 ******************************************************** */
function fn_egov_search_Restde(){
	document.listForm.pageIndex.value = 1;
   	document.listForm.submit();
}
/* ********************************************************
 * 등록 처리 함수 
 ******************************************************** */
function fn_egov_regist_Restde(){
	location.href = "/sym/cal/EgovRestdeRegist.do";
}
/* ********************************************************
 * 상세회면 처리 함수
 ******************************************************** */
function fn_egov_detail_Restde(restdeNo){
	var varForm				 = document.all["Form"];
	varForm.action           = "<c:url value='/sym/cal/EgovRestdeDetail.do'/>";
	varForm.restdeNo.value   = restdeNo;
	varForm.submit();
}
</script>
<form name="Form" action="" method="post">
	<input type=hidden name="restdeNo">
</form>
<div id="bbs_wrap">
<form name="listForm" action="<c:url value='/sym/cal/EgovRestdeList.do'/>" method="post">
<table width="630" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>
<table width="630" cellpadding="0" class="table-line" border="0">
<thead>
<tr>  
	<th class="title" width="5%" nowrap>번호</th>
	<th class="title" width="5%" nowrap>밥</th>
	<th class="title" width="10%" nowrap>국</th>
	<th class="title" width="10%" nowrap>반찬1</th>
	<th class="title" width="10%" nowrap>반찬2</th>
	<th class="title" width="10%" nowrap>반찬3</th>
	<th class="title" width="10%" nowrap>반찬4</th>
	<th class="title" width="10%" nowrap>특별식</th>
	<th class="title" width="10%" nowrap>디저트</th>
	<th class="title" width="10%" nowrap>식단일자</th>
	<th class="title" width="5%" nowrap>작성자</th>
	<th class="title" width="5%" nowrap>관리</th>
</tr>
</thead>    
<tbody>
<c:forEach items="${resultList}" var="result" varStatus="status">
<tr style="cursor:pointer;cursor:hand;" onclick="fn_egov_detail_Restde('${result.dietId}');">
	<td class="lt_text3" nowrap><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
	<td>${result.rice }</td>
	<td>${result.stew} </td>
	<td>${result.side1} </td>
	<td>${result.side2} </td>
	<td>${result.side3} </td>
	<td>${result.side4} </td>
	<td>${result.side5} </td>
	<td>${result.side6} </td>
	<td class="lt_text3" nowrap><c:out value='${fn:substring(result.dietDate, 0,4)}'/>-<c:out value='${fn:substring(result.dietDate,  4,6)}'/>-<c:out value='${fn:substring(result.dietDate, 6, 8)}'/></td>
	<td>${result.writerNm }</td>
	<td>삭제</td>
</tr>   
</c:forEach>
<c:if test="${fn:length(resultList) == 0}">
	<tr> 
		<td class="lt_text3" colspan=13>
			<spring:message code="common.nodata.msg" />
		</td>
	</tr>   	          				 			   
</c:if>
</tbody>  
</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>

<%-- <div align="center">
	<div>
		<ui:pagination paginationInfo = "${paginationInfo}"
				type="image"
				jsFunction="fn_egov_pageview"
				/>
	</div>
</div>
<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
 --%>
<%-- 
<table width="580" cellpadding="8" class="table-search" border="0">
 <tr>
  <td width="40%"class="title_left">
  <th>
  </th>
  <td width="10%">
   	<select name="searchCondition" class="select">
		   <option selected value=''>--선택하세요--</option>
		   <option value='1' <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if>>휴일일자</option>
		   <option value='2' <c:if test="${searchVO.searchCondition == '2'}">selected="selected"</c:if>>휴일명</option>
	   </select>
	</td>
  <td width="35%">
    <input name="searchKeyword" type="text" size="35" value="${searchVO.searchKeyword}"  maxlength="35" /> 
  </td>
  <th width="10%">
   <table border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="조회" width="8" height="20"/></td>
      <td background="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class="text_left" nowrap><a href="javascript:fn_egov_search_Restde();">조회</a></td>
      <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="조회" width="8" height="20"/></td>
      <td width="10"></td>

      <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="등록" width="8" height="20"/></td>
      <td background="${pageContext.request.contextPath}/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class="text_left" nowrap><a href="javascript:fn_egov_regist_Restde();">등록</a></td>
      <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="등록" width="8" height="20"/></td>
    </tr>
   </table>
  </th>  
 </tr>
</table>
 --%>
</form>
<%-- 페이징 처리 --%>
<div id="paging">
	<c:url var="pageUrl" value="/afs/afterSchoolUserList.do${_BASE_PARAM}"></c:url>
	<c:if test="${not empty paginationInfo}">
		<ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</c:if>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8" />