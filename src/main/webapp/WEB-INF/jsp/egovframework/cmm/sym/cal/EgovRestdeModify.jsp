<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMain" value="N"/>
</c:import>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/template/common/css/egovframework/cmm/sym/cal/com.css" />
<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="diet" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 목록 으로 가기
 ******************************************************** */
function fn_egov_list_Restde(){
	location.href = "/sym/cal/EgovRestdeList.do";
}
/* ********************************************************
 * 저장처리화면
 ******************************************************** */
function fn_egov_regist_Restde(form){
	if(confirm("<spring:message code="common.save.msg" />")){
		if(!validateRestde(form)){ 			

		}else{
			form.submit();
		}
	}
}
</script>
<!-- ------------------------------------------------------------------ 상단타이틀 -->
<form:form commandName="diet" name="diet" method="post">
<input name="cmd" type="hidden" value="Modify">
<form:hidden path="dietId"/>
<form:hidden path="dietDate"/>
<!-- ----------------- 상단 타이틀  영역 -->
<table width="700" cellpadding="8" class="table-search" border="0">
 <tr>
  <td width="100%"class="title_left">
 </tr>
</table>
<!-- ------------------------------------------------------------------ 줄간격조정  -->
<table width="700" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>

<!-- ------------------------------------------------------------------ 등록  폼 영역  -->
<table width="700" border="0" cellpadding="0" cellspacing="1" class="table-register">
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >휴일일자<img src="${pageContext.request.contextPath}/template/common/images/egovframework/uss/olp/mgt/icon/required.gif" alt="필수"  width="15" height="15"/></th>          
    <td width="80%" nowrap colspan="3"><c:out value='${fn:substring(restde.restdeDe, 0,4)}'/>-<c:out value='${fn:substring(restde.restdeDe, 4,6)}'/>-<c:out value='${fn:substring(restde.restdeDe, 6,8)}'/></td>
  </tr> 
  <tr> 
    <th width="20%" height="23" class="required_text" nowrap >휴일명<img src="${pageContext.request.contextPath}/template/common/images/egovframework/uss/olp/mgt/icon/required.gif" alt="필수"  width="15" height="15"/></th>
    <td width="80%" nowrap>
      <form:input  path="restdeNm" size="50" maxlength="50"/>
      <form:errors path="restdeNm"/>
    </td>    
  </tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="10"></td>
  </tr>
</table>

<!-- ------------------------------------------------------------------ 줄간격조정  -->
<table width="700" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>
<%-- <table border="0" cellspacing="0" cellpadding="0" align="center">
<tr> 
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="목록" width="8" height="20"/></td>
  <td background="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class=text_left" nowrap><a href="javascript:fn_egov_list_Restde()">목록</a></td>
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="목록" width="8" height="20"/></td>      
  <td width="10"></td>

  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="저장" width="8" height="20"/></td>
  <td background="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class="text_left" nowrap><a href="javascript:fn_egov_regist_Restde(document.restde);">저장</a></td>
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="저장" width="8" height="20"/></td>      
</tr>
</table> --%>
	<div class="btn_c">
		<span class="bbtn_bg1"> 
			<a href="javascript:fn_egov_regist_Restde('document.diet');">수정</a>
		</span>
		<span class="bbtn_bg2"> 
			<a href="javascript:fn_egov_list_Restde();">취소</a>
		</span>
	</div>

</form:form>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8" />