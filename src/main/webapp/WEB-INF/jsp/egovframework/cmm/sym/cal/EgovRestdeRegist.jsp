<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<%-- 
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		 --%>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMain" value="N"/>
</c:import>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/template/common/css/egovframework/cmm/sym/cal/com.css" />
<script type="text/javascript" src="<c:url value='/template/common/js/egovframework/cmm/sym/cal/EgovCalPopup.js' />" ></script>
<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="diet" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 목록 으로 가기
 ******************************************************** */
function fn_egov_list_Restde(){
	location.href = "${pageContext.request.contextPath}/sym/cal/EgovRestdeList.do";
}
/* ********************************************************
 * 저장처리화면
 ******************************************************** */
function fn_egov_regist_Restde(form){
	
	var frm = document.diet;
	if(frm.dietDate.value == ""){
		alert("식단표일자를 입력하십시오.");
	}else{
		if(confirm("<spring:message code="common.save.msg" />")){
			frm.submit();
		}		
	}
}

function fn_egov_regist_Modify(form){
	
	var frm = document.diet;
	if(frm.dietDate.value == ""){
		alert("식단표일자를 입력하십시오.");
	}else{
		if(confirm("<spring:message code="common.update.msg" />")){
			frm.action="/sym/cal/EgovRestdeModify.do";
			frm.submit();
		}		
	}
}
</script>
<!-- ------------------------------------------------------------------ 상단타이틀 -->
<form:form commandName="diet" name="diet" method="post">
<!-- ----------------- 상단 타이틀  영역 -->
<table width="580" cellpadding="8" class="table-search" border="0">
 <tr>
  <td width="100%"class="title_left">
 </tr>
</table>
<!-- ------------------------------------------------------------------ 줄간격조정  -->
<table width="580" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>
<div>
* 식단 옆의 번호표시는 알레르기 정보 입니다. 식품알레르기가 있는 학생은 참고하시길 바랍니다.<br/>
- ①난류, ②우유, ③메밀, ④땅콩, ⑤대두, ⑥밀, ⑦고등어, ⑧게, ⑨새우, ⑩돼지고기, ⑪복숭아, ⑫토마토 
</div>
<!-- ------------------------------------------------------------------ 등록  폼 영역  -->
<table width="580" border="0" cellpadding="0" cellspacing="1" class="table-register">
  <tr> 
    <th width="20%" height="23" class="required_text" nowrap >식단표일자</th>
    <td width="80%" nowrap>
		<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>" />
    	<form:hidden path="dietDate" />
    	<c:if test="${not empty diet.dietId }">
    		<c:set var="dietDate" value="${fn:substring(diet.dietDate,0,4)}-${fn:substring(diet.dietDate,4,6)}-${fn:substring(diet.dietDate,6,8)}" />
    	</c:if>
		<input name="vdietDate" type="text" size="10" value="${dietDate }"  maxlength="10" readonly onClick="fn_egov_NormalCalendar(document.diet, document.diet.dietDate, document.diet.vdietDate);" />
		<a href="javascript:fn_egov_NormalCalendar(document.diet, document.diet.dietDate, document.diet.vdietDate);" style="selector-dummy:expression(this.hideFocus=false);"><img src="<c:url value='/template/common/images/egovframework/cmm/sym/cal/bu_icon_carlendar.gif' />" alt="달력"></a>
   	</td>
  </tr>
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >밥</th>          
    <td width="80%" nowrap>
      <form:input  path="rice" size="20" maxlength="20"/>
      <form:errors path="rice"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >국(찌개)</th>          
    <td width="80%" nowrap>
      <form:input  path="stew" size="20" maxlength="20"/>
      <form:errors path="stew"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >반찬1</th>          
    <td width="80%" nowrap>
      <form:input  path="side1" size="20" maxlength="20"/>
      <form:errors path="side1"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >반찬2</th>          
    <td width="80%" nowrap>
      <form:input  path="side2" size="20" maxlength="20"/>
      <form:errors path="side2"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >반찬3</th>          
    <td width="80%" nowrap>
      <form:input  path="side3" size="20" maxlength="20"/>
      <form:errors path="side3"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >반찬4</th>          
    <td width="80%" nowrap>
      <form:input  path="side4" size="20" maxlength="20"/>
      <form:errors path="side4"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >특별식</th>          
    <td width="80%" nowrap>
      <form:input  path="side5" size="50" maxlength="20"/>
      <form:errors path="side5"/>
    </td>    
  </tr> 
  <tr>
    <th width="20%" height="23" class="required_text" nowrap >디저트</th>          
    <td width="80%" nowrap>
      <form:input  path="side6" size="50" maxlength="20"/>
      <form:errors path="side6"/>
    </td>    
  </tr> 
  
</table>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="10"></td>
  </tr>
</table>

<!-- ------------------------------------------------------------------ 줄간격조정  -->
<table width="580" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td height="3px"></td>
</tr>
</table>
<!-- ------------------------------------------------------------------ 목록/저장버튼  -->
<%-- <table border="0" cellspacing="0" cellpadding="0" align="center">
<tr> 
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="저장" width="8" height="20"></td>
  <td background="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class=text_left" nowrap><a href="javascript:fn_egov_list_Restde()">목록</a></td>
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="저장" width="8" height="20"></td>      
  <td width="10"></td>

  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_left.gif" alt="저장" width="8" height="20"></td>
  <td background="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_bg.gif" class="text_left" nowrap><a href="javascript:fn_egov_regist_Restde(document.restde);">저장</a></td>
  <td><img src="${pageContext.request.contextPath}/template/common/images/egovframework/cmm/sym/cal/btn/bu2_right.gif" alt="저장" width="8" height="20"></td>      
</tr>
</table> --%>
	<div class="btn_c">
		<span class="bbtn_bg1"> 
			<c:choose>
				<c:when test="${not empty diet.dietId }">
					<input name="division" type="hidden" value="Modify">
					<input name="dietId" type="hidden" value="${diet.dietId }">
					<%-- <input type="hidden" name="menuId" value="${searchVO.menuId }"/> --%>
					<a href="javascript:fn_egov_regist_Modify('document.diet');">수정</a>
				</c:when>
				<c:otherwise>
					<a href="javascript:fn_egov_regist_Restde('document.diet');">등록</a>
				</c:otherwise>
			</c:choose>
		</span>
		<span class="bbtn_bg2"> 
			<a href="/sym/cal/EgovNormalMonthCalendar.do?menuId=MNU_0000000000000195">취소</a>
		</span>
	</div>
	<input name="cmd" type="hidden" value="<c:out value='save'/>"/>
</form:form>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8" />