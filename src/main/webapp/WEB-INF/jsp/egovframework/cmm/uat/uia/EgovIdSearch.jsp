<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" import="Kisinfo.Check.IPINClient" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menuSeq" value="01"/>
</c:import>

<%
	NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
    
    String sSiteCode		= "R320";				// NICE신용평가정보로부터 부여받은 사이트 코드
    String sSitePassword	= "43548228";				// NICE신용평가정보부터 부여받은 사이트 패스워드
    
    //아이핀연계정보는 별도 계약 입니다. 계약 확인후 진행해 주세요.
    String sIPINSiteCode	= "H749";				// NICE신용평가정보로부터 부여받은 아이핀사이트 코드(DI/CI 응답이 필요한 경우 사용)
	String sIPINPassword	= "15484538";				// NICE신용평가정보로부터 부여받은 아이핀사이트 패스워드
                                                    	
    String sReturnURL 		= "http://danyang.itshome.co.kr/uss/umt/cmm/NiceNameCheck.do";	//결과 수신 URL    
    String sReturnURLIpin	= "http://danyang.itshome.co.kr/uss/umt/cmm/NiceIpinCheck.do";	//결과 수신 URL    
    String sRequestNO		= "";												// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 필요시 사용
    String sBGType			= "";												//서비스 화면 색상 선택
    String sClientImg		= "";												//서비스 화면 로고 선택: default 는 null 입니다.(full 경로 입력해 주세요.)
    
    String sReserved1		= "";
	String sReserved2		= "";
	String sReserved3		= "";
    
    sRequestNO = niceCheck.getRequestNO(sSiteCode);	//요청고유번호 / 비정상적인 접속 차단을 위해 필요
  	session.setAttribute("REQ_SEQ" , sRequestNO);	//해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
    //out.println ("sRequestNO : " + sRequestNO + "<br/>");
    
    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:RTN_URL" + sReturnURL.getBytes().length + ":" + sReturnURL +
                        "7:REQ_SEQ" + sRequestNO.getBytes().length + ":" + sRequestNO +
                        "7:BG_TYPE" + sBGType.getBytes().length + ":" + sBGType +
                        "7:IMG_URL" + sClientImg.getBytes().length + ":" + sClientImg ; 

    
    String sPlainData1 = "7:RTN_URL" + sReturnURL.getBytes().length + ":" + sReturnURL +
                        "7:REQ_SEQ" + sRequestNO.getBytes().length + ":" + sRequestNO +
                        "7:BG_TYPE" + sBGType.getBytes().length + ":" + sBGType +
                        "7:IMG_URL" + sClientImg.getBytes().length + ":" + sClientImg +
                        "9:RESERVED1" + sReserved1.getBytes().length + ":" + sReserved1 +
                        "9:RESERVED2" + sReserved2.getBytes().length + ":" + sReserved2 +
                        "9:RESERVED3" + sReserved3.getBytes().length + ":" + sReserved3 ;
                        
	String sPlainData2 = "7:RTN_URL" + sReturnURL.getBytes().length + ":" + sReturnURL +
                        "7:REQ_SEQ" + sRequestNO.getBytes().length + ":" + sRequestNO +
                        "7:BG_TYPE" + sBGType.getBytes().length + ":" + sBGType + 
                        "7:IMG_URL" + sClientImg.getBytes().length + ":" + sClientImg +
                        "13:IPIN_SITECODE" + sIPINSiteCode.getBytes().length + ":" + sIPINSiteCode +
                        "17:IPIN_SITEPASSWORD" + sIPINPassword.getBytes().length + ":" + sIPINPassword ;                        
    
    String sMessage = "";
    String sEncData = "";
    
    int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData2);
    if( iReturn == 0 )
    {
        sEncData = niceCheck.getCipherData();
        //out.println ("요청정보_암호화_성공[ : " + sEncData + "]");
    }
    else if( iReturn == -1)
    {
        sMessage = "암호화 시스템 에러입니다.";
    }    
    else if( iReturn == -2)
    {
        sMessage = "암호화 처리오류입니다.";
    }    
    else if( iReturn == -3)
    {
        sMessage = "암호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
    
    
  	//Ipin
	IPINClient pClient = new IPINClient();

  	String sCPRequest = pClient.getRequestNO(sIPINSiteCode);
	String sRtnMsg = "";
	String sEncData_ipin = "";
	//sCPRequest = pClient.getRequestNO(sIPINSiteCode);
	session.setAttribute("CPREQUEST" , sCPRequest);
	
	int iRtn = pClient.fnRequest(sIPINSiteCode, sIPINPassword, sCPRequest, sReturnURLIpin);
	
	if (iRtn == 0)
	{
	
		sEncData_ipin = pClient.getCipherData();		//암호화 된 데이타
		sRtnMsg = "정상 처리되었습니다.";
	
	}
	else if (iRtn == -1 || iRtn == -2)
	{
		sRtnMsg =	"배포해 드린 서비스 모듈 중, 귀사 서버환경에 맞는 모듈을 이용해 주시기 바랍니다.<br/>" +
					"귀사 서버환경에 맞는 모듈이 없다면 ..<br/><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
	}
	else if (iRtn == -9)
	{
		sRtnMsg = "입력값 오류 : fnRequest 함수 처리시, 필요한 4개의 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	}
	else
	{
		sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 개발 담당자에게 문의해 주세요.";
	}  
%>

<script type="text/javascript">

function checkReturnCallFn(ncResult, name, birth, ipinDi, sex, foreigner, MandW) {
	if (ncResult == '1' || ncResult == 1) {
		document.getElementById("credtId").value = ipinDi;
		
		document.frm_main.action = "/uat/uia/egov3MethodIdSearch.do?searchMethod=NameCheck";
		document.frm_main.target = "_self";
		document.frm_main.submit();
	} else {//응답실패 
		alert("응답에 실패하였습니다.");
	}
}

function fnPopup_nameCheck(){
	
	window.open('', 'popup', 'width=450, height=350,toolbar=no,directories=no,scrollbars=no,resizable=no,status=no,menubar=no,top=0,left=0');
	document.frm_main.action = "https://cert.namecheck.co.kr/NiceID/certnc_input.asp";
	document.frm_main.target = "popup";
	document.frm_main.submit();
	
}

function fnPopup_ipin(){
	window.open('', 'popupIPIN2', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
	document.form_ipin.target = "popupIPIN2";
	document.form_ipin.action = "https://cert.vno.co.kr/ipin.cb";
	document.form_ipin.submit();
}

window.name ="Parent_window";

function fnPopup_chk(){
	window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
	document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
	document.form_chk.target = "popupChk";
	document.form_chk.submit();
}
</script>
	<form:form commandName="frm_main" name="frm_main" method="post" action="">
		<input type="hidden" name="enc_data" value="<%=sEncData%>" />	
		<input type="hidden" name="division" value="idSearch"/>
		<input type="hidden" name="credtId" id="credtId"/>
	</form:form> 
	
	<form:form commandName="form_ipin" name="form_ipin" method="post">
		<input type="hidden" name="m" value="pubmain">
	    <input type="hidden" name="enc_data" value="<%= sEncData_ipin %>">
	    <input type="hidden" name="param_r1" value="">
	    <input type="hidden" name="param_r2" value="">
	    <input type="hidden" name="param_r3" value="">
	</form:form>
	<form:form commandName="form_chk" name="form_chk" method="post">
	<input type="hidden" name="m" value="checkplusSerivce">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
		<input type="hidden" name="EncodeData" value="<%= sEncData %>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
		<input type="hidden" name="param_r1" value="">
		<input type="hidden" name="param_r2" value="">
		<input type="hidden" name="param_r3" value="">
	</form:form>

   <iframe src="#" frameborder="0" name="chkFrame" width="0" height="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
	
	<div id="box1">
		<!-- 실명인증 -->
		<h3 class="icon2">실명인증</h3>
		<div class="find_box2">
			<ul>
				<li class="blue">* 실명인증을 통해 ID정보를 조회합니다.</li>
			</ul>
			<a href="#" onclick="fnPopup_nameCheck();" id="authSci" class="btn2"><span>실명인증</span></a>
		</div>
		<!-- 아이핀 -->
		<h3 class="icon2">아이핀(I-PIN)인증</h3>
		<div class="find_box2">
			<p class="blue mB20">* 아이디찾기시 개인정보보호를 위해 주민등록번호 외 본인확인 할 수 <br/>&nbsp;&nbsp;&nbsp;있는 아이핀(I-PIN)을 운영중입니다.</p>
			<ul>
				<li class="blue">* 실명인증을 통해 공공아이핀으로 정보를 조회합니다.</li>
			</ul>
			<a href="#" id="authPin" class="btn2" onclick="fnPopup_ipin();"><span>아이핀(I-PIN)인증</span></a>
		</div>
	</div>
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>