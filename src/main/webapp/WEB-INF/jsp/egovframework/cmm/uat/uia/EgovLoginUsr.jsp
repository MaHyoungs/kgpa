<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.utl.fcc.service.CurrUrlUtil" %>
<%
	//현재 Action 명 불러오기
	String action = (String)request.getAttribute("javax.servlet.forward.request_uri");
	String currUrl = action + CurrUrlUtil.currRetunUrlParameter(request);
	if(currUrl.contains("addBoardArticle")){
		session.setAttribute("returnUrl", currUrl);
	}
	String bbsId = request.getParameter("bbsId");
	if(bbsId != null && (bbsId.equals("BBSMSTR_000000000043") || bbsId.equals("BBSMSTR_000000000064") || bbsId.equals("BBSMSTR_000000000057") || bbsId.equals("BBSMSTR_000000000058") || bbsId.equals("BBSMSTR_000000000059") || bbsId.equals("BBSMSTR_000000000060"))){
		session.setAttribute("returnUrl", currUrl);
	}
%>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%>
</c:set>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images" />
<c:choose>
	<c:when test="${IS_MOBILE}">
		<!DOCTYPE HTML>
		<html>
		<head>
		<title>산림청 녹색사업단</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
		<link rel="stylesheet" href="/template/member/css/login.css" type="text/css" charset="utf-8" />
		<script type="text/javascript" src="/template/common/js/jquery/jquery-1.7.min.js"></script>
		</head>
		<body class="login_body">
			<h1>
				<img src="/template/member/images/mobile/login/logo.png" alt="산림청 녹색사업단" />
			</h1>

			<div class="tit_login">
				<h2>
					<img src="/template/member/images/mobile/login/tit_login.png" alt="산림청 녹색사업단 로그인" />
				</h2>
			</div>
			<div class="mlogin_box">
				<form action="<c:if test="${empty domain}">http://www.kgpa.or.kr/uat/uia/actionTempLogin.do</c:if><c:if test="${not empty domain}">http://${domain}kgpa.or.kr/uat/uia/actionTempLogin.do</c:if>" name="frmGnrlLogin" method="post" onsubmit="return checkGnrlLogin(this)">
					<fieldset>
						<legend>산림청 녹색사업단 로그인 입력폼</legend>
						<div class="mlogin_inp">
							<span> <label for="id"><img src="/template/member/images/login/txt_id.gif" alt="아이디" /> </label> <input type="text" id="id" name="id" value="" title="아이디" />
							</span> <span> <label for="pwd"><img src="/template/member/images/login/txt_pwd.gif" alt="패스워드" /> </label> <input type="password" id="pwd" name="password" value="" title="패스워드" />
							</span>
						</div>
						<input type="image" src="/template/member/images/mobile/login/btn_login.png" alt="로그인" />
					</fieldset>
				</form>
				<p>
					* 산림청 녹색사업단 홈페이지의 계정으로<br /> 접속가능하며, <strong>회원가입,비밀번호찾기등은<br /> 홈페이지
					</strong> 에서 가능합니다.
				</p>
			</div>
		</body>
		</html>
	</c:when>
	<c:when test="${siteVO.siteId eq 'SITE_000000000000002'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8" />
		<script type="text/javascript">
			<c:if test='${not empty message}'>
			alert("${message}");
			</c:if>
			function checkCertLogin(frm) {
				if (frm.id.value == "") {
					alert("아이디를  입력해주세요");
					frm.id.focus();
					return false;
				}
			}

			function checkGnrlLogin(frm) {
				if (frm.id.value == "") {
					alert("아이디를  입력해주세요");
					frm.id.focus();
					return false;
				}

				if (frm.pwd.value == "") {
					alert("비밀번호를  입력해주세요");
					frm.pwd.focus();
					return false;
				}
			}

			function fnPopup(url, name){
				var win = window.open(url, name, 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
				if(win != null){
					win.focus();
				}
			}

			function fnIdFind(){
				$('.certification').show();
				$('#ipinBtn').focus();
				$('#ipinBtn').attr('href', '/gpin/ipin_main.jsp?idFind=true');
				$('#mblBtn').attr('href', '/gpin/mbl_main.jsp?idFind=true');
			}

			function fnPwFind(){
				$('.certification').show();
				$('#ipinBtn').focus();
				$('#ipinBtn').attr('href', '/gpin/ipin_main.jsp?pwFind=true');
				$('#mblBtn').attr('href', '/gpin/mbl_main.jsp?pwFind=true');
			}
		</script>
		<div class="sub_container">
			<div class="sub_top sub_top01">
				<div class="navi">
					<span class="location"> <img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" /> <strong> 로그인</strong>
					</span>
				</div>
				<h2>로그인</h2>
			</div>
			<div id="content">
				<!-- 로그인 -->
				<div class="login">
					<div class="tit_login">
						<h1>
							<a href="/index.do"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.png" alt="산림청 녹색사업단 통합관리시스템" /></a><br />
						</h1>

						<p>
							<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images//login/txt_login.png" alt="Member Login" />
							<strong>로그인이 필요한 서비스입니다. 로그인 해주세요. <span>회원이 아닌 분은 회원가입</span> 해 주세요.
							</strong>
						</p>
					</div>

					<div class="login_border">
						<div class="login_inp">
							<form action="<c:if test="${empty domain}"><c:url value='http://www.kgpa.or.kr/uat/uia/actionTempLogin.do'/></c:if><c:if test="${not empty domain}"><c:url value='http://${domain}kgpa.or.kr/uat/uia/actionTempLogin.do'/></c:if>" name="frmGnrlLogin" method="post" onsubmit="return checkGnrlLogin(this)">
								<fieldset>
									<legend>녹색자금통합관리시스템 로그인 입력</legend>
									<span> <label for="id">아이디</label> <input type="text" name="id" id="id" class="inp" />
									</span> <span> <label for="pwd">비밀번호</label> <input type="password" name="password" id="pwd" class="inp" />
									</span>
									<input type="image" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images//login/btn_login.png" class="btn_login" alt="로그인" />
								</fieldset>
							</form>

							<ul>
								<li><span class="cbtn5"><button onclick="fnIdFind();">아이디찾기</button></span></li>
								<li><span class="cbtn5"><button onclick="fnPwFind();">비밀번호찾기</button></span></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="certification" style="<c:if test="${empty param.certi or param.certi eq 'Y'}">display:none;</c:if>">
					<ul>
						<li class="ipin">
							<strong>아이핀(I-Pin)</strong> <span>인터넷 상에서 주민등록번호를 대신하여 아이핀<br /> 아이디와 비밀번호를 통해 인증합니다.
							</span> <a id="ipinBtn" onclick="fnPopup(this.href, 'ipinPop');return false;" title="새창열림" href="/gpin/ipin_main.jsp">바로가기</a>
						</li>
						<li class="hp">
							<strong>휴대폰인증</strong> <span>고객님의 명의로 등록된 휴대폰 번호를 통해<br /> 본인인증을 합니다.
							</span> <a id="mblBtn" onclick="fnPopup(this.href, 'mobilePop');return false;" title="새창열림" href="/gpin/mbl_main.jsp">바로가기</a>
						</li>
					</ul>
				</div>
				<!-- //로그인 -->
			</div>

			<div class="btn_top">
				<button type="button">
					<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" />
				</button>
			</div>
		</div>
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8" />
	</c:when>
	<c:otherwise>
		<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8" />
		<script type="text/javascript">
			<c:if test='${not empty message}'>
			alert("${message}");
			</c:if>
			function checkCertLogin(frm) {
				if (frm.id.value == "") {
					alert("아이디를  입력해주세요");
					frm.id.focus();
					return false;
				}
			}

			function checkGnrlLogin(frm) {
				if (frm.id.value == "") {
					alert("아이디를  입력해주세요");
					frm.id.focus();
					return false;
				}

				if (frm.pwd.value == "") {
					alert("비밀번호를  입력해주세요");
					frm.pwd.focus();
					return false;
				}
			}
		</script>
		<div class="login_box">

			<!-- 녹색사업단 로그인 -->
			<div class="smartshool login">
				<div class="tit_login">
					<h1>
						<img src="${_IMG}/login/logo.png" alt="산림청 녹색사업단" />
					</h1>

					<p>
						<img src="${_IMG}/login/txt_login.png" alt="Member Login" />
						<strong>로그인이 필요한 서비스입니다. <br />로그인 해주세요.
						</strong>
					</p>
				</div>

				<div class="login_border">

					<div class="login_inp">
						<form action="<c:if test="${empty domain}"><c:url value='http://www.kgpa.or.kr/uat/uia/actionTempLogin.do'/></c:if><c:if test="${not empty domain}"><c:url value='http://${domain}kgpa.or.kr/uat/uia/actionTempLogin.do'/></c:if>" name="frmGnrlLogin" method="post" onsubmit="return checkGnrlLogin(this)">
							<fieldset>
								<legend>녹색사업단 로그인 입력폼</legend>
								<span> <label for="id">아이디</label> <input type="text" name="id" id="id" class="inp" />
								</span> <span> <label for="pwd">비밀번호</label> <input type="password" name="password" id="pwd" class="inp" />
								</span>
								<input type="image" src="${_IMG}/login/btn_login.png" alt="로그인" class="btn_login" />
							</fieldset>
						</form>

					</div>

				</div>

			</div>
			<!-- //녹색사업단 로그인 -->
		</div>
		<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8" />
	</c:otherwise>
</c:choose>