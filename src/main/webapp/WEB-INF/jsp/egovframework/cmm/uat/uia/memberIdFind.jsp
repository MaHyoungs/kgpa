<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
	function fnPopup(url, name){
		var win = window.open(url, name, 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		if(win != null){
			win.focus();
		}
	}

	function fnPwFind(){
		$('.certification').show();
		$('#ipinBtn').focus();
		$('#ipinBtn').attr('href', '/gpin/ipin_main.jsp?pwFind=true');
		$('#mblBtn').attr('href', '/gpin/mbl_main.jsp?pwFind=true');
	}
</script>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="http://gfund.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />&gt;<strong> 아이디찾기</strong>
		</span>
	</div>
	<h2>아이디찾기</h2>
</div>

<div id="content">
	<div class="find_box">
		<strong>아이디찾기</strong>

		<p>아래의 사용자 확인 방법 중 하나를 선택하여 회원가입 시 등록한 정보를 입력해주세요.</p>

		<ul>
			<li>* 개인정보 도용에 대한 피해방지를 위해 <em>아이디의 앞 3자리를 제외한 나머지는 '*' 표시 처리</em>합니다.</li>
			<li>* [아이디 전체 보기] 버튼을 클릭하여 아이디 뒷자리를 확인 할 수 있습니다.</li>
		</ul>

		<div class="line_area">
			<strong>${userVo.userNm}님의 아이디는<em> ${userVo.tempId}</em>입니다.</strong>
		</div>

		<div class="btn_c">
			<span class="cbtn02"><a href="" onclick="alert('${userVo.userNm}님의 아이디는 ${userVo.userId} 입니다.'); return false;">아이디 전체보기</a></span>
			<span class="cbtn01"><a href="" onclick="fnPwFind(); return false;">비밀번호 찾기</a></span>
		</div>
	</div>

	<div class="certification" style="display:none;">
		<ul>
			<li class="ipin">
				<strong>아이핀(I-Pin)</strong> <span>인터넷 상에서 주민등록번호를 대신하여 아이핀<br /> 아이디와 비밀번호를 통해 인증합니다.
							</span> <a id="ipinBtn" onclick="fnPopup(this.href, 'ipinPop');return false;" title="새창열림" href="/gpin/ipin_main.jsp">바로가기</a>
			</li>
			<li class="hp">
				<strong>휴대폰인증</strong> <span>고객님의 명의로 등록된 휴대폰 번호를 통해<br /> 본인인증을 합니다.
							</span> <a id="mblBtn" onclick="fnPopup(this.href, 'mobilePop');return false;" title="새창열림" href="/gpin/mbl_main.jsp">바로가기</a>
			</li>
		</ul>
	</div>
</div>

<%
	/** 세션 삭제 **/
	session.removeAttribute("UserPrivateCert");
	session.removeAttribute("idFind");
	session.removeAttribute("pwFind");
	session.removeAttribute("userVo");
%>