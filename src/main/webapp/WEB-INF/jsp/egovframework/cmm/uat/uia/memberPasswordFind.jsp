<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
	<c:if test="${not empty message}">
		$(function(){
			alert('${message}');
		});
	</c:if>

	function fnPasswordFormSubmit(){
		if($('#userId').val() != ""){
			return true;
		}else{
			alert("아이디를 입력해주십시오.");
			$('#userId').focus();
			return false;
		}
	}
</script>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="http://gfund.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />&gt;<strong> 비밀번호찾기</strong>
		</span>
	</div>
	<h2>비밀번호찾기</h2>
</div>

<div id="content">
	<div class="find_box">
		<strong>비밀번호찾기</strong>
		<form name="passwordForm" id="passwordForm" action="/uat/uia/memberTempPassword.do" method="post" onsubmit="return fnPasswordFormSubmit();">
			<fieldset>
				<legend>비밀번호찾기 입력폼</legend>
				<div class="line_area">
					<label for="userId">아이디</label>
					<input type="text" class="inp" id="userId" name="userId" />
				</div>

				<div class="btn_c">
					<span class="cbtn02"><button type="submit">확인</button></span>
					<span class="cbtn01"><a href="/index.do" >취소</a></span>
				</div>
			</fieldset>
		</form>
	</div>
</div>
