<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="http://gfund.cheongahmit.co.kr/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />&gt;<strong> 비밀번호찾기</strong>
		</span>
	</div>
	<h2>비밀번호찾기</h2>
</div>

<div id="content">
	<div class="find_box">
		<strong>비밀번호찾기</strong>
		<div class="line_area">
			<ul>
				<li>* ${userVo.userNm}님의 임시 비밀번호는 <em>${tmpPw}</em> 입니다.</li>
				<li>* 로그인하신 후 고객님께서 사용하실 비밀번호를 변경해 주시기 바랍니다.</li>
			</ul>
		</div>

		<div class="btn_c">
			<span class="cbtn02"><a href="/uat/uia/egovLoginUsr.do" target="_blank" title="새창열림">로그인</a></span>
		</div>
	</div>
</div>

<%
	/** 세션 삭제 **/
	session.removeAttribute("UserPrivateCert");
	session.removeAttribute("idFind");
	session.removeAttribute("pwFind");
	session.removeAttribute("userVo");
	session.removeAttribute("tmpPw");
%>