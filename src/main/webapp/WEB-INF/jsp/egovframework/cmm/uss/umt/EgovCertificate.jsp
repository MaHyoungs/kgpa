<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	//인증후 리턴 시킬 URL
	session.setAttribute("retUrl", "/uss/umt/user/EgovUserInsertView.do");
%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8" />

<script type="text/javaScript">
	function fnPopup(url, name){
		var win = window.open(url, name, 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		if(win != null){
			win.focus();
		}
	}
</script>

<c:choose>
	<c:when test="${IS_MOBILE}">
	</c:when>
	<c:when test="${siteVO.siteId eq 'SITE_000000000000002'}">
		<!-- sub start -->
		<div  class="sub_container">

			<div class="sub_top sub_top01">
				<div class="navi">
						<span class="location">
							<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" /> <strong>회원가입 </strong>
						</span>
				</div>
				<h2>회원가입</h2>
			</div>

			<div id="content">
				<!-- 컨텐츠  -->
				<div class="join_step">
					<ul>
						<li>
							<span> Step1</span>
							<strong>회원약관 확인 및 동의</strong>
						</li>
						<li class="active">
							<span> Step2</span>
							<strong>회원 가입인증</strong>
						</li>
						<li>
							<span> Step3</span>
							<strong>회원가입 정보입력</strong>
						</li>
						<li>
							<span> Step4</span>
							<strong>회원가입 완료</strong>
						</li>
					</ul>
				</div>

				<h3 class="icon1">본인인증</h3>
				<div class="certification">
					<ul>
						<li class="ipin">
							<strong>아이핀(I-Pin)</strong>
							<span>인터넷 상에서 주민등록번호를 대신하여 아이핀<br /> 아이디와 비밀번호를 통해 인증합니다.</span>
							<a href="/gpin/ipin_main.jsp?memJoin=true" title="새창열림" onclick="fnPopup(this.href, 'ipinPop');return false;">바로가기</a>
						</li>
						<li class="hp">
							<strong>휴대폰인증</strong>
							<span>고객님의 명의로 등록된 휴대폰 번호를 통해<br /> 본인인증을 합니다.</span>
							<a href="/gpin/mbl_main.jsp?memJoin=true"  title="새창열림" onclick="fnPopup(this.href, 'mobilePop');return false;">바로가기</a>
						</li>
					</ul>
				</div>

				<%
					if(request.getRemoteAddr().equals("192.168.1.1") || request.getRemoteAddr().equals("127.0.0.1")){
				%>
				<br />
				<br />
				<a href="/uss/umt/cmm/userInfoTest.do" onclick="fnPopup(this.href, 'testPop');return false;" style="font-size:14pt; font-weight:bold;">개발자 인증</a>
				<%
					}
				%>
				<!-- //컨텐츠  -->
			</div>

			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		</div>
		<!-- sub end -->
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
