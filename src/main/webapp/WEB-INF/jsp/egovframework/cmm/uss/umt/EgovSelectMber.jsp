<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<c:set var="isAuthenticated"><%=EgovUserDetailsHelper.isAuthenticated(request, response) %></c:set>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>
<c:set var="_PREFIX" value="${pageContext.request.contextPath}/uss/umt/cmm"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images" />
<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8"/>

<script type="text/javaScript" language="javascript">
<!--
<c:if test="${isAuthenticated}">
alert("이미 로그인 하셨습니다.");
location.href ="<%=Globals.MAIN_PAGE%>";
</c:if>
//-->
	function user_grade(grade){
		var url = '${pageContext.request.contextPath}EgovStplatCnfirmMber.do';
			url = url+"?grade="+grade;
			document.seletUserGrade.action = url;
			document.seletUserGrade.submit();
	}
</script>
<form method="post" name="seletUserGrade" action="${pageContext.request.contextPath}/uss/umt/cmm/EgovStplatCnfirmMber.do">
	<!-- //20130311 회원선택 -->
	<!-- content strat -->
	<div id="content"> 
		<!-- 20130311 회원선택-->
		<div class="join_choice">
			<ul>
				<li class="join_fbg"><p class="join_ftitle">일반회원 가입신청</p><p class="join_ftxt">일반회원 가입 후  커뮤니티등을  자유롭게 이용하실 수 있습니다.</p>
				<p><a href="#" onclick="user_grade('general')"><img src="${_IMG }/login/join_fbtn.gif" alt="회원가입" /></a></p></li>
				<li class="join_fbg"><p class="join_ftitle">학생 가입신청</p><p class="join_ftxt">가입 후 교사의 승인을 거쳐 정보열람 및 반 커뮤니티등을 이용하실 수 있습니다.</p>
				<p><a href="#" onclick="user_grade('student')"><img src="${_IMG }/login/join_fbtn.gif" alt="회원가입" /></a></p></li>
				<li class="join_fbg"><p class="join_ftitle">학부모 가입신청</p><p class="join_ftxt">회원가입 후 정보열람 및 알림장등의 메세지를 받으실 수있습니다. </p>
				<p><a href="#" onclick="user_grade('parent')"><img src="${_IMG }/login/join_fbtn.gif" alt="회원가입" /></a></p></li>
				<li class="join_fbg"><p class="join_ftitle">외부강사 가입신청</p><p class="join_ftxt">외부강사 가입 후 담당자 승인이후에 로그인 가능합니다.      </p>
				<p><a href="#" onclick="user_grade('lecture')"><img src="${_IMG }/login/join_fbtn.gif" alt="회원가입" /></a></p></li>
				<li class="join_fbg"><p class="join_ftitle">교사 가입신청</p><p class="join_ftxt">교사 정회원 가입 후 교사권한 메뉴들을 이용하실 수 있습니다.</p>
				<p><a href="#" onclick="user_grade('staff')"><img src="${_IMG }/login/join_fbtn.gif" alt="회원가입" /></a></p></li>
			</ul>
			
			
		</div>
	</div>
</form>
			
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>