<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8" />

<script type="text/javaScript">
	function cnfirm() {
		if (!$('input[id=agree01]:checked').is(':checked')) {
			$('input[id=agree01]').focus();
			alert('개인정보보호법 제 15조에 의거 상기와 같이 개인정보를 수집․이용함에 동의하지 않으셨습니다.');
			return false;
		}else if (!$('input[id=agree02]:checked').is(':checked')) {
			$('input[id=agree02]').focus();
			alert('개인정보보호법 제 24조에 따라 고유식별번호(주민등록번호)의 수집․이용에 동의하지 않으셨습니다.');
			return false;
		}else{
			return true;
		}
	}
</script>

<c:choose>
	<c:when test="${IS_MOBILE}">
	</c:when>
	<c:when test="${siteVO.siteId eq 'SITE_000000000000002'}">
		<!-- sub start -->
		<div  class="sub_container">

			<div class="sub_top sub_top01">
				<div class="navi">
						<span class="location">
							<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" /> <strong>회원가입 </strong>
						</span>
				</div>
				<h2>회원가입</h2>
			</div>

			<div id="content">
				<!-- 컨텐츠  -->
				<div class="join_step">
					<ul>
						<li class="active">
							<span> Step1</span>
							<strong>회원약관 확인 및 동의</strong>
						</li>
						<li>
							<span> Step2</span>
							<strong>회원 가입인증</strong>
						</li>
						<li>
							<span> Step3</span>
							<strong>회원가입 정보입력</strong>
						</li>
						<li>
							<span> Step4</span>
							<strong>회원가입 완료</strong>
						</li>
					</ul>
				</div>


				<h3 class="icon1">개인정보 취급방침</h3>

				<div class="pbg">
					<ul class="icon3_list">
						<li>지원하는 녹색사업단 녹색자금 사업에서는 다음과 같이 개인정보를 수집․이용합니다. 단, 개인정보 수집 및 이용에 대한 동의를 철회하는 경우, 보유 및 이용기간이 종료한 경우 해당 개인정보는 지체없이 파기됩니다.
							<ul class="icon_none pL10">
								<li>1. 수집목적 : 지원자 또는 지원기관에 관한 정보 파악 </li>
								<li>2. 수집항목 : 성명, 주민등록번호 또는 생년월일, 연락처, 주소, 이메일, 자격사항 등 정보 </li>
								<li>3. 보유 및 이용기간 : 1년</li>
							</ul>
						</li>
						<li>회원가입 당사자는 개인정보의 수집․이용에 동의하지 않을 권리가 있으며, 이 경우 사업 선정 과정에서 제외되거나 불이익을 받을 수 있습니다.</li>
					</ul>
				</div>

				<form action="/uss/umt/cmm/EgovCertificate.do" onsubmit="return cnfirm();">
					<div class="agree_box">
						<div class="agree">
							<label for="agree01">본인은 상기 내용에 대하여 사전에 충분히 인지하였으며, 개인정보보호법 제 15조에 의거 상기와 같이 개인정보를 수집․이용함에 동의합니다.</label>
							<input type="checkbox" name="" id="agree01" />
						</div>

						<div class="agree right">
							<label for="agree02">상기 목적으로 개인정보보호법 제 24조에 따라 고유식별번호(주민등록번호)의 수집․이용에 동의합니다.</label>
							<input type="checkbox"  name="" id="agree02"/>
						</div>
					</div>

					<div class="btn_c">
						<span class="cbtn1"><button type="submit">동의</button></span>
						<span class="cbtn"><button type="button" onclick="location.href='/index.do';">취소</button></span>
					</div>
				</form>


				<!-- //컨텐츠  -->
			</div>

			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		</div>
		<!-- sub end -->
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
