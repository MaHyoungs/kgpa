<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8" />

<c:choose>
	<c:when test="${IS_MOBILE}">
	</c:when>
	<c:when test="${siteVO.siteId eq 'SITE_000000000000002'}">
		<!-- sub start -->
		<div  class="sub_container">
			<div class="sub_top sub_top01">
				<div class="navi">
						<span class="location">
							<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" /> <strong>회원가입 </strong>
						</span>
				</div>
				<h2>회원가입</h2>
			</div>

			<div id="content">
				<!-- 컨텐츠  -->

				<div class="join_step">
					<ul>
						<li>
							<span> Step1</span>
							<strong>회원약관 확인 및 동의</strong>
						</li>
						<li>
							<span> Step2</span>
							<strong>회원 가입인증</strong>
						</li>
						<li>
							<span> Step3</span>
							<strong>회원가입 정보입력</strong>
						</li>
						<li class="active">
							<span> Step4</span>
							<strong>회원가입 완료</strong>
						</li>
					</ul>
				</div>

				<div class="join_end">
					<strong>축하드립니다</strong>
					회원가입이 정상적으로 완료되었습니다
				</div>

				<!-- //컨텐츠  -->
			</div>

			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		</div>
		<!-- sub end -->
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
