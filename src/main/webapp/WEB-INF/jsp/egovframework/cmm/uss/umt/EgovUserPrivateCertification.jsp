<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="egovframework.com.utl.fcc.service.CurrUrlUtil" %>
<%
	//현재 Action 명 불러오기
	String action = (String)request.getAttribute("javax.servlet.forward.request_uri");
	String currUrl = action + CurrUrlUtil.currRetunUrlParameter(request);
	session.setAttribute("retUrl", currUrl);
%>

<script type="text/javascript">
	function fnPopup(url, name){
		var win = window.open(url, name, 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		if(win != null){
			win.focus();
		}
	}
</script>

<c:if test="${param.bbsId eq 'BBSMSTR_000000000008'}">
	<div class="clean_center">
		<div class="pbg2">
			<span class="red">’10.6.30 </span>일부로 녹색사업단의 회원님의 회원가입정보는 개인정보보호를 위해 모두 삭제되오며, 앞으로의 홈페이지 이용은 아래 공공아이핀인증과 실명인증 후에 사용하실 수 있습니다.<br /><br />

			기존 회원님을 위해 운영하였던 회원 로그인서비스는 <span class="red">’10.6.30</span> 일부로 종료되며 개인정보보호를 위해 회원가입을 받지 않습니다. 로그인이 필요한 서비스는 아래 아이핀 및 실명인증 후에 사용하실 수 있습니다.<br /><br />

			그동안 홈페이지의 로그인 서비스를 이용해 주셔서 대단히 감사드리며 앞으로도 녹색사업단의 웹서비스를 적극적으로 이용해 주시기 바랍니다.<br /><br />

			감사합니다
			<span class="bg"></span>
		</div>
	</div>
</c:if>
<div class="certification">
	<ul>
		<li class="ipin">
			<strong>아이핀(I-Pin)</strong>
			<span>인터넷 상에서 주민등록번호를 대신하여 아이핀<br /> 아이디와 비밀번호를 통해 인증합니다.</span>
			<a href="/gpin/ipin_main.jsp" title="새창열림" onclick="fnPopup(this.href, 'ipinPop');return false;">바로가기</a>
		</li>
		<li class="hp">
			<strong>휴대폰인증</strong>
			<span>고객님의 명의로 등록된 휴대폰 번호를 통해<br /> 본인인증을 합니다.</span>
			<a href="/gpin/mbl_main.jsp"  title="새창열림" onclick="fnPopup(this.href, 'mobilePop');return false;">바로가기</a>
		</li>
	</ul>
</div>
*본인인증 실패시 나이스신용평가정보 고객센터(1600-1522)로 문의하시면, 안내받으실 수 있습니다.

<%--
<br />
<br />
<a href="/uss/umt/cmm/userInfoTest.do" onclick="fnPopup(this.href, 'testPop');return false;" style="font-size:14pt; font-weight:bold;">개발자 인증</a>
--%>
