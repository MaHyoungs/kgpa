<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
	//인증후 리턴 시킬 URL
	session.setAttribute("retUrl", "/uss/umt/user/EgovUserInsertView.do");
%>
<c:import url="/msi/tmplatHead.do" charEncoding="utf-8" />

<c:if test="${not empty userManageVO.emailAdres}">
	<c:set var="emailArr" value="${fn:split(userManageVO.emailAdres, '@')}"/>
	<c:forEach items="${emailArr}" var="arr" varStatus="status">
		<c:if test="${status.count eq 1}"><c:set var="emailHead" value="${fn:trim(arr)}"/></c:if>
		<c:if test="${status.count eq 2}"><c:set var="emailBody" value="${fn:trim(arr)}"/></c:if>
	</c:forEach>
</c:if>
<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javaScript">
<c:if test="${result}">
	alert("<c:out value="${message}"/>");
</c:if>
	//이메일 관련 이벤트
	$(function(){
		var emailAdres3 = false;
		$('#emailAdres_2 option').each(function(){
			if($(this).val() == '${emailBody}' && $(this).val() != ''){
				$(this).prop('selected', true);
				emailAdres3 = true;
				$('#emailAdres_3').hide();
			}
		});
		if(!emailAdres3){
			$('#emailAdres_3').show();
			$('#emailAdres_3').val('${emailBody}');
		}
		$('#emailAdres_2').change(function(){
			if($(this).val() != ''){
				$('#emailAdres_3').hide();
			}else{
				$('#emailAdres_3').show();
				$('#emailAdres_3').focus();
			}
		});
		var nowDate = new Date();
		var todayUTC = new Date(Date.UTC(nowDate.getFullYear()+1, nowDate.getMonth(), nowDate.getDate()));
		var nowFullDate = todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
		fnDatepickerOptionAdd();
		var userDate = '${userManageVO.indvdlinfoPrsrvPd}';
		userDate = (Number(userDate.split("-")[0])+1) + "-" + userDate.split("-")[1] + "-" + userDate.split("-")[2];
		$("#indvdlinfoPrsrvPd").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(userDate), yearRange: "+1:+5"});
	});

	//다음 우편번호 API
	function fnDaumPostCodeSearchAPI(){
		new daum.Postcode({
			oncomplete: function(data) {
				$('#zip_1').val(data.postcode1);
				$('#zip_2').val(data.postcode2);
				$('#adres').val(data.address);
				$('#adresDetail').focus();
			}
		}).open();
	}

	//회원가입 Submit
	function memberInsertSubmit(){
		if($('#password_1').val() != $('#password_2').val()){
			alert("비밀번호가 서로 다릅니다.\n다시 입력해주십시오.");
			$('#password_1').val("");
			$('#password_2').val("");
			$('#password_1').focus();
			return false;
		} else if($('#password_1').val() == ""){
			alert("비밀번호는 영문과 숫자 조합으로 입력해주세요.");
			$('#password_1').focus();
			return false;
		}else if(fn_text_null_check($('input[type=text].null_false'))){
			//이메일,이동전화,일반전화,우편번호 취합
			var emailBody = "";
			if($('#emailAdres_2').val() != ''){
				emailBody = $('#emailAdres_2').val();
			}else{
				emailBody = $('#emailAdres_3').val();
			}

			$('#emailAdres').val($('#emailAdres_1').val()+"@"+emailBody);
			$('#moblphonNo').val($('#moblphonNo_1').val()+"-"+$('#moblphonNo_2').val()+"-"+$('#moblphonNo_3').val());
			$('#tlphonNo').val($('#tlphonNo_1').val()+"-"+$('#tlphonNo_2').val()+"-"+$('#tlphonNo_3').val());
			/**
			$('#faxphonNo').val($('#faxphonNo_1').val()+"-"+$('#faxphonNo_2').val()+"-"+$('#faxphonNo_3').val());
			$('#zip').val($('#zip_1').val()+""+$('#zip_2').val());
			**/
			return true;
		}else{
			return false;
		}
	}
</script>

<c:choose>
	<c:when test="${IS_MOBILE}">
	</c:when>
	<c:when test="${siteVO.siteId eq 'SITE_000000000000002'}">
		<!-- sub start -->
		<div  class="sub_container">

			<div class="sub_top sub_top01">
				<div class="navi">
						<span class="location">
							<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" /> <strong>회원정보</strong>
						</span>
				</div>
				<h2>회원정보</h2>
			</div>

			<div id="content">
				<form name="memberJoinForm" id="memberJoinForm" action="/uss/umt/cmm/EgovUserTempUpdate.do" method="post" onsubmit="return memberInsertSubmit();">

					<input type="hidden" name="userId"      value="${userManageVO.userId}"/>
					<input type="hidden" name="sexdstn"     value="${userManageVO.sexdstn}"/>
					<input type="hidden" name="credtId"     value="${userManageVO.credtId}"/>
					<input type="hidden" name="userSeCode"  value="08"/>
					<input type="hidden" name="brthdy"      value="${userManageVO.brthdy}"/>
					<input type="hidden" name="emailAdres" id="emailAdres"  value=""/>
					<input type="hidden" name="moblphonNo" id="moblphonNo"  value=""/>
					<input type="hidden" name="tlphonNo"   id="tlphonNo"    value=""/>
					<%--
					<input type="hidden" name="faxphonNo"  id="faxphonNo"   value=""/>
					<input type="hidden" name="zip"        id="zip" value="${userManageVO.zip}"/>
					--%>


					<h3 class="icon1">개인정보</h3>
					<div class="join_chart">
						<table summary="녹색자금 통합관리시스템 개인정보 입력표로 이름, 아이디, 비밀번호, 소속, 입니다" class="chart2">
							<caption>녹색자금 통합관리시스템 개인정보 입력</caption>
							<colgroup>
								<col width="20%" />
								<col width="*" />
							</colgroup>
							<tbody>
							<tr>
								<th scope="row">이름</th>
								<td>${userManageVO.userNm}</td>
							</tr>
							<tr>
								<th scope="row">아이디</th>
								<td>${userManageVO.userId}</td>
							</tr>
							<tr>
								<th scope="row"><label for="password_1">*비밀번호</label></th>
								<td><input type="password" class="inp validation mix_engnum" name="password" id="password_1" title="비밀번호" minlength="8" maxlength="20" /> 8~20자의 영문과 숫자 가능</td>
							</tr>
							<tr>
								<th scope="row"><label for="password_2">*비밀번호 확인</label></th>
								<td><input type="password" class="inp validation mix_engnum" name="password_2" id="password_2" title="비밀번호 확인" minlength="8" maxlength="20" /></td>
							</tr>
							<tr>
								<th scope="row">* 이메일 주소</th>
								<td>
									<input type="text" class="inp" name="emailAdres_1" id="emailAdres_1" value="${emailHead}" title="이메일 아이디입력"/> @
									<select title="이메일 도메인선택" name="emailAdres_2" id="emailAdres_2" >
										<option value="">직접입력</option>
										<option value="hanmail.net">다음(hanmail.net)</option>
										<option value="naver.com">네이버(naver.com)</option>
										<option value="nate.com">네이트(nate.com)</option>
										<option value="empal.com">엠파스(empal.com)</option>
										<option value="paran.com">파란(paran.com)</option>
										<option value="hanafos.com">하나포스(hanafos.com)</option>
										<option value="gmail.com">구글(gmail.com)</option>
										<option value="kornet.net">코넷(kornet.net)</option>
										<option value="korea.com">코리아닷컴(korea.com)</option>
										<option value="dreamwiz.com">드림위즈(dreamwiz.com)</option>
										<option value="lycos.co.kr">라이코스(lycos.co.kr)</option>
										<option value="chollian.net">천리안(chollian.net)</option>
										<option value="yahoo.co.kr">야후(yahoo.co.kr)</option>
										<option value="hotmail.com">핫메일(hotmail.com)</option>
									</select>
									<input type="text" class="inp" name="emailAdres_3" id="emailAdres_3" title="이메일 도메인 직접입력" value="${emailBody}" />
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="moblphonNo_1">* 이동전화</label></th>
								<td>
									<c:set var="moblArr" value="${fn:split(userManageVO.moblphonNo, '-')}"/>
									<c:forEach items="${moblArr}" var="arr" varStatus="status">
										<c:if test="${status.count eq 1}"><c:set var="moblphonNo_1" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 2}"><c:set var="moblphonNo_2" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 3}"><c:set var="moblphonNo_3" value="${fn:trim(arr)}"/></c:if>
									</c:forEach>
									<div class="tel_box">
										<select title="휴대 전화번호 앞번호 선택" name="moblphonNo_1" id="moblphonNo_1" class="inp null_false">
											<option value="">국번</option>
											<option value="010" <c:if test="${moblphonNo_1 eq '010'}"> selected="selected"</c:if>>010</option>
											<option value="011" <c:if test="${moblphonNo_1 eq '011'}"> selected="selected"</c:if>>011</option>
											<option value="016" <c:if test="${moblphonNo_1 eq '016'}"> selected="selected"</c:if>>016</option>
											<option value="017" <c:if test="${moblphonNo_1 eq '017'}"> selected="selected"</c:if>>017</option>
											<option value="018" <c:if test="${moblphonNo_1 eq '018'}"> selected="selected"</c:if>>018</option>
											<option value="019" <c:if test="${moblphonNo_1 eq '019'}"> selected="selected"</c:if>>019</option>
										</select>
										<input type="text" name="moblphonNo_2" id="moblphonNo_2" class="tel inp null_false validation number" value="${moblphonNo_2}" maxlength="4"  title="휴대전화번호 중간번호 입력" />
										<input type="text" name="moblphonNo_3" id="moblphonNo_3" class="tel inp null_false validation number" value="${moblphonNo_3}" maxlength="4"  title="휴대전화번호 마지막번호 입력" />
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="tlphonNo_1">&nbsp;&nbsp;전화번호</label></th>
								<td>
									<c:set var="tlphonArr" value="${fn:split(userManageVO.tlphonNo, '-')}"/>
									<c:forEach items="${tlphonArr}" var="arr" varStatus="status">
										<c:if test="${status.count eq 1}"><c:set var="tlphonNo_1" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 2}"><c:set var="tlphonNo_2" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 3}"><c:set var="tlphonNo_3" value="${fn:trim(arr)}"/></c:if>
									</c:forEach>
									<div class="tel_box">
										<select title="자택 전화번호 앞번호 선택" name="tlphonNo_1" id="tlphonNo_1" class="inp number">
											<option value="">국번</option>
											<option value="02" <c:if test="${tlphonNo_1 eq '02'}"> selected="selected"</c:if>>02</option>
											<option value="051" <c:if test="${tlphonNo_1 eq '051'}"> selected="selected"</c:if>>051</option>
											<option value="053" <c:if test="${tlphonNo_1 eq '053'}"> selected="selected"</c:if>>053</option>
											<option value="032" <c:if test="${tlphonNo_1 eq '032'}"> selected="selected"</c:if>>032</option>
											<option value="062" <c:if test="${tlphonNo_1 eq '062'}"> selected="selected"</c:if>>062</option>
											<option value="042" <c:if test="${tlphonNo_1 eq '042'}"> selected="selected"</c:if>>042</option>
											<option value="052" <c:if test="${tlphonNo_1 eq '052'}"> selected="selected"</c:if>>052</option>
											<option value="031" <c:if test="${tlphonNo_1 eq '031'}"> selected="selected"</c:if>>031</option>
											<option value="033" <c:if test="${tlphonNo_1 eq '033'}"> selected="selected"</c:if>>033</option>
											<option value="041" <c:if test="${tlphonNo_1 eq '041'}"> selected="selected"</c:if>>041</option>
											<option value="043" <c:if test="${tlphonNo_1 eq '043'}"> selected="selected"</c:if>>043</option>
											<option value="044" <c:if test="${tlphonNo_1 eq '044'}"> selected="selected"</c:if>>044</option>
											<option value="063" <c:if test="${tlphonNo_1 eq '063'}"> selected="selected"</c:if>>063</option>
											<option value="061" <c:if test="${tlphonNo_1 eq '061'}"> selected="selected"</c:if>>061</option>
											<option value="054" <c:if test="${tlphonNo_1 eq '054'}"> selected="selected"</c:if>>054</option>
											<option value="055" <c:if test="${tlphonNo_1 eq '055'}"> selected="selected"</c:if>>055</option>
											<option value="064" <c:if test="${tlphonNo_1 eq '064'}"> selected="selected"</c:if>>064</option>
											<option value="070" <c:if test="${tlphonNo_1 eq '070'}"> selected="selected"</c:if>>070</option>
										</select>
										<input type="text" name="tlphonNo_2" id="tlphonNo_2" class="inp number tel" value="${tlphonNo_2}" maxlength="4" title="전화번호 중간번호 입력"/>
										<input type="text" name="tlphonNo_3" id="tlphonNo_3" class="inp number tel" value="${tlphonNo_3}" maxlength="4" title="전화번호 마지막번호 입력"/>
									</div>
								</td>
							</tr>
							<%--
							<tr>
								<th scope="row"><label for="faxphonNo_1">팩스번호</label></th>
								<td>
									<c:set var="faxphonNoArr" value="${fn:split(userManageVO.faxphonNo, '-')}"/>
									<c:forEach items="${faxphonNoArr}" var="arr" varStatus="status">
										<c:if test="${status.count eq 1}"><c:set var="faxphonNo_1" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 2}"><c:set var="faxphonNo_2" value="${fn:trim(arr)}"/></c:if>
										<c:if test="${status.count eq 3}"><c:set var="faxphonNo_3" value="${fn:trim(arr)}"/></c:if>
									</c:forEach>
									<div class="tel_box">
										<select title="자택 전화번호 앞번호 선택" name="faxphonNo_1" id="faxphonNo_1" class="inp">
											<option value="">국번</option>
											<option value="02" <c:if test="${faxphonNo_1 eq '02'}"> selected="selected"</c:if>>02</option>
											<option value="051" <c:if test="${faxphonNo_1 eq '051'}"> selected="selected"</c:if>>051</option>
											<option value="053" <c:if test="${faxphonNo_1 eq '053'}"> selected="selected"</c:if>>053</option>
											<option value="032" <c:if test="${faxphonNo_1 eq '032'}"> selected="selected"</c:if>>032</option>
											<option value="062" <c:if test="${faxphonNo_1 eq '062'}"> selected="selected"</c:if>>062</option>
											<option value="042" <c:if test="${faxphonNo_1 eq '042'}"> selected="selected"</c:if>>042</option>
											<option value="052" <c:if test="${faxphonNo_1 eq '052'}"> selected="selected"</c:if>>052</option>
											<option value="031" <c:if test="${faxphonNo_1 eq '031'}"> selected="selected"</c:if>>031</option>
											<option value="033" <c:if test="${faxphonNo_1 eq '033'}"> selected="selected"</c:if>>033</option>
											<option value="041" <c:if test="${faxphonNo_1 eq '041'}"> selected="selected"</c:if>>041</option>
											<option value="043" <c:if test="${faxphonNo_1 eq '043'}"> selected="selected"</c:if>>043</option>
											<option value="044" <c:if test="${faxphonNo_1 eq '044'}"> selected="selected"</c:if>>044</option>
											<option value="063" <c:if test="${faxphonNo_1 eq '063'}"> selected="selected"</c:if>>063</option>
											<option value="061" <c:if test="${faxphonNo_1 eq '061'}"> selected="selected"</c:if>>061</option>
											<option value="054" <c:if test="${faxphonNo_1 eq '054'}"> selected="selected"</c:if>>054</option>
											<option value="055" <c:if test="${faxphonNo_1 eq '055'}"> selected="selected"</c:if>>055</option>
											<option value="064" <c:if test="${faxphonNo_1 eq '064'}"> selected="selected"</c:if>>064</option>
											<option value="070" <c:if test="${faxphonNo_1 eq '070'}"> selected="selected"</c:if>>070</option>
										</select>
										<input type="text" name="faxphonNo_2" id="faxphonNo_2" class="inp validation number tel" value="${faxphonNo_2}" maxlength="4" title="전화번호 중간번호 입력"/>
										<input type="text" name="faxphonNo_3" id="faxphonNo_3" class="inp validation number tel" value="${faxphonNo_3}" maxlength="4" title="전화번호 마지막번호 입력"/>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row">주소</th>
								<td>
									<div class="addr_box">
										<input type="text" name="zip_1" id="zip_1" value="${fn:substring(userManageVO.zip, 0, 3)}" class="inp null_false validation number zipcode" title="우편번호 앞번호" readonly="readonly"/> - <input type="text" name="zip_2" id="zip_2" value="${fn:substring(userManageVO.zip, 3, 6)}" class="inp null_false validation number zipcode" title="우편번호 뒷번호" readonly="readonly"/>
										<a href="/" title="우편번호검색창 새창열림" class="mbtn" onclick="fnDaumPostCodeSearchAPI(); return false;">우편번호검색</a><br />
										<input type="text" name="adres" id="adres" class="inp" title="주소입력" value="${userManageVO.adres}" />
										<input type="text" name="adresDetail" id="adresDetail" class="inp " title="상세주소입력" value="${userManageVO.adresDetail}" />
									</div>
								</td>
							</tr>
							--%>
							<tr>
							<th><label>생년월일</label></th>
							<td colspan="2">
								<c:choose>
									<c:when test="${fn:length(fn:trim(userManageVO.brthdy))>6}">
										<c:set var="brthdy1" value="${fn:substring(userManageVO.brthdy, 0,4)}"/>
										<c:set var="brthdy2" value="${fn:substring(userManageVO.brthdy, 4,6)}"/>
										<c:set var="brthdy3" value="${fn:substring(userManageVO.brthdy, 6,8)}"/>
									</c:when>
									<c:otherwise>
										<c:set var="brthdy1" value="19${fn:substring(userManageVO.brthdy, 0,2)}"/>
										<c:set var="brthdy2" value="${fn:substring(userManageVO.brthdy, 2,4)}"/>
										<c:set var="brthdy3" value="${fn:substring(userManageVO.brthdy, 4,6)}"/>
									</c:otherwise>
								</c:choose>
								<input type="radio" id="slrcldLrrCode" name="slrcldLrrCode" value="01" <c:if test="${userManageVO.slrcldLrrCode eq '01'}"> checked</c:if>/>양력
								<input type="radio" id="slrcldLrrCode" name="slrcldLrrCode" value="02" <c:if test="${userManageVO.slrcldLrrCode eq '02'}"> checked</c:if>/>음력
								<input type="text" value="${brthdy1}" name="brthdy1" id="brthdy1" size="4" maxlength="4" class="inp" readonly="readonly"/>년
								<input type="text" value="${brthdy2}" name="brthdy2" id="brthdy2" size="2" maxlength="2" class="inp" readonly="readonly"/>월
								<input type="text" value="${brthdy3}" name="brthdy3" id="brthdy3" size="2" maxlength="2" class="inp" readonly="readonly"/>일
							</td>
							</tr>
							<tr>
								<th><label >성별</label></th>
								<td colspan="2"><c:if test="${userManageVO.sexdstn=='M'}">남</c:if><c:if test="${userManageVO.sexdstn=='W'}">여</c:if></td>
							</tr>
							<tr>
								<th scope="row">* 메일수신</th>
								<td>
									<div class="radio_box">
										<input type="radio" name="emailRecptnAt" id="emailRecptnAt_y" value="Y" <c:if test="${empty userManageVO}">checked="checked" </c:if><c:if test="${userManageVO.emailRecptnAt eq 'Y'}">checked="checked" </c:if>/><label for="emailRecptnAt_y">수신</label>
										<input type="radio" name="emailRecptnAt" id="emailRecptnAt_n" value="N" <c:if test="${userManageVO.emailRecptnAt eq 'N'}">checked="checked" </c:if>/><label for="emailRecptnAt_n">거부</label>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row">* SMS 수신</th>
								<td>
									<div class="radio_box">
										<input type="radio" name="moblphonRecptnAt" id="moblphonRecptnAt_y" value="Y" <c:if test="${empty userManageVO}">checked="checked" </c:if><c:if test="${userManageVO.moblphonRecptnAt eq 'Y'}">checked="checked" </c:if>/><label for="moblphonRecptnAt_y">수신</label>
										<input type="radio" name="moblphonRecptnAt" id="moblphonRecptnAt_n" value="N" <c:if test="${userManageVO.moblphonRecptnAt eq 'N'}">checked="checked" </c:if>/><label for="moblphonRecptnAt_n">거부</label>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row">* 보존기간</th>
								<td>
									<div class="cal_box">
										<input type="text" class="inp null_false cal" name="indvdlinfoPrsrvPd" id="indvdlinfoPrsrvPd" value="${userManageVO.indvdlinfoPrsrvPd}" readonly="readonly" disabled="disabled"/>
										<button type="button" onclick="$(this).prev().focus();"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_cal.png" alt="달력선택"/></button>
										<span class="mbtn">
											<button type="button" onclick="$(this).parent().prev().prev().prop('disabled', false).focus();">연장</button>
										</span>
									</div>
									<p>잊혀질 권리에 따라 설정된 개인 정보 보존기간까지 개인 정보가 저장되며 연장하지 않을 시 설정일 이 지나면 자동 삭제됩니다.</p>
								</td>
							</tr>
							</tbody>
						</table>
					</div>

					<div class="btn_c">
						<span class="cbtn1"><button type="submit">변경</button></span>
						<span class="cbtn"><button type="button" onclick="location.href='/index.do'">취소</button></span>
					</div>
				</form>
			</div>

			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		</div>
		<!-- sub end -->
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
