<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<div id="conten">
	<div class="adobe_box">
		<strong>Adobe Reader</strong>
		<p>Adobe Reader가 정상적으로 설치되어 있지 않으신 고객님들께서는 아래의 버튼을 클릭하여 설치해 주시기 바랍니다.</p>
		<a href="http://get.adobe.com/kr/reader/?promoid=BPBSF" target="_blank" title="새창열림" class="cbtn"> <span class="icon_adobe">Adobe Reader 다운받기 </span></a>
	</div>

	<p class="mB40">
		녹색자료실에 있는 파일의 저작권은 녹색사업단(www.kgpa.or.kr)에 있습니다. 파일의 무단도용 및 재배포 금지하며 법적 문제 발생시 책임에 따른 법적 제재가 발생될 수 있습니다.
	</p>

	<c:forEach var="been" items="${commonCode}" varStatus="status">
		<h3 class="icon1"><c:out value="${been.codeNm}"/></h3>
			<div class="data_list">
				<ul>
				<c:forEach var="rs" items="${comtngnList}" varStatus="status">
					<c:if test="${rs.org_code eq been.code}">
						<li ><a href="#pop_layer" style="cursor:pointer;" onclick="return fnLayerOpen(this, 'pop_layer', '${rs.title}', '${rs.file_stre_cours}/${rs.img_stre_rnm}', '/cop/gds/FileDown.do?gn_id=${rs.gn_id}&org_code=${rs.org_code}', '${rs.file_stre_snm}');"><c:out value="${rs.title}" /></a></li>
					</c:if>
				</c:forEach>
				</ul>
			</div>
	</c:forEach>

	<div class="layer">
		<div class="bg"></div>
		<div class="pop_layer" id="pop_layer">
			<h4 class="gn_title"></h4>
			<div class="pop_content">
				<div class="data_info">
					<span class="img"><img width="130" height="150" class="gn_img" src="http://www.kgpa.or.kr/files/cms/2/1212852897.jpg" alt="2007 녹색건전성 평가 수상작 표지" /></span>
					<div class="cont">
						<ul>
							<li>기관명 : <span class="gn_sort"></span>녹색사업단</li>
							<li> 파일 : <a href="#link" target="_blank" class="gn_down" title="파일다운로드">1-1.PDF</a> </li>
						</ul>
					</div>
				</div>

				<div class="btn_c">
					<button type="button" class="cbtn pclose">닫기</button>
				</div>
			</div>
			<button type="button" class="pop_close pclose">레이어창 닫기</button>
		</div>
	</div>
</div>
