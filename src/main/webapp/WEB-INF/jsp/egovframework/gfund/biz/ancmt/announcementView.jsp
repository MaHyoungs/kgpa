<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<jsp:useBean id="toDay" class="java.util.Date" />
<fmt:formatDate var="toDate" value="${toDay}" pattern="yyyyMMdd" />

<% pageContext.setAttribute("newLineChar", "\n"); %>

<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/>녹색자금사업 공모 &gt; <strong>${announcementVo.biz_ty_code_nm}</strong>
		</span>
	</div>
	<h2>${announcementVo.biz_ty_code_nm}</h2>
</div>

<div id="content">

	<!--  공모페이지 -->
	<h3 class="icon1">공모내용</h3>
	<table class="chart2" summary="사업제안서 정보">
		<caption>사업제안서 정보</caption>
		<colgroup>
			<col width="20%"/>
			<col width=""/>
		</colgroup>
		<thead>
		<tr>
			<th>기준년도</th>
			<td>${announcementVo.year}</td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<th>사업분야</th>
			<td>${announcementVo.biz_ty_code_nm}</td>
		</tr>
		<tr>
			<th>제안서 접수기간</th>
			<td>${announcementVo.propse_papers_rcept_bgnde} ~ ${announcementVo.propse_papers_rcept_endde}</td>
		</tr>
		<tr>
			<th>사업내용</th>
			<td>
				<c:out value="${fn:replace(announcementVo.biz_cn, newLineChar, '<br/>')}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>첨부파일</th>
			<td>
				<ul class="file_list">
					<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
						<c:param name="param_atchFileId" value="${announcementVo.atch_file_id}" />
						<c:param name="style" value="gfund" />
					</c:import>
				</ul>
			</td>
		</tr>
		</tbody>
	</table>
	<fmt:formatNumber var="bizStartDate" value="${fn:replace(announcementVo.propse_papers_rcept_bgnde, '-', '')}" type="number"/>
	<fmt:formatNumber var="bizEndDate" value="${fn:replace(announcementVo.propse_papers_rcept_endde, '-', '')}" type="number"/>
	<fmt:formatNumber var="toDate" value="${toDate}" type="number"/>
	<fmt:formatDate var="hours" value="${toDay}" type="time" pattern="HH" />

	<div class="btn_c">
		<c:if test="${not empty bsifList}">
			<span class="cbtn2" style="margin: 0 10px;"><a href="/gfund/biz/bassinfo/basicInformationList.do">나의 진행사업</a></span>
		</c:if>

		<c:if test="${announcementVo.propse_rcept_at eq 'Y'}">
			<c:choose>
				<c:when test="${bizStartDate <= toDate and bizEndDate >= toDate}">
					<c:choose>
						<c:when test="${bizEndDate <= toDate and hours >= 18}">
							<span class="cbtn1" style="padding:0 20px;">제안서 접수가 마감되었습니다.</span>
						</c:when>
						<c:otherwise>
							<span class="cbtn2"><a href="/gfund/biz/bassinfo/basicInformationNewForm.do?year=${announcementVo.year}&amp;biz_ty_code=${announcementVo.biz_ty_code}">제안서 접수</a></span>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<span class="cbtn1" style="padding:0 20px;">제안서 접수가 마감되었습니다.</span>
				</c:otherwise>
			</c:choose>
		</c:if>
	</div>
	<!--  //공모페이지 -->
</div>