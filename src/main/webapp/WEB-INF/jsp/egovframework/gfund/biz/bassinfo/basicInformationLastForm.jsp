<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:set var="step1" value="false" />
<c:set var="step2" value="false" />
<c:set var="step3" value="false" />
<c:set var="step4" value="false" />
<c:forEach var="rs" items="${bsList}" varStatus="sts">
	<c:if test="${rs.step_cl eq 'STEP1' and rs.step_cl_use eq 1}"><c:set var="step1" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP2' and rs.step_cl_use eq 1}"><c:set var="step2" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP3' and rs.step_cl_use eq 1}"><c:set var="step3" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP4' and rs.step_cl_use eq 1}"><c:set var="step4" value="true" /></c:if>
</c:forEach>

<script src="/gfund/js/proposalForm.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javascript">
	<c:if test="${not empty param.saveRs}">
		<c:if test="${param.saveRs eq 1}">
			alert('현재까지 입력하신 내역이 저장 되었습니다.');
		</c:if>
	</c:if>

	//제안서 Sumbit
	function fnBasicInformationFormSubmit(){
		if($('#last_biz_plan_presentn_at').val() == 'Y'){
			if($('#area').val() == ''){
				if($('#sido').val() == ''){
					alert($('#sido').attr('title') + '를(을) 선택해 주십시오.');
					$('#sido').focus();
					return false;
				}else if($('#sidogungu').val() == ''){
					alert($('#sidogungu').attr('title') + '를(을) 선택해 주십시오.');
					$('#sidogungu').focus();
					return false;
				}
			}

			var chkCnt = 0;
			var chkCnt2 = 0;
			if($("input:checkbox[id='pj_01']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_02']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_03']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_04']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_05']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_06']").prop("checked", true)) {
				chkCnt ++;
			} else if($("input:checkbox[id='pj_07']").prop("checked", true)) {
				chkCnt ++;
			}

			if(chkCnt == 0) {
				alert('프로그램 참가대상을 선택해 주십시오.');
				return false;
			}

			if($("input:checkbox[id='oc_01']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_02']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_03']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_04']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_05']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_06']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_07']").prop("checked", true)) {
				chkCnt2 ++;
			} else if($("input:checkbox[id='oc_08']").prop("checked", true)) {
				chkCnt2 ++;
			}

			if(chkCnt2 == 0) {
				alert('대상특성을 선택해 주십시오.');
				return false;
			}

			if($("#tot_wct").val() < 1) {
				alert("총사업비를 입력하여 주십시요.");
				return false;
			}

			if(fn_text_null_check($('input[type=text], textarea, input[type=hidden].null_false'))){
				fnInputNameChange();
				return confirm("최종제출 후에는 수정이 불가능 합니다.\n" +
				"제출 하시겠습니까?");
			}else{
				return false
			}
		}else{
			fnInputNameChange();
			return true;
		}
	}
</script>

<c:set var="biz_sort_nm" value="" />
<c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}"><c:set var="biz_sort_nm" value="${btc.codeNm}" /></c:if></c:forEach>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/>녹색자금사업 공모  &gt; <strong>${biz_sort_nm}</strong>
		</span>
	</div>
	<h2><c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}">${btc.codeNm}</c:if></c:forEach></h2>
</div>

<div id="content">

	<!-- ${biz_sort_nm} 선정전형 -->
	<div class="tab">
		<ul>
			<li class="active"><a href="">선정전형</a></li>
			<li><a href="">사업관리</a></li>
			<li><a href="">결과산출</a></li>
		</ul>
	</div>

	<div class="tab_step">
		<ul>
			<li><a href="/gfund/biz/bassinfo/basicInformationView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">제안서 접수</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP1'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP1">1차심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP2'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP2">현장심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP3'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP3">2차심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP4'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP4">최종발표</a></li>
			<li class="active"><a href="" onclick="return false;">최종제출</a></li>
		</ul>
	</div>

	<c:if test="${bsifVo.biz_ty_code ne 'BTC08'}">
		<h3 class="icon1">사업제안서</h3>
	</c:if>
	<c:if test="${bsifVo.biz_ty_code eq 'BTC08'}">
		<h3 class="icon1">사업계획요약서</h3>
	</c:if>

	<div class="select_bar">
		<strong>${bsifVo.area}</strong>
	</div>

	<c:set var="biz_ty_code" />
	<c:choose>
		<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
		<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
		<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
	</c:choose>
	<div class="input_area">
		<form name="basicInformationForm" id="basicInformationForm" action="/gfund/biz/bassinfo/basicInformationSave.do" method="post" onsubmit="return fnBasicInformationFormSubmit();">
			<%-- 제안서 ID --%>
			<input type="hidden" name="biz_id" id="biz_id" value="${bsifVo.biz_id}"/>

			<c:if test="${bsifVo.propse_flag eq 'STEP12' and bsifVo.prst_ty_cc eq 'PRST01'}">
				<input type="hidden" name="propse_biz_id" id="propse_biz_id" value="${bsifVo.biz_id}"/>
			</c:if>

			<%-- 사업분류 --%>
			<input type="hidden" name="biz_ty_code" id="biz_ty_code" value="${biz_ty_code}"/>

			<%--전화번호 및 우편번호 분할 원본 --%>
			<input type="hidden" name="biz_zip" id="biz_zip" value="${bsifVo.biz_zip}"/>

			<%--제안요약서(기본정보) 지역 --%>
			<input type="hidden" name="area" id="area" value="${bsifVo.area}"/>

			<%--제안단계 --%>
			<input type="hidden" name="propse_flag" id="propse_flag" value="STEP12"/>

			<%--제출구분 제안서:PRST01, 최종제출:PRST02 --%>
			<input type="hidden" name="prst_ty_cc" id="prst_ty_cc" value="${bsifVo.prst_ty_cc}"/>

			<%--최종제출 여부 --%>
			<input type="hidden" name="last_biz_plan_presentn_at" id="last_biz_plan_presentn_at" value=""/>

			<%-- 제안서 기본 Form --%>
			<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeFinalBasicInformationForm.jsp"%>
			<%-- 제안서 기본 Form --%>

			<%-- 파일첨부 ID --%>
			<c:if test="${not empty bsifVo.last_biz_plan_presentn_at}">
				<input type="hidden" name="atch_file_id" id="atch_file_id" class="null_false" value="${bsifVo.atch_file_id}" title="첨부파일"/>
				<input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${bsifVo.atch_file_id}"/>
			</c:if>
			<c:if test="${empty bsifVo.last_biz_plan_presentn_at}">
				<input type="hidden" name="atch_file_id" id="atch_file_id" class="null_false" value="" title="첨부파일"/>
				<input type="hidden" name="gatch_file_id" id="gatch_file_id" value=""/>
			</c:if>
		</form>

		<c:choose>
			<c:when test="${biz_ty_code eq 'BTC05'}">
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 녹색자금통합관리시스템에 제출할 때에는 <strong>한글파일로 된 사업제안서 및 계획서(파일명 : 00기관_사업제안서)와 관련 증빙(응모자격 증빙, 개인정보동의서,</strong><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<strong>컨소시엄협약서, 파일명 : 00기관_관련 증빙)을 파일 분리하여 제출</strong>합니다.<br/>
					※ 증빙자료 미제출시 선정에서 제외됩니다.
				</div>
				<%--<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 사업계획요약서, 사업계획서, 동일대상을 반복적으로 교육하는 연속 숲체험교육의 경우 효과성 측정지 포함하여 <strong>하나의 한글파일로 제출합니다.</strong><br/>
					※ 응모자격 증빙, 컨소시엄협약서, 개인정보동의서는 <strong>각각 별도의 파일(JPG)로 제출합니다.</strong> <br/>
					※ 증빙자료 미제출시 선정에서 제외됩니다.
				</div>--%>
			</c:when>
			<c:when test="${biz_ty_code eq 'BTC08'}">
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
					※ 사업대상지 토지소유와 사업시행자(시.도 또는 시.군.자치구, 공익볍인)가 동일해야 합니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;국가나 다른 지자체·공공기관 소유의 토지인 경우에는 영구사용동의서를 제출해야합니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;(개인 소유, 공공기관이 아닌 법인이 소유하는 토지에는 사업시행 불가)
				</div>
			</c:when>
			<c:otherwise>
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
					※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;특수교육시설의 경우 개인정보이용동의서만 제출합니다.
				</div>
			</c:otherwise>
		</c:choose>

		<c:if test="${not empty bsifVo.last_biz_plan_presentn_at}">
			<%-- 파일첨부 Import --%>
			<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
				<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
				<c:param name="updateFlag" value="Y" />
				<c:param name="pathKey" value="Gfund.fileStorePath" />
				<c:param name="appendPath" value="basicInformation" />
				<c:param name="maxSize" value="31457280" />
				<c:param name="maxCount" value="10" />
				<c:param name="formAjaxJs" value="add" />
			</c:import>
			<%-- 파일첨부 Import --%>
		</c:if>
		<c:if test="${empty bsifVo.last_biz_plan_presentn_at}">
			<%-- 파일첨부 Import --%>
			<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
				<c:param name="param_atchFileId" value="" />
				<c:param name="updateFlag" value="Y" />
				<c:param name="pathKey" value="Gfund.fileStorePath" />
				<c:param name="appendPath" value="basicInformation" />
				<c:param name="maxSize" value="31457280" />
				<c:param name="maxCount" value="10" />
				<c:param name="formAjaxJs" value="add" />
			</c:import>
			<%-- 파일첨부 Import --%>
		</c:if>

		<ul class="icon2_list mT40">
			<li>‘<strong>저장</strong>’은 지금까지 작성한 내용을 저장합니다.</li>
			<li>‘<strong>제출</strong>’은 작성 내용을 제출하여 추후 수정이 불가능합니다.</li>
			<li>제안서 제출 시 ‘<strong>제출</strong>’버튼을 눌러 꼭 제출하여야 제안서가 접수됩니다.</li>
		</ul>
	</div>

	<div class="btn_c">
		<span class="cbtn"><button type="button" onclick="$('#last_biz_plan_presentn_at').val('N'); $('#basicInformationForm').submit();">저장</button></span>
		<span class="cbtn1"><button type="button" onclick="$('#last_biz_plan_presentn_at').val('Y'); $('#basicInformationForm').submit();">제출</button></span>
	</div>
</div>

<script type="text/javascript">
	(function(){
		//파일 업로드 Form Ajax
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>