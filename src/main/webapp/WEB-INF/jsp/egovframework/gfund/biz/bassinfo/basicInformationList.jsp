<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="toDay" class="java.util.Date"/>
<fmt:formatDate var="toDate" value="${toDay}" pattern="yyyyMMdd"/>

<script type="text/javascript">
</script>


<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet"/>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/> <strong>마이페이지</strong>
		</span>
	</div>
	<h2>마이페이지</h2>
</div>

<div id="content">

	<h3 class="icon1">제안사업</h3>
	<table class="chart1" summary="제안서">
		<caption>제안사업</caption>
		<colgroup>
			<col width="6%"/>
			<col width="24%"/>
			<col width="24%"/>
			<col width="15%"/>
			<col width="13%"/>
			<col width="12%"/>
			<col width="6%"/>
		</colgroup>
		<thead>
		<tr>
			<th>연도</th>
			<th>사업종류</th>
			<th>사업명</th>
			<th>지역</th>
			<th>작성일</th>
			<th>상태</th>
			<th>관리</th>
		</tr>
		</thead>
		<tbody>
		<c:set var="lstCnt" value="0"/>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<c:if test="${rs.prst_ty_cc eq 'PRST01' or empty rs.prst_ty_cc}">
				<c:set var="lstCnt" value="${lstCnt + 1}"/>
				<tr>
					<td>${rs.year}</td>
					<td>${rs.biz_ty_code_nm}</td>
					<td>${rs.biz_nm}</td>
					<td>${rs.area}</td>
					<td>${rs.frst_regist_pnttm}</td>
					<td>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM215"/>
							<c:param name="chVal" value="${rs.propse_flag}"/>
							<c:param name="elType" value="codeName"/>
						</c:import>
					</td>
					<td>
						<c:choose>
							<c:when test="${rs.propse_flag eq 'STEP01'}">
								<c:set var="bizEndDate" value="${fn:replace(rs.propse_papers_rcept_endde, '-', '')}" />
								<c:choose>
									<c:when test="${bizEndDate < toDate}">
										마감
									</c:when>
									<c:otherwise>
										<a href="/gfund/biz/bassinfo/basicInformationForm.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">수정</a>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP02'}">
								<a href="/gfund/biz/bassinfo/basicInformationView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP03'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP1">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP04'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP1">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP05'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP2">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP06'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP2">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP07'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP2">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP08'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP2">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP09'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP3">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP10'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP3">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP11'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP4">조회</a>
							</c:when>
							<c:when test="${rs.propse_flag eq 'STEP12'}">
								<a href="/gfund/biz/bassinfo/selection/selectionStepView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}&amp;step_cl=STEP4">조회</a>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:if>
		</c:forEach>

		<c:if test="${lstCnt eq 0}">
			<tr>
				<td colspan="7">제안사업 내역이 없습니다.</td>
			</tr>
		</c:if>
		</tbody>
	</table>

	<h3 class="icon1">진행사업</h3>
	<table class="chart1" summary="진행사업">
		<caption>진행사업</caption>
		<colgroup>
			<col width="6%"/>
			<col width="30%"/>
			<col width="30%"/>
			<col width="15%"/>
			<col width="13%"/>
			<col width="6%"/>
		</colgroup>
		<thead>
		<tr>
			<th>연도</th>
			<th>사업종류</th>
			<th>사업명</th>
			<th>지역</th>
			<th>작성일</th>
			<th>관리</th>
		</tr>
		</thead>
		<tbody>
		<c:set var="lstCnt" value="0"/>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<c:if test="${rs.year eq defaultYear.year and rs.prst_ty_cc eq 'PRST02'}">
				<c:set var="lstCnt" value="${lstCnt + 1}"/>
				<tr>
					<td>${rs.year}</td>
					<td>${rs.biz_ty_code_nm}</td>
					<td>${rs.biz_nm}</td>
					<td>${rs.area}</td>
					<td>${rs.frst_regist_pnttm}</td>
					<td>
						<c:choose>
							<c:when test="${rs.last_biz_plan_presentn_at eq 'Y'}">
								<a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">조회</a>
							</c:when>
							<c:when test="${rs.last_biz_plan_presentn_at eq 'N'}">
								<c:set var="bizEndDate" value="${fn:replace(rs.propse_papers_rcept_endde, '-', '')}" />
								<c:choose>
									<c:when test="${bizEndDate < toDate}">
										마감
									</c:when>
									<c:otherwise>
										<a href="/gfund/biz/bassinfo/basicInformationLastForm.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">수정</a>
									</c:otherwise>
								</c:choose>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:if>
		</c:forEach>

		<c:if test="${lstCnt eq 0}">
			<tr>
				<td colspan="7">진행사업 내역이 없습니다.</td>
			</tr>
		</c:if>
		</tbody>
	</table>


	<h3 class="icon1">종료사업</h3>
	<table class="chart1" summary="종료사업">
		<caption>종료사업</caption>
		<colgroup>
			<col width="6%"/>
			<col width="30%"/>
			<col width="30%"/>
			<col width="15%"/>
			<col width="13%"/>
			<col width="6%"/>
		</colgroup>
		<thead>
		<tr>
			<th>연도</th>
			<th>사업종류</th>
			<th>사업명</th>
			<th>지역</th>
			<th>작성일</th>
			<th>관리</th>
		</tr>
		</thead>
		<tbody>
		<c:set var="lstCnt" value="0"/>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<c:if test="${rs.year ne defaultYear.year and rs.prst_ty_cc eq 'PRST02'}">
				<c:set var="lstCnt" value="${lstCnt + 1}"/>
				<tr>
					<td>${rs.year}</td>
					<td>${rs.biz_ty_code_nm}</td>
					<td>${rs.biz_nm}</td>
					<td>${rs.area}</td>
					<td>${rs.frst_regist_pnttm}</td>
					<td>
						<c:choose>
							<c:when test="${rs.last_biz_plan_presentn_at eq 'Y'}">
								<a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">조회</a>
							</c:when>
							<c:when test="${rs.last_biz_plan_presentn_at eq 'N'}">
								<a href="/gfund/biz/bassinfo/basicInformationLastForm.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}&amp;biz_id=${rs.biz_id}">수정</a>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:if>
		</c:forEach>

		<c:if test="${lstCnt eq 0}">
			<tr>
				<td colspan="7">종료사업 내역이 없습니다.</td>
			</tr>
		</c:if>
		</tbody>
	</table>
</div>