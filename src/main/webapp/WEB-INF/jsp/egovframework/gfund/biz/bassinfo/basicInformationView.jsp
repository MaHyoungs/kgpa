<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<c:set var="biz_sort_nm" value="" />
<c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}"><c:set var="biz_sort_nm" value="${btc.codeNm}" /></c:if></c:forEach>

<c:if test="${param.saveRs eq 2}">
	<script type="application/javascript">
		alert('제안서 접수가 완료되었습니다.');
	</script>
</c:if>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/>녹색자금사업 공모  &gt; <strong>${biz_sort_nm}</strong>
		</span>
	</div>
	<h2><c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}">${btc.codeNm}</c:if></c:forEach></h2>
</div>

<div id="content">
	<div class="tab">
		<ul>
			<li class="active"><a href="" onclick="return false;">선정전형</a></li>
			<c:choose>
				<c:when test="${empty lastVo and bsifVo.last_biz_plan_presentn_at eq 'Y' and bsifVo.prst_ty_cc eq 'PRST02'}">
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
				</c:when>
				<c:when test="${not empty lastVo and lastVo.last_biz_plan_presentn_at eq 'Y' and lastVo.prst_ty_cc eq 'PRST02'}">
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${bsifVo.year}&amp;biz_ty_code=${lastVo.biz_ty_code}&amp;biz_id=${lastVo.biz_id}">사업관리</a></li>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do?year=${bsifVo.year}&amp;biz_ty_code=${lastVo.biz_ty_code}&amp;biz_id=${lastVo.biz_id}">사업관리</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">사업관리</a></li>
					<li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">결과산출</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>

	<div class="tab_step">
		<ul>
			<li<c:if test="${empty bsifVo.last_biz_plan_presentn_at or bsifVo.last_biz_plan_presentn_at eq 'N'}"> class="active"</c:if>><a href="">제안서 접수</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP1'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP1">1차심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP2'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP2">현장심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP3'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP3">2차심사</a></li>
			<li <c:if test="${bsVo.step_cl eq 'STEP4'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP4">최종발표</a></li>
			<c:choose>
				<c:when test="${bsVo.step_cl eq 'STEP4' and empty lastVo}">
					<li><a href="/gfund/biz/bassinfo/basicInformationLastForm.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">최종제출</a></li>
				</c:when>
				<c:when test="${bsVo.step_cl eq 'STEP4' and not empty lastVo}">
					<li><a href="/gfund/biz/bassinfo/basicInformationLastView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${lastVo.biz_id}">최종제출</a></li>
				</c:when>
				<c:when test="${not empty lastVo}">
					<li><a href="/gfund/biz/bassinfo/selection/selectionCompletion.do?biz_id=${lastVo.biz_id}&amp;biz_ty_code=${lastVo.biz_ty_code}">최종제출</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="/gfund/biz/bassinfo/selection/selectionCompletion.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}">최종제출</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>

	<c:if test="${empty bsifVo.last_biz_plan_presentn_at or bsifVo.last_biz_plan_presentn_at eq 'N'}">
		<c:if test="${bsifVo.biz_ty_code ne 'BTC08'}">
			<h3 class="icon1">사업제안서</h3>
		</c:if>
		<c:if test="${bsifVo.biz_ty_code eq 'BTC08'}">
			<h3 class="icon1">사업계획요약서</h3>
		</c:if>
	</c:if>
	<c:if test="${bsifVo.last_biz_plan_presentn_at eq 'Y'}">
		<h3 class="icon1">최종사업계획서</h3>
	</c:if>

	<form action="#">
		<div class="select_bar">
			<strong>${bsifVo.area}</strong>
		</div>
	</form>

	<c:set var="biz_ty_code" />
	<c:choose>
		<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
		<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
		<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
	</c:choose>
	<div class="input_area">

		<%-- 제안서 기본 Form --%>
		<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeBasicInformationView.jsp"%>
		<%-- 제안서 기본 Form --%>

		<c:choose>
			<c:when test="${biz_ty_code eq 'BTC05'}">
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 녹색자금통합관리시스템에 제출할 때에는 <strong>한글파일로 된 사업제안서 및 계획서(파일명 : 00기관_사업제안서)와 관련 증빙(응모자격 증빙, 개인정보동의서,</strong><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<strong>컨소시엄협약서, 파일명 : 00기관_관련 증빙)을 파일 분리하여 제출</strong>합니다.<br/>
					※ 증빙자료 미제출시 선정에서 제외됩니다.
				</div>
				<%--<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 사업계획요약서, 사업계획서, 동일대상을 반복적으로 교육하는 연속 숲체험교육의 경우 효과성 측정지 포함하여 <strong>하나의 한글파일로 제출합니다.</strong><br/>
					※ 응모자격 증빙, 컨소시엄협약서, 개인정보동의서는 <strong>각각 별도의 파일(JPG)로 제출합니다.</strong> <br/>
					※ 증빙자료 미제출시 선정에서 제외됩니다.
				</div>--%>
			</c:when>
			<c:when test="${biz_ty_code eq 'BTC08'}">
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
					※ 사업대상지 토지소유와 사업시행자(시.도 또는 시.군.자치구, 공익볍인)가 동일해야 합니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;국가나 다른 지자체·공공기관 소유의 토지인 경우에는 영구사용동의서를 제출해야합니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;(개인 소유, 공공기관이 아닌 법인이 소유하는 토지에는 사업시행 불가)
				</div>
			</c:when>
			<c:otherwise>
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
					※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;특수교육시설의 경우 개인정보이용동의서만 제출합니다.
				</div>
			</c:otherwise>
		</c:choose>

		<%-- 파일첨부 Import --%>
		<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
			<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
			<c:param name="pathKey" value="Gfund.fileStorePath" />
			<c:param name="appendPath" value="basicInformation" />
			<c:param name="maxSize" value="31457280" />
			<c:param name="maxCount" value="10" />
		</c:import>
		<%-- 파일첨부 Import --%>
	</div>
</div>