<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<table class="chart2" summary="${biz_sort_nm} 사업제안서 정보 입력">
	<caption>${biz_sort_nm} 사업제안서 정보 입력</caption>
	<colgroup>

		<%-- 복지 시설 나눔숲 (사회복지시설), 복지시설환경개선 --%>
		<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC09'}">
			<col width="10%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="15%"/>
			<col width="5%"/>
			<col width="5%"/>
			<col width="20%"/>
			<col width="20%"/>
		</c:if>

		<%-- 복지 시설 나눔숲 (특수복지시설), 숲체험ㆍ교육 <체험ㆍ휴양교육사업>, 다함께 나눔길 --%>
		<c:if test="${biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
			<col width="5%"/>
			<col width="5%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="10%"/>
			<col width="15%"/>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<col width="5%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="18%"/>
			<col width="10%"/>
			<col width="5%"/>
			<col width="10%"/>
			<col width="10%"/>
		</c:if>
	</colgroup>
	<tbody>

	<%-- 복지 시설 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th colspan="2">복지시설명(특수학교명)</th>
			<td colspan="2">${bsifVo.instt_nm}</td>
			<th colspan="2">설립유형</th>
			<td colspan="2">${bsifVo.fond_ty_cc_nm}</td>
		</tr>
		<tr>
			<th colspan="2">대표자(학교장)</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
			<th colspan="2">수용정원(명)</th>
			<td colspan="2">${bsifVo.aceptnc_psncpa}</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="2">${bsifVo.biz_nm}</td>
			<th colspan="2">수용현원(명)</th>
			<td colspan="2">${bsifVo.aceptnc_nownmpr}</td>
		</tr>
		<tr>
			<th colspan="2">사업유형 및 면적</th>
			<th>지상녹화(㎡)</th>
			<td>${bsifVo.ground_trplant_ar}</td>
			<th colspan="2">옥상녹화(㎡)</th>
			<td colspan="2">${bsifVo.rf_trplant_ar}</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>
		<tr>
			<th colspan="2">사업지</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th colspan="2">학교명</th>
			<td colspan="3">${bsifVo.instt_nm}</td>
			<th>설립유형</th>
			<td colspan="2">${bsifVo.fond_ty_cc_nm}</td>
		</tr>
		<tr>
			<th colspan="2">학교장</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
			<th>이용자수(명)</th>
			<td>${bsifVo.user_qy}</td>
			<th>관리자수(명)</th>
			<td>${bsifVo.mngr_qy}</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="6">${bsifVo.biz_nm}</td>
		</tr>
		<tr>
			<th colspan="2">숲유형</th>
			<td colspan="3">${bsifVo.frt_ty_spcl_nm}</td>
			<th>조성면적(㎡)</th>
			<td colspan="2">${bsifVo.make_ar}</td>
		</tr>
		<tr>
			<th rowspan="2" colspan="2">주요 수목명&nbsp;</th>
			<th>상록교목초(주)&nbsp;</th>
			<td colspan="2">${bsifVo.main_wdpt_01}</td>
			<th>관목(주)&nbsp;</th>
			<td colspan="2">${bsifVo.main_wdpt_03}</td>
		</tr>
		<tr>
			<th>낙엽교목(주)&nbsp;</th>
			<td colspan="2">${bsifVo.main_wdpt_02}</td>
			<th>본류(본)&nbsp;</th>
			<td colspan="2">${bsifVo.main_wdpt_04}</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>
		<tr>
			<th colspan="2">조성장소</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
			</td>
		</tr>
	</c:if>

	<%-- 지역사회 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC08'}">
		<tr>
			<th colspan="2">기관명</th>
			<td colspan="3">${bsifVo.instt_nm}</td>
			<th>대표자</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="<c:if test="${biz_ty_code eq 'BTC03'}">3</c:if><c:if test="${biz_ty_code eq 'BTC08'}">6</c:if>">${bsifVo.biz_nm}</td>
			<c:if test="${biz_ty_code eq 'BTC03'}">
				<th>사업유형</th>
				<td colspan="2">${bsifVo.frt_ty_area_nm}</td>
			</c:if>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>
		<c:if test="${biz_ty_code eq 'BTC03'}">
			<tr>
				<th rowspan="2" colspan="2">사업지</th>
				<td rowspan="2" colspan="3">
						${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
						${bsifVo.biz_adres} <br/>
						${bsifVo.biz_adres_detail}
				</td>
				<th rowspan="2">재정자립도</th>
				<th>순위</th>
				<td>${bsifVo.fnanc_idpdc_rank}</td>
			</tr>
			<tr>
				<th>퍼센트</th>
				<td>${bsifVo.fnanc_idpdc}(%)</td>
			</tr>
		</c:if>
		<c:if test="${biz_ty_code eq 'BTC08'}">
			<tr>
				<th colspan="2">사업지</th>
				<td colspan="6">
						${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
						${bsifVo.biz_adres} <br/>
						${bsifVo.biz_adres_detail}
				</td>
			</tr>
		</c:if>
	</c:if>

	<%-- 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC07'}">
		<tr>
			<th colspan="2">기관명</th>
			<td colspan="3">${bsifVo.instt_nm}</td>
			<th>대표자</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="6">${bsifVo.biz_nm}</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>
		<tr>
			<th colspan="2">사업지</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<tr>
			<th colspan="2">기관 및 단체명</th>
			<td colspan="2">${bsifVo.instt_nm}</td>
			<th colspan="2">컨소시엄 여부</th>
			<td colspan="2">
				<c:if test="${bsifVo.cnsrtm_at eq 'Y'}">예</c:if>
				<c:if test="${bsifVo.cnsrtm_at eq 'N'}">아니오</c:if>
			</td>
		</tr>
		<tr>
			<th colspan="2">대표자</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
			<th colspan="2">컨소시엄 단체·기관</th>
			<td colspan="2">${bsifVo.cnsrtm_entrps_nm}</td>
		</tr>
		<tr>
			<th colspan="2">사업명</th>
			<td colspan="6">${bsifVo.biz_nm}</td>
		</tr>

		<c:if test="${biz_ty_code eq 'BTC05'}">
			<tr>
				<th colspan="2">기관/단체 설립유형</th>
				<td colspan="6">${bsifVo.fond_ty_gi}</td>
			</tr>
			<tr>
				<th colspan="2">사업구분</th>
				<td colspan="6">${bsifVo.biz_se}</td>
			</tr>
			<tr>
				<th colspan="2">교육형태</th>
				<td colspan="2">${bsifVo.edc_stle}</td>
				<th colspan="2">교육기간</th>
				<td colspan="2">${bsifVo.edc_pd}</td>
			</tr>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05'}">
			<tr>
				<th colspan="2">프로그램 참가대상</th>
				<td colspan="2">
					<c:if test="${bsifVo.pj_01 eq 'pj_01'}">유아</c:if>
					<c:if test="${bsifVo.pj_02 eq 'pj_02'}">초등학생</c:if>
					<c:if test="${bsifVo.pj_03 eq 'pj_03'}">중학생</c:if>
					<c:if test="${bsifVo.pj_04 eq 'pj_04'}">고등학생</c:if>
					<c:if test="${bsifVo.pj_05 eq 'pj_05'}">가족</c:if>
					<c:if test="${bsifVo.pj_06 eq 'pj_06'}">성인</c:if>
					<c:if test="${bsifVo.pj_07 eq 'pj_07'}">기타</c:if>
				</td>
				<th colspan="2">대상특성</th>
				<td colspan="2">
					<c:if test="${bsifVo.oc_01 eq 'oc_01'}">학교폭력</c:if>
					<c:if test="${bsifVo.oc_02 eq 'oc_02'}">다문화</c:if>
					<c:if test="${bsifVo.oc_03 eq 'oc_03'}">자유학기제연계</c:if>
					<c:if test="${bsifVo.oc_04 eq 'oc_04'}">특수아동</c:if>
					<c:if test="${bsifVo.oc_05 eq 'oc_05'}">장애인</c:if>
					<c:if test="${bsifVo.oc_06 eq 'oc_06'}">특정질환자</c:if>
					<c:if test="${bsifVo.oc_07 eq 'oc_07'}">저소득</c:if>
					<c:if test="${bsifVo.oc_08 eq 'oc_08'}">기타</c:if>
				</td>
			</tr>

			<c:set var="ud_rss" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
			<c:set var="ut_rss" value="${bsifVo.ut_01_co + bsifVo.ut_02_co + bsifVo.ut_03_co + bsifVo.ut_04_co}" />
			<c:set var="at_rss" value="${bsifVo.at_01_co + bsifVo.at_02_co + bsifVo.at_03_co}" />

			<tr>
				<th rowspan="11" colspan="2">교육 대상</th>
				<th rowspan="4">소외계층 대상</th>
				<td rowspan="4" class="alC"><c:if test="${ud_rss ne 0}">선택</c:if></td>
				<th colspan="2">다문화</th>
				<td colspan="2">${bsifVo.ud_01_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">저소득</th>
				<td colspan="2">${bsifVo.ud_02_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">장애인</th>
				<td colspan="2">${bsifVo.ud_03_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">기타</th>
				<td colspan="2">${bsifVo.ud_04_co}(명)</td>
			</tr>
			<tr>
				<th rowspan="4">일반 유아·아동·청소년</th>
				<td rowspan="4" class="alC"><c:if test="${ut_rss ne 0}">선택</c:if></td>
				<th colspan="2">유아</th>
				<td colspan="2">${bsifVo.ut_03_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">아동</th>
				<td colspan="2">${bsifVo.ut_01_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">중학생</th>
				<td colspan="2">${bsifVo.ut_04_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">고등학생</th>
				<td colspan="2">${bsifVo.ut_02_co}(명)</td>
			</tr>
			<tr>
				<th rowspan="3">일반 성인</th>
				<td rowspan="3" class="alC"><c:if test="${at_rss ne 0}">선택</c:if></td>
				<th colspan="2">대학생</th>
				<td colspan="2">${bsifVo.at_01_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">일반인</th>
				<td colspan="2">${bsifVo.at_02_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">산주</th>
				<td colspan="2">${bsifVo.at_03_co}(명)</td>
			</tr>
			<tr>
				<th colspan="2">연인원</th>
				<td colspan="2"><span class="ud_ut_at_tot_txt">${ud_rss + ut_rss + at_rss}</span>명 (교육대상 합계)</td>
				<th colspan="2">소외계층 비율</th>
				<c:set var="ud_ut_at_tot_cnt" value="${ud_rss + ut_rss + at_rss}" />
				<c:set var="ud_co_tot_cnt" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
				<td colspan="2"><span class="ud_co_tot_cnt_txt"><fmt:formatNumber value="${ ud_co_tot_cnt / ud_ut_at_tot_cnt * 100}" pattern=".0"/></span>%</td>
			</tr>
		</c:if>

		<%-- 숲체험ㆍ교육 <휴양문화사업> --%>
		<c:if test="${biz_ty_code eq 'BTC06'}">
			<tr>
				<th colspan="2">사업 내용 </th>
				<td colspan="6">
					<table class="chart1">
						<colgroup>
							<col width="10%" />
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
						</colgroup>
						<thead>


						<c:set var="cs_rss" value="${bsifVo.cs_01_co + bsifVo.cs_02_co + bsifVo.cs_03_co + bsifVo.cs_04_co + bsifVo.cs_05_ct + bsifVo.cs_06_ct}" />
						<c:set var="ef_rss" value="${bsifVo.ef_01_co + bsifVo.ef_02_co + bsifVo.ef_03_co + bsifVo.ef_04_co + bsifVo.ef_05_ct + bsifVo.ef_06_ct}" />
						<c:set var="fl_rss" value="${bsifVo.fl_01_co + bsifVo.fl_02_co + bsifVo.fl_03_co + bsifVo.fl_04_co + bsifVo.fl_05_ct + bsifVo.fl_06_ct}" />
						<c:set var="etc_rss" value="${bsifVo.etc_02_co + bsifVo.etc_03_co + bsifVo.etc_04_ct + bsifVo.etc_05_ct}" />
						<tr>
							<th>세부유형 선택</th>
							<th colspan="2" class="vtop">문화·공연 <c:if test="${cs_rss ne 0}">선택</c:if></th>
							<th colspan="2" class="vtop">(체험)박람회 <c:if test="${ef_rss ne 0}">선택</c:if></th>
							<th colspan="2" class="vtop">산림레저활동<br />(걷기,등산,산악스키등) <c:if test="${fl_rss ne 0}">선택</c:if></th>
							<th colspan="2" class="vtop">기 타 <c:if test="${etc_rss ne 0}">선택</c:if></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<th rowspan="2">대상 </th>
							<th>소외계층 </th>
							<td>${bsifVo.cs_01_co}</td>
							<th>소외계층 </th>
							<td>${bsifVo.ef_01_co}</td>
							<th>소외계층 </th>
							<td>${bsifVo.fl_01_co}</td>
							<td colspan="2" rowspan="2">
									${bsifVo.etc_01}
							</td>
						</tr>
						<tr>
							<th>일반인</th>
							<td>${bsifVo.cs_02_co}</td>
							<th>일반인</th>
							<td>${bsifVo.ef_02_co}</td>
							<th>일반인</th>
							<td>${bsifVo.fl_02_co}</td>
						</tr>
						<tr>
							<th rowspan="2">규모 </th>
							<th>횟수 </th>
							<td>${bsifVo.cs_04_co}</td>
							<th>횟수 </th>
							<td>${bsifVo.ef_04_co}</td>
							<th>횟수 </th>
							<td>${bsifVo.fl_04_co}</td>
							<th>횟수</th>
							<td>${bsifVo.etc_03_co}</td>
						</tr>
						<tr>
							<th>회당(명)</th>
							<td>${bsifVo.cs_03_co}</td>
							<th>회당(명)</th>
							<td>${bsifVo.ef_03_co}</td>
							<th>회당(명)</th>
							<td>${bsifVo.fl_03_co}</td>
							<th>회당(명)</th>
							<td>${bsifVo.etc_02_co}</td>
						</tr>
						<tr>
							<th rowspan="2">비용 </th>
							<th>인당(천원) </th>
							<td><fmt:formatNumber value="${bsifVo.cs_06_ct}" groupingUsed="true"/></td>
							<th>인당(천원) </th>
							<td><fmt:formatNumber value="${bsifVo.ef_06_ct}" groupingUsed="true"/></td>
							<th>인당(천원) </th>
							<td><fmt:formatNumber value="${bsifVo.fl_06_ct}" groupingUsed="true"/></td>
							<th>인당(천원)</th>
							<td><fmt:formatNumber value="${bsifVo.etc_05_ct}" groupingUsed="true"/></td>
						</tr>
						<tr>
							<th>회당 (백만원)</th>
							<td><fmt:formatNumber value="${bsifVo.cs_05_ct}" groupingUsed="true"/></td>
							<th>회당 (백만원)</th>
							<td><fmt:formatNumber value="${bsifVo.ef_05_ct}" groupingUsed="true"/></td>
							<th>회당 (백만원)</th>
							<td><fmt:formatNumber value="${bsifVo.fl_05_ct}" groupingUsed="true"/></td>
							<th>회당 (백만원)</th>
							<td><fmt:formatNumber value="${bsifVo.etc_04_ct}" groupingUsed="true"/></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</c:if>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>

		<%-- 숲체험ㆍ교육 <체험교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05'}">
			<tr>
				<th colspan="2">교육장소</th>
				<td colspan="2">${bsifVo.edc_place}</td>
				<th colspan="2">1인당 교육비(천원)</th>
				<c:set var="ud_rss" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
				<c:set var="ut_rss" value="${bsifVo.ut_01_co + bsifVo.ut_02_co + bsifVo.ut_03_co + bsifVo.ut_04_co}" />
				<c:set var="at_rss" value="${bsifVo.at_01_co + bsifVo.at_02_co + bsifVo.at_03_co}" />
				<td colspan="2"><span id="one_edc_mny"><c:if test="${(bsifVo.tot_wct + ud_rss + ut_rss + at_rss) > 0}"><fmt:formatNumber value="${(bsifVo.tot_wct) / (ud_rss + ut_rss + at_rss) * 1000}" pattern="#,###,###,###.#"/></c:if></span></td>
			</tr>
		</c:if>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th colspan="2">복지시설명</th>
			<td colspan="2">${bsifVo.instt_nm}</td>
			<th colspan="2">설립유형</th>
			<td colspan="2">${bsifVo.fond_ty_cc_nm}</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="2">${bsifVo.biz_nm}</td>
			<th colspan="2">대표자</th>
			<td colspan="2">${bsifVo.rprsntv}</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
		</tr>
		<tr>
			<th colspan="2">사업지</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
			</td>
		</tr>
	</c:if>

	<%-- 기본정보 -> 복지 시설 나눔숲 (사회복지시설), 숲체험ㆍ교육 <체험교육사업>, 숲체험ㆍ교육 <휴양문화사업> --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<c:if test="${biz_ty_code eq 'BTC01'}"><c:set var="groupName" value="시설" /></c:if>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}"><c:set var="groupName" value="단체" /></c:if>
		<tr>
			<th rowspan="4">연<br/>락<br/>처</th>
			<th rowspan="2">주 소 </th>
			<td rowspan="2" colspan="2">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/>
						${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
			<th colspan="2">전화번호</th>
			<td colspan="2"><div class="tel_box">${bsifVo.tlphon_no}</div></td>
		</tr>
		<tr>
			<th colspan="2">이동전화</th>
			<td colspan="3"><div class="tel_box">${bsifVo.moblphon_no}</div></td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="2">${bsifVo.email_adres}</td>
			<th colspan="2">F A X</th>
			<td colspan="3">${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th colspan="2">성 명</th>
			<td colspan="3">${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th rowspan="3">연<br/>락<br/>처</th>
			<th>이동전화</th>
			<td colspan="2"><div class="tel_box">${bsifVo.moblphon_no}</div></td>
			<th colspan="2">전화번호</th>
			<td colspan="2"><div class="tel_box">${bsifVo.tlphon_no}</div></td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="2">${bsifVo.email_adres}</td>
			<th colspan="2">F A X</th>
			<td colspan="3">${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th colspan="2">성 명</th>
			<td colspan="3">${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
		<tr>
			<th rowspan="4">기관<br/>연락처</th>
			<th rowspan="2">주 소 </th>
			<td rowspan="2" colspan="4">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/>
						${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
			<th>전화번호</th>
			<td><div class="tel_box">${bsifVo.tlphon_no}</div></td>
		</tr>
		<tr>
			<th>이동전화</th>
			<td><div class="tel_box">${bsifVo.moblphon_no}</div></td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="4">${bsifVo.email_adres}</td>
			<th>F A X</th>
			<td>${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>소속부서</th>
			<td>${bsifVo.department}</td>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th>성 명</th>
			<td>${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th rowspan="5">학교<br />연락처</th>
			<th>학교<br/>주소</th>
			<td colspan="6">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/>
						${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
		</tr>

		<tr>
			<th rowspan="4">담당<br />교사</th>
			<th rowspan="2">성명</th>
			<td rowspan="2" colspan="2">${bsifVo.user_nm}</td>
			<th>전화번호(직통)</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.tlphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>이동전화</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.moblphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>소속(부서ㆍ과목)</th>
			<td colspan="2">${bsifVo.department}</td>
			<th>F A X</th>
			<td colspan="2" >${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>직책</th>
			<td colspan="2">${bsifVo.position}</td>
			<th>E-mail</th>
			<td colspan="2" >${bsifVo.email_adres}</td>
		</tr>
	</c:if>
	</tbody>
</table>

<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">사업계획요약서(기본정보)</h3>
</c:if>
<c:if test="${biz_ty_code ne 'BTC05' and biz_ty_code ne 'BTC09'}">
	<h3 class="icon1">제안요약서(기본정보)</h3>
</c:if>
<table class="chart2" summary="${biz_sort_nm} 제안요약서 정보 입력">
	<caption>${biz_sort_nm} 제안요약서 정보 입력</caption>
	<colgroup>
		<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
		<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC09'}">
			<col width="12%"/>
			<col width="auto"/>
			<col width="12%"/>
			<col width="auto"/>
		</c:if>

		<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
		<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<col class="basic4_1" />
			<col class="basic4_2" />
			<col class="basic4_3" />
			<col class="basic4_4" />
			<col class="basic4_5" />
			<col class="basic4_6" />
			<col class="basic4_7" />
			<col class="basic4_8" />
			<col class="basic4_9" />
		</c:if>
	</colgroup>
	<tbody>

	<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02'}">
		<tr>
			<th>지역</th>
			<td id="area_txt">${bsifVo.area}</td>
			<th>복지시설명(특수학교명)</th>
			<td id="instt_nm_txt">${bsifVo.instt_nm}(${bsifVo.fond_ty_cc_nm})</td>
		</tr>
	</c:if>

	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th>지역</th>
			<td id="area_txt">${bsifVo.area}</td>
			<th>복지시설명</th>
			<td id="instt_nm_txt">${bsifVo.instt_nm}(${bsifVo.fond_ty_cc_nm})</td>
		</tr>
	</c:if>
	<tr>
		<th <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">colspan="2"</c:if>>사업명</th>
		<td <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">colspan="4"</c:if><c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">colspan="4"</c:if> id="biz_nm_txt">${bsifVo.biz_nm}</td>
		<th <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">colspan="2"</c:if>><c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08' or biz_ty_code eq 'BTC09'}">사업장소</c:if><c:if test="${biz_ty_code eq 'BTC05'}">교육장소</c:if><c:if test="${biz_ty_code eq 'BTC06'}">사업유형</c:if></th>
		<td <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">colspan="4"</c:if> <c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">colspan="3"</c:if> id="biz_addr_txt">
			<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08' or biz_ty_code eq 'BTC09'}">
				<c:if test="${not empty bsifVo.biz_zip}">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
				</c:if>
				${bsifVo.biz_adres} <br/>
				${bsifVo.biz_adres_detail}
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC05'}">
				${bsifVo.edc_place}
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC06'}">
				<c:if test="${cs_rss ne 0}">문화·공연</c:if>
				<c:if test="${cs_rss ne 0 and ef_rss ne 0}">, </c:if>
				<c:if test="${ef_rss ne 0}">(체험)박람회</c:if>
				<c:if test="${ef_rss ne 0 and fl_rss ne 0}">, </c:if>
				<c:if test="${fl_rss ne 0}">산림레저활동(걷기,등산,산악스키등)</c:if>
				<c:if test="${fl_rss ne 0 and etc_rss ne 0}">, </c:if>
				<c:if test="${etc_rss ne 0}">기 타</c:if>
			</c:if>
		</td>
	</tr>

	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th>시설구분</th>
			<td><c:if test="${bsifVo.fclty_dv_cc eq 'FCLDV02'}">이용</c:if><c:if test="${bsifVo.fclty_dv_cc eq 'FCLDV01'}">생활</c:if></td>
			<th>시설유형</th>
			<td><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY01'}">아동</c:if><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY02'}">노인</c:if><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY03'}">장애인</c:if><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY04'}">정신질환</c:if><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY05'}">부랑인</c:if><c:if test="${bsifVo.fclty_ty_cc eq 'FCLTY06'}">기타</c:if></td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (사회복지시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th>녹화유형</th>
			<td id="green_type_txt">
				<c:if test="${not empty bsifVo.ground_trplant_ar}">지상녹화</c:if><c:if test="${not empty bsifVo.rf_trplant_ar}"><c:if test="${not empty bsifVo.ground_trplant_ar}">/</c:if>옥상녹화</c:if>
			</td>
			<th>시설유형</th>
			<td>${bsifVo.fclty_ty_cc_nm}</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th>숲유형</th>
			<td id="frt_ty_spcl_txt">${bsifVo.frt_ty_spcl_nm}</td>
			<th>장애유형</th>
			<td>${bsifVo.trobl_ty_cc_nm}</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
		<c:set var="gr_mny_tot" value="${bsifVo.gf_01_ct + bsifVo.gf_02_ct + bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct + bsifVo.gf_08_ct + bsifVo.gf_14_ct + bsifVo.gf_20_ct + bsifVo.gf_21_ct}" />
		<c:set var="ja_mny_tot" value="${bsifVo.gf_22_ct + bsifVo.gf_23_ct + bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct + bsifVo.sp_05_ct + bsifVo.sp_07_ct + bsifVo.sp_08_ct + bsifVo.sp_09_ct}" />
		<c:set var="gr_ja_mny_tot" value="${gr_mny_tot + ja_mny_tot}" />
		<tr>
			<th>사업비</th>
			<th>녹색자금<br />(백만원)</th>
			<td class="gf_vals_tot_txt"><fmt:formatNumber value="${gr_mny_tot}" groupingUsed="true"/></td>
			<td><span class="gf_vals_agv"><fmt:formatNumber value="${gr_mny_tot / gr_ja_mny_tot * 100}" pattern=".0"/></span>%</td>
			<th>자부담<br />(백만원)</th>
			<td class="jabudam_vals_tot_txt"><fmt:formatNumber value="${ja_mny_tot}" groupingUsed="true"/></td>
			<td><span class="jabudam_vals_agv"><fmt:formatNumber value="${ja_mny_tot / gr_ja_mny_tot * 100}" pattern=".0"/></span>%</td>
			<th>합계<br />(백만원)</th>
			<td id="biz_total_txt"><fmt:formatNumber value="${gr_ja_mny_tot}" groupingUsed="true"/></td>
			<td <c:if test="${biz_ty_code eq 'BTC08'}"> colspan="3" </c:if>>100%</td>
			<c:if test="${biz_ty_code ne 'BTC08'}">
				<th>재정자립도(순위)</th>
				<td id="fnanc_idpdc_rank_txt">${bsifVo.fnanc_idpdc_rank}</td>
			</c:if>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<tr>
			<th rowspan="2">기관개요</th>
			<th>설립년도</th>
			<td>${bsifVo.fond_year}</td>
			<th>대표자</th>
			<td id="rprsntv_txt">${bsifVo.rprsntv}</td>
			<th>상근인원(명)</th>
			<td>${bsifVo.fte_nmpr}</td>
			<th>법인허가<br />주무부처</th>
			<td>${bsifVo.cpr_prmisn_miryfc}</td>
		</tr>
		<tr>
			<th>주소</th>
			<td colspan="7">
				<div id="biz_zip">${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)}</div>
				<span id="biz_adres">${bsifVo.biz_adres}</span>
				<span id="biz_adres_detail">${bsifVo.biz_adres_detail}</span>
			</td>
		</tr>
	</c:if>
	</tbody>
</table>

<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
	<h3 class="icon1">제안요약서(사업비내역 - 녹색자금)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col class="cost_1"/>
				<col class="cost_2"/>
				<col class="cost_3"/>
				<col class="cost_4"/>
				<col class="cost_5"/>
				<col class="cost_6"/>
				<col class="cost_7"/>
				<col class="cost_7"/>
				<col class="cost_8"/>
				<col class="cost_9"/>
				<col class="cost_10"/>
				<col class="cost_11"/>
			</colgroup>
			<thead>
			<tr>
				<th rowspan="2">기본<br/>조사비</th>
				<th rowspan="2">설계<br/> 용역</th>
				<th colspan="5">공사원가</th>
				<th rowspan="2">감리비</th>
				<th rowspan="2">회의비</th>
				<th rowspan="2">행사비</th>
				<th rowspan="2">일반<br/>관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>재료비</th>
				<th>노무비</th>
				<th>경비</th>
				<th>기타비용</th>
				<th>계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="gf_vals">
				<td><fmt:formatNumber value="${bsifVo.gf_01_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_02_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_03_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_04_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_05_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_06_ct}" groupingUsed="true"/></td>
				<td id="gf_ct_tot_txt"><fmt:formatNumber value="${bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct}" groupingUsed="true"/></td>

				<td><fmt:formatNumber value="${bsifVo.gf_08_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_14_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_20_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_21_ct}" groupingUsed="true"/></td>
				<td class="gf_vals_tot_txt"><fmt:formatNumber value="${bsifVo.gf_01_ct + bsifVo.gf_02_ct + bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct + bsifVo.gf_08_ct + bsifVo.gf_14_ct + bsifVo.gf_20_ct+ bsifVo.gf_21_ct}" groupingUsed="true"/></td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%-- 복지시설환경개선 --%>
<c:if test="${biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">제안요약서(사업비내역 - 녹색자금)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col class="cost_1"/>
				<col class="cost_2"/>
				<col class="cost_3"/>
				<col class="cost_4"/>
				<col class="cost_5"/>
				<col class="cost_6"/>
				<col class="cost_7"/>
				<col class="cost_7"/>
				<col class="cost_8"/>
				<col class="cost_9"/>
				<col class="cost_10"/>
			</colgroup>
			<thead>
			<tr>
				<th>인건비</th>
				<th>회의비</th>
				<th>여비<br/>교통비</th>
				<th>지급<br/>수수료</th>
				<th>기본<br/>조사비</th>
				<th>설계비</th>
				<th>공사원가</th>
				<th>감리비</th>
				<th>일반<br/>관리비</th>
				<th>합계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="gf_vals">
				<td><fmt:formatNumber value="${bsifVo.gf_07_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_14_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_16_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_13_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_17_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_18_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_04_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_19_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_21_ct}" groupingUsed="true"/></td>
				<td class="gf_vals_tot_txt"><fmt:formatNumber value="${bsifVo.gf_07_ct + bsifVo.gf_14_ct + bsifVo.gf_16_ct + bsifVo.gf_13_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct + bsifVo.gf_21_ct}" groupingUsed="true"/></td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%--지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
	<h3 class="icon1">제안요약서(사업비내역 - 자부담)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col class="cost_1"/>
				<col class="cost_2"/>
				<col class="cost_3"/>
				<col class="cost_4"/>
				<col class="cost_5"/>
				<col class="cost_6"/>
				<col class="cost_7"/>
				<col class="cost_7"/>
				<col class="cost_8"/>
				<col class="cost_9"/>
				<col class="cost_10"/>
				<col class="cost_11"/>
			</colgroup>
			<thead>
			<tr>
				<th rowspan="2">기본<br/>조사비</th>
				<th rowspan="2">설계<br/> 용역</th>
				<th colspan="5">공사원가</th>
				<th rowspan="2">감리비</th>
				<th rowspan="2">회의비</th>
				<th rowspan="2">행사비</th>
				<th rowspan="2">일반<br/>관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>재료비</th>
				<th>노무비</th>
				<th>경비</th>
				<th>기타비용</th>
				<th>계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="jabudam_vals">
				<td><fmt:formatNumber value="${bsifVo.gf_22_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_23_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_01_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_02_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_03_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_04_ct}" groupingUsed="true"/></td>
				<td id="jabudam_sp_tot_txt"><fmt:formatNumber value="${bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_05_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_07_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_08_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_09_ct}" groupingUsed="true"/></td>
				<td class="jabudam_vals_tot_txt"><fmt:formatNumber value="${bsifVo.gf_22_ct + bsifVo.gf_23_ct + bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct + bsifVo.sp_05_ct + bsifVo.sp_07_ct + bsifVo.sp_08_ct + bsifVo.sp_09_ct}" groupingUsed="true"/></td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
	<h3 class="icon1">사업계획요약서(사업비구성)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table  class="chart1" summary="숲 체험교육(체험교육) 제안요약서(사업비 구성)  정보 입력" style="margin-bottom: 0;">
			<caption>숲 체험교육(체험교육) 제안요약서(사업비 구성) 정보 입력</caption>
			<thead>
			<tr>
				<th colspan="3">인건비</th>
				<th colspan="11">직접비</th>
				<th rowspan="2">일반<br />관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>내부 인건비</th>
				<th>외부인건비</th>
				<th>강사비</th>
				<th>소모품비</th>
				<th>광고선전비</th>
				<th>도서인쇄비</th>
				<th>지급수수료</th>
				<th>회의비</th>
				<th>사업진행비</th>
				<th>여비교통비</th>
				<th>기본조사비</th>
				<th>설계용역비</th>
				<th>공사원가</th>
				<th>감리비</th>
			</tr>
			</thead>
			<tbody>
			<tr class="gf_ct_vals">
				<td><fmt:formatNumber value="${bsifVo.gf_07_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_08_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_09_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_10_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_11_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_12_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_13_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_14_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_15_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_16_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_17_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_18_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.sp_04_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_19_ct}" groupingUsed="true"/></td>
				<td><fmt:formatNumber value="${bsifVo.gf_21_ct}" groupingUsed="true"/></td>
				<td id="gf_ct_tot_txt"><fmt:formatNumber value="${bsifVo.gf_07_ct + bsifVo.gf_08_ct + bsifVo.gf_09_ct + bsifVo.gf_10_ct + bsifVo.gf_11_ct + bsifVo.gf_12_ct + bsifVo.gf_13_ct + bsifVo.gf_14_ct + bsifVo.gf_15_ct + bsifVo.gf_16_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct + bsifVo.gf_21_ct}" groupingUsed="true"/></td>
			</tr>
			</tbody>
		</table>
		<table class="chart1" summary="숲 체험교육(체험교육) 제안요약서(사업비 구성)  정보" style="border-top: 0;">
			<caption>숲 체험교육(체험교육) 제안요약서(사업비 구성) 정보</caption>
			<colgroup>
				<col width="25%"/>
				<col width="25%"/>
				<col width="25%"/>
				<col width="25%"/>
			</colgroup>
			<thead>
			<tr>
				<th>합계</th>
				<th>인건비</th>
				<th>직접비</th>
				<th>일반 관리비</th>
			</thead>
			<tbody>

			<c:set var="gf_vals_avg" value="${bsifVo.gf_07_ct + bsifVo.gf_08_ct + bsifVo.gf_09_ct + bsifVo.gf_10_ct + bsifVo.gf_11_ct + bsifVo.gf_12_ct + bsifVo.gf_13_ct + bsifVo.gf_14_ct + bsifVo.gf_15_ct + bsifVo.gf_16_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct + bsifVo.gf_21_ct}" />

			<tr id="gf_ct_vals_age">
				<td>${gf_vals_avg} (<c:if test="${gf_vals_avg gt 0}" >100</c:if><c:if test="${gf_vals_avg eq 0}" >0</c:if>%)</td>
				<td>${bsifVo.gf_07_ct + bsifVo.gf_08_ct + bsifVo.gf_09_ct} (<fmt:formatNumber value="${ ((bsifVo.gf_07_ct + bsifVo.gf_08_ct + bsifVo.gf_09_ct) / gf_vals_avg) * 100}" pattern=".0"/>%)</td>
				<td>${bsifVo.gf_10_ct + bsifVo.gf_11_ct + bsifVo.gf_12_ct + bsifVo.gf_13_ct + bsifVo.gf_14_ct + bsifVo.gf_15_ct + bsifVo.gf_16_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct} (<fmt:formatNumber value="${ ((bsifVo.gf_10_ct + bsifVo.gf_11_ct + bsifVo.gf_12_ct + bsifVo.gf_13_ct + bsifVo.gf_14_ct + bsifVo.gf_15_ct + bsifVo.gf_16_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct) / gf_vals_avg) * 100}" pattern=".0"/>%)</td>
				<td>${bsifVo.gf_21_ct} (<fmt:formatNumber value="${ (bsifVo.gf_21_ct / gf_vals_avg) * 100}" pattern=".0"/>%)</td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC06'}">
	<div class="top_tit">
		<h3 class="icon1">제안요약서(사업연속성)</h3>
	</div>
	<table  class="chart1" summary="숲 체험교육(체험교육) 제안요약서(사업연속성) 정보 입력">
		<caption>숲 체험교육(체험교육) 제안요약서(사업연속성)정보 입력</caption>
		<colgroup>
			<col width="11%"/>
			<col width="auto"/>
			<col width="15%"/>
			<col width="15%"/>
			<col width="20%"/>
		</colgroup>
		<thead>
		<tr>
			<th>구분(년도)</th>
			<th>사업명</th>
			<th>녹색자금(백만원)</th>
			<th>수혜 연인원(명)</th>
			<th>평가결과</th>
		</tr>
		</thead>
		<tbody class="bizCotnSeq">
		<c:if test="${not empty bcsList}">
			<c:forEach var="rs" items="${bcsList}" varStatus="sts">
				<tr>
					<td>${rs.year}</td>
					<td>${rs.biz_nm}</td>
					<td><fmt:formatNumber value="${rs.green_fund}" groupingUsed="true"/></td>
					<td>${rs.rcvfvr_year_nmpr}</td>
					<td>${rs.evl_result}</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${empty bcsList}">
			<tr>
				<td colspan="5">등록된 사업연속성이 없습니다.</td>
			</tr>
		</c:if>
		</tbody>
	</table>
</c:if>

<h3 class="icon1">제안요약서(사업개요)</h3>
<table class="chart2" summary="${biz_sort_nm} 제안요약서(사업개요) 정보 입력">
<caption>${biz_sort_nm} 제안요약서(사업개요) 정보 입력</caption>
<colgroup>
	<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08' or biz_ty_code eq 'BTC09'}">
		<col class="outline_1"/>
		<col class="outline_2"/>
		<col class="outline_3"/>
		<col class="outline_4"/>
		<col class="outline_5"/>
	</c:if>
	<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<col class="expost_1"/>
		<col class="expost_2"/>
	</c:if>
</colgroup>
<tbody>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲  --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC08'}">
	<tr>
		<th rowspan="2">
			<c:if test="${biz_ty_code eq 'BTC08'}">
				토지소유
			</c:if>
			<c:if test="${biz_ty_code ne 'BTC08'}">
				토지소유자 및 조성면적
			</c:if>
		</th>
		<th>
			조성면적(㎡)
		</th>
		<td>
			<c:if test="${empty bsifVo.make_ar}">${bsifVo.ground_trplant_ar + bsifVo.rf_trplant_ar}</c:if>
			<c:if test="${not empty bsifVo.make_ar}">${bsifVo.make_ar}</c:if>
		</td>
		<th>
			<c:choose>
				<c:when test="${biz_ty_code eq 'BTC03'}">
					총대상지면적(㎡)
				</c:when>
				<c:when test="${biz_ty_code eq 'BTC08'}">
					나눔길길이(km)
				</c:when>
				<c:otherwise>
					시설전체면적(㎡)
				</c:otherwise>
			</c:choose>
		</th>
		<td>${bsifVo.fclty_all_ar}</td>
	</tr>
	<tr>
		<td colspan="4">
			※ Daum지도 20m 축척상태에서 면적측정기능을 이용하여 측정한 수치를 기입
		</td>
	</tr>
</c:if>

<%-- 복지시설환경개선  --%>
<c:if test="${biz_ty_code eq 'BTC09'}">
	<tr>
		<th>건물현황</th>
		<td colspan="2">${bsifVo.build_st_cc_nm}<c:if test="${bsifVo.build_st_cc eq 'BUIST05'}"> (${bsifVo.build_st_dt})</c:if></td>
		<th>건물위치</th>
		<td colspan="2">${bsifVo.build_lc_cc_nm}<c:if test="${bsifVo.build_lc_cc eq 'BUILC03'}"> (${bsifVo.build_lc_dt} 층)</c:if></td>
	</tr>
	<tr>
		<th>건물 준공일</th>
		<td colspan="2">${bsifVo.build_fond_year} 년	 ${bsifVo.build_fond_month} 월</td>
		<th>전용면적</th>
		<td colspan="2">${bsifVo.own_ar1} ㎡ . ${bsifVo.own_ar2} ㎡</td>
	</tr>
	<tr>
		<th>거주유형</th>
		<td colspan="2">${bsifVo.resi_ty_cc_nm}<c:if test="${bsifVo.resi_ty_cc eq 'RESTY02'}"> (${bsifVo.resi_ty_dt})</c:if></td>
		<th>조성면적 및 <br/> 조성공간 개소수</th>
		<td colspan="2">${bsifVo.make_ar} ㎡ , ${bsifVo.make_place_num} 실</td>
	</tr>
	<tr>
		<th>5년이내 용도변경 <br/> 예정여부</th>
		<td colspan="4">${bsifVo.typ_ch_cc_nm}</td>
	</tr>
	<tr>
		<th rowspan="2">이용인 수(일평균)</th>
		<th>시설생활 이용인 수</th>
		<td>${bsifVo.fclty_user_qy}</td>
		<th>조성공간의 이용자 수</th>
		<td>${bsifVo.make_user_qy}</td>
	</tr>
	<tr>
		<td colspan="4">
			※ 시설의 등록자 또는 회원 등 확인 가능한 인원수 기재
		</td>
	</tr>
</c:if>

<%-- 다함께 나눔길  --%>
<c:if test="${biz_ty_code eq 'BTC07'}">
	<tr>
		<th rowspan="2">토지소유자 및 조성면적</th>
		<th>조성면적(㎡)</th>
		<td>${bsifVo.make_ar}</td>
		<th>나눔길 길이(m)</th>
		<td>${bsifVo.fclty_all_ar}</td>
	</tr>
	<tr>
		<td colspan="4">
			※ Daum지도 20m 축척상태에서 면적측정기능을 이용하여 측정한 수치를 기입
		</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲 (사회복지시설) --%>
<c:if test="${biz_ty_code eq 'BTC01'}">
	<tr>
		<th>조성유형</th>
		<td colspan="4">${bsifVo.make_ty_nm}</td>
	</tr>
	<tr>
		<th>수용정원(명)</th>
		<td colspan="4">${bsifVo.aceptnc_psncpa}</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲 (특수교육시설) --%>
<c:if test="${biz_ty_code eq 'BTC02'}">
	<tr>
		<th>이용자수(관리자 포함) </th>
		<td colspan="4" id="user_qy_txt">${bsifVo.user_qy + bsifVo.mngr_qy}</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08' or biz_ty_code eq 'BTC09'}">
	<tr>
		<th>조성대상지 현재 상태</th>
		<td colspan="4">
			<div class="area_box"><c:out value="${fn:replace(bsifVo.tp_now_sttus, newLineChar, '<br/>')}" escapeXml="false" /></div>
		</td>
	</tr>
</c:if>

<%-- 복지시설환경개선 --%>
<c:if test="${biz_ty_code eq 'BTC09'}">
	<tr>
		<th>사업목적</th>
		<td colspan="4">
			<div class="area_box"><c:out value="${fn:replace(bsifVo.biz_purps, newLineChar, '<br/>')}" escapeXml="false" /></div>
		</td>
	</tr>
	<tr>
		<th>사업내용</th>
		<td colspan="4">
			<div class="area_box"><c:out value="${fn:replace(bsifVo.biz_contents, newLineChar, '<br/>')}" escapeXml="false" /></div>
		</td>
	</tr>
	<tr>
		<th>기대효과</th>
		<td colspan="4">
			<div class="area_box"><c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" /></div>
		</td>
	</tr>
</c:if>

<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
	<tr>
		<th>대상부지</th>
		<td colspan="4">
			<c:if test="${bsifVo.tp_drng_at eq 'Y'}">배수공사 필요 </c:if>
			<c:if test="${bsifVo.tp_remvl_at eq 'Y'}">포장 등 철거공사 필요 </c:if>
			<c:if test="${bsifVo.tp_splemnt_at eq 'Y'}">기존 수목보완 필요 </c:if>
		</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲  --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC08'}">
	<tr>
		<th>
			<c:if test="${biz_ty_code ne 'BTC08'}">
				<c:if test="${biz_ty_code ne 'BTC03'}">
					대상부지 이용현황
				</c:if>
				<c:if test="${biz_ty_code eq 'BTC03'}">
					지역사회나눔숲 사항
				</c:if>
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC08'}">조성대상지<br/>이용현황</c:if>
		</th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.tp_use_sttus, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
	<tr>
	<th>사업내용 (단위 : 백만원)</th>
	<td colspan="4">
	<table class="chart1" summary="사업내용 내용입력">
	<caption>사업내용 내용입력</caption>
	<c:if test="${biz_ty_code ne 'BTC08'}">
		<colgroup>
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
			<col width="20%"/>
		</colgroup>
	</c:if>
	<thead>
	<c:if test="${biz_ty_code ne 'BTC08'}">
		<tr>
			<th>식재공사비</th>
			<th>기반조성비</th>
			<th>시설물공사비</th>
			<th>기타비용</th>
			<th>공사원가</th>
		</tr>
		</thead>
		<tbody>
		<tr id="biz_ct">
			<c:set var="biz_ct_tot" value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" />
			<td><fmt:formatNumber value="${bsifVo.plt_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.plt_ct}"><fmt:formatNumber value="${ bsifVo.plt_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.fnd_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.fnd_ct}"><fmt:formatNumber value="${ bsifVo.fnd_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.fct_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.fct_ct}"><fmt:formatNumber value="${ bsifVo.fct_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.etc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.etc_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><span id="biz_ct_tot"><fmt:formatNumber value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" groupingUsed="true"/></span> (100%)</td>
		</tr>
		</tbody>
	</c:if>
	<c:if test="${biz_ty_code eq 'BTC08'}">
		<tr>
			<th>공통공사비</th>
			<th>노면공사비</th>
			<th>녹화복원공사비</th>
			<th>지형복원공사비</th>
			<th>시설공사비</th>
			<th>기타비용</th>
			<th>공사원가</th>
		</tr>
		</thead>
		<tbody>
		<tr id="biz_ct">
			<c:set var="biz_ct_tot" value="${bsifVo.cc_ct + bsifVo.rsc_ct + bsifVo.tprc_ct + bsifVo.trc_ct + bsifVo.fclts_ct + bsifVo.etc_ct}"/>
			<td><fmt:formatNumber value="${bsifVo.cc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.cc_ct}"><fmt:formatNumber value="${ bsifVo.cc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.rsc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.rsc_ct}"><fmt:formatNumber value="${ bsifVo.rsc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.tprc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.tprc_ct}"><fmt:formatNumber value="${ bsifVo.tprc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.trc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.trc_ct}"><fmt:formatNumber value="${ bsifVo.trc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.fclts_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.fclts_ct}"><fmt:formatNumber value="${ bsifVo.fclts_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><fmt:formatNumber value="${bsifVo.etc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.etc_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
			<td><span id="biz_ct_tot"><fmt:formatNumber value="${biz_ct_tot}" groupingUsed="true"/></span> (100%)</td>
		</tr>
		</tbody>
	</c:if>
	</table>
	</td>
	</tr>
</c:if>

<%-- 다함께 나눔길  --%>
<c:if test="${biz_ty_code eq 'BTC07'}">
	<tr>
		<th>사업비 (단위 : 백만원)</th>
		<td colspan="4">
			<table class="chart1" summary="사업내용 내용입력">
				<caption>사업내용 내용입력</caption>
				<colgroup>
					<col width="20%"/>
					<col width="20%"/>
					<col width="20%"/>
					<col width="20%"/>
					<col width="20%"/>
				</colgroup>
				<thead>
				<tr>
					<th>식재공사비</th>
					<th>기반조성비</th>
					<th>시설물공사비</th>
					<th>기타비용</th>
					<th>공사원가</th>
				</tr>
				</thead>
				<tbody>
				<tr id="biz_ct">
					<c:set var="biz_ct_tot" value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" />
					<td><fmt:formatNumber value="${bsifVo.plt_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.plt_ct}"><fmt:formatNumber value="${ bsifVo.fnd_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
					<td><fmt:formatNumber value="${bsifVo.fnd_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.fnd_ct}"><fmt:formatNumber value="${ bsifVo.fct_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
					<td><fmt:formatNumber value="${bsifVo.fct_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.fct_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
					<td><fmt:formatNumber value="${bsifVo.etc_ct}" groupingUsed="true"/> (<span><c:if test="${not empty bsifVo.etc_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
					<td><span id="biz_ct_tot"><fmt:formatNumber value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" groupingUsed="true"/></span> (100%)</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02'}">
	<tr>
		<th>식재공사내역</th>
		<th>교목류(주)</th>
		<td>${bsifVo.pcp_qy}</td>
		<th>관목 및 초화류(본)</th>
		<td>${bsifVo.psf_qy}</td>
	</tr>
	<tr>
		<th>기반공사내역</th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.isw_cn, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
	<tr>
		<th>시설물공사내역</th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.fcw_cn, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>

<c:if test="${biz_ty_code ne 'BTC09'}">
	<tr>
		<th>사업목적</th>
		<td <c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC08'}"> colspan="4"</c:if>>
			<c:out value="${fn:replace(bsifVo.biz_purps, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>

<%-- 지역 사회 나눔숲 --%>
<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC08'}">
	<tr>
		<c:if test="${biz_ty_code ne 'BTC08'}">
			<th>지역사회</th>
			<td colspan="4">
				<p>나눔숲의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안</p>
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.ud_bf_dstnct_method, newLineChar, '<br/>')}" escapeXml="false" />
				</div>
			</td>
		</c:if>
		<c:if test="${biz_ty_code eq 'BTC08'}">
			<th>사회적 약자층을 위하여 ‘무장애 나눔길’ 조성 특성화 (UD, BF 반영 등) 방안</th>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.ud_bf_dstnct_method, newLineChar, '<br/>')}" escapeXml="false" />
				</div>
			</td>
		</c:if>
	</tr>
	<tr>
		<th>기대효과 </th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>

<%-- 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC07'}">
	<tr>
		<th>사업내용 및 차별화 방안</th>
		<td colspan="4">
			<p>나눔길의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안</p>
			<div class="area_box">
				<c:out value="${fn:replace(bsifVo.ud_bf_dstnct_method, newLineChar, '<br/>')}" escapeXml="false" />
			</div>
		</td>
	</tr>
	<tr>
		<th>기대효과 </th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길  --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
	<tr>
		<th>주요 추진일정</th>
		<td colspan="4">
			<ul class="icon3_list cal_box_list">
				<li>
					<span>주민설명회 </span>
					<div class="cal_box">
							${bsifVo.prtnfx_dc_dt}
					</div>
				</li>
				<li>
					<span>기본 및 실시 설계 </span>
					<div class="cal_box">
							${bsifVo.prtnfx_dsgn_bgnde} ~  ${bsifVo.prtnfx_dsgn_endde}
					</div>
				</li>
				<li>
					<c:if test="${biz_ty_code eq 'BTC08'}">
						<span>무장애 나눔길 공사</span>
					</c:if>
					<c:if test="${biz_ty_code ne 'BTC08'}">
						<span>수목식재 및 공사</span>
					</c:if>
					<div class="cal_box">
							${bsifVo.prtnfx_cntrwk_bgnde} ~ ${bsifVo.prtnfx_cntrwk_endde}
					</div>
				</li>
			</ul>
		</td>
	</tr>
</c:if>

<%-- 복지시설환경개선  --%>
<c:if test="${biz_ty_code eq 'BTC09'}">
	<%--<tr>
		<th>주요 추진일정</th>
		<td colspan="4">
			<ul class="icon3_list cal_box_list">
				<li>
					<span>기본조사 및 설계 </span>
					<div class="cal_box">
							${bsifVo.prtnfx_dsgn_bgnde} ~ ${bsifVo.prtnfx_dsgn_endde}
					</div>
				</li>
				<li>
					<span>기존시설철거 </span>
					<div class="cal_box">
							${bsifVo.fclty_pudn_bgnde} ~  ${bsifVo.fclty_pudn_endde}
					</div>
				</li>
				<li>
					<span>시공 </span>
					<div class="cal_box">
							${bsifVo.construction_bgnde} ~  ${bsifVo.construction_endde}
					</div>
				</li>
				<li>
					<span>기타 </span>${bsifVo.etc_purps_contents}
				</li>
			</ul>
		</td>
	</tr>--%>
	<tr>
		<th>대체공간확보계획 </th>
		<td colspan="4">
			<c:out value="${fn:replace(bsifVo.alt_space_plan, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>

<%-- 숲체험ㆍ교육 <체험교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC05'}">
	<tr>
		<th>보유 시설‧강사 현황</th>
		<td>
			<c:out value="${fn:replace(bsifVo.fclty_instrctr_sttus, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div class="top_tit">
				<strong>※  운영프로그램 : <span class="program_cnt"><c:if test="${empty bopList}">1</c:if><c:if test="${not empty bopList}">${fn:length(bopList)}</c:if></span> 개(컨소시엄의 경우 프로그램명을 협약기관별을 구분하여 기재)</strong>
			</div>

			<div class="program_input">
				<table summary="운영프로그램 입력폼입니다." class="chart1">
					<caption>운영프로그램 입력</caption>
					<colgroup>
						<col class="prg_view_1" />
						<col class="prg_view_2" />
						<col class="prg_view_3" />
						<col class="prg_view_4" />
						<col class="prg_view_5" />
						<col class="prg_view_6" />
						<col class="prg_view_7" />
							<%--<col class="prg_view_8" />--%>
						<col class="prg_view_9" />
						<col class="prg_view_10" />
						<col class="prg_view_11" />
					</colgroup>
					<thead>
					<tr>
						<th rowspan="2">프로그램명</th>
						<th rowspan="2">날짜</th>
						<th rowspan="2">총 횟수</th>
						<th rowspan="2">연속여부</th>
						<th colspan="2">회당</th>
						<th rowspan="2">교육대상</th>
						<th rowspan="2">회당인원</th>
							<%--<th rowspan="2">총인원</th>--%>
						<th rowspan="2">연인원</th>
						<th rowspan="2">교육장소</th>
					</tr>
					<tr>
						<th>교육일</th>
						<th>강사수 </th>
					</tr>
					</thead>
					<tbody class="bizOpePgm">
					<c:if test="${not empty bopList}">
						<c:set var="edc_co" value="0" />
						<c:set var="tot_co" value="0" />
						<c:set var="round_edcde_co" value="0" />
						<c:set var="edc_trget_01_co" value="0" />
						<c:set var="edc_trget_03_co" value="0" />
						<c:set var="edc_trget_04_co" value="0" />
						<c:set var="edc_trget_05_co" value="0" />
						<c:set var="edc_trget_06_co" value="0" />
						<c:set var="edc_trget_01_co_year" value="0" />
						<c:set var="edc_trget_03_co_year" value="0" />
						<c:set var="edc_trget_05_co_year" value="0" />
						<c:forEach var="rs" items="${bopList}">
							<c:set var="edc_co" value="${edc_co + rs.edc_co}" />
							<c:set var="tot_co" value="${tot_co + rs.tot_co}" />
							<c:set var="round_edcde_co" value="${round_edcde_co + rs.round_edcde_co}" />
							<c:set var="edc_trget_01_co" value="${edc_trget_01_co + rs.edc_trget_01_co}" />
							<c:set var="edc_trget_02_co" value="${edc_trget_02_co + rs.edc_trget_02_co}" />
							<c:set var="edc_trget_03_co" value="${edc_trget_03_co + rs.edc_trget_03_co}" />
							<c:set var="edc_trget_04_co" value="${edc_trget_04_co + rs.edc_trget_04_co}" />
							<c:set var="edc_trget_05_co" value="${edc_trget_05_co + rs.edc_trget_05_co}" />
							<c:set var="edc_trget_06_co" value="${edc_trget_06_co + rs.edc_trget_06_co}" />
							<c:set var="edc_trget_01_co_year" value="${edc_trget_01_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_01_co)}" />
							<c:set var="edc_trget_03_co_year" value="${edc_trget_03_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_03_co)}" />
							<c:set var="edc_trget_05_co_year" value="${edc_trget_05_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_05_co)}" />

							<tr>
								<td rowspan="4" class="prg">${rs.progrm_nm}</td>
								<td rowspan="4" class="prg"> ${rs.bgnde}
									</p> ~ ${rs.endde}</td>
								<td rowspan="4" class="prg">${rs.tot_co}회</td>
								<td rowspan="4" class="prg">
									<c:if test="${rs.ctnu_at eq 'N'}">일회</c:if>
									<c:if test="${rs.ctnu_at eq 'Y'}">연속</c:if>
								</td>
								<td rowspan="4" class="prg">${rs.edc_co}일</td>
								<td rowspan="4" class="prg">${rs.round_edcde_co}명</td>
								<th class="prg">소외계층</th>
								<td class="prg">${rs.edc_trget_01_co}</td>
									<%--<td class="prg">${rs.edc_trget_02_co}</td>--%>
								<td class="prg edc_trget_co_tot_txt_1">${rs.tot_co * rs.edc_co * rs.edc_trget_01_co}</td>
								<td  rowspan="4" class="prg last">${rs.edc_place}</td>
							</tr>
							<tr>
								<th>일반 유아·아동·청소년</th>
								<td class="bline">${rs.edc_trget_03_co}</td>
									<%--<td class="bline">${rs.edc_trget_04_co}</td>--%>
								<td class="bline edc_trget_co_tot_txt_2">${rs.tot_co * rs.edc_co * rs.edc_trget_03_co}</td>
							</tr>
							<tr>
								<th>일반성인</th>
								<td class="bline">${rs.edc_trget_05_co}</td>
									<%--<td class="bline">${rs.edc_trget_06_co}</td>--%>
								<td class="bline edc_trget_co_tot_txt_3">${rs.tot_co * rs.edc_co * rs.edc_trget_05_co}</td>
							</tr>
							<tr>
								<th class="last">소계</th>
								<td class="one_pers_tot_txt">${rs.edc_trget_01_co + rs.edc_trget_03_co + rs.edc_trget_05_co}</td>
									<%--<td class="tot_pers_tot_txt">${rs.edc_trget_02_co + rs.edc_trget_05_co + rs.edc_trget_06_co}</td>--%>
								<td class="year_pers_tot_txt">${rs.tot_co * rs.edc_co * (rs.edc_trget_01_co + rs.edc_trget_03_co + rs.edc_trget_05_co)}</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${empty bopList}">
						<tr>
							<td colspan="11">등록된 운영프로그램이 없습니다.</td>
						</tr>
					</c:if>
					</tbody>
					<c:if test="${not empty bopList}">
						<tfoot>
						<tr>
							<th colspan="10" class="alC">합계</th>
						</tr>
						<tr>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="edc_co_tot_txt">${edc_co}</td>
							<td rowspan="4" class="round_edcde_co_tot_txt">${round_edcde_co}</td>
							<th>소외계층 </th>
							<td class="edc_trget_01_co_tot_txt">${edc_trget_01_co}</td>
								<%--<td class="edc_trget_02_co_tot_txt">${edc_trget_02_co}</td>--%>
							<td class="edc_trget_co_tot_txt_1_txt">${edc_trget_01_co_year}</td>
							<td rowspan="4">X</td>
						</tr>
						<tr>
							<th>일반 유아·아동·청소년</th>
							<td class="edc_trget_03_co_tot_txt">${edc_trget_03_co}</td>
								<%--<td class="edc_trget_04_co_tot_txt">${edc_trget_04_co}</td>--%>
							<td class="edc_trget_co_tot_txt_2_txt">${edc_trget_03_co_year}</td>
						</tr>
						<tr>
							<th>일반성인</th>
							<td class="edc_trget_05_co_tot_txt">${edc_trget_05_co}</td>
								<%--<td class="edc_trget_06_co_tot_txt">${edc_trget_06_co}</td>--%>
							<td class="edc_trget_co_tot_txt_3_txt">${edc_trget_05_co_year}</td>
						</tr>
						<tr>
							<th>소계</th>
							<td class="edc_trget_135_tot_txt"><c:if test="${(edc_trget_01_co + edc_trget_03_co + edc_trget_05_co) > 0}">${edc_trget_01_co + edc_trget_03_co + edc_trget_05_co}</c:if></td>
								<%--<td class="edc_trget_246_tot_txt"><c:if test="${(edc_trget_02_co + edc_trget_04_co + edc_trget_06_co) > 0}">${edc_trget_02_co + edc_trget_04_co + edc_trget_06_co}</c:if></td>--%>
							<td class="edc_trget_135_year_tot_txt"><c:if test="${(edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year) > 0}">${edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year}</c:if></td>
						</tr>
						</tfoot>
					</c:if>
				</table>
			</div>
			<ul class="icon3_list">
				<li>① 가급적 교육대상을 한정하여 수혜자특성에 맞는 프로그램 운영할 것</li>
				<li>② ‘교육장소’는 교육시설명 및 휴양림명 등을 구체적으로 기술</li>
			</ul>
		</td>
	</tr>
	<tr>
		<th>1인당 교육비(천원)<br />(총사업비 / 참가 연인원)</th>
		<td><span class="one_edu_money"><fmt:formatNumber value="${bsifVo.tot_wct / (edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year) * 1000}" pattern="#,###,###.#"/></span></td>
	</tr>
</c:if>

<%-- 숲체험ㆍ교육 <휴양문화사업> --%>
<c:if test="${biz_ty_code eq 'BTC06'}">
	<tr>
		<th rowspan="2">사업내용</th>
		<td>
			<ul class="icon3_list cnt_list">
				<li>
					<span>사업기간</span>
					<div class="tx biz_bgnde_txt"><c:if test="${not empty bsifVo.biz_bgnde and not empty bsifVo.biz_endde }">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</c:if>&nbsp;</div>
				</li>
				<li>
					<span>대상자</span>
					<div class="tx">
						위에 작성한 사업제안서 대상자와 동일
					</div>
				</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td>
			<ul class="icon3_list cnt_list">
				<li>
					<span>사업장소</span>
					<div class="tx">&nbsp;</div>
				</li>
			</ul>
			<div class="addr_box">
					${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
					${bsifVo.biz_adres}<br/>
					${bsifVo.biz_adres_detail}
			</div>
		</td>
	</tr>
	<tr>
		<th>사업규모</th>
		<td>
			<table class="chart1" summary="사업규모 입력폼입니다.">
				<caption>사업규모 입력폼</caption>
				<colgroup>
					<col width="auto"/>
					<col width="auto"/>
					<col width="auto"/>
					<col width="auto"/>
					<col width="auto"/>
					<col width="auto"/>
					<col width="auto"/>
					<col width="9%"/>
				</colgroup>
				<thead>
				<tr>
					<th>수혜 대상</th>
					<th>수혜 인원</th>
					<th>세부행사 규모</th>
					<th>횟수</th>
					<th>성과물 내용<br />제작수</th>
					<th>성과물 내용<br />배포수</th>
					<th>기타</th>
				</tr>
				</thead>
				<tbody class="bizSize">
				<c:if test="${not empty bbsList}">
					<c:forEach var="rs" items="${bbsList}" varStatus="sts">
						<tr>
							<td>${rs.rcvfvr_trget}</td>
							<td>${rs.rcvfvr_nmpr_co}</td>
							<td>${rs.detail_event_scale}</td>
							<td>${rs.co}</td>
							<td>${rs.rslt_cn_mnfct_co}</td>
							<td>${rs.rslt_cn_wdtb_co}</td>
							<td>${rs.etc}</td>
						</tr>
					</c:forEach>
				</c:if>
				<c:if test="${empty bbsList}">
					<tr>
						<td colspan="7">등록된 사업규모가 없습니다.</td>
					</tr>
				</c:if>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<th>참가자 모집.관리</th>
		<td>
			※ 취약계층 참가자 모집 및 실적관리 방법을 구체적으로 기재
			<div>
				<c:out value="${fn:replace(bsifVo.adhrnc_rcrit_manage, newLineChar, '<br/>')}" escapeXml="false" />
			</div>
		</td>
	</tr>
</c:if>

<%-- 숲체험ㆍ교육 <휴양문화사업, 체험교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
	<tr>
		<th>추진일정</th>
		<td colspan="4">
			<strong>※ 기간과 내용을 구체적으로 기재</strong>
			<ul class="icon3_list cal_box_list">
				<li>
					<span>사업준비 </span>
					<div class="cal_box">
							${bsifVo.prtnfx_bizprpare_bgnde} ~ ${bsifVo.prtnfx_bizprpare_endde}
					</div>
				</li>
				<li>
					<span>모집공고 </span>
					<div class="cal_box">
							${bsifVo.prtnfx_rcritpblanc_bgnde} ~ ${bsifVo.prtnfx_rcritpblanc_endde}
					</div>
				</li>
				<li>
					<span>사업진행</span>
					<div class="cal_box">
							${bsifVo.prtnfx_bizprogrs_bgnde} ~ ${bsifVo.prtnfx_bizprogrs_endde}
					</div>
				</li>
				<li>
					<span>평가보고</span>
					<div class="cal_box">
							${bsifVo.prtnfx_evlreport_bgnde} ~ ${bsifVo.prtnfx_evlreport_endde}
					</div>
				</li>
			</ul>
		</td>
	</tr>
</c:if>

<%-- 숲체험ㆍ교육 <체험교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC06'}">
	<tr>
		<th>기대효과 </th>
		<td>
			<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
		</td>
	</tr>
</c:if>
</tbody>
</table>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길  --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">제안요약서(사후관리)</h3>
	<table class="chart2" summary="${biz_sort_nm} 제안요약서(사후관리) 정보 입력">
		<caption>${biz_sort_nm} 제안요약서(사후관리)정보 입력</caption>
		<colgroup>
			<col class="expost_1"/>
			<col class="expost_2"/>
		</colgroup>
		<tbody>
		<tr>
			<th>사후관리</th>
			<td>
				<c:out value="${fn:replace(bsifVo.aftfat_manage, newLineChar, '<br/>')}" escapeXml="false" />
			</td>
		</tr>
		</tbody>
	</table>
</c:if>