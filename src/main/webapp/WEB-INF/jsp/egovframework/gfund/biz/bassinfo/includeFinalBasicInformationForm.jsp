<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate var="years" value="${date}" pattern="yyyy" />
<fmt:formatDate var="month" value="${date}" pattern="MM" />

<input type="hidden" name="user_nm" value="${bsifVo.user_nm}" />
<input type="hidden" name="email_adres" value="${bsifVo.email_adres}" />
<input type="hidden" name="tlphon_no" value="${bsifVo.tlphon_no}" />
<input type="hidden" name="moblphon_no" value="${bsifVo.moblphon_no}" />
<input type="hidden" name="faxphon_no" value="${bsifVo.faxphon_no}" />
<input type="hidden" name="zip" value="${bsifVo.zip}" />
<input type="hidden" name="adres" value="${bsifVo.adres}" />
<input type="hidden" name="adres_detail" value="${bsifVo.adres_detail}" />
<input type="hidden" name="r_adres" value="${bsifVo.r_adres}" />
<input type="hidden" name="r_adres_detail" value="${bsifVo.r_adres_detail}" />
<input type="hidden" name="department" value="${bsifVo.department}" />
<input type="hidden" name="position" value="${bsifVo.position}" />

<table class="chart2" summary="${biz_sort_nm} 사업제안서 정보 입력">
	<caption>${biz_sort_nm}사업제안서 정보 입력</caption>
	<colgroup>

		<%-- 복지 시설 나눔숲 (사회복지시설) --%>
		<c:if test="${biz_ty_code eq 'BTC01'}">
			<col width="10%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="20%"/>
			<col width="10%"/>
			<col width="5%"/>
			<col width="15%"/>
			<col width="15%"/>
		</c:if>

		<%-- 복지시설환경개선 --%>
		<c:if test="${biz_ty_code eq 'BTC09'}">
			<col width="10%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="20%"/>
			<col width="10%"/>
			<col width="5%"/>
			<col width="18%"/>
			<col width="12%"/>
		</c:if>

		<%-- 복지 시설 나눔숲 (특수복지시설), 숲체험ㆍ교육 <체험ㆍ휴양교육사업>, 다함께 나눔길 --%>
		<c:if test="${biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
			<col width="5%"/>
			<col width="5%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="10%"/>
			<col width="15%"/>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<col width="5%"/>
			<col width="10%"/>
			<col width="15%"/>
			<col width="18%"/>
			<col width="10%"/>
			<col width="5%"/>
			<col width="10%"/>
			<col width="10%"/>
		</c:if>
	</colgroup>
	<tbody>

	<%-- 복지 시설 나눔숲 (사회복지시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th colspan="2">시설명</th>
			<td colspan="2">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="시설명"/>
			</td>
			<th colspan="2">설립유형</th>
			<td colspan="2">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM217"/>
						<c:param name="chVal" value="${bsifVo.fond_ty_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="fond_ty_cc"/>
						<c:param name="elStart" value="1"/>
						<c:param name="elEnd" value="3"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">대표자</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="대표자"/>
			</td>
			<th colspan="2">수용정원(명)</th>
			<td colspan="2">
					${bsifVo.aceptnc_psncpa}
				<input type="hidden" name="aceptnc_psncpa" id="aceptnc_psncpa" value="${bsifVo.aceptnc_psncpa}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="수용정원(명)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="2">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
			<th colspan="2">수용현원(명)</th>
			<td colspan="2">
					${bsifVo.aceptnc_nownmpr}
				<input type="hidden" name="aceptnc_nownmpr" id="aceptnc_nownmpr" value="${bsifVo.aceptnc_nownmpr}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="수용현원(명)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업유형 및 면적</th>
			<th>지상녹화(㎡)</th>
			<td>
					${bsifVo.ground_trplant_ar}
				<input type="hidden" name="ground_trplant_ar" id="ground_trplant_ar" value="${bsifVo.ground_trplant_ar}" class="ninp validation number" onkeyup="fnInputNumCom($(this));" title="지상녹화(㎡)"/>
			</td>
			<th colspan="2">옥상녹화(㎡)</th>
			<td colspan="2">
					${bsifVo.rf_trplant_ar}
				<input type="hidden" name="rf_trplant_ar" id="rf_trplant_ar" value="${bsifVo.rf_trplant_ar}" class="ninp validation number" onkeyup="fnInputNumCom($(this));" title="옥상녹화(㎡)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">
				<div class="cal_box">
						${bsifVo.biz_bgnde}
					<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
					~
						${bsifVo.biz_endde}
					<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
				</div>
			</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비(백만원)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업지</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
				<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
				<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
				<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
				<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
			</td>
		</tr>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th colspan="2">복지시설명</th>
			<td colspan="2">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="복지시설명"/>
			</td>
			<th colspan="2">설립유형</th>
			<td colspan="2">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM217"/>
						<c:param name="chVal" value="${bsifVo.fond_ty_cc}"/>
						<c:param name="elType" value="select"/>
						<c:param name="elName" value="fond_ty_cc"/>
						<c:param name="elClass" value="select"/>
						<c:param name="elStart" value="1"/>
						<c:param name="elEnd" value="6"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="2">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
			<th colspan="2">대표자</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="대표자"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">
				<div class="cal_box">
						${bsifVo.biz_bgnde}
					<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
					~
						${bsifVo.biz_endde}
					<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
				</div>
			</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비(백만원)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업지</th>
			<td colspan="6">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
				<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
				<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
				<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
				<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th colspan="2">학교명</th>
			<td colspan="3">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="시설명"/>
			</td>
			<th>설립유형</th>
			<td colspan="2">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM217"/>
						<c:param name="chVal" value="${bsifVo.fond_ty_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="fond_ty_cc"/>
						<c:param name="elStart" value="4"/>
						<c:param name="elEnd" value="6"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">학교장</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="학교장"/>
			</td>
			<th>이용자수(명)</th>
			<td>
					${bsifVo.user_qy}
				<input type="hidden" name="user_qy" id="user_qy" value="${bsifVo.user_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="이용자수(명)"/>
			</td>
			<th>관리자수(명)</th>
			<td>
					${bsifVo.mngr_qy}
				<input type="hidden" name="mngr_qy" id="mngr_qy" value="${bsifVo.mngr_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="관리자수(명)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="6">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">숲유형</th>
			<td colspan="3">
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM212"/>
					<c:param name="chVal" value="${bsifVo.frt_ty_spcl}"/>
					<c:param name="elType" value="select"/>
					<c:param name="elClass" value="select"/>
					<c:param name="elName" value="frt_ty_spcl"/>
				</c:import>
			</td>
			<th>조성면적(㎡)</th>
			<td colspan="2">
				<input type="txet" name="make_ar" id="make_ar" value="${bsifVo.make_ar}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성면적(㎡)"/>
			</td>
		</tr>
		<tr>
			<th rowspan="2" colspan="2">주요 수목명&nbsp;</th>
			<th>상록교목초(주)&nbsp;</th>
			<td colspan="2">
				<input type="txet" name="main_wdpt_01" id="main_wdpt_01" value="${bsifVo.main_wdpt_01}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="상록교목초(주)"/>
			</td>
			<th>관목(주)&nbsp;</th>
			<td colspan="2">
				<input type="txet" name="main_wdpt_03" id="main_wdpt_03" value="${bsifVo.main_wdpt_03}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="관목(주)"/>
			</td>
		</tr>
		<tr>
			<th>낙엽교목(주)&nbsp;</th>
			<td colspan="2">
				<input type="txet" name="main_wdpt_02" id="main_wdpt_02" value="${bsifVo.main_wdpt_02}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="낙엽교목(주)"/>
			</td>
			<th>본류(본)&nbsp;</th>
			<td colspan="2">
				<input type="txet" name="main_wdpt_04" id="main_wdpt_04" value="${bsifVo.main_wdpt_04}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="본류(본)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">
				<div class="cal_box">
						${bsifVo.biz_bgnde}
					<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
					~
						${bsifVo.biz_endde}
					<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
				</div>
			</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비(백만원)"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">조성장소</th>
			<td colspan="6">
				<div class="addr_box">
						${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
						${bsifVo.biz_adres} <br/>
						${bsifVo.biz_adres_detail}
					<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
					<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
					<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
					<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 지역사회 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC03'}">
		<tr>
			<th colspan="2">기관명</th>
			<td colspan="3">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="시설명"/>
			</td>
			<th>대표자</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="대표자"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="3">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
			<th>사업유형</th>
			<td colspan="2">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM213"/>
						<c:param name="chVal" value="${bsifVo.frt_ty_area}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="frt_ty_area"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">
					${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}
				<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
				<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
			</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비(백만원)"/>
			</td>
		</tr>
		<tr>
			<th rowspan="2" colspan="2">사업지</th>
			<td rowspan="2" colspan="3">
					${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
				<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
				<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
				<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
				<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
			</td>
			<th rowspan="2">재정자립도</th>
			<th>순위</th>
			<td>
					${bsifVo.fnanc_idpdc_rank}
				<input type="hidden" name="fnanc_idpdc_rank" id="fnanc_idpdc_rank" value="${bsifVo.fnanc_idpdc_rank}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="재정자립도 순위"/>
			</td>
		</tr>
		<tr>
			<th>퍼센트</th>
			<td>
					${bsifVo.fnanc_idpdc}
				<input type="hidden" name="fnanc_idpdc" id="fnanc_idpdc" value="${bsifVo.fnanc_idpdc}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="재정자립도 퍼센트"/>
				(%)
			</td>
		</tr>
	</c:if>

	<%-- 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC07'}">
		<tr>
			<th colspan="2">기관명</th>
			<td colspan="3">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="시설명"/>
			</td>
			<th>대표자</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="대표자"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">제안사업명</th>
			<td colspan="3">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="3">
					${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}
				<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
				<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
			</td>
			<th>총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비(백만원)"/>
			</td>
		</tr>
		<tr>
			<th rowspan="2" colspan="2">사업지</th>
			<td rowspan="2" colspan="3">
					${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
					${bsifVo.biz_adres} <br/>
					${bsifVo.biz_adres_detail}
				<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
				<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
				<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
				<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
			</td>
			<th rowspan="2">재정자립도</th>
			<th>순위</th>
			<td>
					${bsifVo.fnanc_idpdc_rank}
				<input type="hidden" name="fnanc_idpdc_rank" id="fnanc_idpdc_rank" value="${bsifVo.fnanc_idpdc_rank}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="재정자립도 순위"/>
			</td>
		</tr>
		<tr>
			<th>퍼센트</th>
			<td>
					${bsifVo.fnanc_idpdc}
				<input type="hidden" name="fnanc_idpdc" id="fnanc_idpdc" value="${bsifVo.fnanc_idpdc}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="재정자립도 퍼센트"/>
				(%)
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<tr>
			<th colspan="2">기관 및 단체명</th>
			<td colspan="2">
					${bsifVo.instt_nm}
				<input type="hidden" name="instt_nm" id="instt_nm" class="inp null_false" value="${bsifVo.instt_nm}" title="기관 및 단체명"/>
			</td>
			<th colspan="2">컨소시엄 여부</th>
			<td colspan="2">
				<div class="radio_box">
					<input type="radio" name="cnsrtm_at" id="cnsrtm_at_y" value="Y" onClick="this.form.cnsrtm_entrps_nm.disabled=false" <c:if test="${bsifVo.cnsrtm_at eq 'Y'}">checked="checked"</c:if> title="컨소시엄 여부"/>
					<label for="cnsrtm_at_y">있음</label>
					<input type="radio" name="cnsrtm_at" id="cnsrtm_at_n" value="N" onClick="this.form.cnsrtm_entrps_nm.disabled=true" <c:if test="${bsifVo.cnsrtm_at eq 'N' or empty bsifVo.cnsrtm_at}">checked="checked"</c:if> title="컨소시엄 여부"/>
					<label for="cnsrtm_at_n">없음</label>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">대표자</th>
			<td colspan="2">
					${bsifVo.rprsntv}
				<input type="hidden" name="rprsntv" id="rprsntv" value="${bsifVo.rprsntv}" class="inp null_false" title="대표자"/>
			</td>
			<th colspan="2">컨소시엄 단체·기관</th>
			<td colspan="2">
					${bsifVo.cnsrtm_entrps_nm}
				<input type="hidden" name="cnsrtm_entrps_nm" id="cnsrtm_entrps_nm" value="${bsifVo.cnsrtm_entrps_nm}" class="inp" title="컨소시엄 단체·기관"/>
				<br/>업체명은 ','로 구분하여 기재할 것
			</td>
		</tr>
		<tr>
			<th colspan="2">사업명</th>
			<td colspan="6">
					${bsifVo.biz_nm}
				<input type="hidden" name="biz_nm" id="biz_nm" class="inp null_false" value="${bsifVo.biz_nm}"/>
			</td>
		</tr>

		<%-- 숲체험ㆍ교육 <체험교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05'}">

			<c:set var="ud_rss" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
			<c:set var="ut_rss" value="${bsifVo.ut_01_co + bsifVo.ut_02_co + bsifVo.ut_03_co + bsifVo.ut_04_co}" />
			<c:set var="at_rss" value="${bsifVo.at_01_co + bsifVo.at_02_co + bsifVo.at_03_co}" />

			<tr>
				<th colspan="2">프로그램 참가대상</th>
				<td colspan="2">
					<input type="checkbox" name="pj_01" id="pj_01" value="pj_01" <c:if test="${bsifVo.pj_01 eq 'pj_01'}">checked="checked"</c:if> /><label for="pj_01">유아</label>
					<input type="checkbox" name="pj_02" id="pj_02" value="pj_02" <c:if test="${bsifVo.pj_02 eq 'pj_02'}">checked="checked"</c:if> /><label for="pj_02">초등학생</label>
					<input type="checkbox" name="pj_03" id="pj_03" value="pj_03" <c:if test="${bsifVo.pj_03 eq 'pj_03'}">checked="checked"</c:if> /><label for="pj_03">중학생</label>
					<input type="checkbox" name="pj_04" id="pj_04" value="pj_04" <c:if test="${bsifVo.pj_04 eq 'pj_04'}">checked="checked"</c:if> /><label for="pj_04">고등학생</label>
					<br/>
					<input type="checkbox" name="pj_05" id="pj_05" value="pj_05" <c:if test="${bsifVo.pj_05 eq 'pj_05'}">checked="checked"</c:if> /><label for="pj_05">가족</label>
					<input type="checkbox" name="pj_06" id="pj_06" value="pj_06" <c:if test="${bsifVo.pj_06 eq 'pj_06'}">checked="checked"</c:if> /><label for="pj_06">성인</label>
					<input type="checkbox" name="pj_07" id="pj_07" value="pj_07" <c:if test="${bsifVo.pj_07 eq 'pj_07'}">checked="checked"</c:if> /><label for="pj_07">기타</label>
				</td>
				<th colspan="2">대상특성</th>
				<td colspan="2">
					<input type="checkbox" name="oc_01" id="oc_01" value="oc_01" <c:if test="${bsifVo.oc_01 eq 'oc_01'}">checked="checked"</c:if> /><label for="oc_01">학교폭력</label>
					<input type="checkbox" name="oc_02" id="oc_02" value="oc_02" <c:if test="${bsifVo.oc_02 eq 'oc_02'}">checked="checked"</c:if> /><label for="oc_02">다문화</label>
					<input type="checkbox" name="oc_03" id="oc_03" value="oc_03" <c:if test="${bsifVo.oc_03 eq 'oc_03'}">checked="checked"</c:if> /><label for="oc_03">자유학기제연계</label>
					<input type="checkbox" name="oc_04" id="oc_04" value="oc_04" <c:if test="${bsifVo.oc_04 eq 'oc_04'}">checked="checked"</c:if> /><label for="oc_04">특수아동</label>
					<br/>
					<input type="checkbox" name="oc_05" id="oc_05" value="oc_05" <c:if test="${bsifVo.oc_05 eq 'oc_05'}">checked="checked"</c:if> /><label for="oc_05">장애인</label>
					<input type="checkbox" name="oc_06" id="oc_06" value="oc_06" <c:if test="${bsifVo.oc_06 eq 'oc_06'}">checked="checked"</c:if> /><label for="oc_06">특정질환자</label>
					<input type="checkbox" name="oc_07" id="oc_07" value="oc_07" <c:if test="${bsifVo.oc_07 eq 'oc_07'}">checked="checked"</c:if> /><label for="oc_07">저소득</label>
					<input type="checkbox" name="oc_08" id="oc_08" value="oc_08" <c:if test="${bsifVo.oc_08 eq 'oc_08'}">checked="checked"</c:if> /><label for="oc_08">기타</label>
				</td>
			</tr>
			<tr>
				<th rowspan="11" colspan="2">교육 대상<br />(해당되는 대상 모두 V표)</th>
				<th rowspan="4"><label for="ud_co_ck">소외계층 대상</label></th>
				<td rowspan="4" class="alC"><input type="checkbox" name="ud_co_ck" id="ud_co_ck" <c:if test="${ud_rss ne 0}">checked="checked"</c:if> /></td>
				<th colspan="2"><label for="ud_01_co">다문화</label></th>
				<td colspan="2"><input type="text" name="ud_01_co" id="ud_01_co" value="${bsifVo.ud_01_co}" <c:if test="${ud_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ud_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="다문화" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ud_02_co">저소득</label></th>
				<td colspan="2"><input type="text" name="ud_02_co" id="ud_02_co" value="${bsifVo.ud_02_co}" <c:if test="${ud_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ud_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="저소득" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ud_03_co">장애인</label></th>
				<td colspan="2"><input type="text" name="ud_03_co" id="ud_03_co" value="${bsifVo.ud_03_co}" <c:if test="${ud_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ud_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="장애인" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ud_04_co">기타</label></th>
				<td colspan="2"><input type="text" name="ud_04_co" id="ud_04_co" value="${bsifVo.ud_04_co}" <c:if test="${ud_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ud_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타"/> (명)</td>
			</tr>
			<tr>
				<th rowspan="4"><label for="ut_co_ck">일반 유아·아동·청소년</label></th>
				<td rowspan="4" class="alC"><input type="checkbox" name="ut_co_ck" id="ut_co_ck" <c:if test="${ut_rss ne 0}">checked="checked"</c:if> /></td>
				<th colspan="2"><label for="ut_03_co">유아</label></th>
				<td colspan="2"><input type="text" name="ut_03_co" id="ut_03_co" value="${bsifVo.ut_03_co}" <c:if test="${ut_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ut_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="유아" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ut_01_co">아동</label></th>
				<td colspan="2"><input type="text" name="ut_01_co" id="ut_01_co" value="${bsifVo.ut_01_co}" <c:if test="${ut_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ut_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="아동" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ut_04_co">중학생</label></th>
				<td colspan="2"><input type="text" name="ut_04_co" id="ut_04_co" value="${bsifVo.ut_04_co}" <c:if test="${ut_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ut_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="중학생" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="ut_02_co">고등학생</label></th>
				<td colspan="2"><input type="text" name="ut_02_co" id="ut_02_co" value="${bsifVo.ut_02_co}" <c:if test="${ut_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 ut_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="고등학생" /> (명)</td>
			</tr>
			<tr>
				<th rowspan="3"><label for="at_co_ck">일반 성인</label></th>
				<td rowspan="3" class="alC"><input type="checkbox" name="at_co_ck" id="at_co_ck" <c:if test="${at_rss ne 0}">checked="checked"</c:if> /></td>
				<th colspan="2"><label for="at_01_co">대학생</label></th>
				<td colspan="2"><input type="text" name="at_01_co" id="at_01_co" value="${bsifVo.at_01_co}" <c:if test="${at_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 at_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="대학생" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="at_02_co">일반인</label></th>
				<td colspan="2"><input type="text" name="at_02_co" id="at_02_co" value="${bsifVo.at_02_co}" <c:if test="${at_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 at_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="일반인" /> (명)</td>
			</tr>
			<tr>
				<th colspan="2"><label for="at_03_co">산주</label></th>
				<td colspan="2"><input type="text" name="at_03_co" id="at_03_co" value="${bsifVo.at_03_co}" <c:if test="${at_rss eq 0}">disabled="disabled"</c:if> class="ninp w50 at_co_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산주"/> (명)</td>
			</tr>
			<tr>
				<c:set var="ud_rss" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
				<c:set var="ut_rss" value="${bsifVo.ut_01_co + bsifVo.ut_02_co + bsifVo.ut_03_co + bsifVo.ut_04_co}" />
				<c:set var="at_rss" value="${bsifVo.at_01_co + bsifVo.at_02_co + bsifVo.at_03_co}" />
				<th colspan="2">연인원</th>
				<td colspan="2"><span class="ud_ut_at_tot_txt">${ud_rss + ut_rss + at_rss}</span>명 (교육대상 합계)</td>
				<th colspan="2">소외계층 비율</th>
				<c:set var="ud_ut_at_tot_cnt" value="${ud_rss + ut_rss + at_rss}" />
				<c:set var="ud_co_tot_cnt" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
				<td colspan="2"><span class="ud_co_tot_cnt_txt"><fmt:formatNumber value="${ud_co_tot_cnt / ud_ut_at_tot_cnt * 100}" pattern=".0"/></span>%</td>
			</tr>
		</c:if>

		<%-- 숲체험ㆍ교육 <휴양문화사업> --%>
		<c:if test="${biz_ty_code eq 'BTC06'}">
			<tr>
				<th colspan="2">사업 내용 </th>
				<td colspan="6">
					<table class="chart1">
						<colgroup>
							<col width="10%" />
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
							<col width="13%" />
							<col width="10%"/>
						</colgroup>
						<thead>


						<c:set var="cs_rss" value="${bsifVo.cs_01_co + bsifVo.cs_02_co + bsifVo.cs_03_co + bsifVo.cs_04_co + bsifVo.cs_05_ct + bsifVo.cs_06_ct}" />
						<c:set var="ef_rss" value="${bsifVo.ef_01_co + bsifVo.ef_02_co + bsifVo.ef_03_co + bsifVo.ef_04_co + bsifVo.ef_05_ct + bsifVo.ef_06_ct}" />
						<c:set var="fl_rss" value="${bsifVo.fl_01_co + bsifVo.fl_02_co + bsifVo.fl_03_co + bsifVo.fl_04_co + bsifVo.fl_05_ct + bsifVo.fl_06_ct}" />
						<c:set var="etc_rss" value="${bsifVo.etc_02_co + bsifVo.etc_03_co + bsifVo.etc_04_ct + bsifVo.etc_05_ct}" />
						<tr>
							<th>세부유형 선택</th>
							<th colspan="2" class="vtop"><input type="checkbox" id="cs_ck" <c:if test="${cs_rss ne 0}">checked="checked"</c:if> class="vMid" /> <label for="cs_ck" class="vMid">문화·공연</label> </th>
							<th colspan="2" class="vtop"><input type="checkbox" id="ef_ck" <c:if test="${ef_rss ne 0}">checked="checked"</c:if> class="vMid" /> <label for="ef_ck" class="vMid">(체험)박람회</label></th>
							<th colspan="2" class="vtop"><input type="checkbox" id="fl_ck" <c:if test="${fl_rss ne 0}">checked="checked"</c:if> class="vMid" /> <label for="fl_ck" class="vMid">산림레저활동<br />(걷기,등산,산악스키등) </label> </th>
							<th colspan="2" class="vtop"><input type="checkbox" id="etc_ck" <c:if test="${etc_rss ne 0}">checked="checked"</c:if> class="vMid" /> <label for="etc_ck" class="vMid">기 타 </label></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<th rowspan="2">대상 </th>
							<th>소외계층 </th>
							<td><input type="text" name="cs_01_co" id="cs_01_co" value="${bsifVo.cs_01_co}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 소외계층" /></td>
							<th>소외계층 </th>
							<td><input type="text" name="ef_01_co" id="ef_01_co" value="${bsifVo.ef_01_co}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 소외계층" /></td>
							<th>소외계층 </th>
							<td><input type="text" name="fl_01_co" id="fl_01_co" value="${bsifVo.fl_01_co}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 소외계층" /></td>
							<td colspan="2" rowspan="2">
								<input type="text" name="etc_01" id="etc_01" value="${bsifVo.etc_01}" <c:if test="${etc_rss eq 0}">disabled="disabled"</c:if> class="inp etc_vals null_false" title="기타 대상 사업내용기재"/>
							</td>
						</tr>
						<tr>
							<th>일반인</th>
							<td><input type="text" name="cs_02_co" id="cs_02_co" value="${bsifVo.cs_02_co}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 일반인" /></td>
							<th>일반인</th>
							<td><input type="text" name="ef_02_co" id="ef_02_co" value="${bsifVo.ef_02_co}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 일반인" /></td>
							<th>일반인</th>
							<td><input type="text" name="fl_02_co" id="fl_02_co" value="${bsifVo.fl_02_co}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 일반인" /></td>
						</tr>
						<tr>
							<th rowspan="2">규모 </th>
							<th>횟수 </th>
							<td><input type="text" name="cs_04_co" id="cs_04_co" value="${bsifVo.cs_04_co}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 횟수" /></td>
							<th>횟수 </th>
							<td><input type="text" name="ef_04_co" id="ef_04_co" value="${bsifVo.ef_04_co}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 횟수" /></td>
							<th>횟수 </th>
							<td><input type="text" name="fl_04_co" id="fl_04_co" value="${bsifVo.fl_04_co}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 횟수" /></td>
							<th>횟수</th>
							<td><input type="text" name="etc_03_co" id="etc_03_co" value="${bsifVo.etc_03_co}" <c:if test="${etc_rss eq 0}">disabled="disabled"</c:if> class="rinp etc_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타 횟수" /></td>
						</tr>
						<tr>
							<th>회당(명)</th>
							<td><input type="text" name="cs_03_co" id="cs_03_co" value="${bsifVo.cs_03_co}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 회당(명)" /></td>
							<th>회당(명)</th>
							<td><input type="text" name="ef_03_co" id="ef_03_co" value="${bsifVo.ef_03_co}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 회당(명)" /></td>
							<th>회당(명)</th>
							<td><input type="text" name="fl_03_co" id="fl_03_co" value="${bsifVo.fl_03_co}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 회당(명)" /></td>
							<th>회당(명)</th>
							<td><input type="text" name="etc_02_co" id="etc_02_co" value="${bsifVo.etc_02_co}" <c:if test="${etc_rss eq 0}">disabled="disabled"</c:if> class="rinp etc_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타 회당(명)" /></td>
						</tr>
						<tr>
							<th rowspan="2">비용 </th>
							<th>인당(천원) </th>
							<td><input type="text" name="cs_06_ct" id="cs_06_ct" value="${bsifVo.cs_06_ct}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 인당(천원)" /></td>
							<th>인당(천원) </th>
							<td><input type="text" name="ef_06_ct" id="ef_06_ct" value="${bsifVo.ef_06_ct}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 인당(천원)" /></td>
							<th>인당(천원) </th>
							<td><input type="text" name="fl_06_ct" id="fl_06_ct" value="${bsifVo.fl_06_ct}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 인당(천원)" /></td>
							<th>인당(천원)</th>
							<td><input type="text" name="etc_05_ct" id="etc_05_ct" value="${bsifVo.etc_05_ct}" <c:if test="${etc_rss eq 0}">disabled="disabled"</c:if> class="rinp etc_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타 인당(천원)" /></td>
						</tr>
						<tr>
							<th>회당 (백만원)</th>
							<td><input type="text" name="cs_05_ct" id="cs_05_ct" value="${bsifVo.cs_05_ct}" <c:if test="${cs_rss eq 0}">disabled="disabled"</c:if> class="rinp cs_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="문화·공연 회당 (백만원)" /></td>
							<th>회당 (백만원)</th>
							<td><input type="text" name="ef_05_ct" id="ef_05_ct" value="${bsifVo.ef_05_ct}" <c:if test="${ef_rss eq 0}">disabled="disabled"</c:if> class="rinp ef_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="(체험)박람회 회당 (백만원)" /></td>
							<th>회당 (백만원)</th>
							<td><input type="text" name="fl_05_ct" id="fl_05_ct" value="${bsifVo.fl_05_ct}" <c:if test="${fl_rss eq 0}">disabled="disabled"</c:if> class="rinp fl_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="산림레저활동 (걷기,등산,산악스키등) 회당 (백만원)" /></td>
							<th>회당 (백만원)</th>
							<td><input type="text" name="etc_04_ct" id="etc_04_ct" value="${bsifVo.etc_04_ct}" <c:if test="${etc_rss eq 0}">disabled="disabled"</c:if> class="rinp etc_vals null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타 회당 (백만원)" /></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</c:if>
		<tr>
			<th colspan="2">사업기간</th>
			<td colspan="2">
					${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}
				<input type="hidden" name="biz_bgnde" id="biz_bgnde" value="${bsifVo.biz_bgnde}" class="cinp null_false" readonly="readonly" title="사업기간 시작날짜"/>
				<input type="hidden" name="biz_endde" id="biz_endde" value="${bsifVo.biz_endde}" class="cinp null_false" readonly="readonly" title="사업기간 종료날짜"/>
			</td>
			<th colspan="2">총 사 업 비(백만원)</th>
			<td colspan="2">
				<input type="text" name="tot_wct" id="tot_wct" value="${bsifVo.tot_wct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="총사업비"/>
			</td>
		</tr>

		<%-- 숲체험ㆍ교육 <체험교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05'}">
			<tr>
				<th colspan="2">교육장소</th>
				<td colspan="2">
						${bsifVo.edc_place}
					<input type="hidden" name="edc_place" id="edc_place" value="${bsifVo.edc_place}" class="inp null_false" title="교육장소"/>
					<br/> 교육장소는 ','로 구분하여 기재할 것
				</td>
				<th colspan="2">1인당 교육비(천원)</th>
				<td colspan="2">
					<c:set var="ud_rss" value="${bsifVo.ud_01_co + bsifVo.ud_02_co + bsifVo.ud_03_co + bsifVo.ud_04_co}" />
					<c:set var="ut_rss" value="${bsifVo.ut_01_co + bsifVo.ut_02_co + bsifVo.ut_03_co + bsifVo.ut_04_co}" />
					<c:set var="at_rss" value="${bsifVo.at_01_co + bsifVo.at_02_co + bsifVo.at_03_co}" />
					<span id="one_edc_mny"><fmt:formatNumber value="${bsifVo.tot_wct / (ud_rss + ut_rss + at_rss) * 1000}" pattern="#,###,###,###.#"/></span>
				</td>
			</tr>
		</c:if>
	</c:if>

	<%-- 기본정보 -> 복지 시설 나눔숲 (사회복지시설), 숲체험ㆍ교육 <체험교육사업>, 숲체험ㆍ교육 <휴양문화사업> --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<c:if test="${biz_ty_code eq 'BTC01'}">
			<c:set var="groupName" value="시설"/>
		</c:if>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<c:set var="groupName" value="단체"/>
		</c:if>
		<tr>
			<th rowspan="4">${groupName}<br/>연락처
			</th>
			<th rowspan="2">주 소 </th>
			<td rowspan="2" colspan="2">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/> ${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
			<th colspan="2">전화번호</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.tlphon_no}</div>
			</td>
		</tr>
		<tr>
			<th colspan="2">이동전화</th>
			<td colspan="3">
				<div class="tel_box">${bsifVo.moblphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="2">${bsifVo.email_adres}</td>
			<th colspan="2">F A X</th>
			<td colspan="3">${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th colspan="2">성 명</th>
			<td colspan="3">${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th rowspan="3">연락처</th>
			<th>이동전화</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.moblphon_no}</div>
			</td>
			<th colspan="2">전화번호</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.tlphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="2">${bsifVo.email_adres}</td>
			<th colspan="2">F A X</th>
			<td colspan="3">${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th colspan="2">성 명</th>
			<td colspan="3">${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
		<tr>
			<th rowspan="4">
				기관<br/>연락처
			</th>
			<th rowspan="2">주 소 </th>
			<td rowspan="2" colspan="4">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/> ${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
			<th>전화번호</th>
			<td>
				<div class="tel_box">${bsifVo.tlphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>이동전화</th>
			<td>
				<div class="tel_box">${bsifVo.moblphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>E-mail</th>
			<td colspan="4">${bsifVo.email_adres}</td>
			<th>F A X</th>
			<td>${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>실무자</th>
			<th>소속부서</th>
			<td>${bsifVo.department}</td>
			<th>직위</th>
			<td>${bsifVo.position}</td>
			<th>성 명</th>
			<td>${bsifVo.user_nm}</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th rowspan="5">
				학교<br/>연락처
			</th>
			<th>
				학교<br/>주소
			</th>
			<td colspan="6">
				<div class="addr_box">
						${fn:substring(bsifVo.zip, 0, 3)} - ${fn:substring(bsifVo.zip, 3, 6)} <br/> ${bsifVo.adres} <br/>${bsifVo.adres_detail}
				</div>
			</td>
		</tr>

		<tr>
			<th rowspan="4">
				담당<br/>교사
			</th>
			<th rowspan="2">성명</th>
			<td rowspan="2" colspan="2">${bsifVo.user_nm}</td>
			<th>전화번호(직통)</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.tlphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>이동전화</th>
			<td colspan="2">
				<div class="tel_box">${bsifVo.moblphon_no}</div>
			</td>
		</tr>
		<tr>
			<th>소속(부서ㆍ과목)</th>
			<td colspan="2">${bsifVo.department}</td>
			<th>F A X</th>
			<td colspan="2">${bsifVo.faxphon_no}</td>
		</tr>
		<tr>
			<th>직책</th>
			<td colspan="2">${bsifVo.position}</td>
			<th>E-mail</th>
			<td colspan="2">${bsifVo.email_adres}</td>
		</tr>
	</c:if>
	</tbody>
</table>

<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">사업계획요약서(기본정보)</h3>
</c:if>
<c:if test="${biz_ty_code ne 'BTC05' and biz_ty_code ne 'BTC09'}">
	<h3 class="icon1">제안요약서(기본정보)</h3>
</c:if>
<table class="chart2" summary="${biz_sort_nm} 제안요약서 정보 입력">
	<caption>${biz_sort_nm}제안요약서 정보 입력</caption>
	<colgroup>
		<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
		<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02'}">
			<col width="12%"/>
			<col width="35%"/>
			<col width="12%"/>
			<col width="40%"/>
		</c:if>

		<%-- 복지시설환경개선 --%>
		<c:if test="${biz_ty_code eq 'BTC09'}">
			<col width="12%"/>
			<col width="35%"/>
			<col width="12%"/>
			<col width="40%"/>
		</c:if>

		<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
		<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
			<col width="8.3%"/>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<col class="basic4_1"/>
			<col class="basic4_2"/>
			<col class="basic4_3"/>
			<col class="basic4_4"/>
			<col class="basic4_5"/>
			<col class="basic4_6"/>
			<col class="basic4_7"/>
			<col class="basic4_8"/>
			<col class="basic4_9"/>
		</c:if>
	</colgroup>
	<tbody>

	<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02'}">
		<tr>
			<th>지역</th>
			<td id="area_txt">${bsifVo.area}</td>
			<th>복지시설명(특수학교명)</th>
			<td id="instt_nm_txt">${bsifVo.instt_nm}</td>
		</tr>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th>지역</th>
			<td id="area_txt">${bsifVo.area}</td>
			<th>복지시설명</th>
			<td id="instt_nm_txt">${bsifVo.instt_nm}</td>
		</tr>
	</c:if>

	<tr>
		<th <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">colspan="2"</c:if>>사업명</th>
		<td <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">colspan="4"</c:if> <c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">colspan="4"</c:if> id="biz_nm_txt">${bsifVo.biz_nm}
		</td>
		<th <c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">colspan="2"</c:if>>
			<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC09'}">사업장소</c:if>
			<c:if test="${biz_ty_code eq 'BTC05'}">교육장소</c:if>
			<c:if test="${biz_ty_code eq 'BTC06'}">사업유형</c:if>
		</th>
		<td
				<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">colspan="4"</c:if>
				<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">colspan="3"</c:if> id="biz_addr_txt">
			<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC09'}">
				<c:if test="${not empty bsifVo.biz_zip}">
					${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} <br/>
				</c:if>
				${bsifVo.biz_adres} <br/>
				${bsifVo.biz_adres_detail}
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC05'}">
				${bsifVo.edc_place}
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC06'}">
				<c:if test="${cs_rss ne 0}">문화·공연</c:if>
				<c:if test="${cs_rss ne 0 and ef_rss ne 0}">, </c:if>
				<c:if test="${ef_rss ne 0}">(체험)박람회</c:if>
				<c:if test="${ef_rss ne 0 and fl_rss ne 0}">, </c:if>
				<c:if test="${fl_rss ne 0}">산림레저활동(걷기,등산,산악스키등)</c:if>
				<c:if test="${fl_rss ne 0 and etc_rss ne 0}">, </c:if>
				<c:if test="${etc_rss ne 0}">기 타</c:if>
			</c:if>
		</td>
	</tr>

	<%-- 복지 시설 나눔숲 (사회복지시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th>녹화유형</th>
			<td id="green_type_txt">
				<c:if test="${not empty bsifVo.ground_trplant_ar}">지상녹화</c:if><c:if test="${not empty bsifVo.rf_trplant_ar}"><c:if test="${not empty bsifVo.ground_trplant_ar}">/</c:if>옥상녹화</c:if>
			</td>
			<th>시설유형</th>
			<td>
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM218"/>
						<c:param name="chVal" value="${bsifVo.fclty_ty_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="fclty_ty_cc"/>
					</c:import>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th>시설구분</th>
			<td>
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM221"/>
						<c:param name="chVal" value="${bsifVo.fclty_dv_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="fclty_dv_cc"/>
					</c:import>
				</div>
			</td>
			<th>시설유형</th>
			<td>
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM218"/>
						<c:param name="chVal" value="${bsifVo.fclty_ty_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="fclty_ty_cc"/>
					</c:import>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th>숲유형</th>
			<td id="frt_ty_spcl_txt">${bsifVo.frt_ty_spcl_nm}</td>
			<th>장애유형</th>
			<td>
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM219"/>
						<c:param name="chVal" value="${bsifVo.trobl_ty_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="trobl_ty_cc"/>
					</c:import>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
		<c:set var="gr_mny_tot" value="${bsifVo.gf_01_ct + bsifVo.gf_02_ct + bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct + bsifVo.gf_08_ct + bsifVo.gf_14_ct + bsifVo.gf_20_ct + bsifVo.gf_21_ct}" />
		<c:set var="ja_mny_tot" value="${bsifVo.gf_22_ct + bsifVo.gf_23_ct + bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct + bsifVo.sp_05_ct + bsifVo.sp_07_ct + bsifVo.sp_08_ct + bsifVo.sp_09_ct}" />
		<c:set var="gr_ja_mny_tot" value="${gr_mny_tot + ja_mny_tot}" />
		<tr>
			<th>사업비</th>
			<th>녹색자금<br />(백만원)</th>
			<td class="gf_vals_tot_txt">${gr_mny_tot}</td>
			<td>
				<span class="gf_vals_agv">
					<c:if test="${(gr_mny_tot + gr_ja_mny_tot) > 0 }">
						<fmt:formatNumber value="${gr_mny_tot / gr_ja_mny_tot * 100}" pattern=".0"/>
					</c:if>
				</span>%
			</td>
			<th>자부담<br />(백만원)</th>
			<td class="jabudam_vals_tot_txt">${ja_mny_tot}</td>
			<td>
				<span class="jabudam_vals_agv">
					<c:if test="${(ja_mny_tot + gr_ja_mny_tot) > 0 }">
						<fmt:formatNumber value="${ja_mny_tot / gr_ja_mny_tot * 100}" pattern=".0"/>
					</c:if>
				</span>%
			</td>
			<th>합계<br />(백만원)</th>
			<td id="biz_total_txt">${gr_ja_mny_tot}</td>
			<td>100%</td>
			<th>재정자립도(위)</th>
			<td id="fnanc_idpdc_rank_txt">${bsifVo.fnanc_idpdc_rank}</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<tr>
			<th rowspan="2">기관개요</th>
			<th>설립년도</th>
			<td>
				<select name="fond_year" id="fond_year">
					<c:forEach var="yy" begin="0" end="${years-(years-80)}">
						<option value="${years-yy}" <c:if test="${years-yy eq bsifVo.fond_year}">selected</c:if> >${years-yy}</option>
					</c:forEach>
				</select>
			</td>
			<th>대표자</th>
			<td id="rprsntv_txt">${bsifVo.rprsntv}</td>
			<th>상근인원(명)</th>
			<td>
					${bsifVo.fte_nmpr}
				<input type="hidden" name="fte_nmpr" id="fte_nmpr" value="${bsifVo.fte_nmpr}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="상근인원(명)"/>
			</td>
			<th>
				법인허가<br/>주무부처
			</th>
			<td>
					${bsifVo.cpr_prmisn_miryfc}
				<input type="hidden" name="cpr_prmisn_miryfc" id="cpr_prmisn_miryfc" value="${bsifVo.cpr_prmisn_miryfc}" class="inp null_false" title="법인허가 주무부처"/>
			</td>
		</tr>
		<tr>
			<th>주소</th>
			<td colspan="7">
				<div id="biz_zip">${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)}</div>
				<span id="biz_adres">${bsifVo.biz_adres}</span>
				<span id="biz_adres_detail">${bsifVo.biz_adres_detail}</span>
			</td>
		</tr>
	</c:if>
	</tbody>
</table>

<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
	<h3 class="icon1">제안요약서(사업비내역 - 녹색자금)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col class="cost_1"/>
				<col class="cost_2"/>
				<col class="cost_3"/>
				<col class="cost_4"/>
				<col class="cost_5"/>
				<col class="cost_6"/>
				<col class="cost_7"/>
				<col class="cost_7"/>
				<col class="cost_8"/>
				<col class="cost_9"/>
				<col class="cost_10"/>
				<col class="cost_11"/>
			</colgroup>
			<thead>
			<tr>
				<th rowspan="2">기본<br/>조사비</th>
				<th rowspan="2">설계<br/> 용역</th>
				<th colspan="5">공사원가</th>
				<th rowspan="2">감리비</th>
				<th rowspan="2">회의비</th>
				<th rowspan="2">행사비</th>
				<th rowspan="2">일반<br/>관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>재료비</th>
				<th>노무비</th>
				<th>경비</th>
				<th>기타비용</th>
				<th>계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="gf_vals">
				<td><input type="text" name="gf_01_ct" id="gf_01_ct" value="${bsifVo.gf_01_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="기본조사비"/></td>
				<td><input type="text" name="gf_02_ct" id="gf_02_ct" value="${bsifVo.gf_02_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="설계용역"/></td>

				<td><input type="text" name="gf_03_ct" id="gf_03_ct" value="${bsifVo.gf_03_ct}" onkeyup="fnInputNumCom($(this));" class="inp gt_ct_val null_false validation number" title="공사원가 재료비"/></td>
				<td><input type="text" name="gf_04_ct" id="gf_04_ct" value="${bsifVo.gf_04_ct}" onkeyup="fnInputNumCom($(this));" class="inp gt_ct_val null_false validation number" title="공사원가 노무비"/></td>
				<td><input type="text" name="gf_05_ct" id="gf_05_ct" value="${bsifVo.gf_05_ct}" onkeyup="fnInputNumCom($(this));" class="inp gt_ct_val null_false validation number" title="공사원가 경비"/></td>
				<td><input type="text" name="gf_06_ct" id="gf_06_ct" value="${bsifVo.gf_06_ct}" onkeyup="fnInputNumCom($(this));" class="inp gt_ct_val null_false validation number" title="공사원가 기타비용"/></td>
				<td id="gf_ct_tot_txt">${bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct}</td>

				<td><input type="text" name="gf_08_ct" id="gf_08_ct" value="${bsifVo.gf_08_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="감리비"/></td>
				<td><input type="text" name="gf_14_ct" id="gf_14_ct" value="${bsifVo.gf_14_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="회의비"/></td>
				<td><input type="text" name="gf_20_ct" id="gf_20_ct" value="${bsifVo.gf_20_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="행사비"/></td>
				<td><input type="text" name="gf_21_ct" id="gf_21_ct" value="${bsifVo.gf_21_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="일반 관리비"/></td>
				<td class="gf_vals_tot_txt">${bsifVo.gf_01_ct + bsifVo.gf_02_ct + bsifVo.gf_03_ct + bsifVo.gf_04_ct + bsifVo.gf_05_ct + bsifVo.gf_06_ct + bsifVo.gf_08_ct + bsifVo.gf_14_ct + bsifVo.gf_20_ct+ bsifVo.gf_21_ct}</td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%--지역 사회 나눔숲, 다함께 나눔길 --%>
<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
	<h3 class="icon1">제안요약서(사업비내역 - 자부담)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col class="cost_1"/>
				<col class="cost_2"/>
				<col class="cost_3"/>
				<col class="cost_4"/>
				<col class="cost_5"/>
				<col class="cost_6"/>
				<col class="cost_7"/>
				<col class="cost_7"/>
				<col class="cost_8"/>
				<col class="cost_9"/>
				<col class="cost_10"/>
				<col class="cost_11"/>
			</colgroup>
			<thead>
			<tr>
				<th rowspan="2">기본<br/>조사비</th>
				<th rowspan="2">설계<br/> 용역</th>
				<th colspan="5">공사원가</th>
				<th rowspan="2">감리비</th>
				<th rowspan="2">회의비</th>
				<th rowspan="2">행사비</th>
				<th rowspan="2">일반<br/>관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>재료비</th>
				<th>노무비</th>
				<th>경비</th>
				<th>기타비용</th>
				<th>계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="jabudam_vals">
				<td><input type="text" name="gf_22_ct" id="gf_22_ct" value="${bsifVo.gf_22_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="기본조사비"/></td>
				<td><input type="text" name="gf_23_ct" id="gf_23_ct" value="${bsifVo.gf_23_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="설계용역"/></td>

				<td><input type="text" name="sp_01_ct" id="sp_01_ct" value="${bsifVo.sp_01_ct}" onkeyup="fnInputNumCom($(this));" class="inp jabudam_sp_val null_false validation number" title="공사원가 재료비"/></td>
				<td><input type="text" name="sp_02_ct" id="sp_02_ct" value="${bsifVo.sp_02_ct}" onkeyup="fnInputNumCom($(this));" class="inp jabudam_sp_val null_false validation number" title="공사원가 노무비"/></td>
				<td><input type="text" name="sp_03_ct" id="sp_03_ct" value="${bsifVo.sp_03_ct}" onkeyup="fnInputNumCom($(this));" class="inp jabudam_sp_val null_false validation number" title="공사원가 경비"/></td>
				<td><input type="text" name="sp_04_ct" id="sp_04_ct" value="${bsifVo.sp_04_ct}" onkeyup="fnInputNumCom($(this));" class="inp jabudam_sp_val null_false validation number" title="공사원가 기타비용"/></td>
				<td id="jabudam_sp_tot_txt">${bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct}</td>

				<td><input type="text" name="sp_05_ct" id="sp_05_ct" value="${bsifVo.sp_05_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="감리비"/></td>
				<td><input type="text" name="sp_07_ct" id="sp_07_ct" value="${bsifVo.sp_07_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="회의비"/></td>
				<td><input type="text" name="sp_08_ct" id="sp_08_ct" value="${bsifVo.sp_08_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="행사비"/></td>
				<td><input type="text" name="sp_09_ct" id="sp_09_ct" value="${bsifVo.sp_09_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="일반 관리비"/></td>
				<td class="jabudam_vals_tot_txt">${bsifVo.gf_22_ct + bsifVo.gf_23_ct + bsifVo.sp_01_ct + bsifVo.sp_02_ct + bsifVo.sp_03_ct + bsifVo.sp_04_ct + bsifVo.sp_05_ct + bsifVo.sp_07_ct + bsifVo.sp_08_ct + bsifVo.sp_09_ct}</td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
	<h3 class="icon1">사업계획요약서(사업비구성)<span class="unit">(단위:백만원)</span></h3>
	<div class="cost_input">
		<table  class="chart1" summary="숲 체험교육(체험교육) 제안요약서(사업비 구성)  정보 입력">
			<caption>숲 체험교육(체험교육) 제안요약서(사업비 구성) 정보 입력</caption>
			<thead>
			<tr>
				<th colspan="3">인건비</th>
				<th colspan="11">직접비</th>
				<th rowspan="2">일반<br />관리비</th>
				<th rowspan="2">합계</th>
			</tr>
			<tr>
				<th>내부 인건비</th>
				<th>외부인건비</th>
				<th>강사비</th>
				<th>소모품비</th>
				<th>광고선전비</th>
				<th>도서인쇄비</th>
				<th>지급수수료</th>
				<th>회의비</th>
				<th>사업진행비</th>
				<th>여비교통비</th>
				<th>기본조사비</th>
				<th>설계용역비</th>
				<th>공사원가</th>
				<th>감리비</th>
			</tr>
			</thead>
			<tbody>
			<tr id="gf_ct_vals">
				<td><input type="text" name="gf_07_ct" id="gf_07_ct" value="${bsifVo.gf_07_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="내부 인건비"/></td>
				<td><input type="text" name="gf_08_ct" id="gf_08_ct" value="${bsifVo.gf_08_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="외부 인건비"/></td>
				<td><input type="text" name="gf_09_ct" id="gf_09_ct" value="${bsifVo.gf_09_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="강사비"/></td>
				<td><input type="text" name="gf_10_ct" id="gf_10_ct" value="${bsifVo.gf_10_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="소모품비"/></td>
				<td><input type="text" name="gf_11_ct" id="gf_11_ct" value="${bsifVo.gf_11_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="광고선전비"/></td>
				<td><input type="text" name="gf_12_ct" id="gf_12_ct" value="${bsifVo.gf_12_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="도서인쇄비"/></td>
				<td><input type="text" name="gf_13_ct" id="gf_13_ct" value="${bsifVo.gf_13_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="지급수수료"/></td>
				<td><input type="text" name="gf_14_ct" id="gf_14_ct" value="${bsifVo.gf_14_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="회의비"/></td>
				<td><input type="text" name="gf_15_ct" id="gf_15_ct" value="${bsifVo.gf_15_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="사업진행비"/></td>
				<td><input type="text" name="gf_16_ct" id="gf_16_ct" value="${bsifVo.gf_16_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="여비교통비"/></td>
				<td><input type="text" name="gf_17_ct" id="gf_17_ct" value="${bsifVo.gf_17_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="기본조사비"/></td>
				<td><input type="text" name="gf_18_ct" id="gf_18_ct" value="${bsifVo.gf_18_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="설계용역비"/></td>
				<td><input type="text" name="sp_04_ct" id="sp_04_ct" value="${bsifVo.sp_04_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="공사원가"/></td>
				<td><input type="text" name="gf_19_ct" id="gf_19_ct" value="${bsifVo.gf_19_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="감리비"/></td>
				<td><input type="text" name="gf_21_ct" id="gf_21_ct" value="${bsifVo.gf_21_ct}" class="inp null_false validation number" onkeyup="fnInputNumCom($(this));" title="일반 관리비"/></td>
				<td id="gf_ct_vals_tot_txt">${bsifVo.gf_07_ct + bsifVo.gf_08_ct + bsifVo.gf_09_ct + bsifVo.gf_10_ct + bsifVo.gf_11_ct + bsifVo.gf_12_ct + bsifVo.gf_13_ct + bsifVo.gf_14_ct + bsifVo.gf_15_ct + bsifVo.gf_16_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct + bsifVo.gf_21_ct}</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="top_tit">
		<h3 class="icon1">제안요약서(사업연속성)</h3>
		<span class="mbtn"><button type="button" onclick="fnBizCotnSeqAdd();">추가</button></span>
	</div>
	<table  class="chart1" summary="숲 체험교육(체험교육) 제안요약서(사업연속성) 정보 입력">
		<caption>숲 체험교육(체험교육) 제안요약서(사업연속성)정보 입력</caption>
		<colgroup>
			<col width="11%"/>
			<col width="auto"/>
			<col width="15%"/>
			<col width="15%"/>
			<col width="20%"/>
			<col width="7%"/>
		</colgroup>
		<thead>
		<tr>
			<th>구분(년도)</th>
			<th>사업명</th>
			<th>녹색자금(백만원)</th>
			<th>수혜 연인원(명)</th>
			<th>평가결과</th>
			<th>삭제</th>
		</tr>
		</thead>
		<tbody class="bizCotnSeq">
		<tr class="bizCotnSeq_0" style="display:none;">
			<td colspan="6">등록된 사업연속성이 없습니다.</td>
		</tr>
		<c:if test="${not empty bcsList}">
			<c:forEach var="rs" items="${bcsList}" varStatus="sts">
				<tr>
					<td>
						<input type="hidden" name="biz_ctnu_id" value="${rs.biz_ctnu_id}" />
						<select name="year" id="year">
							<c:forEach var="y" begin="0" end="${years-(years-80)}">
								<option value="${years-y}" <c:if test="${years-y eq rs.year}">selected</c:if> >${years-y}</option>
							</c:forEach>
						</select>
					</td>
					<td><input type="text" name="biz_nm" value="${rs.biz_nm}" class="inp null_false" title="사업명" /></td>
					<td><input type="text" name="green_fund" value="${rs.green_fund}" onkeyup="fnInputNumCom($(this));" class="ninp null_false validation number" title="녹색자금(백만원)" /></td>
					<td><input type="text" name="rcvfvr_year_nmpr" value="${rs.rcvfvr_year_nmpr}" onkeyup="fnInputNumCom($(this));" class="ninp null_false validation number" title="수혜 연인원(명)" /></td>
					<td><input type="text" name="evl_result" value="${rs.evl_result}" class="inp null_false" title="평가결과" /></td>
					<td><span class="mbtn"><button type="button" onclick="fnBizCotnSeqDelete($(this));">삭제</button></span></td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${empty bcsList}">
			<tr>
				<td>
					<input type="hidden" name="biz_ctnu_id" />
					<select name="year" id="year">
						<c:forEach var="y" begin="0" end="${years-(years-80)}">
							<option value="${years-y}" <c:if test="${years-y eq rs.year}">selected</c:if> >${years-y}</option>
						</c:forEach>
					</select>
				</td>
				<td><input type="text" name="biz_nm" class="inp null_false" title="사업명" /></td>
				<td><input type="text" name="green_fund" onkeyup="fnInputNumCom($(this));" class="ninp null_false validation number" title="녹색자금(백만원)" /></td>
				<td><input type="text" name="rcvfvr_year_nmpr" onkeyup="fnInputNumCom($(this));" class="ninp null_false validation number" title="수혜 연인원(명)" /></td>
				<td><input type="text" name="evl_result" class="inp null_false" title="평가결과" /></td>
				<td><span class="mbtn"><button type="button" onclick="fnBizCotnSeqDelete($(this));">삭제</button></span></td>
			</tr>
		</c:if>
		</tbody>
	</table>
</c:if>

<%-- 복지시설환경개선 --%>
<c:if test="${biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">제안요약서(사업비내역 - 녹색자금)<span class="unit">(단위:백만원)</span></h3>

	<div class="cost_input">
		<table class="chart1" summary="${biz_sort_nm} 제안요약서(사업비내역) 정보 입력">
			<caption>${biz_sort_nm} 제안요약서(사업비내역) 정보 입력</caption>
			<colgroup>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
			</colgroup>
			<thead>
			<tr>
				<th>인건비</th>
				<th>회의비</th>
				<th>여비<br/> 교통비</th>
				<th>지급<br/> 수수료</th>
				<th>기본<br/> 조사비</th>
				<th>설계비</th>
				<th>공사원가</th>
				<th>감리비</th>
				<th>일반<br/>관리비</th>
				<th>합계</th>
			</tr>
			</thead>
			<tbody>
			<tr id="gf_vals">
				<td><input type="text" name="gf_07_ct" id="gf_07_ct" value="${bsifVo.gf_07_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="인건비"/></td>
				<td><input type="text" name="gf_14_ct" id="gf_14_ct" value="${bsifVo.gf_14_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="회의비"/></td>
				<td><input type="text" name="gf_16_ct" id="gf_16_ct" value="${bsifVo.gf_16_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="여비교통비"/></td>
				<td><input type="text" name="gf_13_ct" id="gf_13_ct" value="${bsifVo.gf_13_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="지급수수료"/></td>
				<td><input type="text" name="gf_17_ct" id="gf_17_ct" value="${bsifVo.gf_17_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="기본조사비"/></td>
				<td><input type="text" name="gf_18_ct" id="gf_18_ct" value="${bsifVo.gf_18_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="설계비"/></td>
				<td><input type="text" name="sp_04_ct" id="sp_04_ct" value="${bsifVo.sp_04_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="공사원가"/></td>
				<td><input type="text" name="gf_19_ct" id="gf_19_ct" value="${bsifVo.gf_19_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="감리비"/></td>
				<td><input type="text" name="gf_21_ct" id="gf_21_ct" value="${bsifVo.gf_21_ct}" onkeyup="fnInputNumCom($(this));" class="inp null_false validation number" title="일반관리비"/></td>
				<td class="gf_vals_tot_txt">${bsifVo.gf_07_ct + bsifVo.gf_14_ct + bsifVo.gf_16_ct + bsifVo.gf_13_ct + bsifVo.gf_17_ct + bsifVo.gf_18_ct + bsifVo.sp_04_ct + bsifVo.gf_19_ct + bsifVo.gf_21_ct}</td>
			</tr>
			</tbody>
		</table>
	</div>
</c:if>

<h3 class="icon1">제안요약서(사업개요)</h3>
<table class="chart2" summary="${biz_sort_nm} 제안요약서(사업개요) 정보 입력">
	<caption>${biz_sort_nm}제안요약서(사업개요) 정보 입력</caption>
	<colgroup>
		<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길 --%>
		<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
			<col class="outline_1"/>
			<col class="outline_2"/>
			<col class="outline_3"/>
			<col class="outline_4"/>
			<col class="outline_5"/>
		</c:if>

		<%-- 복지시설환경개선 --%>
		<c:if test="${biz_ty_code eq 'BTC09'}">
			<col class="outline_1"/>
			<col class="outline_2"/>
			<col class="outline_3"/>
			<col class="outline_4"/>
			<col class="outline_5"/>
		</c:if>

		<%-- 숲체험ㆍ교육 <체험ㆍ휴양교육사업> --%>
		<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
			<col class="expost_1"/>
			<col class="expost_2"/>
		</c:if>
	</colgroup>
	<tbody>

	<%-- 복지 시설 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03'}">
		<tr>
			<th rowspan="2">토지소유자 및 조성면적</th>
			<th>조성면적(㎡)</th>
			<td>
				<input type="text" name="make_ar" id="make_ar" value="${bsifVo.make_ar}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성면적(㎡)"/>
			</td>
			<th>시설전체면적(㎡)</th>
			<td>
				<input type="text" name="fclty_all_ar" id="fclty_all_ar" value="${bsifVo.fclty_all_ar}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설전체면적(㎡)"/>
			</td>
		</tr>
		<tr>
			<td colspan="4">※ Daum지도 20m 축척상태에서 면적측정기능을 이용하여 측정한 수치를 기입</td>
		</tr>
	</c:if>

	<%-- 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC07'}">
		<tr>
			<th rowspan="2">토지소유자 및 조성면적</th>
			<th>조성면적(㎡)</th>
			<td>
				<input type="text" name="make_ar" id="make_ar" value="${bsifVo.make_ar}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성면적(㎡)"/>
			</td>
			<th>나눔길 길이(m)</th>
			<td>
				<input type="text" name="fclty_all_ar" id="fclty_all_ar" value="${bsifVo.fclty_all_ar}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설전체면적(㎡)"/>
			</td>
		</tr>
		<tr>
			<td colspan="4">※ Daum지도 20m 축척상태에서 면적측정기능을 이용하여 측정한 수치를 기입</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (사회복지시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01'}">
		<tr>
			<th rowspan="2">토지소유자 및 조성면적</th>
			<th>조성면적(㎡)</th>
			<td>
				<span id="make_area_txt">${bsifVo.ground_trplant_ar + bsifVo.rf_trplant_ar}</span>
			</td>
			<th>시설전체면적(㎡)</th>
			<td><input type="text" name="fclty_all_ar" id="fclty_all_ar" value="${bsifVo.fclty_all_ar}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설전체면적(㎡)"/></td>
		</tr>
		<tr>
			<td colspan="4">
				※ Daum지도 20m 축척상태에서 면적측정기능을 이용하여 측정한 수치를 기입
			</td>
		</tr>

		<tr>
			<th>조성유형</th>
			<td colspan="4">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM214"/>
						<c:param name="chVal" value="${bsifVo.make_ty}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="make_ty"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th>수용정원(명)</th>
			<td colspan="4" id="aceptnc_psncpa_txt">${bsifVo.aceptnc_psncpa}</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC02'}">
		<tr>
			<th>이용자수(관리자 포함)</th>
			<td colspan="4" id="user_qy_txt">${bsifVo.user_qy + bsifVo.mngr_qy}</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲, 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}">
		<tr>
			<th>대상부지</th>
			<td colspan="4">
				<div class="radio_box">
					<input type="checkbox" name="tp_drng_at" id="tp_drng_at" value="Y" <c:if test="${bsifVo.tp_drng_at eq 'Y'}"> checked="checked" </c:if> title="배수공사 필요"/>
					<label for="tp_drng_at">배수공사 필요</label>
					<input type="checkbox" name="tp_remvl_at" id="tp_remvl_at" value="Y" <c:if test="${bsifVo.tp_remvl_at eq 'Y'}"> checked="checked" </c:if> title="포장 등 철거공사 필요"/>
					<label for="tp_remvl_at">포장 등 철거공사 필요</label>
					<input type="checkbox" name="tp_splemnt_at" id="tp_splemnt_at" value="Y" <c:if test="${bsifVo.tp_splemnt_at eq 'Y'}"> checked="checked" </c:if> title="기존 수목보완 필요"/>
					<label for="tp_splemnt_at">기존 수목보완 필요</label>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲, 지역 사회 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03'}">
		<tr>
			<c:if test="${biz_ty_code ne 'BTC03'}">
				<th>대상부지 이용현황</th>
			</c:if>
			<c:if test="${biz_ty_code eq 'BTC03'}">
				<th>지역사회나눔숲 사항</th>
			</c:if>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.tp_use_sttus, newLineChar, '<br/>')}" escapeXml="false" />
					<c:if test="${biz_ty_code eq 'BTC03'}">
						<textarea name="tp_use_sttus" id="tp_use_sttus" cols="10" rows="10" class="inp_area null_false hidden" placeholder="⦁ (대상부지에 대한 현재 이용현황, 인근에 소외계층 시설을 기재)
⦁ (대상부지가 사회, 경제적 취약계층 생활권의 녹지취약지역임을 설명하는 내용 등을 기재)">${bsifVo.tp_use_sttus}</textarea>
					</c:if>
					<c:if test="${biz_ty_code eq 'BTC03'}">
						<textarea name="tp_use_sttus" id="tp_use_sttus" cols="10" rows="10" class="inp_area null_false hidden" placeholder="※ ① 현재용도(주차장, 옥상 등) 및 철거공사 필요여부 기재, 옥상녹화의 경우 전문가 적합여부 의견
※ ② 옥상녹화는 구조안전용역을 선행하여 국토교통부고제 제2014-46호 조경기준 제14조, 15조에 따라
교목 식재토심이 70cm(인공토양 60cm)이상 적용가능 한지에 대한 내용이 담긴 구조안전확인서 등 제시">${bsifVo.tp_use_sttus}</textarea>
					</c:if>
				</div>
			</td>
		</tr>
		<tr>
			<th>사업내용 (단위 : 백만원)</th>
			<td colspan="4">
				<table class="chart1" summary="사업내용 내용입력">
					<caption>사업내용 내용입력</caption>
					<colgroup>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
					</colgroup>
					<thead>
					<tr>
						<th>식재공사비</th>
						<th>기반조성비</th>
						<th>시설물공사비</th>
						<th>기타비용</th>
						<th>공사원가</th>
					</tr>
					</thead>
					<tbody>
					<tr id="biz_ct">
						<c:set var="biz_ct_tot" value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" />
						<td><input type="text" name="plt_ct" id="plt_ct" value="${bsifVo.plt_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="식재공사비"/> (<span><c:if test="${not empty bsifVo.plt_ct}"><fmt:formatNumber value="${ bsifVo.plt_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="fnd_ct" id="fnd_ct" value="${bsifVo.fnd_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="기반조성비"/> (<span><c:if test="${not empty bsifVo.fnd_ct}"><fmt:formatNumber value="${ bsifVo.fnd_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="fct_ct" id="fct_ct" value="${bsifVo.fct_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설물공사비"/> (<span><c:if test="${not empty bsifVo.fct_ct}"><fmt:formatNumber value="${ bsifVo.fct_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="etc_ct" id="etc_ct" value="${bsifVo.etc_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타비용"/> (<span><c:if test="${not empty bsifVo.etc_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><span id="biz_ct_tot">${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}</span> (100%)</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<tr>
			<th>건물현황</th>
			<td colspan="4">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM222"/>
						<c:param name="chVal" value="${bsifVo.build_st_cc}"/>
						<c:param name="elType" value="radioWithText"/>
						<c:param name="elName" value="build_st_cc"/>
						<c:param name="classN" value="s"/>
						<c:param name="txtElName" value="build_st_dt"/>
						<c:param name="txtPosition" value="5"/>
						<c:param name="txtChVal" value="${bsifVo.build_st_dt}"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th>건물위치</th>
			<td colspan="4">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM223"/>
						<c:param name="chVal" value="${bsifVo.build_lc_cc}"/>
						<c:param name="elType" value="radioWithText"/>
						<c:param name="elName" value="build_lc_cc"/>
						<c:param name="classN" value="e"/>
						<c:param name="txtElName" value="build_lc_dt"/>
						<c:param name="txtEtc" value="층"/>
						<c:param name="txtPosition" value="3"/>
						<c:param name="txtChVal" value="${bsifVo.build_lc_dt}"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th>건물 준공일</th>
			<td colspan="2">
				<input type="text" name="build_fond_year" id="build_fond_year" value="${bsifVo.build_fond_year}" maxlength="4" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="년"/> 년
				&nbsp;
				<input type="text" name="build_fond_month" id="build_fond_month" value="${bsifVo.build_fond_month}" maxlength="2" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="월"/> 월
			</td>
			<th>전용면적</th>
			<td colspan="2">
				<input type="text" name="own_ar1" id="own_ar1" value="${bsifVo.own_ar1}" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="전용면적1(㎡)"/> ㎡
				&nbsp;.&nbsp;
				<input type="text" name="own_ar2" id="own_ar2" value="${bsifVo.own_ar2}" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="전용면적2(㎡)"/> ㎡
			</td>
		</tr>
		<tr>
			<th>거주유형</th>
			<td colspan="2">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM224"/>
						<c:param name="chVal" value="${bsifVo.resi_ty_cc}"/>
						<c:param name="elType" value="radioWithText"/>
						<c:param name="elName" value="resi_ty_cc"/>
						<c:param name="classN" value="s"/>
						<c:param name="txtElName" value="resi_ty_dt"/>
						<c:param name="txtPosition" value="2"/>
						<c:param name="txtChVal" value="${bsifVo.resi_ty_dt}"/>
					</c:import>
				</div>
			</td>
			<th>조성면적 및 <br/> 조성공간 개소수</th>
			<td colspan="2">
				<input type="text" name="make_ar" id="make_ar" value="${bsifVo.make_ar}" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성면적(㎡)"/> ㎡
				&nbsp;,&nbsp;
				<input type="text" name="make_place_num" id="make_place_num" value="${bsifVo.make_place_num}" class="ainp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성공간개소수"/> 실
			</td>
		</tr>
		<tr>
			<th>5년이내 용도변경 <br/> 예정여부</th>
			<td colspan="4">
				<div class="radio_box">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM225"/>
						<c:param name="chVal" value="${bsifVo.typ_ch_cc}"/>
						<c:param name="elType" value="radio"/>
						<c:param name="elName" value="typ_ch_cc"/>
					</c:import>
				</div>
			</td>
		</tr>
		<tr>
			<th rowspan="2">이용인 수(일평균)</th>
			<th>시설생활 이용인 수</th>
			<td><input type="text" name="fclty_user_qy" id="fclty_user_qy" value="${bsifVo.fclty_user_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설생활이용자수"/></td>
			<th>조성공간의 이용자 수</th>
			<td><input type="text" name="make_user_qy" id="make_user_qy" value="${bsifVo.make_user_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="조성공간이용자수"/></td>
		</tr>
		<tr>
			<td colspan="4">
				※ 시설의 등록자 또는 회원 등 확인 가능한 인원수 기재
			</td>
		</tr>
		<tr>
			<th>조성대상지 현재 상태 <br/> (현재용도)</th>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.tp_now_sttus, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea cols="10" rows="10" name="tp_now_sttus" id="tp_now_sttus" class="inp_area null_false hidden" title="조성대상지">${bsifVo.tp_now_sttus}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<th>사업목적</th>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.biz_purps, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea cols="10" rows="10" name="biz_purps" id="biz_purps" class="inp_area null_false hidden" title="사업목적">${bsifVo.biz_contents}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<th>사업내용</th>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.biz_contents, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea cols="10" rows="10" name="biz_contents" id="biz_contents" class="inp_area null_false hidden" title="사업내용">${bsifVo.biz_contents}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<th>기대효과</th>
			<td colspan="4">
				<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
				<textarea name="expc_effect" id="expc_effect" cols="10" rows="10" class="inp_area null_false hidden" title="기대효과">${bsifVo.expc_effect}</textarea>
			</td>
		</tr>
	</c:if>

	<%-- 다함께 나눔길  --%>
	<c:if test="${biz_ty_code eq 'BTC07'}">
		<tr>
			<th>사업비 (단위 : 백만원)</th>
			<td colspan="4">
				<table class="chart1" summary="사업내용 내용입력">
					<caption>사업내용 내용입력</caption>
					<colgroup>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
						<col width="20%"/>
					</colgroup>
					<thead>
					<tr>
						<th>식재공사비</th>
						<th>기반조성비</th>
						<th>시설물공사비</th>
						<th>기타비용</th>
						<th>공사원가</th>
					</tr>
					</thead>
					<tbody>
					<tr id="biz_ct">
						<c:set var="biz_ct_tot" value="${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}" />
						<td><input type="text" name="plt_ct" id="plt_ct" value="${bsifVo.plt_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="식재공사비"/> (<span><c:if test="${not empty bsifVo.plt_ct}"><fmt:formatNumber value="${ bsifVo.fnd_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="fnd_ct" id="fnd_ct" value="${bsifVo.fnd_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="기반조성비"/> (<span><c:if test="${not empty bsifVo.fnd_ct}"><fmt:formatNumber value="${ bsifVo.fct_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="fct_ct" id="fct_ct" value="${bsifVo.fct_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="시설물공사비"/> (<span><c:if test="${not empty bsifVo.fct_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><input type="text" name="etc_ct" id="etc_ct" value="${bsifVo.etc_ct}" maxlength="10" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="기타비용"/> (<span><c:if test="${not empty bsifVo.etc_ct}"><fmt:formatNumber value="${ bsifVo.etc_ct / biz_ct_tot * 100}" pattern=".0"/></c:if></span>%)</td>
						<td><span id="biz_ct_tot">${bsifVo.plt_ct + bsifVo.fnd_ct + bsifVo.fct_ct + bsifVo.etc_ct}</span> (100%)</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲 (사회복지시설, 특수교육시설) --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02'}">
		<tr>
			<th>식재공사내역</th>
			<th>교목류(주)</th>
			<td>
				<input type="text" name="pcp_qy" id="pcp_qy" value="${bsifVo.pcp_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="교목류(주)"/>
			</td>
			<th>관목 및 초화류(본)</th>
			<td>
				<input type="text" name="psf_qy" id="psf_qy" value="${bsifVo.psf_qy}" class="ninp null_false validation number" onkeyup="fnInputNumCom($(this));" title="관목 및 초화류(본)"/>
			</td>
		</tr>
		<tr>
			<th>기반공사내역</th>
			<td colspan="4">
				<textarea cols="10" rows="10" name="isw_cn" id="isw_cn" maxlength="254" class="inp_area null_false" title="기반공사내역">${bsifVo.isw_cn}</textarea>
			</td>
		</tr>
		<tr>
			<th>시설물공사내역</th>
			<td colspan="4">
				<textarea cols="10" rows="10" name="fcw_cn" id="fcw_cn" maxlength="254" class="inp_area null_false" title="시설물공사내역">${bsifVo.fcw_cn}</textarea>
			</td>
		</tr>
	</c:if>

	<c:if test="${biz_ty_code ne 'BTC09'}">
		<tr>
			<th>사업목적</th>
			<td <c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07'}"> colspan="4"</c:if>>
				<c:out value="${fn:replace(bsifVo.biz_purps, newLineChar, '<br/>')}" escapeXml="false" />
				<textarea cols="10" rows="10" name="biz_purps" id="biz_purps" class="inp_area null_false hidden" title="사업목적">${bsifVo.biz_purps}</textarea>
			</td>
		</tr>
	</c:if>

	<%-- 지역 사회 나눔숲 --%>
	<c:if test="${biz_ty_code eq 'BTC03'}">
		<tr>
			<th>지역사회</th>
			<td colspan="4">
				<p>나눔숲의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안</p>
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.ud_bf_dstnct_method, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea name="ud_bf_dstnct_method" id="ud_bf_dstnct_method" cols="10" rows="10" class="inp_area null_false hidden" title="나눔숲의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안" placeholder="(식재비율 50% 이상을 원칙으로 함)
(시설의 경우 UD, BF 등을 반영한 시설내역, 투입예산 및 비율/공사원가 시설물공사의 30%이내)
(ex.휠체어 이동이 가능한 동선·포장계획, 점자블럭·안내판, 경고시설 등)">${bsifVo.ud_bf_dstnct_method}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<th>기대효과</th>
			<td colspan="4">
				<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
				<textarea name="expc_effect" id="expc_effect" cols="10" rows="10" class="inp_area null_false hidden" title="기대효과">${bsifVo.expc_effect}</textarea>
			</td>
		</tr>
	</c:if>

	<%-- 다함께 나눔길 --%>
	<c:if test="${biz_ty_code eq 'BTC07'}">
		<tr>
			<th>사업내용 및 차별화 방안</th>
			<td colspan="4">
				<p>나눔길의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안</p>
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.ud_bf_dstnct_method, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea name="ud_bf_dstnct_method" id="ud_bf_dstnct_method" cols="10" rows="10" class="inp_area null_false hidden" title="나눔길의 사회적 약자층을 위한 UD, BF 등의 반영사항 및 도시숲과 차별화 방안" placeholder="(식재비율 50% 이상을 원칙으로 함)
(시설의 경우 UD, BF 등을 반영한 시설내역, 투입예산 및 비율/공사원가 시설물공사의 30%이내)
(ex.휠체어 이동이 가능한 동선·포장계획, 점자블럭·안내판, 경고시설 등)">${bsifVo.ud_bf_dstnct_method}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<th>기대효과</th>
			<td colspan="4">
				<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
				<textarea name="expc_effect" id="expc_effect" cols="10" rows="10" class="inp_area null_false hidden" title="기대효과">${bsifVo.expc_effect}</textarea>
			</td>
		</tr>
	</c:if>

	<%-- 복지시설환경개선 --%>
	<c:if test="${biz_ty_code eq 'BTC09'}">
		<%--<tr>
			<th>주요 추진일정</th>
			<td colspan="4">
				<ul class="icon3_list cal_box_list">
					<li>
						<span>기본조사 및 설계 </span>
						<div class="cal_box">
								${bsifVo.prtnfx_dsgn_bgnde} ~ ${bsifVo.prtnfx_dsgn_endde}
							<input type="hidden" name="prtnfx_dsgn_bgnde" id="prtnfx_dsgn_bgnde" value="${bsifVo.prtnfx_dsgn_bgnde}" readonly="readonly" class="cinp null_false" title="기본조사 및 설계 시작날짜"/>
							<input type="hidden" name="prtnfx_dsgn_endde" id="prtnfx_dsgn_endde" value="${bsifVo.prtnfx_dsgn_endde}" readonly="readonly" class="cinp null_false" title="기본조사 및 설계 종료날짜"/>
						</div>
					</li>
					<li>
						<span>기존시설철거 </span>
						<div class="cal_box">
								${bsifVo.fclty_pudn_bgnde} ~ ${bsifVo.fclty_pudn_endde}
							<input type="hidden" name="fclty_pudn_bgnde" id="fclty_pudn_bgnde" value="${bsifVo.fclty_pudn_bgnde}" readonly="readonly" class="cinp null_false" title="기존시설철거 시작날짜"/>
							<input type="hidden" name="fclty_pudn_endde" id="fclty_pudn_endde" value="${bsifVo.fclty_pudn_endde}" readonly="readonly" class="cinp null_false" title="기존시설철거 종료날짜"/>
						</div>
					</li>
					<li>
						<span>시공 </span>
						<div class="cal_box">
								${bsifVo.construction_bgnde} ~ ${bsifVo.construction_endde}
							<input type="hidden" name="construction_bgnde" id="construction_bgnde" value="${bsifVo.construction_bgnde}" readonly="readonly" class="cinp null_false" title="시공 시작날짜"/>
							<input type="hidden" name="construction_endde" id="construction_endde" value="${bsifVo.construction_endde}" readonly="readonly" class="cinp null_false" title="시공 종료날짜"/>
						</div>
					</li>
					<li>
						<span>기타 </span>
						<div class="area_box">
								${bsifVo.etc_purps_contents}
							<input type="hidden" name="etc_purps_contents" id="etc_purps_contents" value="${bsifVo.etc_purps_contents}" class="ninp null_false" title="기타"/>
						</div>
					</li>
				</ul>
			</td>
		</tr>--%>
		<tr>
			<th>대체공간확보계획</th>
			<td colspan="4">
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.alt_space_plan, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea name="alt_space_plan" id="alt_space_plan" cols="10" rows="10" class="inp_area null_false hidden" title="대체공간확보계획">${bsifVo.alt_space_plan}</textarea>
				</div>
			</td>
		</tr>
	</c:if>

	<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길  --%>
	<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC08'}">
		<tr>
			<th>주요 추진일정</th>
			<td colspan="4">
				<ul class="icon3_list cal_box_list">
					<li>
						<span>주민설명회 </span>

						<div class="cal_box">
								${bsifVo.prtnfx_dc_dt}
							<input type="hidden" name="prtnfx_dc_dt" id="prtnfx_dc_dt" value="${bsifVo.prtnfx_dc_dt}" readonly="readonly" class="cinp null_false" title="주민설명회 날짜"/>
						</div>
					</li>
					<li>
						<span>기본 및 실시 설계 </span>

						<div class="cal_box">
								${bsifVo.prtnfx_dsgn_bgnde} ~ ${bsifVo.prtnfx_dsgn_endde}
							<input type="hidden" name="prtnfx_dsgn_bgnde" id="prtnfx_dsgn_bgnde" value="${bsifVo.prtnfx_dsgn_bgnde}" readonly="readonly" class="cinp null_false" title="기본 및 실시 설계 시작날짜"/>
							<input type="hidden" name="prtnfx_dsgn_endde" id="prtnfx_dsgn_endde" value="${bsifVo.prtnfx_dsgn_endde}" readonly="readonly" class="cinp null_false" title="기본 및 실시 설계 종료날짜"/>
						</div>
					</li>
					<li>
						<c:if test="${biz_ty_code eq 'BTC08'}">
							<span>무장애 나눔길 공사</span>
						</c:if>
						<c:if test="${biz_ty_code ne 'BTC08'}">
							<span>수목식재 및 공사</span>
						</c:if>

						<div class="cal_box">
								${bsifVo.prtnfx_cntrwk_bgnde} ~ ${bsifVo.prtnfx_cntrwk_endde}
							<input type="hidden" name="prtnfx_cntrwk_bgnde" id="prtnfx_cntrwk_bgnde" value="${bsifVo.prtnfx_cntrwk_bgnde}" readonly="readonly" class="cinp null_false" title="목식재 및 공사 시작날짜"/>
							<input type="hidden" name="prtnfx_cntrwk_endde" id="prtnfx_cntrwk_endde" value="${bsifVo.prtnfx_cntrwk_endde}" readonly="readonly" class="cinp null_false" title="목식재 및 공사 종료날짜"/>
						</div>
					</li>
				</ul>
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05'}">
		<tr>
			<th>보유 시설‧강사 현황</th>
			<td>
				※ 해당사항 없으면 기재 안함
				<div class="area_box">
					<c:out value="${fn:replace(bsifVo.fclty_instrctr_sttus, newLineChar, '<br/>')}" escapeXml="false" />
					<textarea name="fclty_instrctr_sttus" id="fclty_instrctr_sttus" cols="10" rows="10" class="inp_area hidden">${bsifVo.fclty_instrctr_sttus}</textarea>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="top_tit">
					<strong>※  운영프로그램 : <span class="program_cnt"><c:if test="${empty bopList}">1</c:if><c:if test="${not empty bopList}">${fn:length(bopList)}</c:if></span> 개(컨소시엄의 경우 프로그램명을 협약기관별을 구분하여 기재)</strong>
				</div>

				<div class="program_input">
					<table summary="운영프로그램 입력폼입니다." class="chart1">
						<caption>운영프로그램 입력</caption>
						<colgroup>
							<col class="prg_1" />
							<col class="prg_2" />
							<col class="prg_3" />
							<col class="prg_4" />
							<col class="prg_5" />
							<col class="prg_6" />
							<col class="prg_7" />
								<%--<col class="prg_8"/>--%>
							<col class="prg_9" />
							<col class="prg_10" />
							<col class="prg_11" />
						</colgroup>
						<thead>
						<tr>
							<th rowspan="2">프로그램명</th>
							<th rowspan="2">날짜</th>
							<th rowspan="2">총 횟수</th>
							<th rowspan="2">연속여부</th>
							<th colspan="2">회당</th>
							<th rowspan="2">교육대상</th>
							<th rowspan="2">회당인원</th>
							<th rowspan="2">총인원</th>
							<th rowspan="2">연인원</th>
							<th rowspan="2">교육장소</th>
							<th rowspan="2">삭제</th>
						</tr>
						<tr>
							<th>교육일</th>
							<th>강사수 </th>
						</tr>
						</thead>
						<tbody class="bizOpePgm">
						<tr class="BizOpePgm_0" style="display:none;">
							<td colspan="12">등록된 운영프로그램이 없습니다.</td>
						</tr>
						<c:if test="${not empty bopList}">
							<c:set var="edc_co" value="0" />
							<c:set var="tot_co" value="0" />
							<c:set var="round_edcde_co" value="0" />
							<c:set var="edc_trget_01_co" value="0" />
							<c:set var="edc_trget_03_co" value="0" />
							<c:set var="edc_trget_04_co" value="0" />
							<c:set var="edc_trget_05_co" value="0" />
							<c:set var="edc_trget_06_co" value="0" />
							<c:set var="edc_trget_01_co_year" value="0" />
							<c:set var="edc_trget_03_co_year" value="0" />
							<c:set var="edc_trget_05_co_year" value="0" />
							<c:forEach var="rs" items="${bopList}">
								<c:set var="edc_co" value="${edc_co + rs.edc_co}" />
								<c:set var="tot_co" value="${tot_co + rs.tot_co}" />
								<c:set var="round_edcde_co" value="${round_edcde_co + rs.round_edcde_co}" />
								<c:set var="edc_trget_01_co" value="${edc_trget_01_co + rs.edc_trget_01_co}" />
								<c:set var="edc_trget_02_co" value="${edc_trget_02_co + rs.edc_trget_02_co}" />
								<c:set var="edc_trget_03_co" value="${edc_trget_03_co + rs.edc_trget_03_co}" />
								<c:set var="edc_trget_04_co" value="${edc_trget_04_co + rs.edc_trget_04_co}" />
								<c:set var="edc_trget_05_co" value="${edc_trget_05_co + rs.edc_trget_05_co}" />
								<c:set var="edc_trget_06_co" value="${edc_trget_06_co + rs.edc_trget_06_co}" />
								<c:set var="edc_trget_01_co_year" value="${edc_trget_01_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_01_co)}" />
								<c:set var="edc_trget_03_co_year" value="${edc_trget_03_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_03_co)}" />
								<c:set var="edc_trget_05_co_year" value="${edc_trget_05_co_year + (rs.edc_co * rs.tot_co * rs.edc_trget_05_co)}" />



								<tr>
									<td colspan="12" class="prg">
										<div>
											<table>
												<colgroup>
													<col class="prg_1" />
													<col class="prg_2" />
													<col class="prg_3" />
													<col class="prg_4" />
													<col class="prg_5" />
													<col class="prg_6" />
													<col class="prg_7" />
														<%--<col class="prg_8"/>--%>
													<col class="prg_9" />
													<col class="prg_10" />
													<col class="prg_11" />
													<col class="prg_12" />
												</colgroup>
												<tbody>
												<tr>
													<td rowspan="4">
														<input type="hidden" name="biz_progrm_id" value="${rs.biz_progrm_id}"/>
														<textarea name="progrm_nm" cols="10" rows="10" class="inp_area2 null_false" title="프로그램명" maxlength="255">${rs.progrm_nm}</textarea>
													</td>
													<td rowspan="4">
														<input type="text" name="bgnde" value="${rs.bgnde}" readonly="readonly" class="cinp2 bgnde validation dates" title="운영프로그램 시작날짜" /> ~
														<input type="text" name="endde" value="${rs.endde}" readonly="readonly" class="cinp2 endde validation dates" title="운영프로그램 종료날짜" />
													</td>
													<td rowspan="4"><input type="text" name="tot_co" value="${rs.tot_co}" class="minp null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoYearTotal($(this))" />회</td>
													<td rowspan="4">
														<select name="ctnu_at" title="연속여부" class="select2">
															<option value="N" <c:if test="${rs.ctnu_at eq 'N'}">selected="selected"</c:if>>일회</option>
															<option value="Y" <c:if test="${rs.ctnu_at eq 'Y'}">selected="selected"</c:if>>연속</option>
														</select>
													</td>
													<td rowspan="4"><input type="text" name="edc_co" value="${rs.edc_co}" class="minp null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoYearTotal($(this))" title="교육일" />일</td>
													<td rowspan="4"><input type="text" name="round_edcde_co" value="${rs.round_edcde_co}" class="minp null_false validation number" onkeyup="fnInputNumCom($(this));" title="강사수"/></td>
													<th>소외계층 </th>
													<td class="bline"><input type="text" name="edc_trget_01_co" value="${rs.edc_trget_01_co}" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="소외계층 회당인원" /></td>
													<td class="bline"><input type="text" name="edc_trget_02_co" value="${rs.edc_trget_02_co}" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="소외계층 총인원" /></td>
													<td class="bline edc_trget_co_tot_txt_1">${rs.tot_co * rs.edc_co * rs.edc_trget_01_co}</td>
													<td rowspan="4"><textarea name="edc_place" cols="10" rows="10" class="inp_area2 null_false" title="교육장소" maxlength="100">${rs.edc_place}</textarea></td>
													<td rowspan="4" class="last"><span class="mbtn"><button type="button" onclick="fnBizOpePgmDelete($(this));">삭제</button></span></td>
												</tr>
												<tr>
													<th>일반 유아·아동·청소년 </th>
													<td class="bline"><input type="text" name="edc_trget_03_co" value="${rs.edc_trget_03_co}" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="일반청소년 회당인원" /></td>
													<td class="bline"><input type="text" name="edc_trget_04_co" value="${rs.edc_trget_04_co}" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="일반청소년 총인원" /></td>
													<td class="bline edc_trget_co_tot_txt_2">${rs.tot_co * rs.edc_co * rs.edc_trget_03_co}</td>
												</tr>
												<tr>
													<th>일반성인</th>
													<td class="bline"><input type="text" name="edc_trget_05_co" value="${rs.edc_trget_05_co}" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="일반성인 회당인원" /></td>
													<td class="bline"><input type="text" name="edc_trget_06_co" value="${rs.edc_trget_06_co}" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="일반성인 총인원" /></td>
													<td class="bline edc_trget_co_tot_txt_3">${rs.tot_co * rs.edc_co * rs.edc_trget_05_co}</td>
												</tr>
												<tr>
													<th>소계</th>
													<td class="one_pers_tot_txt">${rs.edc_trget_01_co + rs.edc_trget_03_co + rs.edc_trget_05_co}</td>
													<td class="tot_pers_tot_txt">${rs.edc_trget_02_co + rs.edc_trget_05_co + rs.edc_trget_06_co}</td>
													<td class="year_pers_tot_txt">${rs.tot_co * rs.edc_co * (rs.edc_trget_01_co + rs.edc_trget_03_co + rs.edc_trget_05_co)}</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${empty bopList}">
							<tr>
								<td colspan="12" class="prg">
									<div>
										<table>
											<colgroup>
												<col class="prg_1" />
												<col class="prg_2" />
												<col class="prg_3" />
												<col class="prg_4" />
												<col class="prg_5" />
												<col class="prg_6" />
												<col class="prg_7" />
													<%--<col class="prg_8"/>--%>
												<col class="prg_9" />
												<col class="prg_10" />
												<col class="prg_11" />
												<col class="prg_12" />
											</colgroup>
											<tbody>
											<tr>
												<td rowspan="4">
													<input type="hidden" name="biz_progrm_id" value=""/>
													<textarea name="progrm_nm" cols="10" rows="10" class="inp_area2 null_false" maxlength="255" title="프로그램명"></textarea>
												</td>
												<td rowspan="4">
													<input type="text" name="bgnde" value="" readonly="readonly" class="cinp2 bgnde validation dates" title="운영프로그램 시작날짜" /> ~
													<input type="text" name="endde" value="" readonly="readonly" class="cinp2 endde validation dates" title="운영프로그램 종료날짜" />
												</td>
												<td rowspan="4"><input type="text" name="tot_co" value="" class="minp null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoYearTotal($(this))" />회</td>
												<td rowspan="4">
													<select name="ctnu_at" title="연속여부" class="select2">
														<option value="N">일회</option>
														<option value="Y">연속</option>
													</select>
												</td>
												<td rowspan="4"><input type="text" name="edc_co" value="" class="minp null_false validation number" onkeyup="fnInputNumCom($(this)); onchange="fnAutoYearTotal($(this))" title="교육일" />일</td>
												<td rowspan="4"><input type="text" name="round_edcde_co" value="" class="minp null_false validation number" onkeyup="fnInputNumCom($(this));" title="강사수"/></td>
												<th>소외계층 </th>
												<td class="bline"><input type="text" name="edc_trget_01_co" value="" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="소외계층 회당인원" /></td>
												<td class="bline"><input type="text" name="edc_trget_02_co" value="" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="소외계층 총인원" /></td>
												<td class="bline edc_trget_co_tot_txt_1"></td>
												<td rowspan="4"><textarea name="edc_place" cols="10" rows="10" class="inp_area2 null_false" maxlength="100" title="교육장소"></textarea></td>
												<td rowspan="4" class="last"><span class="mbtn"><button type="button" onclick="fnBizOpePgmDelete($(this));">삭제</button></span></td>
											</tr>
											<tr>
												<th>일반 유아·아동·청소년 </th>
												<td class="bline"><input type="text" name="edc_trget_03_co" value="" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="일반청소년 회당인원" /></td>
												<td class="bline"><input type="text" name="edc_trget_04_co" value="" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="일반청소년 총인원" /></td>
												<td class="edc_trget_co_tot_txt_2 bline"></td>
											</tr>
											<tr>
												<th>일반성인</th>
												<td class="bline"><input type="text" name="edc_trget_05_co" value="" class="minp one_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.one_pers_tot_txt', 'input[name=edc_trget_01_co]', 'input[name=edc_trget_03_co]', 'input[name=edc_trget_05_co]')" title="일반성인 회당인원" /></td>
												<td class="bline"><input type="text" name="edc_trget_06_co" value="" class="minp tot_pers_val null_false validation number" onkeyup="fnInputNumCom($(this));" onchange="fnAutoStatistics($(this), '.tot_pers_tot_txt', 'input[name=edc_trget_02_co]', 'input[name=edc_trget_04_co]', 'input[name=edc_trget_06_co]')" title="일반성인 총인원" /></td>
												<td class="edc_trget_co_tot_txt_3 bline"></td>
											</tr>
											<tr>
												<th>소계</th>
												<td class="one_pers_tot_txt"></td>
												<td class="tot_pers_tot_txt"></td>
												<td class="year_pers_tot_txt"></td>
											</tr>
											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</c:if>
						</tbody>
						<tfoot>
						<tr>
							<th colspan="12" class="alC">합계</th>
						</tr>
						<c:if test="${edc_trget_01_co_year eq 0}">
							<c:set var="edc_trget_01_co_year" value="" />
						</c:if>
						<c:if test="${edc_trget_03_co_year eq 0}">
							<c:set var="edc_trget_03_co_year" value="" />
						</c:if>
						<c:if test="${edc_trget_05_co_year eq 0}">
							<c:set var="edc_trget_05_co_year" value="" />
						</c:if>
						<tr>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="alC">X</td>
							<td rowspan="4" class="edc_co_tot_txt">${edc_co}</td>
							<td rowspan="4" class="round_edcde_co_tot_txt">${round_edcde_co}</td>
							<th>소외계층 </th>
							<td class="edc_trget_01_co_tot_txt">${edc_trget_01_co}</td>
							<td class="edc_trget_02_co_tot_txt">${edc_trget_02_co}</td>
							<td class="edc_trget_co_tot_txt_1_txt">${edc_trget_01_co_year}</td>
							<td rowspan="4" colspan="2">X</td>
						</tr>
						<tr>
							<th>일반청소년 </th>
							<td class="edc_trget_03_co_tot_txt">${edc_trget_03_co}</td>
							<td class="edc_trget_04_co_tot_txt">${edc_trget_04_co}</td>
							<td class="edc_trget_co_tot_txt_2_txt">${edc_trget_03_co_year}</td>
						</tr>
						<tr>
							<th>일반성인</th>
							<td class="edc_trget_05_co_tot_txt">${edc_trget_05_co}</td>
							<td class="edc_trget_06_co_tot_txt">${edc_trget_06_co}</td>
							<td class="edc_trget_co_tot_txt_3_txt">${edc_trget_05_co_year}</td>
						</tr>
						<tr>
							<th>소계</th>
							<td class="edc_trget_135_tot_txt"><c:if test="${(edc_trget_01_co + edc_trget_03_co + edc_trget_05_co) > 0}">${edc_trget_01_co + edc_trget_03_co + edc_trget_05_co}</c:if></td>
							<td class="edc_trget_246_tot_txt"><c:if test="${(edc_trget_02_co + edc_trget_04_co + edc_trget_06_co) > 0}">${edc_trget_02_co + edc_trget_04_co + edc_trget_06_co}</c:if></td>
							<td class="edc_trget_135_year_tot_txt"><c:if test="${(edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year) > 0}">${edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year}</c:if></td>
						</tr>
						</tfoot>
					</table>
				</div>
				<ul class="icon3_list">
					<li>① 가급적 교육대상을 한정하여 수혜자특성에 맞는 프로그램 운영할 것</li>
					<li>② ‘교육장소’는 교육시설명 및 휴양림명 등을 구체적으로 기술</li>
				</ul>

			</td>
		</tr>
		<tr>
			<th>
				1인당 교육비(천원)<br/>(총사업비 / 참가 연인원)
			</th>
			<td>
				<span class="one_edu_money"><fmt:formatNumber value="${bsifVo.tot_wct / (edc_trget_01_co_year + edc_trget_03_co_year + edc_trget_05_co_year) * 1000}" pattern="#,###,###.#"/></span>
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <휴양문화사업> --%>
	<c:if test="${biz_ty_code eq 'BTC06'}">
		<tr>
			<th rowspan="2">사업내용</th>
			<td>
				<ul class="icon3_list cnt_list">
					<li>
						<span>사업기간</span>
						<div class="tx biz_bgnde_txt">
							<c:if test="${not empty bsifVo.biz_bgnde and not empty bsifVo.biz_endde }">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</c:if>
							&nbsp;
						</div>
					</li>
					<li>
						<span>대상자</span>
						<div class="tx">위에 작성한 사업제안서 대상자와 동일</div>
					</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<ul class="icon3_list cnt_list">
					<li>
						<span>사업장소</span>
						<div class="tx">&nbsp;</div>
					</li>
				</ul>
				<div class="addr_box">
						${fn:substring(bsifVo.biz_zip, 0, 3)}-${fn:substring(bsifVo.biz_zip, 3, 6)}<br/>
						${bsifVo.biz_adres}<br/>
						${bsifVo.biz_adres_detail}
					<input type="hidden" name="biz_zip_1" id="biz_zip_1" value="${fn:substring(bsifVo.biz_zip, 0, 3)}" class="inp zipcode" title="우편번호 앞자리" readonly="readonly"/>
					<input type="hidden" name="biz_zip_2" id="biz_zip_2" value="${fn:substring(bsifVo.biz_zip, 3, 6)}" class="inp zipcode" title="우편번호 뒷자리" readonly="readonly"/>
					<input type="hidden" name="biz_adres" id="biz_adres" value="${bsifVo.biz_adres}" class="inp null_false" title="주소입력" readonly="readonly"/>
					<input type="hidden" name="biz_adres_detail" id="biz_adres_detail" value="${bsifVo.biz_adres_detail}" class="inp null_false" title="상세주소입력"/>
				</div>
			</td>
		</tr>
		<tr>
			<th>사업규모</th>
			<td>
				<div class="alR">
					<span class="mbtn"><button type="button" onclick="fnbizSizeAdd();">추가</button></span>
				</div>
				<table class="chart1" summary="사업규모 입력폼입니다.">
					<caption>사업규모 입력폼</caption>
					<colgroup>
						<col width="auto"/>
						<col width="auto"/>
						<col width="auto"/>
						<col width="auto"/>
						<col width="auto"/>
						<col width="auto"/>
						<col width="auto"/>
						<col width="9%"/>
					</colgroup>
					<thead>
					<tr>
						<th>수혜 대상</th>
						<th>수혜 인원</th>
						<th>세부행사 규모</th>
						<th>횟수</th>
						<th>성과물 내용<br />제작수</th>
						<th>성과물 내용<br />배포수</th>
						<th>기타</th>
						<th>삭제</th>
					</tr>
					</thead>
					<tbody class="bizSize">
					<tr class="bizSize_0" style="display:none;">
						<td colspan="8">등록된 사업규모가 없습니다.</td>
					</tr>
					<c:if test="${not empty bbsList}">
						<c:forEach var="rs" items="${bbsList}" varStatus="sts">
							<tr>
								<td>
									<input type="hidden" name="biz_scale_id" value="${rs.biz_scale_id}" />
									<input type="text" name="rcvfvr_trget" value="${rs.rcvfvr_trget}" class="inp null_false " maxlength="255" title="수혜 대상" />
								</td>
								<td><input type="text" name="rcvfvr_nmpr_co" value="${rs.rcvfvr_nmpr_co}" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="수혜 인원" /></td>
								<td><input type="text" name="detail_event_scale" value="${rs.detail_event_scale}" class="inp null_false" maxlength="255"  title="세부행사 규모" /></td>
								<td><input type="text" name="co" value="${rs.co}" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="횟수" /></td>
								<td><input type="text" name="rslt_cn_mnfct_co" value="${rs.rslt_cn_mnfct_co}" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="성과물 내용 제작수" /></td>
								<td><input type="text" name="rslt_cn_wdtb_co" value="${rs.rslt_cn_wdtb_co}" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="성과물 내용 배포수" /></td>
								<td><input type="text" name="etc" value="${rs.etc}" class="inp" maxlength="255" title="기타" /></td>
								<td><span class="mbtn"><button type="button" onclick="fnbizSizeDelete($(this));">삭제</button></span></td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${empty bbsList}">
						<tr>
							<td>
								<input type="hidden" name="biz_scale_id" />
								<input type="text" name="rcvfvr_trget" class="inp null_false " maxlength="255" title="수혜 대상" />
							</td>
							<td><input type="text" name="rcvfvr_nmpr_co" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="수혜 인원" /></td>
							<td><input type="text" name="detail_event_scale" class="inp null_false" maxlength="255"  title="세부행사 규모" /></td>
							<td><input type="text" name="co" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="횟수" /></td>
							<td><input type="text" name="rslt_cn_mnfct_co" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="성과물 내용 제작수" /></td>
							<td><input type="text" name="rslt_cn_wdtb_co" onkeyup="fnInputNumCom($(this));" class="rinp null_false validation number" title="성과물 내용 배포수" /></td>
							<td><input type="text" name="etc" class="inp" maxlength="255" title="기타" /></td>
							<td><span class="mbtn"><button type="button" onclick="fnbizSizeDelete($(this));">삭제</button></span></td>
						</tr>
					</c:if>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<th>참가자 모집.관리</th>
			<td>
				※ 취약계층 참가자 모집 및 실적관리 방법을 구체적으로 기재
				<textarea name="adhrnc_rcrit_manage" id="adhrnc_rcrit_manage" cols="10" rows="10" class="inp_area null_false" title="참가자 모집.관리">${bsifVo.adhrnc_rcrit_manage}</textarea>
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <휴양문화사업, 체험교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC05' or biz_ty_code eq 'BTC06'}">
		<tr>
			<th>추진일정</th>
			<td colspan="4">
				<strong>※ 기간과 내용을 구체적으로 기재</strong>
				<ul class="icon3_list cal_box_list">
					<li>
						<span>사업준비 </span>
						<div class="cal_box">
								${bsifVo.prtnfx_bizprpare_bgnde} ~ ${bsifVo.prtnfx_bizprpare_endde}
							<input type="hidden" name="prtnfx_bizprpare_bgnde" id="prtnfx_bizprpare_bgnde" value="${bsifVo.prtnfx_bizprpare_bgnde}" readonly="readonly" class="cinp null_false" title="사업준비 시작날짜"/>
							<input type="hidden" name="prtnfx_bizprpare_endde" id="prtnfx_bizprpare_endde" value="${bsifVo.prtnfx_bizprpare_endde}" readonly="readonly" class="cinp" title="사업준비 종료날짜"/>
						</div>
					</li>
					<li>
						<span>모집공고 </span>
						<div class="cal_box">
								${bsifVo.prtnfx_rcritpblanc_bgnde} ~ ${bsifVo.prtnfx_rcritpblanc_endde}
							<input type="hidden" name="prtnfx_rcritpblanc_bgnde" id="prtnfx_rcritpblanc_bgnde" value="${bsifVo.prtnfx_rcritpblanc_bgnde}" readonly="readonly" class="cinp null_false" title="모집공고 시작날짜"/>
							<input type="hidden" name="prtnfx_rcritpblanc_endde" id="prtnfx_rcritpblanc_endde" value="${bsifVo.prtnfx_rcritpblanc_endde}" readonly="readonly" class="cinp null_false" title="모집공고 종료날짜"/>
						</div>
					</li>
					<li>
						<span>사업진행</span>
						<div class="cal_box">
								${bsifVo.prtnfx_bizprogrs_bgnde} ~ ${bsifVo.prtnfx_bizprogrs_endde}
							<input type="hidden" name="prtnfx_bizprogrs_bgnde" id="prtnfx_bizprogrs_bgnde" value="${bsifVo.prtnfx_bizprogrs_bgnde}" readonly="readonly" class="cinp null_false" title="사업진행 시작날짜"/>
							<input type="hidden" name="prtnfx_bizprogrs_endde" id="prtnfx_bizprogrs_endde" value="${bsifVo.prtnfx_bizprogrs_endde}" readonly="readonly" class="cinp null_false" title="사업진행 종료날짜"/>
						</div>
					</li>
					<li>
						<span>평가보고</span>
						<div class="cal_box">
								${bsifVo.prtnfx_evlreport_bgnde} ~ ${bsifVo.prtnfx_evlreport_endde}
							<input type="hidden" name="prtnfx_evlreport_bgnde" id="prtnfx_evlreport_bgnde" value="${bsifVo.prtnfx_evlreport_bgnde}" readonly="readonly" class="cinp null_false" title="평가보고 시작날짜"/>
							<input type="hidden" name="prtnfx_evlreport_endde" id="prtnfx_evlreport_endde" value="${bsifVo.prtnfx_evlreport_endde}" readonly="readonly" class="cinp null_false" title="평가보고 종료날짜"/>
						</div>
					</li>
				</ul>
			</td>
		</tr>
	</c:if>

	<%-- 숲체험ㆍ교육 <체험교육사업> --%>
	<c:if test="${biz_ty_code eq 'BTC06'}">
		<tr>
			<th>기대효과</th>
			<td>
				<c:out value="${fn:replace(bsifVo.expc_effect, newLineChar, '<br/>')}" escapeXml="false" />
				<textarea name="expc_effect" id="expc_effect" cols="10" rows="10" class="inp_area null_false hidden" title="기대효과">${bsifVo.expc_effect}</textarea>
			</td>
		</tr>
	</c:if>
	</tbody>
</table>

<%-- 복지 시설 나눔숲, 지역 사회 나눔숲, 다함께 나눔길  --%>
<c:if test="${biz_ty_code eq 'BTC01' or biz_ty_code eq 'BTC02' or biz_ty_code eq 'BTC03' or biz_ty_code eq 'BTC07' or biz_ty_code eq 'BTC09'}">
	<h3 class="icon1">제안요약서(사후관리)</h3>
	<table class="chart2" summary="${biz_sort_nm} 제안요약서(사후관리) 정보 입력">
		<caption>${biz_sort_nm}제안요약서(사후관리)정보 입력</caption>
		<colgroup>
			<col class="expost_1"/>
			<col class="expost_2"/>
		</colgroup>
		<tbody>
		<tr>
			<th>사후관리</th>
			<td>
				<div class="area_box">
					<textarea name="aftfat_manage" id="aftfat_manage" cols="10" rows="10" class="inp_area null_false" title="사후관리">${bsifVo.aftfat_manage}</textarea>

					<div class="ftxt" tabindex="0" <c:if test="${not empty bsifVo.aftfat_manage}"> style="display:none;"</c:if>>
						관리재원 :<br/> 관리방안 :
					</div>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</c:if>