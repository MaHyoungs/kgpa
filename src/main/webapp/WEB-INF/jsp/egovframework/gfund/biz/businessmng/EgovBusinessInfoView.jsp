<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${PropseBizId}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${PropseBizId}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript">
<c:if test="${lastBizPlanPresentnAt ne 'Y' }">
	alert('최종사업계획서가 제출되지 않았습니다.');
	location.href="/gfund/biz/bassinfo/basicInformationView.do?year=${param.year}&biz_id=${param.biz_id}&biz_ty_code=${param.biz_ty_code}";
</c:if>
<c:if test='${not empty message}'>
	alert("${message}");
</c:if>
</script>

<!--  main start -->
<div id="container">
			
	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">

			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li class="active"><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li class="active"><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">기본정보</a></li>
					<li><a href="/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
					<li><a href="/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
					<li><a href="/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
				</ul>
			</div>
			
			<h3 class="icon1">최종사업계획서</h3>

			<div class="select_bar">
				<strong>${bsifVo.area}</strong>
			</div>
			
			<c:set var="biz_ty_code" />
			<c:choose>
				<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
				<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
				<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
			</c:choose>
			<div class="input_area">
			
				<%-- 제안서 기본 Form --%>
				<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeBasicInformationView.jsp"%>
				<%-- 제안서 기본 Form --%>
			
				<div class="pbg mB20">
					<strong>필수제출서류</strong><br/><br/>
					※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
					※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
					&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
					※ 특수교육시설의 경우 개인정보이용동의서만 제출합니다.
				</div>
			
				<%-- 파일첨부 Import --%>
				<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
					<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
					<c:param name="pathKey" value="Gfund.fileStorePath" />
					<c:param name="appendPath" value="basicInformation" />
					<c:param name="maxSize" value="31457280" />
					<c:param name="maxCount" value="10" />
				</c:import>
				<%-- 파일첨부 Import --%>
			</div>
		
			<div class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		
		</div>
		
	</div>
	<!-- sub end -->
	
</div>
<!--  main end -->
