<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mobile/images"/>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<meta name="format-detection" content="telephone=no" />
<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/template/mobile/js/script.js"></script>
<script type="text/javascript" src="/template/common/js/common.js"></script>
<link rel="stylesheet" href="/template/mobile/css/style.css" type="text/css" charset="utf-8" />
<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>

<body>

<script type="text/javascript">
	// 이전 페이지
	function backPage(){
		history.back();
	}
	// 시도 불러오기
	function fnSidoOpenAPIAdd(el, code){
		var apikey = "ESGA2014121253129491";
		if(code == null){
			code = 1;
		}
		var url = '/template/common/api/areaAPI/areaAPI.jsp';
		var data = [];
		data = {apikey : apikey, code : code};
		var successFn = function(json){
			if(json.data != null){
				for(var i=0; i<json.data.length; i++){
					var opt = $('<option value="' + json.data[i].code + '">' + json.data[i].name + '</option>');
					el.append(opt);
				}
			}
		};
		var errorFn = function(){
			alert('현재 행정구역 OpenAPI 서비스가 점검중입니다.\n기관 지역(시.도, 시.군.구)선택할 수 없습니다. ');
		};
		fn_ajax_json(url, data, successFn, null);
	}

	// 사업명 검색
	function fnbizNmSearch(el, area) {
		var url = '/gfund/biz/mbusinmng/EgovbizNmSearchAjax.do';
		var data = [];
		data = {area : area, isNotPaging : 'isNotPaging'};
		var successFn = function(json) {
			if(json.rs != null){
				for(var i=0; i<json.rs.length; i++){
					var opt = $('<option value="' + json.rs[i].biz_id + '">' + json.rs[i].biz_nm + '</option>');
					el.append(opt);
				}
			}
		};
		var errorFn = function(){
			alert('잠시 후 다시 시작하여 주십시요.');
		};
		fn_ajax_json(url, data, successFn, null);
	}

	// 확인버튼
	function fnChckSearchBizNm(el, bizId) {
		var url = '/gfund/biz/mbusinmng/EgovbizzNmSearchAjax.do';
		var data = [];
		data = {bizId : bizId};
		var successFn = function(json) {
			//저장완료여부 확인

				if(json.rs != null) {
					el.text('');
					el.text($("#bizNm option:selected").text());

					var compAt = json.rs["compAt"];

					if(compAt == 'Y') {
						$('.nocompAt').text('X');
						$('.compAt').text('O');
					} else if(compAt == 'N' || compAt == '' || compAt == null) {
						$('.nocompAt').text('O');
						$('.compAt').text('X');
					}

				} else if(json.rs == null) {
					el.text('');
					el.text($("#bizNm option:selected").text());

					if($("#bizNm option:selected").text() != '사업명'){

						$('.nocompAt').text('O');
						$('.compAt').text('X');
					} else {
						alert("사업을 선택하여주십시요.");
					}
				}
		};
		var errorFn = function(){
			alert('잠시 후 다시 시작하여 주십시요.');
		};
		fn_ajax_json(url, data, successFn, null);
	}

	//페이지 OnLoad 자동이벤트
	$(function(){
		fnSidoOpenAPIAdd($('#sido'), 1);
		//시도, 시도군구 이벤트
		$('#sido').change(function() {
			$('#sidogungu option').remove();
			$('#bizNm option').remove();
			var opt = $('<option value="">시.군.구</option>');
			$('#sidogungu').append(opt);
			var opt = $('<option value="">사업명</option>');
			$('#bizNm').append(opt);
			fnSidoOpenAPIAdd($('#sidogungu'), $('#sido').val());
			var area = $("#sido option:selected").text();
			fnbizNmSearch($('#bizNm'), area);
		});
		//사업명 이벤트
		$('#sidogungu').change(function(){
			$('#bizNm option').remove();
			var opt = $('<option value="">사업명</option>');
			$('#bizNm').append(opt);
			var area = $("#sido option:selected").text() + " " + $("#sidogungu option:selected").text();
			fnbizNmSearch($('#bizNm'), area);
		});

		//확인버튼 이벤트
		$(".searchBizNm").click(function(){
			var bizId = $("#bizNm option:selected").val();

			if(bizId == null || bizId == '') {
				alert("사업을 선택하여주십시요");
			} else{
				fnChckSearchBizNm($('#bizName'), bizId);
			}

		});
		//지도점검시작 이벤트
		$(".chckStart").click(function(){

			var bizId = $("#bizNm option:selected").val();

			var compAt = $('.compAt').text();

			if( compAt ==  null || compAt == ''){
				alert("사업을 선택하여주십시요");
			}else if(compAt == 'O' || compAt == 'X') {
				if(bizId == '사업명' || bizId == null || bizId == '') {
					alert("사업을 선택하여주십시요");
				} else {
					location.href = "/gfund/biz/mbusinmng/EgovChckListGuide.do?bizId=" + bizId;
				}
			}
		});
	});

</script>
		<!-- header start -->
		<div id="header">
			<div class="header">
				<a href="javascript:backPage()" class="back"><img src="${_IMG}/mobile/btn_prev.png" alt="이전" /></a>
				<h1><a href="/gfund/biz/mbusinmng/EgovChckListBusinSelect.do"><img src="${_IMG}/mobile/logo.png" alt="녹색자금 통합관리시스템" /></a></h1>
				<a href="/gfund/biz/mbusinmng/EgovChckListBusinSelect.do" class="home"><img src="${_IMG}/mobile/btn_home.png" alt="홈으로 이동" /></a>
			</div>
			<div id="lnb">
				<strong>사업선택</strong>
			</div>
		</div>
		<!-- //header end-->

		<!-- content  start -->
		<div id="content">

			<div class="section">
				<form action="#">
					<div class="select_bar">
						<legend>사업선택 입력폼</legend>
						<select name="sido" id="sido" title="시도선택">
							<option>시. 도</option>
						</select>
						<select name="sidogungu" id="sidogungu" title="시. 군.구선택">
							<option>시. 군.구</option>
						</select>
						<select name="bizNm" id="bizNm" class="wide" title="사업명 선택">
							<option>사업명</option>
						</select>
					</div>
				</form>
					<span class="cbtn1"><button type="submit" class="searchBizNm">확인</button></span>
			</div>

			<div class="radius">
				<table class="chart1" summary="사업목록을 나타낸표로 사업명 , 대상여부, 실시여부 항목을 나타낸표입니다">
					<caption>사업목록</caption>
					<colgroup>
						<col class="list1_1" />
						<col class="list1_2" />
						<col class="list1_3" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col">사업명</th>
							<th scope="col">대상여부</th>
							<th scope="col">저장완료여부</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="bizName">사업명</td>
							<td class="alC"><span class="nocompAt o"></span></td>
							<td class="alC"><span class="compAt x"></span></td>
						</tr>
					</tbody>
				</table>
			</div>

			<span class="cbtn"><button class="chckStart"><span class="icon_view"></span> 지도점검 시작</button></span>

		</div>
		<!-- content  end-->


		<!-- footer start -->
		<div id="footer">
			<div class="skip_link">
				<c:url var="logout" value="/gfund/uat/uia/EgovmChckLogout.do">
				</c:url>
				<a href="<c:out value="${logout}"/>" class="btn_logout">로그아웃<span class="icon_logout"></span></a>
				<a href="#content" class="btn_top">TOP으로<span class="icon_top"></span></a>
			</div>
			<p>Copyright © 2014 KGPA. All Right Reserved.</p>
		</div>
		<!-- //footer end-->
	</body>
</html>