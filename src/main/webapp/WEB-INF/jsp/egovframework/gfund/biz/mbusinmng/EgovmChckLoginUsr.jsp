<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mobile/images"/>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<meta name="format-detection" content="telephone=no" />
<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/template/mobile/js/script.js"></script>
<script type="text/javascript" src="/template/common/js/common.js"></script>
<link rel="stylesheet" href="/template/mobile/css/style.css" type="text/css" charset="utf-8" />

<script type="text/javascript">
	function checkLogin(){
		var frm = document.frmGnrlLogin;
		if(frm.id.value == ""){
			alert("아이디를  입력해주세요");
			frm.id.focus();
			return false;
		}

		if(frm.password.value == ""){
			alert("비밀번호를  입력해주세요");
			frm.password.focus();
			return false;
		}

		var chk = document.getElementById("gfundIdSave");
		if(chk){
			if(chk){
				if(chk.checked){
					fnSetCookiePopup("loginId", frm.id.value, 30);
				}else{
					fnSetCookiePopup("loginId", null, -1);
				}
			}
		}
		frm.submit();
	}
</script>

<title>녹색자금 통합관리시스템</title>

	<body>
		<script type="text/javascript">
		<c:if test='${not empty frdbmessage}'>
			alert("${frdbmessage}");
		</c:if>
		</script>
		<!-- header start -->
		<div id="header">
			<div class="header">
				<h1><a href="/gfund/uat/uia/EgovChckListMoibleLogin.do"><img src="${_IMG}/mobile/logo.png" alt="녹색자금 통합관리시스템" /></a></h1>
			</div>
		</div>
		<!-- //header end-->

		<!-- content  start -->
		<div id="content">

			<div class="login">
				<h2>로그인</h2>

				<div class="login_input">
					<form name="frmGnrlLogin" action="/gfund/uat/uia/EgovChckListMoibleLogin.do" method="post" onsubmit="return checkLogin()">
						<ul>
							<li><input type="text" class="inp" id="id" name="id" placeholder="아이디" title="아이디입력" /></li>
							<li><input type="password" class="inp" id="password" name="password" placeholder="비밀번호" title="비밀번호입력" /></li>
						</ul>

						<span class="cbtn">
							<%--<button type="submit" class="button"><span class="icon_login" onclick="return (this.form);"></span>로그인</button>--%>
							<button type="submit" class="button"><span class="icon_login"></span>로그인</button>
						</span>
					</form>
					<div class="chk">
							<p class="remember_id"><input type="checkbox" id="gfundIdSave" value=""  /><label for="gfundIdSave">아이디저장</label></p>
							<script type="text/javascript">
								var userId = fnGetCookiePopup('loginId');
								var frm = document.frmGnrlLogin;
								if(userId){
									if(frm) {
										frm.id.value = userId;
										frm.password.focus();
										var chk = document.getElementById("gfundIdSave");
										if(chk) {
											chk.checked = true;
										}
									}
								}else{
									frm.id.focus();
								}
							</script>
						</div>
				</div>
			</div>

		</div>
		<!-- content  end-->

		<!-- footer start -->
		<div id="footer">
			<p>Copyright © 2014 KGPA. All Right Reserved.</p>
		</div>
		<!-- //footer end-->
	</body>
</html>