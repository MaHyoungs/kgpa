<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<!--  main start -->
<div id="container">
			
	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
					
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			
			<h3 class="icon1">사업명 " ${basicInformationVo.biz_nm} " 평가결과</h3>
			<div class="pbg mB20">
				<strong>
	<c:choose>
		<c:when test="${empty basicInformationVo.last_evl_result or null eq basicInformationVo.last_evl_result or fn:length(basicInformationVo.last_evl_result) < 1}">
					진행하신 사업은 평가 결과가 입력되지 않았습니다.
		</c:when>
		<c:otherwise>
					진행하신 사업의 평가 결과는 " 
			<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
				<c:param name="codeId" value="COM216" />
				<c:param name="chVal" value="${basicInformationVo.last_evl_result}" />
				<c:param name="elType" value="codeName" />
			</c:import>
					" 입니다.
		</c:otherwise>
	</c:choose>
				</strong>
			</div>
			
			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		
		</div>
		
	</div>
	<!-- sub end -->
	
</div>
<!--  main end -->
