<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:set var="formAction" value="/gfund/biz/productmng/EgovExcclcInsert.do${_BASE_PARAM}" />

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
			
			<link charset="utf-8" href="/str/cre/bbs/tmplat/BBSTMP_0000000000001/style.css" type="text/css" rel="stylesheet" />
		
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			
	<c:choose>
		<c:when test="${not empty excclcVo}"> <%-- 조회용 화면으로 제공, 저장 버튼 없음 --%>
			<h3 class="icon1">최종정산</h3>
			<table  class="chart1" summary="최종정산 결과산출 출력하는 표 입니다">
				<caption>최종정산</caption>
				<colgroup>
					<col width="10%" />
					<col width="10%" />
					<col width="11%">
					<col width="11%">
					<col width="11%">
					<col width="*">
				</colgroup>
				<thead>	
					<tr>
						<th colspan="2">구분</th>
						<th>사업비<br/>(A)</th>
						<th>집행액<br/>(B)</th>
						<th>반납액<br/>(A-B)</th>
						<th>반납사유</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2">사업비</td>
						<td><fmt:formatNumber value="${excclcVo.decsnAmount}" groupingUsed="true"/>원</td>
						<td><fmt:formatNumber value="${excclcVo.excutAmount}" groupingUsed="true"/>원</td>
						<td><fmt:formatNumber value="${excclcVo.decsnAmount - excclcVo.excutAmount}" groupingUsed="true"/>원</td>
						<td><c:out value="${fn:replace(excclcVo.rturnResn, newLineChar, '<br/>')}" escapeXml="false"/></td>
					</tr>
					<tr>
						<td rowspan="2">이자발생</td>
						<td>예금이자</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><fmt:formatNumber value="${excclcVo.dpstIntr}" groupingUsed="true"/>원</td>
						<td><c:out value="${fn:replace(excclcVo.dpstIntrResn, newLineChar, '<br/>')}" escapeXml="false"/></td>
					</tr>
					<tr>
						<td>해지이자</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><fmt:formatNumber value="${excclcVo.trmnatIntr}" groupingUsed="true"/>원</td>
						<td><c:out value="${fn:replace(excclcVo.trmnatIntrResn, newLineChar, '<br/>')}" escapeXml="false"/></td>
					</tr>
				</tbody>
			</table>
			
			<div class="btn_c">
				<span class="cbtn1"><button type="button" onclick="alert('이미 제출되었습니다.');">제출</button></span>
			</div>	
		</c:when>
		<c:otherwise> <%-- 입력용 화면으로 제공, 저장 버튼 존재 --%>
			<form name="excclcForm" id="excclcForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">
			<div class="input_area">
				<h3 class="icon1">최종정산</h3>
				<table  class="chart1" summary="최종정산 결과산출 입력하는 표 입니다">
					<caption>최종정산</caption>
					<colgroup>
						<col width="10%" />
						<col width="10%" />
						<col width="11%">
						<col width="11%">
						<col width="11%">
						<col width="*">
					</colgroup>
					<thead>	
						<tr>
							<th colspan="2">구분</th>
							<th>사업비<br/>(A)</th>
							<th>집행액<br/>(B)</th>
							<th>반납액<br/>(A-B)</th>
							<th>반납사유</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2">사업비</td>
							<td><input type="text" name="decsnAmount" id="decsnAmount" class="inp" title="사업비" value="${decsnAmount}" readonly="readonly" /></td>
							<td><input type="text" name="excutAmount" id="excutAmount" class="inp null_false validation number" title="집행액" value="" placeholder="(단위 : 원)" /></td>
							<td><input type="text" name="absAmount" id="absAmount" class="inp null_false validation number" title="반납액" value="${decsnAmount}" placeholder="(단위 : 원)" /></td>
							<td><input type="text" name="rturnResn" id="rturnResn" class="inp null_false" title="반납사유" value="" placeholder="반납사유 기재" /></td>
						</tr>
						<tr>
							<td rowspan="2">이자발생</td>
							<td>예금이자</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><input type="text" name="dpstIntr" id="dpstIntr" class="inp null_false validation number" title="예금이자반납액" value="" placeholder="(단위 : 원)" /></td>
							<td><input type="text" name="dpstIntrResn" id="dpstIntrResn" class="inp null_false" title="예금이자반납사유" value="" placeholder="반납사유 기재" /></td>
						</tr>
						<tr>
							<td>해지이자</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><input type="text" name="trmnatIntr" id="trmnatIntr" class="inp null_false validation number" title="해지이자반납액" value="" placeholder="(단위 : 원)" /></td>
							<td><input type="text" name="trmnatIntrResn" id="trmnatIntrResn" class="inp null_false" title="해지이자반납사유" value="" placeholder="반납사유 기재" /></td>
						</tr>
					</tbody>
				</table>				
			</div>
			</form>
			
			<div class="btn_c">
				<span class="cbtn1"><button type="button" onclick="$('#excclcForm').submit();">제출</button></span>
			</div>
		</c:otherwise>
	</c:choose>
	
		</div>
		
		<div  class="btn_top">
			<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
		</div>
		
	</div>
	<!-- sub end -->
	
	<c:if test="${empty excclcVo}"> <%-- 입력용 화면인 경우에만 자바스크립트 활성화 --%>
	<script type="text/javascript">
	// 교육횟수 전체 합계
		$('input[name=excutAmount]').change(function(){
			var absAmount = Number($('#decsnAmount').val()) - Number($('#excutAmount').val());
			$('#absAmount').val(absAmount);
		});
	</script>
	</c:if>
	
</div>
<!--  main end -->
