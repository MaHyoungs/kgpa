<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
	<c:if test="${empty lastPapersVo}">
		<c:set var="formType" value="등록" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${param.biz_id}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
		</c:url>
	</c:if>
	<c:if test="${not empty lastPapersVo}">
		<c:set var="formType" value="수정" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${lastPapersVo.bizId}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:param name="pageIndex" value="${param.pageIndex}" />
			<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
		</c:url>
	</c:if>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript">
	function resetForm() {
		
		var frm = document.lastPapersForm;
		frm.reset();
	
		return false;
	}
	
	function checkForm() {
		
		if ( $('#tr_file_empty').css('display') != 'none' ) {
			alert('첨부된 파일이 없습니다');	
			return false;
		}
		
		var frm = document.lastPapersForm;
		frm.submit();
		
		return false;
	}
</script>

<c:if test="${empty lastPapersVo}">
	<c:set var="formAction" value="/gfund/biz/productmng/EgovLastPapersInsert.do${_BASE_PARAM}" />
</c:if>
<c:if test="${not empty lastPapersVo}">
	<c:set var="formAction" value="/gfund/biz/productmng/EgovLastPapersSelectUpdt.do${_BASE_PARAM}" />
</c:if>

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">

			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			
			<div class="input_area">
				<h3 class="icon1">증빙서${formType}</h3>
				<form name="lastPapersForm" id="lastPapersForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">
					<input type="hidden" name="atchFileId" id="atch_file_id" class="inp null_false" value="${lastPapersVo.atchFileId}" title="파일첨부" />
					<input type="hidden" name="fileGroupId" id="fileGroupId" value="${lastPapersVo.atchFileId}"/>
					<input type="hidden" name="bizId" id="bizId" value="${lastPapersVo.bizId}" />
					<input type="hidden" name="lastPapersId" id="lastPapersId" value="${lastPapersVo.lastPapersId}" />
					<c:if test="${empty lastPapersVo}">
						<input type="hidden" name="frstRegisterId" id="frstRegisterId" value="${USER_INFO.id}" />
					</c:if>
					<c:if test="${not empty lastPapersVo}">
						<input type="hidden" name="lastUpdusrId" id="lastUpdusrId" value="${USER_INFO.id}" />
					</c:if>
					<table class="chart1" summary="최종서류제출 결과산출 정보를 ${formType}하는 표 입니다">
						<caption>최종서류제출</caption>
						<colgroup>
							<col width="15%" />
							<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<th>제목</th>
								<td><input type="text" name="nttSj" id="nttSj" class="inp null_false" title="제목" value="${lastPapersVo.nttSj}"/></td>
							</tr>
							<tr>
								<th>분류</th>
								<td>
									<select name="lastPapersCl" id="lastPapersCl" title="증빙서분류" class="inp null_false">
										<option value="">선택</option>
										<c:forEach var="cmmCode" items="${cmmCodeList}">							
											<option value="${cmmCode.code}" <c:if test="${cmmCode.code eq lastPapersVo.lastPapersCl}">selected="selected"</c:if> >${cmmCode.codeNm}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<textarea name="nttCn" id="nttCn" cols="10" rows="10" class="inp null_false" title="본문" style="height:300px;">${lastPapersVo.nttCn}</textarea>
								</td>
							</tr>
						</tbody>
					</table>			
				</form>
			</div>
			
			<%-- 파일첨부 Import --%>
			<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
				<c:param name="param_atchFileId" value="${lastPapersVo.atchFileId}" />
				<c:param name="updateFlag" value="Y" />
				<c:param name="pathKey" value="Gfund.fileStorePath" />
				<c:param name="appendPath" value="lastPapers" />
				<c:param name="maxSize" value="31457280" />
				<c:param name="maxCount" value="10" />
				<c:param name="formAjaxJs" value="add" />
			</c:import>
			<%-- 파일첨부 Import --%>
			
			<div class="btn_c">				
				<span class="cbtn1"><button type="submit" onclick="return checkForm();">${formType}</button></span>
				<span class="cbtn"><button type="button" onclick="return resetForm();">취소</button></span>
				<c:url var="listUrl" value="/gfund/biz/productmng/EgovLastPapersList.do">
					<c:param name="year" value="${param.year}" />
					<c:choose>
						<c:when test="${empty lastPapersVo}">
							<c:param name="biz_id" value="${param.biz_id}" />
						</c:when>
						<c:otherwise>
							<c:param name="biz_id" value="${lastPapersVo.bizId}" />
						</c:otherwise>
					</c:choose>
					<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
					<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>
				<span class="cbtn"><a href="${listUrl}">이전화면</a></span>
			</div>
			
		</div>
		
	</div>
	
</div>

<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>
<!--  main end -->
