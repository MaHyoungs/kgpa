<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript">
</script>

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
		
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			
			<table  class="chart1" summary="최종서류제출 결과산출 정보를 출력하는 표 입니다">
				<caption>최종서류제출</caption>
				<colgroup>
					<col width="15%" />
					<col width="*">
				</colgroup>
				<tbody>
					<tr>
						<th>제목</th>
						<td><c:out value="${lastPapersVo.nttSj}" escapeXml="false" /></td>
					</tr>
					<tr>
						<th>분류</th>
						<td>
							<c:forEach var="cmmCode" items="${cmmCodeList}">							
								<c:if test="${cmmCode.code eq lastPapersVo.lastPapersCl}"><c:out value="${cmmCode.codeNm}" /></c:if>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<c:out value="${fn:replace(lastPapersVo.nttCn, newLineChar, '<br/>')}" escapeXml="false"/>
						</td>
					</tr>
					<tr>
						<th>첨부파일</th>
						<td>
							<ul class="file_list">
								<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
									<c:param name="param_atchFileId" value="${lastPapersVo.atchFileId}" />
									<c:param name="style" value="gfund" />
								</c:import>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			
			<div class="btn_c">
				<c:url var="listUrl" value="/gfund/biz/productmng/EgovLastPapersList.do">
					<c:param name="year" value="${param.year}" />
					<c:param name="biz_id" value="${lastPapersVo.bizId}" />
					<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
					<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>
				<span class="cbtn"><a href="${listUrl}">이전화면</a></span>
			</div>
		
		</div>
		
	</div>
	
</div>
<!--  main end -->
