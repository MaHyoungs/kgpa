<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${PropseBizId}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${PropseBizId}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />		
	</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript">
<c:if test="${lastBizPlanPresentnAt ne 'Y' }">
	alert('최종사업계획서가 제출되지 않았습니다.');
	location.href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}";
</c:if>
<c:if test='${not empty message}'>
	alert("${message}");
</c:if>	
</script>

<style type="text/css">
	/* 페이징 */
	#paging{text-align:center;margin:23px 0 27px}
	#paging li {display:inline;text-align:center;font-family:Tahoma;padding:2px 5px; font-weight:bold;}
	#paging li a { color:#a4a4a4;}
	#paging li a.commthis{ color:#333;}
</style>

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
		
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			
			<p class="total">총  문서수 ${paginationInfo.totalRecordCount}개 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
			<table class="chart1" summary="최종서류제출 결과산출 목록을 출력하는 표 입니다." >
			<caption class="hdn">최종서류제출</caption>
			<colgroup>
				<col width="50"/>
				<col width="200"/>
				<col width="*"/>
				<col width="100"/>
				<col width="100"/>
				<col width="100"/>
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>분류</th>
					<th>제목</th>
					<th>작성자</th>
					<th>작성일</th>
					<th>관리</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${resultCnt > 0}">
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr>
					<td>
						<fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/>										
					</td>
					<td>
						<c:forEach var="cmmCode" items="${cmmCodeList}">							
							<c:if test="${cmmCode.code eq result.lastPapersCl}"><c:out value="${cmmCode.codeNm}" /></c:if>
						</c:forEach>
					</td>
					<td><c:out value="${result.nttSj}" escapeXml="false" /></td>
					<td><c:out value="${result.frstRegisterId}" escapeXml="false" /></td>
					<td><fmt:formatDate pattern="yyyy-MM-dd" value="${result.frstRegistPnttm}"/></td>
					<td>
						<c:url var="viewUrl" value="/gfund/biz/productmng/EgovLastPapersSelectView.do">						  	
						  	<c:param name="year" value="${param.year}" />
							<c:param name="biz_id" value="${param.biz_id}" />
						  	<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
						  	<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
						  	<c:param name="lastPapersId" value="${result.lastPapersId}" />
						  	<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					    </c:url>
					    <a href="${viewUrl}"><img src="${_IMG}/btn/btn_select.gif"/></a>
						<c:url var="editUrl" value="/gfund/biz/productmng/EgovLastPapersUpdtView.do">							
							<c:param name="year" value="${param.year}" />
							<c:param name="biz_id" value="${param.biz_id}" />
							<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
							<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
							<c:param name="lastPapersId" value="${result.lastPapersId}" />
							<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
						</c:url>
			        	<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
			        	<c:url var="delUrl" value="/gfund/biz/productmng/EgovLastPapersDelete.do">
			        		<c:param name="year" value="${param.year}" />
							<c:param name="biz_id" value="${param.biz_id}" />
			        		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			        		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
			        		<c:param name="lastPapersId" value="${result.lastPapersId}" />
			        		<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
						</c:url>	
			        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
				    </td>
				</tr>
			</c:forEach>
			</c:if>
			<c:if test="${empty resultList or resultCnt < 1}">
				<tr>
				    <td colspan="6">
				    	자료가 없습니다.
					</td>
				</tr>
			</c:if>
			</tbody>
			</table>

			<div class="btn_r">
				<a href='<c:url value="/gfund/biz/productmng/EgovLastPapersAddView.do${_BASE_PARAM}"/>' >
					<span class="cbtn1"><button type="button">등록</button></span>
				</a>
			</div>

			<div id="paging">
				<c:url var="pageUrl" value="/gfund/biz/productmng/EgovLastPapersList.do">
					<c:param name="year" value="${param.year}" />
					<c:param name="biz_id" value="${param.biz_id}" />
					<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
				</c:url>
				
				<c:if test="${not empty paginationInfo}">
					<ul>
						<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
					</ul>
				</c:if>
			</div>

		</div>

	</div>

</div>
<!--  main end -->
