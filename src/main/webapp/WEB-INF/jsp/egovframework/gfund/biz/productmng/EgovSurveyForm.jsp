<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
				
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			

			<h3 class="icon1"><c:out value="${comtnschdulinfoVO.schdulNm}" /></h3>
			
			<table class="chart1" summary="녹색사업단  설문조사 내용을 나타낸표로 분류 제목, 분상태, 진행일정, 작성일, 조회수 항목을 제공하고 있습니다">
				<caption>최종서류제출</caption>
				<colgroup>
					<col width="15%" />
					<col width="*">
				</colgroup>
				<tbody>
					<tr>
						<th>상태</th>
						<td style="text-align:left">
							<c:choose>
								<c:when test="${comtnschdulinfoVO.state and comtnschdulinfoVO.useAt eq 'Y'}">
									진행중
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${comtnschdulinfoVO.state and comtnschdulinfoVO.useAt eq 'N'}">
											대기중
										</c:when>
										<c:when test="${comtnschdulinfoVO.useAt eq 'C'}">
											마감
										</c:when>
										<c:when test="${comtnschdulinfoVO.useAt eq 'R'}">
											마감
										</c:when>
										<c:otherwise>
											마감
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th>작성자</th>
						<td style="text-align:left">관리자</td>
					</tr>
					<tr>
						<th>작성일</th>
						<td style="text-align:left"><fmt:formatDate value="${comtnschdulinfoVO.frstRegisterPnttm}" pattern="yyyy-MM-dd" /></td>
					</tr>
					<tr>
						<th>시작일자</th>
						<td style="text-align:left">
							<c:out value="${fn:substring(comtnschdulinfoVO.schdulBgnde, 0,4)}-${fn:substring(comtnschdulinfoVO.schdulBgnde, 4,6)}-${fn:substring(comtnschdulinfoVO.schdulBgnde, 6,8)} ${fn:substring(comtnschdulinfoVO.schdulBgnde, 8,10)}:${fn:substring(comtnschdulinfoVO.schdulBgnde, 10,12)}" />
						</td>
					</tr>
					<tr>
						<th>종료일자</th>
						<td style="text-align:left">
							<c:out value="${fn:substring(comtnschdulinfoVO.schdulEndde, 0,4)}-${fn:substring(comtnschdulinfoVO.schdulEndde, 4,6)}-${fn:substring(comtnschdulinfoVO.schdulEndde, 6,8)} ${fn:substring(comtnschdulinfoVO.schdulEndde, 8,10)}:${fn:substring(comtnschdulinfoVO.schdulEndde, 10,12)}" />
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:left">
							<c:out value="${fn:replace(comtnschdulinfoVO.schdulCn, newLineChar, '<br/>')}" escapeXml="false"/>
						</td>
					</tr>					
					<tr>
						<th>첨부파일</th>
						<td>
							<ul class="file_list">
								<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
									<c:param name="param_atchFileId" value="${comtnschdulinfoVO.atchFileId}" />
									<c:param name="style" value="gfund" />
								</c:import>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			
			<div class="btn_c">
			<c:if test="${comtnschdulinfoVO.state and comtnschdulinfoVO.useAt eq 'Y' and comtnschdulinfoVO.schdulClCode ne '1'}">
				<c:url var="addUrl" value="/gfund/biz/productmng/participateSurveyEvent.do">
					<c:param name="schdulId" value="${comtnschdulinfoVO.schdulId}" />
					<c:param name="schdulClCode" value="${comtnschdulinfoVO.schdulClCode}" />
				</c:url>
				<span class="cbtn1">
					<a href="<c:out value='${addUrl}'/>" onclick="window.open(this.href,'evtPartcptn','height=600,width=550,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=yes,resizable=no');return false;" target="_blank" title="참여하기">참여하기</a>
				</span>				
			</c:if>
				
				<c:url var="listUrl" value="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">
					<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>
				<span class="cbtn"><a href="${listUrl}">이전화면</a></span>				
			</div>
					
		</div>
		
	</div>
	<!-- sub end -->
	
</div>
<!--  main end -->
