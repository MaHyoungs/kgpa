<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
	</c:url>
	<c:url var="_PROPOSE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.propose_biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript">
</script>

<style type="text/css">
	/* 페이징 */
	#paging{text-align:center;margin:23px 0 27px}
	#paging li {display:inline;text-align:center;font-family:Tahoma;padding:2px 5px; font-weight:bold;}
	#paging li a { color:#a4a4a4;}
	#paging li a.commthis{ color:#333;}
</style>

<!--  main start -->
<div id="container">

	<!-- sub start -->
	<div  class="sub_container">

		<div class="sub_top sub_top01">
			<div class="navi">
				<span class="location">
					<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png" />녹색자금사업 공모  &gt; 
					<strong>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM210" />
							<c:param name="chVal" value="${param.biz_ty_code}" />
							<c:param name="elType" value="codeName" />
						</c:import>
					</strong>
				</span>
			</div>
			<h2>
				<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
					<c:param name="codeId" value="COM210" />
					<c:param name="chVal" value="${param.biz_ty_code}" />
					<c:param name="elType" value="codeName" />
				</c:import>
			</h2>
		</div>
		
		<div id="content">
				
			<div class="tab">
				<ul>
					<li><a href="/gfund/biz/bassinfo/basicInformationView.do${_PROPOSE_PARAM}">선정전형</a></li>
					<li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do${_BASE_PARAM}">사업관리</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">결과산출</a></li>
				</ul>
			</div>
			
			<div class="tab2">
				<ul>
					<li><a href="/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
					<li><a href="/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
					<li class="active"><a href="/gfund/biz/productmng/EgovSurveyList.do${_BASE_PARAM}">설문조사</a></li>
					<li><a href="/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
				</ul>
			</div>
			

			<h3 class="icon1">당해 모든 사업에 대한 1건의 설문조사가 진행됩니다.</h3>
			
			<table class="chart1" summary="녹색사업단  설문조사 목록을 나타낸표로 번호, 구분, 제목, 상태, 진행일정, 작성일 항목을 제공하고 있습니다">
				<caption>설문조사 목록</caption>
				<colgroup>
					<col width="5%" />
					<col width="12%" />
					<col width="auto" />
					<col width="10%" />
					<col width="20%" />
					<col width="15%" />
				</colgroup>
				<thead>
				<tr>
					<th scope="col">번호</th>
					<th scope="col">구분</th>
					<th scope="col">제목</th>
					<th scope="col">상태</th>
					<th scope="col">진행일정</th>
					<th scope="col">작성일</th>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<td>
							<c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" />
						</td>
						<td>설문조사</td>
						<td class="tit">
							<c:url var="viewUrl" value="/gfund/biz/productmng/selectSurveyInfo.do${_BASE_PARAM}">
								<c:param name="mode" value="2" />
								<c:param name="pageIndex" value="${searchVO.pageIndex}" />
								<c:param name="schdulId" value="${result.schdulId}" />
							</c:url>
							<a href="<c:out value="${viewUrl}"/>"><c:out value="${result.schdulNm}" /></a>
						</td>
						<td>
							<c:choose>
								<c:when test="${result.state eq '1' and result.useAt eq 'Y'}">
									<span class="progress"><em>진행중</em></span>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${result.state eq '1' and result.useAt eq 'N'}">
											<span class="ready"><em>진행중</em></span>
										</c:when>
										<c:otherwise>
											<span class="finish"><em>종료</em></span>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<strong><c:out value="${fn:substring(result.schdulBgnde, 0,4)}-${fn:substring(result.schdulBgnde, 4,6)}-${fn:substring(result.schdulBgnde, 6,8)}" /> ~ <c:out value="${fn:substring(result.schdulEndde, 0,4)}-${fn:substring(result.schdulEndde, 4,6)}-${fn:substring(result.schdulEndde, 6,8)}" /></strong>
						</td>
						<td>
							<fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(resultList) == 0}">
					<td colspan="7">
						<spring:message code="common.nodata.msg" />
					</td>
				</c:if>
				</tbody>
			</table>
			
			<div id="paging">
				<c:url var="pageUrl" value="/gfund/biz/productmng/EgovSurveyList.do">
					<c:param name="year" value="${param.year}" />
					<c:param name="biz_id" value="${param.biz_id}" />
					<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					<c:param name="propose_biz_id" value="${param.propose_biz_id}" />
					<c:param name="mode" value="2" />
				</c:url>
				
				<c:if test="${not empty paginationInfo}">
					<ul>
						<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
					</ul>
				</c:if>
			</div>
			
			<div  class="btn_top">
				<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기" /></button>
			</div>
		
		</div>
		
	</div>
	<!-- sub end -->
	
</div>
<!--  main end -->
