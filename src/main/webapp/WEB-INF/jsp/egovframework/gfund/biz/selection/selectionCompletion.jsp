<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<c:set var="biz_sort_nm" value="" />
<c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}"><c:set var="biz_sort_nm" value="${btc.codeNm}" /></c:if></c:forEach>

<div class="sub_top sub_top01">
  <div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/>녹색자금사업 공모  &gt; <strong>${biz_sort_nm}</strong>
		</span>
  </div>
  <h2><c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code}">${btc.codeNm}</c:if></c:forEach></h2>
</div>

<div id="content">

  <!-- ${biz_sort_nm} 선정전형 -->
  <div class="tab">
    <ul>
      <li class="active"><a href="">선정전형</a></li>
      <c:choose>
        <c:when test="${bsifVo.last_biz_plan_presentn_at eq 'Y' and bsifVo.prst_ty_cc eq 'PRST02'}">
          <li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
          <li><a href="/gfund/biz/productmng/EgovLastPapersList.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
        </c:when>
        <c:otherwise>
          <li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">사업관리</a></li>
          <li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">결과산출</a></li>
        </c:otherwise>
      </c:choose>
    </ul>
  </div>

  <div class="tab_step">

    <ul>
      <c:set var="biz_id" value="" />
      <c:if test="${empty bsifVo.last_biz_plan_presentn_at and bsifVo.prst_ty_cc eq 'PRST01'}"><c:set var="biz_id" value="${bsifVo.biz_id}" /></c:if>
      <c:if test="${not empty bsifVo.last_biz_plan_presentn_at and bsifVo.prst_ty_cc eq 'PRST02'}"><c:set var="biz_id" value="${bsifVo.propse_biz_id}" /></c:if>
      <li><a href="/gfund/biz/bassinfo/basicInformationView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${biz_id}">제안서 접수</a></li>
      <li><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP1">1차심사</a></li>
      <li><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP2">현장심사</a></li>
      <li><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP3">2차심사</a></li>
      <li><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP4">최종발표</a></li>
      <li class="active"><a href="" onclick="return false;">최종제출</a></li>
    </ul>
  </div>
  <c:if test="${empty bsifVo.last_biz_plan_presentn_at and bsifVo.prst_ty_cc eq 'PRST01'}">
    <h3 class="icon1">의견</h3>
    <div class="pbg mB40">
      등록된 의견이 없습니다.
    </div>
  </c:if>
  <c:if test="${bsifVo.last_biz_plan_presentn_at eq 'Y'}">
    <div class="result_submit">
      <strong>최종제출이 완료</strong>되었습니다.
    </div>
  </c:if>

</div>