<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<c:set var="biz_sort_nm" value="" />
<c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}"><c:set var="biz_sort_nm" value="${btc.codeNm}" /></c:if></c:forEach>

<c:set var="step1" value="false" />
<c:set var="step2" value="false" />
<c:set var="step3" value="false" />
<c:set var="step4" value="false" />
<c:forEach var="rs" items="${bsList}" varStatus="sts">
  <c:if test="${rs.step_cl eq 'STEP1' and rs.step_cl_use eq 1}"><c:set var="step1" value="true" /></c:if>
  <c:if test="${rs.step_cl eq 'STEP2' and rs.step_cl_use eq 1}"><c:set var="step2" value="true" /></c:if>
  <c:if test="${rs.step_cl eq 'STEP3' and rs.step_cl_use eq 1}"><c:set var="step3" value="true" /></c:if>
  <c:if test="${rs.step_cl eq 'STEP4' and rs.step_cl_use eq 1}"><c:set var="step4" value="true" /></c:if>
</c:forEach>

<c:set var="step_cl" value="" />
<c:if test="${not empty param.step_cl}"><c:set var="step_cl" value="${param.step_cl}" /></c:if>
<c:if test="${not empty bsVo.step_cl}"><c:set var="step_cl" value="${bsVo.step_cl}" /></c:if>
<div class="sub_top sub_top01">
  <div class="navi">
		<span class="location">
			<img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"/>녹색자금사업 공모  &gt; <strong>${biz_sort_nm}</strong>
		</span>
  </div>
  <h2><c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code}">${btc.codeNm}</c:if></c:forEach></h2>
</div>

<div id="content">

  <!-- ${biz_sort_nm} 선정전형 -->
  <div class="tab">
    <ul>
      <li class="active"><a href="">선정전형</a></li>
      <c:choose>
        <c:when test="${empty lastVo and bsifVo.last_biz_plan_presentn_at eq 'Y' and bsifVo.prst_ty_cc eq 'PRST02'}">
          <li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
          <li><a href="/gfund/biz/productmng/EgovLastPapersList.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">사업관리</a></li>
        </c:when>
        <c:when test="${not empty lastVo and lastVo.last_biz_plan_presentn_at eq 'Y' and lastVo.prst_ty_cc eq 'PRST02'}">
          <li><a href="/gfund/biz/businessmng/EgovBusinessInfoView.do?year=${bsifVo.year}&amp;biz_ty_code=${lastVo.biz_ty_code}&amp;biz_id=${lastVo.biz_id}">사업관리</a></li>
          <li><a href="/gfund/biz/productmng/EgovLastPapersList.do?year=${bsifVo.year}&amp;biz_ty_code=${lastVo.biz_ty_code}&amp;biz_id=${lastVo.biz_id}">사업관리</a></li>
        </c:when>
        <c:otherwise>
          <li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">사업관리</a></li>
          <li><a href="" onclick="alert('아직 진행 할수 없습니다.'); return false;">결과산출</a></li>
        </c:otherwise>
      </c:choose>
    </ul>
  </div>

  <div class="tab_step">
    <ul>
      <li><a href="/gfund/biz/bassinfo/basicInformationView.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">제안서 접수</a></li>
      <li <c:if test="${step_cl eq 'STEP1'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP1">1차심사</a></li>
      <li <c:if test="${step_cl eq 'STEP2'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP2">현장심사</a></li>
      <li <c:if test="${step_cl eq 'STEP3'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP3">2차심사</a></li>
      <li <c:if test="${step_cl eq 'STEP4'}">class="active"</c:if>><a href="/gfund/biz/bassinfo/selection/selectionStepView.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;step_cl=STEP4">최종발표</a></li>
      <c:choose>
        <c:when test="${step_cl eq 'STEP4' and empty lastVo}">
          <li><a href="/gfund/biz/bassinfo/selection/selectionCompletion.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}">최종제출</a></li>
        </c:when>
        <c:when test="${step_cl eq 'STEP4' and not empty lastVo and lastVo.last_biz_plan_presentn_at eq 'Y'}">
          <li><a href="/gfund/biz/bassinfo/selection/selectionCompletion.do?biz_id=${lastVo.biz_id}&amp;biz_ty_code=${lastVo.biz_ty_code}">최종제출</a></li>
        </c:when>
        <c:when test="${step_cl eq 'STEP4' and not empty lastVo and lastVo.last_biz_plan_presentn_at eq 'N'}">
          <li><a href="/gfund/biz/bassinfo/basicInformationLastForm.do?biz_id=${lastVo.biz_id}&amp;biz_ty_code=${lastVo.biz_ty_code}">최종제출</a></li>
        </c:when>
        <c:otherwise>
          <li><a href="/gfund/biz/bassinfo/selection/selectionCompletion.do?biz_id=${bsifVo.biz_id}&amp;biz_ty_code=${bsifVo.biz_ty_code}">최종제출</a></li>
        </c:otherwise>
      </c:choose>
    </ul>
  </div>

  <c:set var="biz_ty_code" />
  <c:choose>
    <c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
    <c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
    <c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
  </c:choose>


  <c:choose>
    <c:when test="${bsifVo.propse_flag eq 'STEP06' and step_cl eq 'STEP2'}">
      <div class="result_npass">
        <strong>현장심사 탈락</strong>
        아쉽게도 귀 기관은 녹색자금 나눔숲 조성사업에 선정되지 않았습니다.<br />
        보여주신 열의에 감사드립니다.
      </div>
    </c:when>
    <c:when test="${bsifVo.propse_flag eq 'STEP07' and step_cl eq 'STEP2'}">
      <div class="result_pass">
        <strong>현장심사 합격</strong>
        귀 기관은 녹색자금 나눔숲 조성사업 현장심사에 통과하였습니다.
      </div>
    </c:when>
    <c:when test="${step_cl eq 'STEP3' and bsifVo.prst_ty_cc eq 'PRST01' and (empty lastVo or lastVo.last_biz_plan_presentn_at eq 'N')}">
      <h3 class="icon1">발표자료 업로드</h3>
      <div class="file_box">
          <%-- 파일첨부 Import --%>
        <c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
          <c:param name="param_atchFileId" value="${bsifVo.presnatn_atch_file_id}" />
          <c:param name="updateFlag" value="Y" />
          <c:param name="pathKey" value="Gfund.fileStorePath" />
          <c:param name="appendPath" value="basicInformation" />
          <c:param name="maxSize" value="31457280" />
          <c:param name="maxCount" value="1" />
          <c:param name="formAjaxJs" value="add" />
        </c:import>

        <script type="text/javascript">
          (function(){
            //파일 업로드 Form Ajax
            var progress=$('.progress');
            var bar = $('.bar');
            $('#boardFileAjaxForm').ajaxForm({
              dataType: 'json',
              beforeSend: function(){
                progress.show();
                var percentVal='0%';
                progress.width(percentVal);
                bar.html(percentVal);
              },
              uploadProgress: function(event, position, total, percentComplete){
                var percentVal=percentComplete-1 + '%';
                progress.width(percentVal);
                bar.html(percentVal);
              },
              success: function(json){
                if(json.rs == 'countOver'){
                  $('.progress').hide();
                  $('.progress').width("0%");
                  $('.bar').html("0%");
                  alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
                }else if(json.rs == 'overflow'){
                  $('.progress').hide();
                  $('.progress').width("0%");
                  $('.bar').html("0%");
                  alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
                }else if (json.rs == 'denyExt') {
	                $('.progress').hide();
	                $('.progress').width("0%");
	                $('.bar').html("0%");
	                alert("첨부할 수 없는 확장자입니다.");
                }else{
                  var percentVal='100%';
                  progress.width("99%");
                  bar.html(percentVal);
                  fnAjaxFileUploadComplete(json);
                }
              },
              complete: function(xhr){
                var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
                var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
                $('#boardFileAjaxForm input[name=uploadfile]').remove();
                file_parent.prepend(file);
              }
            });
          })();
        </script>
          <%-- 파일첨부 Import --%>

        <form action="/gfund/biz/bassinfo/updatePresentationsFile.do" method="post" onsubmit="return fn_text_null_check($('#atch_file_id'));">
          <input type="hidden" name="biz_id" value="${bsifVo.biz_id}" />
          <input type="hidden" name="presnatn_atch_file_id" id="atch_file_id" class="null_false" value="${bsifVo.presnatn_atch_file_id}" title="발표자료"/>
          <input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${bsifVo.presnatn_atch_file_id}"/>

          <div class="btn_c">
            <span class="cbtn1"><button type="submit">발표자료 제출</button></span>
          </div>

        </form>
      </div>
    </c:when>
    <c:when test="${bsifVo.propse_flag eq 'STEP09' and step_cl eq 'STEP3'}">
      <div class="result_npass">
        <strong>2차심사 탈락</strong>
        아쉽게도 귀 기관은 녹색자금 나눔숲 조성사업에 선정되지 않았습니다.<br />
        보여주신 열의에 감사드립니다.
      </div>
    </c:when>
    <c:when test="${bsifVo.propse_flag eq 'STEP10' and step_cl eq 'STEP3'}">
      <div class="result_pass" style="margin-bottom:50px;">
        <strong>2차심사 합격</strong>
        귀 기관은 녹색자금 나눔숲 조성사업 현장심사에 통과하였습니다.<br />
        최종발표를 위한 발표자료를 업로드 해주시기 바랍니다.
      </div>
    </c:when>
    <c:when test="${bsifVo.propse_flag eq 'STEP11' and step_cl eq 'STEP4'}">
      <div class="result_npass2">
        <strong>불합격자</strong>
        아쉽게도 귀 기관은 녹색자금 나눔숲 조성사업에 선정되지 않았습니다.<br />
        보여주신 열의에 감사드립니다.
      </div>
    </c:when>
    <c:when test="${bsifVo.propse_flag eq 'STEP12' and step_cl eq 'STEP4'}">
      <div class="result_pass2">
        <strong><em>합격</em>축하드립니다.</strong>
        귀 기관은 녹색자금 나눔숲 조성사업에 선정되었습니다.<br />
        1차심사, 현장심사, 2차심사 전문가의 의견을 반영하여 초기 제안서를 <br />
        수정하여 최종 제출하여 주시기 바랍니다.

        <div class="btn_c">
          <c:if test="${empty lastVo or lastVo.last_biz_plan_presentn_at eq 'N'}">
            <span class="cbtn4"><a href="/gfund/biz/bassinfo/basicInformationLastForm.do?year=${bsifVo.year}&amp;biz_ty_code=${bsifVo.biz_ty_code}&amp;biz_id=${bsifVo.biz_id}">최종제안서 제출</a></span>
          </c:if>
          <c:if test="${not empty lastVo and lastVo.last_biz_plan_presentn_at eq 'Y'}">
            <span class="cbtn4">이미 최종제출을 완료 했습니다.</span>
          </c:if>
        </div>
      </div>
    </c:when>
    <c:otherwise>

    </c:otherwise>
  </c:choose>

  <c:if test="${step_cl eq 'STEP3'}">
    <c:if test="${param.saveRs eq 3}">
      <script type="text/javascript">
        alert('발표자료가 제출 되었습니다.');
      </script>
    </c:if>
    <h3 class="icon1">발표자료</h3>
    <div class="section1">
      <%-- 파일첨부 Import --%>
      <c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
        <c:param name="style" value="gfSelection" />
        <c:param name="param_atchFileId" value="${bsifVo.presnatn_atch_file_id}" />
        <c:param name="pathKey" value="Gfund.fileStorePath" />
        <c:param name="appendPath" value="basicInformation" />
      </c:import>
      <%-- 파일첨부 Import --%>
    </div>
  </c:if>
  <c:if test="${step_cl ne 'STEP4'}">
    <h3 class="icon1">의견</h3>
    <div class="pbg mB40">
      <c:if test="${empty bsVo.expert_opinion}">등록된 의견이 없습니다.</c:if>
      <c:if test="${not empty bsVo.expert_opinion}">
        <c:out value="${fn:replace(bsVo.expert_opinion, newLineChar, '<br/>')}" escapeXml="false" />
      </c:if>
    </div>

    <h3 class="icon1">첨부파일</h3>
    <div class="section1">
      <%-- 파일첨부 Import --%>
      <c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
        <c:param name="style" value="gfSelection" />
        <c:param name="param_atchFileId" value="${bsVo.atch_file_id}" />
        <c:param name="pathKey" value="Gfund.fileStorePath" />
        <c:param name="appendPath" value="bizSelection" />
      </c:import>
      <%-- 파일첨부 Import --%>
    </div>
  </c:if>
  <c:if test="${step_cl eq 'STEP4' and empty bsVo}">
    <h3 class="icon1">의견</h3>
    <div class="pbg mB40">
      등록된 의견이 없습니다.
    </div>
  </c:if>

</div>

<%--<c:if test="${empty bsVo}">--%>
  <%--<script type="text/javascript">--%>
    <%--<c:if test="${param.step_cl eq 'STEP1' and step1 eq 'false'}">--%>
    <%--alert('1차 심사 내역이 없습니다.\n사업관리 페이지로 이동 됩니다.');--%>
    <%--history.back(0);--%>
    <%--</c:if>--%>
    <%--<c:if test="${param.step_cl eq 'STEP2' and step2 eq 'false'}">--%>
    <%--alert('현장 심사 내역이 없습니다.\n이전 심사 페이지로 이동 됩니다.');--%>
    <%--history.back(0);--%>
    <%--</c:if>--%>
    <%--<c:if test="${param.step_cl eq 'STEP3' and step3 eq 'false'}">--%>
    <%--alert('2차 심사 내역이 없습니다.\n이전 심사 페이지로 이동 됩니다.');--%>
    <%--history.back(0);--%>
    <%--</c:if>--%>
    <%--<c:if test="${param.step_cl eq 'STEP4' and step4 eq 'false'}">--%>
    <%--alert('최종발표 내역이 없습니다.\n이전 심사 페이지로 이동 됩니다.');--%>
    <%--history.back(0);--%>
    <%--</c:if>--%>
  <%--</script>--%>
<%--</c:if>--%>