<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location"> <img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"> <strong>사후관리 > 과거 사후관리 자료실</strong></span>
	</div>
	<h2>과거 사후관리 자료실</h2>
</div>

<!--  main start -->
<div  class="sub_container">
	<div id="content">
		<ul class="sub2_2m">
			<li class="img1"><a href="http://gfund.kgpa.or.kr/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000057&certi=Y"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/m2_img1.png" alt="복지시설나눔숲"/></a></li>
			<li class="img2"><a href="http://gfund.kgpa.or.kr/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000058&certi=Y"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/m2_img2.png" alt="지역사회나눔숲"/></a></li>
			<li class="img3"><a href="http://gfund.kgpa.or.kr/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000059&certi=Y"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/m2_img3.png" alt="숲체험ㆍ교육"/></a></li>
			<li class="img4"><a href="http://gfund.kgpa.or.kr/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000060&certi=Y"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/m2_img4.png" alt="다함께(무장애)나눔길"/></a></li>
		</ul>
	</div>
</div>
<!--  main end -->
