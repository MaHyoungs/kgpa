<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location"> <img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png"> <strong>이메일무단수집거부</strong></span>
	</div>
	<h2>이메일무단수집거부</h2>
</div>

<div id="content">
	<div class="pbg2">
		<p class="ft20">녹색사업단 사이트에서는 <strong class="red">전자우편주소 수집을 금지</strong>하고 있습니다.</p>
		<br/>
		<p>본 사이트에 게시된 이메일 주소가 전자우편 수집 프로그램이나 그 밖의 기술적 장치를 이용하여 무단으로 수집되는 것을 거부하며, 이를 위반시 정보통신망법에 의해 형사처벌됨을 유념하시기 바랍니다.</p>
		<div class="btn_r">등록일 2008년 6월 10일</div>
		<span class="bg">&nbsp;</span>
	</div>
</div>

