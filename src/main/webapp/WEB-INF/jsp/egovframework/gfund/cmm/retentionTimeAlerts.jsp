<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${rs eq 'true'}">
  <script type="text/javascript">
    $(function(){
      if (getCookie('retentionTimeAlertsPopup') != "done") {
        fn_open_popup('/gfund/retentionTimeAlertsPopup.do', 'retentionTimeAlertsPopup', '600', '300', '0', '0');
      }
    });
  </script>
</c:if>
