<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link charset="utf-8" href="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/style.css" type="text/css" rel="stylesheet" />
  <title>정보보존 일자 만료</title>

  <script type="text/javascript">
    //팝업 닫기, 쿠키로 5분동안 안열리게 한다.
    function fnPopupClose() {
      var todayDate = new Date();
      todayDate.setTime(todayDate.getTime()+todayDate.setTime(50*1000));
      var name = "retentionTimeAlertsPopup";
      var value = "done";
      document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
      self.close();
    }
  </script>
</head>

<body>
<div id="popup">
  <h1>정보보존 일자 만료</h1>
  <div class="popup_cont">
    <div class="alim_txt pbg">
      <ul class="icon3_list">
        <li>ID가 삭제된 경우 가입 후 활동하신 내용은 마이페이지에서 확인하실 수 없습니다.</li>
        <li>단, 해당 내역은 그대로 남아 녹색사업단에 귀속됩니다.</li>
        <li>정보보존 만료일자 연장은 회원정보에서 진행하실 수 있습니다.</li>
      </ul>
    </div>

    <div class="btn_c">
      <span class="cbtn1"><a href="" onclick="opener.location.href='/uss/umt/cmm/EgovUserUpdateView.do'; fnPopupClose(); return false;">회원으로 이동</a></span>
      <span class="cbtn"><a href="" onclick="fnPopupClose(); return false;">닫기</a></span>
      <%--
	  <span class="cbtn1"><button type="button" onclick="opener.location.href='/uss/umt/cmm/EgovUserUpdateView.do'; fnPopupClose();">회원으로 이동</button></span>
	  <span class="cbtn"><button type="button" onclick="self.close();">닫기</button></span>
      --%>
    </div>
  </div>

</div>
</body>
</html>