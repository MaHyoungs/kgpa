<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<%pageContext.setAttribute("crlf", "\r\n"); %>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${param.imagePath}"/>

<c:set var="_PREFIX" value="/mma"/>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">	 
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />	
</c:if>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="mltmdMvpId" value="${param.mltmdMvpId}" />
	<c:param name="mltmdId" value="${param.mltmdMvpId}" />
	<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
	<c:if test="${not empty param.searchCate}"><c:param name="searchCate" value="${param.searchCate}" /></c:if>
	<c:if test="${not empty param.searchCondition}"><c:param name="searchCondition" value="${param.searchCondition}" /></c:if>
	<c:if test="${not empty param.searchKeyword}"><c:param name="searchKeyword" value="${param.searchKeyword}" /></c:if>
	<c:if test="${not empty param.tmplatImportAt}"><c:param name="tmplatImportAt" value="${param.tmplatImportAt}"/></c:if>
	<c:param name="trgetId" value="${param.trgetId}" />
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<% /*URL 정의*/ %>

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="comment" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">
function fn_egov_insert_commentList(frm) {
	if (!validateComment(frm)){
		return false;
	}
	
	if (!confirm('<spring:message code="common.regist.msg" />')) {
		return false;
	}				
}

function fn_egov_deleteCommentList(url) {

	if (confirm('<spring:message code="common.delete.msg" />')) {
		document.location.href = url;
	}
}

function addComment(commentNo, num, ordrCode, ordrCodeDp) {
	jQuery(".commentPopup").hide();
	jQuery(".commentPopup").html("");
	jQuery("#replyComment"+num).html(
			"<div class='comment'>"+
				"<div class='comment_inp'>"+
				"<form name='commentVO' action='${_PREFIX}/insertReplyComment.do' method='post' onsubmit='return fn_egov_insert_commentList(this);'>"+
					"<input type='hidden' name='tmplatImportAt' value='${parma.tmplatImportAt}'/>" +
					<c:if test="${not empty param.pageIndex}">"<input type='hidden' name='pageIndex' value='${param.pageIndex}'/>" +</c:if>
					"<input name='trgetId' type='hidden' value='${param.trgetId}' />" +
					<c:if test="${not empty param.searchCondition}">"<input name='searchCondition' type='hidden' value='${param.searchCondition}'/>" + </c:if>
					<c:if test="${not empty param.searchKeyword}">"<input name='searchKeyword' type='hidden' value='${param.searchKeyword}'/>" + </c:if>
					"<input name='searchCate' type='hidden' value='${param.searchCate}'/>" +
					<c:if test="${not empty searchVO.subPageIndex}">"<input name='subPageIndex' type='hidden' value='${searchVO.subPageIndex}' />" + </c:if>
					"<input name='modified' type='hidden' value='false'/>" +
					"<input name='confirmPassword' type='hidden'/>" +
					"<input type='hidden' name='mltmdMvpId' value='${param.mltmdMvpId}'/>"+
					"<input type='hidden' name='mltmdId' value='${param.mltmdMvpId}'/>"+
					"<input type='hidden' name='commentNo' value='" + commentNo + "'/>"+
					"<input type='hidden' name='ordrCode' value='" + ordrCode + "'/>"+
					"<input type='hidden' name='ordrCodeDp' value='" + ordrCodeDp + "'/>"+
					"<textarea name='commentCn' rows='0' cols='0' placeholder='댓글을 입력하세요.'></textarea>"+
					"<input type='image' src='${_IMG}/sub/btn_reply_ok.gif' alt='덧글달기' align='absmiddle'/>"+
				"</form>"+
				"</div>"+
				"<p><spring:message code='cop.comment.msg' /></p>"+
			"</div>"
			
	);
	jQuery("#replyComment"+num).show();
	return false;
}
</script>
	<div class="bbs_reply"> 	
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<div class="reply type<c:out value="${result.ordrCodeDp+1 }"/>">
				<strong><c:out value="${result.wrterNm}" /></strong>
				<span class="date"><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/>
					<c:if test="${not empty USER_INFO.id}">
						<a href="#reply_txt" title="답변쓰기 열림" onclick="return addComment(${result.commentNo}, ${status.count}, '${result.ordrCode }', ${result.ordrCodeDp });">
							<img src="${_IMG}/sub/btn_reply_re.gif" alt="답변쓰기" />
						</a>
					</c:if>
					<c:if test="${not empty USER_INFO.id and result.frstRegisterId eq USER_INFO.id}">
						<c:url var="delUrl" value="${_PREFIX}/deleteComment.do${_BASE_PARAM}">
							<c:param name="commentNo" value="${result.commentNo}" />
							<c:param name="subPageIndex" value="${searchVO.subPageIndex}" />
							<c:param name="modified" value="true" />
						</c:url>
						<a href="<c:out value="${delUrl}"/>" title="덧글삭제하기" onclick="fn_egov_deleteCommentList(this.href);return false;">
							<img src="${_IMG}/sub/btn_delete.gif" alt="삭제" />
						</a>
					</c:if>
				</span>
				<p class="reply_cont">
					<c:set var="cn" value="${fn:escapeXml(result.commentCn)}"/>
					<c:set var="cn" value="${fn:replace(cn , crlf , '<br/>')}"/>
					<c:out value="${cn}" escapeXml="false" />
				</p>
			</div>
			<div id="replyComment${status.count }" style="display:none" class="commentPopup"></div>
		</c:forEach>
	</div>
	<c:if test="${fn:length(resultList) ne 0}">
	<div id="paging">				
	    <c:url var="pageUrl" value="${_PREFIX}/selectMltmdMvpInfo.do${_BASE_PARAM}">
	    </c:url>
	    <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
	    <ui:pagination paginationInfo="${paginationInfo}" type="egovImgSubPaging" jsFunction="${pagingParam}" />
	</div>
	</c:if>

	<div class="comment">
		<c:choose>		
		<c:when test="${not empty USER_INFO.id}">
			<div class="comment_inp">
				<form name="comment" id="comment" method="post" action="<c:url value='${_PREFIX}/insertComment.do'/>" onsubmit="return fn_egov_insert_commentList(this);">
					<input type="hidden" name="mltmdMvpId" value="<c:out value='${param.mltmdMvpId}'/>" />
					<input type="hidden" name="mltmdId" value="<c:out value='${param.mltmdMvpId}'/>" />

					<c:if test="${not empty param.pageIndex}"><input type="hidden" name="pageIndex" value="<c:out value='${param.pageIndex}'/>"/></c:if>
					<c:if test="${not empty param.searchCondition}"><input name="searchCondition" type="hidden" value="<c:out value="${param.searchCondition}"/>"/></c:if>
					<c:if test="${not empty param.searchKeyword}"><input name="searchKeyword" type="hidden" value="<c:out value="${param.searchKeyword}"/>"/></c:if>
					<input name="searchCate" type="hidden" value="<c:out value="${param.searchCate}"/>"/>
					<input name="tmplatImportAt" type="hidden" value="${param.tmplatImportAt}"/>
					
					<input type="hidden" name="menuId" value="<c:out value='${param.menuId}'/>"/>
					<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
						<c:if test="${not empty searchCate}">
							<input name="searchCateList" type="hidden" value="<c:out value="${searchCate}"/>"/>
						</c:if>
					</c:forEach>
					
					<input name="commentNo" type="hidden" value="<c:out value='${searchVO.commentNo}'/>"/>
					<input name="modified" type="hidden" value="false"/>
					<input name="confirmPassword" type="hidden"/>
		
					<strong>댓글쓰기</strong>
					<textarea name="commentCn" rows="0" cols="0" placeholder="댓글을 입력하세요."></textarea>
					<input type="image" src="${_IMG}/sub/btn_reply_ok.gif" alt="입력"/>
					<p><spring:message code='cop.comment.msg' /></p>
				</form>
		  	</div>
		</c:when>
		<c:otherwise>
			<div class="comment_inp">
				<strong>댓글쓰기</strong>
				<textarea name="commentCn" rows="0" cols="0" readonly="readonly">덧글을 작성하시려면 로그인이 필요합니다.</textarea>
				<img src="${_IMG}/btn_reply_ok.gif" alt="입력" />
				<p><spring:message code='cop.comment.msg' /></p>
		  	</div>
		</c:otherwise>
		</c:choose>
	</div>
  
  <c:if test="${not empty subMsg}">
  <script type="text/javascript">
    alert("<c:out value='${subMsg}'/>");
  </script>
  </c:if>