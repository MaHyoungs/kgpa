<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mma/images"/>
<c:set var="_CSS" value="${pageContext.request.contextPath}/template/mma/css"/>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
	  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:set var="agent" value="${header['user-agent'] }"/>
<c:set var="phoneType" value="0"/>

<c:choose>
<c:when test="${fn:indexOf(agent,'iPhone') ne '-1' or fn:indexOf(agent,'iPad') ne '-1' or fn:indexOf(agent,'iPod') ne '-1'  or fn:indexOf(agent,'iPod') ne '-1'}">
	<c:set var="tailParam" value="/playlist.m3u8"/>
</c:when>
<c:otherwise>
	<c:set var="mediaMovieServiceWebUrl" value="${fn:replace(mediaMovieServiceWebUrl,'http','rtsp' )}" />
	<c:set var="tailParam" value=""/>	
</c:otherwise>
</c:choose>	


<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<title>VOD</title>
	<link rel="stylesheet" type="text/css" href="${_CSS}/default_mbl.css">
	<script type="text/javascript" src="${_C_JS}/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		var input = <c:choose><c:when test="${not empty mltmdMvpCnvrInfo.playTime}">${mltmdMvpCnvrInfo.playTime}</c:when><c:otherwise>0</c:otherwise></c:choose>;
		var day;
		var hr;
		var min;
		var sec;
		cday=86400; //하루
		chr=3600; //한시간
		cmin=60;  //1분
		day = parseInt(input/cday);  //1일로 나눈 몫
		hr = parseInt(input%cday/chr);  //1일로 나눈 나머지를 시간으로 나눔
		min = parseInt(input%cday%chr/cmin);  //일과 시간으로 나눈 나머지를 분으로 나눔
		sec = input%cday%chr%cmin; //그 나머지
		var str = ((hr < 10) ? '0' + hr : hr) + ":" + ((min < 10) ? '0' + min : min) + ":" + ((sec < 10) ? '0' + sec : sec);
		$('#playTime').text('재생시간 : ' + str);
	});
	function play() {
		var video = document.getElementById("video");
		if(video.paused) {		
			//
			video.play();
			//movie.webkitEnterFullscreen();
			
		}
	}
</script>
</head>
<body>

	<div id="wrap">
		<!-- 
		<div id="header">
			<a href="index.html" class="prev"><img src="images/btn_prv.png" alt="이전" /></a>
			<a href="index.html" class="home"><img src="images/btn_home.png" alt="홈" /></a>
			<h1>VOD</h1>
			<a href="" class="reflash"><img src="images/btn_reflash.png" alt="새로고침" /></a>
		</div>
		 -->
		 
		<div id="location">
			전체영상 &gt; <c:out value="${currCtgry.ctgryPathByName}"/>
		</div>

		<div id="content">
			
			<div class="video">
				<span class="vd">
					<video id="video" controls="controls">
				    	<source src="<c:out value="${mediaMovieServiceWebUrl}${mltmdMvpCnvrInfo.fileStreCours}/${mltmdMvpCnvrInfo.physclNm}${tailParam }"/>"/>  	
				    </video>
				    <script type="text/javascript">					
				    	document.getElementById("video").addEventListener('click',function(){
							this.play();
						},false);					
					</script>					
					<!-- 
					<c:forEach var="result" items="${moviePhotoList}" varStatus="status">
						<c:if test="${status.count eq 1}">	
							<img src="${mediaMovieImageWebUrl}${result.fileStreCours}/${result.physclNm}" alt="동영상 이미지" />
						</c:if>
					</c:forEach>
					-->
				</span>
				<c:if test="${phoneType eq '3' or phoneType eq '4' or phoneType eq '5'}">
					<span class="vd"><a href="#" onclick="play();return false;"><img src="${_IMG}/btn_play.png" alt="재생" /></a></span>
				</c:if>
				<c:if test="${phoneType eq '2' or phoneType eq '1' }">
					<span class="vd"><a href="#" onclick="play();return false;"><img src="${_IMG}/btn_play.png" alt="재생" /></a></span>
				</c:if>
			</div>
			
			<div class="video_info">
				<ul>
					<li>제목 : <strong><c:out value="${mltmdMvpInfoVO.mvpSj}"/></strong></li>
					<li>날짜 : <fmt:formatDate value="${mltmdMvpInfoVO.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></li>
					<li id="playTime">재생시간 : </li>
				</ul>

				<div>
					<c:out value="${mltmdMvpInfoVO.mvpCn}" escapeXml="false" />
				</div>
			</div>

		
			
		</div>

		<!-- 
		<div class="skip_link">
			<ul>
				<li><a href=""><img src="images/btn_bottom01_on.png" alt="알림방" /></a></li>
				<li><a href=""><img src="images/btn_bottom02_off.png" alt="VOD" /></a></li>
				<li><a href=""><img src="images/btn_bottom03_off.png" alt="나의정보" /></a></li>
				<li><a href=""><img src="images/btn_bottom04_off.png" alt="학교찾기" /></a></li>
				<li><a href=""><img src="images/btn_bottom05_off.png" alt="기타" /></a></li>
			</ul>
		</div>
		 -->

	</div>
	
	
</body>
</html>