<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mma/images"/>
<c:set var="_CSS" value="${pageContext.request.contextPath}/template/mma/css"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
	  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<title>VOD</title>
	<link rel="stylesheet" type="text/css" href="${_CSS}/default_mbl.css">
</head>
<body>

	<div id="wrap">
		<!-- 
		<div id="header">
			<a href="" class="prev"><img src="images/btn_prv.png" alt="이전" /></a>
			<a href="" class="home"><img src="images/btn_home.png" alt="홈" /></a>
			<h1>VOD</h1>
			<a href="" class="reflash"><img src="images/btn_reflash.png" alt="새로고침" /></a>
		</div>
		 -->

		<div id="menu">
			<ul>
				<c:forEach var="mpmDepth01" items="${mltmdCateList}" varStatus="status">
					<c:if test="${mpmDepth01.ctgryLevel eq 1}">	
						<li><a href="<c:url value="/mma/MblMltmdMvpInfoList.do?searchCate=${mpmDepth01.ctgryId}"/>" <c:if test="${mpmDepth01.ctgryId eq searchVO.searchCate}">class="active"</c:if>><c:out value="${mpmDepth01.ctgryNm}"/></a></li>
					</c:if>
				</c:forEach>
			</ul>
		</div>

		<div id="content">
			
			<div class="cont">
				<table summary="목록정보(번호, 제목, 조회수)를 나타낸표 " class="chart">
					<caption>목록</caption>
					<colgroup>
						<col width="30%" />
						<col width="*" />
						<col width="20%" />
					</colgroup>
					<thead>
						<tr>
							<th>번호</th>
							<th>제목</th>
							<th>조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="mvp" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="/mma/selectMblMltmdMvpInfo.do${_BASE_PARAM}">
							  	<c:param name="mltmdMvpId" value="${mvp.mltmdMvpId}" />
							  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
						    </c:url>
							<tr>
								<td><a href="<c:out value="${viewUrl}"/>" class="thumb"><c:choose><c:when test="${not empty mvp.thumbFilePath}"><img src="${mediaMovieImageWebUrl}${mvp.thumbFilePath}" alt="<c:out value="${mvp.mvpSj}"/>" onerror="this.src='${_IMG}/common/no_img.png'" width="75" height="55"/></c:when><c:otherwise><img src="${_IMG}/common/no_img.png" alt="<c:out value="${result.mvpSj}"/>" width="75" height="55"/></c:otherwise></c:choose></a></td>
								<td class="tit"><a href="<c:out value="${viewUrl}"/>" class="tit"><c:out value="${mvp.mvpSj}"/></a></td>
								<td class="hit" ><fmt:formatNumber value="${mltmdMvpInfoVO.inqireCo}" type="number"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<div id="paging">
					<c:url var="pageUrl" value="/mma/MblMltmdMvpInfoList.do${_BASE_PARAM}">
					</c:url>
					<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
					<ui:pagination paginationInfo="${paginationInfo}" type="egovPaging" jsFunction="${pagingParam}" />
				</div>


			</div>
				
		</div>

		<!-- 
		<div class="skip_link">
			<ul>
				<li><a href=""><img src="images/btn_bottom01_off.png" alt="알림방" /></a></li>
				<li><a href=""><img src="images/btn_bottom02_off.png" alt="VOD" /></a></li>
				<li><a href=""><img src="images/btn_bottom03_off.png" alt="나의정보" /></a></li>
				<li><a href=""><img src="images/btn_bottom04_off.png" alt="학교찾기" /></a></li>
				<li><a href=""><img src="images/btn_bottom05_off.png" alt="기타" /></a></li>
			</ul>
		</div>
		 -->

	</div>
	
	
</body>
</html>