<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mma/images"/>
<c:set var="_C_LIB" value="${pageContext.request.contextPath}/lib"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mma/tmplatHead.do" charEncoding="utf-8">
	<c:param name="searchTarget" value="MVP"/>
	<c:param name="ctgrymasterId" value="${searchVO.ctgrymasterId}"/>
</c:import>

<script type="text/javascript">
	$(document).ready(function() {
		$('#btnRecomend').click(function() {
			var url = "<c:url value="/mma/insertMltmdMvpRecomendForAjax.do?mltmdMvpId=${mltmdMvpInfoVO.mltmdMvpId}"/>";
			$.getJSON(url, function(data) {
				$('#lblRecomend').text('추천수 : ' + fn_comma(data.recomendCo));
				if(data.applyAt == 'Y') {
					alert('추천해 주셔서 감사합니다.');
				} else {
					alert('이미 추천해 주셨습니다.');
				}
			});
			
			return false;
		});
	});
</script>
				<div class="view_tit">
					<h3><c:out value="${mltmdMvpInfoVO.mvpSj}"/></h3>
					<c:url var="recomendUrl" value="/mma/insertMltmdMvpRecomend.do${_BASE_PARAM}">
					  	<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}" />
				    </c:url>
					<a href="<c:out value="${recomendUrl}"/>" id="btnRecomend" class="btn_hit"><span>추천하기</span></a>
				</div>

				<div class="view_intro">
					<span class="name">작성자: <c:out value="${mltmdMvpInfoVO.wrterNm}"/></span>
					<span class="date">작성일 : <fmt:formatDate value="${mltmdMvpInfoVO.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></span>
					<span class="visit">조회수 : <fmt:formatNumber value="${mltmdMvpInfoVO.inqireCo}" type="number"/></span>
					<span id="lblRecomend" class="hit">추천수 : <fmt:formatNumber value="${mltmdMvpInfoVO.recomendCo}" type="number"/></span>
				</div>

				<div class="view_content">
					<div style="width:650px;height:450px">			
						<object id="mvpPlayer" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
					  		<param name="source" value="${_C_LIB}/mma/MovieClient.xap"/>
						  	<param name="onError" value="onSilverlightError" />
						  	<param name="background" value="white" />
						  	<param name="windowless" value="true" />
						  	<param name="minRuntimeVersion" value="3.0.40818.0" />
						  	<param name="autoUpgrade" value="true" />
						  	<param name="initParams" value="InfoXmlPath=/mma/mvpMediaInfo.do?siteId=${searchVO.siteId}&mltmdFileId=${mltmdMvpInfoVO.mltmdFileId}&mltmdFileDetailId={0}&mltmdMvpId=${searchVO.mltmdMvpId}&searchCate=${searchVO.searchCate}&contents=<c:out value="${mltmdMvpInfoVO.mvpSj}"/>" />				  	
						  	<a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40818.0" style="text-decoration:none;">
								<img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Microsoft Silverlight 가져오기" style="border-style:none;"/>
						  	</a>
					    </object>
					</div>
					    
					<p><c:out value="${mltmdMvpInfoVO.mvpCn}" escapeXml="false" /></p>
				</div>

				<!-- comment start -->
				<div class="key_word">
					<strong>키워드</strong><span> : <c:out value="${mltmdMvpInfoVO.searchKwrd}"/></span>
				</div>
				
				<div class="btn_r">
					<c:if test="${mltmdMvpInfoVO.frstRegisterId eq USER_INFO.id or USER_INFO.userSe >= 10}">
						<c:url var="forUpdateUrl" value="/mma/forUpdateMltmdMvpInfo.do${_BASE_PARAM}">
					      		<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}" />
							</c:url>
						<a href="<c:out value="${forUpdateUrl}"/>" class="bbtn3"><span>수정하기</span></a>
					</c:if>
					<c:url var="listUrl" value="/mma/MltmdMvpInfoList.do${_BASE_PARAM}">
					</c:url>
					<a href="<c:out value="${listUrl}"/>" class="bbtn4"><span>목록으로</span></a>
				</div>
				
				<c:url var="_COMMENT_PARAM" value="">
					<c:param name="imagePath" value="${_IMG}" />
	    			<c:if test="${empty param.mltmdId}"><c:param name="mltmdId" value="${param.mltmdMvpId}" /></c:if>
				</c:url>
				
				<c:import url="/mma/selectCommentList.do${_COMMENT_PARAM}" charEncoding="utf-8"/>
				
				

<c:import url="/mma/tmplatBottom.do" charEncoding="utf-8"/>