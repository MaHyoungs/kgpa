<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mma/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
	  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mma/tmplatHead.do" charEncoding="utf-8">
	<c:param name="searchTarget" value="MVP"/>
	<c:param name="ctgrymasterId" value="${searchVO.ctgrymasterId}"/>
</c:import>

<script type="text/javascript">
	function fn_egov_add(url) {
		<c:choose>
			<c:when test="${not empty USER_INFO.id}">
				location.href = url;
			</c:when>
			<c:otherwise>
				location.href = "<%=egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper.getRedirectLoginUrl()%>";
			</c:otherwise>
		</c:choose>
	}
</script>
				<!-- 
				<div class="btn_movie_list">
					<a href="#" class="this"><span class="ctype1">사진으로 보기</span></a>
					<a href="#"><span class="ctype2">목록으로 보기</span></a>
				</div>
				 -->

				<div class="list_search">
					<img src="${_IMG}/sub/icon_srearch.gif" alt="검색" />

					<div class="search_bar">
						<form name="searchForm" action="<c:url value="/mma/MltmdMvpInfoList.do"/>" method="post">
							<input type="hidden" name="searchCate" value="${searchVO.searchCate}"/>
							<select name="searchCondition" class="list_select">
								<option value="0">제목</option>
								<option value="1">내용</option>
								<option value="2">키워드</option>
							</select>
	
							<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="list_search_inp">
	
							<button type="submit"  class="btn_search">검색</button>
						</form>
					</div>
				</div>

				<div class="section_01">
					<ul>
						<c:forEach var="mvp" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="/mma/selectMltmdMvpInfo.do${_BASE_PARAM}">
							  	<c:param name="mltmdMvpId" value="${mvp.mltmdMvpId}" />
							  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
						    </c:url>
							<li>
								<a href="<c:out value="${viewUrl}"/>"><c:choose><c:when test="${not empty mvp.thumbFilePath}"><img src="${mediaMovieImageWebUrl}${mvp.thumbFilePath}" alt="<c:out value="${mvp.mvpSj}"/>" onerror="this.src='${_IMG}/common/no_img.png'"/></c:when><c:otherwise><img src="${_IMG}/common/no_img.png" alt="<c:out value="${result.mvpSj}"/>" /></c:otherwise></c:choose></a>
								<a href="<c:out value="${viewUrl}"/>" class="movie_tit"><c:out value="${mvp.mvpSj}"/> <img src="${_IMG}/common/btn_go.png" alt="go" /></a>
								<span class="date"><fmt:formatDate value="${mvp.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></span>
							</li>
						</c:forEach>
					</ul>
				</div>
				<!-- 학교홍보영상 end -->

				<div id="paging">
					<c:url var="pageUrl" value="/mma/MltmdMvpInfoList.do${_BASE_PARAM}">
					</c:url>
					<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
					<ui:pagination paginationInfo="${paginationInfo}" type="egovPaging" jsFunction="${pagingParam}" />
				</div>

				<div class="btn_r">
					<c:url var="addUrl" value="/mma/addMltmdMvpInfo.do${_BASE_PARAM}">
					</c:url>
					<a class="bbtn3" href="<c:out value="${addUrl}"/>" onclick="fn_egov_add(this.href);return false;"><span>동영상 올리기</span></a>
				</div>

<c:import url="/mma/tmplatBottom.do" charEncoding="utf-8"/>