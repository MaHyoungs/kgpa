<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/mma/images"/>
<c:set var="_C_LIB" value="${pageContext.request.contextPath}/lib"/>

<c:set var="_MODE" value=""/>
<c:set var="_EDITOR_ID" value="mvpCn"/>
<c:choose>
	<c:when test="${empty searchVO.mltmdMvpId}">
		<c:set var="_MODE" value="REG"/>
		<c:set var="moviePluginLoaded" value="moviePluginLoaded"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
		<c:set var="moviePluginLoaded" value="moviePluginLoadedModify"/>
	</c:otherwise>
</c:choose>

<c:import url="/mma/tmplatHead.do" charEncoding="utf-8">
	<c:param name="searchTarget" value="MVP"/>
	<c:param name="ctgrymasterId" value="${searchVO.ctgrymasterId}"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="mltmdMvpInfoVO" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javaScript" language="javascript" defer="defer">
<!--

function fn_egov_regist(frm) {
	if(!confirm("<spring:message code="${_MODE eq 'REG' ? 'common.regist.msg' : 'common.update.msg'}" />")){
		return fn_egov_check(frm);
	}
}

function cntntsHiddenSubmit() {
	var thisForm = document.mltmdMvpInfoVO;	
	thisForm.submit();
}

function fn_egov_regist() {
	var frm = document.mltmdMvpInfoVO;
	if (!validateMltmdMvpInfoVO(frm)){
		return false;
	}
	
	if($('#${_EDITOR_ID}').html().length == 0) {
		alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
		return false;
	}
	
	$('#ctgryId').val('');
	for(var cmIdx = 1 ; cmIdx <= ${mltmdCateLevel} ; cmIdx++){
		var cmObj = document.getElementById("ctgry" + cmIdx);
		if(cmObj != null) {
			if($('#ctgry' + cmIdx + ' option:selected').val() != '') {
				$('#ctgryId').val($('#ctgry' + cmIdx + ' option:selected').val());
			}
		}
	}
	
	if($.trim($('#mltmdMvpId').val()) == "") {
		$('#mltmdMvpId').val($('#takeMvpId').val());
	}
	
	if(movieSlCtl.Content.Files.TotalFilesSelected == 0 && $.trim($('#mltmdFileId').val()) == "") {
		alert('파일을 첨부해 주세요');
		return false;
	}
		
	if(confirm("<spring:message code="${_MODE eq 'REG' ? 'common.regist.msg' : 'common.update.msg'}" />")){
		if(!movieFinished && movieSlCtl.Content.Files.Percentage == 0) {		
			isSubmitAction = true;
			alert("업로드가 진행됩니다. 잠시만 기다려주세요.");
			movieStartUpload();
			return false;
		}
	} else {
		return false;
	}
	
}

function fnMltmdCtgryChange(lvl) {
	for(var cmIdx = lvl ; cmIdx <= ${mltmdCateLevel} ; cmIdx++){
		$('#ctgry' + (cmIdx + 1)).empty().append("<option value=''>선택(0)</option>");
	}
	
	if(${mltmdCateLevel} > lvl && $('#ctgry' + lvl + ' option:selected').val() != '') {
		var url = "/mma/selectCtgryListForAjax.do?ctgrymasterId=${searchVO.ctgrymasterId}&searchTarget=" + $('#ctgry' + lvl).val();
		$.getJSON(url, function(data) {
			$('#ctgry' + (lvl + 1)).empty().append("<option value=''>선택(" + (data.length - 1) + ")</option>");
			$.each(data, function(idx, ctgry){
				if($('#ctgry' + lvl).val() != ctgry.ctgryId) {
					$('#ctgry' + (lvl + 1)).append("<option value='" + ctgry.ctgryId + "' " + ((ctgry.siteId == '${mltmdMvpInfoVO.ctgryId}') ? 'selected=selected': '') + ">"+ ctgry.ctgryNm + "</option>");
				}
			});
		})
		.success(function() {fnMltmdCtgryInit(lvl);})
		.error(function() {alert('문제가 발생하여 작업을 완료하지 못하였습니다.');})
		.complete(function() {});
	}
};
function fnMltmdCtgryInit(lvl) {
	var arr = "${mltmdMvpInfoVO.ctgryPathById}".replace('[','').replace(']','').split(",");
	if(arr.length >= lvl) {
		$("#ctgry" + (lvl + 1)).val(arr[lvl]).attr("selected", "selected");
		fnMltmdCtgryChange(lvl + 1);
	}
}
$(document).ready( function() {
	$('#${_EDITOR_ID}').tinymce({
		script_url : '${_C_LIB}/tiny_mce/tiny_mce.js',
		language : "ko", 
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "silver",
		plugins : "autolink,lists,table,advhr,advimage,advlink,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,advlist,template",
		theme_advanced_buttons1 : "code,|,fullscreen,|,preview,|,print,|,newdocument,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,link,unlink,image,media,cleanup,|,tablecontrols",
		theme_advanced_buttons2 : "fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,outdent,indent,|,template,charmap,hr,removeformat,visualaid,|,sub,sup,|,ltr,rtl",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		content_css : "${pageContext.request.contextPath}/template/manage/css/default.css",
		template_external_list_url : "${pageContext.request.contextPath}/template/manage/docTemplate/mma/template_list.js",
		convert_urls : false
	});
	
	<c:if test="${not empty mltmdMvpInfoVO.ctgryPathById}">
		fnMltmdCtgryInit(0);
	</c:if>
});

/* movie */
var movieSlCtl = null;
var movieFinished = false;
var isSubmitAction = false;
function moviePluginLoaded(sender) {
    movieSlCtl = document.getElementById("MovieMultiFileUploader");
    movieSlCtl.Content.Files.AllFilesFinished = movieAllFilesFinished;
    movieSlCtl.Content.Files.FileAdded = movieStateChanged;
    movieSlCtl.Content.Files.FileRemoved = movieStateChanged;
    movieSlCtl.Content.Files.StateChanged = movieStateChanged; 
}

function moviePluginLoadedModify(sender) {
    movieSlCtl = document.getElementById("MovieMultiFileUploader");
    movieSlCtl.Content.Files.AllFilesFinished = movieAllFilesFinished;
    movieSlCtl.Content.Files.FileAdded = movieStateChanged;
    movieSlCtl.Content.Files.FileRemoved = movieStateChanged;
    movieSlCtl.Content.Files.StateChanged = movieStateChanged; 
    movieSlCtl.Content.Control.SetInitData(document.getElementById('mvpJsonData').value);

}

function movieAllFilesFinished() {
    var jsonValue = movieSlCtl.Content.Files.UploadImageInfoByJson;
    document.getElementById('mvpJsonData').value = jsonValue;
    movieFinished = true;
    
    if(movieFinished && isSubmitAction) {
    	cntntsHiddenSubmit();
    }
}

function movieStateChanged() {
	document.getElementById('mvpChangeAt').value = "Y";
}

function movieStartUpload() {
    if (movieSlCtl != null) {
        movieSlCtl.Content.Control.StartUpload();
    }
}

function onSilverlightError(sender, args) {

    var appSource = "";
    if (sender != null && sender != 0) {
        appSource = sender.getHost().Source;
    }
    var errorType = args.ErrorType;
    var iErrorCode = args.ErrorCode;

    var errMsg = "Unhandled Error in Silverlight 2 Application " + appSource + "\n";

    errMsg += "Code: " + iErrorCode + "    \n";
    errMsg += "Category: " + errorType + "       \n";
    errMsg += "Message: " + args.ErrorMessage + "     \n";

    if (errorType == "ParserError") {
        errMsg += "File: " + args.xamlFile + "     \n";
        errMsg += "Line: " + args.lineNumber + "     \n";
        errMsg += "Position: " + args.charPosition + "     \n";
    }
    else if (errorType == "RuntimeError") {
        if (args.lineNumber != 0) {
            errMsg += "Line: " + args.lineNumber + "     \n";
            errMsg += "Position: " + args.charPosition + "     \n";
        }
        errMsg += "MethodName: " + args.methodName + "     \n";
    }

    throw new Error(errMsg);
}
// -->
</script>
	<c:choose>
	<c:when test="${_MODE eq 'REG' }">
		<c:url var="actionUrl" value="/mma/insertMltmdMvpInfo.do"/>
	</c:when>
	<c:otherwise>
		<c:url var="actionUrl" value="/mma/updateMltmdMvpInfo.do"/>
	</c:otherwise>
	</c:choose>
	<form:form commandName="mltmdMvpInfoVO" name="mltmdMvpInfoVO" action="${actionUrl}" method="post" onsubmit="return fn_egov_regist()">
	
	<form:hidden path="siteId"/>
	<form:hidden path="takeMvpId"/>
	<form:hidden path="mltmdMvpId"/>
	<form:hidden path="ctgryId"/>
	<form:hidden path="mltmdFileId"/>
	
	<input type="hidden" name="mvpBassCours" value="<c:out value='${mvpFileVO.mvpBassCours}'/>"/>
	<input type="hidden" id="mvpChangeAt" name="mvpChangeAt" value=""/>
	<input type="hidden" name="movieFileId" value="${mvpFileVO.mvpFileId}"/>
	<textarea id="mvpJsonData" name="mvpJsonData" style="display:none">${mvpFileVO.mvpJsonData}</textarea>
	
	<input type="hidden" name="searchCate" value="<c:out value='${searchVO.searchCate}'/>"/>
	<input type="hidden" name="searchCondition" value="<c:out value='${searchVO.searchCondition}'/>"/>
	<input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>"/>
	<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>
	
				<table class="chart">
					<tr>
						<th>* 카테고리</th>
						<td>
							<c:forEach var="ctgryLevel" begin="1" end="${mltmdCateLevel}" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.first}">
										<select name="ctgry${ctgryLevel}" id="ctgry${ctgryLevel}" class="board_select_0${status.count}" onchange="fnMltmdCtgryChange(${ctgryLevel})">
											<option value="">선택</option>
											<c:forEach var="cate" items="${mltmdCateList}">
												<c:if test="${cate.ctgryLevel eq 1 }">
													<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
												</c:if>
											</c:forEach>
										</select>
									</c:when>
									<c:otherwise><select name="ctgry${ctgryLevel}" id="ctgry${ctgryLevel}" class="board_select_0${status.count}" onchange="fnMltmdCtgryChange(${ctgryLevel})"><option value="">선택</option></select></c:otherwise>
								</c:choose>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<th>* <label for="mvpSj">제목</label></th>
						<td><form:input path="mvpSj" size="60" cssClass="board_tit" maxlength="255"/><form:errors path="mvpSj" /></td>
					</tr>
					<tr>
						<th>* <label for="searchKwrd">키워드</label></th>
						<td><form:input path="searchKwrd" size="60" cssClass="board_key" maxlength="100"/><form:errors path="searchKwrd" /></td>
					</tr>
					<tr>
						<th>* 동영상첨부</th>
						<td>
							<div id="silverlightControlHost_01" >
						        <object id="MovieMultiFileUploader" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="90">
						            <param name="source" value="${_C_LIB}/mma/MultiFileUpload.xap" />
						            <param name="onerror" value="onSilverlightError" />
						            <param name="background" value="white" />				            
						            <param name="onload" value="${moviePluginLoaded}" />
						            <param name="minRuntimeVersion" value="2.0.31005.0" />
						            <param name="autoUpgrade" value="true" />           
						            <param name="initParams" value="path=${mvpFileVO.mvpBassCours}/${_MODE eq 'REG' ? mltmdMvpInfoVO.takeMvpId : mltmdMvpInfoVO.mltmdMvpId}, Target=MVP, BaseMetaEdited=false, BaseMetaAuthEdited=false, InfoXmlPath=/mma/uploaderInfo.do"/>
									<a href="http://go.microsoft.com/fwlink/?LinkID=124807" style="text-decoration: none;">
						                <img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Get Microsoft Silverlight" style="border-style: none;" />
						            </a>
						        </object>
						        <iframe style='visibility:hidden;height:0;width:0;border:0;'></iframe>
						    </div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<form:textarea path="mvpCn" rows="30" cssStyle="width:100%"/><br/><form:errors path="mvpCn" />
						</td>
					</tr>
				</table>
	
				<div class="btn_r">
					<button type="submit" class="bbtn3">${_MODE eq 'REG' ? '등록하기' : '수정하기' }</button>
					<c:url var="listUrl" value="/mma/MltmdMvpInfoList.do">
						<c:param name="pageIndex" value="${searchVO.pageIndex}" />
						<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
					  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
						<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
					</c:url>					
					<a href="<c:out value="${listUrl}"/>" class="bbtn4"><span>목록으로</span></a>
				</div>
	</form:form>

<c:import url="/mma/tmplatBottom.do" charEncoding="utf-8"/>