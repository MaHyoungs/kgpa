<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="${pageContext.request.contextPath}/template/common/css"/>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<c:set var="_PREFIX" value="/mma/cop/bbs"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="menuId" value="${searchVO.menuId}"/>
		<c:param name="bbsId" value="${searchVO.bbsId}" />
		<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
			<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
				<c:if test="${not empty searchCate}">
					<c:param name="searchCateList" value="${searchCate}" />
				</c:if>
			</c:forEach>
		</c:if>
	  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
		<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/mma/tmplatHead.do" charEncoding="utf-8">
			<c:param name="searchTarget" value="BBS"/>
			<c:param name="BBS_CSS" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId}/style.css"/>
			<c:param name="BBS_JS" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId}/script.js"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="Content-Script-Type" content="text/javascript" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />	
			<link charset="utf-8" href="${_C_CSS}/default.css" type="text/css" rel="stylesheet"/>
			<script type="text/javascript" src="${_C_JS}/jquery/jquery-1.8.2.min.js"></script>
			<script type="text/javascript" src="${_C_JS}/common.js"></script>
			<link charset="utf-8" href="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId}/style.css" type="text/css" rel="stylesheet"/>
			<script type="text/javascript" src="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId}/script.js"></script>
		</head>
		<body>
	</c:otherwise>
</c:choose>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
		<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.js" ></script>
		<script type="text/javascript" src="${_C_JS}/board.js" ></script>
		<script type="text/javascript">
		
		<c:if test="${!empty brdMstrVO.ctgrymasterId}">		
			var boardCateLevel = ${boardCateLevel};
			var boardCateList = [];
			<c:forEach var="cate" items="${boardCateList}" varStatus="status">
				boardCateList[${status.index}] = new ctgryObj('${cate.upperCtgryId}', '${cate.ctgryId}', '${cate.ctgryNm}', ${cate.ctgryLevel});
			</c:forEach>		
		</c:if>
		
		function fn_egov_addNotice(url) {
	    	<c:choose>
				<c:when test="${not empty USER_INFO.id}">
					location.href = url;
				</c:when>
				<c:otherwise>
					location.href = "<%=egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper.getRedirectLoginUrl()%>";
				</c:otherwise>
			</c:choose>
	    }
		
		<c:if test="${(SE_CODE >= 10) and (not IS_MOBILE) }">
		//관리자 함수 시작
		$(document).ready(function(){
			$("#checkAll").click(function() {	
				$("input:checkbox[name=nttNoArr]").attr("checked", $(this).is(":checked"));
			});
			
/*			$('#btnManageMove').click(function() {if(checkArticle()) {$('#registAction').val('Move');bbsSelectPop();}return false;});
			$('#btnManageCopy').click(function() {if(checkArticle()) {$('#registAction').val('Copy');bbsSelectPop();}return false;});*/
			$('#btnManageHide').click(function() {if(checkArticle()) {if(confirm('삭제 하시겠습니까?')) {$('#registAction').val('Hide');} else {return false;}} else {return false;}});
			$('#btnManageRemove').click(function() {if(checkArticle()) {if(confirm('완전삭제 후에는 복구 할 수 없습니다. 완전삭제 하시겠습니까?')) {$('#registAction').val('Remove');} else {return false;}} else {return false;}});
			$('#btnManageRepair').click(function() {if(checkArticle()) {if(confirm('복구 하시겠습니까?')) {$('#registAction').val('Repair');} else {return false;}} else {return false;}});
			
			$("#listForm").ajaxForm({
				url : '/cop/bbs/manageArticle.do'
				, dataType : 'json'
		        , beforeSubmit : function($form, options) {
		        	if(checkArticle()) {
		        		cfgCommonPopShow();
		        	} else {
		        		return false;
		        	}
		        }
		        , success : function(data) {   
		        	cfgCommonPopHide();
		        	alert(data.message);
		        	document.location.href = $('#returnUrl').val();
				}
		        , error : function() {
		        	alert('문제가 발생하여 요청처리를 완료하지 못하였습니다.');
		        	cfgCommonPopHide();
		        }
		        , resetForm : true
		   });
		});

		function checkArticle() {
			if($("input:checkbox[name=nttNoArr]:checked").length == 0) {
				alert('게시글을 선택해주세요');
				return false;
			}
			return true;
		}

		function bbsSelectPop() {
			var url = "/cop/com/selectAllBBSMasterManageInfs.do?siteId=${brdMstrVO.siteId}&bbsId=${brdMstrVO.bbsId}&trgetId=MMAMVP_SERVICE_BOARD";
			var win = window.open(url ,'bbsSelectPop',' scrollbars=yes, resizable=yes, left=0, top=0, width=700,height=650');
			if(win != null) {
				win.focus();
			}
		}

		function selectBbsMaster(bbsId, ctgryId) {
			$('#trgetId').val(bbsId);
			$('#ctgryId').val(ctgryId);
			
			$('#listForm').submit();
		}

		function cancleBbsMaster() {
			cfgCommonPopHide();
		}

		function cfgCommonPopShow() {
			$('#wrap').append("<div id='layer_blind_box' style='position:absolute; position:fixed; top:0; left:0; width:100%; height:100%; background:#000; z-index:50;'></div>");
			$('#layer_blind_box').css('opacity', 0.3);
		}

		function cfgCommonPopHide() {
				$('#layer_blind_box').remove();
		}
		//관리자 함수끝 
		</c:if>
		</script>		

<c:choose>
	<c:when test="${IS_MOBILE }">
	<div id="bbs_mbl">
	</c:when>
	<c:otherwise>
	<div id="bbs_wrap">
	</c:otherwise>
</c:choose>
	<form id="listForm" name="listForm" method="post">
		<input type="hidden" id="registAction" name="registAction"/>
		<input type="hidden" id="bbsId" name="bbsId" value="<c:out value="${brdMstrVO.bbsId}"/>"/>
		<input type="hidden" id="trgetId" name="trgetId"/>
		<input type="hidden" id="ctgryId" name="ctgryId"/>
		<c:url var="_LIST_HIDDEN_URL" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			<c:param name="pageIndex" value="${searchVO.pageIndex}" />
		</c:url>
		<input type="hidden" id="returnUrl" name="returnUrl" value="<c:out value="${_LIST_HIDDEN_URL}"/>"/>
		
		<div class="total">총 게시물 <strong>${paginationInfo.totalRecordCount}</strong>건 ㅣ 현재페이지 <strong>${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</div>							
		  	
		<c:choose>
		<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA02'}">
			<%--갤러리형 목록 --%>
			<div class="list_photo">
	            <ul>
	            <c:forEach var="result" items="${resultList}" varStatus="status">
	            	<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
					  	<c:param name="nttNo" value="${result.nttNo}" />
					  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
				    </c:url>
				    <c:set var="isViewEnable" value=""/>
				    <c:choose>
						<c:when test="${SE_CODE eq '10'}"><c:set var="isViewEnable" value="Y"/></c:when>
						<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}"><c:set var="isViewEnable" value="N"/></c:when>
						<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}"><c:set var="isViewEnable" value="N"/></c:when>
						<c:otherwise><c:set var="isViewEnable" value="Y"/></c:otherwise>
					</c:choose>
					<li <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>	                
						<div class="ph_img">
							<c:if test="${isViewEnable eq 'Y'}"><a href="<c:out value="${viewUrl}"/>"></c:if>
	                    	<c:choose>
	                    		<c:when test="${empty result.atchFileNm}">
	                    			<img src="${_IMG}/noimg.gif" width="200" height="140" alt="${result.nttSj}"/>
	                    		</c:when>
	                    		<c:otherwise>
	                    			<img src='<c:url value='/cmm/fms/getImage.do'/>?thumbYn=Y&siteId=<c:out value="${brdMstrVO.siteId}"/>&appendPath=<c:out value="${searchVO.bbsId}"/>&atchFileNm=<c:out value="${result.atchFileNm}"/>' alt="${result.nttSj}" width="200" height="140"/>
	                    		</c:otherwise>
	                    	</c:choose>
	                    	<c:if test="${isViewEnable eq 'Y'}"></a></c:if>
						</div>
						<div class="ph_cont">
							<span>
								<c:if test="${isViewEnable eq 'Y'}"><a href="<c:out value="${viewUrl}"/>"></c:if>
	                    		<c:choose>
								<c:when test="${fn:length(result.nttSj) > 15}">
									<c:out value='${fn:substring(result.nttSj, 0, 15)}'/>...
								</c:when>
								<c:otherwise>
									<c:out value="${result.nttSj}" />
								</c:otherwise>
								</c:choose>
	                    		<c:if test="${isViewEnable eq 'Y'}"></a></c:if>								 
	                    		<c:if test="${result.othbcAt eq 'N'}">
	                    			<img src="${_IMG}/ico_board_lock.gif" width="14" height="13" alt="비밀글" />
	                    			<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
										<c:choose>
											<c:when test="${result.commentCount eq 0}"><strong>[<c:out value="${result.commentCount}" />]</strong></c:when>
											<c:otherwise><strong>[<c:out value="${result.commentCount}" />]</strong></c:otherwise>
										</c:choose>
									</c:if>	                    				                    		
	                    		</c:if>								
							</span>
							<span class="ph_date"><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></span>
						</div>
	                  
	                 
	              </li>
	            </c:forEach>
				</ul>
				<c:if test="${fn:length(resultList) == 0}">
					<table class="boardscn_list" ><tr><td><spring:message code="common.nodata.msg" /></td></tr></table>
			    </c:if>
			</div>
		</c:when>				
		<c:otherwise>
			<%-- 일반 게시판 목록 --%>
			<div id="${brdMstrVO.tmplatId }" class="bss_list">
				<%-- 주의 : 컬럼이 추가시 summary에 작성을 해야한다. --%>
				<table summary="
					<spring:message code="cop.nttNo"/>,
					<c:if test="${not empty brdMstrVO.ctgrymasterId}"><spring:message code="cop.category.view" />,</c:if>
					<spring:message code="cop.nttSj" />
					<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><spring:message code="cop.processSttus" />,</c:if>
					<spring:message code="cop.ntcrNm" />,
					<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}"><spring:message code="cop.listAtchFile" />,</c:if>
					<spring:message code="cop.frstRegisterPnttm" />,
					<spring:message code="cop.inqireCo" />
				" class="list_table">
				<caption>${brdMstrVO.bbsNm }</caption>
				<thead>
					<tr>
						<c:if test="${SE_CODE >= 10 }">
						<th class="check"><input type="checkbox" id="checkAll" value="all"/><label for="checkAll">선택</label></th>
						</c:if>
						<th class="num"><spring:message code="cop.nttNo"/></th>
						<c:if test="${not empty brdMstrVO.ctgrymasterId}">
							<th class="class"><spring:message code="cop.category.view" /></th>							
						</c:if>
						<th class="tit"><spring:message code="cop.nttSj" /></th>
						<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
			          		<th class="state"><spring:message code="cop.processSttus" /></th>
			          	</c:if>
						<th class="writer"><spring:message code="cop.ntcrNm" /></th>
						<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
							<th class="file"><spring:message code="cop.listAtchFile" /></th>
						</c:if>	
						<th class="date"><spring:message code="cop.frstRegisterPnttm" /></th>
						<th class="hits"><spring:message code="cop.inqireCo" /></th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach var="result" items="${noticeList}" varStatus="status">
						<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
					    </c:url>
					<tr class="notice">
						<c:if test="${SE_CODE >= 10 }">
							<td class="check"></td>
						</c:if>
						<td class="num"><img src="${_IMG}/icon_notice.gif" alt="공지" /></td>
						<c:if test="${not empty brdMstrVO.ctgrymasterId}">
							<td class="class"><c:out value="${result.ctgryNm}" /></td>
						</c:if>
						<td class="tit">
							<a href="<c:out value="${viewUrl}"/>" class="notice_ti"><c:out value="${result.nttSj}" /></a>
							<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">								
								<c:choose>
									<c:when test="${IS_MOBILE }"><em class="boardrenum"><c:out value="${result.commentCount}" /></em></c:when>
									<c:when test="${result.commentCount eq 0}"><em class="boardrenumno">[<c:out value="${result.commentCount}" />]</em></c:when>
									<c:otherwise><em class="boardrenum">[<c:out value="${result.commentCount}" />]</em></c:otherwise>
								</c:choose>								
							</c:if>
						</td>
						<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
			          		<td class="state">
			          			<c:choose>
									<c:when test="${result.processSttusCode eq 'QA01'}"><img src="${_C_IMG}/page/board/btn_receipt.gif" alt="${result.processSttusNm}"/>
									</c:when>
									<c:when test="${result.processSttusCode eq 'QA03'}"><img src="${_C_IMG}/page/board/btn_comp.gif" alt="${result.processSttusNm}"/>
									</c:when>
									<c:when test="${result.processSttusCode eq 'QA02'}"><img src="${_C_IMG}/page/board/btn_disp.gif" alt="${result.processSttusNm}"/>
									</c:when>
								</c:choose>
			          		</td>
			          	</c:if>						
						<td  class="writer"><c:out value="${result.ntcrNm}"/></td>
						<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
							<td class="file"><c:choose><c:when test="${not empty result.atchFileId}"><img src="${_IMG}/ico_file.gif" alt="첨부파일" /></c:when><c:otherwise><c:if test="${not IS_MOBILE }">-</c:if></c:otherwise></c:choose></td>
						</c:if>
						<td class="date"><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
						<td class="hits"><c:out value="${result.inqireCo}"/></td>
					</tr>
					</c:forEach>					
					<c:forEach var="result" items="${resultList}" varStatus="status">
						<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
					    </c:url>
						<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
							<c:if test="${SE_CODE >= 10 and (not IS_MOBILE)}">
								<td class="check"><input type="checkbox" name="nttNoArr" value="${result.nttNo}" title="선택"/></td>
							</c:if>
							<td class="num"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
							<c:if test="${not empty brdMstrVO.ctgrymasterId}">
								<td class="class"><c:out value="${result.ctgryNm}" /></td>
							</c:if>
							<td class="tit">
								<c:if test="${result.ordrCodeDp gt 0}">
								  <img src="${_C_IMG}/sub/board/blank_bg.gif" width="${result.ordrCodeDp * 19}" height="0" alt="${result.ordrCodeDp} Depth" /><img src="${_IMG}/ico_reply.gif" alt="따라붙은글" />
						        </c:if>
						        <c:choose>
									<c:when test="${SE_CODE eq '10'}"><a href="<c:out value="${viewUrl}"/>" class="notice_ti"><c:out value="${result.nttSj}" /></a></c:when>
									<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
										<c:out value="${result.nttSj}" />
									</c:when>
									<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
										<c:out value="${result.nttSj}" />
									</c:when>
									<c:otherwise><a href="<c:out value="${viewUrl}"/>"><c:out value="${result.nttSj}" /></a></c:otherwise>
								</c:choose>
								<c:if test="${result.othbcAt eq 'N'}"><img src="${_IMG}/ico_board_lock.gif" alt="잠긴글" /></c:if>
								<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
									<c:choose>
										<c:when test="${IS_MOBILE }"><em class="boardrenum"><c:out value="${result.commentCount}" /></em></c:when>
										<c:when test="${result.commentCount eq 0}"><em class="boardrenumno">[<c:out value="${result.commentCount}" />]</em></c:when>
										<c:otherwise><em class="boardrenum">[<c:out value="${result.commentCount}" />]</em></c:otherwise>
									</c:choose>
								</c:if>
							</td>
							<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
				          		<td class="state">
				          			<c:choose>
										<c:when test="${result.processSttusCode eq 'QA01'}"><img src="${_C_IMG}/page/board/btn_receipt.gif" alt="${result.processSttusNm}"/>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA03'}"><img src="${_C_IMG}/page/board/btn_comp.gif" alt="${result.processSttusNm}"/>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA02'}"><img src="${_C_IMG}/page/board/btn_disp.gif" alt="${result.processSttusNm}"/>
										</c:when>
									</c:choose>
				          		</td>
				          	</c:if>						
							<td  class="writer"><c:out value="${result.ntcrNm}"/></td>
							<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
								<td class="file"><c:choose><c:when test="${not empty result.atchFileId}"><img src="${_IMG}/ico_file.gif" alt="첨부파일" /></c:when><c:otherwise><c:if test="${not IS_MOBILE }">-</c:if></c:otherwise></c:choose></td>
							</c:if>
							<td class="date"><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
							<td class="hits"><c:out value="${result.inqireCo}"/></td>
						</tr>
					</c:forEach>
					
					<c:if test="${fn:length(resultList) == 0}">
				    	<tr class="empty"><td colspan="10"><spring:message code="common.nodata.msg" /></td></tr>
				    </c:if>
					
				</tbody>
				</table>				
			</div>
			
			<c:if test="${brdMstrVO.registAuthor eq '02' or SE_CODE >= brdMstrVO.registAuthor or SE_CODE >= 10}">			
			<div class="btn_all"> <%--버튼목록 --%>
				<c:if test="${(SE_CODE >= 10) and (not IS_MOBILE) }">
				<div class="fL">
<%--					<input type="image" id="btnManageMove" src="${_IMG}/btn_move.gif" alt="이동" />
					<input type="image" id="btnManageCopy" src="${_IMG}/btn_copy.gif" alt="복사" />--%>
					<input type="image" id="btnManageHide" src="${_IMG}/btn_close.gif" alt="삭제" />
					<input type="image" id="btnManageRemove" src="${_IMG}/btn_del_all.gif" alt="완전삭제" />	
					<input type="image" id="btnManageRepair" src="${_IMG}/btn_repair.gif" alt="복구" />	
				</div>
				</c:if>
				<div class="fR">
					<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
						<c:param name="registAction" value="regist" />
					</c:url>
					<a href="<c:out value="${addBoardArticleUrl}"/>" onclick="fn_egov_addNotice(this.href);return false;" title="글쓰기화면 이동">
						<img src="${_IMG}/btn_write.gif" alt="글쓰기" />
					</a>
				</div>				
			</div>
			</c:if>	
	</form>
			<div id="paging"> <%--페이징 처리 --%>
			    <c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
				</c:url>
			   <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
			   <ui:pagination paginationInfo="${paginationInfo}" type="egovPaging" jsFunction="${pagingParam}" />
			   
			</div>
  		</c:otherwise>	  		
	  	</c:choose>
			<div id="bbs_search"> <%--검색 --%>
				<form name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
				  	<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
					<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
					<input name="searchCate" type="hidden" value="${searchVO.searchCate}" />
					<fieldset>
						<legend>검색조건입력폼</legend>
						<c:if test="${!empty brdMstrVO.ctgrymasterId}">
					        <c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.first}">
										<label for="ctgry${ctgryLevel}" class="hdn">분류</label>
										<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})">
											<option value="">선택</option>
											<c:forEach var="cate" items="${boardCateList}">
												<c:if test="${cate.ctgryLevel eq 1 }">
													<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
												</c:if>
											</c:forEach>
										</select>
									</c:when>
									<c:otherwise>
										<label for="ctgry${ctgryLevel}" class="hdn">분류${ctgryLevel}</label>
										<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})" class="search_sel"><option value="">선택</option></select>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<script type="text/javascript">
								fnCtgryInit('${searchVO.searchCateList}');
							</script>
					      </c:if>
						<label for="ftext" class="hdn">분류검색</label>
						<select name="searchCnd" id="ftext">
							<option value="0" <c:if test="${searchVO.searchCnd eq '0'}">selected="selected"</c:if>>제목</option>
			          		<option value="1" <c:if test="${searchVO.searchCnd eq '1'}">selected="selected"</c:if>>내용</option>
			          		<option value="2" <c:if test="${searchVO.searchCnd eq '2'}">selected="selected"</c:if>>작성자</option>
						</select>
						<label for="inp_text" class="hdn">검색어입력</label>
						<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" class="inp_s" id="inp_text" />
						<input type="image" src="${_IMG}/btn_search.gif" alt="검색" />
					</fieldset>
				</form>
			</div>			  	
			
</div> <!-- #bbsWrap end -->

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/mma/tmplatBottom.do" charEncoding="utf-8"/>
	</c:when>
	<c:otherwise>
		</body>
		</html>
	</c:otherwise>
</c:choose>
