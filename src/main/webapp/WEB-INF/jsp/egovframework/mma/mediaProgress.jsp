<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<root>
	<StatusInfo>
		<Status>${result.cnvrClCode}</Status>
		<Progress>${result.cnvrRt}</Progress>
		<Memo>${result.memo}</Memo>
	</StatusInfo>
</root>
