<?xml version="1.0" encoding="utf-8" ?>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_C_LIB" value="${pageContext.request.contextPath}/lib"/>
<c:set var="serverName" value="<%=request.getServerName()%>"/>
	<c:set var="serverPort" value="<%=request.getServerPort()%>"/>
	<c:choose>
		<c:when test="${serverPort == 80}">
			<c:set var="serverPort" value=""/>
		</c:when>
		<c:otherwise>
			<c:set var="serverPort" value=":${serverPort}"/>
		</c:otherwise>
	</c:choose>
	<root>
	  <WaterMarkConfig typevalue="Image" text="" bold="False" itelic="False" imagename="http://${serverName}${serverPort}${SiteFileStoreWebPath}${mltmdSetup.wmStreFileNm}" halign="${mltmdSetup.wmLcWidth}" valign="${mltmdSetup.wmLcVrticl}" opacity="<fmt:formatNumber value="${mltmdSetup.wmTrnsprcNcl * 0.01}" type="number"/>" />
	  <StatusInfo>
	  	<Status>${mltmdFile.cnvrClCode}</Status>
		<Progress>${mltmdFile.cnvrRt}</Progress>
		<Memo>${mltmdFile.memo}</Memo>
	  </StatusInfo>
	  <ContentUrlInfo>
	    <ProgressUrl><![CDATA[http://${serverName}${serverPort}/mma/mvpProgress.do?mltmdFileId=${mltmdFile.mltmdFileId}]]></ProgressUrl>
	  </ContentUrlInfo>
	  <ArticleUrlInfo>
	    <Html>
	      <![CDATA[<object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
	        <param name="source" value="http://${serverName}${serverPort}${_C_LIB}/mma/MovieClient.xap"/>
	        <param name="onerror" value="onSilverlightError" />
	        <param name="background" value="white" />
	        <param name="minRuntimeVersion" value="3.0.40818.0" />
	        <param name="autoUpgrade" value="true" />
	        <param name="initParams" value="InfoXmlPath=http://${serverName}${serverPort}/mma/mvpMediaInfo.do?mltmdFileId=${mltmdFileId}&mltmdMvpId=${param.mltmdMvpId}&searchCate=${param.searchCate}&contents=<c:out value="${param.contents}"/>" />
	        <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50401.0" style="text-decoration: none;">
	          <img src="http://go.microsoft.com/fwlink/?LinkID=161376" alt="Get Microsoft Silverlight" style="border-style: none"/>
	        </a>
	      </object>
	      ]]>
	    </Html>
	    <LinkUrl><![CDATA[http://${serverName}${serverPort}/mma/selectMltmdMvpInfo.do?mltmdMvpId=${param.mltmdMvpId}&searchCate=${param.searchCate}]]></LinkUrl>
	    <%
	    	String endcodeParam = URLEncoder.encode("/mma/selectMltmdMvpInfo.do?mltmdMvpId=" + request.getParameter("mltmdMvpId") + "&searchCate=" + request.getParameter("searchCate"), "UTF-8");
	    %>
	    <TwiterUrl><![CDATA[http://${serverName}${serverPort}/cop/sns/SendTwitter.do?contents=<c:out value="${param.contents}"/>&currentUrl=http://${serverName}${serverPort}<%=endcodeParam%>]]></TwiterUrl>
	    <FaceBookUrl><![CDATA[http://${serverName}${serverPort}/cop/sns/SendFacebook.do?contents=<c:out value="${param.contents}"/>&currentUrl=http://${serverName}${serverPort}<%=endcodeParam%>]]></FaceBookUrl>
	    <MetoDayUrl><![CDATA[http://${serverName}${serverPort}/cop/sns/SendM2day.do?contents=<c:out value="${param.contents}"/>&currentUrl=http://${serverName}${serverPort}<%=endcodeParam%>]]></MetoDayUrl>
	    <QrCodeImageUrl><![CDATA[http://${serverName}${serverPort}/EgovQrCode.do?width=200&height=200&text=http://${serverName}${serverPort}<%=endcodeParam%>]]></QrCodeImageUrl>
	  </ArticleUrlInfo>
	
	  <PlayInfo>
	    <PlayMode>Vod</PlayMode>
	    <PlaySource><![CDATA[${mediaMovieServiceWebUrl}${mltmdMvpCnvrInfo.fileStreCours}/${mltmdMvpCnvrInfo.physclNm}/Manifest]]></PlaySource>
	    <ThumNailSource></ThumNailSource>
	  </PlayInfo>
	  <ThumnailList>
	  <c:forEach var="result" items="${moviePhotoList}" varStatus="status">
	    <Thumnail>
	      <Path><![CDATA[${mediaMovieImageWebUrl}${result.fileStreCours}/${result.physclNm}]]></Path>
	      <Time><![CDATA[${result.playTime}]]></Time>
	    </Thumnail>
	  </c:forEach>
	  </ThumnailList>
	</root>