<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<root>
  <SysInfo>
    <UploadWebUrl>${UploadWebUrl}</UploadWebUrl>
    <UploadHandlerName>${UploadHandlerName}</UploadHandlerName>
  </SysInfo>
  
  <AuthList>
	<c:forEach var="result" items="${AuthList}" varStatus="status">
		<Auth>
	    	<AuthCode>${result.authorCode}</AuthCode>
	    	<AuthName>${result.authorNm}</AuthName>
	    </Auth>
	</c:forEach>    
  </AuthList>
</root>
