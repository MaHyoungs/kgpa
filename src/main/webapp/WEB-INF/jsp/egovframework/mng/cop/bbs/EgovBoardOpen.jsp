<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="OPEN_MANAGE"/>
	<c:param name="depth1" value="OPENBOARD_MANAGE"/>
</c:import>

<iframe src="/mng/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000036&siteId=SITE_000000000000001&sysTyCode=&trgetId=SYSTEM_DEFAULT_BOARD" frameborder="0" style="width:100%; height:1200px;" scrolling="no"></iframe>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>