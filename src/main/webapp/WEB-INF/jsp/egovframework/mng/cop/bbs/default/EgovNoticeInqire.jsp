<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<%
	pageContext.setAttribute("newLineChar", "\n");
%>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images" />
<c:set var="_PREFIX" value="/mng/cop/bbs/" />
<c:set var="serverName" value="<%=request.getServerName()%>" />
<c:set var="serverPort" value="<%=request.getServerPort()%>" />
<c:choose>
	<c:when test="${serverPort == 80}">
		<c:set var="serverPort" value="" />
	</c:when>
	<c:otherwise>
		<c:set var="serverPort" value=":${serverPort}" />
	</c:otherwise>
</c:choose>

<%
	/*URL 정의*/
%>
<c:url var="_BASE_PARAM" value="">
	<c:param name="siteId" value="${searchVO.siteId}" />
	<c:param name="sysTyCode" value="${searchVO.sysTyCode}" />
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:param name="trgetId" value="${searchVO.trgetId}" />
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="searchCnd" value="${searchVO.searchCnd}" />
	<c:param name="searchWrd" value="${searchVO.searchWrd}" />
	<c:param name="searchCate" value="${searchVO.searchCate}" />
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<%
	/*URL 정의*/
%>

<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
	<c:param name="title" value="${brdMstrVO.bbsNm}" />
</c:import>

<script type="text/javascript">
	function onloading() {
		if ("<c:out value='${msg}'/>" != "") {
			alert("<c:out value='${msg}'/>");
		}
	}

	function backPage() {
		history.back(-1);
	}

	function fn_egov_delete_notice(url) {

		if (confirm('<spring:message code="common.delete.msg" />')) {
			document.location.href = url;
		}
	}

	function urlCopy() {
		var addr = "http://${serverName}${serverPort}/hpg/bbs/selectBoardArticle.do?nttNo=${searchVO.nttNo}&menuId=${menuId}&bbsId=${searchVO.bbsId}";
		window.clipboardData.setData('Text', addr);
		alert("페이지 주소가 복사되었습니다.\n" + addr);
	}
</script>
<script type="text/javaScript" src="/template/manage/js/mngworklogs.js"></script>
<div id="cntnts">

	<%-- 목록, 답글 수정 URL --%>
	<c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
		<c:param name="nttNo" value="${board.nttNo}" />
		<c:param name="registAction" value="reply" />
	</c:url>
	<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
		<c:param name="nttNo" value="${board.nttNo}" />
		<c:param name="registAction" value="updt" />
	</c:url>
	<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
		<c:param name="nttNo" value="${board.nttNo}" />
	</c:url>
	<%-- 목록, 답글 수정 URL --%>

	<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
		<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
			<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
		</c:import>
	</c:if>

	<%-- 공통 게시판 버튼  --%>
	<div class="btn_a">
		<div class="tt">
			<a href="javascript:backPage()"><img src="${_IMG}/btn/btn_back.gif" alt="뒤로" /></a></span>
		</div>
		<div class="aa">
			<a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>"><img src="${_IMG}/btn/btn_list.gif" alt="목록" /></a>
			<c:if test="${brdMstrVO.replyPosblAt eq 'Y'}">
				<a href="<c:out value="${addReplyBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_reply.gif" alt="답글" /></a>
			</c:if>

			<c:choose>
				<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and USER_INFO.userSe >= 4 and board.processSttusCode eq 'BBSS01'}" >
					<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정" /></a>
				</c:when>
				<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and USER_INFO.userSe >= 4 and board.processSttusCode eq 'BBSS02'}" >
					<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정" /></a>
				</c:when>
				<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and USER_INFO.userSe >= 4 and board.processSttusCode eq 'BBSS03'}" >
					<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정" /></a>
				</c:when>
				<c:when test="${brdMstrVO.bbsAttrbCode ne 'BBSA06'}" >
					<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정" /></a>
				</c:when>
			</c:choose>

			<a href="<c:out value="${deleteBoardArticleUrl}"/>" onclick="fn_egov_delete_notice(this.href);return false;"><img src="${_IMG}/btn/btn_del.gif" alt="삭제" /></a>
		</div>
	</div>
	<%-- 공통 게시판 버튼  --%>

	<div class="view_wrap">
		<%-- 사후관리 게시판 시작 --%>
		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
			<dl class="view_tit">
				<dt>사업명</dt>
				<dd><strong>${bsifVo.biz_nm}</strong></dd>
			</dl>
			<table class="view_writer_chart" style="margin-bottom:40px;">
				<caption></caption>
				<colgroup>
					<col width="110px" />
					<col width="*" />
					<col width="110px" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>사업기간</th>
						<td>${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
						<th>총사업비</th>
						<td><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
					</tr>
					<tr>
						<th>사업지</th>
						<td colspan="3" class="last">${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} ${bsifVo.biz_adres} ${bsifVo.biz_adres_detail}</td>
					</tr>
					<tr>
						<th>전화번호</th>
						<td colspan="3" class="last">${bsifVo.tlphon_no}</td>
					</tr>
				</tbody>
			</table>
		</c:if>
		<%-- 사후관리 게시판 끝 --%>


		<dl class="view_tit">
			<dt>
				<spring:message code="cop.nttSj" />
			</dt>
			<dd>
				<strong><c:out value="${board.nttSj}" /></strong> <a href="#" onclick="urlCopy();return false;"><img src="${_IMG}/btn/add_copy.gif" style="vertical-align: middle; margin-left: 10px;" /></a>
			</dd>
		</dl>

		<table class="view_writer_chart">
			<caption></caption>
			<colgroup>
				<col width="110px" />
				<col width="*" />
				<col width="110px" />
				<col width="*" />
			</colgroup>
			<tbody>
				<c:if test="${!empty brdMstrVO.ctgrymasterId}">
					<tr>
						<th><spring:message code="cop.category.view" /></th>
						<td colspan="3" class="last">
							<c:out value="${board.ctgryNm}" />
						</td>
					</tr>
				</c:if>
				<tr>
					<th><spring:message code="cop.ntcrNm" /></th>
					<td colspan="3" class="last">
						<c:out value="${board.ntcrNm}" />
						<c:if test="${not empty board.frstRegisterId}">
							(<c:out value="${board.frstRegisterId}" />)
						</c:if>
					</td>
				</tr>
				<tr>
					<th><spring:message code="cop.frstRegisterPnttm" /></th>
					<td>
						<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss" />
					</td>
					<th><spring:message code="cop.inqireCo" /></th>
					<td class="last">
						<c:out value="${board.inqireCo}" />
					</td>
				</tr>
				<c:if test="${(board.bbsId eq 'BBSMSTR_000000000023') or (board.bbsId eq 'BBSMSTR_000000000024') or (board.bbsId eq 'BBSMSTR_000000000025')}">
					<tr>
						<th>링크 #</th>
						<td colspan="3" class="last">
							<a href="<c:out value="${board.tmp01}"/>"> <c:out value="${board.tmp01}" /></a>
						</td>
					</tr>
				</c:if>
			</tbody>
		</table>

		<div class="view_cont">
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04' and not empty board.tmp01}">
				<div style="text-align: center; padding: 20px 0 40px 0;">${board.tmp01}</div>
			</c:if>
			<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
		</div>

		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and board.processSttusCode eq 'BBSS02' and not empty board.tmp01}">
			<table class="view_writer_chart">
				<colgroup>
					<col width="110px" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>반려사유</th>
						<td>
							<c:out value="${fn:replace(board.tmp01, newLineChar, '<br/>')}" escapeXml="false" />
						</td>
					</tr>
				</tbody>
			</table>
		</c:if>

		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and board.processSttusCode eq 'BBSS04' and not empty board.tmp02}">
			<table class="view_writer_chart">
				<colgroup>
					<col width="110px" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>기각사유</th>
						<td>
							<c:out value="${fn:replace(board.tmp02, newLineChar, '<br/>')}" escapeXml="false" />
						</td>
					</tr>
				</tbody>
			</table>
		</c:if>

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="view_tit02">
				<dt>
					<spring:message code="cop.atchFileList" />
				</dt>
				<dd>
					<ul class="list">
						<c:if test="${not empty board.atchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }" />
							</c:import>
						</c:if>
					</ul>
				</dd>
			</dl>
		</c:if>

		<c:if test="${(brdMstrVO.bbsAttrbCode eq 'BBSA11' and board.processSttusCode eq 'QA03') or (brdMstrVO.bbsAttrbCode eq 'BBSA07' and board.processSttusCode eq 'QA03')}">
			<c:if test="${not empty board.estnData}">
				<div class="view_cont">
					<c:out value="${board.estnParseData.cn}" escapeXml="false" />
				</div>
			</c:if>
			<c:if test="${not empty board.estnAtchFileId}">
				<dl class="view_tit02">
					<dt>
						<spring:message code="cop.atchFileList" />
					</dt>
					<dd>
						<ul class="list">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.estnAtchFileId}" />
								<c:param name="imagePath" value="${_IMG }" />
							</c:import>
						</ul>
					</dd>
				</dl>
			</c:if>
		</c:if>
	</div>

	<%-- 정보 공개 목록 상태 처리 변경 뷰 --%>
	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA06' and USER_INFO.userSe > 4}">
		<style type="text/css">
			div.sauBox{padding: 10px; display: none;}
			div.sauBox div.sauTit{text-align: center; font-size: 14px; font-weight: bold;}
			textarea.sau{width: 400px; border: 1px solid #9a9b9a;}
		</style>
		<script type="application/javascript">
			function fnAjaxOpenProcessSttusCode(el, status){
				var str = '';
				var data = [];
				if(status == 'BBSS03'){
					str = '승인';
					data = {processSttusCode: status, lastAnswrrId : '${USER_INFO.id}', lastAnswrrNm : '${USER_INFO.name}', bbsId : '${board.bbsId}' , nttNo : '${board.nttNo}', adminAt : 'Y', lastUpdusrId : '${USER_INFO.id}'};
				}else if(status == 'BBSS02'){
					str = '반려';
					$('.sauTit').text('반려사유');
					$('.sauBox').show();
					$('.sau').focus();
					if($('#sau').val() == ''){
						alert('반려 사유를 작성해 주시십시오.');
						$('#sau').val($('#tmp01').val());
						return false;
					}else{
						data = {processSttusCode: status, lastAnswrrId : '${USER_INFO.id}', lastAnswrrNm : '${USER_INFO.name}', bbsId : '${board.bbsId}' , nttNo : '${board.nttNo}', adminAt : 'Y', lastUpdusrId : '${USER_INFO.id}', tmp01 : $('#sau').val()};
					}
				}else if(status == 'BBSS04'){
					str = '기각';
					$('.sauTit').text('기각사유');
					$('.sauBox').show();
					$('.sau').focus();
					if($('#sau').val() == ''){
						alert('기각 사유를 작성해 주시십시오.');
						$('#sau').val($('#tmp02').val());
						return false;
					}else{
						data = {processSttusCode: status, lastAnswrrId : '${USER_INFO.id}', lastAnswrrNm : '${USER_INFO.name}', bbsId : '${board.bbsId}' , nttNo : '${board.nttNo}', adminAt : 'Y', lastUpdusrId : '${USER_INFO.id}', tmp02 : $('#sau').val()};
					}
				}
				if(confirm("해당 게시물을 " + str + " 하시겠습니까?")){
					var url = $(el).attr('href');
					var successFn = function(json){
						if(Number(json.rs) > 0){
							alert(str + " 되었습니다.");
							location.reload(true);
						}
					};
					fn_ajax_json(url, data, successFn, null);
				}
				return false;
			}
		</script>

		<div class="btn_c">
			<div class="sauBox">
				<div class="sauTit">반려사유</div>
				<textarea class="sau" name="sau" id="sau" rows="7"></textarea>
			</div>
			<c:if test="${board.processSttusCode eq 'BBSS01'}">
				<span class="cbtn2">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS03');">승인</a>
				</span>
				<span class="cbtn3">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS02');">반려</a>
				</span>
				<span class="cbtn1">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS04');">기각</a>
				</span>
			</c:if>

			<c:if test="${board.processSttusCode eq 'BBSS02'}">
				<span class="cbtn2">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS03');">승인</a>
				</span>
				<span class="cbtn1">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS04');">기각</a>
				</span>
			</c:if>

			<c:if test="${board.processSttusCode eq 'BBSS03'}">
				<span class="cbtn3">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS02');">반려</a>
				</span>
				<span class="cbtn1">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS04');">기각</a>
				</span>
			</c:if>

			<c:if test="${board.processSttusCode eq 'BBSS04'}">
				<span class="cbtn2">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS03');">승인</a>
				</span>
				<span class="cbtn3">
					<a href="/mng/cop/bbs/updateBoardArticleByOpenProcessSttusCode.do" onclick="return fnAjaxOpenProcessSttusCode(this, 'BBSS02');">반려</a>
				</span>
			</c:if>
			<input type="hidden" id="tmp01" value="${board.tmp01}"/>
			<input type="hidden" id="tmp02" value="${board.tmp02}"/>
		</div>
	</c:if>

</div>

<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8" />