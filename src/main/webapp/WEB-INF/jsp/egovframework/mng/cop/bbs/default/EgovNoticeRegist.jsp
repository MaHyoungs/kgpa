<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images" />
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js" />
<c:set var="_C_LIB" value="${pageContext.request.contextPath}/lib" />
<c:set var="_C_HTML" value="${pageContext.request.contextPath}/template/common/html" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images" />
<c:set var="_PREFIX" value="/mng/cop/bbs" />
<c:set var="_ACTION" value="" />
<c:set var="_EDITOR_ID" value="nttCn" />

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do" />
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do" />
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do" />
	</c:when>
</c:choose>


<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
	<c:param name="title" value="${brdMstrVO.bbsNm}" />
</c:import>

<script type="text/javascript" src="${_C_LIB}/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="${_C_JS}/board.js"></script>
<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.min.js"></script>
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false" />
<script type="text/javascript">

	function fn_egov_regist() {


		if (!fn_egov_bbs_basic_regist(document.board)){
			return false;
		}

		if($('#${_EDITOR_ID}').html().length == 0) {
			alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
			return false;
		}

		<c:choose>
		<c:when test="${searchVO.registAction eq 'updt'}">
		if (!confirm('<spring:message code="common.update.msg" />')) {
			return false
		}
		</c:when>
		<c:otherwise>
		if (!confirm('<spring:message code="common.regist.msg" />')) {
			return false;
		}
		</c:otherwise>
		</c:choose>
	}

	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">

	var boardCateLevel = ${boardCateLevel};

	</c:if>


	$(document).ready( function() {
		var adfile_config = {
			storePath:"Board.fileStorePath",
			appendPath:"${brdMstrVO.bbsId}",
			siteId:"${searchVO.siteId}",
			editorId:"${_EDITOR_ID}"
		};

		fn_egov_bbs_editor(adfile_config);
	});
</script>
<div id="cntnts">
	<form:form commandName="board" name="board" method="post" action="${pageContext.request.contextPath}${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
		<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>" />
		<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>" />
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
		<input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
		<input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}" />
		<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>" />
		<input type="hidden" name="sysTyCode" value="${searchVO.sysTyCode}" />
		<input type="hidden" name="siteId" value="${searchVO.siteId}" />
		<input type="hidden" name="registAction" value="${searchVO.registAction}" />
		<input type="hidden" name="trgetId" value="${searchVO.trgetId}" />

		<form:hidden path="nttNo" />
		<form:hidden path="ordrCode" />
		<form:hidden path="ordrCodeDp" />
		<form:hidden path="atchFileId" />

		<table class="chart2" summary="작성인, 제목, 내용, 파일첨부를 입력하는 표입니다.">
			<caption></caption>
			<colgroup>
				<col width="10%" />
				<col width="90%" />
			</colgroup>
			<tbody>
			<c:choose>
				<c:when test="${(brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply') or (brdMstrVO.bbsAttrbCode eq 'BBSA07' and searchVO.registAction eq 'reply')}">
					<input type="hidden" name="nttSj" value="dummy" />
					<tr>
						<th><spring:message code="cop.processSttus" /></th>
						<td>
							<select name="processSttusCode" class="select">
								<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
									<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}" /></option>
								</c:forEach>
							</select>
						</td>
					</tr>
				</c:when>
				<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA06'}">
					<tr>
						<th><label for="nttSj"><spring:message code="cop.nttSj" /></label></th>
						<td>
							<form:input path="nttSj" cssClass="inp_long" />
							<br />
							<form:errors path="nttSj" />
						</td>
					</tr>
					<c:if test="${not empty statusCodeList}">
						<tr>
							<th><spring:message code="cop.processSttus" /></th>
							<td>
									<%-- 정보공개 관리자 --%>
								<c:if test="${USER_INFO.userSe > 4}">
									<select name="processSttusCode" class="select">
										<c:forEach var="resultState" items="${statusCodeList}" varStatus="status">
											<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}" /></option>
										</c:forEach>
									</select>
								</c:if>

									<%-- 정보공개 담당자 --%>
								<c:if test="${USER_INFO.userSe == 4}">
									<c:choose>
										<c:when test="${board.processSttusCode eq 'BBSS01'}">
											상신 <input type="hidden" name="processSttusCode" value="${board.processSttusCode}"/>
										</c:when>
										<c:when test="${board.processSttusCode eq 'BBSS02'}">
											<select name="processSttusCode" class="select">
												<option value='BBSS01' selected="selected">재상신</option>
											</select>
										</c:when>
										<c:when test="${board.processSttusCode eq 'BBSS03'}">
											승인 <input type="hidden" name="processSttusCode" value="${board.processSttusCode}"/>
										</c:when>
										<c:when test="${board.processSttusCode eq 'BBSS04'}">
											기각 <input type="hidden" name="processSttusCode" value="${board.processSttusCode}"/>
										</c:when>
									</c:choose>
								</c:if>
							</td>
						</tr>
					</c:if>
				</c:when>
				<c:otherwise>
					<tr>
						<th><label for="nttSj"><spring:message code="cop.nttSj" /></label></th>
						<td>
							<form:input path="nttSj" cssClass="inp_long" />
							<br />
							<form:errors path="nttSj" />
						</td>
					</tr>
					<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
						<tr>
							<th><label for="ctgry1"><spring:message code="cop.category.view" /></label></th>
							<td>
								<select name="ctgryId" id="ctgryId" class="inp_long">
									<c:forEach var="cate" items="${boardCateList}">
										<c:if test="${cate.ctgryLevel ne 0 }">
											<option value="${cate.ctgryId}" <c:if test="${cate.ctgryId eq board.ctgryId or cate.ctgryId eq searchVO.ctgryId}"> selected="selected"</c:if>>${cate.ctgryNm}</option>
										</c:if>
									</c:forEach>
								</select>
								<script type="text/javascript">
									fnCtgryInit('${board.ctgryPathById}');
								</script>
							</td>
						</tr>
					</c:if>
					<c:if test="${brdMstrVO.bbsAttrbCode ne 'BBSA04'}">
						<tr>
							<th><label for="noticeAt"><spring:message code="cop.noticeAt" /></label></th>
							<td>
								<label for="noticeAt1"><spring:message code="button.yes" /></label> : <form:radiobutton path="noticeAt" value="Y" />
								&nbsp;
								<label for="noticeAt2"><spring:message code="button.no" /></label> : <form:radiobutton path="noticeAt" value="N" />
								<br />
								<form:errors path="noticeAt" />
							</td>
						</tr>
					</c:if>
					<c:if test="${(brdMstrVO.bbsId eq 'BBSMSTR_000000000023') or (brdMstrVO.bbsId eq 'BBSMSTR_000000000024') or (brdMstrVO.bbsId eq 'BBSMSTR_000000000025')}">
						<tr>
							<th>링크 #</th>
							<td><form:input path="tmp01" cssClass="inp_long" /></td>
						</tr>
					</c:if>
					<c:if test="${brdMstrVO.othbcUseAt eq 'Y'}">
						<tr>
							<th><label for="othbcAt"><spring:message code="cop.publicAt" /></label></th>
							<td>
								<spring:message code="cop.public" />
								:
								<form:radiobutton path="othbcAt" value="Y" />
								&nbsp;
								<spring:message code="cop.private" />
								:
								<form:radiobutton path="othbcAt" value="N" />
								<br />
								<form:errors path="othbcAt" />
							</td>
						</tr>
					</c:if>
					<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04'}">
						<tr>
							<th><label for="tmp01">Youtube<br />동영상 소스
							</label></th>
							<td>
								<form:input path="tmp01" cssClass="inp_long" />
								<br />
								<form:errors path="tmp01" />
							</td>
						</tr>
					</c:if>
				</c:otherwise>
			</c:choose>
			<tr>
				<td colspan="2">
					<form:textarea path="nttCn" rows="30" cssStyle="width:100%" />
					<br />
					<form:errors path="nttCn" />
				</td>
			</tr>
				<%--
				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<tr>
						<th><spring:message code="cop.atchFile" /></th>
						<td>
							<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
								<c:param name="editorId" value="${_EDITOR_ID}" />
								<c:param name="estnAt" value="${(brdMstrVO.bbsAttrbCode eq 'BBSA11' or brdMstrVO.bbsAttrbCode eq 'BBSA07') and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
								<c:param name="param_atchFileId" value="${board.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }" />
							</c:import>

							<noscript>
								<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
							</noscript>
						</td>
					</tr>
				</c:if>
				--%>
			</tbody>
		</table>
	</form:form>

	<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
		<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
			<c:param name="editorId" value="${_EDITOR_ID}" />
			<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
			<c:param name="param_atchFileId" value="${board.atchFileId}" />
			<c:param name="imagePath" value="${_IMG }" />
			<c:param name="potalService" value="Y" />
		</c:import>
		<noscript>
			<label for="egovComFileUploader"><spring:message code="cop.atchFileList" /></label>
			<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
		</noscript>
	</c:if>

	<div class="btn_c">
		<c:choose>
			<c:when test="${searchVO.registAction eq 'regist'}">
				<span class="cbtn1">
					<button type="button" onclick="$('#board').submit();">
						<spring:message code="button.create" />
					</button>
				</span>
			</c:when>
			<c:when test="${searchVO.registAction eq 'updt'}">
				<span class="cbtn1">
					<button type="button" onclick="$('#board').submit();">
						<spring:message code="button.update" />
					</button>
				</span>
			</c:when>
			<c:when test="${searchVO.registAction eq 'reply'}">
				<span class="cbtn1">
					<button type="button" onclick="$('#board').submit();">
						<spring:message code="button.reply" />
					</button>
				</span>
			</c:when>
		</c:choose>
		<c:url var="selectBoardListUrl" value="${_PREFIX}/selectBoardList.do">
			<c:param name="siteId" value="${searchVO.siteId}" />
			<c:param name="sysTyCode" value="${searchVO.sysTyCode}" />
			<c:param name="bbsId" value="${brdMstrVO.bbsId}" />
			<c:param name="trgetId" value="${searchVO.trgetId}" />
			<c:param name="pageIndex" value="${searchVO.pageIndex}" />
			<c:param name="searchCnd" value="${searchVO.searchCnd}" />
			<c:param name="searchWrd" value="${searchVO.searchWrd}" />
			<c:param name="searchCate" value="${searchVO.searchCate}" />
			<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
				<c:if test="${not empty searchCate}">
					<c:param name="searchCateList" value="${searchCate}" />
				</c:if>
			</c:forEach>
		</c:url>
		<span class="cbtn">
			<button type="button" onclick="location.href='${selectBoardListUrl}';">목록</button>
		</span>

	</div>
</div>

<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>
<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8" />