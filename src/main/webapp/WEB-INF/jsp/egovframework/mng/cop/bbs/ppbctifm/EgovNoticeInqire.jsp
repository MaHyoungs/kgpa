<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/cop/bbs"/>
<c:set var="serverName" value="<%=request.getServerName()%>"/>
<c:set var="serverPort" value="<%=request.getServerPort()%>"/>
<c:choose>
	<c:when test="${serverPort == 80}">
		<c:set var="serverPort" value=""/>
	</c:when>
	<c:otherwise>
		<c:set var="serverPort" value=":${serverPort}"/>
	</c:otherwise>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="siteId" value="${searchVO.siteId}"/>
	<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:param name="trgetId" value="${searchVO.trgetId}" />
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="searchCnd" value="${searchVO.searchCnd}" />
	<c:param name="searchWrd" value="${searchVO.searchWrd}" />
	<c:param name="searchCate" value="${searchVO.searchCate}" />
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
	<c:param name="title" value="${brdMstrVO.bbsNm}"/>
</c:import>

<script type="text/javascript">
	function onloading() {
		if ("<c:out value='${msg}'/>" != "") {
			alert("<c:out value='${msg}'/>");
		}
	}

	function backPage() {
		history.back(-1);
	}

	function fn_egov_delete_notice(url) {

		if (confirm('<spring:message code="common.delete.msg" />')) {
			document.location.href = url;
		}
	}

	function urlCopy() {
		var addr = "http://${serverName}${serverPort}/hpg/bbs/selectBoardArticle.do?nttNo=${searchVO.nttNo}&menuId=${menuId}&bbsId=${searchVO.bbsId}";
		window.clipboardData.setData('Text', addr);
		alert("페이지 주소가 복사되었습니다.\n" + addr);
	}

</script>
<script type="text/javaScript" src="/template/manage/js/mngworklogs.js"></script>
<div id="cntnts">

	<div class="btn_a">
		<div class="tt">
			<a href="javascript:backPage()"><img src="${_IMG}/btn/btn_back.gif" alt="뒤로" /></a>
		</div>
		<div class="aa">
			<a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}&ctgryId=${board.ctgryId}"/>"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	        <c:if test="${brdMstrVO.replyPosblAt eq 'Y'}">
	            <c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
	                <c:param name="nttNo" value="${board.nttNo}" />
		            <c:param name="registAction" value="reply" />
				</c:url>
	        </c:if>
	        <c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
	            <c:param name="nttNo" value="${board.nttNo}" />
		        <c:param name="registAction" value="updt" />
			</c:url>
	        <a href="<c:out value="${forUpdateBoardArticleUrl}&ctgryId=${board.ctgryId}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>
	        <c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
	            <c:param name="nttNo" value="${board.nttNo}" />
			</c:url>
	        <a href="<c:out value="${deleteBoardArticleUrl}"/>" onclick="fn_egov_delete_notice(this.href);return false;"><img src="${_IMG}/btn/btn_del.gif" alt="삭제"/></a>
		</div>
	</div>

	<div class="view_wrap">
		<table  class="view_writer_chart">
			<caption></caption>
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tbody>
                <tr>
	                <th>구분</th>
	                <td colspan="3" class="last">${board.ctgryNm}</td>
                </tr>
                <tr>
	                <th><spring:message code="cop.ntcrNm" /></th>
	                <td colspan="3" class="last"><c:out value="${board.ntcrNm}" /> (<c:out value="${board.frstRegisterId}" />)</td>
                </tr>
                <tr>
                	<th><spring:message code="cop.frstRegisterPnttm" /></th>
	                <td><fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                <th><spring:message code="cop.inqireCo" /></th>
	                <td class="last"><c:out value="${board.inqireCo}" /></td>
                </tr>
				<tr>
					<th>공표목록</th>
					<td colspan="3" class="last"><c:out value="${board.nttSj}"/></td>
				</tr>
				<tr>
					<th>공표항목</th>
					<td colspan="3" class="last"><c:out value="${board.tmp02}"/></td>
				</tr>
				<tr>
					<th>공표시기</th>
					<td><c:out value="${board.tmp03}"/></td>
					<th>공표주기</th>
					<td class="last"><c:out value="${board.tmp04}"/></td>
				</tr>
				<tr>
					<th>부서명</th>
					<td><c:out value="${board.tmp05}"/></td>
					<th>공표방법</th>
					<td class="last"><c:out value="${board.tmp06}"/></td>
				</tr>
                <c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<tr>
						<th><spring:message code="cop.atchFileList" /></th>
						<td colspan="3">
							<ul class="list">
								<c:if test="${not empty board.atchFileId}">
									<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
										<c:param name="param_atchFileId" value="${board.atchFileId}" />
										<c:param name="imagePath" value="${_IMG }"/>
									</c:import>
								</c:if>
							</ul>
						</td>
					</tr>
				</c:if>
 			</tbody>
		</table>
	</div>
	
	<c:if test="${brdMstrVO.commentUseAt eq 'Y'}"> 
    		<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
    			<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
    		</c:import> 
    </c:if>
    
</div>        

<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8"/>	