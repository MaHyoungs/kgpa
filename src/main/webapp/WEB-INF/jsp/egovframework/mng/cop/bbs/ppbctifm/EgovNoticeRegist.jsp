<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="_C_LIB" value="${pageContext.request.contextPath}/lib"/>
<c:set var="_C_HTML" value="${pageContext.request.contextPath}/template/common/html"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/cop/bbs"/>
<c:set var="_ACTION" value=""/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>


<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
	<c:param name="title" value="${brdMstrVO.bbsNm}"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.min.js"></script>
<script type="text/javascript" src="${_C_JS}/egovframework/cmm/sym/cal/EgovCalPopup.js" ></script>
<%--<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>--%>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">

	function fn_egov_regist() {

		if($('#nttSj').val() == ''){
			$('#nttSj').focus();
			alert($('#nttSj').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp01').val() == ''){
			$('#tmp01').focus();
			alert($('#tmp01').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp02').val() == ''){
			$('#tmp02').focus();
			alert($('#tmp02').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp03').val() == ''){
			$('#tmp03').focus();
			alert($('#tmp03').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp04').val() == ''){
			$('#tmp04').focus();
			alert($('#tmp04').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp05').val() == ''){
			$('#tmp05').focus();
			alert($('#tmp05').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}else if($('#tmp06').val() == ''){
			$('#tmp06').focus();
			alert($('#tmp06').parent().prev().find('label').text() + '은(는) 필수 입력값입니다.');
			return false;
		}

		<c:choose>
		<c:when test="${searchVO.registAction eq 'updt'}">
		if (!confirm('<spring:message code="common.update.msg" />')) {
			return false
		}
		</c:when>
		<c:otherwise>
		if (!confirm('<spring:message code="common.regist.msg" />')) {
			return false;
		}
		</c:otherwise>
		</c:choose>
	}


	$(document).ready( function() {
		var adfile_config = {
			storePath:"Board.fileStorePath",
			appendPath:"${brdMstrVO.bbsId}",
			siteId:"${searchVO.siteId}",
			editorId:"${_EDITOR_ID}"
		};

		fn_egov_bbs_editor(adfile_config);
	});
</script>

<div id="cntnts">


	<form:form commandName="board" name="board" method="post" action="${pageContext.request.contextPath}${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
		<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
		<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>" />
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
		<input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
		<input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
		<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>" />
		<input type="hidden" name="sysTyCode" value="${searchVO.sysTyCode}"/>
		<input type="hidden" name="siteId" value="${searchVO.siteId}"/>
		<input type="hidden" name="registAction" value="${searchVO.registAction}"/>
		<input type="hidden" name="trgetId" value="${searchVO.trgetId}"/>

		<form:hidden path="nttNo"/>
		<form:hidden path="ordrCode"/>
		<form:hidden path="ordrCodeDp"/>
		<form:hidden path="atchFileId"/>

		<table class="chart2" summary="작성인, 제목, 내용, 파일첨부를 입력하는 표입니다." >
			<caption> </caption>
			<colgroup>
				<col width="10%" />
				<col width="90%" />
			</colgroup>
			<tbody>
			<tr>
				<th><label for="ctgryId"구분</label></th>
				<td>
					<select name="ctgryId" id="ctgryId" class="inp_long">
						<c:forEach var="cate" items="${boardCateList}">
							<c:if test="${cate.ctgryLevel ne 0 }">
								<option value="${cate.ctgryId}" <c:if test="${cate.ctgryId eq board.ctgryId or cate.ctgryId eq searchVO.ctgryId}"> selected="selected"</c:if>>${cate.ctgryNm}</option>
							</c:if>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="nttSj">공표목록</label></th>
				<td><form:input path="nttSj" cssClass="inp_long"/><br/><form:errors path="nttSj" /></td>
			</tr>
			<tr>
				<th><label for="tmp02">공표항목</label></th>
				<td><form:input path="tmp02" cssClass="inp_long"/><br/><form:errors path="tmp02" /></td>
			</tr>
			<tr>
				<th><label for="tmp03">공표시기</label></th>
				<td><form:input path="tmp03" cssClass="inp_long"/><br/><form:errors path="tmp03" /></td>
			</tr>
			<tr>
				<th><label for="tmp04">공표주기</label></th>
				<td><form:input path="tmp04" cssClass="inp_long"/><br/><form:errors path="tmp04" /></td>
			</tr>
			<tr>
				<th><label for="tmp05">부서명</label></th>
				<td><form:input path="tmp05" cssClass="inp_long"/><br/><form:errors path="tmp05" /></td>
			</tr>
			<tr>
				<th><label for="tmp06">공표방법</label></th>
				<td><form:input path="tmp06" cssClass="inp_long"/><br/><form:errors path="tmp06" /></td>
			</tr>
			<tr style="display:none;">
				<td colspan="2">
					<form:hidden path="nttCn" rows="30" cssStyle="width:100%"/><br/><form:errors path="nttCn" />
				</td>
			</tr>
			</tbody>
		</table>
	</form:form>

	<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
		<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
			<c:param name="editorId" value="${_EDITOR_ID}"/>
			<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
			<c:param name="param_atchFileId" value="${board.atchFileId}" />
			<c:param name="imagePath" value="${_IMG }"/>
			<c:param name="potalService" value="Y" />
		</c:import>

		<noscript>
			<label for="egovComFileUploader"><spring:message code="cop.atchFileList" /></label>
			<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
		</noscript>
	</c:if>

	<div class="btn_r">
		<c:choose>
			<c:when test="${searchVO.registAction eq 'regist' }">
				<input type="image" src="${_IMG}/btn/btn_regist.gif" onclick="$('#board').submit();" alt="등록"/>
			</c:when>
			<c:when test="${searchVO.registAction eq 'updt' }">
				<input type="image" src="${_IMG}/btn/btn_modify.gif" onclick="$('#board').submit();" alt="수정"/>
			</c:when>
			<c:when test="${searchVO.registAction eq 'reply' }">
				<input type="image" src="${_IMG}/btn/btn_reply.gif" onclick="$('#board').submit();" alt="답변"/>
			</c:when>
		</c:choose>
		<c:url var="selectBoardListUrl" value="${_PREFIX}/selectBoardList.do">
			<c:param name="siteId" value="${searchVO.siteId}"/>
			<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
			<c:param name="bbsId" value="${brdMstrVO.bbsId}" />
			<c:param name="trgetId" value="${searchVO.trgetId}" />
			<c:param name="pageIndex" value="${searchVO.pageIndex}" />
			<c:param name="searchCnd" value="${searchVO.searchCnd}" />
			<c:param name="searchWrd" value="${searchVO.searchWrd}" />
			<c:param name="searchCate" value="${searchVO.searchCate}" />
			<c:if test="${not empty searchVO.ctgryId}">
				<c:param name="ctgryId" value="${searchVO.ctgryId}" />
			</c:if>
			<c:if test="${not empty board.ctgryId}">
				<c:param name="ctgryId" value="${board.ctgryId}" />
			</c:if>
			<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
				<c:if test="${not empty searchCate}">
					<c:param name="searchCateList" value="${searchCate}" />
				</c:if>
			</c:forEach>
		</c:url>
		<a href="${selectBoardListUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록" /></a>
	</div>

</div>

<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>

<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8"/>	