<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="CMMN_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="CMMN_JS" value="/template/common/js"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="BOARD_MANAGE"/>
	<c:param name="depth1" value="COMTNGN"/>
	<c:param name="title" value="녹색자료실06~09"/>
</c:import>

		<div id="content">
		<form name="comtngnVO" id="comtngnVO" method="post" action="/mng/gds/insertComtngn.do" enctype="multipart/form-data">
			<table class="chart2" summary="녹색자료등록">
			<caption>녹색자료등록</caption>
			<colgroup>
				<col width="50" />
			    <col width="70" />
			</colgroup>
			<tbody>
			<tr>
				<th><label name="title"> 제목</label></th>
				<td><input type="text" name="title" id="title"></td>
			</tr>
			<tr>
				<th><label name="org_code">기관</label></th>
				<td colspan="2">
					<select id="org_code" name="org_code" title="기관">
						<option value="">기관명</option>
						<option value="GCI001">녹색사업단</option>
						<option value="GCI002">목재문화포럼</option>
						<option value="GCI003">산림정책연구회</option>
						<option value="GCI004">한그루녹색회</option>
						<option value="GCI005">한국산지보전협회</option>
						<option value="GCI006">경향신문사</option>
						<option value="GCI007">생명의숲</option>
						<option value="GCI008">숲길</option>
						<option value="GCI009">한국녹색문화재단</option>
						<option value="GCI010">산림조합중앙회</option>
						<option value="GCI011">그린레인저</option>
						<option value="GCI012">숲과문화연구회</option>
						<option value="GCI013">한국산림휴양학회</option>
						<option value="GCI014">한국산악문화협회</option>
						<option value="GCI015">환경재단</option>
						<option value="GCI016">수목장실천회</option>
						<option value="GCI017">한국등산지원센터</option>
						<option value="GCI018">한국사회복지사협회</option>
					</select>
				</td>
			</tr>
			<tr>
				<th><label name="title_sn"> 제목번호</label></th>
				<td><input type="text" name="title_sn" id=></td>
			</tr>
			<tr>
				<th><label name="imgfile"> 표시이미지</label></th>
				<td><input type="file" name="imgfile" id="imgfile"></td>
			</tr>
			<tr>
				<th><label name="contfile"> 첨부파일</label></th>
				<td><input type="file" name="contfile" id="contfile"></td>
			</tr>
			</tbody>
		</table>

		<div class="btn_r">
			<input type="image" src="<c:url value='${_IMG}/btn/btn_regist.gif'/>" alt="등록" onclick="return Executor.doInsert(this.form);"/>
			<c:url var="listUrl" value="/mng/gds/selectMngComtngnList.do">
			</c:url>
			<a href="${listUrl}"><img src="${_IMG}/btn/btn_cancel.gif" alt="취소"/></a>
  	    </div>
	</form>
	</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>


<!-- 스트립트 영역 -->
<script type="text/javascript">

	var Executor = {

		doInsert : function(form){

			var title = form.title.value;
			var orgCode = form.org_code.value;
			var titleSn = form.title_sn.value;
			var contfile = form.contfile.value;

			if(title == null || title.length < 1 ) {
				alert("제목은 필수입력값입니다.");
				return false;
			}

			if(orgCode == "") {
				alert("기관은 필수선택사항입니다.");
				return false;
			}

			if(titleSn ==  null || titleSn.length < 1 ) {
				alert("순서는 필수입력값입니다.");
				return false;
			}

			if(contfile == null || contfile.length < 1) {
				alert("첨부파일은 필수입력값입니다.");
				return false;
			}

			if(confirm('등록하시겠습니까?')){
				return true;
			}else{
				return false;
			}
		}
	};
</script>
