<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="CMMN_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="CMMN_JS" value="/template/common/js"/>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="BOARD_MANAGE"/>
	<c:param name="depth1" value="COMTNGN"/>
	<c:param name="title" value="녹색자료실06~09"/>
</c:import>
<%--<style>
	.data_list a{cursor:pointer;}
	.data_list .hide{display:none;}
</style>--%>


	<div id="cntnts" style="padding:10px 100px 40px 100px ;">
		<div class="btn_r">
		    <c:url var="addUrl" value="/mng/gds/addComtngn.do${_PAGE_PARAM}" />
			<a href="${addUrl}"><img src="${_IMG}/btn/btn_regist_new.gif" alt="등록하기"/></a>
	    </div>

		<c:forEach var="been" items="${commonCode}" varStatus="status">
			<div <c:if test="${status.count ne 1}"> style="margin-top:60px;"</c:if>>
				<h3 class="total"><c:out value="${been.codeNm}"/></h3>
				<table summary="녹색자료실 <c:out value="${been.codeNm}"/>" class="chart_board">
					<caption class="hdn">녹색자료실 <c:out value="${been.codeNm}"/> 목록</caption>
					<colgroup>
						<col class="co2"/>
						<col width=""/>
						<col class="co2"/>
					</colgroup>
					<thead>
						<tr>
							<th>번호</th>
							<th>제목</th>
							<th>등록일</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="bbsIdx" value="1" />
						<c:forEach var="result" items="${comtngnList}">
							<c:if test="${result.org_code eq been.code}">
								<tr>
									<td>${bbsIdx}</td>
									<td class="tit">
										<a href="#" onclick="Executor.doUpdate('<c:out value="${result.gn_id}"/>', '<c:out value="${result.org_code}"/>')">
											<c:out value="${result.title}"/>
										</a>
									</td>
									<td>${result.register_date}</td>
								</tr>
								<c:set var="bbsIdx" value="${bbsIdx + 1}" />
							</c:if>
						</c:forEach>
					</tbody>
				</table>
	        </div>
		</c:forEach>
	</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>


<!-- 스크립트 영역 -->
<script type="text/javascript">

	var Executor = {

		doUpdate : function(gn_id, org_code) {

			document.location.href = "/mng/gds/selectComtngnInfo.do?gn_id="+gn_id+"&org_code="+org_code;
		}
	};
</script>