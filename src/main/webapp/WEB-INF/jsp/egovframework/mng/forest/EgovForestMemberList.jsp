<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SANRIMDB_MANAGE"/>
	<c:param name="depth1" value="MEMBER_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="한반도산림복원관리 > 회원관리"/>
</c:import>	
<c:set var="_ACTION" value="/mng/forest/usr/EgovMberManage.do" />
<script type="text/javascript">
<!--
<c:if test='${not empty frdbmessage}'>
alert("${frdbmessage}");
</c:if>

function fn_egov_delete(url){
	if(confirm('<spring:message code="common.delete.msg" />')){
		document.location.href = url;	
	}		
}
-->
</script>

<div id="cntnts">
 
	<c:if test="${USER_INFO.userSe > 10}">
		<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>	
	
		<form:form name="listForm" action="${_ACTION }" method="post">
		<input type="hidden" name="siteId" value="${searchVO.siteId}"/>
		<p class="total">총  회원 ${paginationInfo.totalRecordCount}명ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

		<table class="chart_board">
		<colgroup>
			<col width="70"/>				
			<col width="100"/>
			<col/>
			<col/>
			<col/>
			<col width="80"/>
			<col width="80"/>
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>아이디</th>
				<th>기관명</th>
				<th>이름</th>				
				<th>등록일</th>
				<th>사용여부</th>
				<th>관리</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
				<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
				<td class="listtd"><c:out value="${result.userId}" /></td>
				<td class="listtd"><c:out value="${result.organNm}" /></td>
				<td class="listtd"><c:out value="${result.userNm}" /></td>				
				<td class="listtd"><c:out value="${result.frstRegistPnttm}" /></td>
				<td class="listtd">
					<c:choose>
						<c:when test="${result.confmAt eq 'Y'}">
							<c:out value="예" />
						</c:when>
						<c:otherwise>
							<c:out value="아니오" />
						</c:otherwise>
					</c:choose>
				</td>
				<td>
					<c:url var="editUrl" value="/mng/forest/usr/EgovUserSelectUpdtView.do${_BASE_PARAM}">
						<c:param name="userId" value="${result.userId}" />
						<c:param name="userSeCode" value="${result.userSeCode}" />
						<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					</c:url>
		        	<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
		        	<c:url var="delUrl" value="/mng/forest/usr/EgovMberManageDelete.do">
		        		<c:param name="userId" value="${result.userId}" />
		        		<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					</c:url>	
		        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
			    </td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="7">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>

		</tbody>
		</table>
	
	<div class="btn_r">
       	<a href="<c:url value='/mng/forest/usr/EgovMberAddView.do'/>"><img src="${_IMG}/btn/btn_regist.gif" /></a>
	</div>

	<div id="paging">
		<c:url var="pageUrl" value="/mng/forest/usr/EgovMberManage.do${_BASE_PARAM}">
		</c:url>

		<c:if test="${not empty paginationInfo}">
			<ul>
				<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			</ul>
		</c:if>
	</div>
	
	<div id="bbs_search">
		<label for="ftext1" class="hdn">분류검색</label>
		<select name="searchCondition" id="ftext1">
			<option value="">검색항목</option>
			<option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if>>아이디</option>			
			<option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if>>이름</option>
			<option value="8" <c:if test="${searchVO.searchCondition == '8'}">selected="selected"</c:if>>기관명</option>			
  	  	</select>
  	  	<select name="searchCnd" id="ftext">
  	  		<option value="OR" <c:if test="${searchVO.searchCnd == 'OR' or searchVO.searchCnd == ''}">selected="selected"</c:if>>연관검색</option>
			<option value="AND" <c:if test="${searchVO.searchCnd == 'AND'}">selected="selected"</c:if>>일치검색</option>
		</select>
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="inp" id="inp_text" />
		<input type=image src="${_IMG}/board/btn_search.gif" alt="검색" />
	</div>

	</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	