<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SANRIMDB_MANAGE"/>
	<c:param name="depth1" value="MEMBER_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="validator" value="userManageVO"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<script type="text/javascript">

<c:if test='${not empty frdbmessage}'>
	alert("${frdbmessage}");
</c:if>

		function resetForm(form) {
			
			var frm = document.userManageVO;
			frm.reset();
			
			return false;
		}
		
		function fnCheckSpace(str){
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    ch = ch_char.charCodeAt();
			        if(ch == 32) {
			            return false;
			        }
			}
    	    return true;
    	}
		
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}
		
		function fnCheckTksu(str) { 
			for (var i=0; i < str.length; i++) {
			    ch_char = str .charAt(i);
			    ch = ch_char.charCodeAt();
		        if( !(ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64) || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126) ) {
		            
		        } else {
		        	return true;
		        }
			}
            return false;            
        }
        
        function fnCheckEnglish(str){
       		for(var i=0;i<str.length;i++){
       			var EnglishChar = str.charCodeAt(i);
       			if( !( 0x61 <= EnglishChar && EnglishChar <= 0x7A ) && !( 0x41 <= EnglishChar && EnglishChar <= 0x5A ) ) {
       				
       			} else {
		        	return true;
		        }
       		}
       		return false;
       }
       
       function fnCheckDigit(str) {  
			for (var i=0; i < str.length; i++) {
			    ch_char = str.charAt(i);
			    iValue = parseInt(ch_char);
		        if(isNaN(iValue)) {
		           
		        } else {
		        	return true;
		        }
			}
            return false;
        }
       
       function checkForm(form) {
			
			var userNm = form.userNm.value;
			if (userNm.length < 1) {
				alert("이름을 입력해야 합니다.");
				return false;
			}
			else if (!fnCheckSpace(userNm) || fnCheckDigit(userNm) || fnCheckTksu(userNm)) {
				alert("이름은 띄어쓰기 없는 영문 또는 한글로 입력해야 합니다.");
				return false;
			}
			
			// 기관명은 유효성 검사를 하지 않음
			var organNm = form.organNm.value;
			if (organNm.length < 1) {
				alert("기관명을 입력해야 합니다.");
				return false;
			}
			
			if (!(document.getElementById('confm_yes').checked || document.getElementById('confm_no').checked)) {
				alert("사용 여부를 선택해야 합니다.");
				return false;
			}
	
			if (confirm('<spring:message code="common.update.msg" />')) {	
				return true;
			} else {
				return false;
			}
		}
       
       function initPassword(form) {
    	   if(confirm("비밀번호를 재발급 하시겠습니까?")) {
		        document.userManageVO.action = "${pageContext.request.contextPath}/mng/forest/usr/InitPassword.do";
		        document.userManageVO.target = "passSand";
		        return true;
		    }else{
		    	return false;
			}
       }
		
	</script>
<div id="cntnts">

	<form:form commandName="userManageVO" name="userManageVO" method="post" action="${pageContext.request.contextPath}/mng/forest/usr/EgovUserSelectUpdt.do"> 
		<input type="hidden" name="searchSe" value="<c:out value='${searchVO.searchSe}'/>"/>
		<input type="hidden" name="searchCnd" value="<c:out value='${searchVO.searchCnd}'/>"/>
		<input type="hidden" name="searchCondition" value="<c:out value='${searchVO.searchCondition}'/>"/>
		<input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>"/>
		<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>
		
		<input type="hidden" id="checkId" name="checkId" value=""/>
		
			
		<table class="chart2">
			<caption>관리자관리 폼</caption>
			<colgroup>
				<col width="150"/>
				<col width=""/>
			</colgroup>
			<tbody>
			<tr>
				<th><em>*</em><label for="userId"> 아이디</label></th>
				<td><form:input path="userId" id="userId" cssClass="inp" maxlength="20" value="${userManageVO.userId}" readonly="true" /></td>
			</tr>
			<tr>
				<th><em>*</em><label for="password"> 비밀번호</label></th>
				<td>
					<input type="image" src="<c:url value='${MNG_IMG}/btn/btn_pass_reissue.gif'/>" alt="비밀번호재발급" onclick="return initPassword(document.userManageVO);"/> <strong class="org">* 비밀번호를 아이디로 초기화 합니다.</strong>
				</td>
			</tr>
			<tr>
				<th><em>*</em><label for="userNm"> 이름</label></th>
				<td>
					<form:input path="userNm" id="userNm" cssClass="inp" value="${userManageVO.userNm}"/>
				</td>
			</tr>
			<tr>
				<th><em>*</em><label for="organNm"> 기관명</label></th>
				<td>
					<form:input path="organNm" id="organNm" cssClass="inp" value="${userManageVO.organNm }"/>
				</td>
			</tr>
			<tr>
				<th><em>*</em><label for="사용여부"> 사용여부</label></th>
				<td>
					<form:radiobutton path="confmAt" id="confm_yes" value="Y" cssClass="cho" /> <label for="confm_yes">예</label>
					<form:radiobutton path="confmAt" id="confm_no" value="N" cssClass="cho" /> <label for="confm_no">아니오</label>
				</td>
			</tr>
			<tr>
				<th><label for="등록자ID"> 등록자ID</label></th>
				<td><c:out value="${userManageVO.frstRegisterId}"/></td>
			</tr>
			<tr>
				<th><label for="등록일시"> 등록일시</label></th>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${userManageVO.frstRegistPnttm}"/></td>
			</tr>
			<tr>
				<th><label for="수정자ID"> 수정자ID</label></th>
				<td><c:out value="${userManageVO.lastUpdusrId}"/> </td>
			</tr>
			<tr>
				<th><label for="수정일시"> 수정일시</label></th>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${userManageVO.lastUpdusrPnttm}"/></td>
			</tr>
			</tbody>
		</table>
		
		<div class="btn_r">
			<c:url var="listUrl" value="/mng/forest/usr/EgovMberManage.do">
				<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
				<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
			</c:url>
			<input type="image" src="${MNG_IMG}/btn/btn_modify.gif" alt="수정하기" onclick="return checkForm(document.userManageVO);"/>
			<input type="image" src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소하기" onclick="return resetForm();"/>
			<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록으로"/></a>						
		</div>
	</form:form>
	<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	