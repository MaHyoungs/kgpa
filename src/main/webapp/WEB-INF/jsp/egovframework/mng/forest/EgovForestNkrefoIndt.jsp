<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>

<%-- 게시판 소스의 설정변수명과 동일하게 사용 --%>
<c:set var="posblAtchFileNumber" value="10"/>
<c:set var="posblAtchFileSize" value="100"/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SANRIMDB_MANAGE"/>
	<c:param name="depth1" value="RESOURCE_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="한반도산림복원관리 > 자료관리"/>
</c:import>	

<link type="text/css" rel="stylesheet" href="<c:url value='${_C_CSS}/nkrefo.css'/>"/>
<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.min.js" ></script>
<script type="text/javascript" src="${_C_JS}/nkrefo.js" ></script>

<script type="text/javascript">
<c:if test='${not empty frdbmessage}'>
	alert("${frdbmessage}");
</c:if>
	
		function resetForm(form) {
	
			var frm = document.nkrefoManageVO;
			frm.reset();
	
			return false;
		}
	
		function fnCheckDate(str) {
			var format = /^(19[7-9][0-9]|20\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
			if (!format.test(str)) return false;
			else return true;
		}
		
		function checkForm(form) {

			if(fn_text_null_check($('input[type=text], textarea, input[type=hidden].null_false'))){

				if(form.dataForm.value == ''){
					alert('자료형태를 선택해야 합니다.');
					return false;
				}

				if(form.bodyLang.value == ''){
					alert('본문언어를 선택해야 합니다.');
					return false;
				}

				if(!fnCheckDate(form.pubYear.value)){
					alert('발행일자는 YYYY-MM-DD 형식으로 입력하셔야 합니다.');
					return false;
				}

				if(!(document.getElementById('use_yes').checked || document.getElementById('use_no').checked)){
					alert("사용 여부를 선택해야 합니다.");
					return false;
				}

				if(!confirm('<spring:message code="common.regist.msg" />')){
					return false;
				}

				document.nkrefoManageVO.submit();
			}
		}
</script>
<div id="cntnts">

	<form:form commandName="nkrefoManageVO" name="nkrefoManageVO" method="post" action="/mng/forest/nkrefo/EgovNkrefInsert.do" enctype="multipart/form-data"> 
		<input type="hidden" name="searchSe" value="<c:out value='${searchVO.searchSe}'/>"/>
		<input type="hidden" name="searchCnd" value="<c:out value='${searchVO.searchCnd}'/>"/>
		<input type="hidden" name="searchCondition" value="<c:out value='${searchVO.searchCondition}'/>"/>
		<input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>"/>
		<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>
				
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${posblAtchFileNumber}" />
        <input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${posblAtchFileSize * 1024 * 1024}" />
        <input type="hidden" id="fileGroupId" name="fileGroupId" value="${nkrefoManageVO.atchFileId}"/>
		
		<table class="chart2">
			<caption>관리자관리 폼</caption>
			<colgroup>
				<col width="150"/>
				<col width=""/>
			</colgroup>
			<tbody>
			<tr>
				<th><label for="title"> 표제</label></th>
				<td>
					<form:input path="title" id="title" cssClass="inp null_false" maxlength="200" title="표제" style="width:500px"/>
				</td>
			</tr>
			<tr>
				<th><label for="author"> 저자</label></th>
				<td>
					<form:input  path="author" id="author" cssClass="inp" maxlength="200" title="저자" style="width:500px"/>
				</td>
			</tr>
			<tr>
				<th><label for="publisher"> 발행자</label></th>
				<td>
					<form:input path="publisher" id="publisher" cssClass="inp" maxlength="200" title="발행자" style="width:500px"/>
				</td>
			</tr>
			<tr>
				<th><label for="tag"> 키워드(태그)</label></th>
				<td>
					콤마로 구분하여 넣어주세요.<br />
					<form:input path="tag" id="tag" cssClass="inp" title="키워드(태그)" style="width:500px"/>
				</td>
			</tr>
			<tr>
				<th><label for="dataForm"> 자료형태</label></th>
				<td>
					<select name="dataForm" id="dataForm">
							<option value="">자료형태</option>
						<c:forEach var="dataForm" items="${dataFormList}">
							<option value="${dataForm.code}"><c:out value="${dataForm.codeNm}" /></option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="bodyLang"> 본문언어</label></th>
				<td>
					<select name="bodyLang" id="bodyLang">
							<option value="">본문언어</option>
						<c:forEach var="bodyLang" items="${bodyLangList}">
							<option value="${bodyLang.code}"><c:out value="${bodyLang.codeNm}" /></option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="summary"> 요약내용</label></th>
				<td>
					<form:textarea path="summary" id="summary" cols="81" rows="10" style="border:1px solid #c9c9c9;"/>
				</td>
			</tr>
			<tr>
				<th><label for="pubYear"> 발행일자</label></th>
				<td>
					<form:input path="pubYear" id="pubYear" cssClass="inp" title="발행일자" maxlength="10"/>
				</td>
			</tr>
			<tr>
				<th><label for="useAt"> 사용여부</label></th>
				<td>
					<form:radiobutton path="useAt" id="use_yes" value="Y" cssClass="cho" checked="checked"/> <label for="use_yes">예</label>
					<form:radiobutton path="useAt" id="use_no" value="N" cssClass="cho"/> <label for="use_no">아니오</label>
				</td>
			</tr>
			</tbody>
		</table>
	</form:form>
	
	<c:import url="/cmm/fms/nkrefo/selectFileInfsForUpdate.do" charEncoding="utf-8">
		<c:param name="editorId" value="${_EDITOR_ID}"/>
		<c:param name="estnAt" value="N" />
		<c:param name="param_atchFileId" value="${nkrefoManageVO.atchFileId}" />
		<c:param name="imagePath" value=""/>
		<c:param name="potalService" value="Y" />
	</c:import>
	
	<div class="btn_r">
		<c:url var="listUrl" value="/mng/forest/nkrefo/EgovNkrefoManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<input type="image" src="${MNG_IMG}/btn/btn_regist.gif" alt="등록하기" onclick="return checkForm(document.nkrefoManageVO);"/>
		<input type="image" src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소하기" onclick="return resetForm();"/>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록으로"/></a>						
	</div>
	
	<script type="text/javascript">
		(function(){
			var progress=$('.progress');
			var bar = $('.bar');
			$('#nkrefoFileAjaxForm').ajaxForm({
				dataType: 'json',
				beforeSend: function(){
					progress.show();
					var percentVal='0%';
					progress.width(percentVal);
					bar.html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete){
					var percentVal=percentComplete-1 + '%';
					progress.width(percentVal);
					bar.html(percentVal);
				},
				success: function(json){
					if(json.rs == 'countOver'){
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
					}else if(json.rs == 'overflow'){
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
					}else if (json.rs == 'denyExt') {
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부할 수 없는 확장자입니다.");
					}else{
						var percentVal='100%';
						progress.width("99%");
						bar.html(percentVal);
						fnAjaxFileUploadComplete(json);
					}
				},
				complete: function(xhr){
					var file=$('#nkrefoFileAjaxForm input[name=uploadfile]').clone();
					var file_parent = $('#nkrefoFileAjaxForm input[name=uploadfile]').parent();
					$('#nkrefoFileAjaxForm input[name=uploadfile]').remove();
					file_parent.prepend(file);
				}
			});
		})();
	</script>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	