<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SANRIMDB_MANAGE"/>
	<c:param name="depth1" value="RESOURCE_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="한반도산림복원관리 > 자료관리"/>
</c:import>	

<script type="text/javascript">
<c:if test='${not empty frdbmessage}'>
	alert("${frdbmessage}");
</c:if>	
</script>

<div id="cntnts">
	
	<table class="chart2">
		<caption>관리자관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
		</colgroup>
		<tbody>
		<tr>
			<th>표제</th>
			<td>
				<%-- <input type="text" cssClass="inp" style="width:500px" readonly="readonly" value="<c:out value="${nkrefoManageVO.title}" escapeXml="false"/>" > --%>
				<c:out value="${nkrefoManageVO.title}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>저자</th>
			<td>
				<%-- <input type="text" cssClass="inp" style="width:500px" readonly="readonly" value="<c:out value="${nkrefoManageVO.author}" escapeXml="false"/>" > --%>
				<c:out value="${nkrefoManageVO.author}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>발행자</th>
			<td>
				<%-- <input type="text" cssClass="inp" style="width:500px" readonly="readonly" value="<c:out value="${nkrefoManageVO.publisher}" escapeXml="false"/>" > --%>
				<c:out value="${nkrefoManageVO.publisher}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>키워드(태그)</th>
			<td>
				<%-- <input type="text" cssClass="inp" style="width:500px" readonly="readonly" value="<c:out value="${nkrefoManageVO.tag}" escapeXml="false"/>" >--%>
				<c:out value="${nkrefoManageVO.tag}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>자료형태</th>
			<td>
				<c:forEach var="dataForm" items="${dataFormList}">
					<c:if test="${dataForm.code eq nkrefoManageVO.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
				</c:forEach>
			</td>
		</tr>
		<tr>
			<th>본문언어</th>
			<td>
				<c:forEach var="bodyLang" items="${bodyLangList}">
					<c:if test="${bodyLang.code eq nkrefoManageVO.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
				</c:forEach>
			</td>
		</tr>
		<tr>
			<th>요약내용</th>
			<td>
				<%-- 
				<textarea cols="81" rows="10" style="border:1px solid #c9c9c9;" readonly="readonly" >
					<c:out value="${nkrefoManageVO.summary}" escapeXml="false"/>
				</textarea>
				--%>
				<c:out value="${fn:replace(nkrefoManageVO.summary, newLineChar, '<br/>')}" escapeXml="false"/>
			</td>
		</tr>
		<tr>
			<th>발행일자</th>
			<td>
				<%-- <input type="text" cssClass="inp" readonly="readonly" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${nkrefoManageVO.pubYear}"/>" > --%>
				<fmt:formatDate pattern="yyyy-MM-dd" value="${nkrefoManageVO.pubYear}"/>
			</td>
		</tr>
		<tr>
			<th>사용여부</th>
			<td>
				<c:choose>
					<c:when test="${nkrefoManageVO.useAt eq 'Y'}">
						<c:out value="예" />
					</c:when>
					<c:otherwise>
						<c:out value="아니오" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<th>첨부파일</th>
			<td>
				<c:import url="/cmm/fms/nkrefo/selectFileInfs.do" charEncoding="utf-8">
					<c:param name="param_atchFileId" value="${nkrefoManageVO.atchFileId}" />
					<c:param name="imagePath" value="${_IMG }" />
				</c:import>
			</td>
		</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="/mng/forest/nkrefo/EgovNkrefoManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록으로"/></a>						
	</div>

</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	