<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SANRIMDB_MANAGE"/>
	<c:param name="depth1" value="RESOURCE_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="한반도산림복원관리 > 자료관리"/>
</c:import>	
<c:set var="_ACTION" value="/mng/forest/nkrefo/EgovNkrefoManage.do" />

<script type="text/javascript">
<!--
<c:if test='${not empty frdbmessage}'>
alert("${frdbmessage}");
</c:if>

function checkForm(form){
	form.searchKeyword.value = encodeURIComponent(form.userKeyword.value);

	form.submit();
}

function fn_egov_delete(url){
	if(confirm('<spring:message code="common.delete.msg" />')){
		document.location.href = url;	
	}		
}

function fncExcelUpload() {
	var win = window.open('/mng/forest/nkrefo/EgovNkrefoExcelUploadView.do?siteId=<c:out value="${searchVO.siteId}" />' ,'user',' scrollbars=yes, resizable=yes, left=0, top=0, width=1100,height=400');
	if(win != null) {
		win.focus();
	}
}
-->
</script>

<div id="cntnts">
 
	<c:if test="${USER_INFO.userSe > 10}">
		<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>
	
	<form:form name="listForm" action="${_ACTION }" method="post">
		<input type="hidden" name="siteId" value="${searchVO.siteId}"/>
		<p class="total">총  자료수 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
		<table class="chart_board">
			<colgroup>
				<col width="50"/>
				<col width="*"/>
				<col width="100"/>
				<col width="100"/>
				<col width="200"/>
				<col width="70"/>
				<col width="80"/>
				<col width="80"/>
				<col width="60"/>
				<col width="90"/>
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>표제</th>
					<th>저자</th>
					<th>발행자</th>
					<th>키워드(태그)-콤마구분</th>
					<th>자료형태</th>
					<th>본문언어</th>
					<th>발행일자</th>
					<th>사용여부</th>
					<th>관리</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<td class="listtd">
							<fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/>
						</td>
						<td class="listtd" style="text-align:left;">
							<c:url var="detailUrl" value="/mng/forest/nkrefo/EgovNkrefoSelectView.do${_BASE_PARAM}">
								<c:param name="frId" value="${result.frId}" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${detailUrl}"><c:out value="${result.title}" escapeXml="false" /></a>
						</td>
						<td class="listtd"><c:out value="${result.author}" escapeXml="false" /></td>
						<td class="listtd"><c:out value="${result.publisher}" escapeXml="false" /></td>
						<td class="listtd"><c:out value="${result.tag}" escapeXml="false" /></td>
						<td class="listtd">
							<c:forEach var="dataForm" items="${dataFormList}">
								<c:if test="${dataForm.code eq result.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
							</c:forEach>
						</td>
						<td class="listtd">
							<c:forEach var="bodyLang" items="${bodyLangList}">
								<c:if test="${bodyLang.code eq result.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
							</c:forEach>
						</td>
						<td class="listtd"><fmt:formatDate pattern="yyyy-MM-dd" value="${result.pubYear}"/></td>
						<td class="listtd">
							<c:choose>
								<c:when test="${result.useAt eq 'Y'}">
									<c:out value="예" />
								</c:when>
								<c:otherwise>
									<c:out value="아니오" />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:url var="viewUrl" value="/mng/forest/nkrefo/EgovNkrefoSelectView.do${_BASE_PARAM}">
								<c:param name="frId" value="${result.frId}" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${viewUrl}"><img src="${_IMG}/btn/btn_select.gif"/></a>
							<c:url var="editUrl" value="/mng/forest/nkrefo/EgovNkrefoSelectUpdtView.do${_BASE_PARAM}">
								<c:param name="frId" value="${result.frId}" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
							<c:url var="delUrl" value="/mng/forest/nkrefo/EgovNkrefoManageDelete.do">
								<c:param name="frId" value="${result.frId}" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="listtd" colspan="10">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
			</tbody>
		</table>

		<div class="btn_r">
			<a href="#" onclick="fncExcelUpload();return false;"><img src="${_IMG}/btn_nkrefo_excel.gif" alt="엑셀자료 일괄등록하기" /></a>
	       	<a href="<c:url value='/mng/forest/nkrefo/EgovNkrefoAddView.do'/>"><img src="${_IMG}/btn/btn_regist.gif" /></a>
		</div>

		<div id="paging">
			<c:url var="pageUrl" value="/mng/forest/nkrefo/EgovNkrefoManage.do${_BASE_PARAM}">
			</c:url>
	
			<c:if test="${not empty paginationInfo}">
				<ul>
					<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				</ul>
			</c:if>
		</div>

		<div id="bbs_search">
			<label for="ftext1" class="hdn">검색항목</label>
			<select name="searchCondition" id="ftext1">
				<option value="">검색항목</option>
				<option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if>>표제</option>			
				<option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if>>저자</option>
				<option value="2" <c:if test="${searchVO.searchCondition == '2'}">selected="selected"</c:if>>발행자</option>			
				<option value="3" <c:if test="${searchVO.searchCondition == '3'}">selected="selected"</c:if>>키워드(태그)</option>
				<option value="4" <c:if test="${searchVO.searchCondition == '4'}">selected="selected"</c:if>>자료형태</option>
				<option value="5" <c:if test="${searchVO.searchCondition == '5'}">selected="selected"</c:if>>본문언어</option>
				<option value="7" <c:if test="${searchVO.searchCondition == '7'}">selected="selected"</c:if>>발행일자</option>
	  	  	</select>
			<label for="inp_text" class="hdn">검색어입력</label>
			<input type="hidden" id="searchKeyword" name="searchKeyword" value="" />
			<input type="text" id="userKeyword" name="userKeyword" value="<c:out value="${searchVO.decodedKeyword}"/>" class="inp" id="inp_text" />
			<input type=image src="${_IMG}/board/btn_search.gif" alt="검색" onclick="return checkForm(document.listForm);"/>
		</div>
	</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>