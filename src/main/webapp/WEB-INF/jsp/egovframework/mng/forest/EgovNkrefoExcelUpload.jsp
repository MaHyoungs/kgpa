<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_IMG" value="/template/member/images"/>
<c:set var="_CSS" value="/template/member/css"/>
<c:set var="C_JS" value="/template/common/js"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="stylesheet" href="${_CSS}/login.css" type="text/css" charset="utf-8" /> 
  <style>
	.total{background:url('/template/common/images/page/board/icon_balloon.gif') 0 center no-repeat;padding-left:18px;font-size:11px;color:#999999;letter-spacing:-0.05em;margin-bottom:10px;}
	.chart_board{margin-top:30px;margin-bottom:50px;width:100%;border-collapse:collapse;color:#666666; background:url('/template/common/images/page/board/bg_board_tit2.gif') repeat-x 0 0;}
	.chart_board img {vertical-align:middle;}
	.chart_board thead th{ text-align:center; padding:10px 0; color:#657591; font-weight:bold; }
	.chart_board thead th.first{background:none;border-left:1px solid #e4e4e4;}
	.chart_board thead th.last{background:none;border-right:1px solid #e4e4e4;}
	.chart_board tbody td{background:none;border-bottom:1px dotted #cbcbcb;border-right:1px dotted #cbcbcb;padding:5px 0; text-align:center;word-break:normal;}
	.chart_board tbody td.org{background:none;color: #EE8342;font-size: 11px;border-right:0px; text-align:center;}
	.chart_board tbody td a{color:#666666;}
	.chart_board tbody td.tit{text-align:left}
	.chart_board tbody tr.deleted a{color:#dfdfdf;text-decoration: line-through;}
  	/*.org {color: #EE8342;font-size: 11px;border-right:0px;}*/
  </style>
  <script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
<title>한반도산림복원자료 일괄등록</title>
<style type="text/css">
	body{background:none;}
</style>
<script type="text/javascript">

function fn_egov_regist() {
	if($('#siteId').val() == '') {
		alert('사이트가 선택되지 않았습니다. 창을 닫고 다시 시도해주세요');
		return false;
	}
	
	if($('#excelFile').val() == '') {
		alert('파일을 첨부해주세요');
		return false;
	}
	
	if($('#excelFile').val().toUpperCase().indexOf('.XLS') != -1 || $('#excelFile').val().toUpperCase().indexOf('.XLSX') != -1) {
		return true;
	} else {
		alert("파일형식이 올바르지 않습니다('.xls','.xlsx')");
		return false;
	}
}

</script>
</head>

 <body>

		<div class="pop_tit">
			<h1>자료 일괄등록</h1>
		</div>
	
		<div class="pop_con">
			
			<form name="checkForm" action="<c:url value='/mng/forest/nkrefo/EgovMberExcelUpload.do'/>" method="post" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
				<input type="hidden" id="siteId"  name="siteId" value="<c:out value="${searchVO.siteId}"/>" />
				<fieldset>
					<legend>자료 일괄등록</legend>
					
					<div class="pop_gbox" style="width:100%;height:80px;background-color:#F6F7F8;background-image:none;background-repeat:repeat;margin-bottom:10px;margin-top:0px">
						
						<c:choose>
			                <c:when test="${empty message}">
			                	<div class="id_no">
									<p><span style="color:#ee8342;">엑셀파일을 첨부해주세요</span></p>
									<p>양식이 없으시면 하단 양식을 <a href="/template/manage/nkrefoSample.xlsx"><span style="color:#ee8342;">다운로드</span></a> 하시기바랍니다.</p>
									<p>엑셀의 첫번째 라인은 필드정보를 표기한 것이며 저장하지 않습니다. 마지막 라인 다음 라인의 첫번째 셀에"END" 를 기입하여 마지막임을 표시합니다.</p>
									<p>양식대로 작성하여 올려주시기 바랍니다.</p>
								</div>
			                </c:when>
			                <c:otherwise>
			                	<div class="id_no">
									<p><span style="color:#ee8342;"><c:out value="${message}"/></span></p>
								</div>
			                </c:otherwise>
		                </c:choose>

					</div>
					
					<div class="id_chk" style="width:780px;">
						<span><a href="/template/manage/nkrefoSample.xlsx"><strong><img src="/template/manage/images/btn/xls.gif" style="text-align:center;"/> 양식</strong></a></span>
						<input type="file" class="inp" id="excelFile" name="excelFile"/>
						<span class="btn_s"><button type="submit">확인</button></span>
					</div>
				 <%--  wdith 1100 height 400  --%>
					<c:if test="${fn:length(dataList) >= 0}">
						<table class="chart_board" summary="">
							<caption class="hdn">결과 목록</caption>
								<col width="120px" />
								<col width="80px" />
								<col width="100px" />
								<col width="130px" />
								<col width="80px" />
								<col width="80px" />
								<col width="260px" />
								<col width="80px" />
								<col width="70px" />
								<col width="100px" />
							</colgroup>
							<thead>
								<tr>
									<th>표제</th>
									<th>저자</th>
									<th>발행자</th>
									<th>태그(키워드)</th>
									<th>자료형태</th>
									<th>본문언어</th>
									<th>요약내용</th>
									<th>발행일자</th>
									<th>사용여부</th>
									<th>오류메시지</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="result" items="${dataList}" varStatus="status">
									<tr>
										<td><c:out value="${result.title}" escapeXml="false"/></td>
										<td><c:out value="${result.author}" escapeXml="false"/></td>
										<td><c:out value="${result.publisher}" escapeXml="false"/></td>
										<td><c:out value="${result.tag}" escapeXml="false"/></td>
										<td><c:out value="${result.dataForm}"/></td>
										<td><c:out value="${result.bodyLang}"/></td>
										<td><c:out value="${result.summary}" escapeXml="false"/></td>
										<td><fmt:formatDate pattern="yyyy-MM-dd" value="${result.pubYear}"/></td>
										<td><c:out value="${result.useAt}"/></td>
										<td class="org"><c:out value="${result.message}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</fieldset>
			</form>
			
		</div>

		<button class="pop_mclose" onclick="window.close(); return false;">닫기</button>


 </body>
</html>
