<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:if test="${empty announcementVo}">
	<c:set var="formType" value="등록" />
</c:if>
<c:if test="${not empty announcementVo}">
	<c:set var="formType" value="수정" />
</c:if>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="ANNOUNCEMENT_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업공고관리(${formType})"/>
</c:import>

<script type="text/javascript">
	fnDatepickerOptionAdd();
	$(function() {
		$("#propse_papers_rcept_bgnde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
		$("#propse_papers_rcept_endde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
		$("#last_biz_plan_regist_bgnde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
		$("#last_biz_plan_regist_endde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });

		$('#year').change(function(){
			fnAjaxAnnouncementOverlapCheck("year");
		});

		$('#biz_ty_code').change(function(){
			fnAjaxAnnouncementOverlapCheck("biz_ty_code");
		});
	});

	//사업공고 중복 작성 체크
	function fnAjaxAnnouncementOverlapCheck(el){
		var url = "/mng/gfund/biz/ancmt/ajaxAnnouncementOverlapCheck.do";
		var data = [];
		data = {year: $('#year').val(), biz_ty_code: $('#biz_ty_code').val()};
		var successFn = function(json){
			if(json.rs == 'false'){
				if(el == 'biz_ty_code'){
					alert('선택하신 기준년도의 사업분류는 이미 작성되어, 중복 됩니다.');
					$('#biz_ty_code').val('');
				}else{
					alert('선택하신 기준년도와 사업분류는 이미 작성되어, 중복 됩니다.');
					$('#year').val('');
					$('#biz_ty_code').val('');
				}
			}
		};
		fn_ajax_json(url, data, successFn, null);
	}
</script>

<c:if test="${empty announcementVo}">
	<c:set var="formAction">/mng/gfund/biz/ancmt/announcementInsert.do</c:set>
</c:if>
<c:if test="${not empty announcementVo}">
	<c:set var="formAction">/mng/gfund/biz/ancmt/announcementUpdate.do</c:set>
</c:if>

<div id="cntnts">
	<form name="yearForm" id="yearForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">
		<input type="hidden" name="atch_file_id" id="atch_file_id" class="inp" value="${announcementVo.atch_file_id}" title="파일첨부" />
		<c:if test="${empty announcementVo}">
			<input type="hidden" name="frst_register_id" id="frst_register_id" value="${USER_INFO.id}" />
		</c:if>
		<c:if test="${not empty announcementVo}">
			<input type="hidden" name="last_updusr_id" id="last_updusr_id" value="${USER_INFO.id}" />
		</c:if>

		<table class="chart2">
			<colgroup>
				<col class="co1"/>
				<col class="co3"/>
			</colgroup>
			<caption class="hdn">사업공고관리</caption>
			<tbody>
			<tr>
				<th>
					<label for="year">* 기준년도</label>
				</th>
				<td>
					<select name="year" id="year" class="inp null_false" title="기준년도선택">
						<option value="">선택</option>
						<c:forEach var="rs" items="${yearList}" varStatus="status">
							<option value="${rs.year}" <c:if test="${announcementVo.year eq rs.year}"> selected="selected"</c:if>  <c:if test="${param.year eq rs.year}"> selected="selected"</c:if> >${rs.year}년</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					<label for="biz_ty_code">* 사업분류</label>
				</th>
				<td>
					<select name="biz_ty_code" id="biz_ty_code" class="inp null_false" title="사업분류">
						<option value="">선택</option>
						<c:forEach var="rs" items="${cmmCodeList}" varStatus="status">
							<option value="${rs.code}"<c:if test="${announcementVo.biz_ty_code eq rs.code}"> selected="selected"</c:if>>${rs.codeNm}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					<label for="propse_papers_rcept_bgnde">* 접수기간</label>
				</th>
				<td>
					<input type="text" name="propse_papers_rcept_bgnde" id="propse_papers_rcept_bgnde" class="inp null_false dates" readonly="readonly" value="${announcementVo.propse_papers_rcept_bgnde}" title="접수기간"/>
					<a href="#" onclick="$('#propse_papers_rcept_bgnde').focus();">
						<img align="middle" title="게시기간 시작달력" alt="접수기간 시작달력" style="border:0px" src="/template/common/images/egovframework/cmm/sym/cal/bu_icon_carlendar.gif"></img>
					</a>
					~ <input type="text" name="propse_papers_rcept_endde" id="propse_papers_rcept_endde" class="inp null_false dates" readonly="readonly" value="${announcementVo.propse_papers_rcept_endde}" title="접수기간"/>
					<a href="#" onclick="$('#propse_papers_rcept_endde').focus();">
						<img align="middle" title="게시기간 시작달력" alt="접수기간 시작달력" style="border:0px" src="/template/common/images/egovframework/cmm/sym/cal/bu_icon_carlendar.gif"></img>
					</a>
				</td>
			</tr>
			<tr>
				<th>
					<label for="last_biz_plan_regist_bgnde">* 최종제출기간</label>
				</th>
				<td>
					<input type="text" name="last_biz_plan_regist_bgnde" id="last_biz_plan_regist_bgnde" class="inp null_false dates" readonly="readonly" value="${announcementVo.last_biz_plan_regist_bgnde}" title="최종제출기간"/>
					<a href="#" onclick="$('#last_biz_plan_regist_bgnde').focus();">
						<img align="middle" title="게시기간 시작달력" alt="최종제출기간 시작달력" style="border:0px" src="/template/common/images/egovframework/cmm/sym/cal/bu_icon_carlendar.gif"></img>
					</a>
					~ <input type="text" name="last_biz_plan_regist_endde" id="last_biz_plan_regist_endde" class="inp null_false dates" readonly="readonly" value="${announcementVo.last_biz_plan_regist_endde}" title="최종제출기간"/>
					<a href="#" onclick="$('#last_biz_plan_regist_endde').focus();">
						<img align="middle" title="게시기간 시작달력" alt="최종제출기간 시작달력" style="border:0px" src="/template/common/images/egovframework/cmm/sym/cal/bu_icon_carlendar.gif"></img>
					</a>
				</td>
			</tr>
			<tr>
				<th><label for="biz_cn">* 사업내용</label></th>
				<td>
					<textarea name="biz_cn" id="biz_cn" class="inp null_false" style="width:99%; height:200px" title="사업내용">${announcementVo.biz_cn}</textarea>
				</td>
			</tr>
			<tr>
				<th>* 제안서접수여부</th>
				<td>
					<label for="propse_rcept_at_y">사용</label>
					<input type="radio" name="propse_rcept_at" id="propse_rcept_at_y" value="Y" <c:if test="${announcementVo.propse_rcept_at eq 'Y'}"> checked="checked"</c:if><c:if test="${empty announcementVo.propse_rcept_at}"> checked="checked"</c:if>/>
					<label for="propse_rcept_at_n">미사용</label>
					<input type="radio" name="propse_rcept_at" id="propse_rcept_at_n" value="N" <c:if test="${announcementVo.propse_rcept_at eq 'N'}"> checked="checked"</c:if>/>
				</td>
			</tr>
			<tr>
				<th>* 사용여부</th>
				<td>
					<label for="use_at_y">사용</label>
					<input type="radio" name="use_at" id="use_at_y" value="Y" <c:if test="${announcementVo.use_at eq 'Y'}"> checked="checked"</c:if><c:if test="${empty announcementVo.use_at}"> checked="checked"</c:if>/>
					<label for="use_at_n">미사용</label>
					<input type="radio" name="use_at" id="use_at_n" value="N" <c:if test="${announcementVo.use_at eq 'N'}"> checked="checked"</c:if>/>
				</td>
			</tr>
			</tbody>
		</table>
	</form>

	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${announcementVo.atch_file_id}" />
		<c:param name="updateFlag" value="Y" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="announcemen" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
		<c:param name="formAjaxJs" value="add" />
	</c:import>
	<%-- 파일첨부 Import --%>
</div>

<div class="btn_c">
	<span class="cbtn2">
		<button type="button" onclick="$('#yearForm').submit();"><c:if test="${empty announcementVo}">등록</c:if><c:if test="${not empty announcementVo}">수정</c:if></button>
	</span>
	<span class="cbtn1">
		<a href="/mng/gfund/biz/ancmt/announcementList.do<c:if test="${not empty announcementVo}">?year=${announcementVo.year}</c:if>">취소</a>
	</span>
</div>

<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>