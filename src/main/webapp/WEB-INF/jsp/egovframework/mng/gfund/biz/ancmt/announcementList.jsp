<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="ANNOUNCEMENT_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업공고관리"/>
</c:import>

<script>
	function fnAnnouncementDelete() {
		return confirm("<spring:message code="common.delete.msg" />");
	}
</script>

<div id="cntnts">
	<form id="announcementListForm" name="announcementListForm" action="" method="post">
		<div class="select_bar">
			<span class="cbtn1" style="float:left;">
				<a href="/mng/gfund/biz/year/yearList.do" onclick="fn_open_popup($(this).attr('href'), 'yearPopup', '600', '400', '0', '0'); return false;">년도관리</a>
			</span>


			<strong>년도선택</strong>
			<select id="year" name="year" title="년도선택" onchange="announcementListForm.submit();">
				<c:forEach var="rs" items="${yearList}" varStatus="status">
					<option value="${rs.year}"<c:if test="${announcementVo.year eq rs.year}"> selected="selected"</c:if>>${rs.year}년</option>
				</c:forEach>
			</select>
		</div>

		<table class="chart_board">
			<colgroup>
				<col class="co2"/>
				<col class="co1"/>
				<col class="co1"/>
				<col class="co1"/>
				<col class="co1"/>
				<col class="co1"/>
			</colgroup>
			<caption class="hdn">사업공고관리</caption>
			<thead>
			<tr>
				<th>년도</th>
				<th>녹색자금 사업명</th>
				<th>접수기간</th>
				<th>최종제출기간</th>
				<th>제안서제출여부</th>
				<th>관리</th>
			</tr>
			</thead>
			<tbody>

			<c:forEach var="rs" items="${resultList}" varStatus="status">
				<tr>
					<td>${rs.year}년</td>
					<td class="tit">
						<a href="/mng/gfund/biz/ancmt/announcementView.do?year=${rs.year}&biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}</a>
					</td>
					<td>${rs.propse_papers_rcept_bgnde} ~ ${rs.propse_papers_rcept_endde}</td>
					<td>${rs.last_biz_plan_regist_bgnde} ~ ${rs.last_biz_plan_regist_endde}</td>
					<td>
						<c:if test="${rs.propse_rcept_at eq 'Y'}">사용</c:if>
						<c:if test="${rs.propse_rcept_at eq 'N'}">미사용</c:if>
					</td>
					<td>
						<a href="/mng/gfund/biz/ancmt/announcementForm.do?year=${rs.year}&biz_ty_code=${rs.biz_ty_code}"><img src="/template/manage/images/btn/edit.gif"></a>

						<c:if test="${rs.use_cnt eq 0}">
							<a href="/mng/gfund/biz/ancmt/announcementDelete.do?year=${rs.year}&biz_ty_code=${rs.biz_ty_code}" onclick="return fnAnnouncementDelete();"><img src="/template/manage/images/btn/del.gif"></a>
						</c:if>
					</td>
				</tr>
			</c:forEach>

			<c:if test="${fn:length(resultList) == 0}">
				<tr>
					<td class="listCenter" colspan="5"><spring:message code="common.nodata.msg"/></td>
				</tr>
			</c:if>

			</tbody>
		</table>
	</form>

	<div class="btn_r">
		<a href="/mng/gfund/biz/ancmt/announcementForm.do?year=${announcementVo.year}"><img src="/template/manage/images/btn/btn_creat.gif" alt="생성"></a>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
