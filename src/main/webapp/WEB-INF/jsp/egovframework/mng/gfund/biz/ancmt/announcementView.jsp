<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="ANNOUNCEMENT_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업공고관리(조회)"/>
</c:import>

<div id="cntnts">
	<table class="chart2">
		<colgroup>
			<col class="co1"/>
			<col class="co3"/>
		</colgroup>
		<caption class="hdn">사업공고관리</caption>
		<tbody>
		<tr>
			<th>기준년도</th>
			<td>${announcementVo.year}</td>
		</tr>
		<tr>
			<th>사업분류</th>
			<td>${announcementVo.biz_ty_code_nm}</td>
		</tr>
		<tr>
			<th>접수기간</th>
			<td>${announcementVo.propse_papers_rcept_bgnde} ~ ${announcementVo.propse_papers_rcept_endde}</td>
		</tr>
		<tr>
			<th>최종제출기간</th>
			<td>${announcementVo.last_biz_plan_regist_bgnde} ~ ${announcementVo.last_biz_plan_regist_endde}</td>
		</tr>
		<tr>
			<th>사업내용</th>
			<td>
				<textarea name="biz_cn" id="biz_cn" readonly="readonly" class="inp null_false" style="width:99%; height:200px" title="사업내용">${announcementVo.biz_cn}</textarea>
			</td>
		</tr>
		<tr>
			<th>사용여부</th>
			<td>
				<c:if test="${announcementVo.use_at eq 'Y'}">사용</c:if>
				<c:if test="${announcementVo.use_at eq 'N'}">미사용</c:if>
			</td>
		</tr>
		</tbody>
	</table>

	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${announcementVo.atch_file_id}" />
		<c:param name="appendPath" value="announcemen" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
	</c:import>
	<%-- 파일첨부 Import --%>
</div>

<div class="btn_c">
	<span class="cbtn2">
		<a href="/mng/gfund/biz/ancmt/announcementForm.do?year=${announcementVo.year}&biz_ty_code=${announcementVo.biz_ty_code}">수정</a>

	</span>
	<span class="cbtn1">
		<a href="/mng/gfund/biz/ancmt/announcementList.do<c:if test="${not empty announcementVo}">?year=${announcementVo.year}</c:if>">목록</a>
	</span>
</div>

<script type="text/javascript">
	<c:if test="${param.insertRs eq '1'}">
		alert("${announcementVo.year}년도 " + $("#biz_ty_code option:selected").text() + " 사업공고가 생성 되었습니다.");
	</c:if>
	<c:if test="${param.updateRs eq '1'}">
		alert("${announcementVo.year}년도 " + $("#biz_ty_code option:selected").text() + " 사업공고가 수정 되었습니다.");
	</c:if>
	<c:if test="${param.deleteRs eq '1'}">
		alert("${announcementVo.year}년도 " + $("#biz_ty_code option:selected").text() + " 사업공고가 삭제 되었습니다.");
	</c:if>
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>