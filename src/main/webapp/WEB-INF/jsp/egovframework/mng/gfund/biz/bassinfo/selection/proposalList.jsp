<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="SELECTTYPE_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 선정전형"/>
</c:import>

<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javascript">
	$(function(){
		//진행단계 변경 onChange 이벤트
		$('select[name=propse_flag]').change(function(){
			var biz_id = $(this).next('input[name=biz_id]').val();
			var propse_flag = $(this).val();
			var propse_msg = $(this).children('option:selected').text();
			if(confirm("해당 사업 제안서를 '" + propse_msg + "' 단계로 변경하시겠습니까?")){
				fnAjaxBizStepModify(biz_id, propse_flag);
			}
		});


		//행정구역 시.도, 시.군.구 OPNE API 이벤트
		if($('#area').val() != ""){
			$('#sido option:eq(0)').text($('#area').val().split(' ')[0]);
			$('#sidogungu option:eq(0)').text($('#area').val().split(' ')[1]);
			fnSidoOpenAPIAdd($('#sido'), 1);
		}else{
			//시도, 시도군구 이벤트
			fnSidoOpenAPIAdd($('#sido'), 1);
		}
		$('#sido').change(function(){
			$('#sidogungu option').remove();
			var opt = $('<option value="">시.군.구</option>');
			$('#sidogungu').append(opt);
			fnSidoOpenAPIAdd($('#sidogungu'), $('#sido').val());
			$('#area').val($("#sido option:selected").text());
		});
		$('#sidogungu').change(function(){
			$('#area').val($('#area').val() + " " + $("#sidogungu option:selected").text());
		});
	});

	//진행단계 변경 Ajax
	function fnAjaxBizStepModify(biz_id, propse_flag){
		var url = '/mng/gfund/biz/selection/updateBasicInformationStep.do';
		var data = [];
		data = {biz_id : biz_id, propse_flag : propse_flag};
		var successFn = function(json){
			if(Number(json.rs) > 0){
				alert('변경 되었습니다.');
			}
		};
		fn_ajax_json(url, data, successFn, null);
	}
</script>

<h3 class="icon1">선정전형</h3>
<form name="searchForm" action="/mng/gfund/biz/selection/proposalList.do" method="post">
	<input type="hidden" name="pageIndex" id="pageIndex" value="${bsifVo.pageIndex}"/>
	<div id="bbs_search">
		<select name="year" title="연도">
			<option value="">연도</option>
			<c:forEach var="rs" items="${yearlist}" varStatus="sts">
				<option value="${rs.year}" <c:if test="${bsifVo.year eq rs.year}"> selected="selected"</c:if> >${rs.year}</option>
			</c:forEach>
		</select>

		<select name="sido" id="sido" title="시,도">
			<option value="">시.도</option>
		</select>
		<select name="sidogungu" id="sidogungu" title="시,군,구">
			<option value="">시.군.구</option>
		</select>
		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM210"/>
			<c:param name="chVal" value="${bsifVo.biz_ty_code}"/>
			<c:param name="elType" value="select"/>
			<c:param name="elName" value="biz_ty_code"/>
			<c:param name="emptyMsg" value="사업분류 선택"/>
			<c:param name="elFast" value="elFast"/>
		</c:import>

		<select name="searchCondition">
			<option value="">키워드</option>
			<option value="1"<c:if test="${bsifVo.searchCondition eq 1}"> selected="selected"</c:if>>사업명</option>
			<option value="2"<c:if test="${bsifVo.searchCondition eq 2}"> selected="selected"</c:if>>기관명</option>
			<option value="3"<c:if test="${bsifVo.searchCondition eq 3}"> selected="selected"</c:if>>대표자</option>
		</select>

		<input type="hidden" name="area" id="area" value="${bsifVo.area}"/>
		<label for="searchKeyword" class="hdn">검색어입력</label>
		<input type="text" id="searchKeyword" name="searchKeyword" value="${bsifVo.searchKeyword}" class="inp" id="inp_text"/>
		<input type=image src="/template/manage/images/btn/btn_search.gif" alt="검색" />
	</div>
</form>

<p class="total">총 사업 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

<table class="chart_board">
	<colgroup>
		<col width="4%"/>
		<col width="8%"/>
		<col width="11%"/>
		<col width="auto"/>
		<col width="15%"/>
		<col width="12%"/>
		<col width="15%"/>
		<col width="8%"/>
		<col width="5%"/>
	</colgroup>
	<thead>
		<tr>
			<th>번호</th>
			<th>연도</th>
			<th>사업분류</th>
			<th>사업명</th>
			<th>제안기관 및 단체명</th>
			<th>지역</th>
			<th>사업기간</th>
			<th>단계</th>
			<th>관리</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<tr>
				<%--<td class="listtd">${paginationInfo.totalRecordCount - paginationInfo.firstRecordIndex - sts.count + 1}</td>--%>
				<td class="listtd">${paginationInfo.firstRecordIndex + sts.count}</td>
				<td class="listtd">${rs.year}년</td>
				<td class="listtd">${rs.biz_ty_code_nm}</td>
				<td class="listtd">${rs.biz_nm}</td>
				<td class="listtd">${rs.instt_nm}</td>
				<td class="listtd">${rs.area}</td>
				<td class="listtd">${rs.biz_bgnde} ~ ${rs.biz_endde}</td>
				<td class="listtd">
					<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
						<c:param name="codeId" value="COM215"/>
						<c:param name="chVal" value="${rs.propse_flag}"/>
						<c:param name="elType" value="select"/>
						<c:param name="elName" value="propse_flag"/>
						<c:param name="elFast" value="elFast"/>
					</c:import>
					<input type="hidden" name="biz_id" value="${rs.biz_id}" />
				</td>
				<td class="listtd">
					<a href="/mng/gfund/biz/selection/proposalView.do?biz_id=${rs.biz_id}"><img src="/template/manage/images/btn/btn_select.gif"/></a>
				</td>
			</tr>
		</c:forEach>
		<c:if test="${empty bsifList}">
			<tr>
				<td class="listtd" colspan="7">
					<spring:message code="common.nodata.msg"/>
				</td>
			</tr>
		</c:if>
	</tbody>
</table>

<div id="paging">
	<c:if test="${not empty paginationInfo}">
		<ul>
			<ui:pagination paginationInfo="${paginationInfo}" type="egovRenderer" jsFunction="fnPageMove"/>
		</ul>
	</c:if>
</div>
<%--
<div class="btn_c">
	<form name="" id="" action="/mng/gfund/biz/bassinfo/basicInformationForm.do" method="get">
		<select name="year" id="year">
			<c:forEach var="rs" items="${yearlist}">
				<option value="${rs.year}">${rs.year}년</option>
			</c:forEach>
		</select>

		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM210" />
			<c:param name="elType" value="select" />
			<c:param name="elName" value="biz_ty_code" />
		</c:import>

		<span class="cbtn">
			<button type="submit">작성</button>
		</span>
	</form>
</div>
--%>

<script>
	function fnOnSubmitRecordExcelDownload(){
		if($('form[name=recordExcelDownload] select[name=year]').val() == ""){
			alert("연도를 선택해 주세요.");
			return false;
		}else if($('form[name=recordExcelDownload] select[name=biz_ty_code]').val() == ""){
			alert("사업분류를 선택해 주세요.");
			return false;
		}else {
			return true;
		}
	}
</script>
<h3 class="icon1">사전심사자료(엑셀) 다운로드</h3>
<div id="bbs_search">
<form name="recordExcelDownload" action="/mng/biz/bassinfo/recordExcelDownload.do" method="post" onsubmit="return fnOnSubmitRecordExcelDownload();">
	<select name="year" title="연도">
		<option value="">연도</option>
		<c:forEach var="rs" items="${yearlist}" varStatus="sts">
			<option value="${rs.year}" <c:if test="${bsifVo.year eq rs.year}"> selected="selected"</c:if> >${rs.year}</option>
		</c:forEach>
	</select>
	<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
		<c:param name="codeId" value="COM210"/>
		<c:param name="chVal" value="${bsifVo.biz_ty_code}"/>
		<c:param name="elType" value="select"/>
		<c:param name="elName" value="biz_ty_code"/>
		<c:param name="emptyMsg" value="사업분류 선택"/>
		<c:param name="elFast" value="elFast"/>
	</c:import>
	<span class="mbtn1">
		<button type="submit">다운로드</button>
	</span>
</form>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
