<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="SELECTTYPE_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 선정전형"/>
</c:import>

<c:set var="step1" value="false" />
<c:set var="step2" value="false" />
<c:set var="step3" value="false" />
<c:set var="step4" value="false" />
<c:forEach var="rs" items="${bsList}" varStatus="sts">
	<c:if test="${rs.step_cl eq 'STEP1' and rs.step_cl_use eq 1}"><c:set var="step1" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP2' and rs.step_cl_use eq 1}"><c:set var="step2" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP3' and rs.step_cl_use eq 1}"><c:set var="step3" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP4' and rs.step_cl_use eq 1}"><c:set var="step4" value="true" /></c:if>
</c:forEach>

<div class="tab_step">
	<ul>
		<li class="active"><a href="/mng/gfund/biz/selection/proposalView.do?biz_id=${bsifVo.biz_id}">제안서 접수</a></li>
		<li <c:if test="${param.step_cl eq 'STEP1' or bsVo.step_cl eq 'STEP1'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step1 eq 'true'}">View</c:if><c:if test="${step1 ne 'true'}">Form</c:if>.do?biz_id=${bsifVo.biz_id}&step_cl=STEP1">1차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP2' or bsVo.step_cl eq 'STEP2'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step2 eq 'true'}">View</c:if><c:if test="${step2 ne 'true'}">Form</c:if>.do?biz_id=${bsifVo.biz_id}&step_cl=STEP2">현장심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP3' or bsVo.step_cl eq 'STEP3'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step3 eq 'true'}">View</c:if><c:if test="${step3 ne 'true'}">Form</c:if>.do?biz_id=${bsifVo.biz_id}&step_cl=STEP3">2차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP4' or bsVo.step_cl eq 'STEP4'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step4 eq 'true'}">View</c:if><c:if test="${step4 ne 'true'}">Form</c:if>.do?biz_id=${bsifVo.biz_id}&step_cl=STEP4">최종심사</a></li>
		<li><a href="/mng/gfund/biz/selection/finalProposalView.do?biz_id=${bsifVo.biz_id}">최종제출</a></li>
	</ul>
</div>

<h3 class="icon1">사업제안서</h3>

<form action="#">
	<div class="select_bar">
		<strong>${bsifVo.area}</strong>
	</div>
</form>

<c:set var="biz_ty_code" />
<c:choose>
	<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
	<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
	<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
</c:choose>
<div class="input_area">

	<%-- 제안서 기본 Form --%>
	<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeBasicInformationView.jsp"%>
	<%-- 제안서 기본 Form --%>

	<div class="pbg mB20">
		<strong>필수제출서류</strong><br/><br/>
		※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
		※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
		&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
		※ 특수교육시설의 경우 개인정보이용동의서만 제출합니다.
	</div>

	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="basicInformation" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
	</c:import>
	<%-- 파일첨부 Import --%>


</div>

<div class="btn_c">
	<span class="cbtn">
		<a href="/mng/gfund/biz/selection/proposalForm.do?biz_id=${bsifVo.biz_id}">수정</a>
	</span>
	<span class="cbtn1">
		<a href="/mng/gfund/biz/selection/proposalList.do">목록</a>
	</span>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
