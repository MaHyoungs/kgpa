<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="SELECTTYPE_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 선정전형"/>
</c:import>

<c:set var="step1" value="false" />
<c:set var="step2" value="false" />
<c:set var="step3" value="false" />
<c:set var="step4" value="false" />
<c:forEach var="rs" items="${bsList}" varStatus="sts">
	<c:if test="${rs.step_cl eq 'STEP1' and rs.step_cl_use eq 1}"><c:set var="step1" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP2' and rs.step_cl_use eq 1}"><c:set var="step2" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP3' and rs.step_cl_use eq 1}"><c:set var="step3" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP4' and rs.step_cl_use eq 1}"><c:set var="step4" value="true" /></c:if>
</c:forEach>

<div class="tab_step">
	<ul>
		<li><a href="/mng/gfund/biz/selection/proposalView.do?biz_id=${biVo.biz_id}">제안서 접수</a></li>
		<li <c:if test="${param.step_cl eq 'STEP1' or bsVo.step_cl eq 'STEP1'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step1 eq 'true'}">View</c:if><c:if test="${step1 ne 'true'}">Form</c:if>.do?biz_id=${biVo.biz_id}&step_cl=STEP1">1차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP2' or bsVo.step_cl eq 'STEP2'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step2 eq 'true'}">View</c:if><c:if test="${step2 ne 'true'}">Form</c:if>.do?biz_id=${biVo.biz_id}&step_cl=STEP2">현장심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP3' or bsVo.step_cl eq 'STEP3'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step3 eq 'true'}">View</c:if><c:if test="${step3 ne 'true'}">Form</c:if>.do?biz_id=${biVo.biz_id}&step_cl=STEP3">2차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP4' or bsVo.step_cl eq 'STEP4'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step4 eq 'true'}">View</c:if><c:if test="${step4 ne 'true'}">Form</c:if>.do?biz_id=${biVo.biz_id}&step_cl=STEP4">최종심사</a></li>
		<li><a href="/mng/gfund/biz/selection/finalProposalView.do?biz_id=${biVo.biz_id}">최종제출</a></li>
	</ul>
</div>

<c:set var="step_cl" value="" />
<c:choose>
	<c:when test="${not empty param.step_cl}"><c:set var="step_cl" value="${param.step_cl}" /></c:when>
	<c:when test="${not empty bsVo.step_cl}"><c:set var="step_cl" value="${bsVo.step_cl}" /></c:when>
</c:choose>

<c:set var="biz_id" value="" />
<c:choose>
	<c:when test="${not empty param.biz_id}"><c:set var="biz_id" value="${param.biz_id}" /></c:when>
	<c:when test="${not empty biVo.biz_id}"><c:set var="biz_id" value="${biVo.biz_id}" /></c:when>
	<c:when test="${not empty bsVo.biz_id}"><c:set var="biz_id" value="${bsVo.biz_id}" /></c:when>
</c:choose>

<c:if test="${step_cl ne 'STEP4'}">
	<h3 class="icon1">파일첨부</h3>
	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${bsVo.atch_file_id}" />
		<c:param name="updateFlag" value="Y" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="bizSelection" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
		<c:param name="formAjaxJs" value="add" />
	</c:import>
	<%-- 파일첨부 Import --%>
</c:if>

<c:set var="frmBtnMsg" value="" />
<c:if test="${empty bsVo.jdgmn_id}"><c:set var="frmBtnMsg" value="등록" /></c:if>
<c:if test="${not empty bsVo.jdgmn_id}"><c:set var="frmBtnMsg" value="변경" /></c:if>

<c:if test="${step_cl ne 'STEP4'}">
<script type="text/javascript">
	function fnBizSelectionFormSubmit(){
		if($('#expert_opinion').val() == ""){
			alert('의견을 입력하십시오.');
			$('#expert_opinion').focus();
			return false;
		}else if(!$('input[name=propse_flag]').is(":checked")){
			alert('합격여부를 선택하세요.');
			$('input[name=propse_flag]').focus();
			return false;
		}else{
			fn_text_XSS_check($('input[type=text], textarea'));
			return confirm("심사 ${frmBtnMsg} 하시겠습니까?");
		}
		/*if(!$('input[name=propse_flag]').is(":checked")){
			alert('합격여부를 선택하세요.');
			$('input[name=propse_flag]').focus();
			return false;
		}else if($('#expert_opinion').val() == ""){
			alert('의견을 입력하십시오.');
			$('#expert_opinion').focus();
			return false;
		}else{
			return confirm("심사 ${frmBtnMsg} 하시겠습니까?");
		}*/
	}
</script>
</c:if>
<c:if test="${step_cl eq 'STEP4'}">
<script type="text/javascript">
	function fnBizSelectionFormSubmit(){
		if(!$('input[name=propse_flag]').is(":checked")){
			alert('합격여부를 선택하세요.');
			$('input[name=propse_flag]').focus();
			return false;
		}else{
			return confirm("심사 ${frmBtnMsg} 하시겠습니까?");
		}
	}
</script>
</c:if>

<form action="/mng/gfund/biz/selection/selectionStepSave.do" method="post" onsubmit="return fnBizSelectionFormSubmit();">
	<input type="hidden" name="jdgmn_id" value="${bsVo.jdgmn_id}" >
	<input type="hidden" name="biz_id" value="${biz_id}" >
	<input type="hidden" name="step_cl" value="${step_cl}" >
	<%-- 파일첨부 ID --%>
	<input type="hidden" name="atch_file_id" id="atch_file_id" class="null_false" value="${bsVo.atch_file_id}" title="첨부파일"/>
	<input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${bsVo.atch_file_id}"/>

	<c:if test="${step_cl ne 'STEP4'}">
		<h3 class="icon1">의견</h3>
		<textarea name="expert_opinion" id="expert_opinion" class="txtarea mB20">${bsVo.expert_opinion}</textarea>
	</c:if>

	<c:choose>
		<c:when test="${step_cl eq 'STEP1'}">
			<input type="radio" name="propse_flag" id="propse_flag" value="STEP03" style="display:none;" checked="checked" />
		</c:when>
		<c:when test="${step_cl eq 'STEP2'}">
			<h3 class="icon1">현장심사 합격여부</h3>
			<div class="radio_box pL20">
				<input type="radio" name="propse_flag" id="propse_flag_y" value="STEP07" <c:if test="${biVo.propse_flag eq 'STEP07'}"> checked="checked"</c:if>/> <label for="propse_flag_y">합격</label>
				<input type="radio" name="propse_flag" id="propse_flag_n" value="STEP06" <c:if test="${biVo.propse_flag eq 'STEP06'}"> checked="checked"</c:if>/> <label for="propse_flag_n">탈락</label>
			</div>
		</c:when>
		<c:when test="${step_cl eq 'STEP3'}">
			<h3 class="icon1">2차심사 합격여부</h3>
			<div class="radio_box pL20">
				<input type="radio" name="propse_flag" id="propse_flag_y" value="STEP10" <c:if test="${biVo.propse_flag eq 'STEP10'}"> checked="checked"</c:if>/> <label for="propse_flag_y">합격</label>
				<input type="radio" name="propse_flag" id="propse_flag_n" value="STEP09" <c:if test="${biVo.propse_flag eq 'STEP09'}"> checked="checked"</c:if>/> <label for="propse_flag_n">탈락</label>
			</div>
		</c:when>
		<c:when test="${step_cl eq 'STEP4'}">
			<h3 class="icon1">최종심사 합격여부</h3>
			<div class="radio_box pL20">
				<input type="radio" name="propse_flag" id="propse_flag_y" value="STEP12" <c:if test="${biVo.propse_flag eq 'STEP12'}"> checked="checked"</c:if>/> <label for="propse_flag_y">합격</label>
				<input type="radio" name="propse_flag" id="propse_flag_n" value="STEP11" <c:if test="${biVo.propse_flag eq 'STEP11'}"> checked="checked"</c:if>/> <label for="propse_flag_n">탈락</label>
			</div>
		</c:when>
	</c:choose>

	<div class="btn_c">
		<span class="cbtn1"><button type="submit">${frmBtnMsg}</button></span>
	</div>
</form>


<script type="text/javascript">
	(function(){
		//파일 업로드 Form Ajax
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>