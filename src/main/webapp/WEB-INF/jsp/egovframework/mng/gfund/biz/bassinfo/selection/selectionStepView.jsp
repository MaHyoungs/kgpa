<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="SELECTTYPE_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 선정전형"/>
</c:import>

<c:set var="step1" value="false" />
<c:set var="step2" value="false" />
<c:set var="step3" value="false" />
<c:set var="step4" value="false" />
<c:forEach var="rs" items="${bsList}" varStatus="sts">
	<c:if test="${rs.step_cl eq 'STEP1' and rs.step_cl_use eq 1}"><c:set var="step1" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP2' and rs.step_cl_use eq 1}"><c:set var="step2" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP3' and rs.step_cl_use eq 1}"><c:set var="step3" value="true" /></c:if>
	<c:if test="${rs.step_cl eq 'STEP4' and rs.step_cl_use eq 1}"><c:set var="step4" value="true" /></c:if>
</c:forEach>

<c:set var="step_cl" value="" />
<c:choose>
	<c:when test="${not empty param.step_cl}"><c:set var="step_cl" value="${param.step_cl}" /></c:when>
	<c:when test="${not empty bsVo.step_cl}"><c:set var="step_cl" value="${bsVo.step_cl}" /></c:when>
</c:choose>

<c:set var="biz_id" value="" />
<c:choose>
	<c:when test="${not empty param.biz_id}"><c:set var="biz_id" value="${param.biz_id}" /></c:when>
	<c:when test="${not empty biVo.biz_id}"><c:set var="biz_id" value="${biVo.biz_id}" /></c:when>
	<c:when test="${not empty bsVo.biz_id}"><c:set var="biz_id" value="${bsVo.biz_id}" /></c:when>
</c:choose>

<div class="tab_step">
	<ul>
		<li><a href="/mng/gfund/biz/selection/proposalView.do?biz_id=${biVo.biz_id}">제안서 접수</a></li>
		<li <c:if test="${param.step_cl eq 'STEP1' or bsVo.step_cl eq 'STEP1'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step1 eq 'true'}">View</c:if><c:if test="${step1 ne 'true'}">Form</c:if>.do?biz_id=${bsVo.biz_id}&step_cl=STEP1">1차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP2' or bsVo.step_cl eq 'STEP2'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step2 eq 'true'}">View</c:if><c:if test="${step2 ne 'true'}">Form</c:if>.do?biz_id=${bsVo.biz_id}&step_cl=STEP2">현장심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP3' or bsVo.step_cl eq 'STEP3'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step3 eq 'true'}">View</c:if><c:if test="${step3 ne 'true'}">Form</c:if>.do?biz_id=${bsVo.biz_id}&step_cl=STEP3">2차심사</a></li>
		<li <c:if test="${param.step_cl eq 'STEP4' or bsVo.step_cl eq 'STEP4'}">class="active"</c:if>><a href="/mng/gfund/biz/selection/selectionStep<c:if test="${step4 eq 'true'}">View</c:if><c:if test="${step4 ne 'true'}">Form</c:if>.do?biz_id=${bsVo.biz_id}&step_cl=STEP4">최종심사</a></li>
		<li><a href="/mng/gfund/biz/selection/finalProposalView.do?biz_id=${biVo.biz_id}">최종제출</a></li>
	</ul>
</div>

<c:if test="${step_cl ne 'STEP4'}">
	<h3 class="icon1">파일첨부</h3>
	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${bsVo.atch_file_id}" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="bizSelection" />
	</c:import>
	<%-- 파일첨부 Import --%>
</c:if>

<c:set var="frmBtnMsg" value="" />
<c:if test="${empty bsVo.jdgmn_id}"><c:set var="frmBtnMsg" value="등록" /></c:if>
<c:if test="${not empty bsVo.jdgmn_id}"><c:set var="frmBtnMsg" value="변경" /></c:if>

<input type="hidden" name="jdgmn_id" value="${bsVo.jdgmn_id}" >
<input type="hidden" name="biz_id" value="${biz_id}" >
<input type="hidden" name="step_cl" value="${step_cl}" >
<%-- 파일첨부 ID --%>
<input type="hidden" name="atch_file_id" id="atch_file_id" class="null_false" value="${bsVo.atch_file_id}" title="첨부파일"/>
<input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${bsVo.atch_file_id}"/>

<c:if test="${step_cl ne 'STEP4'}">
	<h3 class="icon1">의견</h3>
	<div>
		<c:out value="${fn:replace(bsVo.expert_opinion, newLineChar, '<br/>')}" escapeXml="false" />
	</div>
</c:if>

<c:choose>
	<c:when test="${step_cl eq 'STEP1'}">
		<input type="hidden" name="propse_flag" value="STEP05" >
	</c:when>
	<c:when test="${step_cl eq 'STEP2'}">
		<h3 class="icon1">현장심사 합격여부</h3>
		<div class="radio_box pL20">
			<c:if test="${step2 eq 'true' or biVo.propse_flag eq 'STEP07'}">현장심사 합격</c:if>
			<c:if test="${biVo.propse_flag eq 'STEP06'}">현장심사 탈락</c:if>
		</div>
	</c:when>
	<c:when test="${step_cl eq 'STEP3'}">
		<h3 class="icon1">2차심사 합격여부</h3>
		<div class="radio_box pL20">
			<c:if test="${step3 eq 'true' or biVo.propse_flag eq 'STEP10'}">2차심사 합격</c:if>
			<c:if test="${biVo.propse_flag eq 'STEP09'}">2차심사 탈락</c:if>
		</div>

		<h3 class="icon1" style="margin-top:100px;">발표자료</h3>
		<div class="section1">
			<%-- 파일첨부 Import --%>
			<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
				<c:param name="style" value="gfSelection" />
				<c:param name="param_atchFileId" value="${biVo.presnatn_atch_file_id}" />
				<c:param name="pathKey" value="Gfund.fileStorePath" />
				<c:param name="appendPath" value="basicInformation" />
			</c:import>
			<%-- 파일첨부 Import --%>
		</div>
	</c:when>
	<c:when test="${step_cl eq 'STEP4'}">
		<h3 class="icon1">최종심사 합격여부</h3>
		<div class="radio_box pL20">
			<c:if test="${biVo.propse_flag eq 'STEP12'}">최종심사 합격</c:if>
			<c:if test="${biVo.propse_flag eq 'STEP11'}">최종심사 탈락</c:if>
		</div>
	</c:when>
</c:choose>

<div class="btn_c">
	<span class="cbtn1">
		<a href="/mng/gfund/biz/selection/selectionStepForm.do?biz_id=${biz_id}&step_cl=${step_cl}">수정</a>
	</span>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>