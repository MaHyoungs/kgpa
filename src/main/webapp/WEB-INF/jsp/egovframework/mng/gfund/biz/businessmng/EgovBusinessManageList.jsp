<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리"/>
</c:import>

<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javascript">
	$(function(){
		//행정구역 시.도, 시.군.구 OPNE API 이벤트
		if($('#area').val() != ""){
			$('#sido option:eq(0)').text($('#area').val().split(' ')[0]);
			$('#sidogungu option:eq(0)').text($('#area').val().split(' ')[1]);
			fnSidoOpenAPIAdd($('#sido'), 1);
		}else{
			//시도, 시도군구 이벤트
			fnSidoOpenAPIAdd($('#sido'), 1);
		}
		$('#sido').change(function(){
			$('#sidogungu option').remove();
			var opt = $('<option value="">시.군.구</option>');
			$('#sidogungu').append(opt);
			fnSidoOpenAPIAdd($('#sidogungu'), $('#sido').val());
			$('#area').val($("#sido option:selected").text());
		});
		$('#sidogungu').change(function(){
			$('#area').val($('#area').val() + " " + $("#sidogungu option:selected").text());
		});
	});

	//지도점검대상여부 상태 변경  Ajax
	function fnAjaxCchchckTargetAtModify(biz_id, targetYn, el){
		var url = '/mng/gfund/biz/businessmng/ajaxUpdateBasicInformationCchchckTargetAt.do';
		var data = [];
		data = {biz_id : biz_id, cch_chck_trget_at : targetYn};
		var successFn = function(json){
			if(Number(json.rs) > 0){
				alert('변경 되었습니다.');
				$(el).prop('checked', true);
			}
		};
		fn_ajax_json(url, data, successFn, null);
	}

	function cts_popup(obj) {
		// 기존 출력값 초기화
		$('#bizNm').text("");
		$('#makeAr').text("");
		$('#bizZip').text("");
		$('#totWct').text("");
		$('#chckType').text("");
		$('#frstRegistPnttm').text("");
		$('#frstRegisterId').text("");
		
		$('#bizNm').text($(obj).attr('bizNm'));
		$('#makeAr').text($(obj).attr('makeAr'));
		$('#bizZip').text($(obj).attr('bizZip'));
		$('#totWct').text($(obj).attr('totWct'));
		$('#chckType').text($(obj).attr('chckType'));
		$('#frstRegistPnttm').text($(obj).attr('frstRegistPnttm'));
		$('#frstRegisterId').text($(obj).attr('frstRegisterId'));
		
		$('#ctsPopupLayer').css('display', 'block');
	}
	
</script>

<h3 class="icon1">사업관리</h3>
<form action="" method="post">
	<div id="bbs_search">

		<select name="year" title="연도">
			<option value="">연도</option>
			<c:forEach var="rs" items="${yearlist}" varStatus="sts">
				<option value="${rs.year}" <c:if test="${bsifVo.year eq rs.year}"> selected="selected"</c:if> >${rs.year}</option>
			</c:forEach>
		</select>

		<select name="sido" id="sido" title="시,도">
			<option value="">시.도</option>
		</select>
		<select name="sidogungu" id="sidogungu" title="시,군,구">
			<option value="">시.군.구</option>
		</select>

		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM210"/>
			<c:param name="chVal" value="${bsifVo.biz_ty_code}"/>
			<c:param name="elType" value="select"/>
			<c:param name="elName" value="biz_ty_code"/>
			<c:param name="emptyMsg" value="사업분류 선택"/>
			<c:param name="elFast" value="elFast"/>
		</c:import>

		<select name="searchCondition">
			<option value="">키워드</option>
			<option value="1"<c:if test="${bsifVo.searchCondition eq 1}"> selected="selected"</c:if>>사업명</option>
			<option value="2"<c:if test="${bsifVo.searchCondition eq 2}"> selected="selected"</c:if>>기관명</option>
			<option value="3"<c:if test="${bsifVo.searchCondition eq 3}"> selected="selected"</c:if>>대표자</option>
		</select>

		<input type="hidden" name="area" id="area" value="${bsifVo.area}"/>
		<label for="searchKeyword" class="hdn">검색어입력</label>
		<input type="text" id="searchKeyword" name="searchKeyword" value="${bsifVo.searchKeyword}" class="inp" id="inp_text"/>
		<input type=image src="/template/manage/images/btn/btn_search.gif" alt="검색" />
	</div>
</form>

<p class="total">총 사업 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

<table class="chart_board">
	<colgroup>
		<col width="8%"/>
		<col width="auto"/>
		<col width="auto"/>
		<col width="15%"/>
		<col width="15%"/>
		<col width="10%"/>
		<col width="10%"/>
		<col width="5%"/>
		<col width="5%"/>
	</colgroup>
	<thead>
		<tr>
			<th>연도</th>
			<th>사업종류</th>
			<th>사업명</th>
			<th>지역</th>
			<th>사업기간</th>
			<th>지도점검대상여부</th>
			<th>실시여부</th>
			<th>지도점검</th>
			<th>관리</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<tr>
				<td class="listtd">${rs.year}년</td>
				<td class="listtd">${rs.biz_ty_code_nm}</td>
				<td class="listtd">${rs.biz_nm}</td>
				<td class="listtd">${rs.area}</td>
				<td class="listtd">${rs.biz_bgnde} ~ ${rs.biz_endde}</td>
				<td class="listtd">
					<input type="radio" name="cch_chck_trget_at_${sts.count}" id="target_yes" value="Y" cssClass="cho" <c:if test="${rs.cch_chck_trget_at eq 'Y'}">checked="checked"</c:if> onclick="fnAjaxCchchckTargetAtModify('${rs.biz_id}','Y',this);return false;" /> <label for="target_yes">예</label>
					<input type="radio" name="cch_chck_trget_at_${sts.count}" id="target_no" value="N" cssClass="cho" <c:if test="${rs.cch_chck_trget_at eq 'N'}">checked="checked"</c:if> onclick="fnAjaxCchchckTargetAtModify('${rs.biz_id}','N',this);return false;" /> <label for="target_no">아니오</label>
				</td>
				<td class="listtd">
					<c:choose><c:when test="${rs.comp_at eq 'Y'}" >예</c:when><c:otherwise>아니오</c:otherwise></c:choose>
				</td>
				<td class="listtd">
					<span 	bizNm="${rs.biz_nm}" 
							makeAr="<fmt:formatNumber value="${rs.make_ar}" groupingUsed="true"/> (㎡)" 
							bizZip="(${rs.biz_zip}) ${rs.biz_adres} ${rs.biz_adres_detail}"
							totWct="<fmt:formatNumber value="${rs.tot_wct}" groupingUsed="true"/> (단위: 원)"
							<c:if test="${(rs.biz_ty_code eq 'BTC01')}">
								<c:choose>
									<c:when test="${(rs.ground_trplant_ar ne '' or rs.ground_trplant_ar ne null or rs.rf_trplant_ar ne '' or rs.rf_trplant_ar ne null)}">
									chckType="지상녹화, 옥상녹화"
									</c:when>
									<c:when test="${(rs.ground_trplant_ar eq '' or rs.ground_trplant_ar eq null or rs.rf_trplant_ar ne '' or rs.rf_trplant_ar ne null)}">
									chckType="옥상녹화"
									</c:when>
									<c:when test="${(rs.ground_trplant_ar ne '' or rs.ground_trplant_ar ne null or rs.rf_trplant_ar eq '' or rs.rf_trplant_ar eq null)}">
									chckType="지상녹화"
									</c:when>
								</c:choose>
							</c:if>
							<c:if test="${rs.biz_ty_code eq 'BTC02'}">
							chckType="${rs.codeNm}"
							</c:if>
							<c:if test="${(rs.biz_ty_code eq 'BTC03') or (rs.biz_ty_code eq 'BTC04') or (rs.biz_ty_code eq 'BTC05') or (rs.biz_ty_code eq 'BTC06') or (rs.biz_ty_code eq 'BTC08')}">
							chckType="${rs.codeNm}"
							</c:if>
							frstRegistPnttm="${rs.chck_last_updusr_pnttm}"
							frstRegisterId="${rs.chck_frst_register_id}"
							onclick="cts_popup(this);" style="cursor:pointer;cursor:hand;">보기</span>
				</td>
				<td class="listtd">
					<a href="/mng/gfund/biz/businessmng/finalProposalView.do?year=${rs.year}&biz_id=${rs.biz_id}&biz_ty_code=${rs.biz_ty_code}"><img src="/template/manage/images/btn/btn_select.gif"/></a>
				</td>
			</tr>
		</c:forEach>
		<c:if test="${empty bsifList}">
			<tr>
				<td class="listtd" colspan="8">
					<spring:message code="common.nodata.msg"/>
				</td>
			</tr>
		</c:if>
	</tbody>
</table>

<div id="paging">
	<c:url var="pageUrl" value="/mng/gfund/biz/businessmng/EgovBusinessManageList.do">
		<c:param name="biz_id" value="${param.biz_id}" />
	</c:url>

	<c:if test="${not empty paginationInfo}">
		<ul>
			<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}"/>
		</ul>
	</c:if>
</div>

<style type="text/css">
	.ctsPopupStyle {
		position:absolute;
		top:300px;
		left:400px;
		padding:0px;
		border:1px solid #004876;
		background:#FFF;
		overflow:hidden;
	}
</style>

<div id="ctsPopupLayer" class="ctsPopupStyle" style="display:none;">
	<table class="chart2" style="width:700px">
		<caption>지도점검요약표</caption>
		<colgroup>
			<col width="12%">
			<col width="56%">
			<col width="12%">
			<col width="20%">
		</colgroup>
		<tbody>
			<tr style="border:0px none;">
				<td colspan="4" style="text-align:right"><span onclick="$('#ctsPopupLayer').css('display', 'none');" style="cursor:pointer;cursor:hand;"><img src="/template/manage/images/btn_sdelete.gif" alt="닫기"></span></td>
			</tr>
			<tr>
				<th>사업명</th><td><span id="bizNm"></span></td><th>사업면적</th><td><span id="makeAr"></span></td>
			</tr>
			<tr>
				<th>위치</th><td><span id="bizZip"></span></td><th>사업비</th><td><span id="totWct"></span></td>
			</tr>
			<tr>
				<th rowspan="2">점검분야</th><td rowspan="2"><span id="chckType"></span></td><th>점검일시</th><td><span id="frstRegistPnttm"></span></td>
			</tr>
			<tr>
				<th>점검자</th><td><span id="frstRegisterId"></span></td>
			</tr>
			<tr>
				<td colspan="4"><center><span onclick="$('#ctsPopupLayer').css('display', 'none');" style="cursor:pointer;cursor:hand;">닫기</span></center></td>
			</tr>
		</tbody>
	</table>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
