<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 지도점검"/>
</c:import>

<link type="text/css" rel="stylesheet" href="<c:url value='/template/mobile/css/chckupload.css'/>"/>
<script type="text/javascript">

	$(function(){
		//달력 이벤트 로드
		fnDatepickerOptionAdd();
		$("#lastUpdusrPnttm").datepicker({ dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true });
	});

	function tabs_change(idx) {
		// 평가지표
		if(idx==1) {
			$('#idx1').attr('class', 'active');
			$('#idx2').attr('class', 'idx2');
			$('#idx3').attr('class', 'idx3');
			document.getElementById('bizForm').style.display="block";
			document.getElementById('valIdx').style.display="block";
			document.getElementById('comSig').style.display="none";
			document.getElementById('fileUp').style.display="none";
			// 일반사항
		} else if(idx==2) {
			$('#idx1').attr('class', 'idx1');
			$('#idx2').attr('class', 'active');
			$('#idx3').attr('class', 'idx2');
			document.getElementById('bizForm').style.display="block";
			document.getElementById('valIdx').style.display="none";
			document.getElementById('comSig').style.display="block";
			document.getElementById('fileUp').style.display="none";
			// 파일업로드
		} else if(idx==3) {
			$('#idx1').attr('class', 'idx1');
			$('#idx2').attr('class', 'idx2');
			$('#idx3').attr('class', 'active');
			document.getElementById('bizForm').style.display="none";
			document.getElementById('valIdx').style.display="none";
			document.getElementById('comSig').style.display="none";
			document.getElementById('fileUp').style.display="block";
		}
	}
	function submitForm() {
		if(confirm("저장하시겠습니까?")) {
				document.chckListVo.submit();
		}
	}
</script>

<c:set var="formAction" value="/mng/gfund/biz/businessmng/EgovChckListSelectUpdt.do${_BASE_PARAM}" />

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${selectBusinessInfo.biz_nm}" escapeXml="false" />
	</div>

	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>

	<div class="mtab">
		<ul>
			<li id="idx1" class="active"><a href="#" style="cursor:pointer;" onclick="tabs_change(1);">평가지표</a> <span>|</span></li></li>
			<li id="idx2" class="idx2"><a href="#" style="cursor:pointer;" onclick="tabs_change(2);">일반사항</a> <span>|</span></li></li>
			<li id="idx3" class="idx3"><a href="#" style="cursor:pointer;" onclick="tabs_change(3);">파일업로드</a></li>
		</ul>
	</div>

	<form name="chckListVo" id="chckListVo" method="post" action="${formAction}" enctype="multipart/form-data" onsubmit="return fnCommonOnSubmit($('.inp'));">
		<input type="hidden" id="bizId" name="bizId" value="${bizId}" />

		<div id="bizForm" style="display:block;">
			<table  class="chart2 mB40" summary="지도점검 사업정보">
				<caption>지도점검 사업정보 등록폼</caption>
				<colgroup>
					<col width="15%" />
					<col width="35%" />
					<col width="15%" />
					<col width="35%" />
				</colgroup>
				<tbody>
					<tr>
						<th>사업명</th>
						<td>${selectBusinessInfo.biz_nm}</td>
						<th>사업면적</th>
					<c:if test="${not empty selectBusinessInfo.make_ar}">
						<td>${selectBusinessInfo.make_ar} ㎡</td>
					</c:if>
					<c:if test="${empty selectBusinessInfo.make_ar}">
						<td></td>
					</c:if>
					</tr>
					<tr>
						<th>위치</th>
					<c:if test="${not empty selectBusinessInfo.biz_zip}">
						<td>(${selectBusinessInfo.biz_zip}) ${selectBusinessInfo.biz_adres} ${selectBusinessInfo.biz_adres_detail}</td>
					</c:if>
					<c:if test="${empty selectBusinessInfo.biz_zip}">
						<td></td>
					</c:if>
						<th>사업비</th>
					<c:if test="${not empty selectBusinessInfo.tot_wct}">
						<td><fmt:formatNumber value="${selectBusinessInfo.tot_wct}" groupingUsed="true"/> (단위 : 원)</td>
					</c:if>
					<c:if test="${empty selectBusinessInfo.tot_wct}">
						<td></td>
					</c:if>
					</tr>
					<tr>
						<th rowspan="2">점검분야</th>
					<c:if test="${(selectBusinessInfo.biz_ty_code eq 'BTC01')}">
						<td rowspan="2">${fn:substringBefore(selectBusinessInfo.codeNm, '(')}(
								<c:choose>
									<c:when test="${(selectChckList.ground_trplant_ar ne '' or selectChckList.ground_trplant_ar ne null or selectChckList.rf_trplant_ar ne '' or selectChckList.rf_trplant_ar ne null)}">
										지상녹화, 옥상녹화 )</td>
									</c:when>
									<c:when test="${(selectChckList.ground_trplant_ar eq '' or selectChckList.ground_trplant_ar eq null or selectChckList.rf_trplant_ar ne '' or selectChckList.rf_trplant_ar ne null)}">
										옥상녹화 )</td>
									</c:when>
									<c:when test="${(selectChckList.ground_trplant_ar ne '' or selectChckList.ground_trplant_ar ne null or selectChckList.rf_trplant_ar eq '' or selectChckList.rf_trplant_ar eq null)}">
										지상녹화 )</td>
									</c:when>
								</c:choose>
								</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC02'}">
						<td rowspan="2">${selectBusinessInfo.codeNm}</td>
					</c:if>
					<c:if test="${(selectBusinessInfo.biz_ty_code eq 'BTC03') or (selectBusinessInfo.biz_ty_code eq 'BTC04') or (selectBusinessInfo.biz_ty_code eq 'BTC05') or (selectBusinessInfo.biz_ty_code eq 'BTC06') or (selectBusinessInfo.biz_ty_code eq 'BTC08')}">
						<td rowspan="2">${selectBusinessInfo.codeNm}</td>
					</c:if>
						<th>점검일시</th>
					<c:if test="${empty selectChckList.lastUpdusrPnttm}">
						<td>점검일시는 현재날짜가 기입됩니다.</td>
					</c:if>
					<c:if test="${not empty selectChckList.lastUpdusrPnttm}">
						<td>${selectChckList.lastUpdusrPnttm}</td>
					</c:if>
					</tr>
					<tr>
						<th>점검자</th>
					<c:if test="${(selectChckList.frstRegisterId eq null) or (selectChckList.frstRegisterId eq '')}">
						<input type="hidden" class="inp" name="frstRegisterId" id="frstRegisterId" value="${loginId}"/>
					</c:if>
					<c:if test="${(selectChckList.frstRegisterId ne null)}">
						<input type="hidden" class="inp" name="lastUpdusrId" id="lastUpdusrId" value="${loginId}"/>
					</c:if>
						<td>${loginId}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<%-- 사업관리 > 지도점검 > 평가지표 --%>
			<div id="valIdx" style="display:block;">
				<table  class="chart1 mB40" summary="지도점검 점검항목, 결과 등록폼">
					<caption>지도점검 점검항목, 결과 등록폼</caption>
					<colgroup>
						<col width="15%" />
						<col width="30%" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>구분 </th>
							<th>점검항목 </th>
							<th>점검결과 </th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
							<th rowspan="9">계획 ‧  준비 </th>
						</c:if>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
							<th rowspan="8">계획 ‧  준비 </th>
						</c:if>
							<td class="bg" colspan="2">① 사업목적의 타당성·적절성 </td>
						</tr>
						<tr>
							<td>- 수혜대상자가 녹색자금 지원취지와 부합하는가? </td>
							<td><textarea class="inp" name="eipp0101" id="eipp0101" title="계획‧준비 1-1">${selectChckList.eipp0101}</textarea></td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td>- 수혜대상자를 위한 맞춤형 조성공간으로 구성되었는가? </td>
							<td><textarea class="inp" name="eipp0102" id="eipp0102" title="계획‧준비 1-2">${selectChckList.eipp0102}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<td>수혜대상자를 위한 맞춤형 프로그램으로 구성되었는가? </td>
							<td><textarea class="inp" name="eipp0103" id="eipp0103" title="계획‧준비 1-2">${selectChckList.eipp0103}</textarea></td>
						</tr>
					</c:if>
						<tr>
							<td  class="bg" colspan="2">② 사업 타당성에 대한 사전분석의 적절성(의견수렴) </td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td>- 주민설명회, 구조안전진단 실시 등을 실시하였는가? </td>
							<td><textarea class="inp" name="eipp0201" id="eipp0201" title="계획‧준비 2-1">${selectChckList.eipp0201}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05'}">
						<tr>
							<td>수혜자선정의 적정성 </td>
							<td><textarea class="inp" name="eipp0202" id="eipp0202" title="계획‧준비 2-1">${selectChckList.eipp0202}</textarea></td>
						</tr>
						<tr>
							<td>1인당 교육비 산정의 타당성(교육횟수, 교육인원) </td>
							<td><textarea class="inp" name="eipp0203" id="eipp0203" title="계획‧준비 2-2">${selectChckList.eipp0203}</textarea></td>
						</tr>
						<tr>
							<td>교육커리큐럼의 품질(독창성 및 다양성) </td>
							<td><textarea class="inp" name="eipp0204" id="eipp0204" title="계획‧준비 2-3">${selectChckList.eipp0204}</textarea></td>
						</tr>
						<tr>
							<td>강의의 적정성(강사진, 강사비 선정) </td>
							<td><textarea class="inp" name="eipp0205" id="eipp0205" title="계획‧준비 2-4">${selectChckList.eipp0205}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<td>홍보의 효과성 </td>
							<td><textarea class="inp" name="eipp0206" id="eipp0206" title="계획‧준비 2-1">${selectChckList.eipp0206}</textarea></td>
						</tr>
						<tr>
							<td>홍보(행사) 참여확대를 위한 노력 정도 </td>
							<td><textarea class="inp" name="eipp0207" id="eipp0207" title="계획‧준비 2-2">${selectChckList.eipp0207}</textarea></td>
						</tr>
						<tr>
							<td>캠페인 노출효과(노출빈도 포함) </td>
							<td><textarea class="inp" name="eipp0208" id="eipp0208" title="계획‧준비 2-3">${selectChckList.eipp0208}</textarea></td>
						</tr>
						<tr>
							<td>행사장소 선정의 적정성 </td>
							<td><textarea class="inp" name="eipp0209" id="eipp0209" title="계획‧준비 2-4">${selectChckList.eipp0209}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td class="bg">③ 기술자문위원에 설계자문 의뢰 여부 </td>
						</tr>
						<tr>
							<td>기술자문위원게 설계자문을 의뢰하였는가? </td>
							<td><textarea class="inp" name="eipp0301" id="eipp0301" title="계획‧준비 3-1">${selectChckList.eipp0301}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td class="bg">④ 설계변경 필요 여부 </td>
						</tr>
						<tr>
							<td>수종, 시설물 등에 대한 설계변경이 필요한가? </td>
							<td><textarea class="inp" name="eipp0401" id="eipp0401" title="계획‧준비 4-1">${selectChckList.eipp0401}</textarea></td>
						</tr>
					</c:if>
						<tr>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
							<th rowspan="15">집행 ‧ 관리</th>
						</c:if>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
							<th rowspan="14">집행 ‧ 관리</th>
						</c:if>
							<td  class="bg" colspan="2">① 추진계획 대비 일정 준수 여부 </td>
						</tr>
						<tr>
							<td>- 사업계획 대비 일정을 준수하고 있는가? </td>
							<td><textarea class="inp" name="eiem0101" id="eiem0101" title="집행‧관리 1-1">${selectChckList.eiem0101}</textarea></td>
						</tr>
						<tr>
							<td>- 사업예산 계획 대비 집행실적은 적절한가? </td>
							<td><textarea class="inp" name="eiem0102" id="eiem0102" title="집행‧관리 1-2">${selectChckList.eiem0102}</textarea></td>
						</tr>
						<tr>
							<td>- 지적 및 권고사항을 조치·완료하였는가? </td>
							<td><textarea class="inp" name="eiem0103" id="eiem0103" title="집행‧관리 1-3">${selectChckList.eiem0103}</textarea></td>
						</tr>
						<tr>
							<td  class="bg" colspan="2">② 사업추진체계 및 모니터링 체계 운영 적정성 </td>
						</tr>
						<tr>
							<td>- 유관기관·사업과의 연계협조체계를 구축하였는가? </td>
							<td><textarea class="inp" name="eiem0201" id="eiem0201" title="집행‧관리 2-1">${selectChckList.eiem0201}</textarea></td>
						</tr>
						<tr>
							<td>- 사업별 통장관리를 하고 있는가? </td>
							<td><textarea class="inp" name="eiem0202" id="eiem0202" title="집행‧관리 2-2">${selectChckList.eiem0202}</textarea></td>
						</tr>
						<tr>
							<td>- 예산절감노력을 하고 있는가? </td>
							<td><textarea class="inp" name="eiem0203" id="eiem0203" title="집행‧관리 2-3">${selectChckList.eiem0203}</textarea></td>
						</tr>
						<tr>
							<td>- 지출·증빙 자료의 관리·비치하였는가? </td>
							<td><textarea class="inp" name="eiem0204" id="eiem0204" title="집행‧관리 2-4">${selectChckList.eiem0204}</textarea></td>
						</tr>
						<tr>
							<td>- 민원, 안전사고예방을 위하여 적절한 조치를 하였는가? </td>
							<td><textarea class="inp" name="eiem0205" id="eiem0205" title="집행‧관리 2-5">${selectChckList.eiem0205}</textarea></td>
						</tr>
						<tr>
							<td>- 사업단  요구자료에 대한 제출시기를 준수하였는가? </td>
							<td><textarea class="inp" name="eiem0206" id="eiem0206" title="집행‧관리 2-6">${selectChckList.eiem0206}</textarea></td>
						</tr>
						<tr>
							<td class="bg" colspan="2">③ 홍보의 적정성 </td>
						</tr>
						<tr>
							<td>- 사업단계별 홍보를 실시하였는가? </td>
							<td><textarea class="inp" name="eiem0301" id="eiem0301" title="집행‧관리 3-1">${selectChckList.eiem0301}</textarea></td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td>- 표지석, 수목표찰을 설치하였는가? </td>
							<td><textarea class="inp" name="eiem0302" id="eiem0302" title="집행‧관리 3-2">${selectChckList.eiem0302}</textarea></td>
						</tr
					</c:if>
						<tr>
							<td>- 홍보문구 준수하였는가? </td>
							<td><textarea class="inp" name="eiem0303" id="eiem0303" title="집행‧관리 3-3">${selectChckList.eiem0303}</textarea></td>
						</tr>

						<tr>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
							<th rowspan="5">성과 ‧ 환류</th>
						</c:if>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
							<th rowspan="5">성과 ‧ 환류</th>
						</c:if>
							<td  class="bg" colspan="2">① 성과목표 달성도 및 노력정도 </td>
						</tr>
						<tr>
							<td>- 사업계획 대비 연간 성과목표를 달성하였는가? </td>
							<td><textarea class="inp" name="eiar0101" id="eiar0101" title="성과‧환류 1-1">${selectChckList.eiar0101}</textarea></td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td>- 조성공간의 접근성 및 상시 개방하고 있는가? </td>
							<td><textarea class="inp" name="eiar0102" id="eiar0102" title="성과‧환류 1-2">${selectChckList.eiar0102}</textarea></td>
						</tr>
					</c:if>
						<tr>
							<td  class="bg" colspan="2">② 모니터링 결과, 사업평가 결과, 지적사항 등 환류여부 </td>
						</tr>
						<tr>
							<td>- 문제점 및 민원(지적)사항에 대한 적절한 대응을 하였는가?  </td>
							<td><input type="text" class="inp" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		<%-- 사업관리 > 지도점검 > 평가지표 끝 --%>

		<%-- 사업관리 > 지도점검 > 일반사항 --%>
			<div id="comSig" style="display:none;">
				<table  class="chart1 mB40" summary="지도점검 점검항목, 결과 등록폼">
					<caption>지도점검 점검항목, 결과 등록폼</caption>
					<colgroup>
						<col width="15%" />
						<col width="auto" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="30%" />
					</colgroup>
					<thead>
						<tr>
							<th rowspan="2">구분 </th>
							<th rowspan="2">점검항목 </th>
							<th colspan="3">점검내역 </th>
							<th rowspan="2">비고 </th>
						</tr>
						<tr>
							<th>적절 </th>
							<th>부적절 </th>
							<th>해당없음 </th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<th>대상자모집 </th>
							<td>①소외계층 모집 여부 </td>
							<td class="alC"><input type="radio" name="gisr01" value="1" <c:if test="${selectChckList.gisr01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisr01" value="2" <c:if test="${selectChckList.gisr01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisr01" value="3" <c:if test="${selectChckList.gisr01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gisr01Rm" id="gisr01Rm">${selectChckList.gisr01Rm}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<th>구조안전진단 </th>
							<td>①구조안전진단 실시 여부 </td>
							<td class="alC"><input type="radio" name="gissi01" value="1" <c:if test="${selectChckList.gissi01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gissi01" value="2" <c:if test="${selectChckList.gissi01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gissi01" value="3" <c:if test="${selectChckList.gissi01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gissi01Rm" id="gissi01Rm">${selectChckList.gissi01Rm}</textarea></td>
						</tr>
						<tr>
							<th rowspan="2">일정 </th>
							<td>①준공예정기일 준수 여부 </td>
							<td class="alC"><input type="radio" name="gisc01" value="1" <c:if test="${selectChckList.gisc01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisc01" value="2" <c:if test="${selectChckList.gisc01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisc01" value="3" <c:if test="${selectChckList.gisc01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gisc01Rm" id="gisc01Rm">${selectChckList.gisc01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②설계변경 등 필요여부 </td>
							<td class="alC"><input type="radio" name="gisc02" value="1" <c:if test="${selectChckList.gisc02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisc02" value="2" <c:if test="${selectChckList.gisc02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gisc02" value="3" <c:if test="${selectChckList.gisc02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gisc02Rm" id="gisc02Rm">${selectChckList.gisc02Rm}</textarea></td>
						</tr>
						<tr>
							<th rowspan="3">식재 </th>
							<td>①사업계획에 따른 수목식재비율 준수 여부 </td>
						<td class="alC"><input type="radio" name="gipl01" value="1"  <c:if test="${selectChckList.gipl01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl01" value="2"  <c:if test="${selectChckList.gipl01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl01" value="3"  <c:if test="${selectChckList.gipl01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipl01Rm" id="gipl01Rm">${selectChckList.gipl01Rm}</textarea></td>
						<tr>
							<td>②고사 수목에 대한 조치필요 여부 </td>
							<td class="alC"><input type="radio" name="gipl02" value="1" <c:if test="${selectChckList.gipl02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl02" value="2" <c:if test="${selectChckList.gipl02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl02" value="3" <c:if test="${selectChckList.gipl02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipl02Rm" id="gipl02Rm">${selectChckList.gipl02Rm}</textarea></td>
						</tr>
						<tr>
							<td>③식재 변경(수종, 교목, 관목 등) 필요 여부 </td>
							<td class="alC"><input type="radio" name="gipl03" value="1" <c:if test="${selectChckList.gipl03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl03" value="2" <c:if test="${selectChckList.gipl03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipl03" value="3" <c:if test="${selectChckList.gipl03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipl03Rm" id="gipl03Rm">${selectChckList.gipl03Rm}</textarea></td>
						</tr>
						<tr>
							<th rowspan="3">표지석 및 표찰 </th>
							<td>①수목식재 후 표찰 부착 여부 </td>
							<td class="alC"><input type="radio" name="gimsl01" value="1" <c:if test="${selectChckList.gimsl01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl01" value="2" <c:if test="${selectChckList.gimsl01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl01" value="3" <c:if test="${selectChckList.gimsl01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gimsl01Rm" id="gimsl01Rm">${selectChckList.gimsl01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②표지석 설치 여부 </td>
							<td class="alC"><input type="radio" name="gimsl02" value="1" <c:if test="${selectChckList.gimsl02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl02" value="2" <c:if test="${selectChckList.gimsl02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl02" value="3" <c:if test="${selectChckList.gimsl02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gimsl02Rm" id="gimsl02Rm">${selectChckList.gimsl02Rm}</textarea></td>
						</tr>
						<tr>
							<td>③표지석 및 표찰에 홍보문구 준수 여부 </td>
							<td class="alC"><input type="radio" name="gimsl03" value="1" <c:if test="${selectChckList.gimsl03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl03" value="2" <c:if test="${selectChckList.gimsl03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gimsl03" value="3" <c:if test="${selectChckList.gimsl03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gimsl03Rm" id="gimsl03Rm">${selectChckList.gimsl03Rm}</textarea></td>
						</tr>
						<tr>
							<th rowspan="2">시설물 </th>
							<td>①고가의 정자·파고라·체육시설 등 설계반영 또는 시공 여부 </td>
							<td class="alC"><input type="radio" name="gifc01" value="1" <c:if test="${selectChckList.gifc01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gifc01" value="2" <c:if test="${selectChckList.gifc01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gifc01" value="3" <c:if test="${selectChckList.gifc01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gifc01Rm" id="gifc01Rm">${selectChckList.gifc01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②기반시설 및 부대시설 설계 또는 시공 과다 여부 </td>
							<td class="alC"><input type="radio" name="gifc02" value="1" <c:if test="${selectChckList.gifc02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gifc02" value="2" <c:if test="${selectChckList.gifc02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gifc02" value="3" <c:if test="${selectChckList.gifc02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gifc02Rm" id="gifc02Rm">${selectChckList.gifc02Rm}</textarea></td>
						</tr>
					</c:if>
						<tr>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
							<th rowspan="3">사업비 집행 </th>
						</c:if>
						<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
							<th rowspan="6">사업비 집행 </th>
						</c:if>
							<td>①사업진행에 따른 사업비집행 준수 여부 </td>
							<td class="alC"><input type="radio" name="giee01" value="1" <c:if test="${selectChckList.giee01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee01" value="2" <c:if test="${selectChckList.giee01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee01" value="3" <c:if test="${selectChckList.giee01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee01Rm" id="giee01Rm">${selectChckList.giee01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②사업비 집행 잔액 발생에 따른 조치 여부 </td>
							<td class="alC"><input type="radio" name="giee02" value="1" <c:if test="${selectChckList.giee02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee02" value="2" <c:if test="${selectChckList.giee02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee02" value="3" <c:if test="${selectChckList.giee02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee02Rm" id="giee02Rm">${selectChckList.giee02Rm}</textarea></td>
						</tr>
						<tr>
							<td>③사업비 변경시 변경신청 절차 준수 여부 </td>
							<td class="alC"><input type="radio" name="giee03" value="1" <c:if test="${selectChckList.giee03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee03" value="2" <c:if test="${selectChckList.giee03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee03" value="3" <c:if test="${selectChckList.giee03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee03Rm" id="giee03Rm">${selectChckList.giee03Rm}</textarea></td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<td>④체크카드 발급 여부 </td>
							<td class="alC"><input type="radio" name="giee04" value="1" <c:if test="${selectChckList.giee04 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee04" value="2" <c:if test="${selectChckList.giee04 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee04" value="3" <c:if test="${selectChckList.giee04 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee04Rm" id="giee04Rm">${selectChckList.giee04Rm}</textarea></td>
						</tr>
						<tr>
							<td>⑤예산낭비 요인 발생 여부 </td>
							<td class="alC"><input type="radio" name="giee05" value="1" <c:if test="${selectChckList.giee05 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee05" value="2" <c:if test="${selectChckList.giee05 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee05" value="3" <c:if test="${selectChckList.giee05 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee05Rm" id="giee05Rm">${selectChckList.giee05Rm}</textarea></td>
						</tr>
						<tr>
							<td>⑥강사비 지급 기준마련, 회계담당자 지정 여부 </td>
							<td class="alC"><input type="radio" name="giee06" value="1" <c:if test="${selectChckList.giee06 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee06" value="2" <c:if test="${selectChckList.giee06 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giee06" value="3" <c:if test="${selectChckList.giee06 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giee06Rm" id="giee06Rm">${selectChckList.giee06Rm}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<th rowspan="2">증빙 </th>
							<td>①사업비 집행 관련 증빙서류 적합 및 보관여부 </td>
							<td class="alC"><input type="radio" name="gipf01" value="1" <c:if test="${selectChckList.gipf01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf01" value="2" <c:if test="${selectChckList.gipf01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf01" value="3" <c:if test="${selectChckList.gipf01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf01Rm" id="gipf01Rm">${selectChckList.gipf01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②기타(사업관련 보고서 작성 등) </td>
							<td class="alC"><input type="radio" name="gipf03" value="1" <c:if test="${selectChckList.gipf03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf03" value="2" <c:if test="${selectChckList.gipf03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf03" value="3" <c:if test="${selectChckList.gipf03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf03Rm" id="gipf03Rm">${selectChckList.gipf03Rm}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<th rowspan="4">증빙 </th>
							<td>①사업비 집행 적합 및 보관 관리 여부 </td>
							<td class="alC"><input type="radio" name="gipf02" value="1" <c:if test="${selectChckList.gipf02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf02" value="2" <c:if test="${selectChckList.gipf02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf02" value="3" <c:if test="${selectChckList.gipf02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf02Rm" id="gipf02Rm">${selectChckList.gipf02Rm}</textarea></td>
						</tr>
						<tr>
							<td>②사업관련 계약(물품계약) 등 적합 여부(복수견적 등) </td>
							<td class="alC"><input type="radio" name="gipf04" value="1" <c:if test="${selectChckList.gipf04 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf04" value="2" <c:if test="${selectChckList.gipf04 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf04" value="3" <c:if test="${selectChckList.gipf04 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf04Rm" id="gipf04Rm">${selectChckList.gipf04Rm}</textarea></td>
						</tr>
						<tr>
							<td>③출장 등에 원인행위 및 결과보고 여부 </td>
							<td class="alC"><input type="radio" name="gipf05" value="1" <c:if test="${selectChckList.gipf05 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf05" value="2" <c:if test="${selectChckList.gipf05 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf05" value="3" <c:if test="${selectChckList.gipf05 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf05Rm" id="gipf05Rm">${selectChckList.gipf05Rm}</textarea></td>
						</tr>
						<tr>
							<td>④원천징수영수증 발급, 강사비지급 명부 관리 여부 </td>
							<td class="alC"><input type="radio" name="gipf06" value="1" <c:if test="${selectChckList.gipf06 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf06" value="2" <c:if test="${selectChckList.gipf06 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gipf06" value="3" <c:if test="${selectChckList.gipf06 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gipf06Rm" id="gipf06Rm">${selectChckList.gipf06Rm}</textarea></td>
						</tr>
					</c:if>
						<tr>
							<th rowspan="4">사업비 관리시스템</th>
							<td>①사업비관리시스템 적시 입력여부 </td>
							<td class="alC"><input type="radio" name="giems01" value="1" <c:if test="${selectChckList.giems01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems01" value="2" <c:if test="${selectChckList.giems01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems01" value="3" <c:if test="${selectChckList.giems01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giems01Rm" id="giems01Rm">${selectChckList.giems01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②실집행액과 사업비관리시스템 집행액 일치 여부 </td>
							<td class="alC"><input type="radio" name="giems02" value="1" <c:if test="${selectChckList.giems02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems02" value="2" <c:if test="${selectChckList.giems02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems02" value="3" <c:if test="${selectChckList.giems02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giems02Rm" id="giems02Rm">${selectChckList.giems02Rm}</textarea></td>
						</tr>
						<tr>
							<td>③월별 추진실적 입력 여부 </td>
							<td class="alC"><input type="radio" name="giems03" value="1" <c:if test="${selectChckList.giems03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems03" value="2" <c:if test="${selectChckList.giems03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems03" value="3" <c:if test="${selectChckList.giems03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giems03Rm" id="giems03Rm">${selectChckList.giems03Rm}</textarea></td>
						</tr>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC01' || selectBusinessInfo.biz_ty_code eq 'BTC02' || selectBusinessInfo.biz_ty_code eq 'BTC03' || selectBusinessInfo.biz_ty_code eq 'BTC04' || selectBusinessInfo.biz_ty_code eq 'BTC08'}">
						<tr>
							<td>④상세내역입력, 오류정정 이행여부 </td>
							<td class="alC"><input type="radio" name="giems04" value="1" <c:if test="${selectChckList.giems04 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems04" value="2" <c:if test="${selectChckList.giems04 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems04" value="3" <c:if test="${selectChckList.giems04 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giems04Rm" id="giems04Rm">${selectChckList.giems04Rm}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<td>④기타(상세내역입력, 오류정정 등) </td>
							<td class="alC"><input type="radio" name="giems05" value="1" <c:if test="${selectChckList.giems05 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems05" value="2" <c:if test="${selectChckList.giems05 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giems05" value="3" <c:if test="${selectChckList.giems05 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giems05Rm" id="giems05Rm">${selectChckList.giems05Rm}</textarea></td>
						</tr>
					</c:if>
					<c:if test="${selectBusinessInfo.biz_ty_code eq 'BTC05' || selectBusinessInfo.biz_ty_code eq 'BTC06'}">
						<tr>
							<th>보험 </th>
							<td>①참가자 보험 가입 여부 </td>
							<td class="alC"><input type="radio" name="giis01" value="1" <c:if test="${selectChckList.giis01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giis01" value="2" <c:if test="${selectChckList.giis01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="giis01" value="3" <c:if test="${selectChckList.giis01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="giis01Rm" id="giis01Rm">${selectChckList.giis01Rm}</textarea></td>
						</tr>
					</c:if>
						<tr>
							<th rowspan="4">기타 (홍보문구 준수 등) 　</th>
							<td>①<textarea class="inp" name="gietcT01" id="gietcT01">${selectChckList.gietcT01}</textarea></td>
							<td class="alC"><input type="radio" name="gietc01" value="1" <c:if test="${selectChckList.gietc01 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc01" value="2" <c:if test="${selectChckList.gietc01 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc01" value="3" <c:if test="${selectChckList.gietc01 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gietcT01Rm" id="gietcT01Rm">${selectChckList.gietcT01Rm}</textarea></td>
						</tr>
						<tr>
							<td>②<textarea class="inp" name="gietcT02" id="gietcT02">${selectChckList.gietcT02}</textarea></td>
							<td class="alC"><input type="radio" name="gietc02" value="1" <c:if test="${selectChckList.gietc02 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc02" value="2" <c:if test="${selectChckList.gietc02 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc02" value="3" <c:if test="${selectChckList.gietc02 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gietcT02Rm" id="gietcT02Rm">${selectChckList.gietcT02Rm}</textarea></td>
						</tr>
						<tr>
							<td>③<textarea class="inp" name="gietcT03" id="gietcT03">${selectChckList.gietcT03}</textarea></td>
							<td class="alC"><input type="radio" name="gietc03" value="1" <c:if test="${selectChckList.gietc03 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc03" value="2" <c:if test="${selectChckList.gietc03 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc03" value="3" <c:if test="${selectChckList.gietc03 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gietcT03Rm" id="gietcT03Rm">${selectChckList.gietcT03Rm}</textarea></td>
						</tr>
						<tr>
							<td>④<textarea class="inp" name="gietcT04" id="gietcT04">${selectChckList.gietcT04}</textarea></td>
							<td class="alC"><input type="radio" name="gietc04" value="1" <c:if test="${selectChckList.gietc04 eq '1'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc04" value="2" <c:if test="${selectChckList.gietc04 eq '2'}"> checked="checked" </c:if> /></td>
							<td class="alC"><input type="radio" name="gietc04" value="3" <c:if test="${selectChckList.gietc04 eq '3'}"> checked="checked" </c:if> /></td>
							<td colspan="3"><textarea class="inp" name="gietcT04Rm" id="gietcT04Rm">${selectChckList.gietcT04Rm}</textarea></td>
						</tr>
					</tbody>
				</table>
			</div>
			<%-- 파일첨부 ID --%>
			<input type="hidden" name="ctt_atch_file_id" id="ctt_atch_file_id" class="null" value="${selectChckList.ctt_atch_file_id}" title="첨부파일"/>
			<input type="hidden" name="gctt_atch_file_id" id="gctt_atch_file_id" class="null" value="${selectChckList.ctt_atch_file_id}" title="첨부파일"/>
			<input type="hidden" name="atch_file_id" id="atch_file_id" class="null" value="${selectChckList.atch_file_id}" title="첨부파일"/>
			<input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${selectChckList.atch_file_id}"/>
		</form>
		<%-- 사업관리 > 지도점검 > 일반사항 끝 --%>

		<%-- 사업관리 > 지도점검 > 파일업로드 --%>
		<div id="fileUp" style="display:none;">
			<h3 class="icon1">지도점검표</h3>
			<div class="file_box">
				<div class="file_top">
				<%-- 파일첨부 Import --%>
					<c:import url="/cmm/fms/selectFileInfsForUpdate5.do" charEncoding="utf-8">
						<c:param name="param_atchFileId" value="${selectChckList.ctt_atch_file_id}" />
						<c:param name="updateFlag2" value="Y" />
						<c:param name="pathKey2" value="Gfund.fileStorePath" />
						<c:param name="appendPath2" value="chckMatrix" />
						<c:param name="maxSize2" value="31457280" />
						<c:param name="maxCount2" value="1" />
						<c:param name="formAjaxJs2" value="add" />
					</c:import>
				<%-- 파일첨부 Import --%>
				</div>
			</div>

			<h3 class="icon1">사진</h3>
			<div class="file_box">
				<div class="file_top">
				<%-- 이미지 파일 Import--%>
					<c:import url="/cmm/fms/selectFileInfsForUpdate6.do" charEncoding="utf-8">
						<c:param name="param_atchFileId" value="${selectChckList.atch_file_id}" />
						<c:param name="updateFlag" value="Y" />
						<c:param name="pathKey" value="Gfund.fileStorePath" />
						<c:param name="appendPath" value="chckImg" />
						<c:param name="maxSize" value="104857600"/>
						<c:param name="maxCount" value="10" />
						<c:param name="formAjaxJs" value="add" />
					</c:import>
				<%--파일첨부 Import--%>
				</div>
			</div>
		</div>
		<%-- 사업관리 > 지도점검 > 파일업로드 끝 --%>
		<div class="btn_r">
		<span class="cbtn1"><button type="submit" onclick="submitForm()">저장</button></span>
		</div>
</div>

<script type="text/javascript">
		(function(){
			//파일 업로드 Form Ajax
			var progress=$('.progress');
			var bar = $('.bar');
			$('#boardFileAjaxForm').ajaxForm({
				dataType: 'json',
				beforeSend: function(){
					progress.show();
					var percentVal='0%';
					progress.width(percentVal);
					bar.html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete){
					var percentVal=percentComplete-1 + '%';
					progress.width(percentVal);
					bar.html(percentVal);
				},
				success: function(json){
					if(json.rs == 'countOver'){
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
					}else if(json.rs == 'overflow'){
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
					}else if (json.rs == 'denyExt') {
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부할 수 없는 확장자입니다.");
					}else{
						var percentVal='100%';
						progress.width("99%");
						bar.html(percentVal);
						fnAjaxFileUploadComplete(json);
					}
				},
				complete: function(xhr){
					var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
					var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
					$('#boardFileAjaxForm input[name=uploadfile]').remove();
					file_parent.prepend(file);
				}
			});
			
			var progress2=$('.file_box2 .progress2');
			var bar2 = $('.file_box2 .bar2');
			$('#chckFileAjaxForm').ajaxForm({
				dataType: 'json',
				beforeSend: function(){
					progress2.show();
					var percentVal='0%';
					progress2.width(percentVal);
					bar2.html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete){
					var percentVal=percentComplete-1 + '%';
					progress2.width(percentVal);
					bar2.html(percentVal);
				},
				success: function(json){
					if(json.rs == 'countOver'){
						$('.file_box2 .progress2').hide();
						$('.file_box2 .progress2').width("0%");
						$('.file_box2 .bar2').html("0%");
						alert("지도점검표 첨부파일은 최대 " + $('#maxCount2').val() + "개를 초과할 수 없습니다.");
					}else if(json.rs == 'overflow'){
						$('.file_box2 .progress2').hide();
						$('.file_box2 .progress2').width("0%");
						$('.file_box2 .bar2').html("0%");
						alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
					}else if (json.rs == 'denyExt') {
						$('.progress').hide();
						$('.progress').width("0%");
						$('.bar').html("0%");
						alert("첨부할 수 없는 확장자입니다.");
					}else{
						var percentVal='100%';
						progress2.width("99%");
						bar2.html(percentVal);
						fnAjaxFileUploadComplete2(json);
					}
				},
				complete: function(xhr){
					var file=$('#chckFileAjaxForm input[name=uploadfile2]').clone();
					var file_parent = $('#chckFileAjaxForm input[name=uploadfile2]').parent();
					$('#chckFileAjaxForm input[name=uploadfile2]').remove();
					file_parent.prepend(file);
				}
			});
		})();
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />