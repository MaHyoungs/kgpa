<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 사업현황 - 월별인원실적"/>
</c:import>

<c:set var="formAction" value="/mng/gfund/biz/businessmng/EgovNmprAcmsltSelectUpdt.do${_BASE_PARAM}" />

<script type="text/javascript">
</script>

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<!-- 사업관리 - 사업현황 - 월별인원실적 -->
	
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>

	<div class="mtab">
		<ul>
			<li>
				<a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">월별사업추진계획 및 실적</a>
			</li>
			<li class="active">
				<span style="margin-right:1px;">|</span>
				<a href="/mng/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do${_BASE_PARAM}">월별인원실적</a>
			</li>
		</ul>
	</div>
	
	<form name="nmprAcmsltForm" id="nmprAcmsltForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.minp'));">
	<div class="input_area">
	<h3 class="icon1">월별 인원실적</h3>
		<table class="chart1" summary="월별교육인원 실적을 나타내는 표 입니다">
			<caption>월별교육인원 실적</caption>
			<thead>
				<tr>
					<th rowspan="2">기관명</th>
					<th rowspan="2">사업명</th>
					<th rowspan="2">월별</th>
					<th rowspan="2">교육횟수</th>
					<th rowspan="2">교육인원</th>
					<th colspan="8">취약계층</td>
					<th rowspan="2">청소년</th>
					<th rowspan="2">일반인</th>
					<th rowspan="2">기타</th>
				</tr>
				<tr>
					<th>장애인</th>
					<th>저소득</th>
					<th>다문화</th>
					<th>한부모</th>
					<th>장기실업</th>
					<th>부적응</th>
					<th>어르신</th>
					<th>북한이탈가족<br /> 피해자(숲길체험)</th>
				</tr>
			</thead>
			<tfoot>	
				<tr>
					<td colspan="2" >합     계</td>
					<td></td>
					<td><input type="text" name="edcCoSum" id="edcCoSum" class="minp" title="교육횟수 합계" readonly="readonly" /></td>
					<td><input type="text" name="edcNmprSum" id="edcNmprSum" class="minp" title="교육인원 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt01Sum" id="frgltySclsrt01Sum" class="minp" title="장애인 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt02Sum" id="frgltySclsrt02Sum" class="minp" title="저소득 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt03Sum" id="frgltySclsrt03Sum" class="minp" title="다문화 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt04Sum" id="frgltySclsrt04Sum" class="minp" title="한부모 합계" readonly="readonly"  /></td>
					<td><input type="text" name="frgltySclsrt05Sum" id="frgltySclsrt05Sum" class="minp" title="장기실업 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt06Sum" id="frgltySclsrt06Sum" class="minp" title="부적용 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt07Sum" id="frgltySclsrt07Sum" class="minp" title="어르신 합계" readonly="readonly" /></td>
					<td><input type="text" name="frgltySclsrt08Sum" id="frgltySclsrt08Sum" class="minp" title="북한이탈가족피해자(숲길체험) 합계" readonly="readonly" /></td>
					<td><input type="text" name="yngbgsSum" id="yngbgsSum" class="minp" title="청소년 합계" readonly="readonly" /></td>
					<td><input type="text" name="gnrlPersonSum" id="gnrlPersonSum" class="minp" title="일반인 합계" readonly="readonly" /></td>
					<td><input type="text" name="etcSum" id="etcSum" class="minp" title="기타 합계" readonly="readonly" /></td>
				</tr>
			</tfoot>
			<tbody>
			<c:choose>
			<c:when test="${empty resultList or resultCnt < 1}">
				<c:forEach begin="1" end="12" step="1" varStatus="status">
				<tr>
					<c:if test="${status.count eq 1}">
						<td rowspan="12" >자동입력</td>
						<td rowspan="12" >자동입력</td>
					</c:if>
					<td>${status.count}월</td>
					<td><input type="text" name="edcCoList" id="edcCoList" class="minp null_false validation number" value="0" title="교육횟수" /></td>
					<td><input type="text" name="edcNmprList" id="edcNmprList" class="minp null_false validation number" value="0" title="교육인원" /></td>
					<td><input type="text" name="frgltySclsrt01List" id="frgltySclsrt01List" class="minp null_false validation number" value="0" title=" 장애인" /></td>
					<td><input type="text" name="frgltySclsrt02List" id="frgltySclsrt02List" class="minp null_false validation number" value="0" title="저소득" /></td>
					<td><input type="text" name="frgltySclsrt03List" id="frgltySclsrt03List" class="minp null_false validation number" value="0" title="다문화" /></td>
					<td><input type="text" name="frgltySclsrt04List" id="frgltySclsrt04List" class="minp null_false validation number" value="0" title="한부모" /></td>
					<td><input type="text" name="frgltySclsrt05List" id="frgltySclsrt05List" class="minp null_false validation number" value="0" title="장기실업" /></td>
					<td><input type="text" name="frgltySclsrt06List" id="frgltySclsrt06List" class="minp null_false validation number" value="0" title="부적용" /></td>
					<td><input type="text" name="frgltySclsrt07List" id="frgltySclsrt07List" class="minp null_false validation number" value="0" title="어르신" /></td>
					<td><input type="text" name="frgltySclsrt08List" id="frgltySclsrt08List" class="minp null_false validation number" value="0" title="북한이탈가족피해자(숲길체험)" ></td>
					<td><input type="text" name="yngbgsList" id="yngbgsList" class="minp null_false validation number" value="0" title="청소년" /></td>
					<td><input type="text" name="gnrlPersonList" id="gnrlPersonList" class="minp null_false validation number" value="0" title="일반인" /></td>
					<td><input type="text" name="etcList" id="etcList" class="minp null_false validation number" value="0" title="기타" /></td>							
					<input type="hidden" name="mtClList" id="mtClList" value="MTH<fmt:formatNumber value="${status.count}" pattern="00" />" />
					<input type="hidden" name="frstRegisterIdList" id="frstRegisterIdList" value="${USER_INFO.id}" />
				</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr>
					<c:if test="${status.count eq 1}">
						<td rowspan="12" >자동입력</td>
						<td rowspan="12" >자동입력</td>
					</c:if>
					<td>${status.count}월</td>
					<td><input type="text" name="edcCoList" id="edcCoList" class="minp null_false validation number" value="${result.edcCo}" title="교육횟수" /></td>
					<td><input type="text" name="edcNmprList" id="edcNmprList" class="minp null_false validation number" value="${result.edcNmpr}" title="교육인원" /></td>
					<td><input type="text" name="frgltySclsrt01List" id="frgltySclsrt01List" class="minp null_false validation number" value="${result.frgltySclsrt01}" title=" 장애인" /></td>
					<td><input type="text" name="frgltySclsrt02List" id="frgltySclsrt02List" class="minp null_false validation number" value="${result.frgltySclsrt02}" title="저소득" /></td>
					<td><input type="text" name="frgltySclsrt03List" id="frgltySclsrt03List" class="minp null_false validation number" value="${result.frgltySclsrt03}" title="다문화" /></td>
					<td><input type="text" name="frgltySclsrt04List" id="frgltySclsrt04List" class="minp null_false validation number" value="${result.frgltySclsrt04}" title="한부모" /></td>
					<td><input type="text" name="frgltySclsrt05List" id="frgltySclsrt05List" class="minp null_false validation number" value="${result.frgltySclsrt05}" title="장기실업" /></td>
					<td><input type="text" name="frgltySclsrt06List" id="frgltySclsrt06List" class="minp null_false validation number" value="${result.frgltySclsrt06}" title="부적용" /></td>
					<td><input type="text" name="frgltySclsrt07List" id="frgltySclsrt07List" class="minp null_false validation number" value="${result.frgltySclsrt07}" title="어르신" /></td>
					<td><input type="text" name="frgltySclsrt08List" id="frgltySclsrt08List" class="minp null_false validation number" value="${result.frgltySclsrt08}" title="북한이탈가족피해자(숲길체험)" ></td>
					<td><input type="text" name="yngbgsList" id="yngbgsList" class="minp null_false validation number" value="${result.yngbgs}" title="청소년" /></td>
					<td><input type="text" name="gnrlPersonList" id="gnrlPersonList" class="minp null_false validation number" value="${result.gnrlPerson}" title="일반인" /></td>
					<td><input type="text" name="etcList" id="etcList" class="minp null_false validation number" value="${result.etc}" title="기타" /></td>							
					<input type="hidden" name="mtClList" id="mtClList" value="${result.mtCl}" />
					<input type="hidden" name="lastUpdusrIdList" id="lastUpdusrIdList" value="${USER_INFO.id}" />
				</tr>
				</c:forEach>
			</c:otherwise>
			</c:choose>	
			</tbody>
		</table>
	</div>
	</form>

	<div class="btn_r">
		<span class="cbtn1"><button type="button" onclick="$('#nmprAcmsltForm').submit();">저장</button></span>
	</div>
	
	<script type="text/javascript">
		// 교육횟수 전체 합계
		$('input[name=edcCoList]').change(function(){
			var edcCoSum = 0;
			$('input[name=edcCoList]').each(function(idx, el){
				edcCoSum += Number($(el).val());
			});
			$('#edcCoSum').val(edcCoSum);
		});
		
		// 교육인원 전체 합계		
		$('input[name=edcNmprList]').change(function(){
			var edcNmprSum = 0;
			$('input[name=edcNmprList]').each(function(idx, el){
				edcNmprSum += Number($(el).val());
			});
			$('#edcNmprSum').val(edcNmprSum);
		});
		
		// 장애인 전체 합계
		$('input[name=frgltySclsrt01List]').change(function(){
			var frgltySclsrt01Sum = 0;
			$('input[name=frgltySclsrt01List]').each(function(idx, el){
				frgltySclsrt01Sum += Number($(el).val());
			});
			$('#frgltySclsrt01Sum').val(frgltySclsrt01Sum);
		});
		
		// 저소득 전체 합계
		$('input[name=frgltySclsrt02List]').change(function(){
			var frgltySclsrt02Sum = 0;
			$('input[name=frgltySclsrt02List]').each(function(idx, el){
				frgltySclsrt02Sum += Number($(el).val());
			});
			$('#frgltySclsrt02Sum').val(frgltySclsrt02Sum);
		});
		
		// 다문화 전체 합계
		$('input[name=frgltySclsrt03List]').change(function(){
			var frgltySclsrt03Sum = 0;
			$('input[name=frgltySclsrt03List]').each(function(idx, el){
				frgltySclsrt03Sum += Number($(el).val());
			});
			$('#frgltySclsrt03Sum').val(frgltySclsrt03Sum);
		});
		
		// 한부모 전체 합계
		$('input[name=frgltySclsrt04List]').change(function(){
			var frgltySclsrt04Sum = 0;
			$('input[name=frgltySclsrt04List]').each(function(idx, el){
				frgltySclsrt04Sum += Number($(el).val());
			});
			$('#frgltySclsrt04Sum').val(frgltySclsrt04Sum);
		});
		
		// 장기실업 전체 합계
		$('input[name=frgltySclsrt05List]').change(function(){
			var frgltySclsrt05Sum = 0;
			$('input[name=frgltySclsrt05List]').each(function(idx, el){
				frgltySclsrt05Sum += Number($(el).val());
			});
			$('#frgltySclsrt05Sum').val(frgltySclsrt05Sum);
		});
		
		// 부적용 전체 합계
		$('input[name=frgltySclsrt06List]').change(function(){
			var frgltySclsrt06Sum = 0;
			$('input[name=frgltySclsrt06List]').each(function(idx, el){
				frgltySclsrt06Sum += Number($(el).val());
			});
			$('#frgltySclsrt06Sum').val(frgltySclsrt06Sum);
		});
		
		// 어르신 전체 합계
		$('input[name=frgltySclsrt07List]').change(function(){
			var frgltySclsrt07Sum = 0;
			$('input[name=frgltySclsrt07List]').each(function(idx, el){
				frgltySclsrt07Sum += Number($(el).val());
			});
			$('#frgltySclsrt07Sum').val(frgltySclsrt07Sum);
		});
			
		// 북한이탈가족피해자(숲길체험) 전체 합계
		$('input[name=frgltySclsrt08List]').change(function(){
			var frgltySclsrt08Sum = 0;
			$('input[name=frgltySclsrt08List]').each(function(idx, el){
				frgltySclsrt08Sum += Number($(el).val());
			});
			$('#frgltySclsrt08Sum').val(frgltySclsrt08Sum);
		});
		
		// 청소년 전체 합계
		$('input[name=yngbgsList]').change(function(){
			var yngbgsSum = 0;
			$('input[name=yngbgsList]').each(function(idx, el){
				yngbgsSum += Number($(el).val());
			});
			$('#yngbgsSum').val(yngbgsSum);
		});
		
		// 일반인 전체 합계
		$('input[name=gnrlPersonList]').change(function(){
			var gnrlPersonSum = 0;
			$('input[name=gnrlPersonList]').each(function(idx, el){
				gnrlPersonSum += Number($(el).val());
			});
			$('#gnrlPersonSum').val(gnrlPersonSum);
		});
		
		// 일반인 전체 합계
		$('input[name=etcList]').change(function(){
			var etcSum = 0;
			$('input[name=etcList]').each(function(idx, el){
				etcSum += Number($(el).val());
			});
			$('#etcSum').val(etcSum);
		});
		
		$(window).load(function () {
			// 교육횟수 전체 합계
			var edcCoSum = 0;
			$('input[name=edcCoList]').each(function(idx, el){ edcCoSum += Number($(el).val());	});
			$('#edcCoSum').val(edcCoSum);

			// 교육인원 전체 합계		
			var edcNmprSum = 0;
			$('input[name=edcNmprList]').each(function(idx, el){ edcNmprSum += Number($(el).val());	});
			$('#edcNmprSum').val(edcNmprSum);

			// 장애인 전체 합계
			var frgltySclsrt01Sum = 0;
			$('input[name=frgltySclsrt01List]').each(function(idx, el){ frgltySclsrt01Sum += Number($(el).val()); });
			$('#frgltySclsrt01Sum').val(frgltySclsrt01Sum);

			// 저소득 전체 합계
			var frgltySclsrt02Sum = 0;
			$('input[name=frgltySclsrt02List]').each(function(idx, el){ frgltySclsrt02Sum += Number($(el).val()); });
			$('#frgltySclsrt02Sum').val(frgltySclsrt02Sum);

			// 다문화 전체 합계
			var frgltySclsrt03Sum = 0;
			$('input[name=frgltySclsrt03List]').each(function(idx, el){	frgltySclsrt03Sum += Number($(el).val()); });
			$('#frgltySclsrt03Sum').val(frgltySclsrt03Sum);

			// 한부모 전체 합계
			var frgltySclsrt04Sum = 0;
			$('input[name=frgltySclsrt04List]').each(function(idx, el){	frgltySclsrt04Sum += Number($(el).val()); });
			$('#frgltySclsrt04Sum').val(frgltySclsrt04Sum);

			// 장기실업 전체 합계
			var frgltySclsrt05Sum = 0;
			$('input[name=frgltySclsrt05List]').each(function(idx, el){	frgltySclsrt05Sum += Number($(el).val()); });
			$('#frgltySclsrt05Sum').val(frgltySclsrt05Sum);

			// 부적용 전체 합계
			var frgltySclsrt06Sum = 0;
			$('input[name=frgltySclsrt06List]').each(function(idx, el){	frgltySclsrt06Sum += Number($(el).val()); });
			$('#frgltySclsrt06Sum').val(frgltySclsrt06Sum);

			// 어르신 전체 합계
			var frgltySclsrt07Sum = 0;
			$('input[name=frgltySclsrt07List]').each(function(idx, el){	frgltySclsrt07Sum += Number($(el).val()); });
			$('#frgltySclsrt07Sum').val(frgltySclsrt07Sum);

			// 북한이탈가족피해자(숲길체험) 전체 합계
			var frgltySclsrt08Sum = 0;
			$('input[name=frgltySclsrt08List]').each(function(idx, el){ frgltySclsrt08Sum += Number($(el).val()); });
			$('#frgltySclsrt08Sum').val(frgltySclsrt08Sum);

			// 청소년 전체 합계
			var yngbgsSum = 0;
			$('input[name=yngbgsList]').each(function(idx, el){	yngbgsSum += Number($(el).val()); });
			$('#yngbgsSum').val(yngbgsSum);

			// 일반인 전체 합계
			var gnrlPersonSum = 0;
			$('input[name=gnrlPersonList]').each(function(idx, el){ gnrlPersonSum += Number($(el).val()); });
			$('#gnrlPersonSum').val(gnrlPersonSum);

			// 일반인 전체 합계
			var etcSum = 0;
			$('input[name=etcList]').each(function(idx, el){ etcSum += Number($(el).val());	});
			$('#etcSum').val(etcSum);
		});
	</script>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />