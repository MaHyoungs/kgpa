<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:if test="${empty photoInfoVo}">
		<c:set var="formType" value="등록" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${param.biz_id}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:param name="bizType" value="${param.bizType}" />
		</c:url>
	</c:if>
	<c:if test="${not empty photoInfoVo}">
		<c:set var="formType" value="수정" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${photoInfoVo.bizId}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:param name="bizType" value="${photoInfoVo.bizType}" />
			<c:param name="pageIndex" value="${param.pageIndex}" />
		</c:url>
	</c:if>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 사진정보"/>
</c:import>

<script type="text/javascript">
	function resetForm() {
		
		var frm = document.photoInfoForm;
		frm.reset();
	
		return false;
	}
	
	function checkForm() {
		if(fnCommonOnSubmit($('.inp'))) {
			if ( $('#tr_file_empty').css('display') != 'none' ) {
				alert('첨부된 파일이 없습니다');
				return false;
			}
			var frm = document.photoInfoForm;
			frm.submit();
			return true;
		}
		return false;
	}
</script>

<c:if test="${empty photoInfoVo}">
	<c:set var="formAction" value="/mng/gfund/biz/businessmng/EgovPhotoInfoInsert.do${_BASE_PARAM}" />
</c:if>
<c:if test="${not empty photoInfoVo}">
	<c:set var="formAction" value="/mng/gfund/biz/businessmng/EgovPhotoInfoSelectUpdt.do${_BASE_PARAM}" />
</c:if>

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>

	<h3 class="icon1">사진${formType}</h3>
	<form name="photoInfoForm" id="photoInfoForm" action="${formAction}" method="post">
		<input type="hidden" name="fileGroupId" id="fileGroupId" value="${photoInfoVo.atchFileId}"/>
		<input type="hidden" name="bizId" id="bizId" value="${photoInfoVo.bizId}" />
		<input type="hidden" name="photoInfoId" id="photoInfoId" value="${photoInfoVo.photoInfoId}" />
		<c:if test="${empty photoInfoVo}">
			<input type="hidden" name="frstRegisterId" id="frstRegisterId" value="${USER_INFO.id}" />
		</c:if>
		<c:if test="${not empty photoInfoVo}">
			<input type="hidden" name="lastUpdusrId" id="lastUpdusrId" value="${USER_INFO.id}" />
		</c:if>
		<table  class="chart2" summary="사진정보를 ${formType}하는 표 입니다">
			<caption>사진정보</caption>
			<colgroup>
				<col width="15%" />
				<col width="*">
			</colgroup>
			<tbody>
				<tr>
					<th>제목</th>
					<td><input type="text" name="nttSj" id="nttSj" class="inp null_false" title="제목" value="${photoInfoVo.nttSj}"/></td>
				</tr>
				<tr>
					<th>분류</th>
					<td>
						<c:choose>
							<c:when test="${photoInfoVo.bizType eq 'BIZ' or bizType eq 'BIZ'}"> <%-- 조성사업인 경우 --%>
								<select name="makeBizCl" id="makeBizCl" title="앨범분류체계" class="inp null_false">
									<option value="">선택</option>
									<c:forEach var="cmmCode" items="${cmmCodeList}">							
										<option value="${cmmCode.code}" <c:if test="${cmmCode.code eq photoInfoVo.makeBizCl}">selected="selected"</c:if> >${cmmCode.codeNm}</option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise> <%-- 숲체험교육인 경우 --%>
								<select name="frtExprnCl" id="frtExprnCl" title="앨범분류체계" class="inp null_false">
									<option value="">선택</option>
									<c:forEach var="cmmCode" items="${cmmCodeList}">							
										<option value="${cmmCode.code}" <c:if test="${cmmCode.code eq photoInfoVo.frtExprnCl}">selected="selected"</c:if> >${cmmCode.codeNm}</option>
									</c:forEach>
								</select>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<textarea name="nttCn" id="nttCn" cols="10" rows="10" class="inp null_false" title="본문" style="width:80%; height:300px;">${photoInfoVo.nttCn}</textarea>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="atchFileId" id="atch_file_id" class="inp null_false" value="${photoInfoVo.atchFileId}" title="파일첨부" />
	</form>

	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${photoInfoVo.atchFileId}" />
		<c:param name="updateFlag" value="Y" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="photoinfo" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
		<c:param name="formAjaxJs" value="add" />
	</c:import>
	<%-- 파일첨부 Import --%>
	
	<div class="btn_r">
		<c:choose>
			<c:when test="${formType eq '등록'}">
				<input type="image" src="${MNG_IMG}/btn/btn_regist.gif" alt="등록하기" onclick="return checkForm();"/>
			</c:when>
			<c:otherwise>
				<input type="image" src="${MNG_IMG}/btn/btn_modify.gif" alt="수정하기" onclick="return checkForm();"/>
			</c:otherwise>
		</c:choose>
		<input type="image" src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소하기" onclick="return resetForm();"/>		
		<c:url var="listUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do">
			<c:param name="year" value="${param.year}" />
			<c:choose>
				<c:when test="${empty photoInfoVo}">
					<c:param name="biz_id" value="${param.biz_id}" />
				</c:when>
				<c:otherwise>
					<c:param name="biz_id" value="${photoInfoVo.bizId}" />
				</c:otherwise>
			</c:choose>
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		</c:url>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록으로"/></a>						
	</div>
	
<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />