<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 사진정보"/>
</c:import>

<script type="text/javascript">
</script>

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<!-- 사업관리 - 사업현황 - 사진정보 -->
	
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>

	<table class="chart2" summary="사진정보출력하는 표 입니다">
		<caption>사진정보</caption>
		<colgroup>
			<col width="15%" />
			<col width="*">
		</colgroup>
		<tbody>
			<tr>
				<th>제목</th>
				<td><c:out value="${photoInfoVo.nttSj}" escapeXml="false" /></td>
			</tr>
			<tr>
				<th>분류</th>
				<td>
					<c:choose>
						<c:when test="${photoInfoVo.bizType eq 'BIZ'}">
							<c:set var="compareCode" value="${photoInfoVo.makeBizCl}" />
						</c:when>
						<c:otherwise>
							<c:set var="compareCode" value="${photoInfoVo.frtExprnCl}" />
						</c:otherwise>
					</c:choose>
					<c:forEach var="cmmCode" items="${cmmCodeList}">							
						<c:if test="${cmmCode.code eq compareCode}"><c:out value="${cmmCode.codeNm}" /></c:if>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<c:out value="${fn:replace(photoInfoVo.nttCn, newLineChar, '<br/>')}" escapeXml="false"/>
				</td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td>
					<ul class="file_list">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${photoInfoVo.atchFileId}" />
							<c:param name="style" value="gfund" />
						</c:import>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${photoInfoVo.bizId}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		</c:url>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록으로"/></a>						
	</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />