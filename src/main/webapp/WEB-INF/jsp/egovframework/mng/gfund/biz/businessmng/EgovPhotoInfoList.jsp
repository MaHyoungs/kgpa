<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		<c:param name="bizType" value="${bizType}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 사진정보"/>
</c:import>

<script type="text/javascript">
</script>

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<!-- 사업관리 - 사업관리 - 사진정보 -->	
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>

		<p class="total">총  문서수 ${paginationInfo.totalRecordCount}개 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
		<table class="chart_board" border="0" style="border:0px none; background-image: none;">
		<caption>사진정보</caption>
		<colgroup>
			<col width="33%"/>
			<col width="33%"/>
			<col width="33%"/>
		</colgroup>
		<tbody>
		<c:if test="${resultCnt > 0}">
			<tr>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<td style="border:0px none">
					<c:url var="editUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoUpdtView.do">
						<c:param name="year" value="${param.year}" />
						<c:param name="biz_id" value="${param.biz_id}" />
						<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
						<c:param name="photoInfoId" value="${result.photoInfoId}" />
						<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					</c:url>
		        	<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
		        	<c:url var="delUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoDelete.do">
		        		<c:param name="year" value="${param.year}" />
						<c:param name="biz_id" value="${param.biz_id}" />
						<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
						<c:param name="photoInfoId" value="${result.photoInfoId}" />
		        		<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					</c:url>	
		        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
		        	<br/>
					<c:url var="downLoadUrl" value="/cmm/fms/FileDown.do">
						<c:param name="atchFileId" value="${result.atchFileId}"/>
						<c:param name="fileSn" value="${result.maxFileSn}"/>
					</c:url>
					<c:url var="viewUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoSelectView.do">
					  	<c:param name="year" value="${param.year}" />
						<c:param name="biz_id" value="${param.biz_id}" />
						<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					  	<c:param name="photoInfoId" value="${result.photoInfoId}" />
					  	<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				    </c:url>
					<a href="${viewUrl}"><img src='${downLoadUrl}' alt='' width="200px" height=""/></a>
					<br/>
					<c:out value="${result.nttSj}" escapeXml="false" />
				</td>
			</c:forEach>
			</tr>
		</c:if>
		<c:if test="${empty resultList or resultCnt < 1}">
			<tr>
			    <td colspan="5" style="border:0px none;">
			    	자료가 없습니다.
				</td>
			</tr>
		</c:if>
		</tbody>
		</table>
		
		<div class="btn_r">
			<a href='<c:url value="/mng/gfund/biz/businessmng/EgovPhotoInfoAddView.do${_BASE_PARAM}"/>' >
				<img src="${_IMG}/btn/btn_regist.gif" />
			</a>
		</div>
	
		<div id="paging">
			<c:url var="pageUrl" value="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do">
				<c:param name="year" value="${param.year}" />
				<c:param name="biz_id" value="${param.biz_id}" />
				<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			</c:url>
	
			<c:if test="${not empty paginationInfo}">
				<ul>
					<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				</ul>
			</c:if>
		</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />