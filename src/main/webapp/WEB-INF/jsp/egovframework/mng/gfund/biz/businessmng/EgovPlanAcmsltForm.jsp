<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 사업현황 - 월별사업추진계획 및 실적"/>
</c:import>

<script type="text/javascript">
</script>

<c:set var="formAction" value="/mng/gfund/biz/businessmng/EgovPlanAcmsltSelectUpdt.do${_BASE_PARAM}" />

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<!-- 사업관리 - 사업현황 - 월별사업추진계획 및 실적 -->
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>
	
	<div class="mtab">
		<ul>
			<li class="active">
				<a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">월별사업추진계획 및 실적 </a>
			</li>
			<c:if test="${bizType eq 'EDU'}">
			<li>
				<span>|</span>
				<a href="/mng/gfund/biz/businessmng/EgovNmprAcmsltUpdtView.do${_BASE_PARAM}">월별인원실적</a>
			</li>
			</c:if>
		</ul>
	</div>
	
	<%-- 사업관리 > 사업현황 > 월별사업추진계획 및 실적 --%>
	<form name="nmprAcmsltForm" id="nmprAcmsltForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">
	<div class="input_area">
	<h3 class="icon1">월별 사업추진계획</h3>
		<c:if test="${empty planAcmsltVo}">
			<input type="hidden" name="frstRegisterId" id="frstRegisterId" value="${USER_INFO.id}" />
		</c:if>
		<c:if test="${not empty planAcmsltVo}">
			<input type="hidden" name="lastUpdusrId" id="lastUpdusrId" value="${USER_INFO.id}" />
		</c:if>
		<table class="chart2" summary="월별 사업추진계획 을 입력하는 표 입니다">
			<caption>월별 사업추진계획</caption>
			<colgroup>
				<col width="15%" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th>1월</th>
					<td><input type="text" name="mt1PrtnPlan" id="mt1PrtnPlan" class="inp null_false" title="1월 추진계획" value="${planAcmsltVo.mt1PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>2월</th>
					<td><input type="text" name="mt2PrtnPlan" id="mt2PrtnPlan" class="inp null_false" title="2월 추진계획" value="${planAcmsltVo.mt2PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>3월</th>
					<td><input type="text" name="mt3PrtnPlan" id="mt3PrtnPlan" class="inp null_false" title="3월 추진계획" value="${planAcmsltVo.mt3PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>4월</th>
					<td><input type="text" name="mt4PrtnPlan" id="mt4PrtnPlan" class="inp null_false" title="4월 추진계획" value="${planAcmsltVo.mt4PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>5월</th>
					<td><input type="text" name="mt5PrtnPlan" id="mt5PrtnPlan" class="inp null_false" title="5월 추진계획" value="${planAcmsltVo.mt5PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>6월</th>
					<td><input type="text" name="mt6PrtnPlan" id="mt6PrtnPlan" class="inp null_false" title="6월 추진계획" value="${planAcmsltVo.mt6PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>7월</th>
					<td><input type="text" name="mt7PrtnPlan" id="mt7PrtnPlan" class="inp null_false" title="7월 추진계획" value="${planAcmsltVo.mt7PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>8월</th>
					<td><input type="text" name="mt8PrtnPlan" id="mt8PrtnPlan" class="inp null_false" title="8월 추진계획" value="${planAcmsltVo.mt8PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>9월</th>
					<td><input type="text" name="mt9PrtnPlan" id="mt9PrtnPlan" class="inp null_false" title="9월 추진계획" value="${planAcmsltVo.mt9PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>10월</th>
					<td><input type="text" name="mt10PrtnPlan" id="mt10PrtnPlan" class="inp null_false" title="10월 추진계획" value="${planAcmsltVo.mt10PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>11월</th>
					<td><input type="text" name="mt11PrtnPlan" id="mt11PrtnPlan" class="inp null_false" title="11월 추진계획" value="${planAcmsltVo.mt11PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>12월</th>
					<td><input type="text" name="mt12PrtnPlan" id="mt12PrtnPlan" class="inp null_false" title="12월 추진계획" value="${planAcmsltVo.mt12PrtnPlan}" placeholder="현재 월에 계획이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class="input_area">
	<h3 class="icon1">월별 사업추진실적</h3>
		<table class="chart2" summary="월별 사업추진실적을 입력하는 표 입니다">
			<caption>월별 사업추진계</caption>
			<colgroup>
				<col width="15%" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th>1월</th>
					<td><input type="text" name="mt1PrtnAcmslt" id="mt1PrtnAcmslt" class="inp null_false" title="1월 추진실적" value="${planAcmsltVo.mt1PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>2월</th>
					<td><input type="text" name="mt2PrtnAcmslt" id="mt2PrtnAcmslt" class="inp null_false" title="2월 추진실적" value="${planAcmsltVo.mt2PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>3월</th>
					<td><input type="text" name="mt3PrtnAcmslt" id="mt3PrtnAcmslt" class="inp null_false" title="3월 추진실적" value="${planAcmsltVo.mt3PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>4월</th>
					<td><input type="text" name="mt4PrtnAcmslt" id="mt4PrtnAcmslt" class="inp null_false" title="4월 추진실적" value="${planAcmsltVo.mt4PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>5월</th>
					<td><input type="text" name="mt5PrtnAcmslt" id="mt5PrtnAcmslt" class="inp null_false" title="5월 추진실적" value="${planAcmsltVo.mt5PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>6월</th>
					<td><input type="text" name="mt6PrtnAcmslt" id="mt6PrtnAcmslt" class="inp null_false" title="6월 추진실적" value="${planAcmsltVo.mt6PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>7월</th>
					<td><input type="text" name="mt7PrtnAcmslt" id="mt7PrtnAcmslt" class="inp null_false" title="7월 추진실적" value="${planAcmsltVo.mt7PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>8월</th>
					<td><input type="text" name="mt8PrtnAcmslt" id="mt8PrtnAcmslt" class="inp null_false" title="8월 추진실적" value="${planAcmsltVo.mt8PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>9월</th>
					<td><input type="text" name="mt9PrtnAcmslt" id="mt9PrtnAcmslt" class="inp null_false" title="9월 추진실적" value="${planAcmsltVo.mt9PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>10월</th>
					<td><input type="text" name="mt10PrtnAcmslt" id="mt10PrtnAcmslt" class="inp null_false" title="10월 추진실적" value="${planAcmsltVo.mt10PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>11월</th>
					<td><input type="text" name="mt11PrtnAcmslt" id="mt11PrtnAcmslt" class="inp null_false" title="11월 추진실적" value="${planAcmsltVo.mt11PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
				<tr>
					<th>12월</th>
					<td><input type="text" name="mt12PrtnAcmslt" id="mt12PrtnAcmslt" class="inp null_false" title="12월 추진실적" value="${planAcmsltVo.mt12PrtnAcmslt}" placeholder="현재 월에 실적이 없는 경우 하이픈(-)을 입력하세요" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	</form>
	
	<div class="btn_r">
		<span class="cbtn1"><button type="button" onclick="$('#nmprAcmsltForm').submit();">저장</button></span>
	</div>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />