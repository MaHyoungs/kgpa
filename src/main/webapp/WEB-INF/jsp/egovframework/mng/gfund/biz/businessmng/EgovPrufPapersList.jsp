<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 관련증빙서"/>
</c:import>

<script type="text/javascript">
</script>

<div id="cntnts">

	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<!-- 사업관리 - 사업현황 - 관련증빙서 -->
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
			<li class="active"><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
			<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
		</ul>
	</div>
	
	<p class="total">총  문서수 ${paginationInfo.totalRecordCount}개 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
	<table class="chart_board" summary="관련증빙서 사업관리 목록을 출력하는 표입니다." >
	<caption class="hdn">관련증빙서 목록</caption>
	<colgroup>
		<col width="70"/>
		<col width="150"/>
		<col width="*"/>
		<col width="100"/>
		<col width="100"/>
		<col width="100"/>
	</colgroup>
	<thead>
		<tr>
			<th>번호</th>
			<th>분류</th>
			<th>제목</th>
			<th>작성자</th>
			<th>작성일</th>
			<th>관리</th>
		</tr>
	</thead>
	<tbody>
	<c:if test="${resultCnt > 0}">
	<c:forEach var="result" items="${resultList}" varStatus="status">
		<tr>
			<td>
				<fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/>										
			</td>
			<td>
				<c:forEach var="cmmCode" items="${cmmCodeList}">							
					<c:if test="${cmmCode.code eq result.prufPapersCl}"><c:out value="${cmmCode.codeNm}" /></c:if>
				</c:forEach>
			</td>
			<td><c:out value="${result.nttSj}" escapeXml="false" /></td>
			<td><c:out value="${result.frstRegisterId}" escapeXml="false" /></td>
			<td><fmt:formatDate pattern="yyyy-MM-dd" value="${result.frstRegistPnttm}"/></td>
			<td>
				<c:url var="viewUrl" value="/mng/gfund/biz/businessmng/EgovPrufPapersSelectView.do">
				  	<c:param name="year" value="${param.year}" />
				  	<c:param name="biz_id" value="${param.biz_id}" />
				  	<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
				  	<c:param name="rppId" value="${result.rppId}" />
				  	<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			    </c:url>
			    <a href="${viewUrl}"><img src="${_IMG}/btn/btn_select.gif"/></a>
				<c:url var="editUrl" value="/mng/gfund/biz/businessmng/EgovPrufPapersUpdtView.do">
					<c:param name="year" value="${param.year}" />
					<c:param name="biz_id" value="${param.biz_id}" />
					<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
					<c:param name="rppId" value="${result.rppId}" />
					<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>
	        	<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
	        	<c:url var="delUrl" value="/mng/gfund/biz/businessmng/EgovPrufPapersDelete.do">	        		
	        		<c:param name="year" value="${param.year}" />
	        		<c:param name="biz_id" value="${param.biz_id}" />
	        		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	        		<c:param name="rppId" value="${result.rppId}" />
	        		<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>	
	        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
		    </td>
		</tr>
	</c:forEach>
	</c:if>
	<c:if test="${empty resultList or resultCnt < 1}">
		<tr>
		    <td colspan="6">
		    	자료가 없습니다.
			</td>
		</tr>
	</c:if>
	</tbody>
	</table>

	<div class="btn_r">
		<a href='<c:url value="/mng/gfund/biz/businessmng/EgovPrufPapersAddView.do${_BASE_PARAM}"/>' >
			<img src="${_IMG}/btn/btn_regist.gif" />
		</a>
	</div>
	
	<div id="paging">
		<c:url var="pageUrl" value="/mng/gfund/biz/businessmng/EgovPrufPapersList.do">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${param.biz_id}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		</c:url>
		
		<c:if test="${not empty paginationInfo}">
			<ul>
				<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			</ul>
		</c:if>
	</div>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />