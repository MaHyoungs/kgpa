<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 기본정보 수정"/>
</c:import>

<script src="/gfund/js/proposalForm.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javascript">
	<c:if test="${not empty param.saveRs}">
		<c:if test="${param.saveRs eq 1}">
			alert('현재까지 입력하신 내역이 저장 되었습니다.');
		</c:if>
	</c:if>

	//제안서 Sumbit
	function fnBasicInformationFormSubmit(){
		if($('#propse_flag').val() == 'STEP02'){
			if($('#area').val() == ''){
				if($('#sido').val() == ''){
					alert($('#sido').attr('title') + '를(을) 선택해 주십시오.');
					$('#sido').focus();
					return false;
				}else if($('#sidogungu').val() == ''){
					alert($('#sidogungu').attr('title') + '를(을) 선택해 주십시오.');
					$('#sidogungu').focus();
					return false;
				}
			}
			if(fn_text_null_check($('input[type=text], textarea, input[type=hidden].null_false'))){
				fnInputNameChange();
				return confirm("제출 후에는 수정이 불가능 합니다.\n제출 하시겠습니까?");
			}else{
				return false
			}
		}else{
			fnInputNameChange();
			return true;
		}
	}
</script>

<c:set var="biz_sort_nm" value="" />
<c:forEach var="btc" items="${bizTyCc}"><c:if test="${param.biz_ty_code eq btc.code or bsifVo.biz_ty_code eq btc.code}"><c:set var="biz_sort_nm" value="${btc.codeNm}" /></c:if></c:forEach>

<div class="ntit">
	<strong>사업명</strong>: <c:out value="${bsifVo.biz_nm}" escapeXml="false" />
</div>

<div class="tab2">
	<ul>
		<li class="active"><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
	</ul>
</div>

<div id="content">
	<h3 class="icon1">최종사업계획서</h3>

	<form action="#">
		<div class="select_bar">
			<strong>기관(시설, 단체, 학교) 지역선택</strong>
			<select name="sido" id="sido" title="시,도">
				<option value="">시.도</option>
			</select>
			<select name="sidogungu" id="sidogungu" title="시,군,구">
				<option value="">시.군.구</option>
			</select>
		</div>
	</form>

	<c:set var="biz_ty_code" />
	<c:choose>
		<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
		<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
		<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
	</c:choose>
	<div class="input_area">
		<form name="basicInformationForm" id="basicInformationForm" action="/mng/gfund/biz/bassinfo/basicInformationSave.do" method="post" onsubmit="return fnBasicInformationFormSubmit();">
			<%-- 제안서 ID --%>
			<input type="hidden" name="biz_id" id="biz_id" value="${bsifVo.biz_id}"/>

			<%-- 제안서 연도 --%>
			<input type="hidden" name="year" id="year" value="${bsifVo.year}"/>

			<c:if test="${bsifVo.propse_flag eq 'STEP04'}">
				<input type="hidden" name="propse_biz_id" id="propse_biz_id" value="${bsifVo.biz_id}"/>
			</c:if>

			<%-- 사업분류 --%>
			<input type="hidden" name="biz_ty_code" id="biz_ty_code" value="${biz_ty_code}"/>

			<%--전화번호 및 우편번호 분할 원본 --%>
			<input type="hidden" name="biz_zip" id="biz_zip" value="${bsifVo.biz_zip}"/>

			<%--제안요약서(기본정보) 지역 --%>
			<input type="hidden" name="area" id="area" value="${bsifVo.area}"/>

			<%--제안단계 --%>
			<input type="hidden" name="propse_flag" id="propse_flag" value="${bsifVo.propse_flag}"/>

			<%--제출구분 제안서:PRST01, 최종제출:PRST02 --%>
			<input type="hidden" name="prst_ty_cc" id="prst_ty_cc" value="${bsifVo.prst_ty_cc}"/>

			<%--최종사업계획서제출여부 Y , N --%>
			<input type="hidden" name="last_biz_plan_presentn_at" id="last_biz_plan_presentn_at" value="${bsifVo.last_biz_plan_presentn_at}"/>

			<%-- 제안서 기본 Form --%>
			<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeBasicInformationForm.jsp"%>
			<%-- 제안서 기본 Form --%>

			<%-- 파일첨부 ID --%>
			<input type="hidden" name="atch_file_id" id="atch_file_id" class="null_false" value="${bsifVo.atch_file_id}" title="첨부파일"/>
			<input type="hidden" name="gatch_file_id" id="gatch_file_id" value="${bsifVo.atch_file_id}"/>
		</form>

		<div class="pbg mB20">
			<strong>필수제출서류</strong><br/><br/>
			※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
			※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
			&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
			※ 특수교육시설의 경우 개인정보이용동의서만 제출합니다.
		</div>

		<%-- 파일첨부 Import --%>
		<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
			<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
			<c:param name="updateFlag" value="Y" />
			<c:param name="pathKey" value="Gfund.fileStorePath" />
			<c:param name="appendPath" value="basicInformation" />
			<c:param name="maxSize" value="31457280" />
			<c:param name="maxCount" value="10" />
			<c:param name="formAjaxJs" value="add" />
		</c:import>
		<%-- 파일첨부 Import --%>

		<ul class="icon2_list mT40">
			<li>‘<strong>저장</strong>’은 지금까지 작성한 내용을 저장합니다.</li>
			<li>‘<strong>취소</strong>’는 이전 화면으로 이동합니다.</li>
		</ul>
	</div>

	<div class="btn_c">
		<span class="cbtn"><button type="button" onclick="$('#basicInformationForm').submit();">저장</button></span>
		<span class="cbtn1">
			<a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">취소</a>
		</span>
	</div>
</div>

<script type="text/javascript">
	(function(){
		//파일 업로드 Form Ajax
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
