<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="BUSINESS_MANAGE"/>
	<c:param name="title" value="녹색자금통합시스템 - 사업관리 - 기본정보 조회"/>
</c:import>

<div class="ntit">
	<strong>사업명</strong>: <c:out value="${bsifVo.biz_nm}" escapeXml="false" />
</div>

<div class="tab2">
	<ul>
		<li class="active"><a href="/mng/gfund/biz/businessmng/finalProposalView.do${_BASE_PARAM}">기본정보</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPhotoInfoList.do${_BASE_PARAM}">사진정보</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPrufPapersList.do${_BASE_PARAM}">관련증빙서</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovPlanAcmsltUpdtView.do${_BASE_PARAM}">사업현황</a></li>
		<li><a href="/mng/gfund/biz/businessmng/EgovChckListUpdtView.do${_BASE_PARAM}">지도점검</a></li>
	</ul>
</div>


<h3 class="icon1">최종사업계획서</h3>

<div class="select_bar">
	<strong>${bsifVo.area}</strong>
</div>
<c:set var="biz_ty_code" />
<c:choose>
	<c:when test="${empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:when>
	<c:when test="${not empty bsifVo.biz_ty_code}"><c:set var="biz_ty_code" value="${bsifVo.biz_ty_code}" /></c:when>
	<c:otherwise><c:set var="biz_ty_code" value="${param.biz_ty_code}" /></c:otherwise>
</c:choose>
<div class="input_area">

	<%-- 제안서 기본 Form --%>
	<%@include file="/WEB-INF/jsp/egovframework/gfund/biz/bassinfo/includeBasicInformationView.jsp"%>
	<%-- 제안서 기본 Form --%>

	<div class="pbg mB20">
		<strong>필수제출서류</strong><br/><br/>
		※ 사업제안서와 요약서, 사업계획서는 <strong>하나의 한글파일</strong>로 제출합니다. (공통) <br/>
		※ 자격요건 증빙을 위해 복지시설신고증, 부동산등기부 등본, 법인설립허가증, 개인정보이용동의서, 구조안전진단보고서(옥상녹화의 경우에 한함)를 <br/>
		&nbsp;&nbsp;&nbsp;&nbsp;반드시 첨부하여야 하며, 미제출 시 선정에서 제외됩니다. <br/>
		※ 특수교육시설의 경우 개인정보이용동의서만 제출합니다.
	</div>

	<h3 class="icon1">최종사업계획서 첨부파일</h3>
	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${bsifVo.atch_file_id}" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="basicInformation" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
	</c:import>
	<%-- 파일첨부 Import --%>
	
<c:if test="${not empty propseVo.atch_file_id}">
	<h3 class="icon1">사업제안서 첨부파일</h3>
	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${propseVo.atch_file_id}" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="basicInformation" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
	</c:import>
	<%-- 파일첨부 Import --%>
</c:if>

</div>


<div class="btn_c">
	<span class="cbtn">
		<a href="/mng/gfund/biz/businessmng/finalProposalForm.do?biz_id=${bsifVo.biz_id}">수정</a>
	</span>
	<span class="cbtn1">
		<a href="/mng/gfund/biz/businessmng/EgovBusinessManageList.do">목록</a>
	</span>
</div>


<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
