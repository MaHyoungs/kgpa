<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang.time.DateFormatUtils"%>

<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="MNG_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="CMMN_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="_PREFIX" value="/mng/gfund/biz/productmng"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="RESULTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 결과산출 - 설문조사관리"/>
</c:import>

<script type='text/javascript'>
</script>

<!-- List -->
<div id="cntnts">
	
<form:form commandName="searchVO" name="listForm" id="listForm" method="post">

	<p class="total">총 게시물 ${paginationInfo.totalRecordCount}개 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

	<table class="chart_board">
		<thead>
			<tr>
				<th align="center">번호</th>
				<th align="center">유형</th>
				<th align="center">제목</th>
				<th align="center">기간</th>
				<th align="center">진행상태</th>
				<th align="center">등록일</th>
				<th align="center">응시현황</th>
				<th align="center">관리</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>																						
				<td align="center" class="listtd"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" /></td>
				<td align="center" class="listtd">
					<c:choose>
					<c:when test="${result.schdulClCode eq '1'}">행사및일반일정</c:when>
					<c:when test="${result.schdulClCode eq '2'}">이벤트</c:when>
					<c:when test="${result.schdulClCode eq '3'}">설문조사</c:when>
					</c:choose>
				</td>
				<td align="center" class="tit"><c:out value="${result.schdulNm}"/></td>
				
				<td align="center" class="listtd">
					<c:out value="${fn:substring(result.schdulBgnde, 0,4)}.${fn:substring(result.schdulBgnde, 4,6)}.${fn:substring(result.schdulBgnde, 6,8)}"/>
					 ~ <c:out value="${fn:substring(result.schdulEndde, 0,4)}.${fn:substring(result.schdulEndde, 4,6)}.${fn:substring(result.schdulEndde, 6,8)}"/>&nbsp;</td>
					 
				<td align="center" class="listtd">
				<c:choose>
					<c:when test="${result.state eq '1' and result.useAt eq 'Y'}">
						<font color="red"><img src="${MNG_IMG}/evt/calendar_ing.gif" alt="진행중" /></font>
					</c:when>
					<c:otherwise>
						<c:choose>
						<c:when test="${result.state eq '1' and result.useAt eq 'N'}"><img src="${MNG_IMG}/evt/calendar_ready.gif" alt="대기중" /></c:when>
						<c:when test="${result.useAt eq 'C'}"><img src="${MNG_IMG}/evt/calendar_clos.gif" alt="마감" /></c:when>
						<c:when test="${result.useAt eq 'R'}"><img src="${MNG_IMG}/evt/calendar_resrve.gif" alt="보류" /></c:when>
						<c:otherwise><img src="${MNG_IMG}/evt/calendar_clos.gif" alt="마감" /></c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>

				</td>
				
				<td align="center" class="listtd"><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/>&nbsp;</td>

				<td align="center">
	            	<c:choose>
						<c:when test="${result.schdulClCode ne '1'}">
						<c:url var="adhrncUrl" value="${_PREFIX}/selectComtneventadhrncList.do">
							<c:param name="siteId" value="${searchVO.siteId}"/>
							<c:param name="schdulId" value="${result.schdulId}" />
							<c:param name="searchSe" value="${searchVO.searchSe}" />
							<c:if test="${!empty searchVO.mode}"><c:param name="mode" value="${searchVO.mode}" /></c:if>
							<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
							<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
						</c:url>
						<a href="<c:out value="${adhrncUrl}"/>"><img src="${MNG_IMG}/btn/construct_n.gif" alt="응시현황" width="70" border="0" /></a>
						</c:when>
						<c:otherwise>-</c:otherwise>
					</c:choose>
	            </td>
	            
	            <td align="center">
		            <c:url var="viewUrl" value="${_PREFIX}/updateComtnschdulinfoView.do">
		            	<c:param name="siteId" value="${searchVO.siteId}"/>
						<c:param name="schdulId" value="${result.schdulId}" />
						<c:param name="searchSe" value="${searchVO.searchSe}" />
						<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
						<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
					</c:url>
	            	<a href="<c:out value="${viewUrl}"/>"><img src="${MNG_IMG}/btn/edit.gif" alt="수정하기" /></a>

	            	<c:url var="delUrl" value="deleteComtnschdulinfo.do">
	            		<c:param name="siteId" value="${searchVO.siteId}"/>
						<c:param name="schdulId" value="${result.schdulId}" />
						<c:param name="searchSe" value="${searchVO.searchSe}" />
						<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
						<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
					</c:url>
	            	<a href="<c:out value="${delUrl}"/>" onclick="return confirm('삭제하시겠습니까?');"><img src="${MNG_IMG}/btn/del.gif" alt="삭제하기" /></a>
	            </td>
			</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
		      <tr>
		      	<td class="listtd" colspan="11">
		        	<spring:message code="common.nodata.msg" />
		        </td>
		      </tr>
		    </c:if>
			</tbody>
		</table>
</form:form>

	
	
	<!-- /List -->
	<div id="paging"> 
		<c:url var="pageUrl" value="${_PREFIX}/selectSchdulinfoList.do?">
			<c:param name="siteId" value="${searchVO.siteId}"/>
			<c:param name="searchSe" value="${searchVO.searchSe}" />
			<c:if test="${not empty searchVO.searchSe}"><c:param name="schdulClCode" value="${searchVO.searchSe}" /></c:if>
			<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	    </c:url>
	    <c:set var="pageParam"><c:out value="${pageUrl}"/></c:set>
		<c:if test="${not empty paginationInfo}">
			<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageParam}"  />
		</c:if>
	</div>
	
	
	<div id="bbs_search">
		<form name="frm" method="post" action="<c:url value='${_PREFIX}/selectSchdulinfoList.do'/>">
		<input type="hidden" name="siteId" value="${searchVO.siteId}"/>
		<input type="hidden" name="searchSe" value="${searchVO.searchSe}" />
		<label for="ftext" class="hdn">분류검색</label>
		<select name="searchCondition" id="ftext">
			<option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if> >제목</option>
			<option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if> >내용</option>
		</select>
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="inp_s" id="inp_text" />
		<input type=image src="${MNG_IMG}/board/btn_search.gif" alt="검색" />
		</form>
	 </div>

	<c:if test="${not empty searchVO.siteId }">
		<div class="btn_r">
			<c:url var="addUrl" value="${_PREFIX}/addComtnschdulinfoView.do">
				<c:param name="siteId" value="${searchVO.siteId}"/>
				<c:param name="schdulClCode" value="${searchVO.searchSe}" />
				<c:if test="${not empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
				<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
				<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
			</c:url>
			<a href="<c:out value="${addUrl}"/>"><img src="${MNG_IMG}/btn/btn_creat.gif" alt="작성" /></a>
		</div>
	</c:if>
	
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	
