<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="RESULTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 결과산출 - 평가결과"/>
</c:import>
<c:set var="formAction" value="/mng/gfund/biz/productmng/EgovEvaluationResultSelectUpdt.do${_BASE_PARAM}" />

<script type="text/javascript">
</script>

<div id="cntnts">

	<%-- 결과산출 > 결과산출 > 평가결과 --%>
	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>
	
	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
			<li><a href="/mng/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
			<li class="active"><a href="/mng/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
		</ul>
	</div>

	<h3 class="icon1">평가결과</h3>
	<form name="evalRstForm" id="evalRstForm" action="${formAction}" method="post">
	<div class="pbg mB20">
		<strong>
<c:choose>
	<c:when test="${empty basicInformationVo.last_evl_result or null eq basicInformationVo.last_evl_result or fn:length(basicInformationVo.last_evl_result) < 1}">			
		진행하신 사업의 평가 결과는 "
		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM216" />	
			<c:param name="chVal" value="RS01" />
			<c:param name="elType" value="codeName" />
		</c:import>
		" 입니다.
		<br/><br/>
		- 등급
		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM216" />	
			<c:param name="elName" value="last_evl_result" />
			<c:param name="chVal" value="RS01" />			
			<c:param name="elType" value="radio" />
		</c:import>
	</c:when>
	<c:otherwise>
		진행하신 사업의 평가 결과는 "
		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM216" />	
			<c:param name="chVal" value="${basicInformationVo.last_evl_result}" />
			<c:param name="elType" value="codeName" />
		</c:import>
		" 입니다.
		<br/><br/>
		- 등급
		<br/>
		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM216" />	
			<c:param name="elName" value="last_evl_result" />
			<c:param name="chVal" value="${basicInformationVo.last_evl_result}" />
			<c:param name="elType" value="radio" />
		</c:import>
	</c:otherwise>
</c:choose>
		</strong>
	</div>
	<div class="btn_c">
		<span class="cbtn"><button type="button" onclick="$('#evalRstForm').submit();">저장</button></span>
	</div>
	</form>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />