<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="year" value="${param.year}" />
		<c:param name="biz_id" value="${param.biz_id}" />
		<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="RESULTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 결과산출 - 최종정산"/>
</c:import>

<c:set var="formAction" value="/mng/gfund/biz/productmng/EgovExcclcDelete.do${_BASE_PARAM}" />

<script type="text/javascript">
<c:if test="${not empty excclcmessage}">
	alert('${excclcmessage}');
</c:if>
</script>

<div id="cntnts">

	<%-- 결과산출 > 결과산출 > 최종정산 목록 --%>
	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<div class="tab2">
		<ul>
			<li><a href="/mng/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
			<li class="active"><a href="/mng/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
			<li><a href="/mng/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
		</ul>
	</div>
	
	<h3 class="icon1">최종정산</h3>
<c:choose>
<c:when test="${not empty excclcVo}"> <%-- 조회용 화면으로 제공, 정산초기화 버튼 활성화 --%>
	<form name="excclcForm" id="excclcForm" action="${formAction}" method="post">
	<input type="hidden" name="bizId" id="bizId" value="${excclcVo.bizId}" />
	<table  class="chart1" summary="최종정산 결과산출 출력하는 표 입니다">
		<caption>최종정산</caption>
		<colgroup>
			<col width="10%" />
			<col width="10%" />
			<col width="11%">
			<col width="11%">
			<col width="11%">
			<col width="*">
		</colgroup>
		<thead>	
			<tr>
				<th colspan="2">구분</th>
				<th>사업비<br/>(A)</th>
				<th>집행액<br/>(B)</th>
				<th>반납액<br/>(A-B)</th>
				<th>반납사유</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2">사업비</td>				
				<td><fmt:formatNumber value="${excclcVo.decsnAmount}" groupingUsed="true"/>원</td>
				<td><fmt:formatNumber value="${excclcVo.excutAmount}" groupingUsed="true"/>원</td>
				<td><fmt:formatNumber value="${excclcVo.decsnAmount - excclcVo.excutAmount}" groupingUsed="true"/>원</td>
				<td><c:out value="${fn:replace(excclcVo.rturnResn, newLineChar, '<br/>')}" escapeXml="false"/></td>
			</tr>
			<tr>
				<td rowspan="2">이자발생</td>
				<td>예금이자</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><fmt:formatNumber value="${excclcVo.dpstIntr}" groupingUsed="true"/>원</td>
				<td><c:out value="${fn:replace(excclcVo.dpstIntrResn, newLineChar, '<br/>')}" escapeXml="false"/></td>
			</tr>
			<tr>
				<td>해지이자</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><fmt:formatNumber value="${excclcVo.trmnatIntr}" groupingUsed="true"/>원</td>
				<td><c:out value="${fn:replace(excclcVo.trmnatIntrResn, newLineChar, '<br/>')}" escapeXml="false"/></td>						
			</tr>
		</tbody>
	</table>
	</form>
	
	<div class="btn_c">
		<span class="cbtn"><button type="button" onclick="$('#excclcForm').submit();">정산초기화</button></span>
	</div>
</c:when>
<c:otherwise>
	<table  class="chart1" summary="최종정산 결과산출 출력하는 표 입니다">
		<caption>최종정산</caption>
		<colgroup>
			<col width="10%" />
			<col width="10%" />
			<col width="11%">
			<col width="11%">
			<col width="11%">
			<col width="*">
		</colgroup>
		<thead>	
			<tr>
				<th colspan="2">구분</th>
				<th>사업비<br/>(A)</th>
				<th>집행액<br/>(B)</th>
				<th>반납액<br/>(A-B)</th>
				<th>반납사유</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2">사업비</td>
				<td><fmt:formatNumber value="${decsnAmount}" groupingUsed="true"/>원</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td rowspan="2">이자발생</td>
				<td>예금이자</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>해지이자</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>						
			</tr>
		</tbody>
	</table>
	
	<div class="btn_c">
		<span class="cbtn"><button type="button" onclick="alert('사업자가 정산관련 정보를 입력하지 않았습니다.');">정산초기화</button></span>
	</div>
</c:otherwise>
</c:choose>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />