<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
	<c:if test="${empty lastPapersVo}">
		<c:set var="formType" value="등록" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${param.biz_id}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
		</c:url>
	</c:if>
	<c:if test="${not empty lastPapersVo}">
		<c:set var="formType" value="수정" />
		<c:url var="_BASE_PARAM" value="">
			<c:param name="year" value="${param.year}" />
			<c:param name="biz_id" value="${lastPapersVo.bizId}" />
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:param name="pageIndex" value="${param.pageIndex}" />
		</c:url>
	</c:if>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="RESULTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 결과산출 - 최종서류제출"/>
</c:import>

<script type="text/javascript">
	function resetForm() {
		
		var frm = document.lastPapersForm;
		frm.reset();
	
		return false;
	}
	
	function checkForm() {
		
		if ( $('#tr_file_empty').css('display') != 'none' ) {
			alert('첨부된 파일이 없습니다');	
			return false;
		}
		
		var frm = document.lastPapersForm;
		frm.submit();
		
		return false;
	}
</script>

<c:if test="${empty lastPapersVo}">
	<c:set var="formAction" value="/mng/gfund/biz/productmng/EgovLastPapersInsert.do${_BASE_PARAM}" />
</c:if>
<c:if test="${not empty lastPapersVo}">
	<c:set var="formAction" value="/mng/gfund/biz/productmng/EgovLastPapersSelectUpdt.do${_BASE_PARAM}" />
</c:if>


<div id="cntnts">

	<%-- 결과산출 > 결과산출 > 최종서류 제출  목록 --%>
	<div class="ntit">
		<strong>사업명</strong>: <c:out value="${basicInformationVo.biz_nm}" escapeXml="false" />
	</div>

	<div class="tab2">
		<ul>
			<li class="active"><a href="/mng/gfund/biz/productmng/EgovLastPapersList.do${_BASE_PARAM}">최종서류제출</a></li>
			<li><a href="/mng/gfund/biz/productmng/EgovExcclcAddView.do${_BASE_PARAM}">최종정산</a></li>
			<li><a href="/mng/gfund/biz/productmng/EgovEvaluationResultUpdtView.do${_BASE_PARAM}">평가결과</a></li>
		</ul>
	</div>

	<div class="input_area">
		<h3 class="icon1">증빙서${formType}</h3>
		<form name="lastPapersForm" id="lastPapersForm" action="${formAction}" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">
			<input type="hidden" name="atchFileId" id="atch_file_id" class="inp null_false" value="${lastPapersVo.atchFileId}" title="파일첨부" />
			<input type="hidden" name="fileGroupId" id="fileGroupId" value="${lastPapersVo.atchFileId}"/>
			<input type="hidden" name="bizId" id="bizId" value="${lastPapersVo.bizId}" />
			<input type="hidden" name="lastPapersId" id="lastPapersId" value="${lastPapersVo.lastPapersId}" />
			<c:if test="${empty lastPapersVo}">
				<input type="hidden" name="frstRegisterId" id="frstRegisterId" value="${USER_INFO.id}" />
			</c:if>
			<c:if test="${not empty lastPapersVo}">
				<input type="hidden" name="lastUpdusrId" id="lastUpdusrId" value="${USER_INFO.id}" />
			</c:if>
			<table class="chart1" summary="최종서류제출 결과산출 정보를 ${formType}하는 표 입니다">
				<caption>최종서류제출</caption>
				<colgroup>
					<col width="15%" />
					<col width="*">
				</colgroup>
				<tbody>
					<tr>
						<th>제목</th>
						<td><input type="text" name="nttSj" id="nttSj" class="inp null_false" title="제목" value="${lastPapersVo.nttSj}"/></td>
					</tr>
					<tr>
						<th>분류</th>
						<td>
							<select name="lastPapersCl" id="lastPapersCl" title="증빙서분류" class="inp null_false">
								<option value="">선택</option>
								<c:forEach var="cmmCode" items="${cmmCodeList}">							
									<option value="${cmmCode.code}" <c:if test="${cmmCode.code eq lastPapersVo.lastPapersCl}">selected="selected"</c:if> >${cmmCode.codeNm}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea name="nttCn" id="nttCn" cols="10" rows="10" class="inp null_false" title="본문" style="width:80%; height:300px;">${lastPapersVo.nttCn}</textarea>
						</td>
					</tr>
				</tbody>
			</table>			
		</form>
	</div>
	
	<%-- 파일첨부 Import --%>
	<c:import url="/cmm/fms/selectFileInfsForUpdate2.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${lastPapersVo.atchFileId}" />
		<c:param name="updateFlag" value="Y" />
		<c:param name="pathKey" value="Gfund.fileStorePath" />
		<c:param name="appendPath" value="lastPapers" />
		<c:param name="maxSize" value="31457280" />
		<c:param name="maxCount" value="10" />
		<c:param name="formAjaxJs" value="add" />
	</c:import>
	<%-- 파일첨부 Import --%>
	
	<div class="btn_c">				
		<span class="cbtn1"><button type="submit" onclick="return checkForm();">${formType}</button></span>
		<span class="cbtn"><button type="button" onclick="return resetForm();">취소</button></span>
		<c:url var="listUrl" value="/mng/gfund/biz/productmng/EgovLastPapersList.do">
			<c:choose>
				<c:when test="${empty lastPapersVo}">
					<c:param name="biz_id" value="${param.biz_id}" />
				</c:when>
				<c:otherwise>
					<c:param name="biz_id" value="${lastPapersVo.bizId}" />
				</c:otherwise>
			</c:choose>
			<c:param name="biz_ty_code" value="${param.biz_ty_code}" />
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		</c:url>
		<span class="cbtn"><a href="${listUrl}">이전화면</a></span>
	</div>
	
	<script type="text/javascript">
	(function(){
		var progress=$('.progress');
		var bar = $('.bar');
		$('#boardFileAjaxForm').ajaxForm({
			dataType: 'json',
			beforeSend: function(){
				progress.show();
				var percentVal='0%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete){
				var percentVal=percentComplete-1 + '%';
				progress.width(percentVal);
				bar.html(percentVal);
			},
			success: function(json){
				if(json.rs == 'countOver'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
				}else if(json.rs == 'overflow'){
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
				}else if (json.rs == 'denyExt') {
					$('.progress').hide();
					$('.progress').width("0%");
					$('.bar').html("0%");
					alert("첨부할 수 없는 확장자입니다.");
				}else{
					var percentVal='100%';
					progress.width("99%");
					bar.html(percentVal);
					fnAjaxFileUploadComplete(json);
				}
			},
			complete: function(xhr){
				var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
				var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
				$('#boardFileAjaxForm input[name=uploadfile]').remove();
				file_parent.prepend(file);
			}
		});
	})();
</script>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8" />