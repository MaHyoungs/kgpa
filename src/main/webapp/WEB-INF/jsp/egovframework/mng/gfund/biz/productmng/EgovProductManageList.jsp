<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="GFUND_MANAGE"/>
	<c:param name="depth1" value="RESULTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="녹색자금통합시스템 - 결과산출"/>
</c:import>

<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
<script type="text/javascript">
	$(function(){
		//행정구역 시.도, 시.군.구 OPNE API 이벤트
		if($('#area').val() != ""){
			$('#sido option:eq(0)').text($('#area').val().split(' ')[0]);
			$('#sidogungu option:eq(0)').text($('#area').val().split(' ')[1]);
			fnSidoOpenAPIAdd($('#sido'), 1);
		}else{
			//시도, 시도군구 이벤트
			fnSidoOpenAPIAdd($('#sido'), 1);
		}
		$('#sido').change(function(){
			$('#sidogungu option').remove();
			var opt = $('<option value="">시.군.구</option>');
			$('#sidogungu').append(opt);
			fnSidoOpenAPIAdd($('#sidogungu'), $('#sido').val());
			$('#area').val($("#sido option:selected").text());
		});
		$('#sidogungu').change(function(){
			$('#area').val($('#area').val() + " " + $("#sidogungu option:selected").text());
		});
	});

	//지도점검대상여부 상태 변경  Ajax
	function fnAjaxLastEvlResultModify(biz_id, evlRst, el){
		var url = '/mng/gfund/biz/productmng/ajaxUpdateBasicInformationLastEvlResult.do';
		var data = [];
		data = {biz_id : biz_id, last_evl_result : evlRst};
		var successFn = function(json){
			if(Number(json.rs) > 0){
				alert('변경 되었습니다.');
				$(el).prop('checked', true);
			}
		};
		fn_ajax_json(url, data, successFn, null);
	}	
	
</script>

<h3 class="icon1">사업관리</h3>
<form action="" method="post">
	<div id="bbs_search">

		<select name="year" title="연도">
			<option value="">연도</option>
			<c:forEach var="rs" items="${yearlist}" varStatus="sts">
				<option value="${rs.year}" <c:if test="${bsifVo.year eq rs.year}"> selected="selected"</c:if> >${rs.year}</option>
			</c:forEach>
		</select>

		<select name="sido" id="sido" title="시,도">
			<option value="">시.도</option>
		</select>
		<select name="sidogungu" id="sidogungu" title="시,군,구">
			<option value="">시.군.구</option>
		</select>

		<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
			<c:param name="codeId" value="COM210"/>
			<c:param name="chVal" value="${bsifVo.biz_ty_code}"/>
			<c:param name="elType" value="select"/>
			<c:param name="elName" value="biz_ty_code"/>
			<c:param name="emptyMsg" value="사업분류 선택"/>
			<c:param name="elFast" value="elFast"/>
		</c:import>

		<select name="searchCondition">
			<option value="">키워드</option>
			<option value="1"<c:if test="${bsifVo.searchCondition eq 1}"> selected="selected"</c:if>>사업명</option>
			<option value="2"<c:if test="${bsifVo.searchCondition eq 2}"> selected="selected"</c:if>>기관명</option>
			<option value="3"<c:if test="${bsifVo.searchCondition eq 3}"> selected="selected"</c:if>>대표자</option>
		</select>

		<input type="hidden" name="area" id="area" value="${bsifVo.area}"/>
		<label for="searchKeyword" class="hdn">검색어입력</label>
		<input type="text" id="searchKeyword" name="searchKeyword" value="${bsifVo.searchKeyword}" class="inp" id="inp_text"/>
		<input type=image src="/template/manage/images/btn/btn_search.gif" alt="검색" />
	</div>
</form>

<p class="total">총 사업 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

<table class="chart_board2">
	<colgroup>
		<col width="5%"/>
		<col width="auto"/>
		<col width="auto"/>
		<col width="5%"/>
		<col width="5%"/>
		<col width="5%"/>
		<col width="5%"/>
		<col width="auto"/>
		<col width="auto"/>
		<col width="5%"/>
	</colgroup>
	<thead>
		<tr>
			<th rowspan="2">연도</th>
			<th rowspan="2">사업종류</th>
			<th rowspan="2">사업명</th>
			<th rowspan="2">지역</th>
			<th rowspan="2">사업기간</th>
			<th rowspan="2">사업비</th>
			<th rowspan="2">집행액</th>
			<th colspan="3">반납액</th>
			<th rowspan="2">평가결과</th>
			<th rowspan="2">관리</th>
		</tr>
		<tr>
			<th>사업비잔액</th>
			<th>이자</th>
			<th>계</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="rs" items="${bsifList}" varStatus="sts">
			<tr>
				<td class="listtd">${rs.year}년</td>
				<td class="listtd">${rs.biz_ty_code_nm}</td>
				<td class="listtd">${rs.biz_nm}</td>
				<td class="listtd">${rs.area}</td>
				<td class="listtd">${rs.biz_bgnde} ~ ${rs.biz_endde}</td>
				<td class="listtd"><fmt:formatNumber value="${rs.decsn_amount}" groupingUsed="true"/><c:if test="${not empty rs.decsn_amount}">원</c:if></td>
				<td class="listtd"><fmt:formatNumber value="${rs.excut_amount}" groupingUsed="true"/><c:if test="${not empty rs.excut_amount}">원</c:if></td>
				<td class="listtd"><fmt:formatNumber value="${rs.decsn_amount - rs.excut_amount}" groupingUsed="true"/>원</td>
				<td class="listtd"><fmt:formatNumber value="${rs.dpst_intr + rs.trmnat_intr}" groupingUsed="true"/>원</td>
				<td class="listtd"><fmt:formatNumber value="${rs.decsn_amount - rs.excut_amount + rs.dpst_intr + rs.trmnat_intr}" groupingUsed="true"/>원</td>
				<td class="listtd">
					<input type="radio" name="last_evl_result_${sts.count}" id="target_${sts.count}" value="RS01" cssClass="cho" <c:if test="${rs.last_evl_result eq 'RS01'}">checked="checked"</c:if> onclick="fnAjaxLastEvlResultModify('${rs.biz_id}','RS01',this);return false;" /> <label for="target_${sts.count}">탁월</label>
					<input type="radio" name="last_evl_result_${sts.count}" id="target_${sts.count}" value="RS02" cssClass="cho" <c:if test="${rs.last_evl_result eq 'RS02'}">checked="checked"</c:if> onclick="fnAjaxLastEvlResultModify('${rs.biz_id}','RS02',this);return false;" /> <label for="target_${sts.count}">우수</label>
					<input type="radio" name="last_evl_result_${sts.count}" id="target_${sts.count}" value="RS03" cssClass="cho" <c:if test="${rs.last_evl_result eq 'RS03'}">checked="checked"</c:if> onclick="fnAjaxLastEvlResultModify('${rs.biz_id}','RS03',this);return false;" /> <label for="target_${sts.count}">보통</label>
					<input type="radio" name="last_evl_result_${sts.count}" id="target_${sts.count}" value="RS04" cssClass="cho" <c:if test="${rs.last_evl_result eq 'RS04'}">checked="checked"</c:if> onclick="fnAjaxLastEvlResultModify('${rs.biz_id}','RS04',this);return false;" /> <label for="target_${sts.count}">미흡</label>
				</td>
				<td class="listtd">					
					<a href="/mng/gfund/biz/productmng/EgovLastPapersList.do?year=${rs.year}&biz_id=${rs.biz_id}&biz_ty_code=${rs.biz_ty_code}"><img src="/template/manage/images/btn/btn_select.gif"/></a>
				</td>
			</tr>
		</c:forEach>
		<c:if test="${empty bsifList}">
			<tr>
				<td class="listtd" colspan="12">
					<spring:message code="common.nodata.msg"/>
				</td>
			</tr>
		</c:if>
	</tbody>
</table>

<div id="paging">
	<c:url var="pageUrl" value="/mng/gfund/biz/productmng/EgovProductManageList.do">
		<c:param name="biz_id" value="${param.biz_id}" />
	</c:url>

	<c:if test="${not empty paginationInfo}">
		<ul>
			<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}"/>
		</ul>
	</c:if>
</div>

<div class="btn_r">
	<c:url var="surveyUrl" value="/mng/gfund/biz/productmng/selectSchdulinfoList.do">		
	</c:url>
	<span class="cbtn"><a href="${surveyUrl}">설문조사관리</a></span>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
