<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>녹색자금통합관리시스템 - 년도관리</title>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/default.css"/>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/page.css"/>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/com.css"/>
	<link type="text/css" href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet"/>
	<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/ui/i18n/jquery.ui.datepicker-ko.js" charset="utf-8"></script>
	<script type="text/javascript" src="/template/common/js/common.js"></script>

	<script type="text/javascript">
		var yearArr = [<c:forEach var="rs" items="${yearList}" varStatus="sts">${rs.year}<c:if test="${sts.count ne fn:length(yearList)}">,</c:if></c:forEach>];
		function fnYearOverlapCheck(el){
			for(var i=0; i<yearArr.length; i++){
				if(yearArr[i] == el.val()){
					el.val('');
					alert("이미 생성된 년도 입니다.\n다른 년도를 입력해주십시오.");
					return;
				}
			}
		}
	</script>
</head>
<body>
<div id="container">
	<div id="contents">
		<div id="cntnts">
			<form name="yearForm" id="yearForm" action="/mng/gfund/biz/year/yearInsert.do" method="post" onsubmit="return fnCommonOnSubmit($('.inp'));">

				<table class="chart2">
					<colgroup>
						<col class="co1"/>
						<col class="co3"/>
					</colgroup>
					<caption class="hdn">년도관리</caption>
					<tbody>
						<tr>
							<th>년도</th>
							<td>
								<input type="text" name="year" id="year" class="inp null_false validation number" onkeyup="fnYearOverlapCheck($(this));" maxlength="4" title="년도" />
							</td>
						</tr>
						<tr>
							<th>기준년도 적용</th>
							<td>
								<label for="use_at_y">적용</label>
								<input type="radio" name="use_at" id="use_at_y" value="Y" title="기준년도" />
								<label for="use_at_n">미적용</label>
								<input type="radio" name="use_at" id="use_at_n" value="N" title="기준년도" checked="checked"/>
							</td>
						</tr>
					</tbody>
				</table>

				<div class="btn_c">
					<span class="cbtn2">
						<button type="submit">등록</button>
					</span>
					<span class="cbtn1">
						<a href="/mng/gfund/biz/year/yearList.do">취소</a>
					</span>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
