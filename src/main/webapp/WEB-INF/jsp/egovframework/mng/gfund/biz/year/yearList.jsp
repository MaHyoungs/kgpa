<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>녹색자금통합관리시스템 - 년도관리</title>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/default.css"/>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/page.css"/>
	<link type="text/css" rel="stylesheet" href="/template/manage/css/com.css"/>
	<link type="text/css" href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet"/>
	<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/ui/i18n/jquery.ui.datepicker-ko.js" charset="utf-8"></script>
	<script type="text/javascript" src="/template/common/js/common.js"></script>
	<script type="text/javascript">
		function fnYearDelete(year){
			var reVal = confirm(year + "년도를 삭제 하시겠습니까?");
			return reVal;
		}
		function fnYearUseUpdate(year){
			var reVal = confirm(year + "년도를 기준년도 설정으로 변경 하시겠습니까?");
			return reVal;
		}

		<c:if test="${insertRs eq '1'}">
			alert("${greenFundYearVo.year}년도가 생성 되었습니다.");
		</c:if>
		<c:if test="${updateRs eq '1'}">
			alert("${greenFundYearVo.year}년도가 적용 되었습니다.");
		</c:if>
		<c:if test="${deleteRs eq '1'}">
			alert("${greenFundYearVo.year}년도가 삭제 되었습니다.");
		</c:if>
	</script>
</head>
<body>
<div id="container">
	<div id="contents">
		<div id="cntnts">
			<table class="chart_board">
				<colgroup>
					<col class="co1"/>
					<col class="co1"/>
					<col class="co1"/>
				</colgroup>
				<caption class="hdn">년도관리</caption>
				<thead>
				<tr>
					<th>년도</th>
					<th>기준년도 여부</th>
					<th>관리</th>
				</tr>
				</thead>
				<tbody>

				<c:forEach var="rs" items="${yearList}" varStatus="status">
					<tr>
						<td>${rs.year}년</td>
						<td>
							<c:if test="${rs.use_at eq 'Y'}">
								<img src="/template/manage/images/btn/use_yes.png" alt="적용"/>
							</c:if>
							<c:if test="${rs.use_at eq 'N'}">
								<img src="/template/manage/images/btn/use_no.png" alt="미적용"/>
							</c:if>
						</td>
						<td>
							<c:if test="${rs.use_at eq 'N'}">
								<span class="btn_pack small icon"><span class="check"></span><a href="/mng/gfund/biz/year/yearUseUpdate.do?year=${rs.year}" onclick="return fnYearUseUpdate('${rs.year}');">적용</a></span>
								<c:if test="${rs.use_cnt eq 0}">
									<span class="btn_pack small icon"><span class="delete"></span><a href="/mng/gfund/biz/year/yearDelete.do?year=${rs.year}" onclick="return fnYearDelete('${rs.year}');">삭제</a></span>
								</c:if>
							</c:if>
						</td>
					</tr>
				</c:forEach>

				<c:if test="${fn:length(yearList) == 0}">
					<tr>
						<td class="listCenter" colspan="3"><spring:message code="common.nodata.msg"/></td>
					</tr>
				</c:if>
				</tbody>
			</table>

			<div class="btn_r">
				<a href="/mng/gfund/biz/year/yearForm.do"><img src="/template/manage/images/btn/btn_creat.gif" alt="생성"></a>
			</div>
		</div>
	</div>
</div>
</body>
</html>
