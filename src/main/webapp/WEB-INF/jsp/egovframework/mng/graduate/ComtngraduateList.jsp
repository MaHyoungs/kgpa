<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="1" value="1" />
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="SCHAFS_MANAGE"/>
	<c:param name="depth1" value="GRADUATE_LIST"/>
	<c:param name="depth2" value="GRADUATE_LIST"/>
	<c:param name="title" value="졸업생관리"/>
</c:import>	
<script type="text/javaScript" language="javascript" defer="defer">
<!--
	/* function fn_addYearSem(){
		var year = $("#year").val();
		var semstr = $("#semstr").val();
		if(year==""){
			alert("년도를 입력하십시오.");
			return false;
		}else if(semstr == ""){
			alert("학기를 입력하십시오.");
			return false;
		}else{

			if(year.length != 4){
				alert("년도는 무조건 4자리 맞춰서 입력해야합니다.");
				return false;
			}else if(semstr.length != 1){
				alert("학기는 무조건 1자리 맞춰서 입력해야합니다.");
				return false;
			}else{
				$.ajax({
				   type: "POST",
				   url: "/mng/yearsem/addComtnyearsemstrAjax.do",
				   data: "year="+year+"&semstr="+semstr,
				   success: function(msg){
					   console.log(msg);
				   	   alert( "성공적으로 처리되었습니다.");
			     	   location.href='/mng/yearsem/ComtnyearsemstrList.do';
				   },
				   error:function(e){
					   alert("error:"+e);
				   }
				});	
			}
		}
	} 
	function fn_register(form){
		$(".addclass").html("<tr><td></td><td><input type='text' name='year' id='year' class='inp' maxlength='4'/></td><td><input type='text' name='semstr' id='semstr' class='inp' maxlength='1' /></td><td><a href='#' onclick='fn_addYearSem();'>추가</a></td><td></td><td></td></tr>");
	}
	
	function fn_deleted(id){
		var frm = document.listForm;
		if(confirm("해당 학기를 삭제하시겟습니까?")){
			frm.action="/mng/yearsem/deleteComtnyearsemstr.do?yearSemstrId="+id;
			frm.submit();
			return true;
		}
	}
	
	function fn_selected(id){
		var frm = document.listForm;
		
		if(confirm("해당 학기를 기본설정으로 설정하시겠습니까?")){
			frm.action="/mng/yearsem/updateComtnyearsemstr.do?yearSemstrId="+id;
			frm.submit();
			return true;
		}
	} */
 // -->
</script>

<div id="cntnts">
	<c:if test="${USER_INFO.userSe > 10}">
		<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>	
	<form:form commandName="searchVO" name="listForm" id="listForm" method="post" action="/mng/graduate/ComtngraduateList.do">
		<table class="chart_board">
		<colgroup>
			<col"/>
			<col"/>
			<col"/>
			<col"/>
			<col"/>
			<col"/>
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>년도</th>
				<th>이름</th>
				<th>아이디</th>
				<th>핸드폰번호</th>
				<th>이메일</th>
				<th>주소</th>
			</tr>
		</thead>
		<tbody class="addclass">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
				<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
				<td align="center" class="listtd"><c:out value="${result.year}"/>&nbsp;</td>
				<td align="center" class="listtd"><c:out value="${result.userNm}"/>&nbsp;</td>
				<td align="center" class="listtd"><c:out value="${result.userId}"/></td>
				<td align="center" class="listtd">
					<c:if test="${result.moblphonNo ne '--'}">
						${result.moblphonNo}
					</c:if>
				</td>
				<td align="center" class="listtd">
					<c:if test="${result.emailAdres ne '@'}">
						<c:out value="${result.emailAdres}"/>
					</c:if>
				</td>
				<td align="center" class="listtd"><c:out value="${result.zip}"/> <c:out value="${result.adres}"/> <c:out value="${result.adresDetail}"/></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="8">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>
		</tbody>
	</table>
	<!-- /List -->
	<div id="paging">
		<c:url var="pageUrl" value="/mng/graduate/ComtngraduateList.do${_BASE_PARAM}">
		</c:url>

		<c:if test="${not empty paginationInfo}">
			<ul>
				<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			</ul>
		</c:if>
	</div>
	<div class="btn_r">
       	<%-- <a href="#" onclick="fn_register(this);"><img src="${_IMG}/btn/btn_regist.gif" /></a> --%>
		<%-- <a href="#" onclick="fncAddView();return false;"><img src="${_IMG}/btn/btn_regist.gif" /></a> --%>
	</div>
	
	<div id="bbs_search">
		<label for="ftext2" class="hdn">분류검색</label>
		<select name="searchYear" id="ftext">
			<option value="">년도선택</option>
			<c:forEach var="result" items="${yearList }" varStatus="status">
				<option value="${result.year }" <c:if test="${searchVO.searchYear eq result.year }">selected="selected"</c:if>>${result.year }년도</option>
			</c:forEach>
		</select>
		<select name="searchCondition" id="ftext2">
			<option value="">검색항목</option>
			<option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if>>아이디</option>
			<option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if>>이름</option>
  	  	</select>
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="inp" id="inp_text" />
		<input type=image src="${_IMG}/board/btn_search.gif" alt="검색" />
	</div>
	</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	
