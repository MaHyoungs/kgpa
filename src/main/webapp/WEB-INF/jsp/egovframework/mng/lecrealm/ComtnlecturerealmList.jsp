<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<%-- 
<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
 --%>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="AFTERSCHOOL_MANAGE"/>
	<c:param name="depth1" value="LECTUREREAM_LIST"/>
	<c:param name="depth2" value="LECTUREREAM_LIST"/>
	<c:param name="title" value="강좌분야관리"/>
</c:import>	
<script type="text/javaScript" language="javascript" defer="defer">
<!--
	function fn_addLecRealm(){
		var lecRealmNm = $("#lecRealmNm").val();
		if(lecRealmNm==""){
			alert("강좌분야명을 입력하십시오.");
			return false;
		}else{
			$.ajax({
			   type: "POST",
			   url: "/mng/lecrealm/addComtnlecturerealmAjax.do",
			   data: "lecRealmNm="+lecRealmNm,
			   success: function(msg){
				   //console.log(msg);
			   	   alert( "성공적으로 처리되었습니다.");
		     	   location.href='/mng/lecrealm/ComtnlecturerealmList.do';
			   },
			   error:function(e){
				   alert("error:"+e);
			   }
			});	
		}
	} 
	
	function fn_register(form){
		$(".btn_r").hide();
		$(".addclass").html("<tr><td></td><td><input type='text' name='lecRealmNm' id='lecRealmNm' class='inp' maxlength='20'/></td><td></td><td><a href='#' onclick='fn_addLecRealm();'><img src='${_IMG}/btn/btn_regist_new.gif' alt='신규 등록하기' /></a></td></tr>");
	}
	
	function fn_deleted(id){
		var frm = document.listForm;
		if(confirm("해당 강좌분야를 삭제하시겟습니까?")){
			frm.action="/mng/lecrealm/deleteComtnlecturerealm.do?lecRealmId="+id;
			frm.submit();
			return true;
		}
	}
	
	function fn_update(id){
		var nm = $("#"+id).val();
		if(confirm("해당 강좌분야를 수정하시겟습니까?")){
			$.ajax({
				type:"POST",
				url:"/mng/lecrealm/updateComtnlecturerealm.do",
				data:"lecRealmId="+id+"&lecRealmNm="+nm,
				succeess:function(msg){
					alert("성공적으로 처리되었습니다.");
		     	    location.href='/mng/lecrealm/ComtnlecturerealmList.do';
				},
			    error:function(e){
			   		alert("error:"+e);
			    }
			});
		}
	}
	
	function fn_update_UseAt(id, useAt){
		var msg = "";
		
		if(useAt == 'N'){
			msg = "해당 강좌분야를 사용하지 않으시겠습니까?";
		}else{
			msg = "해당 강좌분야를 사용하시겠습니까?";
		}
		
		if(confirm(msg)){
			$.ajax({
				type:"POST",
				url:"/mng/lecrealm/updateComtnlecturerealm_UseAt.do",
				data:"lecRealmId="+id+"&useAt="+useAt,
				succeess:function(msg){
					alert("성공적으로 처리되었습니다.");
					location.href='/mng/lecrealm/ComtnlecturerealmList.do';
				},
				error:function(e){
					alert("error:"+e);
				}
			})
		}
	}
 // -->
</script>
<div id="cntnts">
	<c:if test="${USER_INFO.userSe > 10}">
		<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>	
<form:form commandName="searchVO" name="listForm" id="listForm" method="post">
	<table class="chart_board">
		<colgroup>
			<col/>
			<col/>
			<col/>
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>강좌분야</th>
				<th>사용여부</th>
				<th>관리</th>
			</tr>
		</thead>
		<tbody class="addclass"a>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr>
					<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
					<td align="center" class="listtd"><input type="text" value="${result.lecRealmNm}" name="lecRealmNm" id="${result.lecRealmId }" />&nbsp;</td>
					<td>
						<c:choose>
							<c:when test="${result.useAt eq 'Y' }"><a href="#" onclick="fn_update_UseAt('${result.lecRealmId}','N');"><img src="${_IMG }/board/use_yes.png" alt="적용" /></a></c:when>
							<c:otherwise><a href="#" onclick="fn_update_UseAt('${result.lecRealmId}','Y');"><img src="${_IMG }/board/use_no.png" alt="미적용" /></a></c:otherwise>
						</c:choose>
					</td>					
					<td><a href="#" onclick="fn_update('${result.lecRealmId}');"><img src="${_IMG }/btn/edit.gif" alt="수정" /></a> <a href="#" onclick="fn_deleted('${result.lecRealmId}');"><img src="${_IMG }/btn/del.gif" alt="삭제" /></a></td>					
	   			</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="5">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>
		</tbody>
	</table>
	<div class="btn_r">
       	<a href="#" onclick="fn_register(this);"><img src="${_IMG}/btn/btn_regist.gif" /></a>
		<%-- <a href="#" onclick="fncAddView();return false;"><img src="${_IMG}/btn/btn_regist.gif" /></a> --%>
	</div>
</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	