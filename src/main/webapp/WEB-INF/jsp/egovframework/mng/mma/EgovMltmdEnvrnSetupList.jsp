<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_IMG" value="/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="SETUP_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="환경설정"/>
</c:import>

<script type="text/javaScript" language="javascript" defer="defer">
<!--
function fnDelete(url) {
    if(confirm('<spring:message code="common.delete.msg" />')){
    	document.location.href = url;	
    }
}

//-->
</script>

<div id="cntnts">
    
    <c:if test="${USER_INFO.userSe > 10}">
		<form id="listForm" name="listForm" action="<c:url value="/mng/mma/MltmdEnvrnSetupList.do"/>" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>
	
      <!-- contents start -->
      <form name="SiteListForm" action="<c:url value='/mng/mma/MltmdEnvrnSetupList.do'/>" method="post">
      <input name="pageIndex" type="hidden" value="1" />

		<p class="total">총 사이트 ${paginationInfo.totalRecordCount}개 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
              
        <table class="chart_board">
           <colgroup>
				<col class="co1"/>
				<col class="co3"/>
				<col class="co3"/>
				<col class="co6"/>
				<col class="co6"/>
			</colgroup>
          <thead>
            <tr>
              <th>번호</th>
              <th>사이트명</th>
              <th>사이트도메인</th>
              <th>설정여부</th>
              <th>관리</th>
            </tr>
          </thead>
          <tbody>

          <c:forEach items="${resultList}" var="resultInfo" varStatus="status">
            <tr>
              <td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
              <td><c:out value="${resultInfo.siteNm}"/></td>
              <td><a href="http://<c:out value="${resultInfo.siteUrl}"/>" target="_blank">http://<c:out value="${resultInfo.siteUrl}"/></a></td>        
              <td>
              	<c:if test="${resultInfo.setupYn eq 'Y'}">
		       		<img src="${_IMG}/btn/use_yes.png" alt="Y"/>
		       	</c:if>
		       	<c:if test="${resultInfo.setupYn == 'N'}">
		       		<img src="${_IMG}/btn/use_no.png" alt="N"/>
		       	</c:if>
              </td>
              <td>
	        	<c:url var="viewUrl" value="${resultInfo.setupYn eq 'N' ? '/mng/mma/addMltmdEnvrnSetup.do' : '/mng/mma/selectMltmdEnvrnSetup.do'}">
				  <c:param name="siteId" value="${resultInfo.siteId}" />
				  <c:param name="siteNm" value="${resultInfo.siteNm}" />
				  <c:param name="setupYn" value="${resultInfo.setupYn}" />
				  <c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
				  <c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	      		  <c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	      		  <c:if test="${not empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
				</c:url>
				<a href="${viewUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
				<c:if test="${resultInfo.setupYn eq 'Y'}">					
		        	<c:url var="delUrl" value="/mng/mma/deleteMltmdEnvrnSetup.do">
					  <c:param name="siteId" value="${resultInfo.siteId}" />
					  <c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
					  <c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		      		  <c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
		      		  <c:if test="${not empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
					</c:url>		
		        	<a href="${delUrl}" onclick="fnDelete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
	        	</c:if>
		    </td>
            </tr>
          </c:forEach>
      
          <c:if test="${fn:length(resultList) == 0}">
            <tr>
              <td class="listCenter" colspan="5"><spring:message code="common.nodata.msg" /></td>
            </tr>
          </c:if>

          </tbody>    
          </table>
          
          	
  			
  			<div id="paging">
			    <c:url var="pageUrl" value="/mng/mma/MltmdEnvrnSetupList.do?">
			      	<c:if test="${not empty param.searchCate}"><c:param name="searchCate" value="${param.searchCate}" /></c:if>
            		<c:if test="${not empty param.searchCondition}"><c:param name="searchCondition" value="${param.searchCondition}" /></c:if>
            		<c:if test="${not empty param.searchKeyword}"><c:param name="searchKeyword" value="${param.searchKeyword}" /></c:if>
			    </c:url>
			
			    <ul>
			      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			    </ul>
		    </div>
		    
		    <div id="bbs_search">
				<label for="ftext" class="hdn">분류검색</label>
				<select name="searchCondition" id="searchCondition">
	                <option selected value=''>분류검색</option>
	                <option value="siteNm"  <c:if test="${searchVO.searchCondition == 'siteNm'}">selected="selected"</c:if> >사이트명</option>
	                <option value="siteUrl"   <c:if test="${searchVO.searchCondition == 'siteUrl'}">selected="selected"</c:if> >사이트도메인</option>                             
	            </select>
				<label for="inp_text" class="hdn">검색어입력</label>
				<input name="searchKeyword" type="text" size="25" class="inp" value='<c:out value="${searchVO.searchKeyword}"/>' maxlength="35"  class="inp_s" id="inp_text"/>
				<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색"/>
		  </div>
		  
      </form>



    </div>        

    <c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	