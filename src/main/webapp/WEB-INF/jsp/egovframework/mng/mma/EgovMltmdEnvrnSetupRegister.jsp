<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_MODE" value=""/>

<c:choose>
	<c:when test="${param.setupYn eq 'N'}">
		<c:set var="_MODE" value="REG"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
	</c:otherwise>
</c:choose>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="SETUP_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="환경설정"/>
</c:import>

<script type="text/javaScript" language="javascript" defer="defer">
<!--

function fn_egov_regist(frm) {
	
	if($('#ctgrymasterId').val() == "") {
		alert("카테고리를 선택하세요");
		return false;
	}
	
	if($('#wmStreFileNm').val() == "" && $('#wmImageFileIdFile').val() == "") {
		alert("워터마크 이미지를 첨부하세요");
		return false;
	}
	
	if(!$.isNumeric($('#wmTrnsprcNcl').val())) {
		alert("투명도는 숫자만 입력하세요");
		$('#wmTrnsprcNcl').focus();
		return false;
	}
	
	if(!($('#wmTrnsprcNcl').val() >= 0 && $('#wmTrnsprcNcl').val() <= 100)) {
	    alert("투명도는 0~100 사이의 숫자만 입력하세요");
	    frm.wmTrnsprcNcl.focus();
	    return false;
    }
	    
	if(!confirm("<spring:message code="${_MODE eq 'REG' ? 'common.regist.msg' : 'common.update.msg'}" />")){
    	return false;
    }

}

// -->
</script>

<div id="cntnts">

<form:form commandName="mltmdEnvrnSetupVO" name="mltmdEnvrnSetupVO" action="${_MODE eq 'REG' ? '/mng/mma/insertMltmdEnvrnSetup.do' : '/mng/mma/updateMltmdEnvrnSetup.do'}" method="post" enctype="multipart/form-data" onsubmit="return fn_egov_regist(this)">
	
	<form:hidden path="siteId"/>
	<form:hidden path="wmStreFileNm"/>
	<form:hidden path="wmOrignlFileNm"/>
	
	<!-- 검색조건 유지 -->
	<input type="hidden" name="searchCondition" value="<c:out value='${searchVO.searchCondition}'/>"/>
	<input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>"/>
	<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>

	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col class="co1"/>
			<col class="co2"/>
		</colgroup>
			<tbody>
				<tr>
	                <th>
	                  <em>*</em> <spring:message code="common.site" />
	                </th>
	                <td>
	        	        <c:out value="${param.siteNm}"/>
	                </td>         
              	</tr>
              	<tr>
	              <th><em>*</em> <label for="ctgrymasterId">카테고리</label></th>
	              <td>
	                <form:select path="ctgrymasterId">
            	  		<form:option value='' label="--선택--" />
        	      		<form:options items="${ctgrymasterList}" itemValue="ctgrymasterId" itemLabel="ctgrymasterNm"/>
              		</form:select>	    
        	  	    <br/><form:errors path="ctgrymasterId" />
	              </td>
	            </tr>
				<tr>
	              <th><em>*</em> <label for="wmImageFileIdFile">워터마크 이미지</label></th>
	              <td>
	                <input type="file" name="wmImageFileIdFile" id="wmImageFileIdFile" class="input300 inp"  title="이미지"/> (png, jpg)
			     	<c:if test="${not empty mltmdEnvrnSetupVO.wmStreFileNm}">
		            	<br/><img src="${SiteFileStoreWebPath}${mltmdEnvrnSetupVO.siteId}/${mltmdEnvrnSetupVO.wmStreFileNm}" alt="이미지 "/>
		            </c:if>
	              </td>
	            </tr>
	            <tr>
	              <th><em>*</em> <label for="wmTrnsprcNcl">투명도</label></th>
	              <td>
	              	<form:input path="wmTrnsprcNcl" cssClass="inp" size="3"/>% (0~100)
	              </td>
	            </tr>
	            <tr>
	              <th><em>*</em> <label for="wmLcWidth">가로위치</label></th>
	              <td>
	              	<form:select path="wmLcWidth">
            	  		<form:option value='Left' label="왼쪽" />
            	  		<form:option value='Center' label="가운데" />
            	  		<form:option value='Right' label="오른쪽" />
              		</form:select>	
	              </td>
	            </tr>
	            <tr>
	              <th><em>*</em> <label for="wmLcVrticl">세로위치</label></th>
	              <td>
	              	<form:select path="wmLcVrticl">
            	  		<form:option value='Top' label="상단" />
            	  		<form:option value='Center' label="가운데" />
            	  		<form:option value='Bottom' label="하단" />
              		</form:select>	
	              </td>
	            </tr>
	            <tr>
                <th>
                  <em>*</em> 자동승인여부
                </th>
                <td>
                	<spring:message code="button.yes" /> : <form:radiobutton path="atmcConfmAt"  value="Y" />&nbsp;
          	     	<spring:message code="button.no" /> : <form:radiobutton path="atmcConfmAt"  value="N"  />
                </td>         
              </tr>
			</tbody>
	</table>
  
	<div class="btn_r">
		<input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }"/>
        <c:url var="listUrl" value="/mng/mma/MltmdEnvrnSetupList.do">
        	<c:param name="siteId" value="${searchVO.siteId}"/>
	        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
			<c:param name="searchCondition" value="${searchVO.searchCondition}" />
			<c:param name="searchKeyword" value="${searchVO.searchKeyword}" />
	      </c:url>
           <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>

</form:form>

</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	