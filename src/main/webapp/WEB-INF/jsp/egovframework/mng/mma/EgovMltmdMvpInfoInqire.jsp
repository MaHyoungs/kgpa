<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_C_LIB" value="/lib"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}"/>
		<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="CONTENTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="컨텐츠관리"/>
</c:import>

<script type="text/javaScript" language="javascript" defer="defer">

	function fn_egov_delete(url){
		if(confirm('<spring:message code="common.delete.msg" />')){
			document.location.href = url;	
		}		
	}
	
	function fn_egov_confirm(url, at) {
		if(at == 'Y') {
			if(confirm('<spring:message code="common.acknowledgement.msg" />')){
				document.location.href = url;	
			}
		} else if(at == 'N') {
			if(confirm('<spring:message code="common.acknowledgementcancel.msg" />')){
				document.location.href = url;	
			}
		}
	}
 // -->
</script>

<div id="cntnts">

	<table class="chart2">
		<caption>보기폼</caption>
		<colgroup>
			<col class="co1"/>
			<col class="co2"/>
		</colgroup>
			<tbody>
				<tr>
	              <th>카테고리</th>
	              <td><c:out value="${mltmdMvpInfoVO.ctgryNm}"/></td>
	            </tr>
              	<tr>
	              <th>제목</th>
	              <td><c:out value="${mltmdMvpInfoVO.mvpSj}"/></td>
	            </tr>
				<tr>
	              <th>키워드</th>
	              <td><c:out value="${mltmdMvpInfoVO.searchKwrd}"/></td>
	            </tr>
	            <tr>
	              <th>작성자</th>
	              <td><c:out value="${mltmdMvpInfoVO.wrterNm}"/></td>
	            </tr>
	            <tr>
	              <th>조회수</th>
	              <td><c:out value="${mltmdMvpInfoVO.inqireCo}"/></td>
	            </tr>
	            <tr>
	              <th>추천수</th>
	              <td><c:out value="${mltmdMvpInfoVO.recomendCo}"/></td>
	            </tr>
				<tr>
					<th>동영상</th>
					<td>	
						<div style="width:650px;height:450px">			
						<object id="mvpPlayer" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
					  		<param name="source" value="${_C_LIB}/mma/MovieClient.xap"/>
						  	<param name="onError" value="onSilverlightError" />
						  	<param name="background" value="white" />
						  	<param name="windowless" value="true" />
						  	<param name="minRuntimeVersion" value="3.0.40818.0" />
						  	<param name="autoUpgrade" value="true" />
						  	<param name="initParams" value="InfoXmlPath=/mma/mvpMediaInfo.do?siteId=${searchVO.siteId}&mltmdFileId=${mltmdMvpInfoVO.mltmdFileId}&mltmdFileDetailId={0}" />				  	
						  	<a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40818.0" style="text-decoration:none;">
								<img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Microsoft Silverlight 가져오기" style="border-style:none;"/>
						  	</a>
					    </object>
					    </div>
					</td>
				</tr>	
	            <tr>
	            	<th>내용</th>
					<td>
						<c:out value="${mltmdMvpInfoVO.mvpCn}" escapeXml="false" />
					</td>
				</tr>
				<tr>
	            	<th>승인</th>
					<td>
						<c:choose>
							<c:when test="${mltmdMvpInfoVO.confmAt eq 'Y'}">
								<c:url var="confirmUrl" value="/mng/mma/updateMltmdMvpConfirm.do${_BASE_PARAM}">
									<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}"/>
									<c:param name="confmAt" value="N"/>
								</c:url>
								<a href="${confirmUrl}" onclick="fn_egov_confirm(this.href, 'N');return false;" title="승인취소"><img src="${_IMG}/board/btn_appr_can.gif" alt="취소"/></a>
								<c:if test="${not empty mltmdMvpInfoVO.confmerPnttm}">
									<strong class="org">[승인: <fmt:formatDate value="${mltmdMvpInfoVO.confmerPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/>]</strong>
								</c:if>
							</c:when>
							<c:otherwise>
								<c:url var="confirmUrl" value="/mng/mma/updateMltmdMvpConfirm.do${_BASE_PARAM}">
									<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}"/>
									<c:param name="confmAt" value="Y"/>
								</c:url>
								<a href="${confirmUrl}" onclick="fn_egov_confirm(this.href, 'Y');return false;" title="승인하기"><img src="${_IMG}/board/btn_appr.gif" alt="승인"/></a>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
	</table>
  
	<div class="btn_r">
		<c:url var="editUrl" value="/mng/mma/forUpdateMltmdMvpInfo.do${_BASE_PARAM}">
			<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}"/>
		</c:url>
		<a href="${editUrl}"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>
		<c:url var="delUrl" value="/mng/mma/deleteMltmdMvpInfo.do">
       		<c:param name="mltmdMvpId" value="${mltmdMvpInfoVO.mltmdMvpId}"/>
		</c:url>	
		<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/btn_del.gif" alt="삭제"/></a>
		<c:url var="listUrl" value="/mng/mma/MltmdMvpInfoList.do${_BASE_PARAM}">
		</c:url>
		<a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>

</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	