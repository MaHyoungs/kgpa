<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_C_IMG" value="/template/common/images/page/board"/>
<c:set var="_IMG" value="/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}"/>
		<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	  	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="CONTENTS_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="컨텐츠관리"/>
</c:import>

<script type="text/javaScript" language="javascript" defer="defer">

	function fn_egov_delete(url){
		if(confirm('<spring:message code="common.delete.msg" />')){
			document.location.href = url;	
		}		
	}
	
	function fn_egov_confirm(url, at) {
		if(at == 'Y') {
			if(confirm('<spring:message code="common.acknowledgement.msg" />')){
				document.location.href = url;	
			}
		} else if(at == 'N') {
			if(confirm('<spring:message code="common.acknowledgementcancel.msg" />')){
				document.location.href = url;	
			}
		}
	}
 // -->
</script>

<div id="cntnts">

	<c:if test="${USER_INFO.userSe > 10}">
		<form name="SiteListForm" action="/mng/mma/MltmdMvpInfoList.do" method="post">
			<div id="bbs_search">
				<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
			</div>
		</form>
	</c:if>

	<p class="total">총  ${paginationInfo.totalRecordCount}건 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
   
  <table class="chart_board">
    <colgroup>
			<col class="co1"/>
			<col class="co6"/>
			<col class="co3"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
	</colgroup>
    <caption class="hdn">컨텐츠관리</caption>
    <thead>
      <tr>
        <th>번호</th>
        <th>카테고리</th>
        <th>제목명</th>
        <th>작성자</th>
        <th>조회수</th>
        <th>추천수</th>
        <th>변환정보</th>
        <th>승인</th>
        <th>등록일</th>
        <th>관리</th>
      </tr> 
    </thead>
    <tbody>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr>
					<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
					<td class="tit"><c:out value="${result.ctgryNm}"/></td>
					<td class="tit">
						<c:choose>
							<c:when test="${not empty result.thumbFilePath}"><img src="${mediaMovieImageWebUrl}${result.thumbFilePath}" alt="<c:out value="${result.mvpSj}"/>" width="146" height="107" onerror="this.src='${_C_IMG}/no_img.gif'"/></c:when>
							<c:otherwise><img src="${_C_IMG}/no_img.gif" alt="<c:out value="${result.mvpSj}"/>" width="146" height="107"/></c:otherwise>
						</c:choose>
						<c:url var="viewUrl" value="/mng/mma/selectMltmdMvpInfo.do${_BASE_PARAM}">
			        		<c:param name="mltmdMvpId" value="${result.mltmdMvpId}"/>
						</c:url>
						<a href="${viewUrl}"><c:out value="${result.mvpSj}"/></a>
					</td>
					<td><c:out value="${result.wrterNm}"/></td>
					<td><fmt:formatNumber value="${result.inqireCo}" type="number"/></td>
					<td><fmt:formatNumber value="${result.recomendCo}" type="number"/></td>
					<td>
						<c:choose>
							<c:when test="${result.cnvrClCode eq 'READY'}">준비중</c:when>
							<c:when test="${result.cnvrClCode eq 'RUNNING'}">진행중</c:when>
							<c:when test="${result.cnvrClCode eq 'COMPLETE'}">완료</c:when>
							<c:when test="${result.cnvrClCode eq 'ERROR'}">에러</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${result.confmAt eq 'Y'}">
								<c:url var="confirmUrl" value="/mng/mma/updateMltmdMvpConfirm.do${_BASE_PARAM}">
									<c:param name="mltmdMvpId" value="${result.mltmdMvpId}"/>
									<c:param name="confmAt" value="N"/>
								</c:url>
								<a href="${confirmUrl}" onclick="fn_egov_confirm(this.href, 'N');return false;" title="승인취소"><img src="${_IMG}/board/btn_appr_can.gif" alt="취소"/></a>
							</c:when>
							<c:otherwise>
								<c:url var="confirmUrl" value="/mng/mma/updateMltmdMvpConfirm.do${_BASE_PARAM}">
									<c:param name="mltmdMvpId" value="${result.mltmdMvpId}"/>
									<c:param name="confmAt" value="Y"/>
								</c:url>
								<a href="${confirmUrl}" onclick="fn_egov_confirm(this.href, 'Y');return false;" title="승인하기"><img src="${_IMG}/board/btn_appr.gif" alt="승인"/></a>
							</c:otherwise>
						</c:choose>
					</td>
					<td><fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/></td>
					<td>
			        	<c:url var="editUrl" value="/mng/mma/forUpdateMltmdMvpInfo.do${_BASE_PARAM}">
			        		<c:param name="mltmdMvpId" value="${result.mltmdMvpId}"/>
						</c:url>							
			        	<a href="${editUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
			        	<c:url var="delUrl" value="/mng/mma/deleteMltmdMvpInfo.do">
			        		<c:param name="mltmdMvpId" value="${result.mltmdMvpId}"/>
						</c:url>	
			        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
				    </td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
		      <tr>
		        <td class="listCenter" colspan="10"><spring:message code="common.nodata.msg" /></td>
		      </tr>
		    </c:if>

    </tbody>    
    </table>
 
 	<c:if test="${not empty searchVO.siteId and mltmdSetup ne null}">
		<div class="btn_r">
			<c:url var="addUrl" value="/mng/mma/addMltmdMvpInfo.do${_BASE_PARAM}">
			</c:url>	
			<a href="${addUrl}"><img src="${_IMG}/btn/btn_creat.gif" alt="생성"/></a>
		</div>
	</c:if>
  
  <div id="paging">
    <c:url var="pageUrl" value="/mng/mma/MltmdMvpInfoList.do${_BASE_PARAM}">
    </c:url>

    <ul>
      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
    </ul>
  </div>
  
  <div id="bbs_search">
  	<form id="searchForm" name="searchForm" action="<c:url value="/mng/mma/MltmdMvpInfoList.do"/>" method="post">
		<input type="hidden" name="searchCate" value="${searchVO.searchCate}"/>
		<input type="hidden" name="siteId" value="${searchVO.siteId}"/>
		<label for="ftext" class="hdn">분류검색</label>
		<select name="searchCondition" id="ftext">
  		  <option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if> >제목</option>
  		  <option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if> >내용</option>
  		  <option value="2" <c:if test="${searchVO.searchCondition == '2'}">selected="selected"</c:if> >키워드</option>
  	  	</select>
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="inp_s" id="inp_text" />
		<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
	</form>
  </div>
  
  
   
        




</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	