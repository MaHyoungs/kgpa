<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="/template/manage/images"/>


<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="SERVER_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="서버관리"/>
</c:import>

<script type="text/javaScript" language="javascript" defer="defer">

	function fn_egov_delete(url){
		if(confirm('<spring:message code="common.delete.msg" />')){
			document.location.href = url;	
		}		
	}
 // -->
</script>

<div id="cntnts">


	<p class="total">총  ${paginationInfo.totalRecordCount}건 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
   
  <table class="chart_board">
    <colgroup>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co3"/>
			<col class="co6"/>
	</colgroup>
    <caption class="hdn">서버관리</caption>
    <thead>
      <tr>
        <th>번호</th>
        <th>서버ID</th>
        <th>서버명</th>
        <th>관리</th>
      </tr> 
    </thead>
    <tbody>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr>
					<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
					<td><c:out value="${result.serverId}"/></td>
					<td class="tit"><c:out value="${result.serverNm}"/></td>
					<td>
			        	<c:url var="viewUrl" value="/mng/mma/selectMltmdServer.do">
			        		<c:param name="serverId" value="${result.serverId}"/>
							<c:if test="${not empty param.searchCondition}"><c:param name="searchCondition" value="${param.searchCondition}" /></c:if>
	            			<c:if test="${not empty param.searchKeyword}"><c:param name="searchKeyword" value="${param.searchKeyword}" /></c:if>
	            			<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>							
			        	<a href="${viewUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
			        	<c:url var="delUrl" value="/mng/mma/deleteMltmdServer.do">
			        		<c:param name="serverId" value="${result.serverId}"/>
							<c:if test="${not empty param.searchCondition}"><c:param name="searchCondition" value="${param.searchCondition}" /></c:if>
	            			<c:if test="${not empty param.searchKeyword}"><c:param name="searchKeyword" value="${param.searchKeyword}" /></c:if>
	            			<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>	
			        	<a href="${delUrl}" onclick="fn_egov_delete(this.href);return false;"><img src="${_IMG}/btn/del.gif"/></a>
				    </td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
		      <tr>
		        <td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
		      </tr>
		    </c:if>

    </tbody>    
    </table>
 
	 <div class="btn_r">
	 	<c:url var="addUrl" value="/mng/mma/addMltmdServer.do">
		</c:url>	
	    <a href="${addUrl}"><img src="${_IMG}/btn/btn_creat.gif" alt="생성"/></a>
	  </div>
  
  <div id="paging">
    <c:url var="pageUrl" value="/mng/mma/MltmdServerList.do">
		<c:if test="${not empty param.searchCondition}"><c:param name="searchCondition" value="${param.searchCondition}" /></c:if>
		<c:if test="${not empty param.searchKeyword}"><c:param name="searchKeyword" value="${param.searchKeyword}" /></c:if>
    </c:url>

    <ul>
      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
    </ul>
  </div>
  
  <div id="bbs_search">
  	<form id="searchForm" name="searchForm" action="<c:url value="/mng/mma/MltmdServerList.do"/>" method="post">
		<label for="ftext" class="hdn">분류검색</label>
		<select name="searchCondition" id="ftext">
  		  <!-- option selected value=''>--선택하세요--</option-->
  		  <option value="0" <c:if test="${searchVO.searchCondition == '0'}">selected="selected"</c:if> >서버명</option>
  		  <option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected="selected"</c:if> >서버설명</option>
  	  	</select>
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="<c:out value="${searchVO.searchKeyword}"/>" class="inp_s" id="inp_text" />
		<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
	</form>
  </div>
  
  
   
        




</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	