<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_MODE" value=""/>

<c:choose>
	<c:when test="${empty searchVO.serverId}">
		<c:set var="_MODE" value="REG"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
	</c:otherwise>
</c:choose>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MLTMD_MANAGE"/>
	<c:param name="depth1" value="SERVER_MANAGE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="서버관리"/>
</c:import>

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="mltmdServerVO" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javaScript" language="javascript" defer="defer">
<!--

function fn_egov_regist(frm) {
	
	if (!validateMltmdServerVO(frm)){
		return false;
	}
	
	if(!confirm("<spring:message code="${_MODE eq 'REG' ? 'common.regist.msg' : 'common.update.msg'}" />")){
    	return false;
    }

}

// -->
</script>

<div id="cntnts">

<form:form commandName="mltmdServerVO" name="mltmdServerVO" action="${_MODE eq 'REG' ? '/mng/mma/insertMltmdServer.do' : '/mng/mma/updateMltmdServer.do'}" method="post" onsubmit="return fn_egov_regist(this)">
	
	<form:hidden path="serverId"/>
	
	<!-- 검색조건 유지 -->
	<input type="hidden" name="searchCondition" value="<c:out value='${searchVO.searchCondition}'/>"/>
	<input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>"/>
	<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>

	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col class="co1"/>
			<col class="co2"/>
		</colgroup>
			<tbody>
				<c:if test="${_MODE eq 'UPT'}">
				<tr>
	              <th><em>*</em> 서버아이디</th>
	              <td>
	                <c:out value="${mltmdServerVO.serverId}"/>
	              </td>
	            </tr>
	            </c:if>
              	<tr>
	              <th><em>*</em> <label for="serverNm">서버명</label></th>
	              <td>
	                <form:input path="serverNm" size="60" cssClass="inp_long" maxlength="50"/>
                  	<br/><form:errors path="serverNm" />
	              </td>
	            </tr>
				<tr>
	              <th><em>*</em> <label for="serverDc">서버설명</label></th>
	              <td>
	                <form:input path="serverDc" size="60" cssClass="inp_long" maxlength="100"/>
                  <br/><form:errors path="serverDc" />
	              </td>
	            </tr>
			</tbody>
	</table>
  
	<div class="btn_r">
		<input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }"/>
        <c:url var="listUrl" value="/mng/mma/MltmdServerList.do">
	        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
			<c:param name="searchCondition" value="${searchVO.searchCondition}" />
			<c:param name="searchKeyword" value="${searchVO.searchKeyword}" />
	      </c:url>
           <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>

</form:form>

</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	