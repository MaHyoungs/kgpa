<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
  <c:param name="menu" value="STAT_MANAGE"/>
  <c:param name="depth1" value="ADMINLOGIN_LOG"/>
  <c:param name="depth2" value=""/>
  <c:param name="title" value="관리자접속로그"/>
</c:import>

<script type="text/javascript">
  fnDatepickerOptionAdd();
  $(function () {
    $("#fromDate").datepicker({
      dateFormat: "yymmdd",
      changeYear: true,
      changeMonth: true
    });
    $("#toDate").datepicker({
      dateFormat: "yymmdd",
      changeYear: true,
      changeMonth: true
    });
  });

/*  function fnSearchFormSubmit(){
    var fromDate = document.searchForm.fromDate.value;
    var toDate = document.searchForm.toDate.value;
    $('#pageIndex').val(1);
    if(fromDate != ""){
      if(toDate == ""){
        alert("기간 종료일자를 입력하세요");
        return false;
      }else{
        return true;
      }
    }
    if(toDate != ""){
      if(fromDate == ""){
        alert("기간 시작일자를 입력하세요");
        return false;
      }else{
        return true;
      }
    }
  }*/
</script>

<div id="cntnts">
  <form name="searchForm" method="post" action="/mng/sym/log/adminLoginLogList.do" onsubmit="return fnCommonOnSubmit($('.inp'));">
    <div id="bbs_search">
      <input type="hidden" name="pageIndex" id="pageIndex" value="${adminLogVo.pageIndex}" />
      <input type="text" id="fromDate" name="fromDate" class="inp null_false dates" value="${adminLogVo.fromDate}" size="10" readonly="readonly" tabindex="1" title="접속기간"/>
      <a href="#" onclick="$('#fromDate').focus();">
        <img src="${_C_IMG}/egovframework/cmm/sym/cal/bu_icon_carlendar.gif" align="bottom" alt="게시기간 시작달력" title="게시기간 시작달력"/>
      </a>
      ~
      <input type="text" id="toDate" name="toDate" class="inp null_false dates" value="${adminLogVo.toDate}" size="10" readonly="readonly" tabindex="2" title="접속기간"/>
      <a href="#" onclick="$('#toDate').focus();">
        <img src="${_C_IMG}/egovframework/cmm/sym/cal/bu_icon_carlendar.gif" align="bottom" alt="게시기간 시작달력" title="게시기간 시작달력"/>
      </a>
      <select name="searchCondition" id="searchCondition">
        <option value="">분류선택</option>
        <option value="1" <c:if test="${adminLogVo.searchCondition eq 1}">selected="selected"</c:if>>아이디</option>
        <option value="2" <c:if test="${adminLogVo.searchCondition eq 2}">selected="selected"</c:if>>이름</option>
        <option value="3" <c:if test="${adminLogVo.searchCondition eq 3}">selected="selected"</c:if>>구분</option>
      </select>

      <input type="text" id="searchKeyword" name="searchKeyword" class="inp" value="${adminLogVo.searchKeyword}" />
      <button type="submit">검색</button>
    </div>

    <p class="total">총 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>

    <table class="chart_board">
      <colgroup>
        <col width="8%"/>
        <col width="auto"/>
        <col width="auto"/>
        <col width="15%"/>
        <col width="10%"/>
        <col width="10%"/>
      </colgroup>
      <thead>
      <tr>
        <th class="first">번호</th>
        <th>아이디</th>
        <th>이름</th>
        <th>구분</th>
        <th>아이피</th>
        <th>접속일자</th>
      </tr>
      </thead>
      <tbody>
        <c:forEach items="${alist}" var="rs" varStatus="status">
          <tr>
            <td class="listtd">${rs.rn}</td>
            <td class="listtd">${rs.login_id}</td>
            <td class="listtd">${rs.user_nm}</td>
            <td class="listtd">${rs.author_nm}</td>
            <td class="listtd">${rs.login_ip}</td>
            <td class="listtd">${rs.creat_dt}</td>
          </tr>
        </c:forEach>
        <c:if test="${fn:length(alist) == 0}">
          <tr>
            <td colspan="6" align="center" style="padding-top:5px;padding-bottom:5px;"><spring:message code="common.nodata.msg"/></td>
          </tr>
        </c:if>
      </tbody>
    </table>
  </form>

  <div class="btn_all">
	 <div class="fR"><a href="/mng/sym/log/mngLoginExcelDownload.do" class="cho"><img src="${pageContext.request.contextPath}/template/manage/images/excel.gif" alt="엑셀로 다운받기" /></a></div>
  </div>


  <div id="paging">
    <c:if test="${not empty paginationInfo}">
      <ul>
        <ui:pagination paginationInfo="${paginationInfo}" type="egovRenderer" jsFunction="fnPageMove"/>
      </ul>
    </c:if>
  </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	