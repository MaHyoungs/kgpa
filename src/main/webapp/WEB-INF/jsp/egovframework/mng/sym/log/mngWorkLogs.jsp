<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.com.cmm.service.EgovProperties" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_JS" value="${pageContext.request.contextPath}/template/common/js"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="STAT_MANAGE"/>
	<c:param name="depth1" value="MNGWORK_LOG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="관리자작업로그"/>
</c:import>

<script type="text/javascript">
	fnDatepickerOptionAdd();
	$(function () {
		$("#fromDate").datepicker({
			dateFormat: "yymmdd",
			changeYear: true,
			changeMonth: true
		});
		$("#toDate").datepicker({
			dateFormat: "yymmdd",
			changeYear: true,
			changeMonth: true
		});
	});


/*	function fnSearchFormSubmit() {
		var fromDate = document.searchForm.fromDate.value;
		var toDate = document.searchForm.toDate.value;
		$('#pageIndex').val(1);
		if (fromDate != "") {
			if (toDate == "") {
				alert("기간 종료일자를 입력하세요");
				return false;
			} else {
				return true;
			}
		}
		if (toDate != "") {
			if (fromDate == "") {
				alert("기간 시작일자를 입력하세요");
				return false;
			} else {
				return true;
			}
		}
	}*/

	$(function () {
		$('#logDeleteBtn').click(function () {
			if($('.mngrwork_ids:checked').size() <= 0){
				alert("선택한 항목이 없습니다.");
			}else{
				var msg = confirm("선택한 로그 정보를 삭제 하시겠습니까?");
				if(msg){
					$('#deleteForm').submit();
				}
			}
		});
	});
</script>

<div id="cntnts">
	<form name="searchForm" method="post" action="/mng/sym/log/mngWorkLogs.do" onsubmit="return fnCommonOnSubmit($('.inp'));">
		<div id="bbs_search">
			<input type="hidden" name="pageIndex" id="pageIndex" value="${mngWorkLogVo.pageIndex}"/>

			<c:if test="${not empty glist1}">
				<select name="menu_1st_nm" id="menu_1st_nm">
					<option value="">1차메뉴 선택</option>
					<c:forEach var="rs" items="${glist1}">
						<option value="${rs.menu_1st_nm}"
								<c:if test="${mngWorkLogVo.menu_1st_nm eq rs.menu_1st_nm}">selected="selected"</c:if>>${rs.menu_1st_nm}</option>
					</c:forEach>
				</select>
			</c:if>

			<c:if test="${not empty glist2}">
				<select name="menu_2nd_nm" id="menu_2nd_nm">
					<option value="">2차메뉴 선택</option>
					<c:forEach var="rs" items="${glist2}">
						<option value="${rs.menu_2nd_nm}"
								<c:if test="${mngWorkLogVo.menu_2nd_nm eq rs.menu_2nd_nm}">selected="selected"</c:if>>${rs.menu_2nd_nm}</option>
					</c:forEach>
				</select>
			</c:if>

			<c:if test="${not empty glist3}">
				<select name="menu_3rd_nm" id="menu_3rd_nm">
					<option value="">3차메뉴 선택</option>
					<c:forEach var="rs" items="${glist3}">
						<option value="${rs.menu_3rd_nm}"
								<c:if test="${mngWorkLogVo.menu_3rd_nm eq rs.menu_3rd_nm}">selected="selected"</c:if>>${rs.menu_3rd_nm}</option>
					</c:forEach>
				</select>
			</c:if>

			<c:if test="${not empty glist4}">
				<select name="menu_4th_nm" id="menu_4th_nm">
					<option value="">4차메뉴 선택</option>
					<c:forEach var="rs" items="${glist4}">
						<option value="${rs.menu_4th_nm}"
								<c:if test="${mngWorkLogVo.menu_4th_nm eq rs.menu_4th_nm}">selected="selected"</c:if>>${rs.menu_4th_nm}</option>
					</c:forEach>
				</select>
			</c:if>

			<c:if test="${not empty glist5}">
				<select name="menu_5th_nm" id="menu_5th_nm">
					<option value="">5차메뉴 선택</option>
					<c:forEach var="rs" items="${glist5}">
						<option value="${rs.menu_5th_nm}"
								<c:if test="${mngWorkLogVo.menu_5th_nm eq rs.menu_5th_nm}">selected="selected"</c:if>>${rs.menu_5th_nm}</option>
					</c:forEach>
				</select>
			</c:if>


			<input type="text" id="fromDate" name="fromDate" class="inp null_false dates" value="${mngWorkLogVo.fromDate}" size="10"
					readonly="readonly" tabindex="1" title="작업기간" />
			<a href="#" onclick="$('#fromDate').focus();">
				<img src="${_C_IMG}/egovframework/cmm/sym/cal/bu_icon_carlendar.gif" align="bottom" alt="게시기간 시작달력"
						title="게시기간 시작달력"/>
			</a>
			<input type="text" id="toDate" name="toDate" class="inp null_false dates" value="${mngWorkLogVo.toDate}" size="10"
					readonly="readonly" tabindex="2" title="작업기간" />
			<a href="#" onclick="$('#toDate').focus();">
				<img src="${_C_IMG}/egovframework/cmm/sym/cal/bu_icon_carlendar.gif" align="bottom" alt="게시기간 시작달력"
						title="게시기간 시작달력"/>
			</a>
			<select name="searchCondition" id="searchCondition">
				<option value="">분류선택</option>
				<option value="1" <c:if test="${mngWorkLogVo.searchCondition eq 1}">selected="selected"</c:if>>아이디
				</option>
				<option value="2" <c:if test="${mngWorkLogVo.searchCondition eq 2}">selected="selected"</c:if>>이름
				</option>
			</select>

			<input type="text" id="searchKeyword" name="searchKeyword" class="inp" value="${mngWorkLogVo.searchKeyword}"/>
			<button type="submit">검색</button>
		</div>
	</form>

	<p class="total">총 ${paginationInfo.totalRecordCount}개ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
	<form name="deleteForm" id="deleteForm" action="/mng/sym/log/inDeleteMngWorkLog.do" method="post">
		<table class="chart_board">
			<colgroup>
				<col width="5%"/>
				<col width="auto"/>
				<col width="8%"/>
				<col width="8%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="10%"/>
			</colgroup>
			<thead>
			<tr>
				<th class="first">
					<input type="checkbox" id="mngrwork_ids" onchange="$(this).prop('checked') ? $('.mngrwork_ids').prop('checked', true) : $('.mngrwork_ids').prop('checked', false);"/>
					<label for="mngrwork_ids">번호</label>
				</th>
				<th>작업영역</th>
				<th>작업내역</th>
				<th>바로가기</th>
				<th>ID</th>
				<th>IP</th>
				<th>작업일자</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${list}" var="rs" varStatus="vs">
				<tr>
					<td class="listtd">
						<input type="checkbox" name="mngrwork_ids" id="mngrwork_ids${vs.index}" class="mngrwork_ids" value="${rs.mngrwork_id}"/>
						<label for="mngrwork_ids${vs.index}">${paginationInfo.totalRecordCount - ((mngWorkLogVo.pageIndex-1) * mngWorkLogVo.pageUnit) - (vs.count - 1)}</label>
					</td>
					<td class="tit">
						<c:if test="${not empty rs.menu_1st_nm}">
							${rs.menu_1st_nm}
						</c:if>
						<c:if test="${not empty rs.menu_2nd_nm}">
							&gt; ${rs.menu_2nd_nm}
						</c:if>
						<c:if test="${not empty rs.menu_3rd_nm}">
							&gt; ${rs.menu_3rd_nm}
						</c:if>
						<c:if test="${not empty rs.menu_4th_nm}">
							&gt; ${rs.menu_4th_nm}
						</c:if>
						<c:if test="${not empty rs.menu_5th_nm}">
							&gt; ${rs.menu_5th_nm}
						</c:if>
						<c:if test="${not empty rs.id_menu_nm}">
							&gt; ${rs.id_menu_nm}
						</c:if>
					</td>
					<td class="listtd">
							${rs.work_ty_nm}
					</td>
					<td class="listtd">
						<c:if test="${not empty rs.url}">
							<a href="${rs.url}" target="_blank" title="새창열림">바로가기</a>
						</c:if>
						<c:if test="${empty rs.url}">&nbsp;</c:if>
					</td>
					<td class="listtd">
							${rs.opertor_id}
					</td>
					<td class="listtd">
							${rs.opertor_ip}
					</td>
					<td class="listtd">
							${rs.occrrnc_pnttm}
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(list) == 0}">
				<tr>
					<td colspan="7" align="center" style="padding-top:5px;padding-bottom:5px;">
						<spring:message code="common.nodata.msg"/></td>
				</tr>
			</c:if>
			</tbody>
		</table>
	</form>

	<div class="btn_all">
		<div class="fL">
			<input type="image" id="logDeleteBtn" src="${_IMG}/btn_close.gif" alt="삭제"/>
		</div>

		<div class="fR"><a href="/mng/sym/log/mngWorkLogExcelDownload.do" class="cho"><img src="${pageContext.request.contextPath}/template/manage/images/excel.gif" alt="엑셀로 다운받기" /></a></div>
	</div>
	<div id="paging">
		<c:if test="${not empty paginationInfo}">
			<ul>
				<ui:pagination paginationInfo="${paginationInfo}" type="egovRenderer" jsFunction="fnPageMove"/>
			</ul>
		</c:if>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	