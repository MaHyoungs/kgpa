<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@page import="egovframework.com.cmm.service.Globals"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="mngimg" value="/template/manage/images"/>
<c:set var="MENU_AUTO_MAKE_SITE_ID" value="<%=Globals.MENU_AUTO_MAKE_SITE_ID%>"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>관리자페이지 : <c:out value='${param.title}'/></title>
	<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/default.css'/>"/>
	<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/page.css'/>"/>
	<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/com.css'/>"/>
	<link type="text/css" href="<c:url value='/template/common/js/jquery/themes/base/jquery.ui.all.css'/>" rel="stylesheet" />
	<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery-1.9.1.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery-ui.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/template/common/js/jquery/ui/i18n/jquery.ui.datepicker-ko.js'/>" charset="utf-8"></script>
	<script type="text/javascript" src="/template/common/js/xml2json.js"></script>
	<script type="text/javascript" src="<c:url value='/template/common/js/common.js'/>"></script>

	<c:if test="${not empty param.validator}">
		<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
		<validator:javascript formName="${param.validator}" staticJavascript="false" xhtml="true" cdata="false"/>
	</c:if>
	<script type="text/javaScript" language="javascript" defer="defer"></script>
	<script type="text/javaScript" src="/template/manage/js/mngworklogs.js"></script>
</head>
<body>
<div id="wrap">
	<div id="header">
		<div class="topLc">
			<h1 class="logo"><a href="<c:url value="/mng/index.do"/>"><img src="<c:url value="${mngimg}/logo_admin.gif"/>" alt="관리자 페이지"/></a></h1>
			<div id="topMenu">
				<ul class="list">
					<li><span class="nonBtn"><c:out value="${USER_INFO.name}"/>님</span></li>
					<li><a href="/mng/logout.do" class="nonBtn"><img src="<c:url value="${mngimg}/btn_logout.gif"/>" alt="로그아웃"/></a></li>
					 <%--<li><a class="nonBtn" target="_blank" href="http://intra.itshome.co.kr/EMT/?ref=tools/workpaper/board.emt&amp;idx=000001149">요청게시판 <img src="${mngimg}/icon_arrow.gif"/></a></li>--%>
				</ul>
			</div>
		</div>
		<!-- 대메뉴 목록 -->
		<div id="mainMenu">
			<ul class="list">
				<%--
					01  모든사용자
					02  일반사용자
					06  한반도산림사용자
					08  녹색자금사용자

					04  정보공개담당자
					05  정보공개관리자
					07  한반도산림관리자
					09  녹색자금관리자
					10  홈페이지관리자
					11  소외계층관리자
					99  통합관리자
				--%>
				<%--
					시스템관리 SYSTEM_MANAGE
					메뉴관리 MENUCNTNTS_MANAGE
					관리자관리 MBER_MANAGE
					게시판관리 BOARD_MANAGE
					통합캘린더관리 EVENT_MANAGE
					통계관리 STAT_MANAGE
					기타관리 ETC_MANAGE
					정보공개관리 OPEN_MANAGE
					녹색자금통합관리시스템관리 GFUND_MANAGE
					한반도산림복원자료실관리 SANRIMDB_MANAGE
				--%>
				<c:if test="${USER_INFO.userSe >= 10}">
					<c:choose>
						<c:when test="${USER_INFO.userSe eq 11}">
							<li><a href="/mng/UnderPrivilegedBoardList.jsp" class="slt" target="_self">소외계층 체험교육</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="<c:url value="/mng/sym/sit/selectSiteInfoList.do"/>" <c:if test="${param.menu eq 'SYSTEM_MANAGE'}">class="slt"</c:if> target="_self">시스템관리</a></li>
							<li><a href="<c:url value="/mng/sym/mpm/selectMpmList.do"/>" <c:if test="${param.menu eq 'MENUCNTNTS_MANAGE'}">class="slt"</c:if> target="_self">메뉴관리</a></li>
							<li><a href="<c:url value="/mng/usr/EgovMberManage.do"/>" <c:if test="${param.menu eq 'MBER_MANAGE'}">class="slt"</c:if> target="_self">관리자관리</a></li>
							<li><a href="<c:url value="/mng/cop/bbs/SelectBBSMasterInfs.do"/>" <c:if test="${param.menu eq 'BOARD_MANAGE'}">class="slt"</c:if> target="_self">게시판관리</a></li>
							<li><a href="<c:url value="/mng/evt/selectSchdulinfoList.do"/>" <c:if test="${param.menu eq 'EVENT_MANAGE'}">class="slt"</c:if> target="_self">통합캘린더관리</a></li>
							<%-- <li><a href="<c:url value="#"/>" <c:if test="${param.menu eq 'SMS_MANAGE'}">class="slt"</c:if> target="_self">SMS관리</a></li> --%>
							<li><a href="<c:url value="/mng/sym/log/adminLoginLogList.do"/>" <c:if test="${param.menu eq 'STAT_MANAGE'}">class="slt"</c:if> target="_self">통계관리</a></li>
							<li><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN001"/>" <c:if test="${param.menu eq 'ETC_MANAGE'}">class="slt"</c:if> target="_self">기타관리</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${USER_INFO.userSe > 11 or USER_INFO.userSe eq 4 or USER_INFO.userSe eq 5}">
					<li><a href="<c:url value="/mng/cop/bbs/selectOpenBoardList.do"/>" <c:if test="${param.menu eq 'OPEN_MANAGE'}">class="slt"</c:if> href="#" target="_self">정보공개관리</a></li>
				</c:if>
				<c:if test="${USER_INFO.userSe > 11 or USER_INFO.userSe eq 9}">
					<li><a href="<c:url value="/mng/cop/bbs/gfundNoticeList.do" />" <c:if test="${param.menu eq 'GFUND_MANAGE'}">class="slt"</c:if> target="_self">녹색자금통합관리</a></li>
				</c:if>
				<c:if test="${USER_INFO.userSe > 11 or USER_INFO.userSe eq 7}">
					<li><a href="<c:url value="/mng/forest/usr/EgovMberManage.do"/>" <c:if test="${param.menu eq 'SANRIMDB_MANAGE'}">class="slt"</c:if> target="_self">한반도산림복원관리</a></li>
				</c:if>
				<c:if test="${USER_INFO.userSe >= 10 or USER_INFO.userSe eq 4 or USER_INFO.userSe eq 5 or USER_INFO.userSe eq 9 or USER_INFO.userSe eq 7}">
					<li><a href="http://intra.itshome.co.kr/EMT/?ref=tools/workpaper/board.emt&amp;idx=020110630" target="_blank">요청게시판</a></li>
				</c:if>
			</ul>
		</div>
	</div>

	<div id="container">
		<!-- 좌측메뉴 목록 -->
		<div id="leftMenu">
			<c:choose>
				<c:when test="${param.menu eq 'SYSTEM_MANAGE'}"> <!-- 시스템관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/sym/sit/selectSiteInfoList.do"/>" <c:if test="${param.depth1 eq 'SITE_MANAGE'}">class="slt"</c:if>>사이트관리</a></li>
						<c:if test="${USER_INFO.userSe > 10}">
							<li class="smnu"><a href="<c:url value="/mng/cop/com/selectLytTemplateList.do"/>" <c:if test="${param.depth1 eq 'TMPLAT_MANAGE'}">class="slt"</c:if>>템플릿관리</a></li>
							<li class="smnu"><a href="<c:url value="/mng/cop/com/selectLytSourcList.do"/>" <c:if test="${param.depth1 eq 'SOURC_MANAGE'}">class="slt"</c:if>>레이아웃 관리</a></li>
							<li class="smnu"><a href="<c:url value="/mng/sym/ccm/cca/EgovCcmCmmnCodeList.do"/>" <c:if test="${param.depth1 eq 'CODE_MANAGE'}">class="slt"</c:if>>공통코드관리</a></li>
						</c:if>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'MENUCNTNTS_MANAGE'}"> <!-- 메뉴관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/sym/mpm/selectMpmList.do"/>" <c:if test="${param.depth1 eq 'MENU_MANAGE'}">class="slt"</c:if> target="_self">메뉴관리</a></li>
						<%--
						<c:if test="${USER_INFO.userSe > 10}">
							<li class="smnu"><a href="<c:url value="/mng/sym/mpm/selectMpmList.do?siteId=${MENU_AUTO_MAKE_SITE_ID}"/>" <c:if test="${param.depth1 eq 'MENU_AUTHO_MANAGE'}">class="slt"</c:if> target="_self">기본 자동생성 메뉴관리</a></li>
						</c:if>
						--%>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'MBER_MANAGE'}"> <!-- 관리자관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/usr/EgovMberManage.do"/>" <c:if test="${param.depth2 eq 'MBER_MANAGE'}">class="slt"</c:if> target="_self">관리자목록</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'BOARD_MANAGE'}"> <!-- 게시판관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/cop/bbs/SelectBBSMasterInfs.do"/>" <c:if test="${param.depth1 eq 'BOARD_ADMIN'}">class="slt"</c:if>>게시판관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/cop/bbs/ctg/selectBBSCtgryMasterList.do"/>" <c:if test="${param.depth1 eq 'CTGRY_ADMIN'}">class="slt"</c:if>>카테고리관리</a></li>
						<c:if test="${USER_INFO.userSe > 10}">
							<li class="smnu"><a href="<c:url value="/mng/cop/com/selectBbsTemplateList.do"/>" <c:if test="${param.depth1 eq 'TMPLAT_MANAGE'}">class="slt"</c:if>>게시판 템플릿 관리</a></li>
							<li class="smnu"><a href="<c:url value="/mng/cop/com/selectBbsSourcList.do"/>" <c:if test="${param.depth1 eq 'SOURC_MANAGE'}">class="slt"</c:if>>게시판 소스 관리</a></li>
						</c:if>
						<li class="smnu"><a href="<c:url value="/mng/gds/selectMngComtngnList.do"/>" <c:if test="${param.depth1 eq 'COMTNGN'}">class="slt"</c:if>>녹색자료실 06~09</a></li>
					</ul>
				</c:when>

				<%--
				<c:when test="${param.menu eq 'COURSE_MANAGE'}"> <!-- 체험프로그램 -->
					<ul class="list">
						<li class="smnu"><a href="#" <c:if test="${param.depth2 eq 'PROG_MANAGE'}">class="slt"</c:if> target="_self">체험프로그램관리</a></li>
					</ul>
				</c:when>
				--%>

				<c:when test="${param.menu eq 'EVENT_MANAGE'}"> <!-- 통합캘린더관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/evt/selectSchdulinfoList.do"/>" <c:if test="${param.depth1 eq 'EVENT_LIST'}">class="slt"</c:if>>통합캘린더관리</a>
							<ul class="slist">
								<li><a href="<c:url value="/mng/evt/selectSchdulinfoList.do"/>" <c:if test="${empty param.searchSe}">class="slt"</c:if>>전체</a></li>
								<li><a href="<c:url value="/mng/evt/selectSchdulinfoList.do?searchSe=1"/>" <c:if test="${param.searchSe eq 1}">class="slt"</c:if>>행사및일반일정</a></li>
								<!-- <li><a href="<c:url value="/mng/evt/selectSchdulinfoList.do?searchSe=2"/>" <c:if test="${param.searchSe eq 2}">class="slt"</c:if>>이벤트</a></li>-->
								<li><a href="<c:url value="/mng/evt/selectSchdulinfoList.do?searchSe=3"/>" <c:if test="${param.searchSe eq 3}">class="slt"</c:if>>설문조사</a></li>
							</ul>
						</li>
					</ul>
				</c:when>

				<%--
				<c:when test="${param.menu eq 'MILEAGE_MANAGE'}"> <!-- 마일리지관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/mlg/selectMlginfoList.do"/>" <c:if test="${param.depth1 eq 'MILEAGE_SETUP'}">class="slt"</c:if>>마일리지설정</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/mlg/selectMlgUserlogList.do"/>" <c:if test="${param.depth1 eq 'MILEAGE_LIST'}">class="slt"</c:if>>마일리지 내역</a></li>
						<li class="smnu"><a href="<c:url value="/mng/rnk/MileageRanking.do"/>" <c:if test="${param.depth1 eq 'RANKING_MILEAGE'}">class="slt"</c:if>>마일리지 TOP 10</a></li>
					</ul>
				</c:when>
				--%>

				<%--
				<c:when test="${param.menu eq 'SMS_MANAGE'}"> <!-- SMS/이메일관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/sms/selectSmsManage.do"/>" <c:if test="${param.depth1 eq 'SMS_SEND'}">class="slt"</c:if>>SMS/이메일관리</a>
							<ul class="slist">
								<li><a href="<c:url value="/mng/sms/selectSmsManage.do"/>" <c:if test="${param.depth2 eq 'SMS'}">class="slt"</c:if>>개별메시지전송</a></li>
								<li><a href="<c:url value="/mng/sms/selectMmsManage.do"/>" <c:if test="${param.depth2 eq 'MMS'}">class="slt"</c:if>>그룹메세지전송</a></li>
								<!-- <li><a href="<c:url value="/mng/ems/selectEmsManage.do"/>" <c:if test="${param.depth2 eq 'EMS'}">class="slt"</c:if>>이메일전송</a></li> -->
							</ul>
						</li>
					</ul>
				</c:when>
				--%>

				<c:when test="${param.menu eq 'STAT_MANAGE'}"> <!-- 통계관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/sym/log/adminLoginLogList.do"/>" <c:if test="${param.depth1 eq 'ADMINLOGIN_LOG'}">class="slt"</c:if>>관리자접속로그</a>
						<li class="smnu"><a href="<c:url value="/mng/sym/log/mngWorkLogs.do"/>" <c:if test="${param.depth1 eq 'MNGWORK_LOG'}">class="slt"</c:if>>관리자작업로그</a>
						<li class="smnu"><a href="<c:url value="/mng/sts/selectScrinStats.do?statsKind=H"/>" <c:if test="${param.depth1 eq 'SCRIN_STAT'}">class="slt"</c:if>>일/월/년간 접속</a>
							<ul class="slist">
								<li><a href="<c:url value="/mng/sts/selectScrinStats.do?statsKind=H"/>" <c:if test="${param.statsKind eq 'H'}">class="slt"</c:if>>홈페이지</a></li>
									<%--
									<li><a href="<c:url value="/mng/sts/selectScrinStats.do?statsKind=M"/>" <c:if test="${param.statsKind eq 'M'}">class="slt"</c:if>>모바일</a></li>
									--%>
							</ul>
						</li>
						<li class="smnu"><a href="<c:url value="/mng/sts/selectBbsStats.do"/>" <c:if test="${param.depth1 eq 'BBS_STAT'}">class="slt"</c:if>>게시물 통계</a></li>
						<%--
						<li class="smnu"><a href="<c:url value="/mng/sts/selectMlgStats.do"/>" <c:if test="${param.depth1 eq 'MG_STAT'}">class="slt"</c:if>>마일리지 통계</a></li>
						<li class="smnu"><a href="<c:url value="/mng/sts/selectMbrStats.do"/>" <c:if test="${param.depth1 eq 'MBR_STAT'}">class="slt"</c:if>>회원가입 통계</a></li>
						<li class="smnu"><a href="<c:url value="/mng/sts/selectCmyStats.do"/>" <c:if test="${param.depth1 eq 'CMY_STAT'}">class="slt"</c:if>>커뮤니티개설 통계</a></li>
						<li class="smnu"><a href="https://www.google.com/analytics/web/?pli=1#report/visitors-overview/a29280954w55337777p56340389/" target="_blank">접속 통계</a></li>
						<li class="smnu">
							<a href="/mng/sts/selectEvtStats.do" <c:if test="${param.depth1 eq 'EVENT_STAT'}">class="slt"</c:if>>이벤트통계</a>
							<c:if test="${param.depth1 eq 'EVENT_STAT'}">
								<ul class="slist">
									<li><a href="<c:url value="/mng/sts/selectEvtStats.do"/>" <c:if test="${param.depth2 eq 'EVENT_DPT'}">class="slt"</c:if>>회차별 통계</a></li>
									<li><a href="<c:url value="/mng/sts/selectEvtStatUser.do"/>" <c:if test="${param.depth2 eq 'EVENT_USR'}">class="slt"</c:if>>사용자별 통계</a></li>
								</ul>
							</c:if>
						</li>
						--%>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'ETC_MANAGE'}"> <!-- 기타관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN001"/>" <c:if test="${param.bannerTyCode eq 'BAN001'}">class="slt"</c:if>>팝업존관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN002"/>" <c:if test="${param.bannerTyCode eq 'BAN002'}">class="slt"</c:if>>배너존관리</a></li>
						<%--
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN003"/>" <c:if test="${param.bannerTyCode eq 'BAN003'}">class="slt"</c:if>>퀵메뉴관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN004"/>" <c:if test="${param.bannerTyCode eq 'BAN004'}">class="slt"</c:if>>메인배너관리(Type1)</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN005"/>" <c:if test="${param.bannerTyCode eq 'BAN005'}">class="slt"</c:if>>메인배너관리(Type2)</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN006"/>" <c:if test="${param.bannerTyCode eq 'BAN006'}">class="slt"</c:if>>서브배너관리</a></li>
						--%>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/pwm/listPopup.do"/>" <c:if test="${param.depth1 eq 'ETC_POPUPWINDOW'}">class="slt"</c:if>>팝업관리</a></li>
						<%--
						<li class="smnu"><a href="http://intra.itshome.co.kr/EMT/?ref=tools/workpaper/board.emt&idx=000011162" <c:if test="${param.depth1 eq 'ETC_SCRIN'}">class="slt"</c:if>>요청게시판</a></li>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/sit/SiteListInqire.do"/>" <c:if test="${param.depth1 eq 'ETC_SITE'}">class="slt"</c:if>>링크사이트관리</a></li>
						--%>
						<li class="smnu"><a href="<c:url value="/mng/uss/ion/bnr/selectBannerList.do?bannerTyCode=BAN007"/>" <c:if test="${param.bannerTyCode eq 'BAN007'}">class="slt"</c:if>>메인이미지관리</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'OPEN_MANAGE'}"> <!-- 정보공개관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/cop/bbs/selectOpenBoardList.do"/>" <c:if test="${param.depth1 eq 'OPENBOARD_MANAGE'}">class="slt"</c:if> target="_self">정보목록</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'GFUND_MANAGE'}"> <!-- 녹색자금통합시스템관리 -->
					<ul class="list">
						<li class="smnu"><a href="/mng/cop/bbs/gfundNoticeList.do" <c:if test="${param.depth1 eq 'NOTICE_BBS_MANAGE'}">class="slt"</c:if> target="_self">공지사항</a></li>
						<li class="smnu"><a href="/mng/cop/bbs/gfundQnAList.do" <c:if test="${param.depth1 eq 'QNA_BBS_MANAGE'}">class="slt"</c:if> target="_self">Q&A</a></li>
						<li class="smnu"><a href="/mng/gfund/member/EgovMberManage.do" <c:if test="${param.depth1 eq 'MEMBER_MANAGE'}">class="slt"</c:if> target="_self">회원관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/gfund/biz/ancmt/announcementList.do"/>" <c:if test="${param.depth1 eq 'ANNOUNCEMENT_MANAGE'}">class="slt"</c:if> target="_self">사업공고</a></li>
						<li class="smnu"><a href="/mng/gfund/biz/selection/proposalList.do" <c:if test="${param.depth1 eq 'SELECTTYPE_MANAGE'}">class="slt"</c:if> target="_self">선정전형</a></li>
						<li class="smnu"><a href="<c:url value="/mng/gfund/biz/businessmng/EgovBusinessManageList.do"/>" <c:if test="${param.depth1 eq 'BUSINESS_MANAGE'}">class="slt"</c:if> target="_self">사업관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/gfund/biz/productmng/EgovProductManageList.do"/>" <c:if test="${param.depth1 eq 'RESULTS_MANAGE'}">class="slt"</c:if> target="_self">결과산출</a></li>
						<li class="smnu"><a href="/mng/cop/bbs/gfundPostManagement.do" <c:if test="${param.depth1 eq 'EXPOST_MANAGE'}">class="slt"</c:if> target="_self">사후관리</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'SANRIMDB_MANAGE'}"> <!-- 한반도산림복원자료실관리 -->
					<ul class="list">
						<li class="smnu"><a href="<c:url value="/mng/forest/usr/EgovMberManage.do"/>" <c:if test="${param.depth1 eq 'MEMBER_MANAGE'}">class="slt"</c:if> target="_self">회원관리</a></li>
						<li class="smnu"><a href="<c:url value="/mng/forest/nkrefo/EgovNkrefoManage.do"/>" <c:if test="${param.depth1 eq 'RESOURCE_MANAGE'}">class="slt"</c:if> target="_self">자료관리</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'UNDER_PRIVILEGED'}"> <!-- 소외계층 체험교육 게시판 -->
					<ul class="list">
						<li class="smnu"><a href="/mng/UnderPrivilegedBoardList.jsp" class="slt" target="_self">소외계층 숲체험교육</a></li>
					</ul>
				</c:when>

				<c:when test="${param.menu eq 'UNDER_CONTEST'}"> <!-- 아이디어 공모전 게시판 -->
					<ul class="list">
						<li class="smnu"><a href="/mng/UnderContestBoardList.jsp" class="slt" target="_self">아이디어 공모전</a></li>
					</ul>
				</c:when>

				<c:otherwise> <%--등록되지 않은 메뉴들 --%>
					<ul class="list">
						<li class="smnu"><a href="#" class="slt">메뉴1</a></li>
						<li class="smnu"><a href="#">메뉴2</a></li>
						<li class="smnu"><a href="#">메뉴3</a></li>
						<li class="smnu"><a href="#">메뉴4</a></li>
						<li class="smnu"><a href="#">메뉴5</a></li>
					</ul>
				</c:otherwise>
			</c:choose>
		</div>

		<div id="rightPage">
			<c:if test="${not empty param.title and empty param.ctnNo}">
				<div id="navi">
					<h2 class="naviTit"><c:out value='${param.title}'/></h2>
				</div>
			</c:if>
			<div id="contents">
