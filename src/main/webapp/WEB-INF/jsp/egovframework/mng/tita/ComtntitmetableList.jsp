<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<%-- 
<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchSe}"><c:param name="searchSe" value="${searchVO.searchSe}" /></c:if>
		<c:if test="${!empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
 --%>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="AFTERSCHOOL_MANAGE"/>
	<c:param name="depth1" value="TIMETABLE_LIST"/>
	<c:param name="depth2" value="TIMETABLE_LIST"/>
	<c:param name="title" value="시간표관리"/>
</c:import>	
<script type="text/javaScript" language="javascript" defer="defer">
<!--
function fn_addTimeTable(){
	var startHour = $("#startHour").val();
	var startMin = $("#startMin").val();
	var endHour = $("#endHour").val();
	var endMin = $("#endMin").val();
	var period = $("#period").val();
	var parameter = "period="+period+"&startHour="+startHour+"&startMin="+startMin+"&endHour="+endHour+"&endMin="+endMin;
	if(startHour==""){
		alert("시작시간을 입력하십시오.");
		return false;
	}else if(startMin == ""){
		alert("시작분을 입력하십시오.");
		return false;
	}else if(endHour == ""){
		alert("종료시간을 입력하십시오.");
		return false;
	}else if(endMin == ""){
		alert("종료분을 입력하십시오.");
		return false;
	}else{
		if(startHour+startMin>=endHour+endMin){
			alert("종료시간이 시작시간보다 앞에 있을수 없습니다.");
			return false;
		}else{
			$.ajax({
			   type: "POST",
			   url: "/mng/tita/addComtntitmetableAjax.do",
			   data: parameter,
			   success: function(msg){
				   console.log(msg);
			   	   alert( "성공적으로 처리되었습니다.");
		     	   location.href='/mng/tita/ComtntitmetableList.do';
			   },
			   error:function(e){
				   alert("error:"+e);
			   }
			});	
		}
	}
} 

function fn_register(form){
	$(".btn_r").hide();
	$(".addclass").html(
			"<tr><td></td><td><input type='text' name='period' id='period' class='inp' style='width:50px;' maxlength='10'/>교시</td>" + 
			"<td><input type='text' name='startHour' id='startHour' class='inp' style='width:50px;' maxlength='2'/>:<input type='text' name='startMin' id='startMin' class='inp' style='width:50px;' maxlength='2'/>~<input type='text' name='endHour' id='endHour' class='inp' style='width:50px;' maxlength='2'/>:<input type='text' name='endMin' id='endMin' class='inp' style='width:50px;' maxlength='2'/></td>"+
			"<td></td>"+
			"<td><a href='#' onclick='fn_addTimeTable();'><img src='${_IMG}/btn/btn_regist_new.gif' alt='신규 등록하기' /></a></td></tr>");
}

function fn_deleted(id){
	var frm = document.listForm;
	if(confirm("해당 시간표를 삭제하시겟습니까?")){
		frm.action="/mng/tita/deleteComtntitmetable.do?timetableId="+id;
		frm.submit();
		return true;
	}
}

function fn_update(id){
	var startHour = $("#"+id+"_sh").val();
	var startMin = $("#"+id+"_sm").val();
	var endHour = $("#"+id+"_eh").val();
	var endMin = $("#"+id+"_em").val();
	var period = $("#"+id+"_pr").val();
	var parameter = "timetableId="+id+"&period="+period+"&startHour="+startHour+"&startMin="+startMin+"&endHour="+endHour+"&endMin="+endMin;
	if(startHour==""){
		alert("시작시간을 입력하십시오.");
		return false;
	}else if(startMin == ""){
		alert("시작분을 입력하십시오.");
		return false;
	}else if(endHour == ""){
		alert("종료시간을 입력하십시오.");
		return false;
	}else if(endMin == ""){
		alert("종료분을 입력하십시오.");
		return false;
	}else{
		if(startHour+startMin>=endHour+endMin){
			alert("종료시간이 시작시간보다 앞에 있을수 없습니다.");
			return false;
		}else{
			if(confirm("해당 시간표를를 수정하시겟습니까?")){
				$.ajax({
					type:"POST",
					url:"/mng/tita/updateComtntitmetableAjax.do",
					data:parameter,
					succeess:function(msg){
						alert("성공적으로 처리되었습니다.");
			     	    location.href='/mng/tita/ComtntitmetableList.do';
					},
				    error:function(e){
				   		alert("error:"+e);
				    }
				});
			}
		}
	}
}

function fn_update_UseAt(id, useAt){
	var msg = "";
	if(useAt == 'N'){
		msg = "해당 시간표를 사용하지 않으시겠습니까?";
	}else{
		msg = "해당 시간표를 사용하시겠습니까?";
	}
	
	if(confirm(msg)){
		$.ajax({
			type:"POST",
			url:"/mng/tita/updateComtntitmetableAjax_UseAt.do",
			data:"timetableId="+id+"&useAt="+useAt,
			succeess:function(msg){
				alert("성공적으로 처리되었습니다.");
				location.href='/mng/tita/ComtntitmetableList.do';
			},
			error:function(e){
				alert("error:"+e);
			}
		})
	}
}
// -->
</script>

<div id="cntnts">
<c:if test="${USER_INFO.userSe > 10}">
	<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
		<div id="bbs_search">
			<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
		</div>
	</form>
</c:if>	
<form:form commandName="searchVO" name="listForm" id="listForm" method="post">
	<table class="chart_board">
	<colgroup>
		<col"/>
		<col"/>
		<col"/>
		<col"/>
		<col"/>
		<col"/>
	</colgroup>
	<thead>
		<tr>
			<th>번호</th>
			<th>교시</th>
			<th>시간</th>
			<th>사용</th>
			<th>관리</th>
		</tr>
	</thead>
	<tbody class="addclass">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
				<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
				<td align="center" class="listtd"><input type="text" id="${result.timetableId }_pr" value="${result.period }" class="inp" style="width:50px;"/></td>
				<td align="center" class="listtd">
					<input type="text" id="${result.timetableId }_sh" value="${result.startHour }" class="inp" style="width:50px;"/>:<input type="text" id="${result.timetableId }_sm" value="${result.startMin }"  class="inp" style="width:50px;"/>~
					<input type="text" id="${result.timetableId }_eh" value="${result.endHour }" class="inp" style="width:50px;"/>:<input type="text" id="${result.timetableId }_em" value="${result.endMin }"  class="inp" style="width:50px;"/>
				</td>
				<td align="center" class="listtd">
					<c:choose>
						<c:when test="${result.useAt eq 'Y' }"><a href="#" onclick="fn_update_UseAt('${result.lecRealmId}','N');"><img src="${_IMG }/board/use_yes.png" alt="적용" /></a></c:when>
						<c:otherwise><a href="#" onclick="fn_update_UseAt('${result.timetableId}','Y');"><img src="${_IMG }/board/use_no.png" alt="미적용" /></a></c:otherwise>
					</c:choose>
				</td>
				<td><a href="#" onclick="fn_update('${result.timetableId}');"><img src="${_IMG }/btn/edit.gif" alt="수정" /></a> <a href="#" onclick="fn_deleted('${result.timetableId}');"><img src="${_IMG }/btn/del.gif" alt="삭제" /></a></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="6">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>
		</tbody>
	</table>
	<!-- /List -->
	<div class="btn_r">
       	<a href="#" onclick="fn_register(this);"><img src="${_IMG}/btn/btn_regist.gif" /></a>
		<%-- <a href="#" onclick="fncAddView();return false;"><img src="${_IMG}/btn/btn_regist.gif" /></a> --%>
	</div>
	</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	