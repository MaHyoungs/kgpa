<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${not empty ImportUrl}">
	<c:import url="/EgovPageLink.do?link=${ImportUrl}" charEncoding="utf-8" var="html"/>
	<c:set var="html" value="${fn:replace(html, '$SITE_NM$', siteInfo.siteNm)}"/>
	<c:set var="html" value="${fn:replace(html, '$PHONE_NO$', siteInfo.tlphonNo)}"/>
	<c:set var="html" value="${fn:replace(html, '$FAX_NO$', siteInfo.faxNo)}"/>
	<c:out value="${html}" escapeXml="false"/>
</c:if>
<c:if test="${empty ImportUrl}">
	<c:if test="${currMpm.tabIsUse eq 'N'}">
		<c:if test="${currMpm.htmlUseAt eq 'Y'}">
			<c:catch var="ex">
				<c:import url="/EgovPageLink.do?link=${MnuFileStoreWebPathByJspFile}${currMpm.siteId}/${currMpm.menuId}${PUBLISH_APPEND_FREFIX}" charEncoding="utf-8"/>
			</c:catch>
			<c:if test="${ex != null}">
				publishing not found
			</c:if>
		</c:if>
		<c:if test="${currMpm.cntntsTyCode eq 'CTS05'}">
			<iframe id="cntnsIframe" src="${currMpm.url}" scrolling="no" frameborder="0" width="100%" height="2000" title="<c:out value="${currMpm.menuNm}"/>"></iframe>
		</c:if>
		<c:if test="${currMpm.cntntsTyCode eq 'CTS06'}">
			<c:catch var="ex">
				<c:import url="${currMpm.url}" charEncoding="utf-8"/>
			</c:catch>
			<c:if test="${ex != null}">
				포틀릿을 가져오는데 문제가 발생하였습니다.
			</c:if>
		</c:if>
	</c:if>
	<c:if test="${currMpm.tabIsUse eq 'Y'}">
		<div class="tab_box">
			<ul class="tabs">
				<c:set var="idx" value="1" />
				<c:forEach var="mpmArr" items="${mpmList}">
					<c:if test="${mpmArr.upperMenuId eq currMpm.menuId and mpmArr.expsrUseAt eq 'Y'}">
						<li class="tabCts<c:if test="${idx eq 1}"> active</c:if>" ><a href="#tab${idx}">${mpmArr.menuNm}</a></li>
						<c:set var="idx" value="${idx + 1}" />
					</c:if>
				</c:forEach>
			</ul>

			<div class="tab_content">
				<c:set var="idx" value="1" />
				<c:forEach var="mpmArr" items="${mpmList}">
					<c:if test="${mpmArr.upperMenuId eq currMpm.menuId and mpmArr.expsrUseAt eq 'Y'}">
						<div id="tab${idx}" class="tab_cont" <c:if test="${empty param.tab and idx ne 1}">style="display:none;"</c:if><c:if test="${not empty param.tab and idx ne param.tab}">style="display:none;"</c:if>>
							<c:if test="${mpmArr.htmlUseAt eq 'Y'}">
								<c:catch var="ex">
									<c:import url="/EgovPageLink.do?link=${MnuFileStoreWebPathByJspFile}${mpmArr.siteId}/${mpmArr.menuId}${PUBLISH_APPEND_FREFIX}" charEncoding="utf-8"/>
								</c:catch>
								<c:if test="${ex != null}">
									publishing not found
								</c:if>
							</c:if>
						</div>
						<c:set var="idx" value="${idx + 1}" />
					</c:if>
				</c:forEach>
			</div>
		</div>
	</c:if>
</c:if>


