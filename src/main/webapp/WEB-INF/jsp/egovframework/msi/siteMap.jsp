<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="HPG_IMG" value="${pageContext.request.contextPath}/template/homepage/011/images" />

<div id="all_sitemap">
	<c:set var="idx" value="1" />
	<c:forEach var="result1" items="${mnMnuList}" varStatus="status1">
		<c:set var="divClose" value="0" />
		<c:if test="${(result1.expsrUseAt eq 'Y') and (result1.menuLevel eq 1)}">
			<c:if test="${idx eq 1 or idx % 3 eq 1}">
				<c:set var="divClose" value="1" />
				<div class="sitemap_line">
			</c:if>
					<dl class="site01">
						<dt>
							<a href="${fn:replace(result1.autoMenuWebPath, '&' ,'&amp;')}" <c:if test="${result1.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${result1.menuNm}" /></a>
						</dt>
						<dd class="site_con">
							<ul>
								<c:forEach var="result2" items="${mnMnuList}" varStatus="status2">
									<c:if test="${result1.menuId eq result2.upperMenuId and result2.expsrUseAt eq 'Y'}">
										<c:choose>
											<c:when test="${result2.menuLastNodeAt eq 'Y'}">
												<li>
													<a href="${fn:replace(result2.autoMenuWebPath, '&' ,'&amp;')}" <c:if test="${result2.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${result2.menuNm}" /></a>
												</li>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${result2.tabIsUse eq 'Y'}">
														<li>
															<a href="${fn:replace(result2.autoMenuWebPath, '&' ,'&amp;')}" <c:if test="${result2.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${result2.menuNm}" /></a>
														</li>
													</c:when>
													<c:when test="${result2.tabIsUse eq 'N'}">
														<li>
															<a href="<c:out value="${fn:replace(result2.autoMenuWebPath, '&' ,'&amp;')}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, result2.menuId)}">on</c:if>" <c:if test="${result2.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${result2.menuNm}" /></a>
															<ul>
																<c:forEach var="result3" items="${mpmList}" varStatus="status3">
																	<c:if test="${result3.menuLevel eq 3 and fn:contains(result3.menuPathById, result2.menuId) and result3.expsrUseAt eq 'Y'}">
																		<li>
																			<a href="${fn:replace(result3.autoMenuWebPath, '&' ,'&amp;')}" class="<c:if test="${fn:contains(currMpm.menuPathById, result3.menuId)}">on</c:if>" <c:if test="${result3.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${result3.menuNm}" /></a>
																		</li>
																	</c:if>
																</c:forEach>
															</ul>
														</li>
													</c:when>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
							</ul>
						</dd>
					</dl>
			<c:if test="${idx eq 3 or idx eq 7 or idx % 3 eq 0}">
				</div>
			</c:if>
			<c:set var="idx" value="${idx + 1}" />
		</c:if>
	</c:forEach>
</div>
