<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:if test="${fn:length(DietList) != 0}">
	오늘의 식단<br/>
</c:if>
<c:forEach var="result" items="${DietList}" varStatus="status">
	<c:if test="${not empty result.side5 }">＊ ${result.side5 }<br/></c:if>		 
	<c:if test="${not empty result.rice }">＊ ${result.rice }<br/></c:if>		 
	<c:if test="${not empty result.stew }">＊ ${result.stew }<br/></c:if>
	<c:if test="${not empty result.side1 }">＊ ${result.side1 }<br/></c:if>		 
	<c:if test="${not empty result.side2 }">＊ ${result.side2 }<br/></c:if>		 
	<c:if test="${not empty result.side3 }">＊ ${result.side3 }<br/></c:if>		 
	<c:if test="${not empty result.side6 }">＊ ${result.side6 }<br/></c:if>		 
</c:forEach>
<c:if test="${fn:length(DietList) == 0}">
	금일 등록된 식단표가 없습니다.
</c:if>