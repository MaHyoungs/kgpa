<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
	<c:when test="${param.style eq 'mainList'}">
		<c:forEach var="rs" items="${resultList}">
			<c:choose>
				<c:when test="${rs.biz_ty_code eq 'BTC01'}">
					<li class="bg1">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br/>함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm} 공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC08'}">
					<li class="bg7">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC09'}">
					<li class="bg9">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC05'}">
					<li class="bg5">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC02'}">
					<li class="bg2">
						<strong>복지시설 나눔숲</strong>
						<span>(특수교육시설) </span>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC03'}">
					<li class="bg3">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC07'}">
					<li class="bg7">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC04'}">
					<li class="bg4">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">체험교육 나눔숲  공모하기</a>
					</li>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC06'}">
					<li class="bg6">
						<strong>${rs.biz_ty_code_nm}</strong>
						<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
						<a href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}  공모하기</a>
					</li>
				</c:when>
			</c:choose>
		</c:forEach>
	</c:when>
	<c:when test="${param.style eq 'mainPaginate'}">
		<c:forEach var="rs" items="${resultList}">
			<c:choose>
				<c:when test="${rs.biz_ty_code eq 'BTC01'}">
					<a class="active" href="#link">관련링크 1</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC08'}">
					<a href="#link">관련링크 8</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC09'}">
					<a href="#link">관련링크 8</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC05'}">
					<a href="#link">관련링크 5</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC02'}">
					<a href="#link">관련링크 2</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC03'}">
					<a href="#link">관련링크 3</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC07'}">
					<a href="#link">관련링크 7</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC04'}">
					<a href="#link">관련링크 4</a>
				</c:when>
				<c:when test="${rs.biz_ty_code eq 'BTC06'}">
					<a href="#link">관련링크 6</a>
				</c:when>
			</c:choose>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<c:forEach var="rs" items="${resultList}">
			<li><a
					<c:if test="${rs.biz_ty_code eq param.biz_ty_code}">class="active"</c:if> href="/gfund/biz/ancmt/announcementView.do?year=${rs.year}&amp;biz_ty_code=${rs.biz_ty_code}">${rs.biz_ty_code_nm}</a></li>
		</c:forEach>
	</c:otherwise>
</c:choose>
