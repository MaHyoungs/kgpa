<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="_IMG" value="${param.tmplatCours}" />
<c:choose>
	<c:when test="${searchVO.tableId eq 'PopupZone'}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<c:if test="${not empty param.listBtn}">
				<a href="#link${status.count}"<c:if test="${status.count eq 1}"> class="active"</c:if>>이벤트 ${status.count}</a>
			</c:if>
			<c:if test="${empty param.listBtn}">
				<c:if test="${not empty searchVO.listTag}"><${searchVO.listTag}></c:if>
				<a href="${result.linkUrl}" <c:if test="${result.popupTrgetAt eq 'Y'}">target="_blank" title="새창열림"</c:if>><img src="http://${siteInfo.siteUrl}${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" alt="${result.bannerDc}" /></a>
				<c:if test="${not empty searchVO.listTag}"></${searchVO.listTag}></c:if>
			</c:if>
		</c:forEach>
	</c:when>
	<%--<c:when test="${searchVO.tableId eq 'QuickZone'}">--%>
	<%--<c:forEach var="result" items="${resultList}" varStatus="status">--%>
	<%--<c:if test="${not empty searchVO.listTag}"><${searchVO.listTag}></c:if>--%>
	<%--<c:forEach var="colum" items="${searchVO.columInfo}">--%>
	<%--<c:if test="${not empty colum.tag}">--%>
	<%--<${colum.tag} <c:if test="${colum.tag eq 'a'}"> href="<c:out value="${result.linkUrl}" />" onclick="ssoLinkAction(this)" <c:if test="${result.popupTrgetAt eq 'Y'}">target="_blank"</c:if> title="<c:out value="${result.bannerNm}" />"</c:if><c:if test="${not empty colum.cssClass}"> class="${colum.cssClass}"</c:if>>--%>
	<%--</c:if>--%>
	<%--<c:choose>--%>
	<%--<c:when test="${colum.colname eq 'bannerImage'}"><img src="http://${siteInfo.siteUrl}${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" alt="<c:out value="${result.bannerNm}"/>" /></c:when>--%>
	<%--<c:when test="${colum.colname eq 'bannerNm'}"><c:out value="${result.bannerNm}" /></c:when>--%>
	<%--<c:when test="${colum.colname eq 'bannerUrl'}"><c:out value="${result.linkUrl}" /></c:when>--%>
	<%--<c:when test="${colum.colname eq 'linkButton'}"><img src="${_IMG}/page/btn_skip.gif" alt="바로가기" /></c:when>--%>
	<%--<c:otherwise>-</c:otherwise>--%>
	<%--</c:choose>--%>
	<%--<c:if test="${not empty colum.tag}"></${colum.tag}></c:if>--%>
	<%--</c:foreach>--%>
	<%--<span>${result.bannerNm }</span>--%>
	<%--<c:if test="${not empty searchVO.listTag}"></${searchVO.listTag}></c:if>--%>
	<%--</c:forEach>--%>
	<%--</c:when>--%>
	<c:when test="${searchVO.tableId eq 'BannerZone'}">
		<ul id="banner_list">
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<li>
					<a href="<c:out value="${result.linkUrl}" escapeXml="false" />" <c:if test="${result.popupTrgetAt eq 'Y'}">target="_blank" title="새창열림"</c:if>>
						<img src="http://${siteInfo.siteUrl}${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" alt="${result.bannerNm}" />
					</a>
				</li>
			</c:forEach>
		</ul>
	</c:when>
	<%--메인배경이미지--%>
	<c:when test="${searchVO.tableId eq 'idxBg'}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<li style="background:url('${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}') center 0px no-repeat;"></li>
		</c:forEach>
		<%--
		<a href="#link"><span
			<img src="http://${siteInfo.siteUrl}${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" alt="${result.bannerNm}" />
		</a>
		--%>
	</c:when>
	<%--메인배경썸네일이미지--%>
	<c:when test="${searchVO.tableId eq 'idxTh'}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<a href="#link">
				<span <c:if test="${status.count eq 1}">class="on"</c:if>>
					<img src="${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" height="120px" width="100px" alt="배경이미지 ${status.count}"/><strong class="png"></strong>
				</span>
			</a>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<c:if test="${not empty searchVO.listTag}"><${searchVO.listTag}></c:if>
			<c:forEach var="colum" items="${searchVO.columInfo}">
				<c:if test="${not empty colum.tag}">
					<${colum.tag} <c:if test="${colum.tag eq 'a'}"> href="<c:out value="${result.linkUrl}" />" onclick="ssoLinkAction(this)" <c:if test="${result.popupTrgetAt eq 'Y'}">target="_blank"</c:if> title="<c:out value="${result.bannerNm}" />"</c:if><c:if test="${not empty colum.cssClass}"> class="${colum.cssClass}"</c:if>>
				</c:if>
				<c:choose>
					<c:when test="${colum.colname eq 'bannerImage'}"><img src="http://${siteInfo.siteUrl}${BannerFileStoreWebPath}${siteInfo.siteId}/${result.bannerImageFile}" alt="<c:out value="${result.bannerNm}"/>" /></c:when>
					<c:when test="${colum.colname eq 'bannerNm'}"><c:out value="${result.bannerNm}" /></c:when>
					<c:when test="${colum.colname eq 'bannerUrl'}"><c:out value="${result.linkUrl}" /></c:when>
					<c:when test="${colum.colname eq 'linkButton'}"><img src="${_IMG}/page/btn_skip.gif" alt="바로가기" /></c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
				<c:if test="${not empty colum.tag}"></${colum.tag}></c:if>
			</c:forEach>
			<c:if test="${not empty searchVO.listTag}"></${searchVO.listTag}></c:if>
		</c:forEach>
	</c:otherwise>
</c:choose>