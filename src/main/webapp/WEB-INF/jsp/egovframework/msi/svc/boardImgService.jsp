
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<c:forEach var="result" items="${resultList}" varStatus="status">
		<c:if test="${(flag eq false) and (status.count eq 1)}" >
			<c:set var="bbsId" value="${result.bbsId}" />
			<c:set var="nttSj" value="${result.nttSj}" />
			<c:set var="nttNo" value="${result.nttNo}" />
		</c:if>

		<c:if test="${result.atchFileId eq ImgFile.atchFileId}">
			<c:set var="bbsId" value="${result.bbsId}" />
			<c:set var="nttSj" value="${result.nttSj}" />
			<c:set var="nttNo" value="${result.nttNo}" />
			<c:set var="linkMenuId" value="${searchVO.linkMenuId}" />
		</c:if>
	</c:forEach>

	<c:if test="${(not empty resultList) and (flag eq true)}">

		<a href="/cop/bbs/selectBoardArticle.do?menuId=${linkMenuId}&amp;bbsId=${bbsId}&amp;nttNo=${nttNo}" class="thumb">
			<c:url var="imgUrl" value="/cmm/fms/getImage.do">
				<c:param name="thumbYn" value="Y" />
				<c:param name="siteId" value="${siteInfo.siteId}"/>
				<c:param name="appendPath" value="${bbsId}"/>
				<c:param name="atchFileNm" value="${ImgFile.streFileNm}.${ImgFile.fileExtsn}"/>
				<c:param name="fileStorePath" value="${fileStorePath}"/>
				<c:param name="width" value="143"/>
				<c:param name="height" value="93"/>
			</c:url>
			<span><img src="${imgUrl}" width="143" height="93" alt="${nttSj}" /></span>
			<strong>${nttSj}</strong>
		</a>
	</c:if>

	<c:if test="${(empty resultList) or (flag eq false)}">
		<a href="/cop/bbs/selectBoardArticle.do?menuId=${linkMenuId}&amp;bbsId=${bbsId}&amp;nttNo=${nttNo}" class="thumb">
			<span><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/img_noimg.gif" alt="${nttSj}"  /></span>
			<strong>${nttSj}</strong>
		</a>
	</c:if>

