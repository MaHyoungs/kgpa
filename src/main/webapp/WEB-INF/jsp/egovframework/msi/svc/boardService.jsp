
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
	<c:when test="${param.style eq 2}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<c:set var="sj" value="${fn:trim(result.nttSj)}" />
			<li><a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">${sj}</a> <span><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></span></li>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<li<c:if test="${status.count eq 1}"> class="on"</c:if>>
				<c:set var="sj" value="${fn:trim(result.nttSj)}" />
				<strong>${sj}</strong>
				<span><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></span>
				<c:set var="cn" value="${fn:trim(result.nttCn)}" />
				<p>${fn:escapeXml(cn)}</p>
				<a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">자세히보기</a>
			</li>
		</c:forEach>
	</c:otherwise>
</c:choose>

<c:if test="${empty resultList}">
	<li>등록된 게시물이 없습니다.</li>
</c:if>

