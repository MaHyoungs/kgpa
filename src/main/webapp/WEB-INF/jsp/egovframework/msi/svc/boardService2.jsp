
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
	<c:when test="${param.style eq 2}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<c:set var="sj" value="${fn:trim(result.nttSj)}" />
			<li><a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">${sj}</a> <span><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></span></li>
		</c:forEach>
	</c:when>
	<c:when test="${param.style eq 99}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<c:set var="sj" value="${fn:trim(result.nttSj)}" />
				<li><a href="/cop/bbs/selectNkrefoBoardArticle.do?bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}&amp;viewType=bbsDetail"><span>${sj}</span>[${result.commentCount}]</a>  <span><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></span></li>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<c:if test="${searchVO.linkMenuId eq 'MNU_0000000000000051'}" >
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<li<c:if test="${status.count eq 1}"> class="on"</c:if>>
					<c:set var="sj" value="${fn:trim(result.nttSj)}" />
					<c:choose>
						<c:when test="${empty result.tmp01}">
							<a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">${sj}</a>
						</c:when>
						<c:otherwise>
							<a href="<c:out value="${result.tmp01}"/>" target="_blank"><c:out value="${sj}"/></a>
						</c:otherwise>
					</c:choose>
				</li>
			</c:forEach>
		</c:if>
		<c:if test="${(searchVO.linkMenuId ne 'MNU_0000000000000051') and (searchVO.linkMenuId ne 'MNU_0000000000000153')}" >
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<li<c:if test="${status.count eq 1}"> class="on"</c:if>>
					<c:set var="sj" value="${fn:trim(result.nttSj)}" />
					<a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">${sj}</a>
					<span><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></span>
				</li>
			</c:forEach>
		</c:if>
		<c:if test="${searchVO.linkMenuId eq 'MNU_0000000000000153'}" >
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<li<c:if test="${status.count eq 1}"> class="on"</c:if>>
					<c:set var="sj" value="${fn:trim(result.nttSj)}" />
					<a href="/cop/bbs/selectBoardArticle.do?menuId=${searchVO.linkMenuId}&amp;bbsId=${result.bbsId}&amp;nttNo=${result.nttNo}">${sj}</a>
					<c:if test="${status.count eq 1}"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/icon_new.png" alt="새글" /></c:if>
				</li>
			</c:forEach>
		</c:if>
	</c:otherwise>
</c:choose>

<c:if test="${empty resultList}">
	<li>등록된 게시물이 없습니다.</li>
</c:if>

