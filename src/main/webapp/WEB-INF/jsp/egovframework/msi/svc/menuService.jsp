<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
<c:set var="menuExplain" value="" />

<c:choose>
	<c:when test="${param.menuTarget eq 'Main'}">
		<c:choose>
			<c:when test="${param.menuType eq 'FullDisplay'}">
				<c:set var="mpmDepth01Count" value="1"/>
					<c:forEach var="mpmDepth01" items="${mpmList}" varStatus="status">
						<c:if test="${mpmDepth01.menuLevel eq 1 and mpmDepth01Count <= 9 and mpmDepth01.expsrUseAt eq 'Y'}">
							<c:choose>
								<c:when test="${mpmDepth01Count eq 1}">
									<div class="mline">
								</c:when>
								<c:when test="${mpmDepth01Count eq 7}">
									<div class="mline">
								</c:when>
							</c:choose>
						<dl>
							<dt><c:out value="${mpmDepth01.menuNm}"/></dt>
							<dd>
								<ul>
									<c:forEach var="mpmDepth02" items="${mpmList}">
										<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, mpmDepth01.menuId) and mpmDepth02.expsrUseAt eq 'Y'}">
											<li><a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>"><c:out value="${mpmDepth02.menuNm}"/></a>
											<ul class="2depth">
												<c:forEach var="mpmDepth03" items="${mpmList}">
													<c:if test="${mpmDepth03.menuLevel eq 3 and fn:contains(mpmDepth03.menuPathById, mpmDepth02.menuId) and mpmDepth03.expsrUseAt eq 'Y'}">
														<li><a href="<c:out value="${mpmDepth03.autoMenuWebPath}"/>"><c:out value="${mpmDepth03.menuNm}"/></a></li>
													</c:if>
												</c:forEach>

											</ul>
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</dd>
						</dl>
						<c:set var="mpmDepth01Count" value="${mpmDepth01Count + 1}"/>
						<c:choose>
							<c:when test="${mpmDepth01Count eq 7}">
							</div>
							</c:when>
						</c:choose>
						</c:if>
					</c:forEach>
				</div>
			</c:when>
			<c:when test="${param.menuType eq 'Simple2Depth'}">
				<c:set var="mpmDepth01Count" value="1"/>
				<c:forEach var="mpmDepth01" items="${mpmList}" varStatus="status">
				<c:if test="${mpmDepth01.menuLevel eq 1 and mpmDepth01Count <= 9 and mpmDepth01.expsrUseAt eq 'Y'}">
				<li class="m${mpmDepth01Count }">
					<c:set var="menuPathByName" value="${fn:split(currMpm.menuPathByName,'>')}"/>
					<c:set var="depth01TopMenuName" value=""/>
					<c:forEach items="${menuPathByName}" var="menuNm_1depth" varStatus="item">
						<c:if test="${item.count eq 1}">
							<c:set var="depth01TopMenuName" value="${menuNm_1depth}"/>
						</c:if>
					</c:forEach>
					<a href="<c:out value="${mpmDepth01.autoMenuWebPath}"/>" <c:if test="${mpmDepth01.nwdAt eq 'Y'}">target="_blank"</c:if> <c:if test="${mpmDepth01.menuNm eq depth01TopMenuName}">class="on"</c:if>>
						<%--<c:out value="${mpmDepth01.menuNm}"/>--%>
						${mpmDepth01.menuNm}
					</a>
					<ul>
						<c:forEach var="mpmDepth02" items="${mpmList}">
						<c:set var="mpmDepth02Count" value="1"/>
						<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, mpmDepth01.menuId) and mpmDepth02.expsrUseAt eq 'Y'}">
							<li>
								<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>>
									<%--<c:out value="${mpmDepth02.menuNm}"/>--%>
									<c:if test="${mpmDepth02.menuId eq 'MNU_0000000000000037'}">
										${fn:substring(mpmDepth02.menuNm, 0, 7)}<br />${fn:substring(mpmDepth02.menuNm, 7, fn:length(mpmDepth02.menuNm))}
									</c:if>
									<c:if test="${mpmDepth02.menuId ne 'MNU_0000000000000037'}">
										${mpmDepth02.menuNm}
									</c:if>
								</a>
							</li>
							<c:set var="mpmDepth02Count" value="${mpmDepth02Count + 1 }"/>
						</c:if>
						<%-- <c:if test="${mpmDepth02Count eq 1}"><li class="hdn">&nbsp;</li></c:if> --%>
						</c:forEach>
					</ul>
				</li>
				<c:set var="mpmDepth01Count" value="${mpmDepth01Count + 1}"/>
				</c:if>
				<c:if test="${fn:length(mpmList) eq 0}"><li class="alt_msg">메뉴를 생성하세요</li></c:if>
				</c:forEach>
			</c:when>

			<%-- 서브타이틀이미지 --%>
			<c:when test="${param.menuType eq 'subtitle'}">
				<c:set var="mpmDepth01Count" value="1"/>
				<c:forEach var="mpmDepth01" items="${mpmList}" varStatus="status">
				<c:if test="${mpmDepth01.menuLevel eq 1 and mpmDepth01Count <= 9 and mpmDepth01.expsrUseAt eq 'Y'}">
				<li>
					<a href="<c:out value="${mpmDepth01.autoMenuWebPath}"/>" <c:if test="${mpmDepth01.nwdAt eq 'Y'}">target="_blank"</c:if>>
						<c:out value="${mpmDepth01.menuNm}"/>
						<%-- <img src="${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images/common/menu0${mpmDepth01Count}_off.png" alt="<c:out value="${mpmDepth01.menuNm}"/>"/> --%>
					</a>
					<div class="smenu m${mpmDepth01Count }">
						<ul>
							<c:forEach var="mpmDepth02" items="${mpmList}">
							<c:set var="mpmDepth02Count" value="1"/>
							<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, mpmDepth01.menuId) and mpmDepth02.expsrUseAt eq 'Y'}">
								<li>
									<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>>
										<c:out value="${mpmDepth02.menuNm}"/>
									</a>
								</li>
								<c:set var="mpmDepth02Count" value="${mpmDepth02Count + 1 }"/>
							</c:if>
							<%-- <c:if test="${mpmDepth02Count eq 1}"><li class="hdn">&nbsp;</li></c:if> --%>
							</c:forEach>
						</ul>
					</div>
				</li>
				<c:set var="mpmDepth01Count" value="${mpmDepth01Count + 1}"/>
				</c:if>
				<c:if test="${fn:length(mpmList) eq 0}"><li class="alt_msg">메뉴를 생성하세요</li></c:if>
				</c:forEach>
			</c:when>
		</c:choose>
	</c:when>
	<c:when test="${param.menuTarget eq 'Sub'}">
		<c:choose>
			<%-- 2차 메뉴 부터 출력 --%>
			<c:when test="${param.menuType eq '2Depth'}">
				<c:forEach var="mpmDepth02" items="${mpmList}" varStatus="status">
					<c:if test="${not empty currRootMpm.menuId}">
						<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, currRootMpm.menuId) and mpmDepth02.expsrUseAt eq 'Y'}">
							<c:choose>
								<c:when test="${mpmDepth02.menuLastNodeAt eq 'Y'}">
									<li>
										<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">on</c:if>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>>
											<%--<c:out value="${mpmDepth02.menuNm}"/>--%>
											<c:if test="${not empty mpmDepth02.progrmFactr}">
												${fn:substring(mpmDepth02.menuNm, 0, mpmDepth02.progrmFactr)}<br />${fn:substring(mpmDepth02.menuNm, mpmDepth02.progrmFactr, fn:length(mpmDepth02.menuNm))}
											</c:if>
											<c:if test="${empty mpmDepth02.progrmFactr}">
												<c:out value="${mpmDepth02.menuNm}" />
											</c:if>
										</a>
									</li>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${mpmDepth02.tabIsUse eq 'Y'}">
											<li>
												<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">on</c:if>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>>
													<%--<c:out value="${mpmDepth02.menuNm}"/>--%>
													${mpmDepth02.menuNm}
												</a>
											</li>
										</c:when>
										<c:when test="${mpmDepth02.tabIsUse eq 'N'}">
											<li>
												<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">on</c:if>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>>
													<c:if test="${mpmDepth02.menuId eq 'MNU_0000000000000037'}">
														${fn:substring(mpmDepth02.menuNm, 0, 7)}<br />${fn:substring(mpmDepth02.menuNm, 7, fn:length(mpmDepth02.menuNm))}
													</c:if>
													<c:if test="${mpmDepth02.menuId ne 'MNU_0000000000000037'}">
														<c:out value="${mpmDepth02.menuNm}"/>
													</c:if>
												</a>
												<ul class="depth03">
													<c:forEach var="mpmDepth03" items="${mpmList}">
														<c:if test="${mpmDepth03.menuLevel eq 3 and fn:contains(mpmDepth03.menuPathById, mpmDepth02.menuId) and mpmDepth03.expsrUseAt eq 'Y'}">
															<li>
																<a href="<c:out value="${mpmDepth03.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth03.menuId)}">on</c:if>" <c:if test="${mpmDepth03.nwdAt eq 'Y'}">target="_blank"</c:if>>
																	<%--<c:out value="${mpmDepth03.menuNm}"/>--%>
																	${mpmDepth03.menuNm}
																</a>
															</li>
														</c:if>
													</c:forEach>
												</ul>
											</li>
										</c:when>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:if>
				</c:forEach>
			</c:when>

			<%-- 해당 메뉴의 최상위 메뉴의 설명값 불러오기 시작 --%>
			<c:when test="${param.menuType eq 'menuExplain'}">
				<c:set var="menuPathById" value="${fn:split(currMpm.menuPathById,'>')}"/>
				<c:set var="currParentMenuId" value="" />
				<c:set var="currParentMenuExplain" value="" />
				<c:forEach items="${menuPathById}" var="id" varStatus="item">
					<c:if test="${item.index eq 1}">
						<c:set var="currParentMenuId" value="${id}" />
					</c:if>
				</c:forEach>
				<c:forEach var="mpmListTemp" items="${mpmList}" varStatus="tempStatus">
					<c:if test="${not empty mpmDepth02.menuExplain}">
						<c:set var="currParentMenuExplain" value="mpmListTemp.menuExplain" />
					</c:if>
					<c:if test="${empty mpmDepth02.menuExplain}">
						<c:if test="${currParentMenuId eq mpmListTemp.menuId}">
							<c:set var="currParentMenuExplain" value="${mpmListTemp.menuExplain}" />
						</c:if>
					</c:if>
				</c:forEach>
				<c:if test="${not empty currParentMenuExplain}">
					${currParentMenuExplain}
				</c:if>
				<%-- 해당 메뉴의 최상위 메뉴의 설명값 불러오기 끝 --%>
			</c:when>
		</c:choose>
	</c:when>
	<c:when test="${param.menuTarget eq 'Sub_mobile'}">
		<c:forEach var="mpmDepth02" items="${mpmList}" varStatus="status">
			<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, currRootMpm.menuId) and mpmDepth02.expsrUseAt eq 'Y' and mpmDepth02.mobileUseAt eq 'Y'}">
				<c:choose>
					<c:when test="${mpmDepth02.menuLastNodeAt eq 'Y'}">
						<li>
							<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">on</c:if>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth02.menuNm}"/></a>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="<c:out value="${mpmDepth02.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">on</c:if>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth02.menuNm}"/></a>
							<ul class="depth02">
								<c:forEach var="mpmDepth03" items="${mpmList}">
									<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId) and mpmDepth03.menuLevel eq 3 and fn:contains(mpmDepth03.menuPathById, mpmDepth02.menuId) and mpmDepth03.expsrUseAt eq 'Y'}">
										<li><a href="<c:out value="${mpmDepth03.autoMenuWebPath}"/>" class="<c:if test="${fn:contains(currMpm.menuPathById, mpmDepth03.menuId)}">on</c:if>" <c:if test="${mpmDepth03.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth03.menuNm}"/></a></li>
									</c:if>
								</c:forEach>
							</ul>
						</li>
					</c:otherwise>
				</c:choose>
			</c:if>
		</c:forEach>
	</c:when>
</c:choose>