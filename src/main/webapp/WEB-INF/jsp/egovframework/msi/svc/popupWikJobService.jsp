<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="${param.tmplatCours}"/>

<script type="text/javascript">
//<![CDATA[
	<c:forEach items="${popupWikJobList1}" var="resultInfo" varStatus="status">
		<c:if test="${status.count eq 1}">
			if(fnGetCookiePopup('${resultInfo.popupId}') == null ){
			 	fn_egov_popupOpen_PopupManage1('POPUP_00000000009999',
			 			'/uss/ion/pwn/099',
			 			'462',
		    	    	'340',
		    	    	'100',
		    	    	'100',
		    	    	'Y',
		    	    	'${resultInfo.nttSj}',
		    	    	'');
			}
		</c:if>
	</c:forEach>
	//]]>	
</script>