<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="repleTxt">
	<strong class="orange">${searchVO.searchWrd}</strong>
</c:set>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/search/images" />
<c:import url="/msi/sch/tmplatHead.do" charEncoding="utf-8" />
<div class="search_word">
	<strong class="sword">검색어</strong> "<strong class="orange"><c:out value="${searchVO.searchWrd}" /></strong>"에 대한 검색결과 입니다.
</div>
<div class="result_box">
	<h3 class="icon1">게시물검색결과</h3>
	<div class="result1">
		<c:if test="${fn:length(bbsResultList) == 0}">
			<p class="empty">
				<spring:message code="common.nodata.msg" />
			</p>
		</c:if>
		<c:set var="bbsCnt" value="0" />
		<c:if test="${fn:length(bbsResultList) > 0}">
			<ul>
				<c:forEach var="result" items="${bbsResultList}" varStatus="status">
					<%-- 웹접근성 때문에 동영상 게시판 검색 X --%>
					<c:if test="${result.bbsId ne 'BBSMSTR_000000000027'}">
						<c:set var="bbsCnt" value="${bbsCnt + 1}" />
						<c:url var="viewUrl" value="http://${result.siteUrl}/cop/bbs/selectBoardArticle.do">
							<c:param name="nttNo" value="${result.nttNo}" />
							<c:param name="menuId" value="${result.menuId}" />
							<c:param name="bbsId" value="${result.bbsId}" />
							<c:param name="searchWrd" value="${searchVO.searchWrd}" />
						</c:url>
						<li>
							<c:choose>
								<%-- 뉴스 게시판 : 사업단 소식, 국내사업 소식, 해외사업 소식 --%>
								<c:when test="${result.bbsId eq 'BBSMSTR_000000000023' or result.bbsId eq 'BBSMSTR_000000000024' or result.bbsId eq 'BBSMSTR_000000000025'}">
									<c:choose>
										<c:when test="${empty result.tmp01}">
											<a href="<c:out value="${viewUrl}"/>"><strong class="tit"><c:out value="${result.nttSj}" /></strong></a>
											<span>작성자 [ <c:out value="${result.ntcrNm}" /> ] / 작성일 [ <fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd" /> ]</span>
										</c:when>
										<c:otherwise>
											<a href="<c:out value="${result.tmp01}"/>" target="_blank"><strong class="tit"><c:out value="${result.nttSj}" /></strong></a>
											<span>작성자 [ <c:out value="${result.ntcrNm}" /> ] / 작성일 [ <fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd" /> ]</span>
										</c:otherwise>
									</c:choose>
									<br />
								</c:when>
								<%-- 그외 게시판 --%>
								<c:otherwise>
									<a href="<c:out value="${viewUrl}"/>"><strong class="tit"><c:out value="${result.nttSj}" /></strong></a>
									<span>작성자 [ <c:out value="${result.ntcrNm}" /> ] / 작성일 [ <fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd" /> ]</span>
									<p><a href="<c:out value="${viewUrl}"/>"><c:out value="${fn:replace(fn:substring(result.nttCn, 0, 220), searchVO.searchWrd, repleTxt)}" escapeXml="false"/></a></p>
								</c:otherwise>
							</c:choose>
						</li>
					</c:if>
				</c:forEach>

				<c:if test="${bbsCnt eq 0}">
					<li><strong class="sword">검색어</strong> "<strong class="orange"><c:out value="${searchVO.searchWrd}" /></strong>"에 대한 검색결과가 없습니다.</li>
				</c:if>
			</ul>
		</c:if>
		<c:if test="${bbsCnt > 0}">
			<div class="smore">
				<form id="bbsSearchForm" name="bbsSearchForm" action="/sch/bbsSearch.do" method="post">
					<input type="hidden" name="searchCnd" value="bbs"/>
					<input type="hidden" name="searchWrd" value="${searchVO.searchWrd}"/>
				</form>

				<c:url var="moreUrl" value="/sch/bbsSearch.do">
					<c:param name="searchCnd" value="bbs" />
					<c:param name="searchWrd" value="${searchVO.searchWrd}" />
				</c:url>
				<a href="/sch/bbsSearch.do" onclick="$('#bbsSearchForm').submit(); return false;">더 많은 검색결과 보기</a>
			</div>
		</c:if>
	</div>
</div>
<c:import url="/msi/sch/tmplatBottom.do" charEncoding="utf-8" />