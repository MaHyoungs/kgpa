<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	pageContext.setAttribute("newLineChar", "\n");
%>

<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil
		.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}" />
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}" />
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images" />
<c:set var="_C_CSS" value="/template/common/css" />
<c:set var="_C_JS" value="${_WEB_FULL_PATH}/template/common/js" />
<c:set var="_C_IMG" value="/template/common/images" />

<c:set var="_PREFIX" value="/cop/bbs" />

<%
	/*URL 정의*/
%>
<c:url var="_BASE_PARAM" value="">
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="bbsId" value="${board.bbsId}" />
	<c:param name="menuId" value="${searchVO.menuId}" />
	<c:if test="${not empty searchVO.searchCate}">
		<c:param name="searchCate" value="${searchVO.searchCate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchCnd}">
		<c:param name="searchCnd" value="${searchVO.searchCnd}" />
	</c:if>
	<c:if test="${not empty searchVO.searchWrd}">
		<c:param name="searchWrd" value="${searchVO.searchWrd}" />
	</c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}">
		<c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}" />
	</c:if>
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<%
	/*URL 정의*/
%>

<c:if test="${not empty USER_INFO}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
<c:if test="${empty USER_INFO}">
	<c:set var="USER_INFO" value="${UserPrivateCert}" />
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<script type="text/javascript">
	$(document).ready(function() {
		$('#btnBbsDelete').click(function() {
			if (confirm('<spring:message code="common.delete.msg" />')) {
				location.href = this.href;
			} else {
				return false;
			}
		});
	});
</script>

<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and brdMstrVO.bbsId eq 'BBSMSTR_000000000008'}">
	<div class="pbg2 mB20">
		온라인민원(클린센터) 게시판입니다.이 게시판은 민원사무처리에 관한 법률시행령 제2조 1항 3호에 의거, 민원인 성명, 주민등록번호, 전화번호등이 분명하지 아니한 경우는 답변 없이 삭제됨을 알려드립니다. ’10.4.26(날짜확인) 부터 민원인과 피민원인의 비밀보장을 위하여 불편마당 게시판이 "비공개" 처리됨을 알려드립니다 <span class="bg"></span>
	</div>
</c:if>

<c:choose>
	<c:when test="${IS_MOBILE }">
		<div id="bbs_mbl">
	</c:when>
	<c:otherwise>
		<div id="bbs_wrap">
	</c:otherwise>
</c:choose>

<div class="board_view">
	<%-- 일반 게시판 시작--%>
	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA03' or brdMstrVO.bbsAttrbCode eq 'BBSA06'}">
		<dl class="tit_view">
			<dt>
				<spring:message code="cop.nttSj" />
			</dt>
			<dd>
				<c:out value="${board.nttSj}" />
			</dd>
		</dl>
		<c:if test="${!empty brdMstrVO.ctgrymasterId}">
			<dl class="info_view">
				<dt>
					<spring:message code="cop.category.view" />
				</dt>
				<dd>
					<c:out value="${board.ctgryNm}" />
				</dd>
			</dl>
		</c:if>

		<dl class="info_view2">
			<dt class="writer">
				<spring:message code="cop.ntcrNm" />
			</dt>
			<dd class="writer">
				<c:out value="${board.ntcrNm}" />
			</dd>
			<dt>
				<spring:message code="cop.frstRegisterPnttm" />
			</dt>
			<dd>
				<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
			</dd>
			<dt>
				<spring:message code="cop.inqireCo" />
			</dt>
			<dd>
				<c:out value="${board.inqireCo}" />
			</dd>
		</dl>
		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="file_view">
				<dt>
					<spring:message code="cop.atchFileList" />
				</dt>
				<dd>
					<c:if test="${not empty board.atchFileId}">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${board.atchFileId}" />
							<c:param name="imagePath" value="${_IMG }" />
						</c:import>
					</c:if>
				</dd>
			</dl>
		</c:if>
		<div class="view_cont">
			<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
		</div>
	</c:if>
	<%-- 일반 게시판 끝--%>



	<%-- 유튜브 동영상 게시판 시작--%>
	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04'}">
		<dl class="tit_view">
			<dt>
				<spring:message code="cop.nttSj" />
			</dt>
			<dd>
				<c:out value="${board.nttSj}" />
			</dd>
		</dl>
		<c:if test="${!empty brdMstrVO.ctgrymasterId}">
			<dl class="info_view">
				<dt>
					<spring:message code="cop.category.view" />
				</dt>
				<dd>
					<c:out value="${board.ctgryNm}" />
				</dd>
			</dl>
		</c:if>

		<dl class="info_view2">
			<dt class="writer">
				<spring:message code="cop.ntcrNm" />
			</dt>
			<dd class="writer">
				<c:out value="${board.ntcrNm}" />
			</dd>
			<dt>
				<spring:message code="cop.frstRegisterPnttm" />
			</dt>
			<dd>
				<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
			</dd>
			<dt>
				<spring:message code="cop.inqireCo" />
			</dt>
			<dd>
				<c:out value="${board.inqireCo}" />
			</dd>
		</dl>
		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="file_view">
				<dt>
					<spring:message code="cop.atchFileList" />
				</dt>
				<dd>
					<c:if test="${not empty board.atchFileId}">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${board.atchFileId}" />
							<c:param name="imagePath" value="${_IMG }" />
						</c:import>
					</c:if>
				</dd>
			</dl>
		</c:if>
		<div class="view_cont">
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04' and not empty board.tmp01}">
				<div style="text-align: center; padding: 20px 0 40px 0;">${board.tmp01}</div>
			</c:if>
			<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
		</div>
	</c:if>
	<%-- 유튜브 동영상 게시판 끝--%>



	<%-- 민원형 게시판 시작--%>
	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">

		<div class="top_info">
			<span>민원인 : <c:out value="${board.ntcrNm}" /></span> <span>등록일 : <fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd" /></span> <span>상태 : <strong class="red"><c:out value="${board.processSttusNm}" /></strong></span>
		</div>
		<dl class="tit_view">
			<dt>
				<spring:message code="cop.nttSj" />
			</dt>
			<dd>
				<c:out value="${board.nttSj}" />
			</dd>
		</dl>

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="file_view">
				<dt>
					<spring:message code="cop.atchFileList" />
				</dt>
				<dd>
					<c:if test="${not empty board.atchFileId}">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${board.atchFileId}" />
							<c:param name="imagePath" value="${_IMG }" />
						</c:import>
					</c:if>
				</dd>
			</dl>
		</c:if>
		<div class="view_cont">
			<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
		</div>

		<%-- 민원형 게시판 답변 시작--%>
		<c:if test="${board.processSttusCode eq 'QA03'}">
			<c:if test="${not empty board.estnData}">
				<div class="view_cont_rep">
					<strong>답변</strong>
					<div class="cont">
						<c:out value="${fn:replace(board.estnParseData.cn, newLineChar, '<br/>')}" escapeXml="false" />
					</div>
				</div>

				<dl class="info_view2">
					<dt class="writer">답변자</dt>
					<dd class="writer">${board.lastAnswrrName}</dd>
					<dt>등록일</dt>
					<dd>
						<fmt:formatDate value="${board.lastAnswrrPnttm}" pattern="yyyy.MM.dd" />
					</dd>
					<dt>전화번호</dt>
					<dd>${board.lastAnswrrPhone}</dd>
				</dl>
			</c:if>
			<c:if test="${not empty board.estnAtchFileId}">
				<dl class="file_view">
					<dt>
						<spring:message code="cop.atchFileList" />
					</dt>
					<dd>
						<c:if test="${not empty board.estnAtchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.estnAtchFileId}" />
								<c:param name="imagePath" value="${_IMG }" />
							</c:import>
						</c:if>
					</dd>
				</dl>
			</c:if>
		</c:if>
		<%-- 민원형 게시판 답변 끝--%>
	</c:if>
	<%-- 민원형 게시판 끝--%>


	<%-- 사전공표 게시판 시작--%>
	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA05'}">
		<dl class="tit_view">
			<dt>구분</dt>
			<dd>
				<c:out value="${board.ctgryNm}" />
			</dd>
		</dl>
		<dl class="info_view">
			<dt>공표목록</dt>
			<dd>
				<c:out value="${board.nttSj}" />
			</dd>
		</dl>
		<dl class="info_view">
			<dt>공표항목</dt>
			<dd>
				<c:out value="${board.tmp02}" />
			</dd>
		</dl>
		<dl class="info_view2">
			<dt class="writer">공표시기</dt>
			<dd class="writer">
				<c:out value="${board.tmp03}" />
			</dd>
			<dt class="writer">공표주기</dt>
			<dd class="writer">
				<c:out value="${board.tmp04}" />
			</dd>
		</dl>
		<dl class="info_view2">
			<dt class="writer">부서명</dt>
			<dd class="writer">
				<c:out value="${board.tmp05}" />
			</dd>
			<dt class="writer">공표방법</dt>
			<dd class="writer">
				<c:out value="${board.tmp06}" />
			</dd>
		</dl>
		<dl class="info_view2">
			<dt class="writer">작성일</dt>
			<dd class="writer">
				<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
			</dd>
			<dt class="writer">조회수</dt>
			<dd class="writer">
				<c:out value="${board.inqireCo}" />
			</dd>
		</dl>
		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="file_view">
				<dt>
					<spring:message code="cop.atchFileList" />
				</dt>
				<dd>
					<c:if test="${not empty board.atchFileId}">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${board.atchFileId}" />
							<c:param name="imagePath" value="${_IMG }" />
						</c:import>
					</c:if>
				</dd>
			</dl>
		</c:if>
		<div class="view_cont">
			<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
		</div>
	</c:if>
	<%-- 일반 사전공표 끝--%>

	<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
		<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
			<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
			<c:param name="sourcId" value="${brdMstrVO.sourcId}" />
		</c:import>
	</c:if>
</div>

<div class="btn_all">
	<div class="fL">
		<c:if test="${not empty USER_INFO.id or not empty USER_INFO.credtId or not empty USER_INFO.oldSecId}">
			<c:if test="${brdMstrVO.replyPosblAt eq 'Y' and SE_CODE >= brdMstrVO.answerAuthor}">
				<c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
					<c:param name="nttNo" value="${board.nttNo}" />
					<c:param name="registAction" value="reply" />
				</c:url>
				<span class="bbtn"><a href="<c:out value="${addReplyBoardArticleUrl}"/>"><spring:message code="button.reply" /></a></span>
			</c:if>
			<c:if test="${board.frstRegisterId eq USER_INFO.id or board.credtId eq USER_INFO.credtId or board.oldSecId eq USER_INFO.oldSecId or SE_CODE >= 10}">
				<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
					<c:param name="nttNo" value="${board.nttNo}" />
					<c:param name="registAction" value="updt" />
				</c:url>
				<span class="bbtn"><a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><spring:message code="button.update" /></a></span>
				<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
					<c:param name="nttNo" value="${board.nttNo}" />
				</c:url>
				<span class="bbtn"><a id="btnBbsDelete" href="<c:out value="${deleteBoardArticleUrl}"/>"><spring:message code="button.delete" /></a></span>
			</c:if>
		</c:if>
	</div>
	<div class="fR">
		<span class="bbtn"><a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}&ctgryId=${board.ctgryId}"/>"><spring:message code="button.list" /></a></span>
	</div>
</div>