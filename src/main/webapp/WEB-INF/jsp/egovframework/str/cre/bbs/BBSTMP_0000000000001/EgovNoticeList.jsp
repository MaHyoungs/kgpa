<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.com.cmm.service.EgovProperties" %>
<%@ page import="egovframework.com.cmm.service.Globals" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}"/>
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}"/>
			</c:if>
		</c:forEach>
	</c:if>
	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}"/></c:if>
	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}"/></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}"/></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
</c:url>

<c:set var="SE_CODE" value="01"/>
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}"/>
</c:if>

<%-- 웹접근성 관련 타이틀 변경 이벤트 --%>
<script type="text/javascript">
	$(function () {
		var tit = $('title').text();
		tit = tit.replace('${currMpm.menuNm}', '${currMpm.menuNm}(글목록)');
		$('title').html(tit);
	});
</script>
<%-- 웹접근성 관련 타이틀 변경 이벤트 --%>
<c:if test="${not empty param.ctnTabView}">
	<link charset="utf-8" href="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/style.css" type="text/css" rel="stylesheet"/>
	<script type="text/javascript" src="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/script.js"></script>
</c:if>

<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${_C_JS}/board.js"></script>
<script type="text/javascript">

	<c:if test="${!empty brdMstrVO.ctgrymasterId}">
	var boardCateLevel = ${boardCateLevel};
	var boardCateList = [];
	<c:forEach var="cate" items="${boardCateList}" varStatus="status">
	boardCateList[${status.index}] = new ctgryObj('${cate.upperCtgryId}', '${cate.ctgryId}', '${cate.ctgryNm}', ${cate.ctgryLevel});
	</c:forEach>
	</c:if>

	function fn_egov_addNotice(url) {
		location.href = url;
	}

	$(document).ready(function () {
		$('#btnBbsWrite').click(function () {
			fn_egov_addNotice(this.href);
			return false;
		});
		fnCtgryInit('${searchVO.searchCateList}');
	});

	<c:if test="${(SE_CODE >= 10) and (not IS_MOBILE) }">
	//관리자 함수 시작
	$(document).ready(function () {
		$("#checkAll").click(function () {
			$("input:checkbox[name=nttNoArr]").attr("checked", $(this).is(":checked"));
		});

		$('#btnManageMove').click(function () {
			if (checkArticle()) {
				$('#registAction').val('Move');
				bbsSelectPop();
			}
			return false;
		});
		$('#btnManageCopy').click(function () {
			if (checkArticle()) {
				$('#registAction').val('Copy');
				bbsSelectPop();
			}
			return false;
		});
		$('#btnManageHide').click(function () {
			if (checkArticle()) {
				if (confirm('<spring:message code="button.delete"/> 하시겠습니까?')) {
					$('#registAction').val('Hide');
				} else {
					return false;
				}
			} else {
				return false;
			}
		});
		$('#btnManageRemove').click(function () {
			if (checkArticle()) {
				if (confirm('<spring:message code="button.deleteDatabase"/> 후에는 <spring:message code="button.repair"/>할 수 없습니다. <spring:message code="button.deleteDatabase"/> 하시겠습니까?')) {
					$('#registAction').val('Remove');
				} else {
					return false;
				}
			} else {
				return false;
			}
		});
		$('#btnManageRepair').click(function () {
			if (checkArticle()) {
				if (confirm('<spring:message code="button.repair"/> 하시겠습니까?')) {
					$('#registAction').val('Repair');
				} else {
					return false;
				}
			} else {
				return false;
			}
		});

		$("#listForm").ajaxForm({
			url: '/cop/bbs/manageArticle.do',
			dataType: 'json',
			beforeSubmit: function ($form, options) {
				if (checkArticle()) {
					cfgCommonPopShow();
				} else {
					return false;
				}
			},
			success: function (data) {
				cfgCommonPopHide();
				alert(data.message);
				document.location.href = $('#returnUrl').val();
			},
			error: function () {
				alert('문제가 발생하여 요청처리를 완료하지 못하였습니다.');
				cfgCommonPopHide();
			},
			resetForm: true
		});
	});

	function checkArticle() {
		if ($("input:checkbox[name=nttNoArr]:checked").length == 0) {
			alert('게시글을 선택해주세요');
			return false;
		}
		return true;
	}

	function bbsSelectPop() {
		var url = "/cop/com/selectAllBBSMasterManageInfs.do?siteId=${brdMstrVO.siteId}&bbsId=${brdMstrVO.bbsId}";
		var win = window.open(url, 'bbsSelectPop', ' scrollbars=yes, resizable=yes, left=0, top=0, width=700,height=650');
		if (win != null) {
			win.focus();
		}
	}

	function selectBbsMaster(bbsId, ctgryId) {
		$('#trgetId').val(bbsId);
		$('#ctgryId').val(ctgryId);

		$('#listForm').submit();
	}

	function cancleBbsMaster() {
		cfgCommonPopHide();
	}

	function cfgCommonPopShow() {
		$('#wrap').append("<div id='layer_blind_box' style='position:absolute; position:fixed; top:0; left:0; width:100%; height:100%; background:#000; z-index:50;'></div>");
		$('#layer_blind_box').css('opacity', 0.3);
	}

	function cfgCommonPopHide() {
		$('#layer_blind_box').remove();
	}
	//관리자 함수끝
	</c:if>
</script>

	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA03' or brdMstrVO.bbsAttrbCode eq 'BBSA06'}">
		<c:if test="${currMpm.htmlUseAt eq 'Y'}">
			<c:catch var="ex">
				<c:import url="/EgovPageLink.do?link=${MnuFileStoreWebPathByJspFile}${currMpm.siteId}/${currMpm.menuId}${PUBLISH_APPEND_FREFIX}" charEncoding="utf-8"/>
			</c:catch>
			<c:if test="${ex != null}">
				publishing not found
			</c:if>
		</c:if>
	</c:if>

<div id="<c:choose><c:when test="${IS_MOBILE }">bbs_mbl</c:when><c:otherwise>bbs_wrap</c:otherwise></c:choose>">
	<form id="listForm" name="listForm" method="post" action="">
		<input type="hidden" id="menuId" name="${currMpm.menuId}"/>
		<input type="hidden" id="registAction" name="registAction"/>
		<input type="hidden" id="bbsId" name="bbsId" value="<c:out value="${brdMstrVO.bbsId}"/>"/>
		<input type="hidden" id="trgetId" name="trgetId"/>
		<input type="hidden" id="ctgryId" name="ctgryId"/>
		<c:url var="_LIST_HIDDEN_URL" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
		</c:url>
		<input type="hidden" id="returnUrl" name="returnUrl" value="<c:out value="${_LIST_HIDDEN_URL}"/>"/>

		<%-- 사전공표 게시판 및 카테고리 리스트 존재시 탭메뉴 생성 --%>
		<c:if test="${not empty boardCateList and brdMstrVO.bbsAttrbCode eq 'BBSA05'}">
			<div class="tab_box">
				<ul class="tabs">
					<c:forEach var="ctgryRs" items="${boardCateList}" varStatus="sts">
						<c:if test="${ctgryRs.ctgryLevel ne 0}">
							<li class="<c:if test="${ctgryRs.ctgryId eq param.ctgryId}"> active</c:if>">
								<a href="<c:out value="${_LIST_HIDDEN_URL}" escapeXml="true" />&amp;ctgryId=${ctgryRs.ctgryId}">${ctgryRs.ctgryNm}</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</c:if>


		<div class="total">
			Total <strong>${paginationInfo.totalRecordCount}</strong>, Page
			<strong>${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}
		</div>

		<c:choose>
			<%-- 민원형 게시판 시작 --%>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
				<div id="${brdMstrVO.tmplatId }" class="bss_list">
						<%-- 주의 : 컬럼이 추가시 summary에 작성을 해야한다. --%>
					<table summary="${brdMstrVO.bbsNm } 목록을 나타낸표로 번호, 등록일, 제목, 민원인, 진행상태, 처리일자 항목을 제공하고있습니다" class="list_table">
						<caption>${brdMstrVO.bbsNm }목록</caption>
						<thead>
						<tr>
							<th class="num" scope="col">번호</th>
							<th class="date" scope="col">등록일</th>
							<th class="tit" scope="col">제목</th>
							<th class="writer" scope="col">민원인</th>
							<th class="writer" scope="col">진행상태</th>
							<th class="date" scope="col">처리일자</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
								<c:param name="nttNo" value="${result.nttNo}"/>
								<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
							</c:url>
							<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
								<td class="num">
									<c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}"/>
								</td>
								<td class="date">
									<fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/>
								</td>
								<td class="tit">
									<c:if test="${result.ordrCodeDp gt 0}">
										<img src="${_C_IMG}/sub/board/blank_bg.gif" width="${result.ordrCodeDp * 19}" height="0" alt=""/>
										<img src="${_IMG}/ico_reply.gif" alt="<spring:message code="cop.replyNtt"/>"/>
									</c:if>
									<c:choose>
										<c:when test="${SE_CODE eq '10'}">
											<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
										</c:when>
										<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
											<c:out value="${result.nttSj}"/>
										</c:when>
										<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
											<c:out value="${result.nttSj}"/>
										</c:when>
										<c:otherwise>
											<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.nttSj}"/></a>
										</c:otherwise>
									</c:choose>
									<c:if test="${result.othbcAt eq 'N'}">
										<img src="${_IMG}/ico_board_lock.gif" alt="<spring:message code="cop.privateNtt"/>"/>
									</c:if>
									<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
										<c:choose>
											<c:when test="${IS_MOBILE }">
												<em class="boardrenum"><c:out value="${result.commentCount}"/></em>
											</c:when>
											<c:when test="${result.commentCount eq 0}">
												<em class="boardrenumno">[<c:out value="${result.commentCount}"/>]
												</em>
											</c:when>
											<c:otherwise>
												<em class="boardrenum">[<c:out value="${result.commentCount}"/>]
												</em>
											</c:otherwise>
										</c:choose>
									</c:if>
								</td>
								<td class="writer">
									<c:out value="${result.ntcrNm}"/>
								</td>
								<td class="state">
									<c:choose>
										<c:when test="${result.processSttusCode eq 'QA01'}">
											<span class="green"><c:out value="${result.processSttusNm}"/></span>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA02'}">
											<span class="green"><c:out value="${result.processSttusNm}"/></span>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA03'}">
											<span class="green"><c:out value="${result.processSttusNm}"/></span>
										</c:when>
										<c:otherwise>
											<span class="green"><c:out value="${result.processSttusNm}"/></span>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="date">
									<fmt:formatDate value="${result.lastAnswrrPnttm}" pattern="yyyy-MM-dd"/>
								</td>
							</tr>
						</c:forEach>

						<c:if test="${fn:length(resultList) == 0}">
							<tr class="empty">
								<td colspan="6">
									<spring:message code="common.nodata.msg"/>
								</td>
							</tr>
						</c:if>
						</tbody>
					</table>
				</div>
			</c:when>
			<%-- 민원형 게시판 끝 --%>


			<%-- 갤러리형 게시판 & 유튜브 동영상 게시판 시작 --%>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA02' or brdMstrVO.bbsAttrbCode eq 'BBSA04'}">
				<div class="video_list">
					<ul>
						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
								<c:param name="nttNo" value="${result.nttNo}"/>
								<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
							</c:url>
							<c:set var="isViewEnable" value=""/>
							<c:choose>
								<c:when test="${SE_CODE eq '10'}">
									<c:set var="isViewEnable" value="Y"/>
								</c:when>
								<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
									<c:set var="isViewEnable" value="N"/>
								</c:when>
								<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
									<c:set var="isViewEnable" value="N"/>
								</c:when>
								<c:otherwise>
									<c:set var="isViewEnable" value="Y"/>
								</c:otherwise>
							</c:choose>
							<li <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
								<a href="<c:out value="${viewUrl}" escapeXml="true"/>"> <c:choose>
									<c:when test="${empty result.atchFileNm}">
											<span class="img"> <c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA02'}">
												<img src="${_IMG}/sub/board/photo_img_blank.gif" width="210" height="130" alt="${result.nttSj}"/>
											</c:if> <c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04'}">
												<img src="${_IMG}/noimg.gif" alt="${result.nttSj}"/>
												<span class="png"></span>
											</c:if>
											</span>
									</c:when>
									<c:otherwise>
										<span class="img">
											<img src='<c:url value='/cmm/fms/getImage.do'/>?thumbYn=Y&amp;siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>' alt="${result.nttSj}"/>
											<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04'}"><span class="png"></span></c:if>
										</span>
									</c:otherwise>
								</c:choose> <strong> <c:choose>
									<c:when test="${fn:length(result.nttSj) > 15}">
										<c:out value='${fn:substring(result.nttSj, 0, 15)}'/>...
									</c:when>
									<c:otherwise>
										<c:out value="${result.nttSj}"/>
									</c:otherwise>
								</c:choose>
								</strong>
									<span class="date"><fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/></span>
								</a>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${fn:length(resultList) == 0}">
						<div style="text-align: center; padding: 10px 0 30px 0;">
							<spring:message code="common.nodata.msg"/>
						</div>
					</c:if>
				</div>
			</c:when>
			<%-- 갤러리형 게시판 & 유튜브 동영상 게시판 끝 --%>


			<%-- 일반게시판 시작 --%>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA03' or brdMstrVO.bbsAttrbCode eq 'BBSA06'}">
				<div id="${brdMstrVO.tmplatId }" class="bss_list">
						<%-- 주의 : 컬럼이 추가시 summary에 작성을 해야한다. --%>
					<table summary="${brdMstrVO.bbsNm } 목록을 나타낸표로 <spring:message code="cop.nttNo"/>,<c:if test="${not empty brdMstrVO.ctgrymasterId}"><spring:message code="cop.category.view" />,</c:if><spring:message code="cop.nttSj" /><c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><spring:message code="cop.processSttus" />,</c:if><spring:message code="cop.ntcrNm" />,<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}"><spring:message code="cop.listAtchFile" />,</c:if><spring:message code="cop.frstRegisterPnttm" />, <spring:message code="cop.inqireCo" /> 항목을 제공하고 있습니다" class="list_table">
						<caption>${brdMstrVO.bbsNm }목록</caption>
						<thead>
						<tr>
							<c:if test="${SE_CODE >= 10 }">
								<th class="check" scope="col">
									<input type="checkbox" id="checkAll" value="all"/>
									<label for="checkAll"><spring:message code="cop.select"/></label>
								</th>
							</c:if>
							<th class="num" scope="col">
								<spring:message code="cop.nttNo"/>
							</th>
							<c:if test="${not empty brdMstrVO.ctgrymasterId}">
								<th class="class" scope="col">
									<spring:message code="cop.category.view"/>
								</th>
							</c:if>
							<th class="tit" scope="col">
								<spring:message code="cop.nttSj"/>
							</th>
							<th class="writer" scope="col">
								<spring:message code="cop.ntcrNm"/>
							</th>
							<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
								<th class="file" scope="col">
									<spring:message code="cop.listAtchFile"/>
								</th>
							</c:if>
							<th class="date" scope="col">
								<spring:message code="cop.frstRegisterPnttm"/>
							</th>
							<th class="hits" scope="col">
								<spring:message code="cop.inqireCo"/>
							</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach var="result" items="${noticeList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
								<c:param name="nttNo" value="${result.nttNo}"/>
								<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
							</c:url>
							<tr class="notice">
								<c:if test="${SE_CODE >= 10 }">
									<td class="check"></td>
								</c:if>
								<td class="num">
									<img src="${_IMG}/icon_notice.gif" alt="<spring:message code="cop.notice"/>"/>
								</td>
								<c:if test="${not empty brdMstrVO.ctgrymasterId}">
									<td class="class">
										<c:out value="${result.ctgryNm}"/>
									</td>
								</c:if>
								<td class="tit">
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
									<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
										<c:choose>
											<c:when test="${IS_MOBILE }">
												<em class="boardrenum"><c:out value="${result.commentCount}"/></em>
											</c:when>
											<c:when test="${result.commentCount eq 0}">
												<em class="boardrenumno">[<c:out value="${result.commentCount}"/>]
												</em>
											</c:when>
											<c:otherwise>
												<em class="boardrenum">[<c:out value="${result.commentCount}"/>]
												</em>
											</c:otherwise>
										</c:choose>
									</c:if>
								</td>
								<td class="writer">
									<c:out value="${result.ntcrNm}"/>
								</td>
								<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
									<td class="file">
										<c:choose>
											<c:when test="${not empty result.atchFileId}">
												<img src="${_IMG}/ico_file.gif" alt="<spring:message code="cop.listAtchFile"/>"/>
											</c:when>
											<c:otherwise>
												<c:if test="${not IS_MOBILE }">-</c:if>
											</c:otherwise>
										</c:choose>
									</td>
								</c:if>
								<td class="date">
									<fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/>
								</td>
								<td class="hits">
									<c:out value="${result.inqireCo}"/>
								</td>
							</tr>
						</c:forEach>

						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
								<c:param name="nttNo" value="${result.nttNo}"/>
								<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
							</c:url>
							<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
								<c:if test="${SE_CODE >= 10 and (not IS_MOBILE)}">
									<td class="check">
										<input type="checkbox" name="nttNoArr" value="${result.nttNo}" title="<spring:message code="cop.select"/>"/>
									</td>
								</c:if>
								<td class="num">
									<c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}"/>
								</td>
								<c:if test="${not empty brdMstrVO.ctgrymasterId}">
									<td class="class">
										<c:out value="${result.ctgryNm}"/>
									</td>
								</c:if>
								<td class="tit">
									<c:if test="${result.ordrCodeDp gt 0}">
										<img src="${_C_IMG}/sub/board/blank_bg.gif" width="${result.ordrCodeDp * 19}" height="0" alt=""/>
										<img src="${_IMG}/ico_reply.gif" alt="<spring:message code="cop.replyNtt"/>"/>
									</c:if>
									<c:choose>
										<c:when test="${SE_CODE eq '10'}">
											<c:choose>
												<%-- 뉴스 게시판 : 사업단 소식, 국내사업 소식, 해외사업 소식 --%>
												<c:when test="${result.bbsId eq 'BBSMSTR_000000000023' or result.bbsId eq 'BBSMSTR_000000000024' or result.bbsId eq 'BBSMSTR_000000000025'}">
													<c:choose>
														<c:when test="${empty result.tmp01}">
															<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
														</c:when>
														<c:otherwise>
															<a href="<c:out value="${result.tmp01}"/>" target="_blank"><c:out value="${result.nttSj}"/></a>
														</c:otherwise>
													</c:choose>
												</c:when>
												<%-- 그외 게시판 --%>
												<c:otherwise>
													<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
											<c:out value="${result.nttSj}"/>
										</c:when>
										<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
											<c:out value="${result.nttSj}"/>
										</c:when>
										<c:otherwise>
											<c:choose>
												<%-- 뉴스 게시판 : 사업단 소식, 국내사업 소식, 해외사업 소식 --%>
												<c:when test="${result.bbsId eq 'BBSMSTR_000000000023' or result.bbsId eq 'BBSMSTR_000000000024' or result.bbsId eq 'BBSMSTR_000000000025'}">
													<c:choose>
														<c:when test="${empty result.tmp01}">
															<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.nttSj}"/></a>
														</c:when>
														<c:otherwise>
															<a href="<c:out value="${result.tmp01}"/>" target="_blank"><c:out value="${result.nttSj}"/></a>
														</c:otherwise>
													</c:choose>
												</c:when>
												<%-- 그외 게시판 --%>
												<c:otherwise>
													<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.nttSj}"/></a>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									<c:if test="${result.othbcAt eq 'N'}">
										<img src="${_IMG}/ico_board_lock.gif" alt="<spring:message code="cop.privateNtt"/>"/>
									</c:if>
									<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
										<c:choose>
											<c:when test="${IS_MOBILE }">
												<em class="boardrenum"><c:out value="${result.commentCount}"/></em>
											</c:when>
											<c:when test="${result.commentCount eq 0}">
												<em class="boardrenumno">[<c:out value="${result.commentCount}"/>]</em>
											</c:when>
											<c:otherwise>
												<em class="boardrenum">[<c:out value="${result.commentCount}"/>]</em>
											</c:otherwise>
										</c:choose>
									</c:if>
								</td>
								<td class="writer">
									<c:out value="${result.ntcrNm}"/>
								</td>
								<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
									<td class="file">
										<c:choose>
											<c:when test="${not empty result.atchFileId}">
												<img src="${_IMG}/ico_file.gif" alt="<spring:message code="cop.listAtchFile"/>"/>
											</c:when>
											<c:otherwise>
												<c:if test="${not IS_MOBILE }">-</c:if>
											</c:otherwise>
										</c:choose>
									</td>
								</c:if>
								<td class="date">
									<fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/>
								</td>
								<td class="hits">
									<c:out value="${result.inqireCo}"/>
								</td>
							</tr>
						</c:forEach>

						<c:if test="${fn:length(resultList) == 0}">
							<tr class="empty">
								<td colspan="10">
									<spring:message code="common.nodata.msg"/>
								</td>
							</tr>
						</c:if>
						</tbody>
					</table>
				</div>
			</c:when>
			<%-- 일반게시판 끝 --%>


			<%-- 사전공표 게시판 시작 --%>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA05'}">
				<div id="${brdMstrVO.tmplatId }" class="bss_list">
						<%-- 주의 : 컬럼이 추가시 summary에 작성을 해야한다. --%>
					<table summary="${currMpm.menuNm } <c:forEach var="ctgryRs" items="${boardCateList}" varStatus="sts"><c:if test="${ctgryRs.ctgryId eq param.ctgryId}">${ctgryRs.ctgryNm}</c:if></c:forEach>를 나타낸표로 공표목록, 공표항목, 공표시기, 공표방법, 담당부서/연락처 항목을 제공하고 있습니다" class="list_table">
						<caption>${currMpm.menuNm }</caption>
						<colgroup>
							<col width="20%"/>
							<col width=""/>
							<col width="10%"/>
							<col width="10%"/>
							<col width="15%"/>
						</colgroup>
						<thead>
						<tr>
							<th scope="col">공표목록</th>
							<th scope="col">공표항목</th>
							<th scope="col">공표시기</th>
							<th scope="col">공표방법</th>
							<th scope="col">담당부서/연락처</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
								<c:param name="nttNo" value="${result.nttNo}"/>
								<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
							</c:url>
							<tr>
								<td>
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.nttSj}"/></a>
								</td>
								<td class="alL">
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.tmp02}"/></a>
								</td>
								<td>
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.tmp03}"/></a>
								</td>
								<td>
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.tmp06}"/></a>
								</td>
								<td>
									<a href="<c:out value="${viewUrl}" escapeXml="true"/>"><c:out value="${result.tmp05}"/></a>
								</td>
							</tr>
						</c:forEach>

						<c:if test="${fn:length(resultList) == 0}">
							<tr class="empty">
								<td colspan="5">
									<spring:message code="common.nodata.msg"/>
								</td>
							</tr>
						</c:if>
						</tbody>
					</table>
				</div>
			</c:when>
			<%-- 사전공표 게시판 끝 --%>
		</c:choose>
	</form>

	<c:if test="${brdMstrVO.registAuthor eq '02' or SE_CODE >= brdMstrVO.registAuthor or SE_CODE >= 10}">
		<div class="btn_all">
			<c:if test="${(SE_CODE >= 10) and (not IS_MOBILE) }">
				<div class="fL">
						<%--<span class="bbtn"><button type="button" id="btnManageMove">
								<spring:message code="button.move" />
							</button></span> <span class="bbtn"><button type="button" id="btnManageCopy">
								<spring:message code="button.copy" />
							</button></span> --%><span class="bbtn"><button type="button" id="btnManageHide">
					<spring:message code="button.delete"/>
				</button></span> <span class="bbtn"><button type="button" id="btnManageRemove">
					<spring:message code="button.deleteDatabase"/>
				</button></span> <span class="bbtn"><button type="button" id="btnManageRepair">
					<spring:message code="button.repair"/>
				</button></span>
				</div>
			</c:if>
			<div class="fR">
				<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
					<c:param name="registAction" value="regist"/>
				</c:url>
				<span class="bbtn_confirm2"><a href="<c:out value="${addBoardArticleUrl}" escapeXml="true"/>" id="btnBbsWrite"><spring:message code="button.write"/></a></span>
			</div>
		</div>
	</c:if>

	<div id="paging">
		<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
		<c:set var="pagingParam">
			<c:out value="${pageUrl}" escapeXml="true"/>
		</c:set>
		<ui:pagination paginationInfo="${paginationInfo}" type="egovPaging" jsFunction="${pagingParam}"/>
	</div>

	<div id="bbs_search">
		<form name="frm" method="post" action="">
			<fieldset>
				<legend> 검색조건입력폼</legend>
				<c:if test="${not empty brdMstrVO.ctgrymasterId}">
					<c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
						<c:choose>
							<c:when test="${status.first}">
								<label for="ctgry${ctgryLevel}" class="hdn"><spring:message code="cop.category.view"/></label>
								<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})">
									<option value="">
										<spring:message code="cop.select"/></option>
									<c:forEach var="cate" items="${boardCateList}">
										<c:if test="${cate.ctgryLevel eq 1 }">
											<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
										</c:if>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<label for="ctgry${ctgryLevel}" class="hdn"><spring:message code="cop.category.view"/>${ctgryLevel}</label>
								<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})" class="search_sel">
									<option value="">
										<spring:message code="cop.select"/></option>
								</select>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:if>
				<label for="ftext" class="hdn"><spring:message code="cop.category.view"/></label>
				<select name="searchCnd" id="ftext">
					<option value="0" <c:if test="${searchVO.searchCnd eq '0'}">selected="selected"</c:if>>
						<spring:message code="cop.nttSj"/></option>
					<option value="1" <c:if test="${searchVO.searchCnd eq '1'}">selected="selected"</c:if>>
						<spring:message code="cop.nttCn"/></option>
					<option value="2" <c:if test="${searchVO.searchCnd eq '2'}">selected="selected"</c:if>>
						<spring:message code="cop.ntcrNm"/></option>
				</select> <label for="inp_text" class="hdn">검색어입력</label>
				<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" class="inp_s" id="inp_text"/>
				<span class="bbtn_s"><input type="submit" value="검색"/></span>
			</fieldset>
		</form>
	</div>
</div>
<!-- #bbsWrap end -->