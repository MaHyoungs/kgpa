<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<%
	pageContext.setAttribute("newLineChar", "\n");
%>

<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil
		.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}" />
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}" />
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images" />
<c:set var="_C_CSS" value="/template/common/css" />
<c:set var="_C_JS" value="${_WEB_FULL_PATH}/template/common/js" />
<c:set var="_C_IMG" value="/template/common/images" />

<c:set var="_PREFIX" value="/cop/bbs" />

<%
	/*URL 정의*/
%>
<c:url var="_BASE_PARAM" value="">
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="bbsId" value="${board.bbsId}" />
	<c:if test="${not empty searchVO.searchCate}">
		<c:param name="searchCate" value="${searchVO.searchCate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchCnd}">
		<c:param name="searchCnd" value="${searchVO.searchCnd}" />
	</c:if>
	<c:if test="${not empty searchVO.searchWrd}">
		<c:param name="searchWrd" value="${searchVO.searchWrd}" />
	</c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}">
		<c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}" />
	</c:if>
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<%
	/*URL 정의*/
%>

<c:if test="${not empty USER_INFO}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<c:if test="${not empty UserPrivateCert}">
	<c:set var="USER_INFO" value="${UserPrivateCert}" />
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<link href="/dext5upload/sample/css/sample.common.css" rel="stylesheet" />
<script src="/dext5upload/sample/js/sample.common.js" type="text/javascript"></script>
<script src="/dext5upload/js/dext5upload.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
		/** 웹접근성 관련 타이틀 변경 이벤트 **/
		var tit = $('title').text();
		$('title').html('${brdMstrVO.bbsNm}(글목록) < ' + tit);
		/** 웹접근성 관련 타이틀 변경 이벤트 **/

		$('#btnBbsDelete').click(function() {
			if (confirm('<spring:message code="common.delete.msg" />')) {
				location.href = this.href;
			} else {
				return false;
			}
		});
	});

	var G_UploadID;

	function fn_addFile() {

		var tempPath = location.href;
		tempPath = tempPath.substring(0, tempPath.lastIndexOf('/'));
		tempPath = tempPath.substring(0, tempPath.lastIndexOf('/'));
		tempPath = tempPath.substring(0, tempPath.lastIndexOf('/'));

		<c:forEach var="rs" items="${fileList}" varStatus="sts">
			var str = '${rs.streFileNm}';
			if(str.indexOf('FILE_000') != -1) {
				DEXT5UPLOAD.AddUploadedFile('${sts.count}', '${rs.orignlFileNm}', tempPath + '${rs.fileStreCours}' + '/' + '${rs.streFileNm}', '${rs.fileMg}', '', G_UploadID);
			} else {
				DEXT5UPLOAD.AddUploadedFile('${sts.count}', '${rs.orignlFileNm}', tempPath + '/' + '${rs.fileStreCours}' + '${rs.streFileNm}' + '.' + '${rs.fileExtsn}', '${rs.fileMg}', '', G_UploadID);
			}
		</c:forEach>
	}

	// 생성완료 이벤트
	function DEXT5UPLOAD_OnCreationComplete(uploadID) {
		G_UploadID = uploadID;

		fn_addFile();
	}

</script>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location"> <img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png">
			<c:choose>
				<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000053' or brdMstrVO.bbsId eq 'BBSMSTR_000000000054' or brdMstrVO.bbsId eq 'BBSMSTR_000000000055' or brdMstrVO.bbsId eq 'BBSMSTR_000000000056'}" >
					<strong> 사후관리 > 과거 사후관리 자료실 > ${brdMstrVO.bbsNm}</strong>
				</c:when>
				<c:otherwise>
					<strong>${brdMstrVO.bbsNm}</strong>
				</c:otherwise>
			</c:choose>
		</span>
	</div>
	<h2>${brdMstrVO.bbsNm}</h2>
</div>

<div id="content">
	<div id="<c:choose><c:when test="${IS_MOBILE }">bbs_mbl</c:when><c:otherwise>bbs_wrap</c:otherwise></c:choose>">
		<div class="board_view">


			<%-- 사후관리 게시판 시작 --%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
				<dl class="tit_view">
					<dt>사업명</dt>
					<dd>${bsifVo.biz_nm}</dd>
				</dl>
				<dl class="info_view3">
					<dt>사업기간</dt>
					<dd>${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</dd>
					<dt>총사업비</dt>
					<dd><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></dd>
				</dl>
				<dl class="info_view">
					<dt>사업지</dt>
					<dd>
							${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} ${bsifVo.biz_adres} ${bsifVo.biz_adres_detail}
					</dd>
				</dl>
				<dl class="info_view mB30">
					<dt>전화번호</dt>
					<dd>${bsifVo.tlphon_no}</dd>
				</dl>
			</c:if>
			<%-- 사후관리 게시판 끝 --%>


			<%-- 일반 게시판 시작--%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA03' or brdMstrVO.bbsAttrbCode eq 'BBSA06' or brdMstrVO.bbsAttrbCode eq 'BBSA02' or brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
				<dl class="tit_view">
					<dt>
						<spring:message code="cop.nttSj" />
					</dt>
					<dd>
						<c:out value="${board.nttSj}" />
					</dd>
				</dl>
				<c:if test="${!empty brdMstrVO.ctgrymasterId}">
					<dl class="info_view">
						<dt>
							<spring:message code="cop.category.view" />
						</dt>
						<dd>
							<c:out value="${board.ctgryNm}" />
						</dd>

						<c:if test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000057' or brdMstrVO.bbsId eq 'BBSMSTR_000000000058'}">
							<dt>
								시·도
							</dt>
							<dd>
								<c:if test="${empty board.tmp01}">미지정</c:if>
								<c:if test="${board.tmp01 eq '1000'}">서울특별시</c:if>
								<c:if test="${board.tmp01 eq '1001'}">부산광역시</c:if>
								<c:if test="${board.tmp01 eq '1002'}">대구광역시</c:if>
								<c:if test="${board.tmp01 eq '1003'}">인천광역시</c:if>
								<c:if test="${board.tmp01 eq '1004'}">광주광역시</c:if>
								<c:if test="${board.tmp01 eq '1005'}">대전광역시</c:if>
								<c:if test="${board.tmp01 eq '1006'}">울산광역시</c:if>
								<c:if test="${board.tmp01 eq '1007'}">세종특별자치시</c:if>
								<c:if test="${board.tmp01 eq '1008'}">경기도</c:if>
								<c:if test="${board.tmp01 eq '1009'}">강원도</c:if>
								<c:if test="${board.tmp01 eq '1010'}">충청북도</c:if>
								<c:if test="${board.tmp01 eq '1011'}">충청남도</c:if>
								<c:if test="${board.tmp01 eq '1012'}">전라북도</c:if>
								<c:if test="${board.tmp01 eq '1013'}">전라남도</c:if>
								<c:if test="${board.tmp01 eq '1014'}">경상북도</c:if>
								<c:if test="${board.tmp01 eq '1015'}">경상남도</c:if>
								<c:if test="${board.tmp01 eq '1016'}">제주특별자치도</c:if>
							</dd>
						</c:if>
					</dl>
				</c:if>

				<dl class="info_view2">
					<dt class="writer">
						<spring:message code="cop.ntcrNm" />
					</dt>
					<dd class="writer">
						<c:out value="${board.ntcrNm}" />
					</dd>
					<dt>
						<spring:message code="cop.frstRegisterPnttm" />
					</dt>
					<dd>
						<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
					</dd>
					<dt>
						<spring:message code="cop.inqireCo" />
					</dt>
					<dd>
						<c:out value="${board.inqireCo}" />
					</dd>
				</dl>

				<%-- 사후관리 게시판 시작 --%>
				<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
					<dl class="info_view2">
						<dt class="writer">
							전화번호
						</dt>
						<dd class="writer">
								${board.tmp02} - ${board.tmp03} - ${board.tmp04}
						</dd>
					</dl>
				</c:if>
				<%-- 사후관리 게시판 끝 --%>

				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<dl class="file_view">
						<dt>
							<spring:message code="cop.atchFileList" />
						</dt>
						<dd>
							<c:if test="${not empty board.atchFileId}">
								<script type="text/javascript">
									DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
									DEXT5UPLOAD.config.Width = '700px';
									DEXT5UPLOAD.config.Height = '200px';
									DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
									DEXT5UPLOAD.config.MaxTotalFileCount = '10';
									DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
									DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
									DEXT5UPLOAD.config.Mode = 'view'; // edit, view
									var upload = new Dext5Upload("dext5upload");
								</script>
							</c:if>
						</dd>
					</dl>
				</c:if>
				<div class="view_cont">
					<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA02'}">
						<script type="text/javascript">
							DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
							DEXT5UPLOAD.config.Width = '700px';
							DEXT5UPLOAD.config.Height = '200px';
							DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
							DEXT5UPLOAD.config.MaxTotalFileCount = '10';
							DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
							DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
							DEXT5UPLOAD.config.Mode = 'view'; // edit, view
							var upload = new Dext5Upload("dext5upload");
						</script>
					</c:if>
					<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
				</div>

				<%-- 민원형 게시판 답변 시작(사후관리)--%>
				<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
					<c:if test="${board.processSttusCode eq 'QA03'}">
						<c:if test="${not empty board.estnData}">
							<div class="view_cont_rep">
								<strong>답변</strong>
								<div class="cont">
									<c:out value="${fn:replace(board.estnParseData.cn, newLineChar, '<br/>')}" escapeXml="false" />
								</div>
							</div>

							<dl class="info_view2">
								<dt class="writer">답변자</dt>
								<dd class="writer">${board.lastAnswrrName}</dd>
								<dt>등록일</dt>
								<dd>
									<fmt:formatDate value="${board.lastAnswrrPnttm}" pattern="yyyy.MM.dd" />
								</dd>
								<dt>전화번호</dt>
								<dd>${board.lastAnswrrPhone}</dd>
							</dl>
						</c:if>
						<c:if test="${not empty board.estnAtchFileId}">
							<dl class="file_view">
								<dt>
									<spring:message code="cop.atchFileList" />
								</dt>
								<dd>
									<c:if test="${not empty board.estnAtchFileId}">
										<script type="text/javascript">
											DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
											DEXT5UPLOAD.config.Width = '700px';
											DEXT5UPLOAD.config.Height = '200px';
											DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
											DEXT5UPLOAD.config.MaxTotalFileCount = '10';
											DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
											DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
											DEXT5UPLOAD.config.Mode = 'view'; // edit, view
											var upload = new Dext5Upload("dext5upload");
										</script>
									</c:if>
								</dd>
							</dl>
						</c:if>
					</c:if>
				</c:if>
			</c:if>
			<%-- 일반 게시판 끝--%>



			<%-- 유튜브 동영상 게시판 시작--%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04'}">
				<dl class="tit_view">
					<dt>
						<spring:message code="cop.nttSj" />
					</dt>
					<dd>
						<c:out value="${board.nttSj}" />
					</dd>
				</dl>
				<c:if test="${!empty brdMstrVO.ctgrymasterId}">
					<dl class="info_view">
						<dt>
							<spring:message code="cop.category.view" />
						</dt>
						<dd>
							<c:out value="${board.ctgryNm}" />
						</dd>
					</dl>
				</c:if>

				<dl class="info_view2">
					<dt class="writer">
						<spring:message code="cop.ntcrNm" />
					</dt>
					<dd class="writer">
						<c:out value="${board.ntcrNm}" />
					</dd>
					<dt>
						<spring:message code="cop.frstRegisterPnttm" />
					</dt>
					<dd>
						<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
					</dd>
					<dt>
						<spring:message code="cop.inqireCo" />
					</dt>
					<dd>
						<c:out value="${board.inqireCo}" />
					</dd>
				</dl>
				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<dl class="file_view">
						<dt>
							<spring:message code="cop.atchFileList" />
						</dt>
						<dd>
							<c:if test="${not empty board.atchFileId}">
								<script type="text/javascript">
									DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
									DEXT5UPLOAD.config.Width = '700px';
									DEXT5UPLOAD.config.Height = '200px';
									DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
									DEXT5UPLOAD.config.MaxTotalFileCount = '10';
									DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
									DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
									DEXT5UPLOAD.config.Mode = 'view'; // edit, view
									var upload = new Dext5Upload("dext5upload");
								</script>
							</c:if>
						</dd>
					</dl>
				</c:if>
				<div class="view_cont">
					<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA04' and not empty board.tmp01}">
						<div style="text-align: center; padding: 20px 0 40px 0;">${board.tmp01}</div>
					</c:if>
					<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
				</div>
			</c:if>
			<%-- 유튜브 동영상 게시판 끝--%>



			<%-- 민원형 게시판 시작--%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">

				<div class="top_info">
					<span>민원인 : <c:out value="${board.ntcrNm}" /></span> <span>등록일 : <fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd" /></span> <span>상태 : <strong class="red"><c:out value="${board.processSttusNm}" /></strong></span>
				</div>
				<dl class="tit_view">
					<dt>
						<spring:message code="cop.nttSj" />
					</dt>
					<dd>
						<c:out value="${board.nttSj}" />
					</dd>
				</dl>

				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<dl class="file_view">
						<dt>
							<spring:message code="cop.atchFileList" />
						</dt>
						<dd>
							<c:if test="${not empty board.atchFileId}">
								<script type="text/javascript">
									DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
									DEXT5UPLOAD.config.Width = '700px';
									DEXT5UPLOAD.config.Height = '200px';
									DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
									DEXT5UPLOAD.config.MaxTotalFileCount = '10';
									DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
									DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
									DEXT5UPLOAD.config.Mode = 'view'; // edit, view
									var upload = new Dext5Upload("dext5upload");
								</script>
							</c:if>
						</dd>
					</dl>
				</c:if>
				<div class="view_cont">
					<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
				</div>

				<%-- 민원형 게시판 답변 시작--%>
				<c:if test="${board.processSttusCode eq 'QA03'}">
					<c:if test="${not empty board.estnData}">
						<div class="view_cont_rep">
							<strong>답변</strong>
							<div class="cont">
								<c:out value="${fn:replace(board.estnParseData.cn, newLineChar, '<br/>')}" escapeXml="false" />
							</div>
						</div>

						<dl class="info_view2">
							<dt class="writer">답변자</dt>
							<dd class="writer">${board.lastAnswrrName}</dd>
							<dt>등록일</dt>
							<dd>
								<fmt:formatDate value="${board.lastAnswrrPnttm}" pattern="yyyy.MM.dd" />
							</dd>
							<dt>전화번호</dt>
							<dd>${board.lastAnswrrPhone}</dd>
						</dl>
					</c:if>
					<c:if test="${not empty board.estnAtchFileId}">
						<dl class="file_view">
							<dt>
								<spring:message code="cop.atchFileList" />
							</dt>
							<dd>
								<script type="text/javascript">
									DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
									DEXT5UPLOAD.config.Width = '700px';
									DEXT5UPLOAD.config.Height = '200px';
									DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
									DEXT5UPLOAD.config.MaxTotalFileCount = '10';
									DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
									DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
									DEXT5UPLOAD.config.Mode = 'view'; // edit, view
									var upload = new Dext5Upload("dext5upload");
								</script>
							</dd>
						</dl>
					</c:if>
				</c:if>
				<%-- 민원형 게시판 답변 끝--%>
			</c:if>
			<%-- 민원형 게시판 끝--%>


			<%-- 사전공표 게시판 시작--%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA05'}">
				<dl class="tit_view">
					<dt>구분</dt>
					<dd>
						<c:out value="${board.ctgryNm}" />
					</dd>
				</dl>
				<dl class="info_view">
					<dt>공표목록</dt>
					<dd>
						<c:out value="${board.nttSj}" />
					</dd>
				</dl>
				<dl class="info_view">
					<dt>공표항목</dt>
					<dd>
						<c:out value="${board.tmp02}" />
					</dd>
				</dl>
				<dl class="info_view2">
					<dt class="writer">공표시기</dt>
					<dd class="writer">
						<c:out value="${board.tmp03}" />
					</dd>
					<dt class="writer">공표주기</dt>
					<dd class="writer">
						<c:out value="${board.tmp04}" />
					</dd>
				</dl>
				<dl class="info_view2">
					<dt class="writer">부서명</dt>
					<dd class="writer">
						<c:out value="${board.tmp05}" />
					</dd>
					<dt class="writer">공표방법</dt>
					<dd class="writer">
						<c:out value="${board.tmp06}" />
					</dd>
				</dl>
				<dl class="info_view2">
					<dt class="writer">작성일</dt>
					<dd class="writer">
						<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
					</dd>
					<dt class="writer">조회수</dt>
					<dd class="writer">
						<c:out value="${board.inqireCo}" />
					</dd>
				</dl>
				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<dl class="file_view">
						<dt>
							<spring:message code="cop.atchFileList" />
						</dt>
						<dd>
							<c:if test="${not empty board.atchFileId}">
								<script type="text/javascript">
									DEXT5UPLOAD.config.InitXml = 'dext5upload.config.xml';
									DEXT5UPLOAD.config.Width = '700px';
									DEXT5UPLOAD.config.Height = '200px';
									DEXT5UPLOAD.config.MaxTotalFileSize = '5GB';
									DEXT5UPLOAD.config.MaxTotalFileCount = '10';
									DEXT5UPLOAD.config.FolderNameRule = "${brdMstrVO.bbsId}";
									DEXT5UPLOAD.config.Lang = "ko-kr"; // ko-kr, en-us, ja-jp, zh-cn, zh-tw
									DEXT5UPLOAD.config.Mode = 'view'; // edit, view
									var upload = new Dext5Upload("dext5upload");
								</script>
							</c:if>
						</dd>
					</dl>
				</c:if>
				<div class="view_cont">
					<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
				</div>
			</c:if>
			<%-- 사전공표 게시판 끝--%>

			<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
				<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
					<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
					<c:param name="sourcId" value="${brdMstrVO.sourcId}" />
				</c:import>
			</c:if>
		</div>

		<div class="btn_all">
			<div class="fL">
				<c:if test="${not empty USER_INFO.id or not empty USER_INFO.credtId or not empty USER_INFO.oldSecId}">
					<c:if test="${brdMstrVO.replyPosblAt eq 'Y' and SE_CODE >= 10}">
						<c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
							<c:param name="nttNo" value="${board.nttNo}" />
							<c:param name="registAction" value="reply" />
						</c:url>
						<span class="bbtn"><a href="<c:out value="${addReplyBoardArticleUrl}"/>"><spring:message code="button.reply" /></a></span>
					</c:if>
					<c:if test="${board.frstRegisterId eq USER_INFO.id or SE_CODE >= 9}">
						<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
							<c:param name="nttNo" value="${board.nttNo}" />
							<c:param name="registAction" value="updt" />
						</c:url>
						<span class="bbtn"><a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><spring:message code="button.update" /></a></span>
						<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
							<c:param name="nttNo" value="${board.nttNo}" />
						</c:url>
						<span class="bbtn"><a id="btnBbsDelete" href="<c:out value="${deleteBoardArticleUrl}"/>"><spring:message code="button.delete" /></a></span>
					</c:if>
				</c:if>
			</div>
			<div class="fR">
				<span class="bbtn"><a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>"><spring:message code="button.list" /></a></span>
			</div>
		</div>
	</div>
</div>