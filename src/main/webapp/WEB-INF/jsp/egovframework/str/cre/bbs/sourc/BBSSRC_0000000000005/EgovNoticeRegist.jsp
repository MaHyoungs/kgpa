<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%>
</c:set>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_LIB" value="/lib"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>


<c:if test="${not empty USER_INFO}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<c:if test="${not empty UserPrivateCert}">
	<c:set var="USER_INFO" value="${UserPrivateCert}" />
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<c:if test="${!IS_MOBILE}">
	<script type="text/javascript" src="${_C_LIB}/tiny_mce/jquery.tinymce.js"></script>
</c:if>
<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
	<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>
</c:if>

<%-- 웹접근성 관련 타이틀 변경 이벤트 --%>
<script type="text/javascript">
	$(function () {
		<c:if test="${not empty board.nttNo}">
		var titSub = "글수정";
		</c:if>
		<c:if test="${empty board.nttNo}">
		var titSub = "글쓰기";
		</c:if>
		var tit = $('title').text();
		$('title').html('${brdMstrVO.bbsNm}(' + titSub + ') < ' + tit);
	});
</script>
<%-- 웹접근성 관련 타이틀 변경 이벤트 --%>

<script type="text/javascript" src="${_C_JS}/board.js"></script>
<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.min.js"></script>
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">
	<c:if test="${!IS_MOBILE}">
	function fn_egov_regist() {
		if (!fn_egov_bbs_basic_regist(document.board)) {
			return false;
		}

		if($('#nttSj').val().length == 0) {
			alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
			return false;
		}

		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
		if($('#tmp02').val() == '' || $('#tmp03').val() == '' || $('#tmp04').val() == '') {
			alert('연락받으실 전화번호를 입력하세요.');
			return false;
		}

		if($('#fileGroupId').val() == ''){
			alert('파일첨부를 해주십시오.');
			return false;
		}

		</c:if>

		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and brdMstrVO.bbsId eq 'BBSMSTR_000000000008'}">
		if(!$('#ComplaintAgree1').is(':checked')) {
			alert('개인정보 수집 이용에 동의하지 않으셨습니다.\n개인정보 수집이용에 동의하셔야 등록이 가능합니다.');
			$('#ComplaintAgree1').focus();
			return false;
		}
		</c:if>

		<c:choose>
		<c:when test="${searchVO.registAction eq 'updt'}">
		if (!confirm('<spring:message code="common.update.msg" />')) {
			return false;
		}
		</c:when>
		<c:otherwise>
		if (!confirm('<spring:message code="common.regist.msg" />')) {
			return false;
		}
		</c:otherwise>
		</c:choose>
	}

	function privateInfo() {
		var option = 'width=1024, height=800, resizable=no, scrollbars=yes, status=no;'
		window.open('/msi/cntntsService.do?menuId=MNU_0000000000000144', '', option);
	}

	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
	var boardCateLevel = ${boardCateLevel};
	</c:if>

	</c:if>
</script>
<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
	<script type="text/javascript">
		$(function(){
			fnSidoOpenAPIAdd($('#sido'), 1);

			//시도, 시도군구 이벤트
			$('#sido').change(function() {
				$('#sigungu option').remove();
				$('#biz_id option').remove();
				var opt = $('<option value="">시.군.구 선택</option>');
				$('#sigungu').append(opt);
				var opt = $('<option value="">사업명 선택</option>');
				$('#biz_id').append(opt);
				fnSidoOpenAPIAdd($('#sigungu'), $('#sido').val());
				var area = $("#sido option:selected").text();
				fnbizNmSearch($('#biz_id'), area);
			});
		});

		//사업명 검색 Ajax
		function fnbizNmSearch(el, area) {
			var url = '/gfund/biz/mbusinmng/EgovbizNmSearchAjax.do';
			var data = [];
			data = {area : area, isNotPaging : 'isNotPaging', prst_ty_cc : 'PRST02', last_biz_plan_presentn_at : 'Y'};
			var successFn = function(json) {
				if(json.rs != null){
					for(var i=0; i<json.rs.length; i++){
						var opt = $('<option value="' + json.rs[i].biz_id + '">' + json.rs[i].biz_nm + '</option>');
						el.append(opt);
					}
				}
			};
			var errorFn = function(){
				alert('잠시 후 다시 시작하여 주십시요.');
			};
			fn_ajax_json(url, data, successFn, null);
		}

		//사업조회 Ajax
		function fnAjaxBasicInformation() {
			if($('#biz_id').val() != ''){
				var biz_id = $('#biz_id').val();
				$('#tmp01').val(biz_id);
				var url = '/ajaxSelectBusinessBasicInformation.do';
				var data = [];
				data = {biz_id : biz_id};
				var successFn = function(json) {
					if(json.rs != null){
						$('.biz_nm').text(json.rs.biz_nm);
						$('.biz_date').text(json.rs.biz_bgnde + ' ~ ' + json.rs.biz_endde);
						$('.tot_wct').text(json.rs.tot_wct);
						if(json.rs.biz_zip != ''){
							var zip_1 = json.rs.biz_zip.substr(0, 3);
							var zip_2 = json.rs.biz_zip.substr(3, 6);
							$('.biz_adres').text(zip_1 + '-' + zip_2 + ' ' + json.rs.biz_adres + ' ' + json.rs.biz_adres_detail);
						}
						$('.tlphon_no').text(json.rs.tlphon_no);//+'('+ json.rs.moblphon_no +')');
					}
				};
				fn_ajax_json(url, data, successFn, null);
			}else{
				alert('사업을 선택하십시오.');
			}
		}
	</script>
</c:if>

<div class="sub_top sub_top01">
	<div class="navi">
		<span class="location"> <img alt="HOME(메인페이지로 이동)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/icon_home.png">
			<c:choose>
				<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000057' or brdMstrVO.bbsId eq 'BBSMSTR_000000000058' or brdMstrVO.bbsId eq 'BBSMSTR_000000000059' or brdMstrVO.bbsId eq 'BBSMSTR_000000000060'}" >
					<strong> 사후관리 > 과거 사후관리 자료실 > ${brdMstrVO.bbsNm}</strong>
				</c:when>
				<c:otherwise>
					<strong>${brdMstrVO.bbsNm}</strong>
				</c:otherwise>
			</c:choose>
		</span>
	</div>
	<h2>${brdMstrVO.bbsNm}</h2>
</div>

<div id="content">
	<div id="<c:choose><c:when test="${IS_MOBILE }">bbs_mbl</c:when><c:otherwise>bbs_wrap</c:otherwise></c:choose>">

		<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
			<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
			<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>"/>
			<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}"/>
			<input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}"/>
			<input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
			<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>"/>
			<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>"/>
			<input type="hidden" name="registAction" value="${searchVO.registAction}"/>
			<input type="hidden" name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/>

			<form:hidden path="nttNo"/>
<%--			<form:hidden path="ctgryId"/>--%>
			<form:hidden path="ordrCode"/>
			<form:hidden path="ordrCodeDp"/>
			<form:hidden path="atchFileId"/>

			<%-- 사후관리 게시판 --%>
			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
				<input type="hidden" name="tmp01" id="tmp01" value="${board.tmp01}"/>
			</c:if>


			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
				<div class="board_write mB30">
					<dl>
						<dt>
							사업선택
						</dt>
						<dd>
							<select name="sido" id="sido" class="inp">
								<option value="">시.도 선택</option>
							</select>
							<select name="sigungu" id="sigungu" class="inp">
								<option value="">시.군.구 선택</option>
							</select>
							<select name="biz_id" id="biz_id" class="inp">
								<option value="">사업명 선택</option>
							</select>
						<span class="bbtn_bg1 fR">
							<button onclick="fnAjaxBasicInformation();" type="button">확인</button>
						</span>
						</dd>
					</dl>
				</div>
				<div class="board_view mB30">
					<table class="write_table" summary="사업명, 사업기간, 총사업비, 사업유형, 사업지, 재정자립도, 전화번호 항목을 제공하고 있습니다">
					<caption>사업명, 사업기간, 총사업비, 사업유형, 사업지, 재정자립도, 전화번호 항목을 제공하고 있습니다</caption>
					<colgroup>
						<col width="15%"/>
						<col width="35%"/>
						<col width="15%"/>
						<col width="35%"/>
					</colgroup>
					<tbody>
					<tr>
						<th>사업명</th>
						<td colspan="3" class="biz_nm">${bsifVo.biz_nm}</td>
					</tr>
					<tr>
						<th>사업기간</th>
						<td class="biz_date">${bsifVo.biz_bgnde} ~ ${bsifVo.biz_endde}</td>
						<th>총사업비</th>
						<td class="tot_wct"><fmt:formatNumber value="${bsifVo.tot_wct}" groupingUsed="true"/></td>
					</tr>
					<tr>
						<th>사업지</th>
						<td colspan="3" class="biz_adres">${fn:substring(bsifVo.biz_zip, 0, 3)} - ${fn:substring(bsifVo.biz_zip, 3, 6)} ${bsifVo.biz_adres} ${bsifVo.biz_adres_detail}</td>
					</tr>
					<tr>
						<th>전화번호</th>
						<td colspan="3" class="tlphon_no">${bsifVo.tlphon_no}</td>
					</tr>
					</tbody>
					</table>
				</div>
			</c:if>

			<div class="board_write">
				<dl>
					<c:choose>
						<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
							<input type="hidden" name="nttSj" value="dummy"/>
							<dt>
								<label for="ftext"><spring:message code="cop.processSttus"/></label>
							</dt>
							<dd>
								<select name="processSttusCode" id="ftext" class="select">
									<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
										<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}"/></option>
									</c:forEach>
								</select>
							</dd>
						</c:when>
						<c:otherwise>
							<dt>
								<label for="nttSj"><spring:message code="cop.nttSj"/></label>
							</dt>
							<dd>
								<form:input path="nttSj" cssClass="inp tit"/>
							</dd>
							<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
								<dt>
					 				<label for="ctgry1">연도</label>
								</dt>
								<dd>
									<select name="ctgryId" id="ctgryId" class="inp_long">
										<c:forEach var="cate" items="${boardCateList}">
											<c:if test="${cate.ctgryLevel ne 0 }">
												<option value="${cate.ctgryId}" <c:if test="${cate.ctgryId eq board.ctgryId or cate.ctgryId eq searchVO.ctgryId}"> selected="selected"</c:if>>${cate.ctgryNm}</option>
											</c:if>
										</c:forEach>
									</select>
								</dd>
							</c:if>

							<c:if test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000057' or brdMstrVO.bbsId eq 'BBSMSTR_000000000058'}">
								<dt>
									<label for="tmp01">시·도</label>
								</dt>
								<dd>
									<select name="tmp01" id="tmp01" class="inp_long3">
										<option value="" <c:if test="${empty board.tmp01}"> selected="selected"</c:if>>미지정</option>
										<option value="1000" <c:if test="${board.tmp01 eq '1000'}"> selected="selected"</c:if>>서울특별시</option>
										<option value="1001" <c:if test="${board.tmp01 eq '1001'}"> selected="selected"</c:if>>부산광역시</option>
										<option value="1002" <c:if test="${board.tmp01 eq '1002'}"> selected="selected"</c:if>>대구광역시</option>
										<option value="1003" <c:if test="${board.tmp01 eq '1003'}"> selected="selected"</c:if>>인천광역시</option>
										<option value="1004" <c:if test="${board.tmp01 eq '1004'}"> selected="selected"</c:if>>광주광역시</option>
										<option value="1005" <c:if test="${board.tmp01 eq '1005'}"> selected="selected"</c:if>>대전광역시</option>
										<option value="1006" <c:if test="${board.tmp01 eq '1006'}"> selected="selected"</c:if>>울산광역시</option>
										<option value="1007" <c:if test="${board.tmp01 eq '1007'}"> selected="selected"</c:if>>세종특별자치시</option>
										<option value="1008" <c:if test="${board.tmp01 eq '1008'}"> selected="selected"</c:if>>경기도</option>
										<option value="1009" <c:if test="${board.tmp01 eq '1009'}"> selected="selected"</c:if>>강원도</option>
										<option value="1010" <c:if test="${board.tmp01 eq '1010'}"> selected="selected"</c:if>>충청북도</option>
										<option value="1011" <c:if test="${board.tmp01 eq '1011'}"> selected="selected"</c:if>>충청남도</option>
										<option value="1012" <c:if test="${board.tmp01 eq '1012'}"> selected="selected"</c:if>>전라북도</option>
										<option value="1013" <c:if test="${board.tmp01 eq '1013'}"> selected="selected"</c:if>>전라남도</option>
										<option value="1014" <c:if test="${board.tmp01 eq '1014'}"> selected="selected"</c:if>>경상북도</option>
										<option value="1015" <c:if test="${board.tmp01 eq '1015'}"> selected="selected"</c:if>>경상남도</option>
										<option value="1016" <c:if test="${board.tmp01 eq '1016'}"> selected="selected"</c:if>>제주특별자치도</option>
									</select>
								</dd>
							</c:if>

							<c:if test="${SE_CODE >= 10}">
								<dt>
									<label><spring:message code="cop.noticeAt"/></label>
								</dt>
								<dd>
									<spring:message code="button.yes" var="noticeAtY"/>
									<form:radiobutton path="noticeAt" value="Y" label="${noticeAtY }"/>
									&nbsp;
									<spring:message code="button.no" var="noticeAtN"/>
									<form:radiobutton path="noticeAt" value="N" label="${noticeAtN}"/>
									<form:errors path="noticeAt"/>
								</dd>
							</c:if>
							<c:if test="${brdMstrVO.othbcUseAt eq 'Y'}">
								<dt>
									<label><spring:message code="cop.publicAt"/></label>
								</dt>
								<dd>
									<spring:message code="cop.public" var="othbcAtY"/>
									<form:radiobutton path="othbcAt" value="Y" label="${othbcAtY }"/>
									&nbsp;
									<spring:message code="cop.private" var="othbcAtN"/>
									<form:radiobutton path="othbcAt" value="N" label="${othbcAtN }"/>
									<form:errors path="othbcAt"/>
								</dd>
							</c:if>
						</c:otherwise>
					</c:choose>

					<dt>
						<label><spring:message code="cop.ntcrNm"/></label>
					</dt>
					<c:choose>
						<c:when test="${searchVO.registAction eq 'updt' }">
							<dd>
								<strong><c:out value="${board.ntcrNm }"/></strong>
								<c:if test="${not empty board.frstRegisterId }">(<c:out value="${board.frstRegisterId }"/>)</c:if>
							</dd>
						</c:when>
						<c:otherwise>
							<dd>
								<strong><c:out value="${USER_INFO.name }"/></strong>
								<c:if test="${not empty USER_INFO.id }">(<c:out value="${USER_INFO.id }"/>)</c:if>
							</dd>
						</c:otherwise>
					</c:choose>

						<%-- 사후관리 게시판 --%>
					<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA07'}">
						<dt>
							<label for="tmp02">전화번호</label>
						</dt>
						<dd>
							<input type="text" name="tmp02" id="tmp02" value="${board.tmp02}" class="inp" maxlength="4" style="width:40px;" onkeyup="fnInputNumCom($(this));" /> -
							<input type="text" name="tmp03" id="tmp03" value="${board.tmp03}" class="inp" maxlength="4" style="width:40px;" onkeyup="fnInputNumCom($(this));" /> -
							<input type="text" name="tmp04" id="tmp04" value="${board.tmp04}" class="inp" maxlength="4" style="width:40px;" onkeyup="fnInputNumCom($(this));" />
						</dd>
					</c:if>

					<dt class="hdn">
						<label for="nttCn"><spring:message code="cop.nttCn"/></label>
					</dt>
					<dd class="write_cont">
						<div class="commediter">
							<c:if test="${(brdMstrVO.bbsAttrbCode eq 'BBSA11' or brdMstrVO.bbsAttrbCode eq 'BBSA07' or brdMstrVO.bbsAttrbCode eq 'BBSA03') and empty board.nttCn}">
								<textarea name="nttCn" id="nttCn" rows="10" style="width: 100%;"></textarea>
							</c:if>
							<c:if test="${not empty board.nttCn}">
								<form:textarea path="nttCn" rows="10" cssStyle="width:100%;"/>
								<br/>
								<form:errors path="nttCn"/>
							</c:if>
						</div>
					</dd>
				</dl>
			</div>

			<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and brdMstrVO.bbsId eq 'BBSMSTR_000000000008'}">
				<div class="pbg3 mB30">
					녹색사업단은 민원 및 사후관리를 위한 담당자와의 업무연락, 정보제공, 통계등을 위하여 위와같은 개인정보를 수집하며, 필요 최소한의 범위내에서 개인정보를 수집합니다. 수집된 개인정보는 민원처리만을 위하여 사용하며 그 이외의 용도로는 사용치 않습니다. 정보통신망 이용촉진 및 정보보호 등에 관한 법률' 제44조의5 (게시판 이용자의 본인 확인)
					<div class="btn_l">
						<a href="javascript:privateInfo();" class="cbtn"><span class="icon_note">개인정보취급방침 보기</span></a>
					</div>

					<div class="radio_box">
						<input type="radio" value="1" id="ComplaintAgree1" name="ComplaintAgree"/>
						<label for="ComplaintAgree1"> 동의합니다.</label>
						<input type="radio" value="0" id="ComplaintAgree0" name="ComplaintAgree"/>
						<label for="ComplaintAgree0"> 동의하지 않습니다.</label>
					</div>
				</div>
			</c:if>
		</form:form>

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
				<c:param name="editorId" value="${_EDITOR_ID}"/>
				<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}"/>
				<c:param name="param_atchFileId" value="${board.atchFileId}"/>
				<c:param name="imagePath" value="${_IMG }"/>
				<c:param name="potalService" value="Y"/>
			</c:import>
			<noscript>
				<label for="egovComFileUploader"><spring:message code="cop.atchFileList"/></label>
				<input name="file_1" id="egovComFileUploader" type="file" class="inp"/>
			</noscript>
		</c:if>

		<div class="btn_c">
			<c:choose>
				<c:when test="${searchVO.registAction eq 'regist' and SE_CODE >= brdMstrVO.registAuthor}">
					<span class="bbtn_bg1"><button type="button" onclick="$('#board').submit();">
						<spring:message code="button.create"/>
					</button></span>
				</c:when>
				<c:when test="${searchVO.registAction eq 'updt' and SE_CODE >= brdMstrVO.registAuthor}">
					<span class="bbtn_bg1"><button type="button" onclick="$('#board').submit();">
						<spring:message code="button.update"/>
					</button></span>
				</c:when>
				<c:when test="${searchVO.registAction eq 'reply' and SE_CODE >= brdMstrVO.registAuthor}">
					<span class="bbtn_bg1"><button type="button" onclick="$('#board').submit();">
						<spring:message code="button.reply"/>
					</button></span>
				</c:when>
			</c:choose>
			<c:url var="selectBoardListUrl" value="${_PREFIX}/selectBoardList.do">
				<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
				<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
				<c:if test="${not empty searchVO.searchCate}">
					<c:param name="searchCate" value="${searchVO.searchCate}"/>
				</c:if>
				<c:if test="${not empty searchVO.searchCnd}">
					<c:param name="searchCnd" value="${searchVO.searchCnd}"/>
				</c:if>
				<c:if test="${not empty searchVO.searchWrd}">
					<c:param name="searchWrd" value="${searchVO.searchWrd}"/>
				</c:if>
				<c:if test="${not empty searchVO.tmplatImportAt}">
					<c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/>
				</c:if>
			</c:url>
			<span class="bbtn_bg2"><a href="<c:out value="${selectBoardListUrl}"/>"><spring:message code="button.reset"/></a></span>
		</div>

		<script type="text/javascript">
			(function () {
				var progress = $('.progress');
				var bar = $('.bar');
				$('#boardFileAjaxForm').ajaxForm({
					dataType: 'json',
					beforeSend: function () {
						progress.show();
						var percentVal = '0%';
						progress.width(percentVal);
						bar.html(percentVal);
					},
					uploadProgress: function (event, position, total, percentComplete) {
						var percentVal = percentComplete - 1 + '%';
						progress.width(percentVal);
						bar.html(percentVal);
					},
					success: function (json) {
						if(json.rs == 'countOver') {
							$('.progress').hide();
							$('.progress').width("0%");
							$('.bar').html("0%");
							alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
						}else if (json.rs == 'overflow') {
							$('.progress').hide();
							$('.progress').width("0%");
							$('.bar').html("0%");
							alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
						}else if (json.rs == 'denyExt') {
							$('.progress').hide();
							$('.progress').width("0%");
							$('.bar').html("0%");
							alert("첨부할 수 없는 확장자입니다.");
						}else {
							var percentVal = '100%';
							progress.width("99%");
							bar.html(percentVal);
							fnAjaxFileUploadComplete(json);
						}
					},
					complete: function (xhr) {
						var file = $('#boardFileAjaxForm input[name=uploadfile]').clone();
						var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
						$('#boardFileAjaxForm input[name=uploadfile]').remove();
						file_parent.prepend(file);
					}
				});
			})();
		</script>
	</div>
</div>