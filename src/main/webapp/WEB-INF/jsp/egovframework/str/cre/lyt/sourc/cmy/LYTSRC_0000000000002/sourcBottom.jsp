<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<%
	/*
		사이트맵 			: /msi/siteMap.do
		개인정보보호정책 	: /msi/indvdlInfoPolicy.do
		이용약관			: /msi/useStplat.do
		이메일수집거부		: /msi/emailColctPolicy.do
	*/
%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images"/>

		<!-- footer start -->
		<div id="footer">
			<h2><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/common/logo02.png" alt="산림청 녹색사업단" /></h2>

			<div class="fcont">
				<ul>
					<li><a href="">기관소개</a><span>|</span></li>
					<li><a href="">개인정보처리방침</a><span>|</span></li>
					<li><a href="">이메일무단수집거부</a><span>|</span></li>
					<li><a href="">오시는길</a><span>|</span></li>
					<li><a href="">관리자메일</a></li>
				</ul>
				<address>
					대전광역시 서구 둔산북로 121, 2층   산림청 녹색사업단     사업자등록번호 34-82-11213    대표 장찬식 <br />
					운영관리 : 042-803-7305     기획홍보 : 042-803-7331     국내사업 : 042-803-7308     글로벌사업 : 042-803-7347     FAX : 042-803-7310<br />
					산림비전센터 : 서울특별시 영등포구 국회대로 82길 9     TEL : 02-782-2612     FAX : 02-782-4321<br />
					산림교육팀[백두대간숲생태원] : 경상북도 상주시 공성면 웅산로 705     TEL : 054-538-0914     FAX : 054-538-0905
				</address>
				<p>Copyright ⓒ 2014 KGPA. All rights reserved.</p>


			</div>
		</div>
		<!-- footer end-->

	</body>
</html>