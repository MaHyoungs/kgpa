<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images"/>

<!-- footer start -->
<div id="footer">
	<div class="fcont">
		<ul>
			<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000125">기관소개</a><span>|</span></li>
			<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000144">개인정보처리방침</a><span>|</span></li>
			<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000145">이메일무단수집거부</a><span>|</span></li>
			<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000133">오시는길</a><span>|</span></li>
			<li><a href="mailto:hyou14@kgpa.or.kr">관리자메일</a></li>
		</ul>
		<address>
			대전광역시 서구 둔산북로 121, 2층 산림청 녹색사업단 사업자등록번호 314-82-11213 대표 허경태 <br/>
			대표번호 : 042-603-7305   FAX : 042-603-7310
		</address>
		<p>Copyright ⓒ 2014 KGPA. All rights reserved.</p>
		<a href="http://webwatch.or.kr/certification/situation.html" class="mark">
			<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/common/img_mark.png" alt="미래창조과학부 WEB ACCESSIBILITY 마크(웹 접근성 품질인증 마크)" title="국가 공인 인증기관 : 웹와치"/>
		</a>
	</div>
</div>
<!-- footer end-->

</body>
</html>