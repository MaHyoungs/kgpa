<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<%@ page import="egovframework.com.cmm.service.Globals" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="PUBLISH_APPEND_FREFIX"><%=Globals.getPublishAppendPrefix(request)%></c:set>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>" />
<c:set var="_WEB_FULL_PATH" value="${user_protocol}${siteInfo.siteUrl}" />
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images" />
<c:set var="_CSS" value="${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}" />
<c:set var="_JS" value="${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}" />
<c:set var="C_JS" value="${_WEB_FULL_PATH}/template/common/js" />
<c:set var="C_CSS" value="${_WEB_FULL_PATH}/template/common/css" />
<c:set var="CURR_URL" value="<%=helper.getOriginatingRequestUri(request) %>" />

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${C_JS}/common.js"></script>
<script type="text/javascript" src="${C_JS}/main.js"></script>
<script type="text/javascript" src="${C_JS}/jquery/jquery.placeholder.min.js"></script>

<link rel="shortcut icon" href="${_WEB_FULL_PATH}/favicon.ico" />

<c:if test="${param.isMain eq 'Y'}">
	<script type="text/javascript" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/daum.js"></script>
	<script type="text/javascript" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/facebook.js"></script>
</c:if>

<c:if test="${(param.menuId eq 'MNU_0000000000000133') or (param.menuId eq 'MNU_0000000000000208') or (param.menuId eq 'MNU_0000000000000209') or (param.menuId eq 'MNU_0000000000000210')}">
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true&language=ko"></script>
</c:if>

<link charset="utf-8" href="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/style.css" type="text/css" rel="stylesheet" />
<script charset="utf-8" src="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/script.js" type="text/javascript"></script>
	
<c:if test="${currMpm.htmlUseAt eq 'Y'}">
	<link charset="utf-8" href="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/style.css" type="text/css" rel="stylesheet" />
	<script charset="utf-8" src="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/script.js" type="text/javascript"></script>
</c:if>
<c:if test="${not empty param.BBS_TMPLATID}">
	<link charset="utf-8" href="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/script.js"></script>
</c:if>
<c:if test="${siteInfo.mouseScrtyApplcAt eq 'Y' }">
	<script type="text/javascript">$(document).ready(function(){
		$(document).bind("contextmenu", function(e){
			return false;
		});
	});</script>
</c:if>
<c:if test="${siteInfo.kybrdScrtyApplcAt eq 'Y' }">
	<script type="text/javascript">$(document).ready(function(){
		$(document).bind('selectstart', function(){
			return false;
		});
		$(document).bind('dragstart', function(){
			return false;
		});
		$(document).keydown(function(e){
			if(e.ctrlKey && e.which == 65) return false;
			if(e.ctrlKey && e.which == 67) return false;
		});
	});	</script>
</c:if>
<c:if test="${param.isMain eq 'Y'}">
	<c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
</c:if>
<title><c:if test="${not empty currMpm}"><c:set var="menuArr" value="${fn:split(currMpm.menuPathByName, '>')}" /><c:forEach var="i" begin="0" end="${fn:length(menuArr)}">${menuArr[fn:length(menuArr)-i]}<c:if test="${i ne 0 and i ne fn:length(menuArr)}"> &#60; </c:if></c:forEach> | </c:if>산림청 녹색사업단</title>

</head>
<body>
	<!-- skipnavi start -->
	<div id="skilNavi">
		<a href="#content" class="skipBtn">본문바로가기</a>
		<a href="#lnb" class="skipBtn">주메뉴바로가기</a>
	</div>
	<!-- //skipnavi end -->


	<!-- header start -->
	<div id="header">
		<div class="header">
			<h1><a href="/index.do"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/common/logo.png" alt="산림청 녹색사업단" /></a></h1>

			<div class="gnb">
				<ul>
					<li><a href="/index.do">HOME</a> <span>|</span></li>
					<li><a href="/msi/siteMap.do?menuId=MNU_0000000000000143">사이트맵</a> <span>|</span></li>
					<%--<li><a href="">웹메일</a> <span>|</span></li>--%>
					<li><a href="" onclick="alert('준비중입니다.'); return false;">English</a></li>
				</ul>

				<div class="tsearch">
					<form name="totalSearch" method="post" action="/sch/search.do">
						<fieldset>
							<legend>통합검색 입력폼</legend>
							<input type="text" name="searchWrd" class="inp_s" title="통합검색어입력" />
							<input type="image" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/common/btn_search.gif" alt="통합검색" />
						</fieldset>
					</form>
				</div>
			</div>

			<div id="lnb">
                <h2 class="hdn">주메뉴</h2>
				<ul>
					<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
						<c:param name="menuTarget" value="Main" />
						<c:param name="menuType" value="Simple2Depth" />
						<c:param name="USER_SE" value="${USER_SE_ }"></c:param>
					</c:import>
				</ul>
            </div>
		</div>
		<c:if test="${param.isMain eq 'N'}">
			<c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
			<div class="header_line"></div>
		</c:if>
	</div>
	<!-- header end -->