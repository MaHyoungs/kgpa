<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}" />
		</c:import>
	</c:when>
	<c:otherwise>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="Content-Script-Type" content="text/javascript" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<link charset="utf-8" href="${_C_CSS}/default.css" type="text/css" rel="stylesheet" />
			<script type="text/javascript" src="${_C_JS}/jquery/jquery-1.8.2.min.js"></script>
			<script type="text/javascript" src="${_C_JS}/common.js"></script>
		</head>
		<body>
	</c:otherwise>
</c:choose>

<!-- main strat -->
<div class="main_container">
	<ul class="visual_list">
		<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
			<c:param name="tableId" value="idxBg" />
		</c:import>
	</ul>

	<div class="msection_box">
		<div id="content">
			<div class="msection1">
				<p class="slogan"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/img_slogan.png" alt="세상을 숲으로 디자인하다" /> </p>

				<div class="btn_ctl">
					<a href="#link" class="play"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_play.png" alt="배경이미지 재생" /></a>
					<a href="#link" class="stop"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_stop.png" alt="배경이미지 멈춤" /></a>
				</div>

				<div class="evt_zone">
					<h3>PopupZone</h3>

					<div class="paginate">
						<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
							<c:param name="tableId" value="PopupZone" />
							<c:param name="listBtn" value="paginate" />
						</c:import>
					</div>

					<div class="position">
						<button class="m_play" >이벤트존 시작</button>
						<button class="m_stop" >이벤트존 멈춤</button>
					</div>

					<ul class="popupzone">
						<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
							<c:param name="tableId" value="PopupZone" />
							<c:param name="listTag" value="li" />
						</c:import>
					</ul>
				</div>
			</div>

			<div class="msection_colum">
				<div class="msection2">
					<div class="mlink">
						<ul>
							<!-- <li class="mlink01"><a href="http://gfund.kgpa.or.kr/" target="_blank" title="녹색자금통합관리시스템">녹색자금<br />통합관리시스템</a></li> --><!-- 6월부터 사용 -->
							<li class="mlink01"><a href="http://carbon.kgpa.or.kr/flow/" target="_blank" title="산림탄소센터">산림탄소센터</a></li>
							<li class="mlink02"><a href="http://www.soop.or.kr/main.do" target="_blank" title="칠곡나눔숲체원">칠곡나눔숲체원</a></li>
							<li class="mlink03"><a href="http://www.ofiis.or.kr " target="_blank" title="해외조림투자지도시스템">해외산림<br>투자정보서비스</a></li>
							<li class="mlink04"><a href="https://www.foresteco.or.kr/" target="_blank" title="백두대간숲생태원">백두대간<br>숲생태원</a></li>
						</ul>
					</div>

					<div class="bbs_list_box">
						<div class="mtab_box on">
							<h3><a href="#btab1">공지사항</a></h3>
							<div id="btab1">
								<ul>
									<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
										<c:param name="linkMenuId" value="MNU_0000000000000049" />
										<c:param name="tableId" value="BBSMSTR_000000000022" />
										<c:param name="itemCount" value="7" />
										<c:param name="listTag" value="Y" />
									</c:import>
								</ul>
								<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000022&amp;menuId=MNU_0000000000000049" class="more"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/bg_more.png" alt="공지사항 더보기" /></a>
							</div>
						</div>

						<div class="mtab_box">
							<h3><a href="#btab2">입찰정보</a></h3>
							<div id="btab2">
								<ul>
									<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
										<c:param name="linkMenuId" value="MNU_0000000000000163" />
										<c:param name="tableId" value="BBSMSTR_000000000049" />
										<c:param name="itemCount" value="7" />
										<c:param name="listTag" value="Y" />
									</c:import>
								</ul>
								<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000049&amp;menuId=MNU_0000000000000163" class="more"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/bg_more.png" alt="입찰정보 더보기" /></a>
							</div>
						</div>

						<div class="mtab_box">
							<h3><a href="#btab3">채용정보</a></h3>
							<div id="btab3">
								<ul>
									<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
										<c:param name="linkMenuId" value="MNU_0000000000000165" />
										<c:param name="tableId" value="BBSMSTR_000000000050" />
										<c:param name="itemCount" value="7" />
										<c:param name="listTag" value="Y" />
									</c:import>
								</ul>
								<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000050&amp;menuId=MNU_0000000000000165" class="more"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/bg_more.png" alt="채용정보 더보기" /></a>
							</div>
						</div>
					</div>
				</div>

				<div class="msection3">
					<div class="blist">
						<h3>사업단소식</h3>
						<ul>
							<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
								<c:param name="linkMenuId" value="MNU_0000000000000051" />
								<c:param name="tableId" value="BBSMSTR_000000000023" />
								<c:param name="itemCount" value="5" />
								<c:param name="listTag" value="Y" />
							</c:import>
						</ul>
						<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000023&menuId=MNU_0000000000000051" class="more"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/bg_more.png" alt="사업단소식 더보기" /></a>
					</div>

					<div class="plist">
						<h3>보도자료</h3>
						<c:import url="/msi/ctn/preImgToBoard.do" charEncoding="utf-8">
							<c:param name="linkMenuId" value="MNU_0000000000000153" />
							<c:param name="tableId" value="BBSMSTR_000000000046" />
							<c:param name="itemCount" value="5" />
						</c:import>
						<ul>
							<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
								<c:param name="linkMenuId" value="MNU_0000000000000153" />
								<c:param name="tableId" value="BBSMSTR_000000000046" />
								<c:param name="itemCount" value="5" />
								<c:param name="listTag" value="Y" />
							</c:import>
						</ul>
						<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000046&amp;menuId=MNU_0000000000000153" class="more"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/bg_more.png" alt="보도자료 더보기" /></a>
					</div>
				</div>
			</div>

			<div class="sns_list_box">
				<div class="sns_box">
					<div class="mtab_box">
						<h3 ><a href="#facebook"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/img_facebook.gif" alt="페이스북" /></a></h3>

						<div id="facebook" class="sns_list">
							<div class="sns_top">
								<span class="logo">
									<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/sns/img_logo.png" alt="녹색사업단"/>
								</span>

								<div class="info">
									<%-- 산림청 이름 , 영어 표시 --%>
									<div class="title">
										<strong>녹색사업단</strong>
										<span>(Korea Green Promotion Agency)</span>
									</div>
									<%-- 좋아요, like 수 --%>
									<span class="hit">
										<!-- <iframe src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fkgpanews&width=62&layout=button&action=like&show_faces=false&share=false&height=26&appId=1405236629770025" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:62px; height:20px;" allowTransparency="true"></iframe> -->

										<span id="like_fb_count">36,590 명</span>
									</span>
								</div>
							</div>
							<div class="bg_this"></div>
							<%-- 내용 --%>
							<div class="sns_content">
								<div class="group" id="facebook_group"></div>
							</div>
						</div>
					</div>
					<div class="mtab_box">
						<h3><a href="http://blog.daum.net/kgpa"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/img_blog.gif" alt="블로그" /></a></h3>
						<%--<div id="blog" class="sns_list">
							<div class="sns_top">
								<span class="logo">
									<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/sns/img_logo.png" alt="녹색사업단"/>
								</span>
								<div class="info">
									<strong id="div_blog_tit"></strong> &lt;%&ndash; 제목 &ndash;%&gt;
									<div class="visiter" id="div_blog_good"></div> &lt;%&ndash; 방문자 &ndash;%&gt;
								</div>
							</div>
							<div class="bg_this"></div>
							&lt;%&ndash; 내용 &ndash;%&gt;
							<div class="sns_content" id="div_blog_group"></div>
						</div>--%>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- banner_zone start-->
<div class="banner_zone">
	<div class="banner">
		<h3><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/tit_banner.png" alt="BannerZone" /></h3>
		<div class="banner_btn">
			<input id="bn_start" type="image" alt="재생버튼(Banner Zone)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_banner_play.png" onclick="banner.resume(); return false;" />
			<input id="bn_stop" type="image" alt="정지버튼(Banner Zone)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_banner_stop.png" onclick="banner.stop(); return false;"/>
			<input id="bn_prev" type="image" alt="이전버튼(Banner Zone)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_banner_prev.png" onclick="banner.direction = 4; banner.resume(); return false;"/>
			<input id="bn_next" type="image" alt="다음버튼(Banner Zone)" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/main/btn_banner_next.png" onclick="banner.direction = 2; banner.resume(); return false;" />
		</div>
		<div class="slide_banner">
			<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
				<c:param name="tableId" value="BannerZone" />
			</c:import>
		</div>

		<script language="javascript" type="text/javascript">
			//<![CDATA[
			var banner = null;
			banner = new js_rolling('banner_list');
			banner.set_direction(4);
			banner.time_dealy = 20;
			banner.time_dealy_pause = 0;
			setTimeout('banner.start()', 2000);
			//]]>
		</script>

	</div>
</div>
<!-- banner_zone end-->
<!-- main end -->

<!-- Bottom Import -->
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}" />
		</c:import>
	</c:when>
	<c:otherwise>
		</body>
		</html>
	</c:otherwise>
</c:choose>
<!-- Bottom Import -->