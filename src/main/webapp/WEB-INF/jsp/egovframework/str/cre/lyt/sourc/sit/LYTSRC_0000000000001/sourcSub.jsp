<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
	<div class="sub_container">
		<div class="container">

			<div id="submenu">
				<h2>
					<c:set var="menuPathByName" value="${fn:split(currMpm.menuPathByName,'>')}"/>
					<c:forEach items="${menuPathByName}" var="menuNm_1depth" varStatus="item">
						<c:if test="${item.count eq 1}">
							<c:if test="${fn:contains(menuNm_1depth, ' ') eq true}">
								<%--${fn:replace(menuNm_1depth, ' ', '<br/>')}--%>
								정부 3.0<br/>정보공개
							</c:if>
							<c:if test="${fn:contains(menuNm_1depth, ' ') eq false}">
								${menuNm_1depth}
							</c:if>
						</c:if>
					</c:forEach>
				</h2>
				<ul class="depth02">
					<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
						<c:param name="menuTarget" value="Sub" />
						<c:param name="menuType" value="2Depth" />
						<c:param name="USER_SE" value="${USER_SE_ }"></c:param>
					</c:import>
				</ul>

				<ul class="etc_link">
					<li class="elink1">
						<a href="http://gfund.cheongahmit.co.kr/" target="_blank" title="새창열림">
							<strong><em>녹색자금</em>통합<br />관리시스템</strong>
							<span>선정전형부터 결과산출까지 통합관리</span>
						</a>
					</li>
				</ul>

			</div>

			<div id="contents">
				<div class="sub_top sub_top01 ${currRootMpm.menuId}">
					<c:if test="${not empty currRootMpm.menuId}">
						<div class="navi">
							<span class="location">
								<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/sub/icon_home.png" alt="HOME(메인페이지로 이동)" />
								<c:forEach items="${menuPathByName}" var="navi" varStatus="item">
									<c:if test="${item.count ne fn:length(menuPathByName)}">
										${navi} &gt;
									</c:if>
									<c:if test="${item.count eq fn:length(menuPathByName)}">
										<strong>${navi}</strong>
									</c:if>
								</c:forEach>
							</span>
						</div>
					</c:if>

					<h2>${currMpm.menuNm}</h2>
					<span class="txt">
						<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
							<c:param name="menuTarget" value="Sub" />
							<c:param name="menuType" value="menuExplain" />
							<c:param name="USER_SE" value="${USER_SE_ }"></c:param>
						</c:import>
					</span>
				</div>

				<div id="content">
					<tiles:insertAttribute name="content" />
				</div>
			</div>

		</div>
	</div>