<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- footer start -->
<div id="footer">
	<h2><img alt="산림청 녹색사업단" src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo02.png" /></h2>
	<div class="fcont">
		<ul>
			<li><a href="/gfund/cmm/privacy.do">개인정보처리방침</a><span>|</span></li>
			<li><a href="/gfund/cmm/mailDeny.do">이메일무단수집거부</a><span>|</span></li>
			<li><a href="mailto:hyou14@kgpa.or.kr">관리자메일</a></li>
		</ul>
		<address>
			대전광역시 서구 둔산북로 121, 2층 산림청 녹색사업단 사업자등록번호 34-82-11213 대표 허경태 <br />
			운영관리 : 042-603-7305 기획홍보 : 042-603-7331 국내사업 : 042-603-7308 글로벌사업 : 042-603-7347 FAX : 042-603-7310<br />
			산림비전센터 : 서울특별시 영등포구 국회대로 82길 9 TEL : 02-782-2612 FAX : 02-782-432<br />
			산림교육팀[백두대간숲생태원] : 경상북도 상주시 공성면 웅산로 705 TEL : 054-538-0914 FAX : 054-538-0905
		</address>
		<p>Copyright ⓒ 2014 KGPA. All rights reserved.</p>
	</div>
</div>
<!-- //footer start -->
</body>
</html>