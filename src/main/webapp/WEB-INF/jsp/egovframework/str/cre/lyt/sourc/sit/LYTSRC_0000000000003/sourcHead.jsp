<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<%@ page import="egovframework.com.cmm.service.Globals" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="PUBLISH_APPEND_FREFIX"><%=Globals.getPublishAppendPrefix(request)%></c:set>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>" />
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}" />
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images" />
<c:set var="_CSS" value="${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}" />
<c:set var="_JS" value="${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}" />
<c:set var="C_JS" value="${_WEB_FULL_PATH}/template/common/js" />
<c:set var="C_CSS" value="${_WEB_FULL_PATH}/template/common/css" />
<c:set var="CURR_URL" value="<%=helper.getOriginatingRequestUri(request) %>" />

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<META NAME="description" CONTENT="녹색자금 통합관리시스템">
	<meta property="og:type" content="website">
	<meta property="og:title" content="녹색자금 통합관리시스템">
	<meta property="og:description" content="녹색자금 통합관리시스템">
	<meta property="og:image" content="http://gfund.fowi.or.kr/favicon.ico">
	<meta property="og:url" content="http://gfund.fowi.or.kr">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<link type="text/css" href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/template/common/js/jquery/ui/i18n/jquery.ui.datepicker-ko.js" charset="utf-8"></script>
	<script type="text/javascript" src="/template/common/js/xml2json.js"></script>
	<script type="text/javascript" src="/template/common/js/common.js"></script>

	<link charset="utf-8" href="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/style.css" type="text/css" rel="stylesheet" />
	<script charset="utf-8" src="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/script.js" type="text/javascript"></script>


	<c:if test="${currMpm.htmlUseAt eq 'Y'}">
		<link charset="utf-8" href="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/style.css" type="text/css" rel="stylesheet" />
		<script charset="utf-8" src="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/script.js" type="text/javascript"></script>
	</c:if>
	<c:if test="${not empty param.BBS_TMPLATID}">
		<link charset="utf-8" href="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/script.js"></script>
	</c:if>
	<c:if test="${siteInfo.mouseScrtyApplcAt eq 'Y' }">
		<script type="text/javascript">$(document).ready(function(){
			$(document).bind("contextmenu", function(e){
				return false;
			});
		});</script>
	</c:if>
	<c:if test="${siteInfo.kybrdScrtyApplcAt eq 'Y' }">
		<script type="text/javascript">$(document).ready(function(){
			$(document).bind('selectstart', function(){
				return false;
			});
			$(document).bind('dragstart', function(){
				return false;
			});
			$(document).keydown(function(e){
				if(e.ctrlKey && e.which == 65) return false;
				if(e.ctrlKey && e.which == 67) return false;
			});
		});	</script>
	</c:if>
	<c:if test="${param.isMain eq 'Y'}">
		<c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
	</c:if>
	<title><c:if test="${not empty currMpm}"><c:set var="menuArr" value="${fn:split(currMpm.menuPathByName, '>')}" /><c:forEach var="i" begin="0" end="${fn:length(menuArr)}">${menuArr[fn:length(menuArr)-i]}<c:if test="${i ne 0 and i ne fn:length(menuArr)}"> &#60; </c:if></c:forEach> | </c:if>산림청 녹색사업단</title>

	<%-- 가입자 보존기간 경고 팝업 시작 --%>
	<c:if test="${not empty USER_INFO and param.isMain eq 'Y'}">
		<c:import url="/gfund/retentionTimeAlerts.do" charEncoding="utf-8"/>
		<%-- 가입자 보존기간 경고 팝업 종료 --%>
	</c:if>
</head>
<body>
<div id="wrap">
	<!-- skipnavi start -->
	<div id="skilNavi">
		<a href="#lnb" class="skipBtn">주메뉴바로가기</a>
		<a href="#content" class="skipBtn">본문바로가기</a>
	</div>
	<!-- //skipnavi end -->

	<!-- header start -->
	<div id="header">
		<div class="header">
			<h1><a href="/index.do"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/logo.png" alt="녹색자금 통합관리시스템" /></a></h1>
			<ul class="gnb">
				<li><a href="/index.do">HOME</a></li>

				<c:if test="${empty USER_INFO}">
					<li><a href="/uat/uia/egovLoginUsr.do">로그인</a></li>
					<li><a href="/uss/umt/cmm/EgovStplatCnfirmMber.do">회원가입</a></li>
				</c:if>
				<c:if test="${not empty USER_INFO}">
					<li><a href="/uat/uia/actionLogout.do">로그아웃</a></li>
					<li><a href="/gfund/biz/bassinfo/basicInformationList.do">마이페이지</a></li>
					<li><a href="/uss/umt/cmm/EgovUserUpdateView.do">회원정보</a></li>
				</c:if>


				<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000044">공지사항</a></li>
				<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000045">Q&amp;A</a></li>
			</ul>

			<div id="lnb">
				<h2 class="hdn">주메뉴</h2>
				<ul>
					<li><a href="#" onclick="$(this).attr('href', $('div.smnu>ul>li:first>a').attr('href'));">녹색자금사업 공모</a>
						<div class="smnu">
							<ul>
								<c:import url="/msi/announcementService.do?use_at=Y" />
							</ul>
						</div>
					</li>
					<li><a href="#" onclick="$(this).attr('href', $('div.smnu>ul>li:last>a').attr('href'));">사후관리</a>
						<div class="smnu">
							<ul>
								<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000043&amp;certi=Y">사후관리</a></li>
								<li><a href="/gfund/cmm/agoPrject.do">과거 사후관리 자료실</a></li>
								<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000064&amp;certi=Y">모니터링 결과</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //header start -->