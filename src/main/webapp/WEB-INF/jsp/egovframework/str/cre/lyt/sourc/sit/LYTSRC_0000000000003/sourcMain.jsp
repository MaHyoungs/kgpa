<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- head Import --%>
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}" />
		</c:import>
	</c:when>
	<c:otherwise>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="Content-Script-Type" content="text/javascript" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<link charset="utf-8" href="${_C_CSS}/default.css" type="text/css" rel="stylesheet" />
			<script type="text/javascript" src="${_C_JS}/jquery/jquery-1.8.2.min.js"></script>
			<script type="text/javascript" src="${_C_JS}/common.js"></script>
		</head>
		<body>
	</c:otherwise>
</c:choose>
<%-- head Import --%>

<!--  main start -->
<div  class="main_container">
	<div id="content">

		<div class="msection1">
			<h3>녹색자금사업 공모</h3>
			<ul class="main_list">
				<c:import url="/msi/announcementService.do?use_at=Y&style=mainList" />
				<%--
				<li class="bg1">
					<strong>복지시설 나눔숲</strong>
					<span>(사회복지시설) </span>
					<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
					<a href="">복지시설 (사회복지시설) 나눔숲  공모하기</a>
				</li>
				<li class="bg2">
					<strong>복지시설 나눔숲</strong>
					<span>(특수교육시설) </span>
					<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
					<a href="">복지시설(특수교육시설)나눔숲  공모하기</a>
				</li>
				<li class="bg3">
					<strong>지역사회 나눔숲</strong>
					<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
					<a href="">지역사회 나눔숲  공모하기</a>
				</li>
				<li class="bg4">
					<strong>체험교육 나눔숲</strong>
					<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
					<a href="">체험교육 나눔숲  공모하기</a>
				</li>
				<li class="bg5">
					<strong>숲 체험교육</strong>
					<p>숲을 통해 서로 힐링하며 <br />함께하는 나눔숲 사업입니다.</p>
					<a href="">숲 체험교육  공모하기</a>
				</li>
				--%>
			</ul>

			<div class="paginate">
				<c:import url="/msi/announcementService.do?use_at=Y&style=mainPaginate" />
			</div>

			<div class="position">
				<button class="m_stop" >정지</button>
				<button class="m_play" >시작</button>
			</div>

		</div>

		<script type="text/javascript">
			var param = ".msection1";
			var btn = ".control, .position";
			var obj = ".main_list >li";
			popupzone(param,btn,obj,true,1000,5000,true,true);
		</script>


		<div class="msection2">
			<h3>사후관리</h3>
			<div class="bg">
				<strong>사후관리</strong>
				<p>녹색자금 사업의 사후관리를 통해 <br />더욱 푸르른 숲을 만들어갑니다.</p>
				<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000043">사후관리보기</a>
			</div>
		</div>

		<div class="msection3">
			<h3>공지사항</h3>
			<ul>
				<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
					<c:param name="tableId" value="BBSMSTR_000000000044" />
					<c:param name="itemCount" value="7" />
					<c:param name="style" value="2" />
				</c:import>
			</ul>
			<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000044" class="more">더보기 +</a>
		</div>

		<div class="banner">
			<ul>
				<%--
				<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
					<c:param name="tableId" value="BannerZone" />
				</c:import>
				--%>
				<li><a href="http://www.forest.go.kr/" target="_blank"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_banner01.gif" alt="산림청" /></a></li>
				<li><a href="http://www.bokgwon.go.kr/" target="_blank"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_banner02.gif" alt="복권위원회" /></a></li>
				<li><a href="http://www.kgpa.or.kr/" target="_blank"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_banner03.gif" alt="산림청 녹색사업단" /></a></li>
				<li><a href="http://www.fowi.or.kr/" target="_blank"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/img_banner04.gif" alt="한국산림복지진흥원" /></a></li>
			</ul>
		</div>

	</div>
</div>
<!--  main end -->


<!-- Bottom Import -->
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}" />
		</c:import>
	</c:when>
	<c:otherwise>
		</body>
		</html>
	</c:otherwise>
</c:choose>
<!-- Bottom Import -->