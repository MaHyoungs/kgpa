<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<div id="container">
	<!-- sub start -->
	<div class="sub_container">
		<tiles:insertAttribute name="content" />

		<div class="btn_top">
			<button type="button"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000003/images/btn_top.png" alt="상단으로 가기"/></button>
		</div>
	</div>
	<!-- sub end -->
</div>