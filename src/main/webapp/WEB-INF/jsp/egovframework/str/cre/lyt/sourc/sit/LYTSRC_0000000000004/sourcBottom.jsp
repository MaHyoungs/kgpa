<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}/images" />

		<!-- footer start -->
		<div id="footer">
			<div class="footer">
				<h2><img src="${_IMG}/logo02.png" alt="녹색사업단" /></h2>
				<p>Copyright ⓒ 2014 KGPA. All rights reserved.</p>
			</div>
		</div>
		<!-- footer end-->

	</div>
	<!-- wrap end -->

	</body>
</html>