<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="_IMG" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}/images" />
<c:set var="_CSS" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}" />
<c:set var="C_JS" value="/template/common/js" />
<c:set var="CURR_URL" value="<%=helper.getOriginatingRequestUri(request) %>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${C_JS}/common.js"></script>
<link charset="utf-8" href="${_CSS}/style.css" type="text/css" rel="stylesheet" />

<title>산림청 녹색사업단 한반도산림복원자료실</title>

<script type="text/javascript">
<c:if test='${not empty frdbmessage}'>
alert("${frdbmessage}" );
</c:if>

$(function(){
	/* 상세검색버튼 */
	$('.btn_sch').click(function(){
		$('#search_detail_box').toggle();
	});

	/*  20150413 자유게시판 검색버튼 */
	$('.btn_mnotice').click(function(){
		$('#mnotice_box').toggle();
	});
});

function fnCheckDate(str) {
	//var format = /^(19[7-9][0-9]|20\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
	var format = /^([1-9][0-9][0-9][0-9]|20\d{2})$/;
	if (!format.test(str)) return false;
	else return true;
}

function totalCheckForm(form) {

	if (form.userKeyword.value == '') {
		alert('검색어를 입력해야 합니다.');
		form.userKeyword.focus();
		return false;
	}

	if ( form.sDate.value && !fnCheckDate(form.sDate.value) ) {
		alert('발행년도의 시작년도는 YYYY 형식으로 입력하셔야 합니다.');
		form.sDate.focus();
		return false;
	}

	if ( form.eDate.value && !fnCheckDate(form.eDate.value) ) {
		alert('발행년도 종료년도는 YYYY 형식으로 입력하셔야 합니다.');
		form.eDate.focus();
		return false;
	}

	//alert('자료검색 시 수행시간이 오래 소요될 수 있습니다.');

	form.searchKeyword.value = encodeURIComponent(form.userKeyword.value);

	form.submit();
}
</script>

</head>
<body>
	<!-- wrap start -->
	<div id="wrap">

		<!-- skipnavi start -->
		<div id="skilNavi">
			<a href="#content" class="skipBtn">본문바로가기</a>
		</div>
		<!-- //skipnavi end -->
		
		<!-- header start -->
		<div id="header">
			<h1>
				<a href="http://kgpa.cheongahmit.co.kr/index.do"><img src="${_IMG}/logo.png" alt="산림청 녹색사업단 " /></a>
			</h1>

			<h2>
				<a href="/index.do"><img src="${_IMG}/logo2.gif" alt="한반도산림복원자료실" /></a>
			</h2>

			<ul>
				<li><a href="/uss/umt/NkrefoUserPasswordUpdateView.do">비밀번호변경</a> <span>|</span></li>
				<li><a href="/nkrefo/logout.do">로그아웃</a></li>
			</ul>
		</div>
		<!-- header end -->