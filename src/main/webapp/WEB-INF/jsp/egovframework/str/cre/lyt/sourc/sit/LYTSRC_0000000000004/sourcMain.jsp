<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>

<%-- 로그인 하지 않은 경우 에 대한 방지처리 --%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>" />
<c:set var="_IMG" value="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000004/images" />
<c:set var="_TOTAL_ACTION" value="/nkrefoSch/totalSearch.do" />
<c:set var="_VIEW_ACTION" value="/nkrefoSch/selectView.do" />
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		<c:if test="${!empty searchVO.sDate}"><c:param name="sDate" value="${searchVO.sDate}" /></c:if>
		<c:if test="${!empty searchVO.eDate}"><c:param name="eDate" value="${searchVO.eDate}" /></c:if>
		<c:if test="${!empty searchVO.dataForm}"><c:param name="dataForm" value="${searchVO.dataForm}" /></c:if>
		<c:if test="${!empty searchVO.bodyLang}"><c:param name="bodyLang" value="${searchVO.bodyLang}" /></c:if>
		<c:if test="${!empty searchVO.fileCondition}"><c:param name="fileCondition" value="${searchVO.fileCondition}" /></c:if>
		<c:if test="${!empty searchVO.sortCondition}"><c:param name="sortCondition" value="${searchVO.sortCondition}" /></c:if>
		<c:if test="${!empty searchVO.pageCondition}"><c:param name="pageCondition" value="${searchVO.pageCondition}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<!-- head Import -->
<c:import url="/nkrefoSourcHead.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>

<script type="text/javascript">
<c:if test='${empty USER_INFO.userSeCode}'>
location.href="/uat/uia/egovLoginUsr.do";
</c:if>
</script>

<script type="text/javascript">
	function fn_egov_regist() {

		if($('#nttSj').val().length == 0) {
			alert('<spring:message code="cop.nttSj" />은(는) 필수 입력값입니다');
			return false;
		}

		if($('#nttCn').val().length == 0) {
			alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
			return false;
		}

		<c:choose>
		<c:when test="${param.viewType eq 'bbsUpdate'}">
		if (!confirm('<spring:message code="common.update.msg" />')) {
			return false
		}
		</c:when>
		<c:otherwise>
		if (!confirm('<spring:message code="common.regist.msg" />')) {
			return false;
		}
		</c:otherwise>
		</c:choose>
	}
</script>

<!-- search start -->
<div id="search_box">
	<form id="totalSearchForm" name="totalSearchForm" action="${_TOTAL_ACTION}" method="post">
	<div class="search">		
		<input type="hidden" name="isMain" value="N"/>
		<input type="hidden" name="viewType" value="List"/>
		<input type="hidden" name="searchKeyword" value=""/>
		<fieldset>
			<legend>한반도산림복원자료실 검색</legend>
			<select id="searchCondition" name="searchCondition" title="항목선택">
				<option value="0" selected >전체</option>
				<option value="1" <c:if test="${param.searchCondition eq '1'}">selected</c:if> >표제</option>
				<option value="2" <c:if test="${param.searchCondition eq '2'}">selected</c:if> >저자</option>
				<option value="3" <c:if test="${param.searchCondition eq '3'}">selected</c:if> >발행자</option>
				<option value="4" <c:if test="${param.searchCondition eq '4'}">selected</c:if> >키워드</option>
			</select>
			<input type="text" class="inp" id="userKeyword" name="userKeyword" title="검색어입력" placeholder="통합검색"
					<c:choose>
					<c:when test="${empty nkrefoManageVO}">
						value="${searchVO.decodedKeyword}"
					</c:when>
					<c:otherwise>
						value="${nkrefoManageVO.decodedKeyword}"
					</c:otherwise>
				</c:choose>
					/>
			<input type="image" src="${_IMG}/btn_search.png" alt="검색" onclick="return totalCheckForm(document.totalSearchForm);" />
			<a href="#detail_search_box" class="btn_sch"><img src="${_IMG}/btn_search02.png" alt="상세검색" /></a>
		</fieldset>
	</div>

	<div id="search_detail_box">
		<fieldset>
			<legend>한반도산림복원자료실 상세검색</legend>
			<ul>
				<li>
					<strong>발행년도</strong>
					<span>
						<input type="text" id="sDate" name="sDate" class="inp date" title="발행년도 시작일자 선택 입력" placeholder="시작년도" value="${nkrefoManage.sDate}" /> 부터  &nbsp;
						<input type="text" id="eDate" name="eDate" class="inp date" title="발행년도 종료일자 선택 입력" placeholder="종료년도" value="${nkrefoManage.eDate}" /> 까지
					</span>
				</li>
				<li>
					<strong><label for="dataForm">자료형태</label></strong>
					<span>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM401" />
							<c:param name="elName" value="dataForm" />
							<c:param name="elType" value="select" />
							<c:param name="elFast" value="any" />
							<c:param name="chVal" value="${nkrefoManage.dataForm}" />
							<c:param name="emptyMsg" value="전체" />
						</c:import>
						<%--
						<select id="dataForm" name="dataForm">
							<option value="" selected>전체</option>
							<c:forEach var="dataForm" items="${dataFormList}">
								<option value="${dataForm.code}"><c:out value="${dataForm.codeNm}" /></option>
							</c:forEach>
						</select>
						 --%>
					</span>
				</li>
				<li>
					<strong><label for="bodyLang">본문언어</label></strong>
					<span>
						<c:import url="/EgovCommCodeList.do" charEncoding="UTF-8">
							<c:param name="codeId" value="COM402" />
							<c:param name="elName" value="bodyLang" />
							<c:param name="elType" value="select" />
							<c:param name="elFast" value="any" />
							<c:param name="chVal" value="${nkrefoManage.bodyLang}" />
							<c:param name="emptyMsg" value="전체" />
						</c:import>
						<%--
						<select id="bodyLang" name="bodyLang">
							<option value="" selected>전체</option>
							<c:forEach var="bodyLang" items="${bodyLangList}">
								<option value="${bodyLang.code}"><c:out value="${bodyLang.codeNm}" /></option>
							</c:forEach>
						</select>
						 --%>
					</span>
				</li>
				<li>
					<strong><label for="fileCondition">자료형식</label></strong>
					<span>
						<select id="fileCondition" name="fileCondition">
							<option value="" <c:if test="${nkrefoManage.fileCondition eq ''}">selected="selected"</c:if>>전체</option>
							<option value="DOC" <c:if test="${nkrefoManage.fileCondition eq 'DOC'}">selected="selected"</c:if>>문서</option>
							<option value="IMG" <c:if test="${nkrefoManage.fileCondition eq 'IMG'}">selected="selected"</c:if>>이미지</option>
							<option value="MOV" <c:if test="${nkrefoManage.fileCondition eq 'MOV'}">selected="selected"</c:if>>동영상</option>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="sortCondition">정렬조건</label></strong>
					<span>
						<select id="sortCondition" name="sortCondition">
							<option value="0" <c:if test="${nkrefoManage.sortCondition eq ''}">selected="selected"</c:if>>선택</option>
							<option value="1" <c:if test="${nkrefoManage.sortCondition eq '1'}">selected="selected"</c:if>>표제ASC</option>
							<option value="2" <c:if test="${nkrefoManage.sortCondition eq '2'}">selected="selected"</c:if>>표제DESC</option>
							<option value="3" <c:if test="${nkrefoManage.sortCondition eq '3'}">selected="selected"</c:if>>저자ASC</option>
							<option value="4" <c:if test="${nkrefoManage.sortCondition eq '4'}">selected="selected"</c:if>>저자DESC</option>
							<option value="5" <c:if test="${nkrefoManage.sortCondition eq '5'}">selected="selected"</c:if>>발행자ASC</option>
							<option value="6" <c:if test="${nkrefoManage.sortCondition eq '6'}">selected="selected"</c:if>>발행자DESC</option>
							<option value="7" <c:if test="${nkrefoManage.sortCondition eq '7'}">selected="selected"</c:if>>발행년ASC</option>
							<option value="8" <c:if test="${nkrefoManage.sortCondition eq '8'}">selected="selected"</c:if>>발행년DESC</option>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="pageCondition">쪽당출력건수</label></strong>
					<span>
						<select id="pageCondition" name="pageCondition">
							<option value="10" <c:if test="${nkrefoManage.pageCondition eq '10'}">selected="selected"</c:if>>10</option>
							<option value="20" <c:if test="${nkrefoManage.pageCondition eq '20'}">selected="selected"</c:if>>20</option>
							<option value="30" <c:if test="${nkrefoManage.pageCondition eq '30'}">selected="selected"</c:if>>30</option>
							<option value="40" <c:if test="${nkrefoManage.pageCondition eq '40'}">selected="selected"</c:if>>40</option>
							<option value="50" <c:if test="${nkrefoManage.pageCondition eq '50'}">selected="selected"</c:if>>50</option>
							<option value="100" <c:if test="${nkrefoManage.pageCondition eq '100'}">selected="selected"</c:if>>100</option>
						</select>
					</span>
				</li>
			</ul>

			<%-- 
			<div class="btn_c">
				<button type="submit" class="bbtn1" onclick="return detailCheckForm(document.detailSearchForm);">검색</button>
			</div>
			--%>
		</fieldset>		
	</div>
	</form>
</div>
<!-- //search end -->

<c:choose>
	<c:when test="${isMain eq 'Y'}">
	<!-- main start -->
	<div id="container" class="main_container">
		<div id="content">
	
			<p class="slogan">
				녹색의 <strong>꿈</strong>으로 녹색의 <strong>미래</strong>로<br />
				<span>Better Life Bright Future with Forest</span>
			</p>
			<div class="mnotice">
					<a href="#mnotice_box" class="btn_mnotice"><img src="${_IMG}/btn_mbbs.png" alt="자유게시판" /></a>
					<div id="mnotice_box">
					<h3>자유게시판</h3>
					<ul>
						<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
							<c:param name="tableId" value="BBSMSTR_000000000052" />
							<c:param name="commentUseAt" value="Y" />
							<c:param name="itemCount" value="7" />
							<c:param name="listTag" value="Y" />
							<c:param name="style" value="99" />
						</c:import>
					</ul>
					<a href="/cop/bbs/selectNkrefoBoardList.do?bbsId=BBSMSTR_000000000052&viewType=bbsList" class="more"><img src="${_IMG}/btn_more.png" alt="더보기" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- main end -->
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${param.viewType eq 'bbsList'}">
				<div id="container">
					<div id="content">
						<div class="total">
							TOTAL <strong>${paginationInfo.totalRecordCount}</strong>건 <span>|</span>
							현재페이지 <strong>${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}
						</div>

						<div class="bss_list">
							<table summary="${brdMstrVO.bbsNm } 목록을 나타낸표로 번호, 제목, 작성자, 파일, 작성일자, 조회수 항목을 제공하고있습니다">
								<caption>${brdMstrVO.bbsNm } 목록</caption>
								<colgroup>
									<col width="10%" />
									<col width="*" />
									<col width="13%" />
									<col width="13%" />
									<col width="10%" />
									<col width="10%" />
								</colgroup>
								<thead>
									<tr>
										<th class="num" scope="col">
											<spring:message code="cop.nttNo"/>
										</th>
										<c:if test="${not empty brdMstrVO.ctgrymasterId}">
											<th class="class" scope="col">
												<spring:message code="cop.category.view"/>
											</th>
										</c:if>
										<th class="tit" scope="col">
											<spring:message code="cop.nttSj"/>
										</th>
										<th class="writer" scope="col">
											<spring:message code="cop.ntcrNm"/>
										</th>
										<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
											<th class="file" scope="col">
												<spring:message code="cop.listAtchFile"/>
											</th>
										</c:if>
										<th class="date" scope="col">
											<spring:message code="cop.frstRegisterPnttm"/>
										</th>
										<th class="hits" scope="col">
											<spring:message code="cop.inqireCo"/>
										</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="result" items="${resultList}" varStatus="status">
										<c:url var="viewUrl" value="/cop/bbs/selectNkrefoBoardArticle.do">
											<c:param name="nttNo" value="${result.nttNo}"/>
											<c:param name="pageIndex" value="${searchVO.pageIndex}"/>
											<c:param name="viewType" value="bbsDetail"/>
											<c:param name="bbsId" value="BBSMSTR_000000000052" />
										</c:url>
										<tr>
											<td class="num">
												<c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}"/>
											</td>
											<c:if test="${result.useAt eq 'Y'}">
												<td class="tit">
													<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
													<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
														<c:choose>
															<c:when test="${IS_MOBILE }">
																<em class="boardrenum"><c:out value="${result.commentCount}"/></em>
															</c:when>
															<c:when test="${result.commentCount eq 0}">
																<em class="boardrenumno">[<c:out value="${result.commentCount}"/>]
																</em>
															</c:when>
															<c:otherwise>
																<em class="boardrenum">[<c:out value="${result.commentCount}"/>]
																</em>
															</c:otherwise>
														</c:choose>
													</c:if>
												</td>
											</c:if>
											<c:if test="${result.useAt eq 'N'}">
												<td class="ttt">
													<a href="<c:out value="${viewUrl}" escapeXml="true"/>" class="notice_ti"><c:out value="${result.nttSj}"/></a>
													<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
														<c:choose>
															<c:when test="${IS_MOBILE }">
																<em class="boardrenum"><c:out value="${result.commentCount}"/></em>
															</c:when>
															<c:when test="${result.commentCount eq 0}">
																<em class="boardrenumno">[<c:out value="${result.commentCount}"/>]
																</em>
															</c:when>
															<c:otherwise>
																<em class="boardrenum">[<c:out value="${result.commentCount}"/>]
																</em>
															</c:otherwise>
														</c:choose>
													</c:if>
												</td>
											</c:if>
											<td class="writer">
												<c:out value="${result.ntcrNm}"/>
											</td>
											<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
												<td class="file">
													<c:choose>
														<c:when test="${not empty result.atchFileId}">
															<img src="/str/cre/bbs/tmplat/BBSTMP_0000000000001/images/ico_file.gif" alt="<spring:message code="cop.listAtchFile"/>"/>
														</c:when>
														<c:otherwise>
															<c:if test="${not IS_MOBILE }">-</c:if>
														</c:otherwise>
													</c:choose>
												</td>
											</c:if>
											<td class="date">
												<fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/>
											</td>
											<td class="hits">
												<c:out value="${result.inqireCo}"/>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

							<c:if test="${not empty USER_INFO.id}">
								<div class="btn_all">
									<div class="fR">
										<c:url var="addBoardArticleUrl" value="/cop/bbs/addNkrefoBoardArticle.do">
											<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
											<c:param name="pageIndex" value="${searchVO.pageIndex}" />
											<c:param name="viewType" value="bbsInsert" />
										</c:url>
										<a href="${addBoardArticleUrl}"><img src="/template/manage/images/btn/btn_write.gif" alt="글쓰기"/></a>
									</div>
								</div>
							</c:if>

							<div id="paging">
								<c:url var="pageUrl" value="/cop/bbs/selectNkrefoBoardList.do">
									<c:param name="viewType" value="bbsList" />
									<c:param name="bbsId" value="BBSMSTR_000000000052" />
								</c:url>
								<c:set var="pagingParam">
									<c:out value="${pageUrl}" escapeXml="true"/>
								</c:set>
								<c:if test="${not empty paginationInfo}">
									<ul>
										<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
									</ul>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</c:when>
			<c:when test="${param.viewType eq 'bbsDetail'}">
				<div id="container">
					<div id="content">
						<div class="board_view">
							<dl class="tit_view">
								<dt>
									<spring:message code="cop.nttSj" />
								</dt>
								<dd>
									<c:out value="${board.nttSj}" />
								</dd>
							</dl>
							<c:if test="${!empty brdMstrVO.ctgrymasterId}">
								<dl class="info_view">
									<dt>
										<spring:message code="cop.category.view" />
									</dt>
									<dd>
										<c:out value="${board.ctgryNm}" />
									</dd>
								</dl>
							</c:if>

							<dl class="info_view2">
								<dt class="writer">
									<spring:message code="cop.ntcrNm" />
								</dt>
								<dd class="writer">
									<c:out value="${board.ntcrNm}" />
								</dd>
								<dt>
									<spring:message code="cop.frstRegisterPnttm" />
								</dt>
								<dd>
									<fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy.MM.dd" />
								</dd>
								<dt>
									<spring:message code="cop.inqireCo" />
								</dt>
								<dd>
									<c:out value="${board.inqireCo}" />
								</dd>
							</dl>
							<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
								<dl class="file_view">
									<dt>
										<spring:message code="cop.atchFileList" />
									</dt>
									<dd>
										<c:if test="${not empty board.atchFileId}">
											<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
												<c:param name="param_atchFileId" value="${board.atchFileId}" />
												<c:param name="imagePath" value="${_IMG }" />
											</c:import>
										</c:if>
									</dd>
								</dl>
							</c:if>
							<div class="view_cont">
								<c:out value="${fn:replace(board.nttCn, newLineChar, '<br/>')}" escapeXml="false" />
							</div>
						</div>

						<div class="btn_all">
							<div class="fR">
								<c:if test="${(board.frstRegisterId eq USER_INFO.id) or (USER_SE_CODE >= 10)}">
									<c:url var="forUpdateBoardArticleUrl" value="/cop/bbs/forUpdateNkrefoBoardArticle.do">
										<c:param name="bbsId" value="${board.bbsId}" />
										<c:param name="nttNo" value="${board.nttNo}" />
										<c:param name="pageIndex" value="${searchVO.pageIndex}" />
										<c:param name="viewType" value="bbsUpdate" />
									</c:url>
										<span class="bbtn"><a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><spring:message code="button.update" /></a></span>

									<c:url var="deleteBoardArticleUrl" value="/cop/bbs/deleteNkrefoBoardArticle.do">
										<c:param name="bbsId" value="${board.bbsId}" />
										<c:param name="nttNo" value="${board.nttNo}" />
										<c:param name="pageIndex" value="${searchVO.pageIndex}" />
										<c:param name="viewType" value="bbsList" />
									</c:url>
										<span class="bbtn"><a id="btnBbsDelete" href="<c:out value="${deleteBoardArticleUrl}"/>"><spring:message code="button.delete" /></a></span>
								</c:if>

								<c:url var="selectBoardListUrl" value="/cop/bbs/selectNkrefoBoardList.do">
									<c:param name="bbsId" value="${brdMstrVO.bbsId}" />
									<c:param name="pageIndex" value="${searchVO.pageIndex}" />
									<c:param name="viewType" value="bbsList" />
								</c:url>
								<span class="bbtn"><a href="<c:out value="${selectBoardListUrl}"/>"><spring:message code="button.list" /></a></span>
							</div>
						</div>
						<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
							<c:import url="/cop/bbs/selectNkrefoCommentList.do" charEncoding="utf-8">
								<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
								<c:param name="sourcId" value="${brdMstrVO.sourcId}" />
								<c:param name="siteId" value="${siteInfo.siteId}" />
							</c:import>
						</c:if>
					</div>
				</div>
			</c:when>
			<c:when test="${(param.viewType eq 'bbsInsert') or (param.viewType eq 'bbsUpdate')}">
				<c:set var="_IMG" value="${LytFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images" />
				<c:choose>
					<c:when test="${param.viewType eq 'bbsInsert' }">
						<c:set var="_ACTION" value="/cop/bbs/insertNkrefoBoardArticle.do"/>
					</c:when>
					<c:when test="${param.viewType eq 'bbsUpdate' }">
						<c:set var="_ACTION" value="/cop/bbs/updateNkrefoBoardArticle.do"/>
					</c:when>
				</c:choose>

				<script type="text/javascript" src="/template/common/js/board.js"></script>
				<script type="text/javascript" src="/template/common/js/jquery/jquery.form.min.js"></script>

				<div id="container">
					<div id="content">
						<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
							<input type="hidden" id="posblAtchFileNumber_nttCn" name="posblAtchFileNumber_nttCn" value="${brdMstrVO.posblAtchFileNumber}" />
							<input type="hidden" id="posblAtchFileSize_nttCn" name="posblAtchFileSize_nttCn" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
							<input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}" />
							<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>" />
							<input type="hidden" name="viewType" value="bbsList" />
							<c:if test="${param.viewType eq 'bbsInsert' }"><input type="hidden" name="registAction" value="regist" /></c:if>
							<c:if test="${param.viewType eq 'bbsUpdate' }">
								<input type="hidden" name="registAction" value="updt" />
								<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>" />
							</c:if>
							<form:hidden path="nttNo" />
							<form:hidden path="ctgryId" />
							<form:hidden path="ordrCode" />
							<form:hidden path="ordrCodeDp" />
							<form:hidden path="atchFileId" />

						<div class="board_write">
							<dt>
								<label for="nttSj"><spring:message code="cop.nttSj" /></label>
							</dt>
							<dd>
								<form:input path="nttSj" cssClass="inp tit" />
							</dd>
							<dt>
								<label><spring:message code="cop.ntcrNm" /></label>
							</dt>
							<c:choose>
								<c:when test="${param.viewType eq 'bbsUpdate' }">
									<dd>
										<strong><c:out value="${board.ntcrNm }" /></strong>
										<c:if test="${not empty board.frstRegisterId }">(<c:out value="${board.frstRegisterId }" />)</c:if>
									</dd>
								</c:when>
								<c:otherwise>
									<dd>
										<strong><c:out value="${USER_INFO.name }" /></strong>
										<c:if test="${not empty USER_INFO.id }">(<c:out value="${USER_INFO.id }" />)</c:if>
									</dd>
								</c:otherwise>
							</c:choose>

							<dt class="hdn">
								<label for="nttCn"><spring:message code="cop.nttCn" /></label>
							</dt>
							<dd class="write_cont">
								<div class="commediter">
									<form:textarea path="nttCn" rows="10" cssStyle="width:100%;" />
									<br />
									<form:errors path="nttCn" />
								</div>
							</dd>
						</div>
						</form:form>

						<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
							<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
								<c:param name="editorId" value="nttCn" />
								<c:param name="estnAt" value="N" />
								<c:param name="param_atchFileId" value="${board.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }" />
								<c:param name="potalService" value="Y" />
							</c:import>
							<noscript>
								<label for="egovComFileUploader"><spring:message code="cop.atchFileList" /></label>
								<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
							</noscript>
						</c:if>
						<div class="btn_c">
							<c:choose>
								<c:when test="${param.viewType eq 'bbsInsert'}">
									<span class="bbtn_bg1">
										<button type="button" onclick="$('#board').submit();">
											<spring:message code="button.create" />
										</button>
									</span>
								</c:when>
								<c:when test="${param.viewType eq 'bbsUpdate'}">
									<span class="bbtn_bg1">
										<button type="button" onclick="$('#board').submit();">
											<spring:message code="button.update" />
										</button>
									</span>
								</c:when>
							</c:choose>
							<c:url var="selectBoardListUrl" value="/cop/bbs/selectNkrefoBoardList.do">
								<c:param name="bbsId" value="${brdMstrVO.bbsId}" />
								<c:param name="pageIndex" value="${searchVO.pageIndex}" />
								<c:param name="viewType" value="bbsList" />
							</c:url>
							<span class="bbtn_bg2"><a href="<c:out value="${selectBoardListUrl}"/>"><spring:message code="button.reset" /></a></span>
						</div>

						<script type="text/javascript">
							(function(){
								var progress=$('.progress');
								var bar = $('.bar');
								$('#boardFileAjaxForm').ajaxForm({
									dataType: 'json',
									beforeSend: function(){
										progress.show();
										var percentVal='0%';
										progress.width(percentVal);
										bar.html(percentVal);
									},
									uploadProgress: function(event, position, total, percentComplete){
										var percentVal=percentComplete-1 + '%';
										progress.width(percentVal);
										bar.html(percentVal);
									},
									success: function(json){
										if(json.rs == 'countOver'){
											$('.progress').hide();
											$('.progress').width("0%");
											$('.bar').html("0%");
											alert("첨부파일은 최대 " + $('#maxCount').val() + "개를 초과할 수 없습니다.");
										}else if(json.rs == 'overflow'){
											$('.progress').hide();
											$('.progress').width("0%");
											$('.bar').html("0%");
											alert("첨부파일 최대 " + $('#maxSize').val() / 1024 / 1024 + "MB 를 초과하여 업로드할 수 없습니다.");
										}else if (json.rs == 'denyExt') {
											$('.progress').hide();
											$('.progress').width("0%");
											$('.bar').html("0%");
											alert("첨부할 수 없는 확장자입니다.");
										}else{
											var percentVal='100%';
											progress.width("99%");
											bar.html(percentVal);
											fnAjaxFileUploadComplete(json);
										}
									},
									complete: function(xhr){
										var file=$('#boardFileAjaxForm input[name=uploadfile]').clone();
										var file_parent = $('#boardFileAjaxForm input[name=uploadfile]').parent();
										$('#boardFileAjaxForm input[name=uploadfile]').remove();
										file_parent.prepend(file);
									}
								});
							})();
						</script>
					</div>
				</div>
			</c:when>
			<c:when test="${param.viewType eq 'List'}">
				<!-- sub start -->
				<div id="container">
					<div id="content">
	
						<div class="total">
							TOTAL <strong>${paginationInfo.totalRecordCount}</strong>건 <span>|</span>
							현재페이지 <strong>${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}
						</div>

						<div class="bss_list">
							<table summary="${searchVO.decodedKeyword} 검색결과 목록을 나타낸표로 번호, 표제, 저자, 발행자, 자료형태, 본문언어, 발형년도 항목을 제공한 표입니다">
								<caption>${searchVO.decodedKeyword} 검색결과</caption>
								<colgroup>
									<col width="10%" />
									<col width="*" />
									<col width="13%" />
									<col width="13%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">표제</th>
										<th scope="col">저자</th>
										<th scope="col">발행자</th>
										<th scope="col">자료형태</th>
										<th scope="col">본문언어</th>
										<th scope="col">발행년도</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="result" items="${resultList}" varStatus="status">
										<c:url var="viewUrl" value="${_VIEW_ACTION}${_BASE_PARAM}">
											<c:param name="isMain" value="N" />
											<c:param name="viewType" value="View" />
											<c:param name="frId" value="${result.frId}" />
											<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
										</c:url>
										<tr>
											<td><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
											<td class="tit"><a href="${viewUrl}"><c:out value="${result.title}" escapeXml="false" /></a></td>
											<td><c:out value="${result.author}" escapeXml="false" /></td>
											<td><c:out value="${result.publisher}" escapeXml="false" /></td>
											<td>
												<c:forEach var="dataForm" items="${dataFormList}">
													<c:if test="${dataForm.code eq result.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
												</c:forEach>
											</td>
											<td>
												<c:forEach var="bodyLang" items="${bodyLangList}">
													<c:if test="${bodyLang.code eq result.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
												</c:forEach>
											</td>
											<td><fmt:formatDate pattern="yyyy-MM-dd" value="${result.pubYear}"/></td>
										</tr>
									</c:forEach>
									<c:if test="${fn:length(resultList) == 0}">
										<tr>
											<td class="alC" colspan="7">자료가 없습니다.</td>
										</tr>
									</c:if>
								</tbody>
							</table>

							<div id="paging">
								<c:url var="pageUrl" value="${_TOTAL_ACTION}${_BASE_PARAM}">
									<c:param name="isMain" value="N" />
									<c:param name="viewType" value="List" />
								</c:url>
						
								<c:if test="${not empty paginationInfo}">
									<ul>
										<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
									</ul>
								</c:if>
							</div>

							<%-- 
							<div id="paging">
								<a href="" class="first" ><span>처음</span></a>
								<a href="" class="prev"><span>이전</span></a>
								<strong class="current">1</strong>
								<a href="">2</a>
								<a href="">3</a>
								<a href="">4</a>
								<a href="">5</a>
								<a href="">6</a>
								<a href="">7</a>
								<a href="">8</a>
								<a href="">9</a>
								<a href="">10</a>
								<a href="" class="next"><span>다음</span></a>
								<a href="" class="last"><span>마지막</span></a>
							</div>
							--%>
	
						</div>				
					</div>
				</div>
				<!-- sub end -->
			</c:when>
			<c:otherwise>
				<!-- sub start -->
				<div id="container"> 
					<div id="content">
	
						<table class="chart2" summary="${nkrefoManageVO.title}정보를 나타낸표로 표제, 저자, 발행자, 키워드, 자료형태, 본문언어, 요양내용, 박형년도, 첨부파일 항목을 제공한 표입니다">
							<caption>${nkrefoManageVO.title} 정보</caption>
							<colgroup>
								<col width="20%" />
								<col width="*" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">표제</th>
									<td><c:out value="${nkrefoManageVO.title}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">저자</th>
									<td><c:out value="${nkrefoManageVO.author}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">발행자</th>
									<td><c:out value="${nkrefoManageVO.publisher}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">키워드(태그) 콤마구분</th>
									<td><c:out value="${nkrefoManageVO.tag}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">자료형태</th>
									<td>
										<c:forEach var="dataForm" items="${dataFormList}">
											<c:if test="${dataForm.code eq nkrefoManageVO.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
										</c:forEach>
									</td>
								</tr>
								<tr>
									<th scope="row">본문언어</th>
									<td>
										<c:forEach var="bodyLang" items="${bodyLangList}">
											<c:if test="${bodyLang.code eq nkrefoManageVO.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
										</c:forEach>
									</td>
								</tr>
								<tr>
									<th scope="row">요약내용</th>
									<td><c:out value="${fn:replace(nkrefoManageVO.summary, newLineChar, '<br/>')}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">발행년도</th>
									<td><fmt:formatDate pattern="yyyy-MM-dd" value="${nkrefoManageVO.pubYear}"/></td>
								</tr>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<c:import url="/cmm/fms/nkrefo/selectFileInfs.do" charEncoding="utf-8">
											<c:param name="param_atchFileId" value="${nkrefoManageVO.atchFileId}" />
											<c:param name="imagePath" value="${_IMG }" />
											<c:param name="userId" value="${USER_ID}" />
											<c:param name="userSe" value="${USER_SE_CODE}" />
										</c:import>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="btn_c">
							<c:url var="listUrl" value="${_TOTAL_ACTION}${_BASE_PARAM}">
								<c:param name="isMain" value="N" />
								<c:param name="viewType" value="List" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${listUrl}" class="bbtn2">이전화면</a>
						</div>
					
					</div>
				</div>
				<!-- sub end -->
			</c:otherwise>
		</c:choose>
	</c:otherwise>	 
</c:choose>

<!-- Bottom Import -->
<c:import url="/nkrefoSourcBottom.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>
<!-- Bottom Import -->