<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- head Import -->
<c:import url="/nkrefoSourcHead.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>

<script type="text/javascript">
	function fnCheckPasswordLength(str) {
	    if (str.length < 8 || str.length > 20 ){
	            return false;
	    }
	    return true;
	}
	
	function fnCheckSpace(str){
		for (var i=0; i < str .length; i++) {
		    ch_char = str .charAt(i);
		    ch = ch_char.charCodeAt();
		        if(ch == 32) {
		            return false;
		        }
		}
	    return true;
	}
	
	function fnCheckNotKorean(koreanStr){                  
	    for(var i=0;i<koreanStr.length;i++){
	        var koreanChar = koreanStr.charCodeAt(i);
	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
	        }else{
	            //hangul finding....
	            return false;
	        }
	    }
	    return true;
	}
	
	function fnCheckTksu(str) { 
		for (var i=0; i < str.length; i++) {
		    ch_char = str .charAt(i);
		    ch = ch_char.charCodeAt();
	        if( !(ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64) || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126) ) {
	            
	        } else {
	        	return true;
	        }
		}
	    return false;            
	}
	
	function fnCheckEnglish(str){
			for(var i=0;i<str.length;i++){
				var EnglishChar = str.charCodeAt(i);
				if( !( 0x61 <= EnglishChar && EnglishChar <= 0x7A ) && !( 0x41 <= EnglishChar && EnglishChar <= 0x5A ) ) {
					
				} else {
	        	return true;
	        }
			}
			return false;
	}
	
	function fnCheckDigit(str) {  
		for (var i=0; i < str.length; i++) {
		    ch_char = str.charAt(i);
		    iValue = parseInt(ch_char);
	        if(isNaN(iValue)) {
	           
	        } else {
	        	return true;
	        }
		}
	    return false;
	}

	function checkForm(form) {
		var password = form.password2.value;
		if(!fnCheckPasswordLength(password) || !fnCheckSpace(password) || !fnCheckNotKorean(password) || !(fnCheckEnglish(password) && fnCheckDigit(password) && !fnCheckTksu(password))) {
			alert("비밀번호는 띄어쓰기 없는 영문+숫자 조합 8~20자 내에서 입력해야 합니다.");
			return false;
		}
		
		if (form.password2.value != form.cfmPassword.value) {
			alert("변경할 비밀번호란에 입력한 비밀번호와 변경할 비밀번호확인란에 입력한 비밀번호가 일치하지 않습니다.");
			return false;
		}
	}
</script>

<!-- sub start -->
<div id="container">
	<div id="content">
		
		<div class="pwd_chg">
			<form name="pwdChgForm" action="/uss/umt/NkrefoUserTempPasswordUpdate.do" method="post">
				<table class="chart2" summary="사용자정보를 나타낸표로 이름, 기관명, 아이디 항목을 제공한 표입니다">
					<caption>사용자 정보</caption>
					<colgroup>
						<col width="35%" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">이름</th>
							<td><c:out value="${userManageVO.userNm}" /></td>
						</tr>
						<tr>
							<th scope="row">기관명</th>
							<td><c:out value="${userManageVO.organNm}" /></td>
						</tr>
						<tr>
							<th scope="row">아이디</th>
							<td><c:out value="${userManageVO.userId}" /></td>
						</tr>
						<tr>
							<th scope="row"><label for="">현재 비밀번호</label></th>
							<td><input type="password" class="inp" id="password" name="password" /></td>
						</tr>
						<tr>
							<th scope="row"><label for="">변경할 비밀번호</label></th>
							<td><input type="password" class="inp" id="password2" name="password2" /></td>
						</tr>
						<tr>
							<th scope="row"><label for="">변경할 비밀번호확인</label></th>
							<td><input type="password" class="inp" id="cfmPassword" name="cfmPassword" /></td>
						</tr>
					</tbody>
				</table>

				<div class="btn_c">
					<button type="submit" class="bbtn1" onclick="return checkForm(document.pwdChgForm);">확인</button>
				</div>
			</form>	
		</div>
	
	</div>
</div>
<!-- sub  end -->

<!-- Bottom Import -->
<c:import url="/nkrefoSourcBottom.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>
<!-- Bottom Import -->