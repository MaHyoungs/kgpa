<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="_IMG" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}/images" />
<c:set var="_CSS" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}" />
<c:set var="C_JS" value="/template/common/js" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${C_JS}/common.js"></script>
<link charset="utf-8" href="${_CSS}/style.css" type="text/css" rel="stylesheet" />

<title>산림청 녹색사업단 한반도산림복원자료실</title>

<script type="text/javascript">
<c:if test='${not empty frdbmessage}'>
alert("${frdbmessage}");
</c:if>
</script>

</head>

<body>
<div id="login_wrap">
	<div class="login">

		<div class="tit_login">
			<h1>
				<a href="/index.do"><img src="${_IMG}/logo.png" alt="산림청 녹색사업단" /></a><br />
				<a href="/nkrefo/index.do" class="logo"><img src="${_IMG}/logo2.gif" alt="한반도산림복원자료실" /></a>
			</h1>
			<p>
				<img src="${_IMG}/txt_login.png" alt="Member Login" />
				<strong>
					로그인이 필요한 서비스입니다. 로그인 해주세요. <span>회원이 아닌 분은 회원가입</span> 해 주세요.
				</strong>
			</p>
		</div>

		<div class="login_border">
			<div class="login_inp">
				<form name="frmGnrlLogin" action="/nkrefo/uat/uia/actionTempLogin.do" method="post">
					<fieldset>
						<legend> 녹색사업단 한반도산림복원자료실로그인입력</legend>
						<span>
							<label for="id">아이디</label>
							<input type="text" id="id" name="id" class="inp" value="" />
						</span>
						<span>
							<label for="pwd">비밀번호</label>
							<input type="password" id="password" name="password" class="inp" value="" />
						</span>
						<input type="image" src="${_IMG}/btn_login.png"	class="btn_login" alt="로그인" />
					</fieldset>
				</form>
			</div>
		</div>
		
	</div>	
</div>

<div id="login_footer">
	<p>
		COPYRIGHT (C) 2014 BY <strong>KGPA</strong> All RIGHTS RESERVED.
	</p>
</div>

</body>
</html>