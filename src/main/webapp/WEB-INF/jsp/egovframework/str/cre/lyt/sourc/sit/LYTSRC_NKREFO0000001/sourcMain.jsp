<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<c:set var="_IMG" value="${LytFileStoreWebPathByWebFile}sit/${lytTmplatId}/images" />
<c:set var="_TOTAL_ACTION" value="/nkrefo/sch/totalSearch.do" />
<c:set var="_VIEW_ACTION" value="/nkrefo/sch/selectView.do" />
<% pageContext.setAttribute("newLineChar", "\n"); %>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		<c:if test="${!empty searchVO.sDate}"><c:param name="sDate" value="${searchVO.sDate}" /></c:if>
		<c:if test="${!empty searchVO.eDate}"><c:param name="eDate" value="${searchVO.eDate}" /></c:if>
		<c:if test="${!empty searchVO.dataForm}"><c:param name="dataForm" value="${searchVO.dataForm}" /></c:if>
		<c:if test="${!empty searchVO.bodyLang}"><c:param name="bodyLang" value="${searchVO.bodyLang}" /></c:if>
		<c:if test="${!empty searchVO.fileCondition}"><c:param name="fileCondition" value="${searchVO.fileCondition}" /></c:if>
		<c:if test="${!empty searchVO.sortCondition}"><c:param name="sortCondition" value="${searchVO.sortCondition}" /></c:if>
		<c:if test="${!empty searchVO.pageCondition}"><c:param name="pageCondition" value="${searchVO.pageCondition}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<!-- head Import -->
<c:import url="/nkrefo/sourcHead.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>

<!-- search start -->
<div id="search_box">
	<form id="totalSearchForm" name="totalSearchForm" action="${_TOTAL_ACTION}" method="post">
	<div class="search">		
		<input type="hidden" name="isMain" value="N"/>
		<input type="hidden" name="viewType" value="List"/>
		<input type="hidden" name="searchKeyword" value=""/>
		<fieldset>
			<legend>한반도산림복원자료실 검색</legend>
			<select id="searchCondition" name="searchCondition" title="항목선택">
				<option value="0" selected >전체</option>
				<option value="1" <c:if test="${param.searchCondition eq '1'}">selected</c:if> >표제</option>
				<option value="2" <c:if test="${param.searchCondition eq '2'}">selected</c:if> >저자</option>
				<option value="3" <c:if test="${param.searchCondition eq '3'}">selected</c:if> >발행자</option>
				<option value="4" <c:if test="${param.searchCondition eq '4'}">selected</c:if> >키워드</option>
			</select>
			<input type="text" class="inp" id="userKeyword" name="userKeyword" title="검색어입력" placeholder="통합검색"
				<c:choose>
					<c:when test="${empty nkrefoManageVO}">
						value="${searchVO.decodedKeyword}"
					</c:when>
					<c:otherwise>
						value="${nkrefoManageVO.decodedKeyword}"
					</c:otherwise>
				</c:choose>
			/>
			<input type="image" src="${_IMG}/btn_search.png" alt="검색" onclick="return totalCheckForm(document.totalSearchForm);" />
			<a href="#detail_search_box" class="btn_sch"><img src="${_IMG}/btn_search02.png" alt="상세검색" /></a>
		</fieldset>
	</div>

	<div id="search_detail_box">
		<fieldset>
			<legend>한반도산림복원자료실 상세검색</legend>
			<ul>
				<li>
					<strong>발행년도</strong>
					<span>
						<input type="text" id="sDate" name="sDate" class="inp date" title="발행년도 시작일자 선택 입력" placeholder="시작년도" value="" /> 부터  &nbsp;
						<input type="text" id="eDate" name="eDate" class="inp date" title="발행년도 종료일자 선택 입력" placeholder="종료년도" value="" /> 까지
					</span>
				</li>
				<li>
					<strong><label for="dataForm">자료형태</label></strong>
					<span>
						<select id="dataForm" name="dataForm">
							<option value="" selected>전체</option>
							<c:forEach var="dataForm" items="${dataFormList}">
								<option value="${dataForm.code}"><c:out value="${dataForm.codeNm}" /></option>
							</c:forEach>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="bodyLang">본문언어</label></strong>
					<span>
						<select id="bodyLang" name="bodyLang">
							<option value="" selected>전체</option>
							<c:forEach var="bodyLang" items="${bodyLangList}">
								<option value="${bodyLang.code}"><c:out value="${bodyLang.codeNm}" /></option>
							</c:forEach>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="fileCondition">자료형식</label></strong>
					<span>
						<select id="fileCondition" name="fileCondition">
							<option value="" selected>전체</option>
							<option value="DOC">문서</option>
							<option value="IMG">이미지</option>
							<option value="MOV">동영상</option>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="sortCondition">정렬조건</label></strong>
					<span>
						<select id="sortCondition" name="sortCondition">
							<option value="0" selected>선택</option>
							<option value="1">표제ASC</option>
							<option value="2">표제DESC</option>
							<option value="3">저자ASC</option>
							<option value="4">저자DESC</option>
							<option value="5">발행자ASC</option>
							<option value="6">발행자DESC</option>
							<option value="7">발행년ASC</option>
							<option value="8">발행년DESC</option>
						</select>	
					</span>
				</li>
				<li>
					<strong><label for="pageCondition">쪽당출력건수</label></strong>
					<span>
						<select id="pageCondition" name="pageCondition">
							<option value="10" selected>10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option value="40">40</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
					</span>
				</li>
			</ul>

			<%-- 
			<div class="btn_c">
				<button type="submit" class="bbtn1" onclick="return detailCheckForm(document.detailSearchForm);">검색</button>
			</div>
			--%>
		</fieldset>		
	</div>
	</form>
</div>
<!-- //search end -->

<c:choose>
	<c:when test="${isMain eq 'Y'}">
	<!-- main start -->
	<div id="container" class="main_container">
		<div id="content">
	
			<p class="slogan">
				녹색의 <strong>꿈</strong>으로 녹색의 <strong>미래</strong>로<br />
				<span>Better Life Bright Future with Forest</span>
			</p>
		
		</div>
	</div>
	<!-- main end -->
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${param.viewType eq 'List'}">
				<!-- sub start -->
				<div id="container"> 
					<div id="content">
	
						<div class="total">
							TOTAL <strong>${paginationInfo.totalRecordCount}</strong>건 <span>|</span>
							현재페이지 <strong>${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}
						</div>

						<div class="bss_list">
							<table summary="${searchVO.decodedKeyword} 검색결과 목록을 나타낸표로 번호, 표제, 저자, 발행자, 자료형태, 본문언어, 발형년도 항목을 제공한 표입니다">
								<caption>${searchVO.decodedKeyword} 검색결과</caption>
								<colgroup>
									<col width="10%" />
									<col width="*" />
									<col width="13%" />
									<col width="13%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">표제</th>
										<th scope="col">저자</th>
										<th scope="col">발행자</th>
										<th scope="col">자료형태</th>
										<th scope="col">본문언어</th>
										<th scope="col">발행년도</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="result" items="${resultList}" varStatus="status">
										<c:url var="viewUrl" value="${_VIEW_ACTION}${_BASE_PARAM}">
											<c:param name="isMain" value="N" />
											<c:param name="viewType" value="View" />
											<c:param name="frId" value="${result.frId}" />
											<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
										</c:url>
										<tr>
											<td><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
											<td class="tit"><a href="${viewUrl}"><c:out value="${result.title}" escapeXml="false" /></a></td>
											<td><c:out value="${result.author}" escapeXml="false" /></td>
											<td><c:out value="${result.publisher}" escapeXml="false" /></td>
											<td>
												<c:forEach var="dataForm" items="${dataFormList}">
													<c:if test="${dataForm.code eq result.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
												</c:forEach>
											</td>
											<td>
												<c:forEach var="bodyLang" items="${bodyLangList}">
													<c:if test="${bodyLang.code eq result.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
												</c:forEach>
											</td>
											<td><fmt:formatDate pattern="yyyy-MM-dd" value="${result.pubYear}"/></td>
										</tr>
									</c:forEach>
									<c:if test="${fn:length(resultList) == 0}">
										<tr>
											<td class="alC" colspan="7">자료가 없습니다.</td>
										</tr>
									</c:if>
								</tbody>
							</table>

							<div id="paging">
								<c:url var="pageUrl" value="${_TOTAL_ACTION}${_BASE_PARAM}">
									<c:param name="isMain" value="N" />
									<c:param name="viewType" value="List" />
								</c:url>
						
								<c:if test="${not empty paginationInfo}">
									<ul>
										<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
									</ul>
								</c:if>
							</div>

							<%-- 
							<div id="paging">
								<a href="" class="first" ><span>처음</span></a>
								<a href="" class="prev"><span>이전</span></a>
								<strong class="current">1</strong>
								<a href="">2</a>
								<a href="">3</a>
								<a href="">4</a>
								<a href="">5</a>
								<a href="">6</a>
								<a href="">7</a>
								<a href="">8</a>
								<a href="">9</a>
								<a href="">10</a>
								<a href="" class="next"><span>다음</span></a>
								<a href="" class="last"><span>마지막</span></a>
							</div>
							--%>
	
						</div>				
					</div>
				</div>
				<!-- sub end -->
			</c:when>
			<c:otherwise>
				<!-- sub start -->
				<div id="container"> 
					<div id="content">
	
						<table class="chart2" summary="${nkrefoManageVO.title}정보를 나타낸표로 표제, 저자, 발행자, 키워드, 자료형태, 본문언어, 요양내용, 박형년도, 첨부파일 항목을 제공한 표입니다">
							<caption>${nkrefoManageVO.title} 정보</caption>
							<colgroup>
								<col width="20%" />
								<col width="*" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">표제</th>
									<td><c:out value="${nkrefoManageVO.title}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">저자</th>
									<td><c:out value="${nkrefoManageVO.author}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">발행자</th>
									<td><c:out value="${nkrefoManageVO.publisher}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">키워드(태그) 콤마구분</th>
									<td><c:out value="${nkrefoManageVO.tag}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">자료형태</th>
									<td>
										<c:forEach var="dataForm" items="${dataFormList}">
											<c:if test="${dataForm.code eq nkrefoManageVO.dataForm}"><c:out value="${dataForm.codeNm}" /></c:if>
										</c:forEach>
									</td>
								</tr>
								<tr>
									<th scope="row">본문언어</th>
									<td>
										<c:forEach var="bodyLang" items="${bodyLangList}">
											<c:if test="${bodyLang.code eq nkrefoManageVO.bodyLang}"><c:out value="${bodyLang.codeNm}" /></c:if>
										</c:forEach>
									</td>
								</tr>
								<tr>
									<th scope="row">요약내용</th>
									<td><c:out value="${fn:replace(nkrefoManageVO.summary, newLineChar, '<br/>')}" escapeXml="false"/></td>
								</tr>
								<tr>
									<th scope="row">발행년도</th>
									<td><fmt:formatDate pattern="yyyy-MM-dd" value="${nkrefoManageVO.pubYear}"/></td>
								</tr>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<c:import url="/cmm/fms/nkrefo/selectFileInfs.do" charEncoding="utf-8">
											<c:param name="param_atchFileId" value="${nkrefoManageVO.atchFileId}" />
											<c:param name="imagePath" value="${_IMG }" />
											<c:param name="userId" value="${USER_ID}" />
											<c:param name="userSe" value="${USER_SE_CODE}" />
										</c:import>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="btn_c">
							<c:url var="listUrl" value="${_TOTAL_ACTION}${_BASE_PARAM}">
								<c:param name="isMain" value="N" />
								<c:param name="viewType" value="List" />
								<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
							</c:url>
							<a href="${listUrl}" class="bbtn2">이전화면</a>
						</div>
					
					</div>
				</div>
				<!-- sub end -->
			</c:otherwise>
		</c:choose>
	</c:otherwise>	 
</c:choose>

<!-- Bottom Import -->
<c:import url="/nkrefo/sourcBottom.do" charEncoding="utf-8">
	<%-- <c:param name="isMain" value="${isMain}" /> --%>
</c:import>
<!-- Bottom Import -->