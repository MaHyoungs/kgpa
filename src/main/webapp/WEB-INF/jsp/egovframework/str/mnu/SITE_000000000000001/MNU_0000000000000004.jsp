<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="txt_bg02 line_green">
<p class="pB20"><strong class="txt_style">맑은물 공급을 위한 산림 환경 개선 사업지원 녹색사업단이 <br /> 함께 합니다.</strong></p>
</div>
<ul class="section_st1 mT20 line_green">
<li class="bg_icon01"><strong>우리는</strong><br />항상 고객의 입장에 서서 우리가 제공할 수 있는 <span class="green">최대한의 서비스 이행표준을 설정</span>하여 이행하겠습니다.</li>
<li class="bg_icon02"><strong>우리는</strong><br />업무 수행에 있어 <span class="green">고객의 의견을 최대한 정책에 반영</span>하고 맡은 임무를 충실히 수행하여 산림환경기능 증진에 앞장서겠습다.</li>
<li class="bg_icon03"><strong>우리는</strong><br /><span class="green">고객의 편에 생각하고 고객</span>에게 불편이 없도록 친절하게 일하겠습니다.</li>
<li class="bg_icon04"><strong>우리는</strong><br /><span class="green">고객만족경영 실천</span>으로 고객에게 고품질의 서비스를 제공하기 위하여 노력하겠습니다.</li>
</ul>