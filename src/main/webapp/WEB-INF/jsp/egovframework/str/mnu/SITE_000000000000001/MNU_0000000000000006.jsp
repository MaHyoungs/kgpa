<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="txt_bg03 line_green">
<p class="pB20"><strong class="txt_style">맑은물 공급을 위한 산림 환경 개선 사업지원 녹색사업단이 <br /> 함께 합니다.</strong></p>
</div>
<div class="talk pB20 line_green">
<p>우리 사업단 직원 일동은 고객여러분을 위해서 존재한다는 사실을 깊이 인식하고 위 사항들을 실천할 것을 약속드리며, <span class="green">고객의 사랑과 신뢰 속에서 고객을 위한 녹색사업단이 되도록 노력하겠습니다.</span></p>
<p>이와 관련하여 고객 여러분께서는 <span class="green">산림환경기능 개선은 물론 행정서비스가 향상될 수 있도록 지속적으로 관심</span>을 가져주시기를 부탁드립니다.</p>
<p>우리 사업단이 제공하는 서비스에 대한 불편사항이나 개선해야할 점이 있으시면, <span class="green">우편이나 홈페이지 또는 전화로 연락주시면 이를 겸허하게 수용하여 개선해 나갈 것을 약속드립니다.</span></p>
<p>민원과 관련하여 법규나 제도상 또는 다수의 고객을 위하여 불가피하게 민원인의 요구사항을 수용하지 못하는 경우에도 이해하여 주시기 바랍니다.</p>
</div>