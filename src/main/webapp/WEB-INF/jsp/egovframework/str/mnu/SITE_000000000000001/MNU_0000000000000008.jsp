<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="clean_center">
<p class="alC"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m1/txt_p.png" alt="녹색사업단의 민원서비스는 국민 여러분께 항상 열려있습니다." /></p>
<div class="pbg2">이 게시판은 민원사무처리에 관한 법률시행령 제2조 1항 3호에 의거, 민원인 성명, 전화번호 등이 분명해야 하며, 2010년 4월 26일(날짜조정)부터 민원인과 피민원인의 비밀보장을 위하여 온라인상담(클린센터) 게시판이 "비공개" 처리됨을 알려드립니다.<br /><br /> 신속하고 정확한 민원 응대를 약속 드리며, 언제나 최선을 다하겠습니다.</div>
<div class="complaint">
<ul>
<li class="bg1"><strong>민원신청</strong><span>민원신청 녹색사업단의 불편사항이나 건의사항 등이 있으시면 누구든지 상담하여 주시기 바랍니다.</span><a href="/cop/bbs/addBoardArticle.do?registAction=regist&amp;menuId=MNU_0000000000000008&amp;bbsId=BBSMSTR_000000000008">신청하기</a></li>
<li class="bg2"><strong>민원안내</strong><span>민원조회 고객님께서 상담하신 내역을 빠르게 확인 하실 수 있습니다</span><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000008&amp;menuId=MNU_0000000000000008">조회하기</a></li>
</ul>
</div>
</div>