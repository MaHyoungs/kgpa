<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon1">정보공개 청구</h3>
<p class="icon3 mB30">녹색사업단에서는 국민의 알권리를 보장하고 기관 경영에 대한 국민의 참여와 투명성을 확보하기 위하여 정보공개제도를 시행하고 있습니다.</p>
<h3 class="icon1">정보공개 책임자 지정 현황</h3>
<ul class="icon3_list section2 mB30">
<li>정보공개책임관 : 운영관리팀장 어경해 042-603-7302</li>
<li>정보공개담당 : 운영관리팀 임민우 042-603-7305</li>
</ul>
<h3 class="icon1">관련근거</h3>
<p class="icon3 mB30">공공기관의정보공개에 관한 법률&middot;시행령&middot;시행규칙.</p>
<h3 class="icon1">정보공개의 원칙</h3>
<p class="icon3 mB30">공공기관이 보유&middot;관리하는 정보는「공공기관의정보공개에 관한 법률」따라 공개하여 한다.</p>
<h3 class="icon1">개념 및 목적</h3>
<p class="icon3 mB30">우리 사업단에서 직무상 작성 또는 취득관리하고 있는 문서(전자문서 포함)&middot;도면&middot;사진&middot;필름&middot;테이프&middot;슬라이드 및 그 밖에 이에 준하는 매체 등에 기록된 사항을 청구인에 열람하게 하거나 그 사본&middot;복제물을 교부하는 것 또는「전자정부법」제2조제10호의 규정에 의한 정보통신망을 통하여 제공하는 제도로써 국민의 알권리 보장, 국민의 국정참여 확보, 국민의 신뢰성 확보, 참된 민주주의 실현, 국민의 권리와 이익보호에 기여하여 녹색사업단 고객의 만족도 제고에 그 목적을 둔다.</p>
<h3 class="icon1">정보공개 대상기관 및 의무</h3>
<p class="icon3 mB30">공공기관(국가기관, 지방자치단체, 정부투자기관관리기본법 제2조의 규정에 의한 정부투자기관, 그밖에 대통령령이 정하는 기관)은 정보의 적절한 보존과 신속한 검색이 이루어지도록 정보관리체계를 정비하고, 정보공개업무를 주관하는 부서 및 담당하는 인력을 적정하게 두고 정보통신망을 활용한 정보공개시스템 등을 구척</p>
<h3 class="icon1">관련자료 다운로드</h3>
<ul class="btn_list">
<li><a class="cbtn" href="/attachfiles/file/20130820_1.hwp"><span class="icon_note">공공기관의 정보공개에 관한 법률</span></a></li>
<li><a class="cbtn" href="/attachfiles/file/20130820_2.hwp"><span class="icon_note">시행규칙</span></a></li>
<li><a class="cbtn" href="/attachfiles/file/20130820_3.hwp"><span class="icon_note">시행령</span></a></li>
</ul>