<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<table class="chart2" summary="녹색사업단 정보공개 수수료 정보를 나타낸표로 공개대상에따른 공개방법및 수수료 항목을 제공하고 있습니다"><caption>녹색사업단 정보공개 수수료 정보</caption><colgroup> <col width="14%" /> <col width="21%" /> <col width="22%" /> <col width="21%" /> <col width="22%" /> </colgroup>
<thead>
<tr><th scope="col" rowspan="2">공개대상</th><th scope="col" colspan="4">공개방법 및 수수료</th></tr>
<tr><th scope="col">원본의 <br /> 열람&middot;시청</th><th scope="col">원본의 사본<br /> (출력물)<br /> 복제물 &middot;인화물</th><th scope="col">전자파일의 <br /> 열람 &middot;시총</th><th scope="col">전자파일의<br /> 사본(출력물)&middot;<br /> 복제물</th></tr>
</thead>
<tbody>
<tr>
<td>문서&middot; 대장 등</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1건 (10매 기준)1회 :200원</li>
<li>10매 초과시 5매마다 :100원</li>
</ul>
</td>
<td>○사본(1매 기준)
<ul class="pL10 icon3_list">
<li>A3 이상 :300원</li>
<li>1매 초과마다 :100원</li>
<li>B4 이하 :250원</li>
<li>1매 초과마다 :50원</li>
</ul>
</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1건(10매 기준)1회 :200원</li>
<li>10매 초과시 5매마다 :100원</li>
</ul>
</td>
<td>○사본(종이출력물)
<ul class="pL10 icon3_list">
<li>A3 이상 300원</li>
<li>1매 초과마다 :100원</li>
<li>B4 이하 250원</li>
<li>1매 초과마다 :50원</li>
<li></li>
</ul>
○복제
<ul class="pL10 icon3_list">
<li>1건(10매 기준)1회 :200원</li>
<li>10매 초과시 5매마다 :100원 <br /> ※ 매체비용은 별도</li>
</ul>
</td>
</tr>
<tr>
<td>도면&middot;카드 등</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1매 : 200원 &middot;1매 초과마다 100원</li>
</ul>
</td>
<td>○사본(1매 기준)
<ul class="pL10 icon3_list">
<li>A3 이상 :300원&middot;1매 초과마다 :100원</li>
<li>B4 이하 :250원</li>
<li>1매 초과마다 :50원</li>
</ul>
</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1매 : 200원 &middot;1매 초과마다 100원</li>
</ul>
</td>
<td>○사본(종이출력물)
<ul class="pL10 icon3_list">
<li>A3 이상 :300원</li>
<li>1매 초과마다 :100원</li>
<li>B4 이하 :250원</li>
<li>1매 초과마다 :50원</li>
</ul>
○복제
<ul class="pL10 icon3_list">
<li>1건(10매 기준)1회 : :200원</li>
<li>10매 초과시 5매마다 :100원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
</tr>
<tr>
<td>녹음 테이프 <br /> (오디오자료)</td>
<td>○청취
<ul class="pL10 icon3_list">
<li>1건이 1개 이상으로 이루어진 경우</li>
<li>1개(60분 기준)마다 :1,500원</li>
<li>여러 건이 1개로 이루어진 경우</li>
<li>1건(30분 기준)마다 :700원</li>
</ul>
</td>
<td>○복제
<ul class="pL10 icon3_list">
<li>1건이 1개 이상으로 이루어진 경우</li>
<li>1개마다 :5,000원</li>
<li>여러 건이 1개로 이루어진 경우</li>
<li>1건마다 :3,000원<br /> ※매체비용은 별도</li>
</ul>
</td>
<td rowspan="3">○시청&middot;청취
<ul class="pL10 icon3_list">
<li>1편 : 1,500원 30분 초과시 10분마다 :500원</li>
</ul>
</td>
<td rowspan="3">○복제
<ul class="pL10 icon3_list">
<li>1건(700MB 기준)마다 :5,000원</li>
<li>700MB 초과시 350MB :2,500원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
</tr>
<tr>
<td>녹화 테이프 <br /> (비디오자료)</td>
<td>○시청
<ul class="pL10 icon3_list">
<li>1편이 1롤 이상으로 이루어진 경우 1롤(60분 기준)마다 :1,500원</li>
<li>여러 편이 1롤로 이루어진 경우</li>
<li>1편(30분 기준)마다 :700원</li>
</ul>
</td>
<td>○복제
<ul class="pL10 icon3_list">
<li>1편이 1롤 이상으로 이루어진 경우</li>
<li>1롤마다 :5,000원</li>
<li>여러 편이 1롤로 이루어진 경우</li>
<li>1편마다 :3,000원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
</tr>
<tr>
<td>영화 필름</td>
<td>○시청
<ul class="pL10 icon3_list">
<li>1편이 1캔 이상으로 이루어진 경우 &middot;1캔(60분 기준)마다 :3,500원</li>
<li>여러 편이 1캔으로 이루어진 경우</li>
<li>1편(30분 기준)마다 :2,000원</li>
</ul>
</td>
<td>　</td>
</tr>
<tr>
<td>슬라이드</td>
<td>○시청
<ul class="pL10 icon3_list">
<li>1컷마다 200원</li>
</ul>
</td>
<td>○복제
<ul class="pL10 icon3_list">
<li>1컷마다 3,000원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
<td>○시청
<ul class="pL10 icon3_list">
<li>1컷마다 200원</li>
</ul>
</td>
<td>　</td>
</tr>
<tr>
<td>마이크로 필름</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1건(10컷 기준)1회 : 500원</li>
<li>10컷 초과시 1컷마다 :100원</li>
</ul>
</td>
<td>○사본(출력물 : 1매 기준)
<ul class="pL10 icon3_list">
<li>A3 이상 :300원 &middot;1매 초과마다 :200원</li>
<li>B4 이하 :250원</li>
<li>1매 초과마다 :150원</li>
</ul>
○복제
<ul class="pL10 icon3_list">
<li>1롤마다 :1,000원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
<td>　</td>
<td>　</td>
</tr>
<tr>
<td>사진&middot;필름</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1매 : 200원 &middot;1매 초과마다 50원</li>
</ul>
</td>
<td>○인화(필름)
<ul class="pL10 icon3_list">
<li>1컷마다 :500원</li>
<li>1매 초과마다 3"&times;5" 200원 5"&times;7" 300원 8"&times;10" 400원</li>
</ul>
○복제(필름)
<ul class="pL10 icon3_list">
<li>1컷마다 :6,000원<br /> ※ 매체비용은 별도</li>
</ul>
</td>
<td>○열람
<ul class="pL10 icon3_list">
<li>1매 : 200원 &middot;1매 초과마다 50원</li>
</ul>
</td>
<td>○사본(종이출력물)
<ul class="pL10 icon3_list">
<li>1컷 : 250원</li>
<li>1매 초과마다3"&times;5" 50원 5"&times;7" 100원8"&times;10" 150원</li>
</ul>
○복제
<ul class="pL10 icon3_list">
<li>1건(1MB 기준)1회 :200원</li>
<li>1MB 초과시 0.5MB마다 :100원<br />※ 매체비용은 별도</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p class="mB40">※ 전자우편을 통해 공개하는 경우 전자파일의 복제의 경우를 적용하여 수수료 산정</p>
<h3 class="icon1">비용부담</h3>
<div class="section1 icon3 mB40">정보의 공개 및 우송 등에 소요되는 비용은 수수료 「전자금융거래법」 제2조제11호에 따른 전자지급수단 또는 수입인지(국가기관에 내는 경우로 한정한다) 또는 수입증지(지방자치단체에 내는 경우로 한정한다) 와 우편요금(공개되는 정보의 사본&middot;출력물&middot;복제물 또는 인화물을 우편으로 송부하는 경우)으로 구분하되, 수수료의 금액은 아래와 같다. 다만, 「공공기관의 정보공개에 관한 법률 시행규칙」제17조에 따라 관계법령에 의거 수수료의 금액을 달리 정할 수 있으며 수수료 감면 또한 이에 따른다.</div>
<h3 class="icon1">기타사항은「공공기관의 정보공개에 관한 법률 및 사업단 정보공개지침」에 따른다.</h3>