<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010101.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">소외계층녹색복지증진</h3>
<p class="icon3 section1 mB30">노약자, 장애인 등 사회적 약자층을 위한 숲 조성으로 계층간의 갈등 해소 및 녹색복지국가 건설에 이바지하고 있습니다.</p>
<h3 class="icon1">녹색건강기반 조성</h3>
<p class="icon3 section1 mB30">생활권역 내 일반시민들을 위한 숲 조성으로 정부의 국정지표인 저탄소 녹색성장에 이바지하고 있습니다..</p>
<h3 class="icon1">트레일 조성</h3>
<p class="icon3 section1 mB30">수직적인 등산 개념을 탈피하여 숲길&middot;마을길&middot;논길 등을 걸으면서 역사&middot;문화&middot;생태적 체험을 할 수 있도록 조성하였습니다.</p>
<h3 class="icon1">기타사업</h3>
<p class="icon3 section1 mB30">옥상녹화&middot;비오톱&middot;학교숲 등 조성과 생활환경림조성사업의 조사&middot;연구 등 사업을 하고 있습니다.</p>
</div>
</div>