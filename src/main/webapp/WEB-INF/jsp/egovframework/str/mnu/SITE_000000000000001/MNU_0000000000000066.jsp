<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010102.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">소외계층 녹색복지증진사업</h3>
<ul class="icon3_list section1">
<li>사회적 약자층이 거주하거나 이용하고 있는 사회복지시설 내에 숲조성, 옥상녹화, 담장벽면 녹화, 원예치료실, 실내정원, 이동식 정원 등을 조성하여 생활환경을 개선하고 정서적 안정감을 유도하여 육체적 건강에 기여함으로써 소외계층이 누리는 공공복지를 실현</li>
<li>사업단은 저소득 장애인, 노인, 아동 등이 상시 이용할 수 있는 소외계층 녹색복지증진 사업을 지속적으로 추진하여 자연적인 숲의 건강기능을 통한 불안감, 우울감 등을 향상시켜 삶의 질을 개선할 수 있도록 지원규모를 확대해 나갈 계획입니다.</li>
</ul>
</div>
</div>