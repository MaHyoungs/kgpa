<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010103.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">녹색건강 기반조성사업</h3>
<ul class="icon3_list section1">
<li>저탄소 녹색성장의 국가정책에 맞춰 탄소저장을 극대화 할 수 있는 녹색공간을 창출하고 도심과 연결되는 녹지축을 형성하여 녹지 네트워크를 구축함으로써 아름다운 경관 및 쉼터를 제공하고 국민과 함께하는 녹색복지 실현</li>
<li>사업단은 도시민 등이 쉽게 접근할 수 있는 일정규모의 국ㆍ공유지 등에 모델숲, 테마숲을 조성하여 도시경관 향상 및 정서함양을 도모하고 국민의 삶의 질 향상을 위하여 녹색건강 기반조성 사업을 지속적으로 추진해 나갈 계획입니다.</li>
</ul>
</div>
</div>