<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010104.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">트레일코스 조성사업</h3>
<ul class="icon3_list section1">
<li>트레일이란 좁은 산길, 밟아 다져진 길로 안내도와 지표가 정비된 좁은 등산로의 의미를 가지며, 산업사회가 시작된 이후, 경관이 아름다운 지역을 걷는 것이 유행했고 2차대전 이후 자연을 보전하려는 움직임이 계기가 되어 국가에서 트레일을 정비하고 국민들이 걸을 수 있는 길을 마련</li>
<li>녹색사업단은 3개도(전남, 전북, 경남), 5개시군(구례, 남원, 하동, 산청, 함양) 16개 읍면, 80여개 마을을 지나는 지리산길 300여km를 연결하여 자연과 문화, 사람이 어우러진 국내최초의 장거리 보도길인 환지리산 트레일코스와 자연풍경이 수려하고 지역의 특성을 살릴 수 있는 상징성 있는 일반 트레일코스를 조성하여, 일반 국민이 손쉽게 이용할 수 있는 길을 만들어갈 계획입니다.</li>
</ul>
</div>
</div>