<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010201.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">숲체원 조성 지원</h3>
<p class="icon3 section1 mB30">청소년 및 소외계층을 위한 숲체험 교육을 목적으로 강원 횡성에 숲체원을 2003년부터 5개년에 걸쳐 조성하고, 국내 최대 규모의 산림교육센터로서 운영되도록 지원하고 있습니다.</p>
<h3 class="icon1">녹색체험교육</h3>
<p class="icon3 section1 mB30">청소년과 다문화가정ㆍ장애인ㆍ게임중독자 등 사회적 약자층과 사회인 등 일반인을 대상으로 숲을 통한 정서적 안정과 숲에 대한 소중함을 인식할 수 있는 사업을 진행하고 있습니다.</p>
<h3 class="icon1">산림문화홍보</h3>
<p class="icon3 section1 mB30">기후변화ㆍ사막화현상 등 점점 지쳐가고 있는 지구의 자연환경을 지키기 위한 산림의 중요성･보전 의식 고취를 위한 사업을 진행하고 있습니다.</p>
</div>
</div>