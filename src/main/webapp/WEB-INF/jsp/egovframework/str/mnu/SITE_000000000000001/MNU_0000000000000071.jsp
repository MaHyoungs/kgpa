<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010202.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">숲체원 운영 지원사업</h3>
<ul class="icon3_list section1">
<li>강원도 횡성군에 위치한 숲체원은 청소년 및 소외계층의 녹색교육을 목적으로 건립된 산림교육의 메카이자 산림환경을 기반으로 한 국민의 복지사업의 중심지 역할을 활용할 목적으로 2003년도에 착공하여 2006년도에 완공되었으며,</li>
<li>사회적 약자인 소외계층을 위하여 계층별로 다양한 체험교육을 통한 자연체험을 할 수 있도록 설계하여 완공된 국내 유일의 숲체험 교육장소로서 녹색복지 실현을 위하여 차별화된 숲체험 프로그램을 운영하고 체험교육 기회를 제공하기 위하여 건립되었습니다.</li>
<li>숲체원은 장애인등이 산림의 쾌적함을 느낄 수 있도록 산의 정상까지 연결되는 데크로드가 설치되어 있으며, 일반 국민 누구나 이용할 수 있도록 숙박시설, 캠프장, 자연학습장 등으로 구성되어 있습니다.</li>
</ul>
</div>
</div>
<div class="btn_box">
<p>숲체원 건립 및 운영에 관한 세부 사항은 아래의 버튼을 클릭하시면 볼수 있습니다</p>
<ul class="btn_l">
<li><a class="cbtn2" href="http://www.soopchewon.or.kr/introduce/sub01_04_01.php" target="_blank"><span class="icon_chk">사업추진 현황</span></a></li>
<li><a class="cbtn2" href="http://www.soopchewon.or.kr/use_info/sub03_01_01_01.php" target="_blank"><span class="icon_chk">숲체원 시설안내</span></a></li>
<li><a class="cbtn2" href="http://www.soopchewon.or.kr/experience/sub02_01_01.php" target="_blank"><span class="icon_chk">체험프로그램</span></a></li>
<li><a class="cbtn2" href="http://www.soopchewon.or.kr/ezboard.php?GID=root&amp;BID=board07" target="_blank"><span class="icon_chk">숲체험 홍보영상</span></a></li>
</ul>
</div>