<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010203.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">녹색체험 교육사업</h3>
<ul class="icon3_list section1">
<li>장애인, 차상위계층, 여성가장 등 저소득층, 소년소녀가장 경제적, 신체적 어려움을 겪고 있는 소외계층이 산림체험활동을 통해 삶에 대한 희망과 의지를 고취시키고, 정상적인 삶을 영위하기 위한 숲치유 프로그램 제공</li>
<li>숲에서의 집단 활동 및 자기성찰을 통한 의사소통, 대인관계, 학습태도개선 등 숲속 활동을 통한 수혜대상의 심리적 개선을 위하여 객관적 검토를 기초로 숲치유 활동을 제공함으로써 사회적 문제 해결을 위한 공익적 기능을 수행하는데 기여하고 있습니다.</li>
</ul>
</div>
</div>