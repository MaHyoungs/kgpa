<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05010204.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">산림문화 홍보사업</h3>
<ul class="icon3_list section1">
<li>생물의다양성보전, 기후완화, 수원함양, 경관보전 등 숲이 주는 공익적 가치를 국민에게 알리는 산림문화 홍보사업은 다양한 종류의 컨텐츠를 개발, 보급하고, 홍보활동을 통해 녹색문화 확산을 도모하며, 산림환경의 중요성을 직접 체험할 수 있도록 다양한 산림정보를 제공</li>
<li>숲과 관련된 공모전, 다큐멘터리 제작, 캠페인 등 녹색성장을 주도하는 산림의 공익적 역할에 대한 국민의 참여와 산림의 중요성에 대한 인식을 높일 수 있도록 노력하고 있습니다.</li>
</ul>
</div>
</div>