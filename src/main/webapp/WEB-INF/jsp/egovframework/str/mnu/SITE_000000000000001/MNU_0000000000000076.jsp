<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="img_cont">
<h2 class="icon mB20">해외조림사업</h2>
<div class="img"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020101.gif" alt="" /></div>
<div class="cont">
<h3 class="icon1">지속가능한 산림경영 및 저탄소 녹색성장</h3>
<p class="icon3 section1 mB30">현재까지의 해외산림사업은 원목의 공급원을 확보하여 제품의 가격경쟁력을 제고한다는 차원으로 추진되었으나, 앞으로의 해외산림사업은 기후변화협약에서 제시하고 있는 요건을 충족시키기 위한 지속가능한 산림경영 및 저탄소 녹색성장을 목적으로 전개될 것입니다.</p>
<h3 class="icon1">2만ha 시범조림 및 목재펠릿 생산 시설 설립</h3>
<p class="icon3 section1 mB30">2009년 3월 한-인도네시아 정상회담시 체결된 「목재 바이오매스 에너지 산업육성 MOU」의 후속조치로서 기업체 투자유치 및 기술 선도 기반을 마련하기 위해 인도네시아에 2만ha 규모의 시범조림사업과 목재펠릿 생산사업을 추진하고 있습니다.</p>
<h3 class="icon1">목재바이로매스 공급 전초기지 역활</h3>
<p class="icon3 section1 mB30">시범단지림은 목재바이오매스 공급 전초기지 역할을 수행하게 되며, 시범사업과 연계한 연구 &middot; 기술개발, 교육장소로 활용될 것이며 관련 전문가 양성과 20만ha 조림 등 목재 바이오매스에너지산업에 민간투자를 유도할 것입니다.</p>
</div>
</div>