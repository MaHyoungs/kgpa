<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">해외산림투자업체 지원</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020102.gif" alt="" /></span>
<div class="fL w400">
<p class="mL20"><strong class="txt_style">해외산림자원개발사업의 성공 잠재력 향상과 맞춤형 해외 정보 제공,</strong></p>
<p class="mL20 mT20"><strong>해외산림자원개발사업의 활성화를 위하여 해외조림 적지를 조사하고 확보하며 조림기술 및 투자절차, 전략 등을 컨설팅하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">맞춤형 해외산림정보 제공</h3>
<p class="icon3 section1 mB30">해외산림자원개발사업의 성공 잠재력 향상과 다양해진 해외산림 사업에 대한 맞춤형 정보를 제공하기위해 해외산림정보를 제공하기 위해 제반 서비스를 확충하여 해외산림자원개발사업의 활성화에 더욱 기여하겠습니다.</p>
<h3 class="icon1">해외산림자원개발 진출 컨설팅</h3>
<p class="icon3 section1 mB30">해외산림자원개발 투자 정보 제공 및 상담, 해외산림자원개발 사업계획신고 등에 대한 자문, 국제 목재시장 및 목제품에 대한 가격조사 및 전망, 해외산림자원개발 홍보자료 발간 및 배포, 국제 산림자원개발 현황 조사 등 다양한 방법을 통해 해외산림자원개발 컨설팅을 실시하고 있습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020102_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020102_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020102_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020102_4.gif" alt="" /></span></div>