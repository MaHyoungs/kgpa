<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">산림정보분석 및 조림성과 모니터링</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020103.gif" alt="" /></span>
<div class="fL w400"><strong class="mL20">위성영상 분석 기법을 활용하여</strong>
<p class="mL20 mT20"><strong class="txt_style">정보가 부족한 해외조림 대상지에 대해 조림 적지를 선별 추출하고,</strong></p>
<p class="mL20 mT20"><strong>대상지 정보 및 투자환경 관련 항목을 데이터 베이스화 하여 객관적인 조림투자 타당성 분석을 수행할 것입니다. 또한 해외조림지에 대해 조림성과를 모니터하여 신뢰성있는 정보를 제공할 것입니다.</strong></p>
</div>
</div>
<h3 class="icon1">해외산림자원정보 분석 및 제공</h3>
<p class="icon3 section1 mB10">위성영상 자료분석을 통한 신뢰성 있는 정보의 제공과 해외산림자원 분석을 통해 조림성과 모니터링을 추진하여 다음과 같은 업무를 수행할 것입니다.</p>
<ol class="section2 icon_none">
<li>01. 해외산림 투자환경 조사</li>
<li>02. 조림적지 선정 및 조림투자 타당성 조사</li>
<li>03. 위성영상분석을 이용한 조림투자희망업체 대상 컨설팅</li>
<li>04. 해외조림 정책자금 지원 조림지 분석을 통한 자금 집행 투명성 확보</li>
<li>05. 정부의 국제산림협력사업지에 대한 현황 분석 및 사업 성과에 대한 지속적인 모니터링</li>
</ol>
<h3 class="icon1">국내 최고 해외산림사업 전문기관으로 도약</h3>
<p class="icon3 section1 mB30">위성영상분석 시스템을 구축하여 위성영상과 GIS 등 최신 기법을 이용한 실용적 정보를 제공할 것이며, 해외조림투자 희망업체의 투자 문의 시 신뢰성 있는 정보를 신속하게 제공하여 투자 활성화를 도모할 것입니다. 그리고 해외조림 가능 대상지에 대해 지속적인 데이터 축적을 통해 국내 최고의 해외산림사업 전문기관으로의 도약하겠습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020103_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020103_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020103_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020103_4.gif" alt="" /></span></div>