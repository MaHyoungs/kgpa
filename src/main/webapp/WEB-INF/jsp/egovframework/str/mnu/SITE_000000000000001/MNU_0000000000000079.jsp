<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="tab_box">
<ul class="tabs tab">
<li><a href="#tab1">탄소배출권 확보를 위한<br /> 새로운 산림산업 개발 및 지원</a></li>
<li><a href="#tab2"><span class="ht">산림부문 기후변화 대응전략 개발</span></a></li>
<li><a href="#tab3">UN기후변화협약에 따른<br /> CDM사업 인증기관으로 도약</a></li>
</ul>
<div class="tab_content"><!-- 탄소배출권 확보를 위한 새로운 산림산업 개발 및 지원  -->
<div id="tab1" class="tab_cont">
<h2 class="icon mB20">탄소배출권 확보를 위한 새로운 산림산업 개발 및 지원</h2>
<div class="img mB30"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201.gif" alt="탄소배출권 확보를 위한 새로운 산림산업 개발 및 지원" /></div>
<h3 class="icon1">기후변화와 CDM사업</h3>
<p class="icon3 section1 mB30">CDM(Clean Development Mechanism: 청정개발체제)사업은 교토의정서 12조에 규정된 것으로 선진국(Annex I 국가)이 개발도상국(Non-Annex I 국가)에 투자하여 발생한 온실가스 배출 감축분을 자국의 감축 실적에 반영할 수 있도록 함으로써 선진국은 비용 효과적으로 온실가스를 저감하고 개도국은 기술적&middot;경제적 지원을 얻는 제도입니다. 신규조림, 재조림, 산림전용 및 산림경영으로 발생한 온실가스 흡수량 또는 배출량을 탄소배출권으로 인정하고 있으며, 이를 근거로 신규조림 CDM 사업은 50년 간 산림이 아니던 토지를 산림으로 전환하는 사업을 의미하며 재조림 CDM 사업은 1990년 이전에 산림이 아닌 토지를 산림으로 전환하는 사업을 의미합니다.</p>
<div class="clear mB40">
<p class="alC mB30"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201_1.gif" alt="CMD사업절차 -  [CDM 사업 발굴사업자 &rarr;사업계획서 작성사업자 &rarr;타당성 확인 CDM 운영기구(CDM 인증원) &rarr;국가승인국가승인기구(국무총리실) &rarr;등록 CDM 집행위원회 &rarr;모니터링 사업자 &rarr;타당성 확인CDM 운영기구(CDM 인증원) &rarr;CER 발행 CDM집행위원회 &rarr;CER 배분 사업자]" /></p>
<p class="alC"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201_2.gif" alt="조림 CDM 사업의 국제적인 법적 틀 - [기후변화협약 &rarr; 교토의정서(12조)  &rarr; 마라케쉬협의문 (LULUCF 결정문)] &rarr; CDM 세부규칙 및 절차(Decision 17/CP.7)  &rarr; CDM-AR-PDD (A/R COM 세부 규칙 및 절차(Decision 19/CP.9), 간소화된 소규모 A/R CDM 세부 규칙 및 절차 (Decision 14/CP.10) , PDD 지침 및 방문, LULUCF GPG 2003 IPCC 2006 Guideline)" /></p>
</div>
<h3 class="icon1">A/R CDM 사업지원</h3>
<p class="icon3 section1 mB30">신규조림 및 재조림사업의 모델 제시를 위한 프로그램의 개발 및 연구를 지원하고, 국내외적으로 급성장하는 탄소시장에 대한 정보를 제공하고 있습니다.</p>
<h3 class="icon1">REDD 사업 지원</h3>
<p class="icon3 section1 mB30">REDD(Reducing Emission from Deforestation and Degradation in Developing countries)란 개도국의 산림 전용과 황폐화 방지를 통한 탄소배출감축활동을 말합니다. 개도국이 수행할 산림전용방지활동의 기준점 설정 및 그에 대한 보상 문제는 여전히 논의 대상이지만, 사업 적합지 선정과 배출권의 인정이 까다로운 A/R CDM에 비해 REDD는 새로운 접근법이 될 수 있을 것입니다.</p>
<h3 class="icon1">녹색성장을 위한 국가 경쟁력 강화</h3>
<p class="icon3 section1">해외산림사업 사업을 통해서 탄소배출권의 확보와 녹색성장을 위한 국가 경쟁력을 높이겠습니다</p>
</div>
<!-- //탄소배출권 확보를 위한 새로운 산림산업 개발 및 지원  --> <!-- 산림부문 기후변화 대응전략 개발  -->
<div id="tab2" class="tab_cont">
<h2 class="icon mB20">산림부문 기후변화 대응전략 개발</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202.gif" alt="산림부문 기후변화 대응전략 개발" /></span>
<div class="fL w450"><strong class="mL20">Post-2012 산림부문의 기후변화 대응을 위해</strong>
<p class="mL20 mT20"><strong class="txt_style">국제동향을 분석하고 정보수집 및 대응전략을 개발하고있습니다.</strong></p>
<p class="mL20 mT20"><strong>또한 국제심포지엄 개최 및 국제네트워크 구축활동을 통해서 산림분야 기후변화 전문가 양성등 체계적인 대응방안 마련을 위해 노력하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">전문가 지식공유, 협력강화</h3>
<p class="icon3 section1 mB30">Post-2012를 대비한 기후변화체제 협상 동향 및 국가 협상 전략을 반영한 산림부문 협상전략 수립을 하고, 지속적인 협력관계를 유지할 수 있는 기틀을 마련하기 위해 국제 세미나를 개최하고 있습니다. 또한 관련 분야 전문가들과 네트워크를 강화하여 지식을 공유하고 있습니다.</p>
<h3 class="icon1">체계적 기후변화 대응방안 수립</h3>
<p class="icon3 section1 mB30">학계, 산업계, NGO 등 민간 분야의 기후변화 대응 방안을 체계적 조직하여 기후변화의 산림부문 협상전략 수립을 지원하고 전문가를 육성하고 있습니다. 또한 지속적인 기후변화 대응 방안을 마련하여 지식과 정보를 축적하고 해외의 다각적인 인적 네트워크를 구성하고 있습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_1.gif" alt="체계적 기후변화 대응방안 수립" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_2.gif" alt="체계적 기후변화 대응방안 수립" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_3.gif" alt="체계적 기후변화 대응방안 수립" /></span></div>
</div>
<!-- //산림부문 기후변화 대응전략 개발  --> <!-- UN기후변화협약에 따른 CDM사업 인증기관으로 도약  -->
<div id="tab3" class="tab_cont">
<h2 class="icon mB20">UN기후변화협약에 따른 CDM사업 인증기관으로 도약</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203.gif" alt="UN기후변화협약에 따른 CDM사업 인증기관으로 도약" /></span>
<div class="fL w400"><strong class="mL20">녹색사업단은 UN 기후변화협약에 따른</strong>
<p class="mL20 mT20"><strong class="txt_style">신규조림 및 재조림 CDM 사업 부문의 인증기관으로</strong></p>
<p class="mL20 mT20"><strong>도약하기 위한 노력을 기울이고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">CDM 사업의 인증심사영역</h3>
<div class="mT20">
<table class="chart" summary="CDM 사업의 인증심사영역의 구분 및 UNFCCC분류코드의 정보를 나타내는 표입니다" cellpadding="0"><caption>CDM 사업의 인증심사영역</caption><colgroup> <col width="20%" /> <col width="80%" /> </colgroup>
<thead>
<tr><th scope="col">구분</th><th scope="col">UNFCCC 분류코드</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">01</td>
<td class="alL">에너지산업(Energy industries)</td>
</tr>
<tr>
<td scope="row">02</td>
<td class="alL">에너지 공급(Energy distribution)</td>
</tr>
<tr>
<td scope="row">03</td>
<td class="alL">에너지 수요(Energy demand)</td>
</tr>
<tr>
<td scope="row">04</td>
<td class="alL">제조업(Manufacturing industries)</td>
</tr>
<tr>
<td scope="row">05</td>
<td class="alL">화학산업(Chemical industry)</td>
</tr>
<tr>
<td scope="row">06</td>
<td class="alL">건설(Construction)</td>
</tr>
<tr>
<td scope="row">07</td>
<td class="alL">수송(Transport)</td>
</tr>
<tr>
<td scope="row">08</td>
<td class="alL">광업/광물(Mining/Mineral production)</td>
</tr>
<tr>
<td scope="row">09</td>
<td class="alL">금속공업(Metal production)</td>
</tr>
<tr>
<td scope="row">10</td>
<td class="alL">연료로부터의 탈루배출(Fugitive emission)</td>
</tr>
<tr>
<td scope="row">11</td>
<td class="alL">할로겐화 탄소, 6불화황 생산/소비</td>
</tr>
<tr>
<td scope="row">12</td>
<td class="alL">용제사용(Solvents use)</td>
</tr>
<tr>
<td scope="row">13</td>
<td class="alL">폐기물취급 및 처리(Waste handling and disposal)</td>
</tr>
<tr>
<td scope="row">14</td>
<td class="alL">조림 및 재조림(Afforestation and reforestation)</td>
</tr>
<tr>
<td scope="row">15</td>
<td class="alL">농업(Agriculture)</td>
</tr>
</tbody>
</table>
</div>
<p class="mB30">위 15개 분야의 CDM 사업 중 신규조림 및 재조림 분야의 전문적인 CDM사업 인증기관으로 도약하기 위해 더욱 노력하겠습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_1.gif" alt="UN기후변화협약에 따른 CDM사업 인증기관으로 도약" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_2.gif" alt="UN기후변화협약에 따른 CDM사업 인증기관으로 도약" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_3.gif" alt="UN기후변화협약에 따른 CDM사업 인증기관으로 도약" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_4.gif" alt="UN기후변화협약에 따른 CDM사업 인증기관으로 도약" /></span></div>
</div>
<!-- //UN기후변화협약에 따른 CDM사업 인증기관으로 도약  --></div>
</div>