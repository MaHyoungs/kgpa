<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">탄소배출권 확보를 위한 새로운 산림산업 개발 및 지원</h2>
<div class="img mB30"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201.gif" alt="" /></div>
<h3 class="icon1">기후변화와 CDM사업</h3>
<p class="icon3 section1 mB30">CDM(Clean Development Mechanism: 청정개발체제)사업은 교토의정서 12조에 규정된 것으로 선진국(Annex I 국가)이 개발도상국(Non-Annex I 국가)에 투자하여 발생한 온실가스 배출 감축분을 자국의 감축 실적에 반영할 수 있도록 함으로써 선진국은 비용 효과적으로 온실가스를 저감하고 개도국은 기술적&middot;경제적 지원을 얻는 제도입니다. 신규조림, 재조림, 산림전용 및 산림경영으로 발생한 온실가스 흡수량 또는 배출량을 탄소배출권으로 인정하고 있으며, 이를 근거로 신규조림 CDM 사업은 50년 간 산림이 아니던 토지를 산림으로 전환하는 사업을 의미하며 재조림 CDM 사업은 1990년 이전에 산림이 아닌 토지를 산림으로 전환하는 사업을 의미합니다.</p>
<div class="clear mB40">
<p class="alC mB30"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201_1.gif" alt="CMD사업절차 -  [CDM 사업 발굴사업자 &rarr;사업계획서 작성사업자 &rarr;타당성 확인 CDM 운영기구(CDM 인증원) &rarr;국가승인국가승인기구(국무총리실) &rarr;등록 CDM 집행위원회 &rarr;모니터링 사업자 &rarr;타당성 확인CDM 운영기구(CDM 인증원) &rarr;CER 발행 CDM집행위원회 &rarr;CER 배분 사업자]" /></p>
<p class="alC"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020201_2.gif" alt="조림 CDM 사업의 국제적인 법적 틀 - [기후변화협약 &rarr; 교토의정서(12조)  &rarr; 마라케쉬협의문 (LULUCF 결정문)] &rarr; CDM 세부규칙 및 절차(Decision 17/CP.7)  &rarr; CDM-AR-PDD (A/R COM 세부 규칙 및 절차(Decision 19/CP.9), 간소화된 소규모 A/R CDM 세부 규칙 및 절차 (Decision 14/CP.10) , PDD 지침 및 방문, LULUCF GPG 2003 IPCC 2006 Guideline)" /></p>
</div>
<h3 class="icon1">A/R CDM 사업지원</h3>
<p class="icon3 section1 mB30">신규조림 및 재조림사업의 모델 제시를 위한 프로그램의 개발 및 연구를 지원하고, 국내외적으로 급성장하는 탄소시장에 대한 정보를 제공하고 있습니다.</p>
<h3 class="icon1">REDD 사업 지원</h3>
<p class="icon3 section1 mB30">REDD(Reducing Emission from Deforestation and Degradation in Developing countries)란 개도국의 산림 전용과 황폐화 방지를 통한 탄소배출감축활동을 말합니다. 개도국이 수행할 산림전용방지활동의 기준점 설정 및 그에 대한 보상 문제는 여전히 논의 대상이지만, 사업 적합지 선정과 배출권의 인정이 까다로운 A/R CDM에 비해 REDD는 새로운 접근법이 될 수 있을 것입니다.</p>
<h3 class="icon1">녹색성장을 위한 국가 경쟁력 강화</h3>
<p class="icon3 section1">해외산림사업 사업을 통해서 탄소배출권의 확보와 녹색성장을 위한 국가 경쟁력을 높이겠습니다</p>