<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">산림부문 기후변화 대응전략 개발</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202.gif" alt="" /></span>
<div class="fL w450"><strong class="mL20">Post-2012 산림부문의 기후변화 대응을 위해</strong>
<p class="mL20 mT20"><strong class="txt_style">국제동향을 분석하고 정보수집 및 대응전략을 개발하고있습니다.</strong></p>
<p class="mL20 mT20"><strong>또한 국제심포지엄 개최 및 국제네트워크 구축활동을 통해서 산림분야 기후변화 전문가 양성등 체계적인 대응방안 마련을 위해 노력하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">전문가 지식공유, 협력강화</h3>
<p class="icon3 section1 mB30">Post-2012를 대비한 기후변화체제 협상 동향 및 국가 협상 전략을 반영한 산림부문 협상전략 수립을 하고, 지속적인 협력관계를 유지할 수 있는 기틀을 마련하기 위해 국제 세미나를 개최하고 있습니다. 또한 관련 분야 전문가들과 네트워크를 강화하여 지식을 공유하고 있습니다.</p>
<h3 class="icon1">체계적 기후변화 대응방안 수립</h3>
<p class="icon3 section1 mB30">학계, 산업계, NGO 등 민간 분야의 기후변화 대응 방안을 체계적 조직하여 기후변화의 산림부문 협상전략 수립을 지원하고 전문가를 육성하고 있습니다. 또한 지속적인 기후변화 대응 방안을 마련하여 지식과 정보를 축적하고 해외의 다각적인 인적 네트워크를 구성하고 있습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020202_3.gif" alt="" /></span></div>