<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">UN기후변화협약에 따른 CDM사업 인증기관으로 도약</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203.gif" alt="" /></span>
<div class="fL w400"><strong class="mL20">녹색사업단은 UN 기후변화협약에 따른</strong>
<p class="mL20 mT20"><strong class="txt_style">신규조림 및 재조림 CDM 사업 부문의 인증기관으로</strong></p>
<p class="mL20 mT20"><strong>도약하기 위한 노력을 기울이고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">CDM 사업의 인증심사영역</h3>
<div class="mT20">
<table class="chart" summary="CDM 사업의 인증심사영역의 구분 및 UNFCCC분류코드의 정보를 나타내는 표입니다" cellpadding="0"><caption>CDM 사업의 인증심사영역</caption><colgroup> <col width="20%" /> <col width="80%" /> </colgroup>
<thead>
<tr><th scope="col">구분</th><th scope="col">UNFCCC 분류코드</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">01</td>
<td class="alL">에너지산업(Energy industries)</td>
</tr>
<tr>
<td scope="row">02</td>
<td class="alL">에너지 공급(Energy distribution)</td>
</tr>
<tr>
<td scope="row">03</td>
<td class="alL">에너지 수요(Energy demand)</td>
</tr>
<tr>
<td scope="row">04</td>
<td class="alL">제조업(Manufacturing industries)</td>
</tr>
<tr>
<td scope="row">05</td>
<td class="alL">화학산업(Chemical industry)</td>
</tr>
<tr>
<td scope="row">06</td>
<td class="alL">건설(Construction)</td>
</tr>
<tr>
<td scope="row">07</td>
<td class="alL">수송(Transport)</td>
</tr>
<tr>
<td scope="row">08</td>
<td class="alL">광업/광물(Mining/Mineral production)</td>
</tr>
<tr>
<td scope="row">09</td>
<td class="alL">금속공업(Metal production)</td>
</tr>
<tr>
<td scope="row">10</td>
<td class="alL">연료로부터의 탈루배출(Fugitive emission)</td>
</tr>
<tr>
<td scope="row">11</td>
<td class="alL">할로겐화 탄소, 6불화황 생산/소비</td>
</tr>
<tr>
<td scope="row">12</td>
<td class="alL">용제사용(Solvents use)</td>
</tr>
<tr>
<td scope="row">13</td>
<td class="alL">폐기물취급 및 처리(Waste handling and disposal)</td>
</tr>
<tr>
<td scope="row">14</td>
<td class="alL">조림 및 재조림(Afforestation and reforestation)</td>
</tr>
<tr>
<td scope="row">15</td>
<td class="alL">농업(Agriculture)</td>
</tr>
</tbody>
</table>
</div>
<p class="mB30">위 15개 분야의 CDM 사업 중 신규조림 및 재조림 분야의 전문적인 CDM사업 인증기관으로 도약하기 위해 더욱 노력하겠습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020203_4.gif" alt="" /></span></div>