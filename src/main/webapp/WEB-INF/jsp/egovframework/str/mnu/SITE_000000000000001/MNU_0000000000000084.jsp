<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">국제산림협력사업</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020301.gif" alt="" /></span>
<div class="fL w400"><strong class="mL20">사막화 및 황사 발생의 억제를 위해</strong>
<p class="mL20 mT20"><strong class="txt_style">동북아산림네트워크 사무국을 담당 운영하며,</strong></p>
<p class="mL20 mT20"><strong>아시아 지역 기후변화 아젠다에 대한 리더십을 발휘할 <span class="txt_style">아시아산림협력기구(AFoCO)</span>의 설립을 지원하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">동북아 산림네트워크, 아시아 산림협력기구 운영</h3>
<p class="icon3 section1 mB30">동북아 산림네트워크 운영를 통해 사막화 확산과 황사발생을 억제하고 동북아시아 지역 산림 문제 해결을 위한 정책적 &middot; 기술적 지원을 할 것입니다. 또한 아시아 산림협력기구(AFoCO) 설립을 지원하여 아시아지역 산림을 생태적, 환경적, 경제적으로 건전하게 경영, 보전 및 이용할 수 있도록 최선을 다할 것입니다.</p>
<h3 class="icon1">정보ㆍ경험ㆍ전문가 교류</h3>
<p class="icon3 section1 mB30">국제산림협력기구는 연구의 발표 및 전문가 교류, 사막화방지프로그램과 연계된 지역프로그램 개발, 국가별 사막화와 황사방지 대책 및 이행과정에 대한 정보 교류, 산림전용, 산림황폐, 불법벌채 등 SFM(Sustainable Forest Management)사업관련 정책 및 이행관련 경험 &middot; 정보, 전문가 교류를 기본방향으로 운영할 것입니다.</p>
<h3 class="icon1">국제사회에서 산림부문의 주도적 지위 확보</h3>
<p class="icon3 section1 mB30">산림부문 협력 증진을 통하여 기후변화, 사막화 등 아시아지역 현안문제에 대한 회원국들의 대응 능력을 향상시키고, 지속가능한 발전에 기여할 것입니다. 그리하여 산림부문 국제사회에서 주도적 지위를 확보할 것이며, 사막화 &middot; 열대림파괴 방지 등 아시아지역 기후변화 아젠다에 대한 리더십을 발휘하는 등 국가 역량 강화에도 큰 역할을 할 것입니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020301_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020301_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020301_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020301_4.gif" alt="" /></span></div>