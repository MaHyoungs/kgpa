<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">해외산림녹화기술 전수 사업</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020302.gif" alt="" /></span>
<div class="fL w450"><strong class="mL20">개발도상국에 선진 산림녹화기술을 전수해주기 위하여</strong>
<p class="mL20 mT20"><strong class="txt_style">사막화방지 및 황폐림 복구사업, 열대림 임목종자개발사업</strong></p>
<p class="mL20 mT20"><strong>등의 국제산림협력사업 등을 지원하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">해외 산림환경 기능증진사업 지원</h3>
<p class="icon3 section1 mB30">인도네시아 열대림 임목종자 관리 및 개발사업(&lsquo;08~&rsquo;10)에 대하여 공동연구, 교육훈련, 전문가 파견 및 평가, 기자재 지원, 사업수행 평가 등을 지원하고 있습니다. 또한, 몽골 그린벨트 조림사업(&rsquo;07~&rsquo;16)의 지원을 추진하고 있습니다.</p>
<h3 class="icon1">나무심기 운동의 확산 및 위상제고</h3>
<p class="icon3 section1 mB30">사막화방지 및 열대 황폐림 복구를 위한 나무심기 운동을 확산시켜 녹색사업단 뿐만아니라 대한민국의 국제적 위상 제고를 이루어 내겠습니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020302_1.gif" alt="해외산림녹화기술 전수 사업" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020302_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020302_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020302_4.gif" alt="" /></span></div>