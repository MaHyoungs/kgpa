<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2 class="icon mB20">해외 산림전문가 육성 사업</h2>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020303.gif" alt="" /></span>
<div class="fL w400"><strong class="mL20">해외산림사업을 이끌어 나갈</strong>
<p class="mL20 mT20"><strong class="txt_style">현지 산림전문가 양성을 위해</strong></p>
<p class="mL20 mT20"><strong>개발도상국 산림전문가 장학지원 프로그램과 국내 대학생 해외산림 현지연수 프로그램을 제공하여 미래의 글로벌 녹색인재를 육성하고 있습니다.</strong></p>
</div>
</div>
<h3 class="icon1">KGPA 장학지원 프로그램</h3>
<p class="icon3 section1 mB10">해외산림사업에 필요한 개발도상국 산림전문가를 육성하여 국제산림인력 네트워크 구축 및 효율적인 사업추진, 국가간 우호증진을 이루어 내겠습니다. <br />해외산림사업 대상국출신의 산림유망주가 국내 대학원 석&middot;박사과정 입학시 수학에 필요한 장학금 지원하고 있습니다.</p>
<ol class="section2 icon_none">
<li>01. 지원기간 : 석사 2 년, 박사 3 년</li>
<li>02. 선발대상 : 해외산림사업 대상국 혹은 잠재대상국 출신 산림관련 전공자</li>
<li>03. 지원항목 : 항공료, 생활비, 정착지원금, 학비, 어학연수비, 교재비, 논문행정비, 논문제작비 등</li>
</ol>
<h3 class="icon1">국제사회가 요구하는 국제산림전문가 양성</h3>
<p class="icon3 section1 mB30">대학생 해외산림 현지연수 프로그램인 미래 글로벌 녹색인재 육성사업을 통해 국제사회가 요구하는 국제산림전문가를 양성할 것입니다.</p>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020303_1.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020303_2.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020303_3.gif" alt="" /></span> <span class="fL mL5"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020303_4.gif" alt="" /></span></div>