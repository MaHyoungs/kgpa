<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="tab_box">
<ul class="tabs tab">
<li><a href="#tab1">위성영상분석 서비스 개요</a></li>
<li><a href="#tab2">위성영상 일반분석</a></li>
<li><a href="#tab3">위성영상 상세분석</a></li>
</ul>
<div class="tab_content"><!-- 위성영상분석 서비스 개요  -->
<div id="tab1" class="tab_cont">
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401.gif" alt="위성영상과 GIS 등 최신기법" /></span>
<div class="fL w350"><strong class="mL20">해외사업 대상지의 현황과 이력을</strong>
<p class="mL20 mT20"><strong class="txt_style">위성영상과 GIS 등 최신기법을 활용,</strong></p>
<p class="mL20 mT20"><strong>신속&middot;정확하게 파악하여 해외산림투자기업과 관련기관이 필요로 하는 정보를 제공하겠습니다.</strong></p>
</div>
</div>
<h3 class="icon1">서비스 개요</h3>
<h4 class="icon2">서비스 대상</h4>
<ul class="icon3_list section2">
<li>해외산림사업 업체(개인포함) 및 기관</li>
</ul>
<h4 class="icon2">분석 범위</h4>
<ul class="icon3_list section2">
<li>사업대상지의 토지이용 현황 및 변화 파악</li>
<li>산림사업의 시계열별 성과 모니터링</li>
<li>고도자료를 이용한 등고선, 경사도 및 방위 파악 및 음영기복</li>
<li>산림재해에 따른 피해현황 파악</li>
<li>3차원 시뮬레이션 뷰어</li>
<li>필요에 따라 현지조사 병행을 통한 정확도 제고</li>
</ul>
<h3 class="icon1">서비스 절차</h3>
<div class="clear mB40">
<div class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_1.gif" alt="분석 대상지의 위치를 구글어스 화면상에 표시" /></div>
<div class="fL w450">
<h4 class="icon2">서비스 신청</h4>
<ul class="icon3_list section2">
<li>위성영상분석 신청서 작성후 이메일(kgpa@kgpa.or.kr) 또는 팩스(042-603-7310) 접수<br /> <a class="cbtn" href="&lt;a href="><span class="icon_note">위성영상분석 신청서</span></a><br /><span class="block mT5">※ 분석 대상지의 위치를 구글어스 화면상에 표시(좌표 제시)하여 확인</span></li>
<li>처리 기간 : 1개 지역 분석시, 영상 획득한 후 약 5일 소요</li>
</ul>
</div>
</div>
<h3 class="icon1">위성영상분석 처리절차</h3>
<div class="clear mT20"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_2.gif" alt="분석요청 접수(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 신청서 접수(seosh@kgpa.or.kr, 팩스: 042-603-7310)</li>
<li>문의: 042) 603-7327</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_3.gif" alt="위성영상 검색(2일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>분석 의뢰업체의 분석요구에 따른 위성영상 선별 및 검색</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_4.gif" alt="견적서 작성 및 통보(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 견적서, 계약서 제시
<ul class="icon_none">
<li>- 분석항목, 분석비, 위성영상분석 서비스 조건 포함</li>
<li>※ 분석비(영상구입 실비 + 분석수수료)</li>
</ul>
</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_5.gif" alt="분석비 납부" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>계좌이체를 통한 분석비 입금</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_6.gif" alt="위성영상 구립(1주~3주)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>분석비 납부확인 후, 영상구매 절차 진행</li>
<li>위성영상구매 전문업체 활용(단, Kompsat 영상은 사업단 직접 구매)<br /><span class="block mT5">※ 신규촬영 필요시 최장 6개월 소요될 수 있음</span></li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_7.gif" alt="위성영상 분석(5일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 실시</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_8.gif" alt="결과통보(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 결과산출물 제공(문서, 도면파일과 컬러 출력도면 등)</li>
</ul>
</div>
</div>
<p class="mT20 pbg alC"><strong>업무담당자:</strong> 해외산림사업본부 서상혁, 전화: 042) 603-7327</p>
</div>
<!-- //위성영상분석 서비스 개요  --> <!-- 위성영상 일반분석  -->
<div id="tab2" class="tab_cont">
<h3 class="icon1">위성영상분석 산출물</h3>
<h4 class="icon2">일반 분석산출물</h4>
<ul class="icon3_list section2">
<li>위성영상분석결과 설명 자료</li>
<li>컬러영상 파일 및 출력도면(A1, A3)</li>
<li>토지피복분류, 정규식생지수, 분석도 파일 및 출력도면(각 A3)</li>
</ul>
<h4 class="icon2">분석방법별 산출물</h4>
<table class="chart" summary="분석방법별 산출물의 분석방법, 산출물, 산출방법 정보를 나타내는 표입니다" cellpadding="0"><caption>분석방법별 산출물 정보</caption><colgroup> <col span="2" width="20%" /> <col width="60%" /> </colgroup>
<thead>
<tr><th scope="col">분석방법</th><th scope="col">산출물</th><th scope="col">산출방법</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">위성영상 분석 단독이용</td>
<td class="alL">
<ul class="icon3_list">
<li>컬러합성영상</li>
<li>토지피복분류</li>
<li>정규식생지수</li>
<li>시계열변화 분석</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>각 영상밴드를 합성하여 실세계와 가장 유사한 영상으로 조정</li>
<li>사용자(감독분류) 또는 분석프로그램(무감독분류)에 의한 토지이용현황 분석 수행</li>
<li>식생지수분석모듈 이용</li>
<li>연도별, 시기별 위성영상을 분석하여 토지피복과 식생지수의 시계열변화 구명</li>
</ul>
</td>
</tr>
<tr>
<td scope="row">위성영상 분석 및<br /> 현장조사 병행</td>
<td class="alL">
<ul class="icon3_list">
<li>토지피복분류</li>
<li>임목축적</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>현지의 토지이용 확인을 통해 정확도 제고</li>
<li>침엽수의 경우, 고해상도 위성영상에서 개체목수를 파악하고, 샘플지역의 개체수와 축적을 현지 조사하여 대상지 임목축적으로 환산</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p class="mT5">※활엽수의 경우, 위성영상상의 개체목 파악이 곤란해 축적환산이 어려움</p>
<h3 class="icon1 mT20">결과물 예시</h3>
<h4 class="icon2 ">가. 영상조합</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402.gif" alt="적외선 원본영상/RGB합성영상" /></span>
<p class="mT5 section2">※ 인공위성의 종류에 따라 컬러영상으로 변환할 수 없는 위성영상도 있음</p>
<h4 class="icon2 mT20">나. 토지피복분류 : 토지이용현황 분석</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402_1.gif" alt="토지피복분류 : 토지이용현황 분석" /></span>
<h4 class="icon2 mT20">다. 정규직생지수(NDVI) : 식생밀집도 분석</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402_2.gif" alt="정규직생지수(NDVI) : 식생밀집도 분석" /></span></div>
<!-- //위성영상 일반분석  --> <!-- 위성영상 상세분석  -->
<div id="tab3" class="tab_cont">
<h3 class="icon1">위성영상분석 산출물</h3>
<h4 class="icon2">상세 분석산출물</h4>
<ul class="icon3_list section2">
<li>음영기복, 등고선, 경사, 방위 파일 및 출력도면(각 A3)</li>
<li>3차원 시뮬레이션뷰어 (DVD)</li>
</ul>
<h4 class="icon2">분석방법별 산출물</h4>
<div class="mT20 section2">
<table class="chart" summary="분석방법별 산출물의 분석방법, 산출물, 산출방법 정보를 나타내는 표입니다" cellpadding="0"><caption>분석방법별 산출물 정보</caption><colgroup> <col span="2" width="20%" /> <col width="60%" /> </colgroup>
<thead>
<tr><th scope="col">분석방법</th><th scope="col">산출물</th><th scope="col">산출방법</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">위성영상과 <br />고도자료 이용분석</td>
<td class="alL">
<ul class="icon3_list">
<li>경사분석</li>
<li>방위분석</li>
<li>음영기복</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>고도자료(SRTM)을 이용한 대상지역 등고선 레이어 추가</li>
<li>Surface 분석 및 통계분석을 통한 경사 및 방위 분석</li>
<li>지형의 표고에 따른 음영효과를 시각적으로 표현</li>
</ul>
</td>
</tr>
<tr>
<td scope="row">RGB합성 위성영상과 고도자료 이용 3차원 시뮬레이션</td>
<td class="alL">
<ul class="icon3_list">
<li>3차원</li>
<li>시뮬레이션</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>시각적인 지리정보 표현을 위한 Z-Scale 조정</li>
<li>입체적 정보를 이용한 대상지와 유사한 지형정보 표현</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h3 class="icon1 mT20">결과물 예시</h3>
<h4 class="icon2 ">가. 경사도 분석</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403.gif" alt="경사도 분석" /></span>
<h4 class="icon2 mT20">나. 방향분석</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_1.gif" alt="방향분석" /></span>
<h4 class="icon2 mT20">다. 음영기복도+등고선(노랑)</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_2.gif" alt="음영기복도+등고선(노랑)" /></span>
<h4 class="icon2 mT20">라. 3차원 시뮬레이션 뷰어</h4>
<span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_3.gif" alt="3차원 시뮬레이션 뷰어" /></span></div>
<!-- //위성영상 상세분석  --></div>
</div>