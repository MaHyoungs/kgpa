<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="clear mB40"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401.gif" alt="" /></span>
<div class="fL w350"><strong class="mL20">해외사업 대상지의 현황과 이력을</strong>
<p class="mL20 mT20"><strong class="txt_style">위성영상과 GIS 등 최신기법을 활용,</strong></p>
<p class="mL20 mT20"><strong>신속&middot;정확하게 파악하여 해외산림투자기업과 관련기관이 필요로 하는 정보를 제공하겠습니다.</strong></p>
</div>
</div>
<h3 class="icon1">서비스 개요</h3>
<h4 class="icon2">서비스 대상</h4>
<ul class="icon3_list section2">
<li>해외산림사업 업체(개인포함) 및 기관</li>
</ul>
<h4 class="icon2">분석 범위</h4>
<ul class="icon3_list section2">
<li>사업대상지의 토지이용 현황 및 변화 파악</li>
<li>산림사업의 시계열별 성과 모니터링</li>
<li>고도자료를 이용한 등고선, 경사도 및 방위 파악 및 음영기복</li>
<li>산림재해에 따른 피해현황 파악</li>
<li>3차원 시뮬레이션 뷰어</li>
<li>필요에 따라 현지조사 병행을 통한 정확도 제고</li>
</ul>
<h3 class="icon1">서비스 절차</h3>
<div class="clear mB40">
<div class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_1.gif" alt="" /></div>
<div class="fL w450">
<h4 class="icon2">서비스 신청</h4>
<ul class="icon3_list section2">
<li>위성영상분석 신청서 작성후 이메일(kgpa@kgpa.or.kr) 또는 팩스(042-603-7310) 접수<br /> <a class="cbtn" href="/attachfiles/file/satellite_form_new.zip"><span class="icon_note">위성영상분석 신청서</span></a><br /><span class="block mT5">※ 분석 대상지의 위치를 구글어스 화면상에 표시(좌표 제시)하여 확인</span></li>
<li>처리 기간 : 1개 지역 분석시, 영상 획득한 후 약 5일 소요</li>
</ul>
</div>
</div>
<h3 class="icon1">위성영상분석 처리절차</h3>
<div class="clear mT20"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_2.gif" alt="분석요청 접수(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 신청서 접수(seosh@kgpa.or.kr, 팩스: 042-603-7310)</li>
<li>문의: 042) 603-7327</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_3.gif" alt="위성영상 검색(2일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>분석 의뢰업체의 분석요구에 따른 위성영상 선별 및 검색</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_4.gif" alt="견적서 작성 및 통보(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 견적서, 계약서 제시
<ul class="icon_none">
<li>- 분석항목, 분석비, 위성영상분석 서비스 조건 포함</li>
<li>※ 분석비(영상구입 실비 + 분석수수료)</li>
</ul>
</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_5.gif" alt="분석비 납부" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>계좌이체를 통한 분석비 입금</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_6.gif" alt="위성영상 구립(1주~3주)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>분석비 납부확인 후, 영상구매 절차 진행</li>
<li>위성영상구매 전문업체 활용(단, Kompsat 영상은 사업단 직접 구매)<br /><span class="block mT5">※ 신규촬영 필요시 최장 6개월 소요될 수 있음</span></li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_7.gif" alt="위성영상 분석(5일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 실시</li>
</ul>
</div>
</div>
<div class="clear"><span class="fL"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020401_8.gif" alt="결과통보(1일)" /></span>
<div class="fL">
<ul class="icon3_list section2">
<li>위성영상분석 결과산출물 제공(문서, 도면파일과 컬러 출력도면 등)</li>
</ul>
</div>
</div>
<p class="mT20 pbg alC"><strong>업무담당자:</strong> 해외산림사업본부 서상혁, 전화: 042) 603-7327</p>