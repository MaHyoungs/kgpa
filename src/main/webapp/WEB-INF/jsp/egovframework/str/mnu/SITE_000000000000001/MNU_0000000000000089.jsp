<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon1">위성영상분석 산출물</h3>
<h4 class="icon2">일반 분석산출물</h4>
<ul class="icon3_list section2">
<li>위성영상분석결과 설명 자료</li>
<li>컬러영상 파일 및 출력도면(A1, A3)</li>
<li>토지피복분류, 정규식생지수, 분석도 파일 및 출력도면(각 A3)</li>
</ul>
<h4 class="icon2">분석방법별 산출물</h4>
<table class="chart" summary="분석방법별 산출물의 분석방법, 산출물, 산출방법 정보를 나타내는 표입니다" cellpadding="0"><caption>분석방법별 산출물 정보</caption><colgroup> <col span="2" width="20%" /> <col width="60%" /> </colgroup>
<thead>
<tr><th scope="col">분석방법</th><th scope="col">산출물</th><th scope="col">산출방법</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">위성영상 분석 단독이용</td>
<td class="alL">
<ul class="icon3_list">
<li>컬러합성영상</li>
<li>토지피복분류</li>
<li>정규식생지수</li>
<li>시계열변화 분석</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>각 영상밴드를 합성하여 실세계와 가장 유사한 영상으로 조정</li>
<li>사용자(감독분류) 또는 분석프로그램(무감독분류)에 의한 토지이용현황 분석 수행</li>
<li>식생지수분석모듈 이용</li>
<li>연도별, 시기별 위성영상을 분석하여 토지피복과 식생지수의 시계열변화 구명</li>
</ul>
</td>
</tr>
<tr>
<td scope="row">위성영상 분석 및<br /> 현장조사 병행</td>
<td class="alL">
<ul class="icon3_list">
<li>토지피복분류</li>
<li>임목축적</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>현지의 토지이용 확인을 통해 정확도 제고</li>
<li>침엽수의 경우, 고해상도 위성영상에서 개체목수를 파악하고, 샘플지역의 개체수와 축적을 현지 조사하여 대상지 임목축적으로 환산</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p class="mT5">※활엽수의 경우, 위성영상상의 개체목 파악이 곤란해 축적환산이 어려움</p>
<h3 class="icon1 mT20">결과물 예시</h3>
<h4 class="icon2 ">가. 영상조합</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402.gif" alt="적외선 원본영상/RGB합성영상" /></span></p>
<p class="mT5 section2">※ 인공위성의 종류에 따라 컬러영상으로 변환할 수 없는 위성영상도 있음</p>
<h4 class="icon2 mT20">나. 토지피복분류 : 토지이용현황 분석</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402_1.gif" alt="토지피복분류 : 토지이용현황 분석" /></span></p>
<h4 class="icon2 mT20">다. 정규직생지수(NDVI) : 식생밀집도 분석</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020402_2.gif" alt="정규직생지수(NDVI) : 식생밀집도 분석" /></span></p>