<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon1">위성영상분석 산출물</h3>
<h4 class="icon2">상세 분석산출물</h4>
<ul class="icon3_list section2">
<li>음영기복, 등고선, 경사, 방위 파일 및 출력도면(각 A3)</li>
<li>3차원 시뮬레이션뷰어 (DVD)</li>
</ul>
<h4 class="icon2">분석방법별 산출물</h4>
<div class="mT20 section2">
<table class="chart" summary="분석방법별 산출물의 분석방법, 산출물, 산출방법 정보를 나타내는 표입니다" cellpadding="0"><caption>분석방법별 산출물 정보</caption><colgroup> <col span="2" width="20%" /> <col width="60%" /> </colgroup>
<thead>
<tr><th scope="col">분석방법</th><th scope="col">산출물</th><th scope="col">산출방법</th></tr>
</thead>
<tbody>
<tr>
<td scope="row">위성영상과 <br />고도자료 이용분석</td>
<td class="alL">
<ul class="icon3_list">
<li>경사분석</li>
<li>방위분석</li>
<li>음영기복</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>고도자료(SRTM)을 이용한 대상지역 등고선 레이어 추가</li>
<li>Surface 분석 및 통계분석을 통한 경사 및 방위 분석</li>
<li>지형의 표고에 따른 음영효과를 시각적으로 표현</li>
</ul>
</td>
</tr>
<tr>
<td scope="row">RGB합성 위성영상과 고도자료 이용 3차원 시뮬레이션</td>
<td class="alL">
<ul class="icon3_list">
<li>3차원</li>
<li>시뮬레이션</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>시각적인 지리정보 표현을 위한 Z-Scale 조정</li>
<li>입체적 정보를 이용한 대상지와 유사한 지형정보 표현</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h3 class="icon1 mT20">결과물 예시</h3>
<h4 class="icon2 ">가. 경사도 분석</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403.gif" alt="경사도 분석" /></span></p>
<h4 class="icon2 mT20">나. 방향분석</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_1.gif" alt="방향분석" /></span></p>
<h4 class="icon2 mT20">다. 음영기복도+등고선(노랑)</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_2.gif" alt="음영기복도+등고선(노랑)" /></span></p>
<h4 class="icon2 mT20">라. 3차원 시뮬레이션 뷰어</h4>
<p><span class="mT10 section2"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_05020403_3.gif" alt="3차원 시뮬레이션 뷰어" /></span></p>