<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon">2011년 해외산림사업</h3>
<h4 class="icon1">해외산림자원조성</h4>
<div class="mB30">
<table class="chart" summary="해외산림자원조성의 사업명, 성과 정보를 나타내는 표입니다"><caption>해외산림자원조성</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외조림사업 진출</td>
<td class="alL">
<ul class="icon3_list">
<li>파라과이 해외조림사업 추진</li>
<li>해외조림대상지 타당성조사(필리핀,캄보디아)</li>
<li>해외산림협력센터 운영</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외 목재펠릿 사업기반 구축</td>
<td class="alL">
<ul class="icon3_list">
<li>조림목 벌채 추진 중인 해외조림기업과 협력 사업개발</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">장기목재비축기지 대상지 확보</td>
<td class="alL">
<ul class="icon3_list">
<li>우량임지 매입 또는 해외조림지 지분참여 및 공동투자 추진</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h4 class="icon1 mT20">투자환경조사 및 컨설팅</h4>
<div class="mB30">
<table class="chart" summary="투자환경조사 및 컨설팅의 사업명, 성과 정보를 나타내는 표입니다"><caption>투자환경조사 및 컨설팅</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림자원 확보 인프라 구축</td>
<td class="alL">
<ul class="icon3_list">
<li>해외산림투자 실무가이드Ⅲ 발간(미얀마,파라과이,칠레)</li>
<li>해외조림 주요 수종의 책자 발간 및 보급</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외조림지 모니터링 및 컨설팅</td>
<td class="alL">
<ul class="icon3_list">
<li>해외조림지 모니터링 사업(뉴질랜드)</li>
<li>민간기업 대상 해외산림투자 컨설팅</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외산림전문가 네트워크 구축 및 활용</td>
<td class="alL">
<ul class="icon3_list">
<li>해외산림전문가 자문단 운영</li>
<li>해외산림자원개발 심의위원회 운영</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h4 class="icon1 mT20">국제산림협력</h4>
<div class="mB30">
<table class="chart" summary="국제산림협력의 사업명, 성과 정보를 나타내는 표입니다"><caption>국제산림협력</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">산림분야 기후변화 대응 협력사업</td>
<td class="alL">
<ul class="icon3_list">
<li>기후변화 대응 협력 사업 추진</li>
<li>사막화방지 사업 수행</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">국제산림협력 강화</td>
<td class="alL">
<ul class="icon3_list">
<li>한-인니 산림협력센터 활성화를 위한 협력 사업 개발</li>
<li>한-파라과이 산림협력 우호림 준공식</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">인재육성 프로그램</td>
<td class="alL">
<ul class="icon3_list">
<li>개도국 출신 국내 석박사 장학지원 사업 대학생 해외산림사업지 연수 프로그램 시행</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>