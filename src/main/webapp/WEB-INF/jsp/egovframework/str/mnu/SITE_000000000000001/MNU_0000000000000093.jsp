<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon">추진실적('08~'10)</h3>
<h4 class="icon1">2010년도 해외산림사업</h4>
<h5 class="icon2">해외산림자원 확보</h5>
<div class="mB30">
<table class="chart " summary="2010년도 해외산림자원 확보의 사업명, 성과 정보를 나타내는 표입니다"><caption>2010년도 해외산림자원 확보</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">목재바이오매스에너지 조림사업
<ul class="icon3_list">
<li>해외 목재바이오매스(목재펠릿) 가공 시범사업 기반조성</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>인니 사업대상지 사전투자타당성 조사 실시</li>
<li>전문기관과 투자타당성조사</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2 mT20">투자환경조사 및 컨설팅</h5>
<div class="mB30">
<table class="chart " summary="2010년도 투자환경조사 및 컨설팅의 사업명, 성과 정보를 나타내는 표입니다"><caption>2010년도 투자환경조사 및 컨설팅</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림투자환경 및 조림투자 타당성 조사</td>
<td class="alL">
<ul class="icon3_list">
<li>베트남 해외산림투자실무가이드 발간</li>
<li>라오스 해외산림투자실무가이드 발간</li>
<li>필리핀 해외산림투자실무가이드 발간<br /> * 필리핀 투자설명회 개최</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외산림자원 정보수집 및 세미나</td>
<td class="alL">
<ul class="icon3_list">
<li>중남미 산림자원개발 투자설명회 개최</li>
<li>FAO 2009 세계산림현황 번역 발간</li>
<li>IUFRO 아시아 지역의 산림과 임업, 그리고 녹색성장 세미나 개최</li>
<li>민주콩고, 튀니지 산림투자환경조사</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외산림자원 모니터링</td>
<td class="alL">
<ul class="icon3_list">
<li>이건산업과 해외조림지 임분생장 공동연구 MOU 체결</li>
<li>이건산업 조림지 임분생장 현지조사<br /> * 결과발표회</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">투자기업 지원 및 컨설팅 서비스</td>
<td class="alL">
<ul class="icon3_list">
<li>해외산림 위성영상정보 분석서비스 지속 실시<br /> (4개사, 435천ha 분석)</li>
<li>해외산림투자희망업체 컨설팅 실시</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2 mT20">국제교류 및 협력</h5>
<div class="mB30">
<table class="chart " summary="2010년도 국제교류 및 협력의 사업명, 성과 정보를 나타내는 표입니다"><caption>2010년도 국제교류 및 협력</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">임업협력회의 및 국제회의</td>
<td class="alL">
<ul class="icon3_list">
<li>한-파라과이 임업협력회의 참석</li>
<li>한-인니 산림포럼 참석</li>
<li>한-아세안 산림협력 워크숍 참석</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">국제산림협력 발굴 및 수행</td>
<td class="alL">
<ul class="icon3_list">
<li>한-파라과이 산림협력 우호림 예정지 현지조사</li>
<li>한-파라과이 산림협력 우호림 조성 추진</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">
<ul>
<li>기후변화 대응관련 인식 확산</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>사막화방지 캠페인 슬로건 공모 및 몽골 나무심기 행사 개최</li>
<li>UNCCD COP 10 개최기념 심포지엄</li>
<li>&ldquo;초록을 꿈꾸는 사진전&rdquo; 공동주최</li>
<li>몽골 A/R CDM Training workshop 수행<br />한-몽 그린벨트사업단과 협력하여 11명 연수</li>
<li>REDD+ 전망과 협상전략 국제심포지엄</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외산림전문가 육성</td>
<td class="alL">
<ul class="icon3_list">
<li>개도국출신 산림분야 석․박사과정 장학프로그램 <br /> * 석사 2명(미얀마, 중국), 박사 1명(필리핀) 선발 지원(&rsquo;10.8)</li>
<li>미래글로벌 녹색인재육성 프로그램<br />․국내 산림분야대학생 13명 인도네시아 산림사업지역 현지연수 실시</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h4 class="icon1">2009년 해외산림사업</h4>
<h5 class="icon2">해외산림자원확보 및 지원사업</h5>
<div class="mB30">
<table class="chart " summary="2009년 해외산림자원확보 및 지원사업의 사업명, 성과 정보를 나타내는 표입니다"><caption>2009년 해외산림자원확보 및 지원사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림자원확보 인프라 구축사업
<ul class="icon3_list">
<li>해외산림투자 실무 가이드</li>
<li>조림투자 타당성 조사</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>산림자원개발관련 법령, 사업허가, 절차, 법인설립 등 투자환경, 조림적지 조사</li>
<li>캄보디아 정부 추천 조림 대상자 타당성 조사</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">해외산림자원 정보 수집 및 분석
<ul class="icon3_list">
<li>해외산림정보 분석시스템</li>
<li>정보 및 동향조사</li>
</ul>
</td>
<td class="alL">
<ul class="icon3_list">
<li>위성영상 시스템 구축 및 운영</li>
<li>산림자원 정보 및 임업 동향</li>
<li>투자 및 지원사업지 모니터링</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2">기후변화 대응사업</h5>
<div class="mB30">
<table class="chart " summary="2009년 기후변화 대응사업의 사업명, 성과 정보를 나타내는 표입니다"><caption>2009년 기후변화 대응사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">산림분야 기후변화 대응</td>
<td class="alL" rowspan="2">
<ul class="icon3_list">
<li>2009 사막화방지의 날 기념 국제 심포지엄 공동개최</li>
<li>Post-2012 기후변화협상 산림부문 협상전략 수립을 위한 전문가 세미나 공동개최</li>
<li>해외목재펠릿가공시설 벤치마킹(호주 Plantation Energy)</li>
<li>제9차 UN사막화방지협약 당사국총회 참석</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">국제동향 및 국제회의</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2">홍보사업</h5>
<div class="mB30">
<table class="chart " summary="2009년 홍보사업의 사업명, 성과 정보를 나타내는 표입니다"><caption>2009년 홍보사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림사업 대외 홍보</td>
<td class="alL">
<ul class="icon3_list">
<li>국문 및 영문리플릿, 해외산림사업탁상달력</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2">기타사업</h5>
<div class="mB30">
<table class="chart " summary="2009년 기타사업의 사업명, 성과 정보를 나타내는 표입니다"><caption>2009년 기타사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림투자환경조사 (국고보조금)</td>
<td class="alL">
<ul class="icon3_list">
<li>인니 조림대상지 조사 및 결과 발표 (한-인니 산림포럼)</li>
<li>인도네시아 목재바이오매스 산업육성을 위한 조림대상지 조사</li>
<li>아프리카 콩고민주공화국 산림투자환경 조사</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">산림청 해외인턴지원 (국고보조금)</td>
<td class="alL">
<ul class="icon3_list">
<li>국제산림협력증진사업 해외인턴운영</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h4 class="icon1">2008년도 해외산림사업</h4>
<h5 class="icon2 mB30">해외산림자원확보 및 지원사업</h5>
<div class="mB30">
<table class="chart " summary="2008년 해외산림자원 확보의 사업명, 성과 정보를 나타내는 표입니다"><caption>2008년 해외산림자원 확보</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">국제산림협력 및 인도네시아 임업동향 조사사업</td>
<td class="alL">
<ul class="icon3_list">
<li>인도네시아 산림자원정보 및 동향조사</li>
</ul>
</td>
</tr>
<tr>
<td class="alL" scope="row">능력배양사업</td>
<td class="alL">
<ul class="icon3_list">
<li>국제산림협력사업지 모니터링</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2">국제교류 및 협력사업</h5>
<div class="mB30">
<table class="chart " summary="2008년 국제교류 및 협력사업 의 사업명, 성과 정보를 나타내는 표입니다"><caption>2008년 국제교류 및 협력사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">국제세미나</td>
<td class="alL">
<ul class="icon3_list">
<li>&lsquo;산림, 탄소시장 그리고 녹색성장&rsquo;국제심포지엄 개최</li>
<li>&lsquo;녹색성장시대의 해외산림투자환경 변화와 대응전략&rsquo; 국제심포지엄</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<h5 class="icon2">홍보사업</h5>
<div class="mB30">
<table class="chart " summary="2008년 홍보사업의 사업명, 성과 정보를 나타내는 표입니다"><caption>2008년 홍보사업</caption><colgroup> <col span="2" width="50%" /> </colgroup>
<thead>
<tr><th scope="col">사업명</th><th scope="col">성과</th></tr>
</thead>
<tbody>
<tr>
<td class="alL" scope="row">해외산림사업 대외 홍보</td>
<td class="alL">
<ul class="icon3_list">
<li>영문리플릿</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<p class="section1 mB30">※ 관련 자료는 &lsquo;사업단홈페이지&gt;자료공유&gt;해외산림사업&rsquo;을 참고하시기 바랍니다.</p>