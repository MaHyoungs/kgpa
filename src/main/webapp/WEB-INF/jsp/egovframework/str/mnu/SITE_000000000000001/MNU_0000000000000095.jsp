<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="txt_bg01 line_green">
<p class="pB20"><strong class="txt_style">맑은 물 공급을 위한 산림 환경 개선 사업지원 녹색사업단이 함께합니다.</strong></p>
</div>
<p class="mT20 pB20 line_green">국회 환경포럼에서 <span class="green">맑은 물과 깨끗한 공기 공급 등 산림의 환경기능에 대한 국민의 수요를 충족시키기 위한 대책</span> 이 제기됨에 따라 98년 산림법 개정안을 통해 99년부터 녹색복권을 발행하여 그 수익금으로 조성되어 오던 중 '03년에 온라인 연합복권(로또) 수익금 배분액이 추가 되었고 04년 복권 및 복권기금법이 제정되면서 복권기금의 수익금 중 일부(2.046%)를 배분받아 조성되어 왔으며, <span class="green">조성된 녹색자금은 산림자원의 조성 및 관리에 관한 법률 제58조 제4항 및 동시행령 제60조 제2항의 규정에 따라 산림환경기능 증진에 관한 다음 사업에 지원하고 있습니다.</span></p>
<h3 class="icon1 mT20">맑은 물 공급을 위한 산림 환경 개선 사업지원</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>5대강 유역내 산림에 대한 간벌, 가지치기 등 숲가꾸기 사업</li>
<li>보안림 등 법정제한림내 사유림 매입</li>
<li>사방댐 등 수원함양 및 수질보전을 위한 시설 설치</li>
</ul>
</div>
<h3 class="icon1 mT20">청소년 등을 위한 신림체험 활동 시설의 설치&middot;운영 및 교육&middot;홍보사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>청소년 등 소외계층이 전문화된 숲 체험을 접할수 있는 체험교 육 시설 및 프로그램의 운영</li>
<li>빈곤층, 결손가정, 저소득 노인, 장애인 등 숲 체험기회 제공</li>
<li>산림환경의 중요성과 산림문화 활동의 대국민 홍보활동 전개</li>
</ul>
</div>
<h3 class="icon1 mT20">산림환경기능 증진과 관련한 임업인의 교육 및 복지증진 사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>임업인이 참여할 수 있는 교육사업의 개발</li>
<li>임업인 자녀를 위한 장학사업의 개발</li>
<li>그 외 임업인의 복지증진을 위한 사업</li>
</ul>
</div>
<h3 class="icon1 mT20">해외산림환경기능 증진 사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>우리나라의 산림녹화기술의 해외 전파</li>
<li>해외 산림전문가 육성 사업</li>
<li>열대 황폐림 복구 및 사막화 방지 사업 지원</li>
</ul>
</div>
<h3 class="icon1 mT20">해외에서 산림자원을 확보하기 위한 사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>목재바이오매스 시범조림 및 생산사업</li>
<li>해외 목재자원 확보를 위한 조림사업</li>
<li>해외산림투자 활성화를 위한 투자업체 지원</li>
<li>과학적인 산림정보 분석 서비스 제공</li>
</ul>
</div>
<h3 class="icon1 mT20">탄소흡수원 확충과 탄소배출 저감관련 사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>탄소배출권 확보를 위한 새로운 조림사업 개발</li>
<li>산림부문 기후변화 대응전략 개발</li>
<li>UN기후변화협약에 따른 CDM 인증 사업 대행</li>
</ul>
</div>
<h3 class="icon1 mT20">그 밖의 산림의 환경기능 증진을 위하여 대통령이 정하는 사업</h3>
<div class="pbg mT20">
<ul class="icon3_list">
<li>산림환경기능 증진을 위한 사업에 관한 연구&middot;개발지원</li>
<li>야생동식물 생태공원의 조성 및 운영의 지원</li>
<li>산림환경기능증진을 위한 국제교류 및 협력사업</li>
<li>산림재해 공재사업 지원</li>
<li>산림재해 방지 등 산림보전에 필요한 사업</li>
</ul>
</div>