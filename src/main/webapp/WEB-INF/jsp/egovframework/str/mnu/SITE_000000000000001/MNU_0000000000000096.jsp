<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="txt_bg01 line_green">
<p class="pB20"><strong class="txt_style">녹색자금은 복권기금으로 조성되며 도시 숲 조성/전통 숲 복원 등 환경기능 증진관련 공익사업에 쓰여집니다.</strong></p>
</div>
<h3 class="icon1 mT20">녹색자금의 조성 재원(산림자원법 제58조 3항)</h3>
<ol class="section1 mB40">
<li>01. 정부 외의 자의 출연금</li>
<li>02. "복권 및 복권기금법" 제23조 제1항의 규정에 의하여 배분된 복권수익금</li>
<li>03. 녹색자금의 운용으로 생기는 수익금</li>
<li>04. 그 밖에 대통령령이 정하는 수익금</li>
</ol>
<h3 class="icon1 mT20">녹색자금의 지원사업</h3>
<ol class="section1">
<li>01. 맑은 물 공급을 위한 산림환경 개선사업</li>
<li>02. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</li>
<li>03. 청소년 등을 위한 산림체험활동 시설의 설치 &middot; 운영 및 교육&middot;홍보사업</li>
<li>04. 산림환경기능증진과 관련한 임업인의 교육 및 복지증진사업</li>
<li>05. 해외산림 환경기능증진 사업</li>
<li>06. 해외에서 산림자원을 확보하기 위한 사업</li>
<li>07. 그 밖에 산림의 환경기능증진을 위하여 대통령령으로 정하는 사업</li>
</ol>