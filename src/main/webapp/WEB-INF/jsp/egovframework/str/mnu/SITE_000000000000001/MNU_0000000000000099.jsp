<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2001년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2001년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2001년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>27개사업</strong></td>
<td><strong>625,590</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>2개사업</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-1</td>
<td>하천변 산림환경개선 중랑천살리기 운동</td>
<td>환경정의시민연대</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-2</td>
<td>맑은 물 공급을 위한 환경개선사업</td>
<td>양평군산림조합</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>2개사업</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>시민의숲, 희망의동산 만들기</td>
<td>생명의숲(대전)</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>가로수 환경조성을 위한 벗나무거리 조성</td>
<td>양구군청</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>5개사업</td>
<td>155,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>송림어린이숲체험교실(표지판, 자료집제작)</td>
<td>생명의숲(포항)</td>
<td>5,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>지역산림생태조사 및 홍보활동</td>
<td>환경운동엽합(진주)</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>청소년 푸른숲선도원 육성 및 홍보</td>
<td>한그루녹색회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>산불방지홍보물 제작 및 산불방지 홍보</td>
<td>산 림 청</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>제1회 숲사진 공모전</td>
<td>산림조합중앙회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>1개사업</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4-1</td>
<td>부락산 생태복원에 대한 조사, 지도, 학습장 조성</td>
<td>환경운동연합(평택)</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>5개사업</td>
<td>102,800</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-1</td>
<td>시민과 함께하는 광릉숲 야생동물보존운동</td>
<td>광릉숲보존회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-2</td>
<td>청소년수련장 야생화 학습단지 조성</td>
<td>한국독림가협회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-3</td>
<td>산림생태계내 야생조수(한국꿩)복원사업(북한산 또는 관악산)</td>
<td>한국임정회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-4</td>
<td>자연생태계복원책자 발간</td>
<td>한국야생동식물<br /> 보호협회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-5</td>
<td>녹색자연 우포만들기(철새안전서식지 제공)</td>
<td>환경운동연합(창녕)</td>
<td>2,800</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>8개사업</td>
<td>147,790</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-1</td>
<td>백두대간 등산로 실태조사 및 관리방안</td>
<td>녹색연합</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-2</td>
<td>산촌실태DB구축 및 전국산촌실태조사 워크샾</td>
<td>생태산촌만들기</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-3</td>
<td>노거수를 활용한 교재제작 및 활용프로그램 개발</td>
<td>생명의숲(울산)</td>
<td>8,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-4</td>
<td>도시공원 숲해설 프로그램개발(광릉수목원 단체탐방객 숲해설)</td>
<td>숲해설가협의</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-5</td>
<td>숲학교운영 및 프로그램 개발</td>
<td>숲해설가협의</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-6</td>
<td>마을숲 발굴 및 보전대책 연구</td>
<td>한국기술인협회</td>
<td>19,790</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-7</td>
<td>전국 등산로 상태조사 및 관리지침 제작</td>
<td>산 림 청</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-8</td>
<td>도시림 생태보호기반 조성 및 생태교육 프로그램</td>
<td>인천시산림조합</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>1개사업</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-1</td>
<td>동북아지역황폐산림복구 국제협력(중국, 몽골)</td>
<td>동북아산림포럼</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>3개사업</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-1</td>
<td>문학산 산림보전을 위한 시민운동</td>
<td>카톨릭환경연대</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-2</td>
<td>'내나무를 가집시다' 캠페인</td>
<td>디지털조선일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-3</td>
<td>녹색복권 및 산불조심 광고판 설치</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>