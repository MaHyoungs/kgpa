<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2002년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2002년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2002년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>22개사업</strong></td>
<td><strong>993,850</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>1개사업</td>
<td>4,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>나무심기로 깨끗한 환경 가치있는 생태계 보전</td>
<td>창녕환경운동연합</td>
<td>4,500</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>7개사업</td>
<td>418,320</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>숲사랑캠페인 및 만화제작</td>
<td>가톨릭환경연대</td>
<td>17,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>숲속 어린이 자연체험장 만들기</td>
<td>대전생명의숲</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>어린이 숲속 체험교실</td>
<td>포항생명의숲</td>
<td>8,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>청소년 푸른숲 선도원 및 지도교사 교육</td>
<td>한그루녹색회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>산림내 자연체험활동 시설의 설치 및 교육홍보사업</td>
<td>한국여성환경운동본부</td>
<td>8,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>숲 체험행사</td>
<td>원주환경운동연합</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>『세계 산의해』와 연계한 TV광고 홍보사업</td>
<td>산림청</td>
<td>299,820</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>1개사업</td>
<td>17,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4-1</td>
<td>시민참여를 통한 소래산 생태문화 학습장 조성</td>
<td>시흥환경운동연합</td>
<td>17,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>4개사업</td>
<td>84,030</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-1</td>
<td>원앙이가 돌아오는 봉선사천 만들기</td>
<td>광릉숲보존협회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-2</td>
<td>울릉도 자생식물 지킴이 운동</td>
<td>우리식물살리기운동</td>
<td>9,030</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-3</td>
<td>한라산 훼손지 희귀식물 복원사업</td>
<td>제주자생식물동호회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-4</td>
<td>산림 생태계내 야생조수(꿩)복원사업</td>
<td>한국임정연구회</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>2개사업</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-1</td>
<td>야생동물을 배려한 임도구조물 설치방법 개발</td>
<td>충남대학교</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-2</td>
<td>산촌 녹색관광 프로그램 개발</td>
<td>생태산촌만들기</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>3개사업</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-1</td>
<td>한.중수교 10주년 기념 우의림 조성</td>
<td>동북아산림포럼</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-2</td>
<td>황사방지 한중미래숲조성 청년 봉사단 사업</td>
<td>황사방지<br /> 한중미래숲모임</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-3</td>
<td>북한강유역 북한 나무심기</td>
<td>한국임업후계자협회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>4개사업</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-1</td>
<td>제2회 숲사진공모전</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-2</td>
<td>도시산림생태계보전과 친자연적이용을 위한 생태교육사업</td>
<td>인천산림조합</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-3</td>
<td>시민과 함께 만드는 숲속 체험환경 교육장 조성</td>
<td>당진환경운동연합</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-4</td>
<td>어린이 대상 자연학습로 설치 및 운영</td>
<td>산림조합중앙회</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>