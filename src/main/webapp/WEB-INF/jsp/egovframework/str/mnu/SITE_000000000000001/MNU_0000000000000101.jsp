<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2003년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2003년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2003년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2">합계</td>
<td>46개사업</td>
<td>8,766,114</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>6개사업</td>
<td>3,854,894</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-1</td>
<td>한가족 푸른나무 가꾸기</td>
<td>(사)바다살리기<br /> 국민운동</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-2</td>
<td>생명과 숲 축제 2003</td>
<td>환경운동연합</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-3</td>
<td>도시내 소생물 서식공간 조성</td>
<td>지방자치단체</td>
<td>1,448,920</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-4</td>
<td>전통마을숲 복원</td>
<td>지방자치단체</td>
<td>499,950</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-5</td>
<td>숲탐방국민 운동</td>
<td>산림청, 생명의숲</td>
<td>1,496,024</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-6</td>
<td>제1회 서울국제환경영화제</td>
<td>환경재단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>3개사업</td>
<td>2,045,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>월영초등학교 학교 담장 녹화 및 경계 숲 조성 사업과 교육적 활용</td>
<td>월영초등학교</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>대지산 숲 가꾸기를통한 시민참여자연공원 만들기</td>
<td>환경정의시민연대</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>산림경영 모델숲 조성</td>
<td>산림조합중앙회</td>
<td>2,000,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>16개사업</td>
<td>1,889,246</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>연재기획 「소나무를 찾아서」/르포「소나무가 죽어간다」</td>
<td>문화일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>제3회 산림문화작품공모전</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>산불조심 홍보용 광고물 설치</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>산불예방 홍보</td>
<td>산 림 청</td>
<td>99,388</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>장애인 및 초등학생을 대상으로 한 숲 학교 운영</td>
<td>(사)숲해설가협회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>청소년수련장 자연체험활동시설 조성사업</td>
<td>(사)한국산림경영인협회</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>산불&middot;산사태 화보 및 해설집 발행</td>
<td>한국임업신문</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>청소년 푸른숲 선도원 및 지도교사 교육&middot;홍보</td>
<td>한그루녹색회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>산림보전 Trust</td>
<td>한국녹색문화재단</td>
<td>100,301</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>청소년녹색 교육센터 설치운영</td>
<td>한국녹색문화재단</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>산림문화홀 조성</td>
<td>한국녹색문화재단</td>
<td>38,019</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>청소년Green School운영</td>
<td>국립수목원, 시도 등</td>
<td>237,629</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>대학의 산림분야 사회교육지원</td>
<td>대학</td>
<td>273,909</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>「korean forest」영상물제작</td>
<td>어스커뮤니케이션</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>도서관 환경서적 지원사업</td>
<td>환경재단</td>
<td>190,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>한국녹색문화재단 설립운영</td>
<td>한국녹색문화재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>3개사업</td>
<td>37,800</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-1</td>
<td>야생동식물 및 생태계 보호사업(백두대간 생태숲조성운동)</td>
<td>백두대간보전회</td>
<td>2,800</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-2</td>
<td>한라산 아고산대 훼손지 복원 및 희귀식물 복원사업</td>
<td>제주자생식물동호회</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-3</td>
<td>설악산 산양 지키기 운동 및 야생동물학교 운영을 통한 야생동물 보호 확산사업</td>
<td>환경운동연합(속초)</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>8개사업</td>
<td>330,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-1</td>
<td>산지계류지역의 생태계보전방안</td>
<td>강원대학<br /> 산림과학연구소</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-2</td>
<td>대도시 토양 및 초목의 오염실태와 대기오염물질 저감을 위한 초목관리 정책방향</td>
<td>국회환경포럼</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-3</td>
<td>꼬리치레 도룡뇽을 통한 산림환경 지표개발 사업</td>
<td>녹색연합</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-4</td>
<td>도시인접 산림을 이용한 자연체험학습장 조성계획과 체험프로그램 개발 및 활용</td>
<td>(사)자연생태연구소</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-5</td>
<td>학교산림환경교육 프로그램 개발 및 시연</td>
<td>한국임정연구회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-6</td>
<td>생활환경림 도입 및 총량적 확보를 위한 법적 근거 등의 제도적 장치 검토</td>
<td>(사)환경과 자치연구소</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-7</td>
<td>산림포럼사업</td>
<td>한국임정연구회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-8</td>
<td>청소년녹색교육게임개발</td>
<td>생명의숲</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>4개사업</td>
<td>324,975</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-1</td>
<td>황사방지를 위한 중국 내몽골 조림사업</td>
<td>(사)동북아산림포럼</td>
<td>44,991</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-2</td>
<td>북한지역 조림을 위한 평양 시범양묘장 조성</td>
<td>평화의 숲</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-3</td>
<td>사막화 방지를 위한 한&middot;중 환경협력 사업</td>
<td>환경운동연합</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-4</td>
<td>사막화 방지 조림사업</td>
<td>동북아산림포럼</td>
<td>99,984</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>6개사업</td>
<td>284,199</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-1</td>
<td>나무를심은사람, L｀hommequi plantaitdes orbres</td>
<td>극단 유시어터</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-2</td>
<td>생태산촌 주택설계 공모전</td>
<td>(사)생태산촌만들기</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-3</td>
<td>자연생태해설가 양성을 통한 마을 숲 운용사업</td>
<td>생명의숲(춘천&middot;홍천)</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-4</td>
<td>어린이 자연 체험장 및 생태 지도 만들기</td>
<td>생명의숲(대전충남)</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-5</td>
<td>자연사랑 문학제 - 자연을 알고 사랑하고 알리기</td>
<td>(사)자연을 사랑하는 문학의 집</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-6</td>
<td>녹색환경추모공원 조성</td>
<td>산림조합중앙회</td>
<td>9,199</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>