<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p></p>
<h3 class="icon1">2004년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2004년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2004년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>28개사업</strong></td>
<td><strong>16,124,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>1개사업</td>
<td>635,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-1</td>
<td>산림경영모델숲 조성사업</td>
<td>산림조합중앙회</td>
<td>635,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>3개사업</td>
<td>3,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>서울 숲 공원 내 대규모 도시생태 숲 조성</td>
<td>(재)서울그린트러스트</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>도시내 소규모생물 서식공간조성</td>
<td>한국녹색문화재단</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>전통마을 숲 복원사업</td>
<td>생명의 숲</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>15개사업</td>
<td>10,326,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>청소년 녹색교육센타설치 운영</td>
<td>한국녹색문화재단</td>
<td>7,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>산림문화홀 조성</td>
<td>한국녹색문화재단</td>
<td>850,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>21세기형 체험학습장 조성 및 산림생태문화 체험교육의 활성화</td>
<td>유네스코한국위원회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>친자연 산책보도를 이용한 자연생태관찰로 조성</td>
<td>인천산림조합</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>산림생태환경 교원 및 청소년산림체험교육 지원</td>
<td>한국녹색문화재단</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>청소년 그린스쿨 운영</td>
<td>한국녹색문화재단</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>대학의 산림분야 사회교육지원</td>
<td>한국녹색문화재단</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>제4회 산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>59,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>청소년 푸른숲 선도원 및 지도교사홍보&middot;교육사업</td>
<td>(사)한그루녹색회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>제4회 자연사랑문학제</td>
<td>자연을사랑하는<br /> 문학의집</td>
<td>32,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>숲탐방 운동</td>
<td>생명의 숲</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>친환경 목조건축대전 2004(korea wood design awards)</td>
<td>(사)목재문화포럼</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>환경재단 그린페스티벌 국제환경영화제</td>
<td>환경재단</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>어린이 산림교육 뮤지컬 노빈손</td>
<td>환경운동연합</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>2개사업</td>
<td>1,400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-1</td>
<td>지리산권을 중심으로한 지속적인 환경숲 관리사업</td>
<td>남원산림조합</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-2</td>
<td>핵심 산림생태계보호&middot;복원 및 산사랑 범국민운동 지원사업</td>
<td>한국산지보전협회</td>
<td>1,300,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>2개사업</td>
<td>142,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-1</td>
<td>설명식 해설판에서 자기주도적 체험교육학습장 조성 등</td>
<td>숲 연구소</td>
<td>42,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-2</td>
<td>지속가능한 사회를 위한 산림포럼사업</td>
<td>(사)한국임정연구회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>4개사업</td>
<td>521,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-1</td>
<td>북한 황폐지 복구를 위한 양묘장지원 및 긴급 조림사업</td>
<td>(사)평화의숲</td>
<td>144,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-2</td>
<td>황사와 사막화를 위한 한중우의림조성 및 대학생 대토론회</td>
<td>한중문화청소년미래숲</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-3</td>
<td>사막화 방지를 위한 중국과 몽골 조림사업</td>
<td>(사)동북아산림포럼</td>
<td>177,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-4</td>
<td>사막화방지를위한 한&middot;중 환경협력사업</td>
<td>환경운동연합</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>1개사업</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-1</td>
<td>국민참여캠페인숲 희망 그리고 미래</td>
<td>(주)ABS농어민방송</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>