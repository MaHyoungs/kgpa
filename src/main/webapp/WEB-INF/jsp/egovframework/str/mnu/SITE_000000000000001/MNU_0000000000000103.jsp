<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2005년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2005년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2005년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>29개사업</strong></td>
<td><strong>21,873,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>2개사업</td>
<td>3,947,000</td>
<td>　</td>
</tr>
<tr>
<td>1-1</td>
<td>산림경영모델숲조성사업</td>
<td>산림조합중앙회</td>
<td>1,231,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-2</td>
<td>맑은물 공급과 수원함양을 위한 수질정화시설 및 녹색댐 조성사업</td>
<td>산림조합중앙회</td>
<td>2,716,000</td>
<td>1,147,611</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>3개사업</td>
<td>2,403,000</td>
<td>　</td>
</tr>
<tr>
<td>2-1</td>
<td>도시내 소규모생물 서식공간 조성</td>
<td>한국녹색문화재단</td>
<td>1,233,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>전통마을숲 복원사업</td>
<td>생명의숲국민운동</td>
<td>1,070,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>생활환경림(난대림) 조성사업</td>
<td>완도군산림조합</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>14개사업</td>
<td>10,996,000</td>
<td>　</td>
</tr>
<tr>
<td>3-1</td>
<td>청소년 녹색교육센터 설치&middot;운영</td>
<td>한국녹색문화재단</td>
<td>6,083,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>그린스쿨 운영</td>
<td>한국녹색문화재단</td>
<td>869,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>대학의 산림분야 사회교육 지원</td>
<td>산림조합중앙회</td>
<td>348,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>51,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>제5회 산림문화작품공모전</td>
<td>산림조합중앙회</td>
<td>156,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>청소년 산림문화 체험관</td>
<td>산림조합중앙회</td>
<td>1,304,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>Green Korea 운동</td>
<td>(주)경향신문사</td>
<td>87,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>산림자원의 육성, 보전 및 활용에 관한 TV다큐멘터리</td>
<td>한국임학회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>청소년 푸른숲선도원 및 지도교사 홍보&middot;교육사업</td>
<td>(사)한그루녹색회</td>
<td>26,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>친환경 목조건축대전 2005</td>
<td>(사)목재문화포럼</td>
<td>65,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>숲탐방운동</td>
<td>생명의숲국민운동</td>
<td>1,085,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>2005 그린페스티벌 제2회 서울국제환경영화제</td>
<td>환경재단</td>
<td>435,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>어린이 환경영상음악극&ldquo;빛그림이야기</td>
<td>환경운동연합</td>
<td>156,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>산림환경영화 제작</td>
<td>환경재단</td>
<td>261,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>2개사업</td>
<td>335,000</td>
<td>　</td>
</tr>
<tr>
<td>4-1</td>
<td>수목장림 연구용역사업(녹색자금사업)</td>
<td>산림조합중앙회</td>
<td>135,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>4-2</td>
<td>꽃이 좋은 숲속 야생초화원 조성사업</td>
<td>인천산림조합</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>2개사업</td>
<td>1,400,000</td>
<td>　</td>
</tr>
<tr>
<td>5-1</td>
<td>지리산권을 중심으로한 지속적 환경 숲 관리사업</td>
<td>남원산림조합</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>5-2</td>
<td>핵심 산림생태계 보호&middot;복원 및 산사랑 범국민운동 지원사업</td>
<td>한국산지보전협회</td>
<td>1,300,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>2개사업</td>
<td>2,042,000</td>
<td>　</td>
</tr>
<tr>
<td>6-1</td>
<td>지속가능한 사회를 위한 산림포럼 사업</td>
<td>한국임정연구회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-2</td>
<td>청소년 녹색교육 게임개발(사업포기)</td>
<td>생명의숲국민운동</td>
<td>1,942,000</td>
<td>포기</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>4개사업</td>
<td>750,000</td>
<td>　</td>
</tr>
<tr>
<td>7-1</td>
<td>북한 황폐지 복구를 위한 양묘장 지원 및 긴급 조림사업</td>
<td>(사)평화의 숲</td>
<td>210,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-2</td>
<td>사막화 방지를 위한 중국과 몽골 조림사업</td>
<td>동북아산림포럼</td>
<td>290,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-3</td>
<td>사막화방지를 위한 한&middot;중환경협력사업</td>
<td>환경운동연합</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-4</td>
<td>북한지역 밤나무 시범 조림 사업</td>
<td>한국산림경영인협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>