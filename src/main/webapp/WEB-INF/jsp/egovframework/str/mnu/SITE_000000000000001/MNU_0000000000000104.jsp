<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2006년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2006년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2006년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>29개사업</strong></td>
<td><strong>20,527,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>1개사업</td>
<td>5,955,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>1-1</td>
<td>산림환경기능 증진을 위한 푸른숲 조성사업</td>
<td>산림조합중앙회</td>
<td>5,955,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>1개사업</td>
<td>2,500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>생활환경림 복원 및 조성사업</td>
<td>한국녹색문화재단</td>
<td>2,500,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>3개사업</td>
<td>10,072,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>청소년 녹색교육센터 설치&middot;운영</td>
<td>한국녹색문화재단</td>
<td>6,400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>녹색문화 체험교육사업</td>
<td>한국녹색문화재단</td>
<td>2,572,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>녹색문화 홍보 및 컨텐츠 개발보급사업</td>
<td>한국녹색문화재단</td>
<td>1,100,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>1개사업</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7-1</td>
<td>산림환경기능 증진을 위한 국제교류 협력사업</td>
<td>한국녹색문화재단</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>1개사업</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>8-1</td>
<td>백두대간&middot;비무장지대 산림보전 및 핵심산림생태계 복원사업</td>
<td>한국산지보전협회</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>