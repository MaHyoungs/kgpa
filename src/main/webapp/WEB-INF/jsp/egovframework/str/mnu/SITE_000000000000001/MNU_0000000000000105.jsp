<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2007년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2007년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2007년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>40개사업</strong></td>
<td><strong>19,704,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>24개사업</td>
<td>13,622,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>병문천&middot;한천 푸른 숲 조성</td>
<td>제주자치도</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>시청앞 광장 도시생태 숲 조성</td>
<td>광주광역시청</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>함평천지 도시공원 조성</td>
<td>함평군청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>무궁화와 함께하는 도시 숲 조성</td>
<td>완주군청</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>폴리텍대학 담장개방 및 수성구청사 옥상녹화</td>
<td>대구광역시청</td>
<td>405,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>상인동~영운천간 도시생태 숲 조성</td>
<td>곡성군청</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>세계 평화의 숲 만들기</td>
<td>생명의숲국민운동</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>공단 배후도시(인동) 시설녹지 도시숲 조성</td>
<td>구미시청</td>
<td>720,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>강릉단오 산림공원 조성</td>
<td>강원도청</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>시민의 숲 조성</td>
<td>대전광역시청</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>10만 녹색지붕 만들기 사업</td>
<td>서울특별시청</td>
<td>675,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>도시산림 건강평가 모니터링 사업</td>
<td>한국산지보전협회</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>동해 부곡동 해안 숲 복원</td>
<td>생명의숲국민운동</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>충북 청천 후평숲 복원</td>
<td>생명의숲국민운동</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>손잡고 밀어주며 나란히 걷는 세하 다님길</td>
<td>금장학원</td>
<td>82,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>산책로 정비사업(대구)</td>
<td>생명의숲국민운동</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>장애우 및 노약자 등을 위한 체험 산책로 정비</td>
<td>건국대산학협력단</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>예양공원 장애우?노약자를 위한 체험산책로</td>
<td>장흥군산림조합</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>산책로 정비사업(울산)</td>
<td>생명의숲국민운동</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>장애우 및 노약자 산책로 조성</td>
<td>부산대학교</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>숲길&middot;체험산책로 조성(춘천)</td>
<td>생명의숲국민운동</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>교룡산성 치유로 조성사업</td>
<td>남원시산림조합</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>숲길&middot;체험산책로 조성(포항)</td>
<td>생명의숲국민운동</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>환지리산 트레킹코스 조성사업</td>
<td>(사)숲길</td>
<td>2,000,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>16개사업</td>
<td>6,082,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>제7회 산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>HD &ldquo;아름다운 우리숲&rdquo;다큐멘터리 제작&middot;방송</td>
<td>㈜산야TV</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>산사랑 국민운동 홍보사업</td>
<td>한국산지보전협회</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>우리 숲과 생활문화 국민체험 캠페인</td>
<td>한국녹색문화재단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>국산목재로 만든 학생용 책&middot;걸상 보급</td>
<td>산림조합중앙회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>제3기 그린코리아 운동</td>
<td>㈜경향신문사</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>LNT등산문화 교육프로그램개발 &middot;홍보</td>
<td>한국산림휴양학회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>청소년 산림문화 컨텐츠 공모대전</td>
<td>한그루녹색회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>SFM이행 확산을 위한 Round Table 운영</td>
<td>한국산림정책연구회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>2007 친환경 목조건축대전</td>
<td>목재문화포럼</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>국산 우수 임산물 홍보 박람회</td>
<td>한국임업후계자협회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>바람직한 수목장 문화 정착</td>
<td>한국산림정책연구회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>지자체 녹색건정성 평가&middot;시상사업</td>
<td>녹색자금관리단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>청소년 녹색교육센터 운영사업</td>
<td>한국녹색문화재단</td>
<td>2,500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>녹색체험 교육사업</td>
<td>한국녹색문화재단</td>
<td>1,867,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>숲체원 오수처리 시설 추가설치사업</td>
<td>한국녹색문화재단</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>