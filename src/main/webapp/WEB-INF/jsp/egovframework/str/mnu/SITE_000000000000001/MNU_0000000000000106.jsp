<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2008년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2008년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2008년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>49개사업</strong></td>
<td><strong>14,045,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>11개사업</td>
<td>9,739,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>제주 병문천 푸른 숲 조성</td>
<td>제주특별자치도</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>시청 앞 광장 도시생태 숲 조성</td>
<td>광주광역시</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>함평천지 도시공원 조성</td>
<td>함평군</td>
<td>540,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>무궁화와 함께하는 도시 숲 조성</td>
<td>완주군</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>폴리텍대학 담장개방 녹화</td>
<td>대구광역시</td>
<td>365,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>세계 평화의 숲 만들기</td>
<td>생명의숲국민운동</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>공단 배후도시(인동) 시설녹지 도시숲 조성</td>
<td>구미시</td>
<td>648,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>강릉단오 산림공원 조성</td>
<td>강원도</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>시민의 숲 조성</td>
<td>대전광역시</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>사회복지 시설 내 녹화(녹색희망 프로젝트)</td>
<td>서울특별시</td>
<td>1,486,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>환지리산 트레킹코스 조성</td>
<td>(사)숲길</td>
<td>1,500,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>38개사업</td>
<td>3,806,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>청소년녹색교육센터 운영(숲체원 운영)</td>
<td>한국녹색문화재단</td>
<td>750,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>소외계층 체험교육</td>
<td>한국녹색문화재단</td>
<td>850,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>제8회 산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>국산목재로 만든 학생용 책상&middot;의자 보급</td>
<td>산림조합중앙회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>산사랑 국민운동 홍보</td>
<td>한국산지보전협회</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>제4기 Green Korea 운동</td>
<td>경향신문사</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>HD &ldquo;아름다운 우리 숲&rdquo; 다큐멘터리 제작&middot;방송</td>
<td>산야TV방송</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>2008 대한민국 목조건축대전</td>
<td>목재문화포럼</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>수목장 문화의 정착과 홍보 및 지도를 통한 국토의 보전</td>
<td>수목장실천회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>SFM 이행을 위한 Round Table 운영</td>
<td>한국산림정책연구회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>청소년 산림문화 컨텐츠 공모전</td>
<td>한그루녹색회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>건전한 산림문화 정착을 위한 친환경 산림문화 교육프로그램 운영&middot;홍보</td>
<td>한국산림휴양학회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>그린 아카이브를 활용한 산림문화&middot;홍보 소프트웨어</td>
<td>환경재단</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>산림안전문화 정착을 위한 홍보 및 컨텐츠 개발</td>
<td>한국산악문화협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>산림의 공익적 가치홍보</td>
<td>한국임업후계자협회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>산림문화&middot;홍보(3D입체 영상물 제작)</td>
<td>울진세계친환경엑스포</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>산림환경기능증진을 위한 토석(채석) 문화홍보</td>
<td>한국채석협회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>우리나라 명품 숲 100선(영문)</td>
<td>숲과문화연구회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>지자체 녹색건전성 평가 및 시상사업</td>
<td>녹색자금관리단<br /> (산림청)</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>기후변화 대응 민&middot;관 이행 프로그램</td>
<td>녹색자금관리단<br /> (산림청)</td>
<td>141,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>푸른 한반도 만들기 범국민 실천 프로그램</td>
<td>녹색자금관리단<br /> (산림청)</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>중증장애 청소년 통합 녹색문화 체험교육</td>
<td>성분도복지관</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>한택식물원 청소년 녹색문화 체험교육</td>
<td>한택식물원</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>지역사회 생태자원을 활용한 청소년 녹색문화 체험교육</td>
<td>녹색소비자연대</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>푸른숲 선도원 학교별 활동지원</td>
<td>한그루녹색회</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>청소년을 위한 &lsquo;숲과 예술, 그리고 비전이 만나다&rsquo;</td>
<td>여성환경연대</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>사회인 녹색문화 체험교육</td>
<td>경북대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>대학의 산림&middot;환경 사회교육 지원</td>
<td>강원대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>2008 강릉 시민자연교실(강릉생명의숲)</td>
<td>생명의숲국민운동</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>녹색문화 체험교육</td>
<td>전북대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>서울대학교 산림분야 사회교육</td>
<td>서울대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>산주와 일반인을 위한 산림교육</td>
<td>고려대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>산주와 일반인을 위한 산림학교 운영</td>
<td>국민대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>녹색문화 체험교육</td>
<td>순천대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>사회인 녹색문화 체험교육</td>
<td>충남대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>녹색문화 체험교육</td>
<td>경상대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>숲해설 및 숲체험 지도자 심화교육</td>
<td>한국임학회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>1개사업</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>6-1</td>
<td>위성 영상자료를 이용한 북한지역 산림 황폐지 실태 파악 및 개성지역 조림 CDM사업 대상지 선정 연구</td>
<td>한국산림과학원</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>