<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>

<h3 class="icon1">2009년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2009년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2009년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>합계</strong></td>
<td><strong>81개사업</strong></td>
<td><strong>12,773,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>43개사업</td>
<td>9,830,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>외국인 주민센터 주변녹지 보완공사</td>
<td>안산시</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>소외계층 녹색복지 증진사업</td>
<td>인애원</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>사랑과 행복 가득한&lt;어르신 쉼터 조성&gt;</td>
<td>신흥사복지원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>너브내 노인전문요양원 녹색복지 증진을 위한 조성</td>
<td>중앙원</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>아동보호기관 옥상녹화</td>
<td>대구광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>소외계층 녹색복지 증진</td>
<td>울산명성복지재단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>녹화사업을 통한 개방쉼터 조성 &ldquo;Rest Silver Green"</td>
<td>샬롬복지재단</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>나눔과 희망의 행복한 숲 조성</td>
<td>길보경애원</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>소외 노인계층의 심리요양을 위한 최적 녹색공간 조성</td>
<td>호서복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>복합노인복지타운 녹지공간 조성</td>
<td>진안군</td>
<td>350,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>소외계층의 건강과 복지를 위한 웰빙숲 조성</td>
<td>영주시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>장애아동들의 치료정원</td>
<td>천애원</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>시설이용 어르신들의 건강한 노후생활을 위한 숲 조성</td>
<td>해송복지원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>녹색 3G사업을 통한 지역사회 통합프로그램</td>
<td>온누리복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>함께하는 마을에 피어난 무지개공원 만들기</td>
<td>원주카톨릭사회복지회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>옥상정원</td>
<td>엘림복지회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>장기요양기관 입소 거주자 및 지역주민을 위한 &ldquo;행복동산&rdquo; 조성</td>
<td>정애캐어</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>영락모자원 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>녹색복지 건강 숲 조성</td>
<td>완주군</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>소외계층 녹색복지 증진</td>
<td>산청군</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>건강산책로 휴게 공간 조성</td>
<td>무주군</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>사회복지시설 녹색희망 프로젝트</td>
<td>한국사회복지사협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>사회복지시설 녹지공간 실태분석 및 개선방안</td>
<td>영남대학교</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>녹색 건강한 푸른 숲 조성</td>
<td>함평군</td>
<td>450,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-25</td>
<td>강릉단오 산림공원 조성</td>
<td>강원도&middot;강릉시</td>
<td>330,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-26</td>
<td>지붕위의 녹색바람-경기중소기업 종합지원센터</td>
<td>경기농림진흥재단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-27</td>
<td>유가 전통 건강숲 조성</td>
<td>안동시</td>
<td>450,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-28</td>
<td>대구시 경관 숲 조성</td>
<td>대구광역시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-29</td>
<td>팔도 내고향 숲 만들기</td>
<td>생명의숲국민운동</td>
<td>450,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-30</td>
<td>금산 건강 숲 조성</td>
<td>금산군</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-31</td>
<td>생태한방 건강 숲 조성</td>
<td>제천시</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-32</td>
<td>대전 명품 숲 조성</td>
<td>대전광역시</td>
<td>350,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-33</td>
<td>모후산 생태 내남천 숲 조성</td>
<td>화순군</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-34</td>
<td>꽃마을 웰빙 숲길 조성</td>
<td>부산광역시서구청</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-35</td>
<td>도시지역 녹색공간 유형의 체험마케팅을 통한 녹색문화 확산 전략</td>
<td>공주대학교</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-36</td>
<td>녹색 웰빙 숲 기반조성 프로그램 개발</td>
<td>강원대학교</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-37</td>
<td>환지리산트레일코스 조성</td>
<td>(사)숲길</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-38</td>
<td>환지리산트레일코스 조성</td>
<td>구례군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-39</td>
<td>환지리산트레일코스 조성</td>
<td>하동군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-40</td>
<td>환지리산트레일코스 조성</td>
<td>남원시</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-41</td>
<td>환지리산트레일코스 조성</td>
<td>산청군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-42</td>
<td>울릉 숲길 조성</td>
<td>울릉군</td>
<td>670,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-43</td>
<td>곡성 숲길 조성</td>
<td>곡성군</td>
<td>330,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>38개사업</td>
<td>2,943,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>국산목재로 만든 학생용 책상&middot;의자 보급</td>
<td>산림조합중앙회</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>산림문화 체험전(EXPO)</td>
<td>미래정책연구회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>녹색성장 인식 확산을 위한 청소년 녹색봉사활동</td>
<td>한국녹색문화재단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>시각장애인을 위한 숲속 동식물 점자도감 발행</td>
<td>대전시시각장인연합회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>녹색성장시대 산림의 역할 제고를 위한 국민참여형 캠페인</td>
<td>중앙일보문화사업</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>산림보전&middot;문화 홍보</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>지역주민과 함께하는 우리산림 LOVE 축제</td>
<td>청계포럼</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>녹색성장을 위한 산림인증제도 정착 및 대국민홍보</td>
<td>한국산림정책연구회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>Green Korea 운동</td>
<td>경향신문사</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>산림문화의 또 다른 가치! 숲속 유치원</td>
<td>인천대학교</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>녹색기획 &lsquo;한국의 숲&rsquo;</td>
<td>GTB(주)강원민방</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>Green-임산물 식품전시 홍보</td>
<td>한국임업후계자협회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>저탄소 녹색시대를 여는 녹색지킴이 홍보단</td>
<td>경남신문사</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>청소년 산림문화 컨텐츠 공모전</td>
<td>그린레인저</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>수목장 문화의 정착과 홍보 및 지도를 통한 국토의 보전</td>
<td>수목장실천회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>산림환경기능 증진을 위한 토석문화 홍보</td>
<td>한국채석협회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>L.N.T 운동을 이용한 건전한 등산문화 홍보</td>
<td>한국등산지원센터</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>대한민국 목조건축대전</td>
<td>목재문화포럼</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>한밭수목원 자연사랑 축제</td>
<td>중도일보</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>한국의 Korea Big Tree 발굴 및 보전</td>
<td>녹색사업단</td>
<td>350,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>지자체 녹색건전성 평가&middot;시상</td>
<td>녹색사업단</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>녹색문화 체험교육</td>
<td>경상대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>사회인 녹색문화 체험교육</td>
<td>순천대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>녹색봉사단</td>
<td>미래숲</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>중증장애인 통합 녹색체험 교육 &lsquo;신바람 산바람&rsquo;</td>
<td>성분도복지관</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>녹색체험 교육사업</td>
<td>전북대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>녹색숲길 체험</td>
<td>자연과사람</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>사회인 녹색문화 체험교육</td>
<td>경북대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>녹색일자리 창출을 위한 숲 치유사 양성과정</td>
<td>한국녹색문화재단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>산주와 일반인을 위한 산림교육</td>
<td>고려대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>사회인 녹색문화 체험교육</td>
<td>동국대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>저탄소 녹색성장에 부응한 대학의 산림환경 지원</td>
<td>강원대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>푸른숲 선도원 학교별 활동지원</td>
<td>그린레인저</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>사회인 녹색문화 체험교육</td>
<td>충남대학교</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>청소년 가족 숲체험 전문가 양성</td>
<td>국립중앙과학원</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>소외계층 체험교육</td>
<td>한국녹색문화재단</td>
<td>843,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>