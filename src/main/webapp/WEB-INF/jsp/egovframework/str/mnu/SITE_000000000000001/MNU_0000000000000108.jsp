<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2010년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2010년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2010년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>[2010년도 사업]</strong></td>
<td><strong>111개사업</strong></td>
<td><strong>15,841,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>57개사업</td>
<td>8,478,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>늘편한집 지상부 녹지 조성(천애원)</td>
<td>서울특별시청</td>
<td>220,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>다니엘복지원 지상부 녹지 조성</td>
<td>서울특별시청</td>
<td>170,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>신아재활원 지상부 녹지 조성</td>
<td>서울특별시청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>친환경 자연 테마가 있는 개방형 복지관 활성화(사하구종합복지관)</td>
<td>부상광역시청</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>소양 녹색숲 쉼터 조성(소양보육원)</td>
<td>부산광역시청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>어르신과 지역주민들이 함께하는 지역중심 자연 녹색공간 조성(반석복지재단)</td>
<td>부산광역시청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>녹색허브 더불어 숲 조성(임실재활원)</td>
<td>대구광역시청</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>녹색복지공간 조성(한국SOS어린이마을)</td>
<td>대구광역시청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>영락재단 옥상녹화</td>
<td>대구광역시청</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>룸비니동산 옥상녹화</td>
<td>대구광역시청</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>우리들의 행복한 치료정원 만들기(색동원)</td>
<td>인천광역시청</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>장애인의 녹색복지공간 제공을 위한 옥상녹화(은광복지재단)</td>
<td>인천광역시청</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>녹색복지공간 조성(장봉혜림재활원)</td>
<td>인천광역시청</td>
<td>170,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>사회복지시설 열린녹색공간 조성(동명회)</td>
<td>광주광역시청</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>어르신과 함께하는 도심속 숨은 정원 "히든가든"(광주주서구노인복지관)</td>
<td>광주광역시청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>맑고 푸른 녹색복지마을 녹색공간 조성(바라밀)</td>
<td>광주광역시청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>행복이 가득한 녹색숲 조성(천양원)</td>
<td>대전광역시청</td>
<td>220,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>성애노인요양원 내 돌-바람이 머무는 들꽃정원 조성</td>
<td>대전광역시청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>녹색복지공간 조성(효도의집)</td>
<td>울산광역시청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>중증장애인의 통합감각 치료를 위한 "너와 내가 만드는 행복한 green island"(은혜재단)</td>
<td>경기도청</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>중증장애인의 삶의 질 향상을 위한 녹지환경 조성(사랑과평화복지재단)</td>
<td>경기도청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>중증장애인의 삶의 질 향상을 위한 녹색프로젝트(혜성원)</td>
<td>경기도청</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>Green! Creen! 숲속 작은마을(생수의집)</td>
<td>경기도청</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>지역사회와 함께하는 "행복 나눔의 녹색공원"만들기(경신재단)</td>
<td>경기도청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-25</td>
<td>장애인들의 심신건강을 위한 초록놀이터(향림원)</td>
<td>경기도청</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-26</td>
<td>상애원 실버숲 조성</td>
<td>강원도청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-27</td>
<td>녹색과 함께하는 행복지수 충전(실버홈)</td>
<td>강원도청</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-28</td>
<td>철원군 노인전문 요양원 주변 치유의숲 조성</td>
<td>강원도청</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-29</td>
<td>재미있는 오솔길 조성(가은복지재단)</td>
<td>강원도청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-30</td>
<td>녹색, 그 자연 속으로(음성꽃동네)</td>
<td>충청북도청</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-31</td>
<td>녹색나눔터 조성(다래동산)</td>
<td>충청북도청</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-32</td>
<td>시설어르신들이 지역주민과 함께하는 푸른숲 조성(죽림원)</td>
<td>충청남도청</td>
<td>170,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-33</td>
<td>서천 어메니티 복지마을녹색복지공간 조성(서천군)</td>
<td>충청남도청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-34</td>
<td>전주사랑의집 치유정원 조성(전주카톨릭사회복지관)</td>
<td>전라북도청</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-35</td>
<td>보은의집 희망의숲 조성(중도원)</td>
<td>전라북도청</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-36</td>
<td>나눔과 희망의 &ldquo;행복한 숲&rdquo;(순천성신원)</td>
<td>전라남도청</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-37</td>
<td>소향원 어르신들의 자연치유를 위한 녹색숲 조성(소향복지재단)</td>
<td>전라남도청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-38</td>
<td>SMILE 사랑의 꽃동산 조성(새암복지재단)</td>
<td>전라남도청</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-39</td>
<td>녹색복지재단 조성(희망세상)</td>
<td>상주시청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-40</td>
<td>건강숲 조성(장수마을)</td>
<td>영주시청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-41</td>
<td>녹색복지공간 조성사업(자비동산)</td>
<td>영주시청</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-42</td>
<td>건강숲 조성 &ldquo;너와 내가 손잡고 함께 걷는 희망의 숲&rdquo;(대성사회복지재단)</td>
<td>안동시청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-43</td>
<td>&ldquo;합천군립노인전문요양원&rdquo; 주변환경 녹색조성</td>
<td>합천군청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-44</td>
<td>시설생활 이용어르신 및 지역주민의 심신건강을 위한녹색복지공간 조성(진주노인센터)</td>
<td>진주시청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-45</td>
<td>노인복지시설 녹화와 휴양쉼터 마련(가야)</td>
<td>함안군청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-46</td>
<td>장애인과 지역주민의 쉼과 건강증진을 위한 옥상녹화 조성(경남장애인종합복지관)</td>
<td>경상남도청</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-47</td>
<td>푸르름 속으로 떠나는 치유여행(제주원광요양원)</td>
<td>제주특별자치도청</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-48</td>
<td>&ldquo;하늘정원&rdquo; 자연치료정원 조성(위미에덴요양원)</td>
<td>제주특별자치도청</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-49</td>
<td>사회복지시설 녹색희망 디딤돌</td>
<td>한국사회복지사협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-50</td>
<td>장애인 및 휠체어 전용 휴양 숲 조성</td>
<td>고창군청</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-51</td>
<td>광양인 도시림 시범 모델 숲</td>
<td>광양시청</td>
<td>178,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-52</td>
<td>서면 산림 테마파크 조성</td>
<td>춘천시청</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-53</td>
<td>아양루 어울림숲 조성</td>
<td>대구동구청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-54</td>
<td>동천 하천생태 숲 조성</td>
<td>울산중구청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-55</td>
<td>낙동강 수변 녹색 나눔 숲 조성</td>
<td>상주시청</td>
<td>250,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-56</td>
<td>아름다운 사랑의 녹색나눔 숲 조성</td>
<td>태안군청</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-57</td>
<td>월명공원 동수림분 조성</td>
<td>군산시청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>54개사업</td>
<td>7,363,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>환지리산트레일코스 조성</td>
<td>숲길</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>환지리산트레일코스 조성</td>
<td>하동군청</td>
<td>306,472</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>환지리산트레일코스 조성</td>
<td>구례군청</td>
<td>493,528</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>지리산숲길 조성사업 편익시설 설치(녹색자금사업)</td>
<td>숲길</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>지리산 둘레길 안내센터 건립(녹색자금사업)</td>
<td>구례군청</td>
<td>565,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>지리산 둘레길 안내센터 건립(녹색자금사업)</td>
<td>산청군청</td>
<td>565,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>울릉숲길 조성</td>
<td>울릉도</td>
<td>670,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>녹색체험시설 조성</td>
<td>한국녹색문화재단</td>
<td>830,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>마음으로 걷는 섬!! 그리고, 숲 !(도서벽지 소외청소년 숲체험 프로그램)</td>
<td>거제시청소년수련관</td>
<td>49,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>중증장애인 녹색체험 교육사업 &lsquo;신바람 산바람&rsquo;</td>
<td>성분도복지관</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>푸른숲선도원 학교별 활동지원</td>
<td>그린레인저</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>녹색일자리 창출을 위한 산림치유 전문가 교육과정</td>
<td>한국녹색문화재단</td>
<td>49,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>100대 명산 클린 캠프</td>
<td>한국등산지원센터</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>아토피 환아를 위한 &ldquo;Ato Camp&rdquo;</td>
<td>에코포리스트</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>청소년가족 숲체험 전문가 양성</td>
<td>국립중앙과학관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>발달장애아동/청소년을 위한 숲속학교 프로젝트</td>
<td>송석문화재단</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>미래숲 제9기 녹색봉사단</td>
<td>미래숲</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>2010 우리산림 LOVE 축제</td>
<td>환경과사람들</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>녹색체험교육(대학)</td>
<td>한국임학회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>소외계층 체험교육</td>
<td>한국녹색문화재단</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>아이들이 만들어가는 지구, 백만 그루의 나무를 심자!</td>
<td>한국자연다큐멘터리제작자협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>한국의 숲</td>
<td>GTB강원민방</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>(가제)녹색의 꿈 우리의 숲에 있어요</td>
<td>춘천MBC</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>국산 목재로 만든 학생용 책상․의자 보급</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>저탄소 녹색성장시대 산림의 역할 제고 캠페인</td>
<td>중앙일보문화사업</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>제10회 산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>대한민국 목조건축대전</td>
<td>목재문화포럼</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>전국 청소년 숲사랑 작품 공모전(구 청소년 산림문화 컨텐츠 공모전)</td>
<td>그린레인저</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>녹색성장시대에 부응한 산림 웰빙서비스 기능의 대국민 홍보</td>
<td>한국산림정책연구회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>&ldquo;저탄소 녹색성장 현장을 가다&rdquo; 화보집 제작 및 특집기사</td>
<td>중도일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>2010 녹색소리 전국청소년 합창제</td>
<td>대경미래문화협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>숲이 만드는 선진녹색복지</td>
<td>서울경제신문</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>한국의 산림문화 국제 홍보</td>
<td>국민대학교</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>내일신문 기획시리즈 &ldquo;녹색미래, 숲에서 답을 찾다</td>
<td>내일신문</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>산림환경기능증진을 위한 2010년도 토석문화 홍보</td>
<td>한국채석협회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>2010 다문화 가정과 함께 만드는 숲의 세계</td>
<td>경남도민일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>수목장 문화의 정착과 홍보 및 지도를 통한 국토의 보전</td>
<td>수목장실천회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-39</td>
<td>녹색역사보전사업</td>
<td>녹색사업단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-40</td>
<td>녹색기획 홍보사업</td>
<td>녹색사업단</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-41</td>
<td>산림보전 문화홍보 사업</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-42</td>
<td>10개 수목원 탐사 기획보도 및 관련 컨텐츠 개발보급</td>
<td>한겨레신문사</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-43</td>
<td>대한민국 Green Green Mountain 캠페인</td>
<td>스포츠조선</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-44</td>
<td>산림생태 소리와 산촌민요 재조명 홍보 및 CD 보급</td>
<td>강원도민일보</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-45</td>
<td>녹색사관학교</td>
<td>경상북도환경연수원</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-46</td>
<td>도시, 숲을 꿈꾸다 특집 다큐멘터리 제작</td>
<td>경기농림진흥재단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-47</td>
<td>Fell the Eco 환경을 느껴요! 환경체험 및 환경가요제</td>
<td>경남일보</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-48</td>
<td>뜨거운 지구를 살릴 미래의 숲을 찾아서</td>
<td>전남일보</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-49</td>
<td>나무이름표 달아주기 및 우수임산물 홍보전</td>
<td>한국임업후계자협회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-50</td>
<td>지리산 숲길 홍보</td>
<td>숲길</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-51</td>
<td>2010 산림문화체험전(EXPO)</td>
<td>미래정책연구원</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-52</td>
<td>산림청 숲해설교육 인증 프로그램 개발</td>
<td>숲과문화연구회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-53</td>
<td>행복하고 안전한 산행을 위한 홍보 서포터즈</td>
<td>대한산악구조협회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-54</td>
<td>숲사랑 실천운동 홍보</td>
<td>한국등산연합회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>