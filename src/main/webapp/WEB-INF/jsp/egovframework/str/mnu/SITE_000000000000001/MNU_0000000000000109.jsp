<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2011년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2011년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2011년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>[2011년도 사업]</strong></td>
<td><strong>143개사업</strong></td>
<td><strong>21,114,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>89개사업</td>
<td>11,713,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>선덕원 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>성동노인종합복지관 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>기쁜우리복지관 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>서부장애인종합복지관 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>시온원 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>친환경 초록길 조성사업(인천사회사업재단)</td>
<td>부산광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>실버하늘공원(행복한오늘)</td>
<td>부산광역시</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>쉼과 치유가 있는 아름다운 공원 녹색복지 조성(안나원)</td>
<td>부산광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>어르신과 지역주민들을 위한 옥상정원 및 숲조성(늘기쁜마을)</td>
<td>부산광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>옥상녹화(반여종합사회복지관)</td>
<td>부산광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>녹색복지 공간조성(화엄도량)</td>
<td>부산광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>행복 녹색복지숲 조성(대구안식원)</td>
<td>대구광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>효성실버타운 옥상 녹화</td>
<td>대구광역시</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>한사랑 녹색복지숲 조성(한사랑마을)</td>
<td>대구광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>더불어 녹색복지숲 조성(더불어진인마을)</td>
<td>대구광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>자연을 담아 자연을 닮은 장애인복지관(부평장애인복지관)</td>
<td>인천광역시</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>내리요양원 숲속 푸름정원</td>
<td>인천광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>거동불편 외상 어르신 정서지원을 위한 녹색환경 만들기(사랑노인요양원)</td>
<td>인천광역시</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>IFEZ 녹색복지 숲(인천노인종합문화회관)</td>
<td>인천광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>어르신들의 삶이 풍요로운 건강 정원(남동구노인복지관)</td>
<td>인천광역시</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>어르신이 행복한 녹색복지 숲 조성(인광의료재단)</td>
<td>광주광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>사랑 나눔 녹색복지 숲 조성(광주사랑의집)</td>
<td>광주광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>초록우산 조성(광주종합사회복지관)</td>
<td>광주광역시</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>하늘정원 만들기(누리보듬주간보호센터)</td>
<td>광주광역시</td>
<td>23,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-25</td>
<td>성우 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-26</td>
<td>후생학원 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-27</td>
<td>은혜 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-28</td>
<td>녹색복지 공간조성(시민복지재단)</td>
<td>울산광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-29</td>
<td>녹색복지 공간조성(신광노인전문요양원)</td>
<td>울산광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-30</td>
<td>녹색복지 공간조성(다비다노인요양원)</td>
<td>울산광역시</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-31</td>
<td>지역주민과 무료노인복지시설 어르신들이 함께할 아름다운 공원만들기(자혜원)</td>
<td>경기도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-32</td>
<td>시각장애인의 보행, 산책로 조성을 겸한 숲 조성(소망원)</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-33</td>
<td>아낌없이 주는 정원(안양노인전문요양원)</td>
<td>경기도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-34</td>
<td>장애인과 비장애인이 함께하는 희망 그린 정원(시흥장애인종합복지관)</td>
<td>경기도</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-35</td>
<td>행복한 노인요양원 녹색자금 옥상녹화 조성(행복한노인요양원)</td>
<td>경기도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-36</td>
<td>하늘공원(수리장애인복지관)</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-37</td>
<td>PIN 녹색치유 테마숲 조성(애향원)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-38</td>
<td>양지바른 숲 조성(계명복지재단)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-39</td>
<td>함께하는 푸른희망 쉼터 조성(강릉시장애인종합복지관)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-40</td>
<td>노(老) 노(NO) 녹색복지숲 조성(동해시노인종합복지관)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-41</td>
<td>생명 건강의숲 조성(오렌지카운티노인요양원)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-42</td>
<td>소외계층 입소어르신과 가족 및 지역주민을 위한 숲조성(서청주노인요양원)</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-43</td>
<td>시설입소 어르신들을 위한 옥상녹화 'a forest on my head'(평화원)</td>
<td>충청북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-44</td>
<td>녹색스케치(성보나벤뚜라노인요양원)</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-45</td>
<td>노인요양장기기관 인우원 옥상조경(인우원)</td>
<td>충청북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-46</td>
<td>다울(다함께 사는 우리) 복지환경 만들기(홍복양로원)</td>
<td>충청북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-47</td>
<td>녹색복지숲 조성을 통한 행복정원(삼휘복지재활원)</td>
<td>충청남도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-48</td>
<td>익선원 녹색복지숲(익선원)</td>
<td>충청남도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-49</td>
<td>백제의 향기 가득한 자연치유적 녹색숲 조성(건양복지원)</td>
<td>충청남도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-50</td>
<td>파랑새둥지 녹색복지숲 조성(파랑새둥지)</td>
<td>충청남도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-51</td>
<td>시설입소 어리신들 및 지역주민을 위한 푸르미 쉼터조성(장수원)</td>
<td>충청남도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-52</td>
<td>행복&middot;사랑치유의 숲 조성(성언복지관)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-53</td>
<td>정심원내 1개소 녹색공간 조성(송광)</td>
<td>전라북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-54</td>
<td>해피그린아이, 해피그린패밀리, 해피그린남원, 전북(햇빛)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-55</td>
<td>정신장애인들의 심리적 안정을 위한 "치유의 숲"(삼정원)</td>
<td>전라북도</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-56</td>
<td>함께해요! 녹색사랑(훈훈한동네)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-57</td>
<td>장애아동 더 먼저 녹색복지숲(베타니아)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-58</td>
<td>지역주민과 하나되는 누리세상(계산원)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-59</td>
<td>소외계층 녹색복지 증진(용욱노인전문요양원)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-60</td>
<td>비룡복지 특성화(비룡복지재단)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-61</td>
<td>2011년도 녹색자금지원(에덴원)</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-62</td>
<td>녹색복지숲 조성(영암노인전문요양원)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-63</td>
<td>은광마을숲 조성(은광마을)</td>
<td>경상북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-64</td>
<td>장애아동의 조기치료를 위한 녹색공간 사업 "해뜰"(효동어린이집)</td>
<td>경상북도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-65</td>
<td>영양웰타운 녹색복지공간 조성(영양웰타운)</td>
<td>경상북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-66</td>
<td>봉화유리요양원 치유녹색공간(안동유리요양원)</td>
<td>경상북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-67</td>
<td>어울림(林) 녹색복지숲(구미시장애인종합복지관)</td>
<td>경상북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-68</td>
<td>숲가꾸기사업(도리원노인복지사업단)</td>
<td>경상북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-69</td>
<td>우주봉 녹색복지숲(복지마을)</td>
<td>경상북도</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-70</td>
<td>사천실버요양원 녹색공간숲 조성(사천실버요양원)</td>
<td>경상남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-71</td>
<td>성심인애원 녹색복지숲(성심인애원)</td>
<td>경상남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-72</td>
<td>사랑으로 가득 찬 녹색복지숲(덕인노인전문요양원)</td>
<td>경상남도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-73</td>
<td>생활시설과 이용시설 이용자를 위한 녹색복지숲(거창노인<br /> 전문요양원)</td>
<td>경상남도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-74</td>
<td>함께 공유하는 아름다운 자연숲(창암재활원)</td>
<td>제주특별자치도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-75</td>
<td>섬속의 희망 녹색숲 조성(추자요양원)</td>
<td>제주특별자치도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-76</td>
<td>자은세상 쉼터 정원 조성(제광원)</td>
<td>제주특별자치도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-77</td>
<td>건강한 삶을 위한 웰빙걷기 테마숲 조성(제주정신요양원)</td>
<td>제주특별자치도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-78</td>
<td>사회복지시설 녹색복지증진 프로그램 개발</td>
<td>한국사회복지사협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-79</td>
<td>영강 녹색나눔숲 조성</td>
<td>문경시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-80</td>
<td>봉서골 녹색나눔숲 조성</td>
<td>완주군</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-81</td>
<td>순천만 보전을 위한 생태 하천숲 복원</td>
<td>순천시</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-82</td>
<td>단계천 생태하천 조성</td>
<td>원주시</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-83</td>
<td>하귀 큰구릉내 녹색나눔숲 조성</td>
<td>제주시</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-84</td>
<td>낙동강변 사벌국왕길</td>
<td>상주시</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-85</td>
<td>창원 "Sea &amp; Wood 향기나라" 조성</td>
<td>창원시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-86</td>
<td>판암동 녹색나눔숲 조성</td>
<td>대전광역시 동구</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-87</td>
<td>녹색공간 조성</td>
<td>증평군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-88</td>
<td>제5호 어린이공원 녹색나눔숲 조성</td>
<td>거창군</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-89</td>
<td>함평천지 녹색나눔숲 조성</td>
<td>함평군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>54개 사업</td>
<td>9,401,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>녹색 꿈 ! 그린전망대</td>
<td>한국녹색문화재단</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>지리산둘레길 조성사업</td>
<td>(사)숲길</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>지리산둘레길 조성사업</td>
<td>남원</td>
<td>146,892</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>지리산둘레길 조성사업</td>
<td>하동</td>
<td rowspan="2">653,108</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>지리산둘레길 조성사업</td>
<td>구례</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>울릉둘레길 조성사업</td>
<td>울릉군</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>사회복지나눔숲</td>
<td>녹색사업단</td>
<td>3,001,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>중증장애인 녹색체험교육 '신바람 산바람'</td>
<td>성분도복지관</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>지역아동센타와 함께하는 행복한 숲</td>
<td>경기농림진흥재단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>청소년가족 숲체험 전문가 양성</td>
<td>국립중앙과학관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>녹색사관학교 운영</td>
<td>경상북도환경연수원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>소외이웃과 함께하는 숲속체험</td>
<td>대전일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>장애청소년을 위한 숲속학교 프로젝트</td>
<td>송석문화재단</td>
<td>34,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>솔향 녹색학교</td>
<td>강릉문화원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>미래숲 제10기 녹색봉사단</td>
<td>미래숲</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>천혜의 경기북부 숲을 활용한 아토피숲 치유 캠프</td>
<td>경기농림진흥재단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>푸른숲 선도원 학교별 활동지원</td>
<td>그린레인저</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>100대 명산 클린캠프</td>
<td>한국등산지원센터</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>시설장애인과 비장애인의 감각자극 및 사회성 향상을 위한 녹색체험 프로그램</td>
<td>향림재활원</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>산지묘지 수목장 전환 문화창달 과정</td>
<td>수목장실천회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>금정 ECO 엔티어링</td>
<td>범어청소년동네</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>얘들아 아마존(Zone)에서 놀자</td>
<td>고강복지회관</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>녹색체험교육 "Fore School (포레스쿨)"</td>
<td>만덕종합사회복지관</td>
<td>36,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>소외계층 체험교육</td>
<td>한국녹색문화재단</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>산림문화예술 체험교육</td>
<td>국민대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>사회인 녹색체험 교육사업</td>
<td>경북대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>녹색체험 교육사업</td>
<td>경상대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>녹색체험 교육사업</td>
<td>순천대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>나눔, 상생, 공존의 숲</td>
<td>영남대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>녹색체험 교육사업</td>
<td>전북대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>사회인 녹색문화 체험교육사업</td>
<td>충남대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>녹색체험 교육사업</td>
<td>충북대학교</td>
<td>37,500</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>한국의숲</td>
<td>GTB강원민방</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>제11회 산림문화작품 공모전</td>
<td>산림조합중앙회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>대구MBC 특집 다큐 2부작 &lt;숲의 생명, 노거수이야기&gt;</td>
<td>대구MBC</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>대한민국 그린그린 마운틴 캠페인</td>
<td>스포츠조선</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>자연으로 가는 관문 충청의 마을 숲 30</td>
<td>충청투데이</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-39</td>
<td>우리의 미래 숲을 향해서</td>
<td>전남일보</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-40</td>
<td>국산목재로 만든 학생용 책상&middot;의자 보급</td>
<td>산림조합중앙회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-41</td>
<td>이야기가 있는 숲' 탐사기획 보도 및 동영상제작</td>
<td>한겨레신문사</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-42</td>
<td>초등학생 교과서 표지 특수비닐커버에 산림홍보</td>
<td>강원일보사</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-43</td>
<td>휴양림 콘서트 '숲 그리고 음악'</td>
<td>대전MBC</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-44</td>
<td>&ldquo;지하철로 떠나는 소통의 숲, 동행&rdquo; 연중 기획시리즈 및 일반홍보 기획기사 제작</td>
<td>CBS노컷뉴스</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-45</td>
<td>지속가능한 산림경영을 위한 국민의 인식제고 캠페인</td>
<td>중앙일보문화사업</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-46</td>
<td>나무이름표 달아주기 및 우수 임산물 홍보전</td>
<td>한국임업후계자협회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-47</td>
<td>산림웰빙서비스 기능 대국민 홍보</td>
<td>한국산림정책연구회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-48</td>
<td>건강을 주는 숲</td>
<td>서울경제신문</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-49</td>
<td>산림생태소리 온오프라인 소통과 치유문화 프로젝트</td>
<td>강원도민일보</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-50</td>
<td>2011 시민과 함께하는 우리산림 러브축제</td>
<td>환경과사람들</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-51</td>
<td>"기후변화를 이겨내는 산림바이오메스" 연재기사 및 자료개발 홍보</td>
<td>중도일보</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-52</td>
<td>산림보전･문화홍보</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-53</td>
<td>기획홍보사업</td>
<td>녹색사업단</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-54</td>
<td>녹색역사보전사업(KBT)</td>
<td>녹색사업단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>