<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2012년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2012년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2012년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>[2012년도 사업]</strong></td>
<td><strong>162개사업</strong></td>
<td><strong>27,122,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>121개사업</td>
<td>22,717,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>구세군강북종합사회복지관 녹색희망 프로젝트</td>
<td>구세군강북종합사회복지관</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>북부종합사회복지관 녹색희망 프로젝트</td>
<td>북부종합사회복지관</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>유자원 녹색희망 프로젝트</td>
<td>유자원</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>성민복지관 녹색희망 프로젝트</td>
<td>성민복지관</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>청운자립생활관 녹색희망 프로젝트</td>
<td>청운자립생활관</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>궁동종합사회복지관 녹색희망 프로젝트</td>
<td>궁동종합사회복지관</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>카톨릭데이케어센터 녹색희망 프로젝트</td>
<td>카톨릭데이케어센터</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간 조성사업</td>
<td>부산장애인종합복지관</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간 조성사업</td>
<td>만덕종합사회복지관</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간조성사업을 통한 어르신들의 삶의 질 향상&ldquo;Happy Senior Park"</td>
<td>(사)대한노인회 부산광역시연합회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>효성누리 녹색복지 숲</td>
<td>효성복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색 숲, 녹색 산책로 조성</td>
<td>좋은세상</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외시설 녹색공간 조성사업</td>
<td>그리스도요양원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외계층의 녹색복지공간 &ldquo;초록 뜰&rdquo;</td>
<td>공창종합사회복지관</td>
<td>65,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>함께가요 녹색세상으로～ 휴그린(休 GREEN)</td>
<td>범어금정노인요양원</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>연광 녹색복지숲 조성</td>
<td>도원복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>한뜰 녹색복지 숲 조성</td>
<td>화성복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>혜천 하늘공원 조성사업</td>
<td>구세군대구혜천원</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>진명실버타운 하늘공원 조성</td>
<td>진명복지재단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>신일 녹색복지숲 조성</td>
<td>신일복지재단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>시설장애인의 녹색복지공간 제공을 위한 옥상녹화사업</td>
<td>성촌재단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>휴식과 치유의 녹색복지 숲</td>
<td>인천천사전문요양원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>은혜의 녹색치유센타 조성사업</td>
<td>대한정신보건협회인천분사무소</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>시설 거주 장애인들의 원예치료와 쉼이 있는&ldquo;해피 가든&rdquo; 조성사업</td>
<td>중증장애인요양시설&nbsp;</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>나눔․희망․행복이 있는 녹색 복지숲 조성</td>
<td>송정권노인복지관</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>내집처럼 편안한 녹색 복지숲 조성</td>
<td>이일성노원</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>치매어르신을 위한 스마일 녹색 복지숲 조성</td>
<td>무등학원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소화 뜰 녹색 복지숲 조성</td>
<td>소화자매원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>선경우요양원 녹색 복지숲 조성</td>
<td>선경우동산</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>하람 녹색복지숲 조성사업</td>
<td>대전벧엘원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>혜생원 녹색복지숲 조성사업</td>
<td>구세군대전혜생원</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>금성 녹색복지숲 조성사업</td>
<td>금성복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>선우 녹색복지숲 조성사업</td>
<td>선우복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>실버랜드 녹색복지숲 조성사업</td>
<td>선아복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>도솔천노인전문요양원 녹색공간 조성사업</td>
<td>통도사자비원</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>희망을 담은 하늘정원</td>
<td>수연복지재단</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>울산노인의집 녹색공간 조성사업</td>
<td>울산한마음복지재단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>창인원 중증장애인들을 위한 녹색공간 조성사업</td>
<td>창인원</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>경산복지재단 중앙공원 녹색공간 조성사업 및 신축건물 옥상녹화사업</td>
<td>경산복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>김포시종합사회복지 녹색문화공간 조성사업</td>
<td>김포시종합사회복지관</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>과천시노인복지관 옥상녹화사업</td>
<td>과천시노인복지관</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>지역사회 통합을 위한 지역주민의 보금자리 &ldquo;도담채 공원&rdquo;</td>
<td>청솔종합사회복지관</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>와평 녹색복지 숲</td>
<td>원주가톨릭사회복지회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소통과 치유의 녹색복지 숲</td>
<td>예가노인전문요양원</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>치료와 나눔의 녹색복지 숲</td>
<td>정다운복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색과 함께하는 행복의 녹색복지 숲</td>
<td>홍천노인전문요양원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>보은 옥상치료정원 만들기</td>
<td>원광보은의집</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사람과 환경을 살리는 녹색복지 숲</td>
<td>천주교춘천교구사회복지회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&lsquo;숲속의 아름다운 사랑의 동산&rsquo; 조성사업</td>
<td>음성향애원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>지적 발달장애인 심신 정서 안정을 위한 웰빙 테라피조성</td>
<td>보듬의집</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&ldquo;라이프 세이브(Life Save)&rdquo;옥상치료정원</td>
<td>보은복지재단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>2012 녹색프로젝트 &ldquo;햇살, 바람 그리고 쉼&rdquo;</td>
<td>청주내덕노인복지관</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>나눔의 시작, 아름다운 &lsquo;문화체험! 녹색복지 숲&rsquo;</td>
<td>천주교보혈선교수녀회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>증평종합사회복지관 녹색공원 조성사업</td>
<td>열림재단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&lsquo;7Happy 무지개동산&rsquo;</td>
<td>우암복지재단</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>장애인과 비장애인(지역주민)이 함께 숨쉬는 푸른 숲 조성사업</td>
<td>논산다애원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외시설 녹색공간 조성사업</td>
<td>유일원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>희망과 나눔의 녹색복지 &ldquo;숲&rdquo; 조성 사업</td>
<td>삼신보육원</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>바이오테라피(생태치유)녹색나눔숲</td>
<td>인삼골건강마을</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외시설 녹색공간 조성사업</td>
<td>석촌재단 성모복지원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>푸른 샤론 공간 가꾸기</td>
<td>샤론복지재단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색 숲속의 아름다운 정원에서 &ldquo;어르신들의 웃음과 행복을 만나보고 싶다&rdquo;</td>
<td>공주사랑요양원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>참사랑 녹지복지 숲 조성사업</td>
<td>참사랑복지재단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>우리마을 착한 녹색복지 숲 조성</td>
<td>믿음의집</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색사랑 이웃사랑 녹색복지 숲 조성</td>
<td>크리스찬복지재단</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>행복한 동행 &lsquo;함께 걷는 녹색복지 숲&rsquo; 조성</td>
<td>한기장복지재단</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>나눔과 소통의 녹색복지 숲 조성</td>
<td>참사랑낙원</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>에덴 사랑의 녹색복지숲 조성</td>
<td>한기장사랑요양원</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>김제가나안요양원 녹색복지 숲 조성</td>
<td>김제가나안복지재단</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사랑의 향기 녹색복지 숲 조성</td>
<td>시온의집</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>테마가 있는 숲 &lsquo;어울림 &rsquo; 행복을 나누다</td>
<td>순천SOS어린이마을</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>2012년도 녹색공간 조성사업</td>
<td>기쁨의집</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>신안군 노인전문요양원 녹색공간 조성사업</td>
<td>신안군노인전문요양원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>네발로 가는 세상(휠체어가 다닐 수 있는 치유의 숲길 조성)</td>
<td>송정회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&ldquo;Green-health 노인요양원 만들기!&rdquo;</td>
<td>평강재단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간조성사업</td>
<td>정우요양원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>시민과 함께 더불어 사는 녹색세상</td>
<td>백양실버타운</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색숲(테라피)공간조성</td>
<td>모니카</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>늘 푸른 향기의 숲</td>
<td>상주연세실버타운</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>아름다운 녹색복지공원 조성사업</td>
<td>주왕산복지회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>직지사노인요양원 거주어르신 및 지역주민을 위한 "Silver Garden" 조성사업</td>
<td>대한불교조계종유지재단</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>어르신들의 편안한 여가활동을 위한 녹색복지 숲 조성</td>
<td>예천노인전문요양원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>허브정원 조성사업</td>
<td>민재경주푸른마을</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>자연 속으로 떠나는 치유여행</td>
<td>경상복지재단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>운정골드에이지 노인전문요양원 녹색복지공간 조성사업</td>
<td>운정골드에이지</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>초록 쉼터(금성지역복지센터)</td>
<td>(사)천주교안동교구사회복지회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>요양원 거주 어르신들과 지역주민을 위한 녹색복지 산책로와 녹색쉼터 조성</td>
<td>진주복지재단</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>자연친화적 "임애치(林愛治)" 녹색사업</td>
<td>천사의집</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>동진의 꽃동산에는 녹색의 바람이 물결치고&hellip;</td>
<td>동진</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>늘푸른 녹색복지 숲 조성사업</td>
<td>지성복지재단</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>대산 靑마루 녹색복지숲</td>
<td>함안복지재단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간 조성사업 "꿈의 있는 산책로"</td>
<td>자광원</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>주거어르신 및 지역주민을 위한 &ldquo;평화로운 웰빙 공원&rdquo;조성 사업</td>
<td>제주평화양로원</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>꽃과 나비의 작은 숲</td>
<td>중증장애인요양시설 송죽원</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사회복지시설내 숲 조성</td>
<td>선도원(부설)벧엘</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색공간 조성사업 성과의 지속화를 위한 자원 네트워킹 프로그램 개발</td>
<td>한국사회복지사협회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>구룡산 옛숲복원(녹색나눔숲) 사업</td>
<td>대구광역시청</td>
<td>750,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>성안 녹색나눔숲 조성사업</td>
<td>울산시중구청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>돌산 덕개 생태하천 녹색나눔 숲 조성</td>
<td>여수시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>오송 녹색 나눔숲 조성</td>
<td>청원군청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>성내리 녹색 나눔 숲</td>
<td>영주시청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사상 어울림 녹색 나눔숲 조성사업</td>
<td>부산시사상구청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>청암 녹색나눔숲 조성사업</td>
<td>군산시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>용담호 녹색나눔숲 조성사업</td>
<td>진안군청</td>
<td>650,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹송공원</td>
<td>정선군청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>현충탑 공원 녹색나눔숲 조성</td>
<td>보령시청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>메타세쿼이아 녹색 나눔숲 조성사업</td>
<td>담양군청</td>
<td>550,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>빛고을 공예창작촌 녹색나눔의 숲 조성사업</td>
<td>광주시남구청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>장성동 걷고 싶은 숲길 조성사업</td>
<td>포항시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>솔밭어린이공원 녹색나눔숲 조성사업</td>
<td>대구광역시청</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사랑의 녹색나눔숲 조성사업</td>
<td>전주시청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>운암 제3근린공원 녹색나눔숲 조성사업</td>
<td>광주광역시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>영산강 나눔 숲 조성사업</td>
<td>나주시청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>영강 녹색나눔숲 조성</td>
<td>문경시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>봉서골 녹색나눔숲 조성</td>
<td>완주군청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>순천만 보전을 위한 생태 하천숲 복원</td>
<td>순천시청</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>단계천 생태하천 조성</td>
<td>원주시청</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>낙동강변 사벌국왕길&nbsp;</td>
<td>상주시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>창원"Sea &amp; Wood 향기나라" 조성</td>
<td>창원시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>판암동 녹색나눔숲 조성</td>
<td>대전광역시동구청</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>산림비전센터 내 다목적공간 조성사업</td>
<td>녹색사업단</td>
<td>617,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>41개사업</td>
<td>4,405,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>중증장애인 녹색체험교육사업 &lsquo;신바람 산바람&rsquo;</td>
<td>성분도복지관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색사관학교 운영</td>
<td>(재)경상북도환경연수원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>청소년과 함께하는 숲 체험 교육</td>
<td>(사) 친환경실천국민운동본부</td>
<td>49,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲 체험교육사업</td>
<td>충남대학교 산학협력단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲체험교육사업</td>
<td>충북대학교산학협력단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>자연과 어우러지는 숲 체험 프로그램</td>
<td>군산청소년수련관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>미래숲 제11기 녹색봉사단</td>
<td>(사)미래숲</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>저탄소 녹색성장에 부응한 산림환경 교육지원 사업</td>
<td>강원대학교 산림과학연구소</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲속학교 프로젝트&ldquo;H․C(Holistic Creative)자연학교&rdquo;</td>
<td>(재)송석문화재단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲 체험 전문가 양성 및 청소년 숲 체험</td>
<td>국립중앙과학관</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>사회인 녹색체험 교육사업</td>
<td>경북대학교</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>국민대학교 산림문화예술 체험 교육</td>
<td>국민대학교산학협력단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&lsquo;생태맹(生態盲)&rsquo; 극복을 위한 숲생태 체험교육 프로그램</td>
<td>(사)우이령보존회</td>
<td>31,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>나눔, 상생, 공존의 숲</td>
<td>영남대학</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색명상을 통한 마음감기(우울) 치유프로그램「울 ․ 화 ․ 통 캠프」</td>
<td>능인수련원</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲속에서 자라나는 아이들 &lt;숲속 지킴이&gt;</td>
<td>(사)충북생명의숲국민운동</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲체험교육사업</td>
<td>경상대학교 산학협력단</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲 체험 교육사업</td>
<td>전북대학교</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>숲으로 떠나는 여행/숲 체험 여행(여름, 겨울 캠프)</td>
<td>아이사랑</td>
<td>17,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>생태길탐방 / 발도장 캠프</td>
<td>(사)푸른전주 운동본부</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>한국형 숲유치원 활성화를 위한 원장ㆍ유아교사ㆍ숲해설가 연수과정</td>
<td>(사)숲유치원협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>산림분야 사회적기업(가)을 위한 아카데미 지원사업</td>
<td>풀빛문화연대</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>장애인과 비장애인이 함께하는 &ldquo;숲에서 놀자!&rdquo;</td>
<td>향림재활원</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>얘들아, 아마존에서 놀자</td>
<td>고강복지회관</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색체험교육사업 &ldquo;Fore School(포레 스쿨)&rdquo;</td>
<td>만덕종합사회복지관</td>
<td>33,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외계층 체험교육</td>
<td>한국녹색문화재단</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>수목원전문가 양성 및 숲 체험활동 프로그램</td>
<td>천리포수목원</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>소외계층 및 사회공헌자 대상 산림복지서비스 제공</td>
<td>녹색사업단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>유소년 숲체험교육</td>
<td>녹색사업단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>국내외 산림분야 지도자 및 전문가 육성 교육</td>
<td>녹색사업단</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>제12회 산림문화작품공모전</td>
<td>산림조합중앙회</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>국산목재로 만든 학생용 책상의자 보급</td>
<td>산림조합중앙회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>올바른 수목장 문화 보급&middot;확산을 위한 캠페인 운동</td>
<td>수목장실천회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>녹색문학상</td>
<td>(사)한국산림문학회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>스마트 시대의 산림웰니스 (Forest-based wellness)기능 캠페인 사업</td>
<td>한국산림정책연구회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>환경영화를 활용한 &ldquo;숲과 기후변화 포털 사이트&rdquo; 제작 및 콘텐츠 개발, 보급</td>
<td>환경재단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>나무이름표 달아주기</td>
<td>한국임업후계자협회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>기후변화에 따른 우리산림 가꾸기 (Go Green Plus)</td>
<td>환경과사람들</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>산지보전 교육.캠페인</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&lsquo;국민의 삶의 질 향상&rsquo; 대국민 캠페인</td>
<td>녹색사업단</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>