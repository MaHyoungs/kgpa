<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2013년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2013년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2013년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>[2013년도 사업]</strong></td>
<td><strong>199개사업</strong></td>
<td><strong>33,017,000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>131개사업</td>
<td>24,100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>보현의집 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>서울인강학교 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>혜명보육원 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>은평의마을 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>금천노인종합복지관녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>삼화경로당데이케어센터 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>43,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>강서뇌성마비복지관 녹색희망 프로젝트</td>
<td>서울특별시</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>죽성 조은수피아</td>
<td>부산광역시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>녹색공간 조성사업(지상녹화)</td>
<td>부산광역시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>이웃과 함께 나눔을 실천하는 "사랑나눔숲" 녹색공간조성사업</td>
<td>부산광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>사계절이숨을쉬는아름다운녹색공간</td>
<td>부산광역시시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>장애인들을 위한 녹색삶터 설치공사</td>
<td>부산광역시</td>
<td>51,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>온누리 녹색복지 숲</td>
<td>부산광역시시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>옥상녹화사업</td>
<td>부산광역시</td>
<td>75,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>은빛 녹색 복지숲 만들기</td>
<td>부산광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>대구 드림텍(희망전망대)하늘공원 조성</td>
<td>대구광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>학산종합사회복지관(웰리안) 하늘공원 조성</td>
<td>대구광역시</td>
<td>93,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>애생 녹색복지숲 조성</td>
<td>대구광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>마이홈 하늘공원 조성</td>
<td>대구광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>육영 녹색복지숲 조성</td>
<td>대구광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>대구 실버타운 녹색 복지숲조성</td>
<td>대구광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>대구 보훈요양원 녹색 복지숲 조성</td>
<td>대구광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>중증장애영유아동등을 위한 "에코힐링"녹색복지 공간제공을 위한 지상녹화사업</td>
<td>인천광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>아이들이 green 숲</td>
<td>인천광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-25</td>
<td>인생 제 2막을 위한 안식의 숲</td>
<td>인천광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-26</td>
<td>인천사할린동포복지회관 둘레길 조성사업</td>
<td>인천광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-27</td>
<td>소외시설 녹색공간 조성사업(옥상녹화)</td>
<td>인천광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-28</td>
<td>농어촌 지역주민을 위한 다목적 체육공원 및 문화시설 설치&middot;무료개방</td>
<td>인천광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-29</td>
<td>녹색공간조성(지상녹화 조성)</td>
<td>인천광역시</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-30</td>
<td>강화정신요양원 녹색공간조성사업</td>
<td>인천광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-31</td>
<td>휴식과 나눔이 있는 아름다운 녹색공간 만들기</td>
<td>인천광역시</td>
<td>22,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-32</td>
<td>일곡한솔전문요양원 녹색복지숲 조성</td>
<td>광주광역시</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-33</td>
<td>자연을 되돌려 드리는 아름답고 편안한 녹색복지 숲 조성</td>
<td>광주광역시</td>
<td>137,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-34</td>
<td>자연속의 행복한 녹색복지 숲 조성</td>
<td>광주광역시</td>
<td>158,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-35</td>
<td>"꿈이 있는 첨단의 하늘 공원"</td>
<td>광주광역시</td>
<td>178,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-36</td>
<td>사랑품은 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-37</td>
<td>"정신치유를 위한 힐링공원" 조성사업</td>
<td>대전광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-38</td>
<td>다비다의집 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>125,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-39</td>
<td>해피존 녹색복지숲 조성</td>
<td>대전광역시</td>
<td>193,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-40</td>
<td>홍익복지재단 원동어린이집 녹색공간 조성사업</td>
<td>울산광역시</td>
<td>83,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-41</td>
<td>접경지역 중증장애인의 생명친화공간 조성사업</td>
<td>경기도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-42</td>
<td>자연과 함께 하는 산책공간</td>
<td>경기도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-43</td>
<td>아름다운 향기가 머무는 곳 "기적의 에덴정원"</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-44</td>
<td>노인복지시설 녹화와 휴양쉼터 마련</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-45</td>
<td>중증장애인의정서적・신체적 재활을 위한 녹지공간조성</td>
<td>경기도</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-46</td>
<td>녹색공간 조성사업</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-47</td>
<td>休 힐링테라피 녹색복지 숲 조성사업</td>
<td>경기도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-48</td>
<td>온누리 나눔 숲 조성사업</td>
<td>강원도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-49</td>
<td>소외계층 녹색복지증진 사업</td>
<td>강원도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-50</td>
<td>녹색공간 조성사업</td>
<td>강원도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-51</td>
<td>하늘정원조성사업</td>
<td>강원도</td>
<td>101,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-52</td>
<td>자연과 함께하는 "희망의 숲"조성사업</td>
<td>강원도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-53</td>
<td>지역시설과 지역사회의 통합 통행로 녹색 나눔숲길조성</td>
<td>강원도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-54</td>
<td>푸르른 소통의 장"맞춤형 푸른 녹지 공간"</td>
<td>충청북도</td>
<td>109,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-55</td>
<td>노년의 푸른 꿈 녹색복지 숲</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-56</td>
<td>나눔의집 옥상 녹화 사업</td>
<td>충청북도</td>
<td>112,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-57</td>
<td>편안한 어울림 곰두리 녹색복지 공간 조성사업</td>
<td>충청북도</td>
<td>105,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-58</td>
<td>단양 다사랑 노인요양원 녹색복지 숲 조성사업</td>
<td>충청북도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-59</td>
<td>울림,끌림,어울림 옥상정원</td>
<td>충청북도</td>
<td>68,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-60</td>
<td>녹색공간 조성사업-옥상녹화사업</td>
<td>충청북도</td>
<td>85,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-61</td>
<td>녹색공간 조성사업</td>
<td>충청북도</td>
<td>94,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-62</td>
<td>아이들의 꿈과 희망이 싹트는 녹색공간 조성사업</td>
<td>충청남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-63</td>
<td>꽃내음 가득한 황계 푸른숲 조성사업</td>
<td>충청남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-64</td>
<td>"더불어 숲이되다" 녹색공간 조성사업</td>
<td>충청남도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-65</td>
<td>당진시립노인요양원 녹색복지 숲 조성사업</td>
<td>충청남도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-66</td>
<td>소외시설 녹색공간 조성사업</td>
<td>충청남도</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-67</td>
<td>"향기로집 아이들의 쉼터 녹색복지 숲"</td>
<td>충청남도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-68</td>
<td>녹색복지숲 조성을 통한 치유정원"휴(休)를 위한 치유정원"</td>
<td>충청남도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-69</td>
<td>균형있는 녹색성장(부제:아동의 정서적 성장을 위한 녹색사업프로그램)</td>
<td>충청남도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-70</td>
<td>"생생꿈마을" 녹색녹지 숲</td>
<td>충청남도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-71</td>
<td>성암복지원 녹색복지 숲</td>
<td>전라북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-72</td>
<td>로뎀나무 생명의 녹색복지 숲</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-73</td>
<td>행복바이러스 "투게더!!워킹!! 녹색복지숲"</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-74</td>
<td>섬김과 나눔이 있는 "어울마당" 녹색복지숲</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-75</td>
<td>"효와 은혜"가 살아 숨쉬는 녹색복지 숲</td>
<td>전라북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-76</td>
<td>"치유와 나눔"의 예은 녹색복지 숲</td>
<td>전라북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-77</td>
<td>희망 녹색복지 숲</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-78</td>
<td>화합과 치유의 녹색복지 숲</td>
<td>전라북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-79</td>
<td>장애인들과 지역주민이 함께하는 녹색복지 숲</td>
<td>전라북도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-80</td>
<td>우리 함께해요 녹색복지 숲</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-81</td>
<td>지역주민과 함께하는 녹색복지 숲 조성</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-82</td>
<td>자연치유 녹색복지 숲 조성</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-83</td>
<td>자비원 녹색복지 숲 조성</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-84</td>
<td>녹색공간조성사업</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-85</td>
<td>정원이 있는 숲길(배회로) 조성</td>
<td>전라남도</td>
<td>69,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-86</td>
<td>녹색공간 조성사업(백련노인전문요양원)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-87</td>
<td>녹색공간 조성사업(목포이랜드노인복지회)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-88</td>
<td>친환경 녹색 공간 조성(영광푸른마을)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-89</td>
<td>친환경 녹색 숲 조성(삼동회심청골효도의집)</td>
<td>전라남도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-90</td>
<td>대창 녹색복지숲 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-91</td>
<td>초록세상 녹색숲 만들기</td>
<td>경상북도</td>
<td>88,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-92</td>
<td>녹색복지숲 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-93</td>
<td>새희망 힐링스 태훈복지숲 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-94</td>
<td>실로암 녹색복지숲 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-95</td>
<td>가람 녹색복지숲 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-96</td>
<td>치유와 사색이 함께하는 초록마을 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-97</td>
<td>녹색공간 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-98</td>
<td>봄마을 녹색공간 조성</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-99</td>
<td>연꽃마을 3세대 녹색복지 숲</td>
<td>경상남도</td>
<td>95,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-100</td>
<td>하동요양원 천년사랑 녹색복지 숲</td>
<td>경상남도</td>
<td>145,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-101</td>
<td>산청복음 전문요양원 주변 녹색 복지 숲</td>
<td>경상남도</td>
<td>97,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-102</td>
<td>동천 산책로 녹색복지 숲</td>
<td>경상남도</td>
<td>101,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-103</td>
<td>꽃나라 녹색복지 숲</td>
<td>경상남도</td>
<td>93,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-104</td>
<td>편백숲 조성을 통한 치유의 녹색복지 숲</td>
<td>경상남도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-105</td>
<td>녹색복지 숲 조성</td>
<td>제주특별자치도</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-106</td>
<td>올레길과 함께하는 숲속의 쉼팡 조성</td>
<td>제주특별자치도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-107</td>
<td>어르신 복지환경 조성을 위한 녹화사업</td>
<td>제주특별자치도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-108</td>
<td>푸른세상 녹색정원 조성</td>
<td>제주특별자치도</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-109</td>
<td>정혜그린치료정원</td>
<td>제주특별자치도</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-110</td>
<td>사회복지시설 녹색복지공간 모니터링 사업</td>
<td>녹색사업단</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-111</td>
<td>구, 여월 정수장 부지 녹색나눔숲 조성</td>
<td>부천시청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-112</td>
<td>단구공원 도시숲 조성사업</td>
<td>원주시청</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-113</td>
<td>양구 비봉 녹색나눔숲 조성사업</td>
<td>양구군청</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-114</td>
<td>청초호 녹색나눔숲 조성사업</td>
<td>속초시청</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-115</td>
<td>아라리공원녹색나눔숲조성사업</td>
<td>정선군청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-116</td>
<td>월평 녹색나눔숲</td>
<td>대전광역시서구청</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-117</td>
<td>내포신도시 행복한 녹색나눔 숲 조성</td>
<td>충청남도청</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-118</td>
<td>미동산 녹색나눔숲 조성</td>
<td>충청북도산림환경연구소</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-119</td>
<td>생태광장 녹색나눔숲 조성사업</td>
<td>광주광역시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-120</td>
<td>광양 소생태계 나눔 숲 조성사업</td>
<td>광양시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-121</td>
<td>고추장마을 녹색나눔숲 조성사업</td>
<td>순창군청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-122</td>
<td>고흥 어울林 녹색나눔숲 조성사업</td>
<td>고흥군청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-123</td>
<td>보성소리 나눔숲 조성사업</td>
<td>보성군청</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-124</td>
<td>익산 희망의 녹색 나눔숲 조성사업</td>
<td>익산시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-125</td>
<td>점터 어린이공원 아낌없이 주는 숲 (녹색나눔숲) 조성사업</td>
<td>대구광역시청</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-126</td>
<td>소백힐링 녹색나눔숲 조성사업</td>
<td>영주시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-127</td>
<td>뱃머리마을 녹색나눔숲 조성</td>
<td>포항시청</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-128</td>
<td>태화마을 녹색나눔숲 조성사업</td>
<td>울산광역시중구청</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-129</td>
<td>지리산 가족숲길 조성사업</td>
<td>함양군청</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-130</td>
<td>함께하는 녹색나눔숲 조성사업</td>
<td>하동군청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-131</td>
<td>토평근린공원 녹색나눔숲 조성사업</td>
<td>서귀포시청</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</td>
<td>68개사업</td>
<td>8,917,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>사회복지나눔숲</td>
<td>녹색사업단</td>
<td>1,004,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>산림교육관조성</td>
<td>천리포수목원</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>숲 체험교육사업</td>
<td>경상대학교산학협력단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>숲체험교육사업</td>
<td>경북대학교산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>우포늪과 함께하는 숲체험학교</td>
<td>푸른우포사람들</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>장애인 트레킹 문화 확산</td>
<td>한국트레킹연맹</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>&lsquo;생태맹(生態盲)&rsquo; 극복을 위한 숲 생태체험교육</td>
<td>우이령사람들</td>
<td>35,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>국립공원과 함께하는 숲 체험 프로그램</td>
<td>국립공원관리공단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>숲의 미래가치를 찾아 떠나는 행복한 동행길</td>
<td>해남문화원</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>장애인의 녹색세상탐방 "자연으로의 아름다운 동행"</td>
<td>제천장애인종합복지관</td>
<td>13,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>장애인과 비장애인이 함께하는 "숲에서 놀자!"</td>
<td>향림재활원</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>장애인과 함께 체험하고 가꾸는 숲</td>
<td>지구촌사회복지재단</td>
<td>27,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>숲 체험을 통한 건강과 생태계 바로 알기</td>
<td>(사)노거수회</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>숲속학교 프로젝트</td>
<td>(재)송석문화재단</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>숲으로 떠나는 소풍 녹색 체험 여행(여름, 겨울 캠프)</td>
<td>아이사랑</td>
<td>19,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>중증장애인 숲체험교육사업 "신바람 산바람"</td>
<td>성분도복지관</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>생태길탐방 / 발도장 캠프</td>
<td>(사)푸른전주운동본부</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>숲 체험교육사업</td>
<td>충북대학교산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>숲에서 자란 우리 임산물 체험 교실</td>
<td>산림조합중앙회임산물유통사업소</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>숲체험교육을 통한 정신장애인의 회복지원프로그램 "林과 함께"</td>
<td>청주정신건강센터</td>
<td>23,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>얘들아,아마존에서 놀자!</td>
<td>고강복지회관</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>녹색교육 - 녹색마음 (소외계층 가정의 학교부적응 아동을 위한 숲심리치유)</td>
<td>현당평생교육원</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>산림관련 학과 학생 현장체험교육사업</td>
<td>한국임학회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>100대 명산 클린 캠프</td>
<td>한국등산트레킹지원센터</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>녹색사관학교</td>
<td>경상북도환경연수원</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>꼬마 파브르 "어린이 숲해설가 교육"</td>
<td>(사)숲생태지도자협회</td>
<td>20,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>소통의 녹색명상 「울 &middot; 화 &middot; 통 &lsquo;울&middot;화&middot;통 캠프」</td>
<td>법주사능인문화원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>숲과 함께하는 "아름다운 동행"</td>
<td>도솔노인복지센터</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>숲과 함께 쑥쑥</td>
<td>(사)한국숲해설가협회</td>
<td>32,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>한국형 숲유치원 활성화를 위한 원장ㆍ유아교사ㆍ숲해설가 연수과정</td>
<td>(사)한국숲유치원협회</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>숲에서 자라나는 아이들 &lt;숲속 지킴이&gt;</td>
<td>(사)충북생명의숲국민운동</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>우리는 토요 숲 친구</td>
<td>(사)청소년문화공동체</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>숲 체험 전문가 양성 및 청소년 숲 체험</td>
<td>한국자연사연구협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>녹색체험 교육사업 &ldquo;Fore School(포레 스쿨)&rdquo;</td>
<td>만덕종합사회복지관</td>
<td>37,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>언남초 방과후학교 숲프로그램 운영 사업</td>
<td>(사)숲과아이들</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>경기도민과 함께하는 숲체험학교</td>
<td>(재)경기농림진흥재단</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>그린콘텐츠 ECO엔티어링</td>
<td>(재)범어청소년동네</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-39</td>
<td>2013년 솔향 녹색학교</td>
<td>강릉문화원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-40</td>
<td>숲 체험교육사업</td>
<td>충남대학교산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-41</td>
<td>지구살리기 그린캠프</td>
<td>(사)미래숲</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-42</td>
<td>청소년 숲체험, 등산 아카데미</td>
<td>(사)한국산악회</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-43</td>
<td>청소년과 함께하는 숲 체험 교육</td>
<td>친환경실천국민운동본부</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-44</td>
<td>녹색나눔 사회통합 프로그램&lsquo;e-green garden'</td>
<td>밀알재활원</td>
<td>24,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-45</td>
<td>소외계층 숲체험교육사업</td>
<td>한국산림복지문화재단</td>
<td>900,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-46</td>
<td>숲치유캠프</td>
<td>천리포수목원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-47</td>
<td>소외계층 체험교육사업</td>
<td>녹색사업단(소외계층)</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-48</td>
<td>백두대간 희망나눔사업</td>
<td>녹색사업단(백두대간)</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-49</td>
<td>인재육성사업</td>
<td>녹색사업단(인재육성)</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-50</td>
<td>교육 및 복지 증진사업</td>
<td>한국산림복지문화재단</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-51</td>
<td>제13회 산림문화작품공모전</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-52</td>
<td>국산목재로 만든 학생용 책상&middot;의자 보급</td>
<td>산림조합중앙회</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-53</td>
<td>기후변화에따른우리산림사랑캠페인</td>
<td>(사)환경과사람들</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-54</td>
<td>제2회 녹색문학상</td>
<td>(사)한국산림문학회</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-55</td>
<td>생애 주기형(LC형) 산림복지서비스의 대국민 캠페인</td>
<td>(사)한국산림정책연구회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-56</td>
<td>올바른 수목장 문화 보급&middot; 확산을 위한 캠페인 운동</td>
<td>수목장실천회</td>
<td>30,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-57</td>
<td>초중등학교 국내 목재 재감 보급 사업</td>
<td>(사)한국목공교육협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-58</td>
<td>아낌 없이 주는 나무 5+1(오감[five senses, 五感]+힐링(치유)</td>
<td>전일엔컬스</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-59</td>
<td>현신규학술상</td>
<td>(사)한국임학회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-60</td>
<td>녹색문화 박람회</td>
<td>(사)한국DIY가구공방협회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-61</td>
<td>"숲 그리고 소리" 나눔 플러스(+)콘서트</td>
<td>(사)멘토오케스트라</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-62</td>
<td>숲 속의 고백(Go Back)</td>
<td>(사)솔잎쉼터</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-63</td>
<td>청소년 녹색 숲 사랑 공동체 캠페인사업</td>
<td>(사)경남환경연합</td>
<td>41,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-64</td>
<td>"숲"사랑축제- 봄소풍, 가을소풍 편</td>
<td>(사)무등산보호단체협의회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-65</td>
<td>계간지 &lt;나무와 숲(가칭)&gt; 발간</td>
<td>(사)한국산림과학기술단체연합회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-66</td>
<td>산사랑문화지발행, 산림생태복원대회, 산사랑사진공모전산사랑교구개발, 산림보전 전시물</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-67</td>
<td>우리숲 큰나무 발굴 및 보전</td>
<td>녹색사업단(큰나무)</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-68</td>
<td>사업단 기획캠페인</td>
<td>녹색사업단(캠페인)</td>
<td>2,322,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원&middot;휴양림&middot;수목장림의 조성&middot;운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>