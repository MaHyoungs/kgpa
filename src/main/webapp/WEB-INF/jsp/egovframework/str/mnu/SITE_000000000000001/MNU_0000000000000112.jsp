<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<p><span class="block alR"><a class="cbtn" title="새창열림" onclick="window.open(this.href,'new','scrollbars=yes, resizable=no, width=982, height=658');return false" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php"><span class="icon_note">년도별지원사업 자세히보기</span></a></span></p>
<h3 class="icon1">2014년도 사업</h3>
<p><span class="block alR">(단위 : 천원)</span></p>
<!-- 테이블  -->
<div>
<table class="chart" summary="녹색사업단 2014년도 녹색자금지원 정보를 나타낸표로 번호, 지원사업명, 기관(단체)명, 지원금액 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 2014년도 사업</caption><colgroup> <col width="10%" /> <col width="*" /> <col width="20%" /> <col span="2" width="10%" /> </colgroup>
<thead>
<tr><th scope="col">번호</th><th scope="col">지원사업명</th><th scope="col">기관(단체)명</th><th scope="col">지원금액</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td colspan="2"><strong>[2014년도 사업]</strong></td>
<td><strong>239개사업</strong></td>
<td><strong>37,516.000</strong></td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">1. 맑은물 공급을 위한 산림환경 개선사업</td>
<td>-</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">2. 공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</td>
<td>57개사업</td>
<td>8,478,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-1</td>
<td>서울시 영보자애원 녹색공간 조성(영보자애원)</td>
<td>서울특별시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-2</td>
<td>시울시립 비전트레이닝센터녹색공간조성(비전트레이닝센터)</td>
<td>서울특별시</td>
<td>185,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-3</td>
<td>서울시 구립신당3동어린이집 녹색공간 조성(신당3동어린이집)</td>
<td>서울특별시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-4</td>
<td>서울시립 영보정신요양원 녹색공간 조성(영보정신요양원)</td>
<td>서울특별시</td>
<td>131,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-5</td>
<td>서울시립 고덕양로원 녹색공간조성(고덕양로원)</td>
<td>서울특별시</td>
<td>95,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-6</td>
<td>서울시 구립서초남서울어린이집 녹지조성사업(서초남서울어린이집)</td>
<td>서울특별시</td>
<td>37,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-7</td>
<td>부산시 자매정신요양원 녹색공간조성(자매정신요양원)</td>
<td>부산광역시</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-8</td>
<td>부산시 해운대실버홈 녹색공간조성(해운대실버홈)</td>
<td>부산광역시</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-9</td>
<td>부산시 사직종합사회복지관 녹색공간조성(사직종합사회복지관)</td>
<td>부산광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-10</td>
<td>부산시 한마음노인건강센터 녹색공간조성사업(한마음노인건강센터)</td>
<td>부산광역시</td>
<td>98,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-11</td>
<td>부산시 사하사랑채노인복지관 녹색공간조성(사하사랑채노인복지관)</td>
<td>부산광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-12</td>
<td>부산시 동구종합사회복지관 녹색공간조성(동구종합사회복지관)</td>
<td>부산광역시</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-13</td>
<td>부산시 동래종합사회복지관 녹색공간조성(동래종합사회복지관)</td>
<td>부산광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-14</td>
<td>대구가톨릭근로자회관 녹색공간조성(가톨릭근로자회관)</td>
<td>대구광역시</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-15</td>
<td>대구 노인종합복지관 녹색공간조성(대구노인종합복지관)</td>
<td>대구광역시</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-16</td>
<td>대구 큰하늘어린이집 녹색공간조성(큰하늘어린이집)</td>
<td>대구광역시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-17</td>
<td>천사의집노인요양원 녹색공간조성사업(천사의집)</td>
<td>인천광역시</td>
<td>32,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-18</td>
<td>시민과 느티나무 장애인주간보호시설장애인의 쉼과 힐링(healing)을 위한녹색공간 (느티나무장애인주간보호시설)</td>
<td>인천광역시</td>
<td>33,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-19</td>
<td>녹색공간 조성사업(인천동구노인복지관)</td>
<td>인천광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-20</td>
<td>염전골 정원 만들기(미추홀종합사회복지관)</td>
<td>인천광역시</td>
<td>65,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-21</td>
<td>인천시 해바라기 "해바라기 정원" 조성사업(사회복지법인더모닝해바라기)</td>
<td>인천광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-22</td>
<td>해피맘힐링공원(사회복지법인선교복지재단)</td>
<td>광주광역시</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-23</td>
<td>광주에버그린녹색공간조성(에버그린실버하우스)</td>
<td>광주광역시</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-24</td>
<td>광주 가교행복빌라 녹색공간조성(가교행복빌라)</td>
<td>광주광역시</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-25</td>
<td>광주 영신원 녹색공간조성(광주영신원)</td>
<td>광주광역시</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-26</td>
<td>늘사랑 녹색복지숲 조성(늘사랑아동센터)</td>
<td>대전광역시</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-27</td>
<td>밝은마음 녹색복지숲 조성(대전광역시립 제1노인전문병원)</td>
<td>대전광역시</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-28</td>
<td>이울림 녹색복지숲 조성(판암사회복지관)</td>
<td>대전광역시</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-29</td>
<td>살아숨쉬는 힐링을 위한 녹색공간조성(문수실버복지관)</td>
<td>울산광역시</td>
<td>126,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-30</td>
<td>이천시 참사랑 전문요양원 녹색공간조성(참사랑전문요양원)</td>
<td>경기도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-31</td>
<td>의왕시 성라자로마을 녹색공간조성(성 라자로 마을)</td>
<td>경기도</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-32</td>
<td>양주시 서울정신요양원 녹색공간조성(서울정신요양원)</td>
<td>경기도</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-33</td>
<td>포천시 포천분도마을 녹색공간조성(포천분도마을)</td>
<td>경기도</td>
<td>139,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-34</td>
<td>연천군 동트는마을 녹색공간조성(동트는마을)</td>
<td>경기도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-35</td>
<td>파주시 성체지역아동보호센터 녹색공간조성(성체지역아동센터)</td>
<td>경기도</td>
<td>119,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-36</td>
<td>오산시 사회복지법인 성심동원 녹색공간조성(성심동원)</td>
<td>경기도</td>
<td>194,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-37</td>
<td>강원도 영월군노인요양원 녹색공간조성(영월군노인요양원)</td>
<td>강원도</td>
<td>160,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-38</td>
<td>강원도 철원몬띠노인요양원 녹색공간조성(몬띠노인요양원)</td>
<td>강원도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-39</td>
<td>강원도 홍천솔치요양마을 녹색공간조성(솔치요양마을)</td>
<td>강원도</td>
<td>115,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-40</td>
<td>강원도 춘천연화마을 녹새공간조성(연화마을)</td>
<td>강원도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-41</td>
<td>강원도 태백노인전문요양원 녹색공간조성(태백노인전문요양원)</td>
<td>강원도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-42</td>
<td>강원도 원주복지원 녹색공간조성(원주복지원)</td>
<td>강원도</td>
<td>115,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-43</td>
<td>강원도 화천평화의집 녹색공간조성(화천평화의집)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-44</td>
<td>강원도 홍천삼덕원 녹색공간조성(삼덕원)</td>
<td>강원도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-45</td>
<td>강원도 동해삼화사노인요양원 녹색공간조성(삼화사노인요양원)</td>
<td>강원도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-46</td>
<td>청원군 담쟁이 주간보호센터 녹색공간조성(담쟁이주간보호센터)</td>
<td>충청북도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-47</td>
<td>청주시 충청북도노인종합복지관 녹색공간조성 (충청북도노인종합복지관)</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-48</td>
<td>옥천군 부활원 녹색공간조성(부활원)</td>
<td>충청북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-49</td>
<td>충주시 홍복마을 녹색공간 조성(홍복마을)</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-50</td>
<td>증평군 보건복지타운 녹색공간 조성(보건복지타운)</td>
<td>충청북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-51</td>
<td>청주시충북재활원 녹색공간 조성(충북재활원)</td>
<td>충청북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-52</td>
<td>단양군 충주댐효나눔센터 녹색공간 조성(충주댐효나눔복지센터)</td>
<td>충청북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-53</td>
<td>옥천군 영실애육원 녹색공간 조성(영실애육원)</td>
<td>충청북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-54</td>
<td>단양군 단양노인보금자리 녹색공간조성(단양노인보금자리)</td>
<td>충청북도</td>
<td>65,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-55</td>
<td>충청남도 공주효센터 녹색공간조성(지장원)</td>
<td>충청남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-56</td>
<td>시설거주인과지역주민이함께하는'힐링쉼터'(명주원)</td>
<td>충청남도</td>
<td>102,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-57</td>
<td>보령시 보령학사 소외시설 녹색공간조성사업(보령학사)</td>
<td>충청남도</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-58</td>
<td>충남 테레사의집 녹색공간조성(수궁원 테레사의 집)</td>
<td>충청남도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-59</td>
<td>함께하는 녹색공간조성(사회복지법인 함께하는 복지재단)</td>
<td>충청남도</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-60</td>
<td>논산YWCA여성의쉼터녹색공간조성사업(논산 YWCA여성의 쉼터)</td>
<td>충청남도</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-61</td>
<td>"장애인과비장애인의 녹색정서키우는푸른숲조성사업"(다사랑)</td>
<td>충청남도</td>
<td>95,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-62</td>
<td>충청남도 새감마을 녹색공간 조성(새감마을)</td>
<td>충청남도</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-63</td>
<td>녹색복지 숲 조성을 통한 행복정원"새소망을 담은 행복한 우리들의 정원"(건생원)</td>
<td>충청남도</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-64</td>
<td>군산시 나포길벗공동체 녹색공간조성(나포 길벗 공동체)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-65</td>
<td>전북 익산 이리자선원 녹색공간조성(이리 자선원)</td>
<td>전라북도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-66</td>
<td>"꿈의 정원"녹색공간조성(샘골 보은의 집)</td>
<td>전라북도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-67</td>
<td>전북도 하은의 집 소외시설 녹색공간조성(하은의 집)</td>
<td>전라북도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-68</td>
<td>전라북도 함께 걷는 녹색공간 조성(흰마실)</td>
<td>전라북도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-69</td>
<td>정신장애인 치유를 위한 녹색복지숲 조성사업(미리암)</td>
<td>전라북도</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-70</td>
<td>애린원 녹색공간 조성사업(애린원)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-71</td>
<td>전라북도 늘푸른집 녹색공간조성(늘푸른집)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-72</td>
<td>사회복지법인 한울안 고창원광참살이(한울안)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-73</td>
<td>전북 익산 동그라미 녹색공간 조성(동그라미)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-74</td>
<td>힐링 숲(Healing Forest) 녹색공간 조성 사업(만복원)</td>
<td>전라북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-75</td>
<td>함평 대덕 힐링숲 조성사업(함평군장애인보호작업장)</td>
<td>전라남도</td>
<td>180,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-76</td>
<td>전남 여수 금강원 녹색공간조성(금강원)</td>
<td>전라남도</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-77</td>
<td>보성군노인복지관 녹색공간조성(보성군노인복지관)</td>
<td>전라남도</td>
<td>136,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-78</td>
<td>광산특수어린이집 녹색공간조성(광산특수어린이집)</td>
<td>전라남도</td>
<td>46,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-79</td>
<td>노숙인의 편안한 녹색공간조성사업(해남희망원)</td>
<td>전라남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-80</td>
<td>포항시 무량복지재단 녹색공간 조성사업(무량복지재단)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-81</td>
<td>김천시 누리복지재단 녹색공간 조성(누리복지재단)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-82</td>
<td>경상북도 보현마을 녹색공간조성(보현마을)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-83</td>
<td>예천군 인덕의료재단 녹색공간조성(경도요양원)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-84</td>
<td>영양군립요양원 녹색공간조성(군립요양원)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-85</td>
<td>문경시 신망애육원 녹색공간 조성(신망애육원)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-86</td>
<td>상주보림원 녹색공간조성(보림원)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-87</td>
<td>청송군 소망의집 녹색공간조성(소망의집)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-88</td>
<td>안동시 선산재활원 녹색공간조성(선산재활원)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-89</td>
<td>경상북도 이레마을 녹색공간조성(이레마을)</td>
<td>경상북도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-90</td>
<td>경상남도 부곡온천요양원 녹색공간조성(부곡온천요양원)</td>
<td>경상남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-91</td>
<td>하동 한사랑요양원 녹색공간조성(한사랑요양원)</td>
<td>경상남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-92</td>
<td>사천건양 요양원 녹색공간조성(사천건양요양원)</td>
<td>경상남도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-93</td>
<td>창원시 행복주식회사 장애인근로사업장 녹색공간조성 (행복주식회사장애인근로사업장)</td>
<td>경상남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-94</td>
<td>이레 소망의 녹색복지숲 조성(이레소망의집)</td>
<td>경상남도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-95</td>
<td>경상남도 참조은노인요양원 녹색공간조성(참조은노인요양원)</td>
<td>경상남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-96</td>
<td>경상남도 산청우리요양원 녹색공간조성(산청우리요양원)</td>
<td>경상남도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-97</td>
<td>합천군 합천노인전문요양원 녹색공간 조성(합천노인전문요양원)</td>
<td>경상남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-98</td>
<td>양산 인성원 새 삶을 위한 자연과 어우러진 녹색공간조성(인성원)</td>
<td>경상남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-99</td>
<td>통영 가경노인요양원 녹색공간조성(가경노인요양원)</td>
<td>경상남도</td>
<td>63,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-100</td>
<td>김해시 보현행원노인요양원 녹색공간조성(보현행원노인요양원)</td>
<td>경상남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-101</td>
<td>진주시 천진복지타운녹색공간조성사업(천진복지타운)</td>
<td>경상남도</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-102</td>
<td>입소어르신과 지역주민을 위한 지상녹화사업(평안전문요양원)</td>
<td>제주특별자치도</td>
<td>130,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-103</td>
<td>제주사라의집 "약속의 정원" 녹색공간조성(제주사라의집)</td>
<td>제주특별자치도</td>
<td>170,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-104</td>
<td>제주 주사랑요양원 녹색공간조성사업(주사랑요양원)</td>
<td>제주특별자치도</td>
<td>157,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-105</td>
<td>내 친구가 사는 집 자미성 녹색공간 조성사업(내친구가사는집자미성)</td>
<td>제주특별자치도</td>
<td>92,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-106</td>
<td>태고 녹색복지쉼터 조성사업(제주특별자치도노인복지관)</td>
<td>제주특별자치도</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-107</td>
<td>모니터링사업</td>
<td>녹색사업단</td>
<td>550,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-108</td>
<td>춘천시 강원명진학교 녹색공간조성(춘천명진학교)</td>
<td>강원도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-109</td>
<td>속초 청해학교 녹색공간조성(속초청해학교)</td>
<td>강원도</td>
<td>140,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-110</td>
<td>원주청원학교 녹색공간 조성(원주청원학교)</td>
<td>강원도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-111</td>
<td>경기도 동현학교 모퉁이숲 조성사업(동현학교)</td>
<td>경기도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-112</td>
<td>서울특별시 서울맹학교 나눔숲(서울맹학교)</td>
<td>서울특별시</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-113</td>
<td>서울특별시 한국육영학교 나눔숲(한국육영학교)</td>
<td>서울특별시</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-114</td>
<td>충주시 충주성모학교 나눔숲(충주성모학교)</td>
<td>충청북도</td>
<td>120,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-115</td>
<td>전라북도다솜학교"사랑더줌숲"(정읍다솜학교)</td>
<td>전라북도</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-116</td>
<td>청주시 청주성신학교 나눔숲(청주성신학교)</td>
<td>충청북도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-117</td>
<td>서산성봉학교나눔숲(서산성봉학교)</td>
<td>충청남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-118</td>
<td>전라북도 전북혜화학교나눔숲(전북혜화학교)</td>
<td>전라북도</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-119</td>
<td>경상북도 안동진명학교 나눔숲(안동진명학교)</td>
<td>경상북도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-120</td>
<td>부산광역시 부산한솔학교 나눔숲(부산한솔학교)</td>
<td>부산광역시</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-121</td>
<td>부산광역시 해마루학교 나눔숲(부산해마루학교)</td>
<td>부산광역시</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-122</td>
<td>영암 소림학교 나눔숲(영암 소림학교)</td>
<td>전라남도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-123</td>
<td>나주이화학교 나눔숲(나주 이화학교)</td>
<td>전라남도</td>
<td>110,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-124</td>
<td>제주특별자치도 서귀포온성학교 모퉁이숲(서귀포온성학교)</td>
<td>제주특별자치도</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-125</td>
<td>경상남도 진주혜광학교 나눔숲(진주혜광학교)</td>
<td>경상남도</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-126</td>
<td>광주선명학교 녹색공간 조성사업(광주선명학교)</td>
<td>광주광역시</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-127</td>
<td>영주시 녹색나눔숲</td>
<td>경북 영주시</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-128</td>
<td>충북증평군 녹색나눔숲</td>
<td>충북 증평군</td>
<td>800,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-129</td>
<td>충청도청 행복나눔숲(내포신도시 내)</td>
<td>충청남도</td>
<td>1,000,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-130</td>
<td>홍천 희망로 녹색나눔숲</td>
<td>강원 홍천군</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-131</td>
<td>익산 희망의 녹색나눔숲</td>
<td>전북 익산시</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-132</td>
<td>성주 독용산 감성누리 녹색나눔숲</td>
<td>경북 성주군</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-133</td>
<td>"3代가 함께 하는" 녹색나눔 숲</td>
<td>광주 서구</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-134</td>
<td>장수노하 녹색나눔숲</td>
<td>전북 장수군</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-135</td>
<td>소만 어린이 공원 녹색나눔숲</td>
<td>경남 거창군</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-136</td>
<td>춘천시 약사천 녹색나눔숲 조성</td>
<td>강원도 춘천시</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-137</td>
<td>행복을 주는 녹색나눔숲</td>
<td>전북 순창군</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-138</td>
<td>장성 편백 녹색나눔숲</td>
<td>전남 장성군</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-139</td>
<td>강릉 백년이음 녹색나눔숲</td>
<td>강원 강릉시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-140</td>
<td>사천 항공우주테마 녹색나눔숲</td>
<td>경남 사천</td>
<td>700,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-141</td>
<td>표선 당케 마을 녹색나눔숲</td>
<td>제주 서귀포</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-142</td>
<td>철원 백마고지 녹색나눔숲</td>
<td>강원 철원군</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-143</td>
<td>상당산성 녹색나눔숲</td>
<td>충북 청주</td>
<td>600,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-144</td>
<td>전주 힐링녹색나눔숲 조성</td>
<td>전북 전주시</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-145</td>
<td>앞산순환도로변 녹색나눔숲(남구)</td>
<td>대구시</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-146</td>
<td>매포 녹색나눔숲</td>
<td>충북 단양군</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-147</td>
<td>안산녹색나눔숲</td>
<td>경기도 안산시</td>
<td>500,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-148</td>
<td>홍성 녹색나눔숲</td>
<td>충남 홍성군</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-149</td>
<td>창원시소통과화합녹색나눔숲</td>
<td>경남 창원시</td>
<td>423,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-150</td>
<td>영양군 행복누리 녹색나눔숲</td>
<td>경북 영양군</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-151</td>
<td>광주광역시남구고싸움전수관</td>
<td>광주 남구</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-152</td>
<td>산림복지나눔숲 내 체험시설조성</td>
<td>녹색사업단</td>
<td>450,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2-153</td>
<td>산림교육홍보센터</td>
<td>천리포수목원</td>
<td>400,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">3. 청소년등을 위한 산림체험활동시설의 설치․운영 및 교육․홍보사업</td>
<td>86개 사업</td>
<td>9,883,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-1</td>
<td>숲체험교육을 통한 정신장애인의 회복지원 프로그램</td>
<td>청주정신건강센터</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-2</td>
<td>장애인 트레킹 문화 확산</td>
<td>(사)한국트레킹연맹</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-3</td>
<td>중증장애인 숲체험교육사업 '신바람 산바람'</td>
<td>성분도복지관</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-4</td>
<td>장애아동숲체험교육및숲치유ㆍ재활치료사업</td>
<td>베타니아</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-5</td>
<td>산림학교 「나무ㆍ숲ㆍ사람」</td>
<td>경북대학교</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-6</td>
<td>2014지역아동센터와 함께하는 녹색교실</td>
<td>(재)경기농림진흥재단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-7</td>
<td>남부학술림 숲체험교육</td>
<td>서울대학교 남부학술림</td>
<td>65,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-8</td>
<td>숲이랑 친구랑 꿈이랑</td>
<td>(사)푸른전주운동본부</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-9</td>
<td>산림관련학과 학생 산림실무 교육사업</td>
<td>(사)한국임학회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-10</td>
<td>숲에서 배우다</td>
<td>상지대학교 산림과학과</td>
<td>47,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-11</td>
<td>녹색자금 숲체험교육사업</td>
<td>충남대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-12</td>
<td>한택식물원 청소년 힐링숲체험교실</td>
<td>한택식물원</td>
<td>58,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-13</td>
<td>가까운 세대와 계층이 함께하는 백두대간 동행</td>
<td>건국대학교 산학협력단</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-14</td>
<td>숲체험교육사업</td>
<td>충북대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-15</td>
<td>소외계층을 위한 숲속 창의예술 캠프</td>
<td>(사)국민여가관광진흥회</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-16</td>
<td>녹색나눔 사회통합프로그램 'e-green garden'</td>
<td>밀알재활원</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-17</td>
<td>숲속학교프로젝트 FORESTORY</td>
<td>(재)송석문화재단</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-18</td>
<td>얘들아, 아마존에서 놀자!</td>
<td>고강복지회관</td>
<td>26,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-19</td>
<td>역사가 어우러진 왕릉 숲 체험교육</td>
<td>사)생태창의성교육 연구소'푸른상상'</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-20</td>
<td>녹색복지국가 구현을 위한 산림환경 교육지원 사업</td>
<td>강원대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-21</td>
<td>임업인, 귀산촌 전문교육과정</td>
<td>국민대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-22</td>
<td>녹색사관학교</td>
<td>(재)경북환경연수원</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-23</td>
<td>소외계층과 함께 하는 숲 체험 여행 "함께 숲으로"</td>
<td>도솔노인복지센터</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-24</td>
<td>발달장애아동가족 숲체험음악프로그램</td>
<td>부천시장애인종합복지관</td>
<td>10,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-25</td>
<td>숲사랑소년단 학교별 숲속교실</td>
<td>사단법인 숲사랑소년단</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-26</td>
<td>유아에서 성인까지 실천하는 숲사랑, 사람사랑</td>
<td>남강재단 홈에버그린</td>
<td>14,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-27</td>
<td>자연에서 배우는 극소외 계층 숲힐링</td>
<td>대전YWCA</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-28</td>
<td>나눔,상생,공존의 숲</td>
<td>영남대학교</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-29</td>
<td>休 in 숲 프로젝트</td>
<td>(사)제주올레</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-30</td>
<td>숲속 보물 상자를 찾아라</td>
<td>(사)한국청소년교육연구원</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-31</td>
<td>2014년 지구살리기 그린캠프</td>
<td>(사)미래숲</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-32</td>
<td>숲 체험 교육사업</td>
<td>서울대학교 수목원</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-33</td>
<td>자연이 우리의 스승이다</td>
<td>(사)노거수회</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-34</td>
<td>숲과의 만남</td>
<td>산림조합중앙회</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-35</td>
<td>지역사회주민들과 함께 하는 생활 속에서의 숲 찾기 "林"과 함께</td>
<td>원주가톨릭종합사회복지관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-36</td>
<td>자연과 어우러지는 숲체험프로그램</td>
<td>군산기독교청년회</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-37</td>
<td>자연을 닮아가는 사람들</td>
<td>제천장애인종합복지관</td>
<td>15,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-38</td>
<td>지역아동센터숲친구맺기 청소년숲체험꿈과희망을보둠다</td>
<td>(사)친환경실천국민운동본부</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-39</td>
<td>2014년도 산림문화교실</td>
<td>전북대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-40</td>
<td>2014년 솔향녹색학교</td>
<td>강릉문화원</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-41</td>
<td>숲과함께 사람과 함께</td>
<td>(사)한국숲해설가협회</td>
<td>59,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-42</td>
<td>우리숲 산행체험</td>
<td>(사)한국등산연합회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-43</td>
<td>숲체험학교</td>
<td>(사)왕산</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-44</td>
<td>미혼모(미혼임신부)를 위한 숲태교</td>
<td>(사)산림문화콘텐츠연구소</td>
<td>46,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-45</td>
<td>녹색기술전문인력 양성교육</td>
<td>산림조합중앙회 임업기술훈련원</td>
<td>74,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-46</td>
<td>동계숲체험산악스키 학교</td>
<td>(사)대한산악스키협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-47</td>
<td>한국형 숲유치원 활성활을 위한 원장&middot;유아교사&middot;학부모 연수</td>
<td>(사)한국숲유치원협회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-48</td>
<td>장애인과 더불어숲(Forest)체험교실</td>
<td>남원시장애인종합복지관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-49</td>
<td>찾아가는 초등학교 숲 ․ 목공 체험 교실</td>
<td>(사)한국목공교육협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-50</td>
<td>숲속오감체험학교</td>
<td>(사)한국산지환경조사연구회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-51</td>
<td>재가장애인 및 그 가족의 숲 이해도 증진 및 치유</td>
<td>상인종합사회복지관</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-52</td>
<td>숲체험! 힐링캠프로의 초대</td>
<td>경상대학교 산학협력단</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-53</td>
<td>호모에코루덴스(자연놀이를 통해 치유되는 삶)</td>
<td>서곡생태마을</td>
<td>45,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-54</td>
<td>숲과 함께하는 &lt;어린이 숲학교&gt; '오감체험'</td>
<td>성산종합사회복지관</td>
<td>25,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-55</td>
<td>1-3세대 통합 숲체험교육 포레스쿨</td>
<td>만덕종합사회복지관</td>
<td>36,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-56</td>
<td>장애및교육소외계층아동을 숲과 함께 하는 아트테라피</td>
<td>(사)숲과 아이들</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-57</td>
<td>숲으로의 아름다운 동행</td>
<td>녹색사업단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-58</td>
<td>산림분야 지도자 및 전문가 육성</td>
<td>녹색사업단</td>
<td>680,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-59</td>
<td>산림복지나눔숲 체험</td>
<td>녹색사업단</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-60</td>
<td>숲체험교육</td>
<td>한국산림복지문화재단</td>
<td>1,200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-61</td>
<td>교육 및 복지증진</td>
<td>한국산림복지문화재단</td>
<td>170,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-62</td>
<td>숲치유쿄육캠프&middot;숲체험활동</td>
<td>천리포수목원</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-63</td>
<td>청소년숲체험&middot;등산아카데미</td>
<td>한국산악회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-64</td>
<td>국산목재로 만든 학생용 책상,의자 및 좌탁 보급</td>
<td>산림조합중앙회</td>
<td>200,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-65</td>
<td>초중등학교 국내 목재 재감 보급 사업</td>
<td>(사)한국목공교육협회</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-66</td>
<td>현신규 학술상</td>
<td>(사)한국임학회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-67</td>
<td>우리지역 명산 '무등산의 사계 숲 바로알기'</td>
<td>(사)광주학교</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-68</td>
<td>제14회 산림문화작품공모전</td>
<td>산림조합중앙회</td>
<td>150,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-69</td>
<td>계간지 &lt;나무와 숲&gt; 발간</td>
<td>(사)한국산림과학 기술단체연합회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-70</td>
<td>문화융성을 위한 대한민국 산림문화대계 발간 사업</td>
<td>(사)숲과문화연구회</td>
<td>100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-71</td>
<td>아름다운 희망나무와 푸른 '숲'사랑</td>
<td>(사)전일엔컬스</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-72</td>
<td>산친구 길동무 캠페인</td>
<td>한국등산트레킹지원센터</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-73</td>
<td>숲을 품은 힐링콘서트</td>
<td>(사)대전충남생명의숲</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-74</td>
<td>치유숲에서 비보이와 함께 춤을</td>
<td>(사)아시아문화발전센터</td>
<td>60,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-75</td>
<td>청소년녹색숲사랑공동체 캠페인</td>
<td>(사)경남환경연합</td>
<td>40,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-76</td>
<td>숲으로가자` 전국 어린이 녹색캠페인 사업</td>
<td>(사)한국숲유치원협회</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-77</td>
<td>제3회 녹색문학상</td>
<td>(사)한국산림문학회</td>
<td>90,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-78</td>
<td>임산물 홍보 및 전시회</td>
<td>(사)한국임업후계자협회</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-79</td>
<td>사람과 숲의 미래를 위한 녹색캠페인</td>
<td>(사)기후변화교육센터</td>
<td>53,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-80</td>
<td>녹색문화박람회</td>
<td>(사)한국DIY가구공방협회</td>
<td>80,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-81</td>
<td>지리산둘레길 아트프로젝트</td>
<td>(사)숲길</td>
<td>70,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-82</td>
<td>조은앙상블과 함께하는 녹색힐링 콘서트</td>
<td>조은음악나눔원</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-83</td>
<td>녹색캠페인 사업</td>
<td>(사)전국산림보호협회</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-84</td>
<td>숲길웰빙걷기대회</td>
<td>(재)대한걷기연맹</td>
<td>50,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-85</td>
<td>기획캠페인</td>
<td>녹색사업단</td>
<td>2,100,000</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>3-86</td>
<td>산림보전문화사업</td>
<td>한국산지보전협회</td>
<td>300,000</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">4. 수목원․휴양림․수목장림의 조성․운영사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">5. 야생동식물 생태공원의 조성 및 운영의 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">6. 연구개발 지원</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">7. 산림환경기능 증진을 위한 국제교류 및 협력사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
<tr class="bg">
<td colspan="2">8. 산림재해 방지 등 산림보전에 필요한 사업</td>
<td>-</td>
<td>-</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<!-- //테이블  -->
<p>&nbsp;</p>