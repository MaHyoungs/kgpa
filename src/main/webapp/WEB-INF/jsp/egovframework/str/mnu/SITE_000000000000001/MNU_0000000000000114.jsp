<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_050401.gif" alt="산림비전센터 전면, 산림비전센터 1층로비" /></div>
<h3 class="icon1 mT20">설립 목적 및 기능</h3>
<ul class="section1 icon3_list">
<li>임업인의 복지증진 및 소통의 장 마련을 위하여, 2011년 11월「산림비전센터」를 설립</li>
<li>임업 선진화를 위한 국내․외 세미나 개최, 산․관․학 협의체 운영 등 임업분야 정보와 정책 교류의 장 역할</li>
<li>숲 체험교육, 산림치유, 도시 숲 등 산림의 다양한 역할에 대한 교육․홍보 기능 수행</li>
<li>산림청 서울사무소, 임우회, AFoCO(아시아산림협력기구) 사무국 등 임업관련 기관 및 단체, 일반기업 등 입주기관과 밀착하여 교류․지원하고 나아가 임업인의 편의와 복지 증진에 기여</li>
</ul>
<h3 class="icon1">건물개요</h3>
<ul class="section1 icon3_list">
<li>소 재 지 : 서울특별시 영등포구 국회대로 62길 9(KBS 맞은편)</li>
<li>규 모 : 지상 11층 / 지하 4층</li>
<li>대지면적 : 793㎡(240坪)</li>
<li>연 면 적 : 7,110㎡(2,151坪)</li>
<li>주차공간 : 53대(기계식 50대, 옥외 3대)</li>
</ul>
<h3 class="icon1">찾아오시는길</h3>
<div class="map mT20">네이버 지도 나오게 해주세요</div>
<h4 class="icon2">1.지하철</h4>
<ul class="icon3_list section2">
<li>지하철 9호선 국회 의사당 역 6번 출구에서 삼환까뮤 본관 후문 쪽으로 도보</li>
<li>지하철 5호선 여의도역 3번 출구로 나오시어 여의도공원을 지나 KBS 쪽으로 도보 (KBS 맞은 편 위치)</li>
</ul>
<h4 class="icon2">2.시내버스</h4>
<ul class="icon3_list section2">
<li>서울역에서 162, 505번(서울역 버스환승센터), 261(손기정 체육공원 입구), 262번(서계동)</li>
<li>서울고속버스 터미널에서 362 샛강 역 앙카라공원 정류장 하차</li>
</ul>
<h4 class="icon2">3. 승용차(대전 &rarr; 서울방향)</h4>
<ul class="icon3_list section2">
<li>경부고속도로&rarr;올림픽대로&rarr;노량대교&rarr;영등포 로터리, 수산시장, 여의도 방면 우측방향 &rarr; 여의대방로 길&rarr;의사당대로 1길</li>
</ul>
<h4 class="icon2">문의처 : 「산림비전센터」(☎ : 02-782-2612, Fax : 02-782-4321)</h4>