<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon1">회의장 규모</h3>
<div class="mT20 mB20"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_050402.gif" alt="대회의장 (2F), 국제회의장(10F), 국제회의장 통역실" /></div>
<table class="chart" summary="녹색사업단 회의장 규모 정보를 나타낸표로 각실별 실면적, 좌석수, 부대시설 항목을 제공하고있습니다"><caption>녹색사업단 회의장 규모 정보</caption>
<thead>
<tr><th scope="col">구분</th><th scope="col">실면적</th><th scope="col">좌석수</th><th scope="col">부대시설</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td>국제회의장(10F)</td>
<td>109.7㎡ (33.2평)</td>
<td>20석(배석 16석)</td>
<td class="alL">&bull; 빔프로젝트 +스크린<br />&bull; 동시통역실(3) <br />&bull; 무선통역기(40)</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>대회의장(2F)</td>
<td>226.8㎡(68.6평)</td>
<td>100석</td>
<td class="alL" colspan="2">&bull; 빔프로젝트(2) + 스크린(2) <br /> &bull; LED TV(2)</td>
</tr>
<tr>
<td>중회의장(2F)</td>
<td>113.4㎡(34.3평)</td>
<td>50석</td>
<td class="alL" colspan="2">&bull; 빔프로젝트(1) + 스크린(1) <br /> &bull; LED TV(1)</td>
</tr>
</tbody>
</table>
<p class="mB40">※ 2F 중회의장은 대회의장 분리사용</p>
<h3 class="icon1">용도 : 각종 회의, 세미나, 교육, 기자회견 등</h3>
<div class="mT20 mB40"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m5/img_050403.gif" alt="대회의장 행사 광경, 국제회의장 회의 광경" /></div>
<h3 class="icon1">회의장 대여로(VAT)별도</h3>
<table class="chart" summary="녹색사업단 회의장 대여료 정보를 나타낸표로 각 시설별 좌석수, 이용자, 시간당요금, 비고 항목을 제공하고 있습니다 "><caption>녹색사업단 회의장 대여료 정보</caption>
<thead>
<tr><th scope="col">시설별</th><th scope="col">좌석수</th><th scope="col">이용자</th><th scope="col">시간당 요금</th><th scope="col">비고</th></tr>
</thead>
<tbody>
<tr>
<td rowspan="2">국제회의장</td>
<td rowspan="2">20(배석 16석)</td>
<td>일반법인 / 개인</td>
<td>200,000</td>
<td rowspan="2">&nbsp;</td>
</tr>
<tr>
<td>산림청 소관 비영리법인</td>
<td>140,000</td>
</tr>
<tr>
<td rowspan="2">대회의장</td>
<td rowspan="2">80~100</td>
<td>일반법인 / 개인</td>
<td>100,000</td>
<td rowspan="2">&nbsp;</td>
</tr>
<tr>
<td>산림청 소관 비영리법인</td>
<td>70,000</td>
</tr>
<tr>
<td rowspan="2">중회의장</td>
<td rowspan="2">40~50</td>
<td>일반법인 / 개인</td>
<td>50,000</td>
<td rowspan="2">&nbsp;</td>
</tr>
<tr>
<td>산림청 소관 비영리법인</td>
<td>35,000</td>
</tr>
<tr>
<td>부대시설</td>
<td>빔프로젝트는 이용자에 상관없이 30,000원</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p class="mB10">※회의장 사용은 신청서를 다운로드 받아 작성 후 이메일(kmchoi@kgpa.or.kr) 또는 Fax(02-782-4321)로 송부 해주시기 바라며, 예약시간 초과 이용 시 시간당 사용료를 추가 징수 (관련 문의전화 : 02-782-2612)</p>
<p><a class="cbtn" href="/attachfiles/file/20130228.hwp"><span class="icon_note">회의실 사용 신청서 다운로드</span></a></p>