<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="tabs">
<li class="active"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000125">이사장인사말</a></li>
<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000126">이사장프로필</a></li>
</ul>
<div class="greeting"><strong class="hdn">숲을 통한 국민행복실현 이사장 허경태</strong> <strong>숲을 통한 국민행복실현과 해외산림 자원확보를 선도하는 녹색사업단이 되겠습니다</strong>
<p>안녕하십니까? 녹색사업단 이사장 허경태입니다. <br />녹색사업단 홈페이지를 찾아주셔서 감사합니다.</p>
<p>녹색사업단은 「산림자원의 조성 및 관리에 관한 법률」 제 62조와 민법」 제32조에 따라 2006년에 설립된 산림청 산하의<br />공공기관입니다. <br />「 산림자원의 조성 및 관리에 관한 법률」에 따라 녹색자금을 위탁 관리하고 있으며, 녹색자금의 공정하고 투명한 관리와 효율성을 높이기 위하여 노력하고 있습니다.</p>
<p>또한, 2008년부터는 글로벌사업본부를 설치하여 해외조림사업, 개도국 ODA사업, 글로벌 인재양성은 물론 탄소흡수원 확충을 통한 기후변화 대응에도 앞장서고 있습니다.<br />녹색사업단은 산림을 통한 국민의 삶의 질 향상에 이바지하고 해외산림자원 개발 전문기관으로 도약하기 위해 항상 노력하겠습니다.</p>
<p>정보3.0에 앞장서고 국민들과 소통하는 녹색사업단이 될 수 있도록 하겠습니다.</p>
<p>앞으로도 여러분의 많은 관심과 성원 부탁드립니다.</p>
<div class="sign"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/img_sign.gif" alt="이사장 허경태" /></div>
</div>