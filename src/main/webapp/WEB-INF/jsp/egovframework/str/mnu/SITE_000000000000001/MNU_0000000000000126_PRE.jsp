<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="tabs">
<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000125">단장인사말</a></li>
<li class="active"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000126">단장프로필</a></li>
</ul>
<div class="profile">
<div class="top">단장 <strong>허경태</strong>(許京泰) <span>kOREA GREEN PROMOTION AGENCY</span></div>
<div class="info">
<h3>학력</h3>
<ul class="mini">
<li><strong>2006.02</strong><span>충남대학교 대학원(박사)</span></li>
<li><strong>1991.02</strong><span>서울대학교 행정대학원(석사)</span></li>
<li><strong>1981.02</strong><span>서울대학교 임산가공학교 졸업(학사)</span></li>
<li><strong>1977.01</strong><span>충남고등학교 졸업</span></li>
</ul>
</div>
<div class="info">
<h3>경력</h3>
<ul>
<li><strong>2013.10 ~</strong><span>현재녹색사업단장</span></li>
<li><strong>2013.01 ~ 2013.09</strong><span>동부지방산림청장</span></li>
<li><strong>2011.02 ~ 2012.01</strong><span>한국농촌경제연구원 파견</span></li>
<li><strong>2009.04 ~ 2011.02</strong><span>산림청 산림이용국장</span></li>
<li><strong>2006.04 ~ 2009.04</strong><span>산림청 산림보호국장</span></li>
<li><strong>2003.01 ~ 2006.04</strong><span>북부지방산림청장</span></li>
<li><strong>2002.01 ~ 2002.12</strong><span>산림청 사유림지원국장</span></li>
<li><strong>2001.01 ~ 2002.01</strong><span>산림청, 중앙공무원교육원 파견</span></li>
<li><strong>2001.01 ~ 2001.01</strong><span>임업연구원 임업연수부장 직무대리</span></li>
<li><strong>1999.05 ~ 2001.01</strong><span>산림청 임업정책국 산지관리과장</span></li>
<li><strong>1998.03 ~ 1999.05</strong><span>산림청 자원조성국 산림토목과장</span></li>
<li><strong>1998.01 ~ 1998.03</strong><span>산림청</span></li>
<li><strong>1997.01 ~ 1998.01</strong><span>세종연구소 파견</span></li>
<li><strong>1997.01 ~ 1997.01</strong><span>산림청</span></li>
<li><strong>1995.01 ~ 1996.12</strong><span>임업정책국 임산물유통과장</span></li>
</ul>
</div>
</div>