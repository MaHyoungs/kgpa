<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="purpose">
<h3>설립근거</h3>
<ul class="icon3_list">
<li>산림자원의 조성 및 관리에 관한 법률 제62조</li>
<li>민법 제 32조</li>
<li>농림수산식품부 및 그 소속청장 소관에 속하는 비영리 법인 설립 및 감독에 관한 규칙</li>
</ul>
</div>
<div class="purpose">
<h3>설립목적</h3>
<ul class="icon3_list">
<li>산림환경을 보호하고 산림의 기능을 증진하는데 소요되는 시설비용 등의 경비를 지원하기 위하여 설치한 산림환경기능증진자금(녹색자금)을 공정하고 투명한 관리로 생활 숲 조성, 산림 체험교육지원 등 지속가능한 산림환경의 조성</li>
<li>해외에서 산림자원을 확보하고 기후변화 대응 및 해외산림환경기능을 증진하기 위한 사업과 국제교류 및 협력 사업을 수행</li>
</ul>
</div>
<div class="purpose">
<h3>경영목표</h3>
<ul class="icon3_list">
<li>녹색자금의 투명하고 효율적 운용ㆍ관리</li>
<li>녹색자금 지원사업의 효율성 강화</li>
<li>지속가능한 산림환경 조성</li>
<li>산림의 환경기능 증진을 통한 국민의 삶의 질 향상</li>
<li>기후변화 대응에 필요한 해외산림사업 지원 및 수행</li>
</ul>
</div>