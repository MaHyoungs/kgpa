<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="tit_histroy"><strong><span>국민과 함께 열어가는 녹색세상, <em>녹색사업단의 연혁</em>입니다.</span></strong></div>
<div class="history bg1">
<h3>2000 ~ 2014</h3>
<ul>
<li><strong>2011. 12 </strong><span>"산림탄소센터" 운영 </span></li>
<li><strong>2011. 08. 21 </strong><span>제3대 장찬식 단장 취임</span></li>
<li><strong> 2010. 06 </strong><span>2006~2009년 복권기금사업 성과평가 결과 4년 연속 최우수기관 </span></li>
<li><strong>2010. 03 </strong><span>"해외산림자원개발 기관"으로 지정 </span></li>
<li><strong>2008. 10. 01 </strong><span>"해외산림사업본부" 설치 사업개시 </span></li>
<li><strong>2008. 07. 10 </strong><span>제2대 조현제 단장 취임 </span></li>
<li><strong>2008. 06. 23 </strong><span>"녹색사업단"으로 명칭 변경 </span></li>
<li><strong>2007. 12. 21 </strong><span>산림자원의 조성 및 관리에 관한 법률 개정 공포 </span></li>
<li><strong>2007. 05 </strong><span>'06년도 복권기금사업 성과평가 1등급기관 선정 (복권위원회) </span></li>
<li><strong>2007. 04. 13 </strong><span>기획예산처장관으로부터 공공기관의 운영에 관한 법률 제5조제4항에 의하여 녹색자금관리단을 "기타 공공기관"으로 지정.고시 </span></li>
<li><strong>2006. 09. 19 </strong><span>초대 최대순 단장 취임 <br />재단법인 녹색자금관리단 설립등기<br />산림청으로부터 녹색자금 관리업무 위탁수행 </span></li>
<li><strong>2006. 09. 15 </strong><span>산림청장의 법인설립허가 </span></li>
<li><strong>2006. 08 </strong><span>녹색자금 관리업무 이관 (산림조합중앙회 - 산림청) </span></li>
<li><strong>2005. 08 </strong><span>"산림자원의 조성 및 관리에 관한법률" 공포 ('06. 8. 5시행) </span></li>
<li><strong>2004. 04 </strong><span>국무총리 소속 [복권위원회] 로부터 복권 위탁 발행 </span></li>
</ul>
</div>
<div class="history bg2">
<h3>1990 ~ 1999</h3>
<ul>
<li><strong>1999. 09 </strong><span>제1회차 녹색복권 발행 </span></li>
<li><strong>1998. 02 </strong><span>산림법 개정 공포(법률 제5760호) - 녹색복권발행을 통해 녹색자금 설치 </span></li>
<li><strong>1998. 09 </strong><span>여론수렴 및 국민적 공감대 형성을 위한 공청회 개최 </span></li>
<li><strong>1998. 07 </strong><span>[녹색복권] 도입을 위한 산림법 개정안 발의 (여야의원37인) </span></li>
<li><strong>1998. 03 </strong><span>국회 환경포럼에서 "산림의 친환경적 수자원 증대 및 재원확보방안" 발표 </span></li>
</ul>
</div>