<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="vision">
<div class="v01"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/logo.gif" alt="산/림/청 녹색사업단" /> <strong>미션</strong> 산림기능을 증진하고 <br />해외산림사업을 선도</div>
</div>
<div class="vision1"><strong>비전</strong> <span>|</span>국내외 산림자원의 가치를 확대하고 선도하는 공공기관</div>
<div class="vision2">
<div class="target1"><strong>전략목표</strong>
<ul>
<li>산림공익서비스 확대</li>
<li>산림자원 가치 증진</li>
<li>글로벌 산림사업 활성화</li>
<li>경영관리체계 고도화</li>
</ul>
</div>
<div class="target2"><strong>전략과제</strong><ol>
<li>① 녹색자금사업 품질 제고</li>
<li class="line">② 소외계층 산림체험교육 활성화</li>
<li>③ 산림탄소상쇄사업 활성화</li>
<li class="line">④ 산림사업지 관리</li>
<li>⑤ 해외산림사업 기반 구축</li>
<li class="line">⑥ 산림협력 지원 확대</li>
<li>⑦ 성과중심 조직문화 구축</li>
<li>⑧ 고객중심 경영체계 확립</li>
</ol></div>
</div>