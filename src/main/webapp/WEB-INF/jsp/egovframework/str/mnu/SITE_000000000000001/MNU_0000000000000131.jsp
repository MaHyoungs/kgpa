<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon">조직현황</h3>
<div class="mB40"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/img_org.gif" alt="녹색사업단 조직현황" longdesc="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/longdesc/org.html" /></div>
<h3 class="icon">연락처</h3>
<table class="chart" summary="녹색사업단 연락처 정보를 나타낸표로 각 부서에따른 성명, 전화번호 항목을 제공하고 있습니다"><caption>녹색사업단 연락처 정보</caption>
<thead>
<tr><th scope="col" colspan="2">부서명</th><th scope="col" colspan="2">성 명</th><th scope="col">전화번호</th></tr>
</thead>
<tbody>
<tr>
<td class="bg" colspan="2"><strong>녹색사업단장</strong></td>
<td class="bg" colspan="2"><strong>허경태</strong></td>
<td class="bg">042&ndash;603&ndash;7301</td>
</tr>
<tr>
<td class="bg" colspan="2"><strong>검사역</strong></td>
<td class="bg" colspan="2"><strong>하헌경</strong></td>
<td class="bg">042&ndash;603&ndash;7309</td>
</tr>
<tr>
<td rowspan="16">운<br /> 영<br /> 총<br /> 괄<br /> 본<br /> 부</td>
<td class="bg"><strong>본부장</strong></td>
<td class="bg" colspan="2">　</td>
<td class="bg">　</td>
</tr>
<tr>
<td rowspan="5">운영지원팀</td>
<td colspan="2"><strong>어경해</strong></td>
<td>042&ndash;603&ndash;7302</td>
</tr>
<tr>
<td colspan="2">김태규</td>
<td>042&ndash;603&ndash;7333</td>
</tr>
<tr>
<td colspan="2">임민우</td>
<td>042-603-7305</td>
</tr>
<tr>
<td colspan="2">강효경</td>
<td>042&ndash;603&ndash;7307</td>
</tr>
<tr>
<td colspan="2">반형철</td>
<td>042&ndash;603&ndash;7304</td>
</tr>
<tr>
<td rowspan="5">기획전략팀</td>
<td colspan="2"><strong>장윤호</strong></td>
<td>042-603-7334</td>
</tr>
<tr>
<td rowspan="2">기획전략</td>
<td>이근욱</td>
<td>042-603-7337</td>
</tr>
<tr>
<td>민서정</td>
<td>042-603-7338</td>
</tr>
<tr>
<td rowspan="2">신사업팀</td>
<td>조두연</td>
<td>042-603-7342</td>
</tr>
<tr>
<td>우병건</td>
<td>042-603-7332</td>
</tr>
<tr>
<td rowspan="2">&nbsp;</td>
<td rowspan="2">대외협력</td>
<td><strong>최관묵</strong></td>
<td>042-603-7320</td>
</tr>
<tr>
<td>백현진</td>
<td>042-603-7331</td>
</tr>
<tr>
<td rowspan="3">산림탄소팀</td>
<td colspan="2"><strong>김태규</strong></td>
<td>042-603-7333</td>
</tr>
<tr>
<td colspan="2">김민지</td>
<td>042-603-7328</td>
</tr>
<tr>
<td colspan="2">장은석</td>
<td>042-603-7343</td>
</tr>
<tr>
<td rowspan="11">국<br />내<br />사<br />업<br />본<br />부</td>
<td class="bg"><strong>본부장</strong></td>
<td class="bg" colspan="2">&nbsp;</td>
<td class="bg">&nbsp;</td>
</tr>
<tr>
<td rowspan="4">녹색자금팀</td>
<td colspan="2"><strong>김형수</strong></td>
<td>042-603-7316</td>
</tr>
<tr>
<td colspan="2">김동희</td>
<td>042-603-7318</td>
</tr>
<tr>
<td colspan="2">안효현</td>
<td>042-603-7330</td>
</tr>
<tr>
<td colspan="2">김대광</td>
<td>042-603-7341</td>
</tr>
<tr>
<td rowspan="6">산림교육팀<br />(054-977)</td>
<td colspan="2"><strong>국형일</strong></td>
<td>054-977-8771</td>
</tr>
<tr>
<td colspan="2">김창휴</td>
<td>042-603-7317</td>
</tr>
<tr>
<td colspan="2">이지은</td>
<td>054-977-8772</td>
</tr>
<tr>
<td colspan="2">조성열</td>
<td>054-977-8774</td>
</tr>
<tr>
<td colspan="2">이혜영</td>
<td>054-977-8773</td>
</tr>
<tr>
<td colspan="2">이근배</td>
<td>054-977-8774</td>
</tr>
<tr>
<td rowspan="10">해<br />외<br />사<br />업<br />본<br />부</td>
<td class="bg"><strong>본부장</strong></td>
<td class="bg" colspan="2"><strong>공영호</strong></td>
<td class="bg">042-603-7321</td>
</tr>
<tr>
<td rowspan="5">해외자원팀</td>
<td colspan="2"><strong>전재홍</strong></td>
<td>042-603-7324</td>
</tr>
<tr>
<td colspan="2">이창배</td>
<td>070-8250-5682</td>
</tr>
<tr>
<td colspan="2">소순진</td>
<td>070-8250-5683</td>
</tr>
<tr>
<td colspan="2">정규상</td>
<td>070-8250-5691</td>
</tr>
<tr>
<td colspan="2">배우리</td>
<td>042-603-7346</td>
</tr>
<tr>
<td rowspan="4">해외협력팀</td>
<td colspan="2"><strong>이경훈</strong></td>
<td>042-603-7325</td>
</tr>
<tr>
<td colspan="2">서상혁</td>
<td>042-603-7327</td>
</tr>
<tr>
<td colspan="2">강미랑</td>
<td>042-603-7326</td>
</tr>
<tr>
<td colspan="2">유리</td>
<td>042-603-7336</td>
</tr>
</tbody>
</table>
<ul class="tel_info">
<li>
<h4>녹색사업단</h4>
<span class="addr">대전광역시 서구 둔산북로 121, 2층 206호 </span> <span>전화:042-603-7305<br /> FAX: 042-603-7310</span></li>
<li>
<h4>산림비전센터</h4>
<span class="addr">서울특별시 영등포구 국회대로62길 9</span> <span>전화: 02-782-2612<br /> FAX:02-782-4321</span></li>
<li>
<h4>산림교육팀<span>(백두대간숲생태원)</span></h4>
<span class="addr">경상북도 상주시 공성면 웅산로 705</span> <span>전화:054-536-0914 <br /> FAX:054-536-0905</span></li>
<li class="last">
<h4>산림교육팀<span>(칠곡 산림복지나눔숲)</span></h4>
<span class="addr">경상북도 칠곡군 석적읍 산73-13번지</span> <span>전화:054-977-8772<br /></span></li>
</ul>