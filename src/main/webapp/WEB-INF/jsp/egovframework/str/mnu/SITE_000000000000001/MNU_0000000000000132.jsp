<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon">심볼마크</h3>
<div class="mB20"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/img_symbol01.gif" alt="녹색사업단 심볼마크" /></div>
<div class="btn_r"><a class="cbtn" href="/attachfiles/file/logo_img.zip"><span class="icon_down">이미지 다운로드</span> </a> <a class="cbtn" href="/attachfiles/file/logo_ai.zip"><span class="icon_down">AI 다운로드 </span></a></div>
<div class="symbol_txt">
<p>녹색사업단의 이미지를 친근감있게 표현하기 위해 동그라미 3개를 조화롭게 배열하여 숲에서 <span class="green">배출되는 신선한 산소의 입자를 상징화</span>하였으며, 산림환경기능증진사업을 통해 국민의 삶의 질 향상에 기여하는 녹색사업단 이미지를 형상화하였습니다.</p>
<p>녹색원형과 나뭇잎의 형상은 생활 숲 조성과 숲 체험교육 등의 산림환경기능증진사업을 통해 국민의 삶의 질 향상에 기여하고, 해외산림자원개발 및 조성사업을 통해 해외산림지원확보, 기후변화대응 등을 통해 국가정책인 <span class="green">녹색성장</span>에 기여하는 녹색사업단의 목표와 비전을 표현</p>
<p>녹색원청색원형은 복권기금(녹색자금)의 투명한 운영과 공공기관으로서의 <span class="green">청렴한 투명경영</span>을 상징화하였으며, 노랑원형은 녹색사업단이 추구하는 숲을 통한 인간의 <span class="green"> 녹색복지ㆍ행복</span>을 표현 형상화 함</p>
</div>
<h3 class="icon">로고타입</h3>
<div class="mB20"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/img_symbol02.gif" alt="녹색사업단 로고타입" /></div>
<p class="mB40">심볼마크와 함께 녹색사업단의 이미지 형성을 위하여 모든 시각 매체에 광범위하게 전달하는 중요요소로서 독특한 전용서체 개발로 녹색사업단의 뚜렷한 이미지를 표현</p>
<h3 class="icon">로고 보조색상</h3>
<div class="mB20"><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m7/img_symbol03.gif" alt="녹색사업단 보조색상 BLACK CYAN" /></div>
<p>심볼마크와 함께 녹색사업단의 이미지 형성을 위하여 모든 시각 매체에 광범위하게 전달하는 중요요소로서 독특한 전용서체 개발로 녹색사업단의 뚜렷한 이미지를 표현</p>