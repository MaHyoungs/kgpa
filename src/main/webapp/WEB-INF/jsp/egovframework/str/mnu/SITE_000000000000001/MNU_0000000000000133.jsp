<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="map" class="map">&nbsp;</div>
<div class="trf01">
<h3 class="icon">고속버스ㆍ시외버스 이용시</h3>
</div>
<div class="pbg mB30">
<h4 class="icon1">대전고속터미널, 동부시외버스터미널(같은위치)</h4>
<ul class="icon3_list section1">
<li>버스 201번 이용(2.8km, 6정류소 경유) 승차 정류소 : 고속버스터미널 하차 정류소 : 목척교</li>
<li>버스 514번 환승(5.7km, 12정류소 경유) 승차 정류소 : 목척교 하차 정류소 : 법원/검찰청</li>
</ul>
<h4 class="icon1">대전고속터미널, 동부시외버스터미널(같은위치)</h4>
<ul class="icon3_list section1">
<li>버스 916번 이용(11.7km, 28정류소 경유) 승차 정류소 : 서부터미널 하차 정류소 : 법원/검찰청</li>
</ul>
</div>
<div class="trf02">
<h3 class="icon">철도ㆍ고속철도(KTX)</h3>
</div>
<div class="pbg mB30">
<h4 class="icon1">대전역</h4>
<ul class="icon3_list section1">
<li>지하철 승차하여 대전시청역 하차 5번출구로 나와 도보로 두 블록 직진</li>
</ul>
<h4 class="icon1">대전고속터미널, 동부시외버스터미널(같은위치)</h4>
<ul class="icon3_list section1">
<li>버스 612번 이용(1.7km, 5정류소 경유) 승차 정류소 : 서대전역 하차 정류소 : 교원연합회</li>
<li>버스 108번 환승(5.4km, 11정류소 경유) 승차 정류소 : 한국철도시설공단 하차 정류소 : 법원/검찰청</li>
</ul>
</div>
<div class="trf03">
<h3 class="icon">일반자가용</h3>
</div>
<div class="pbg mB30">
<h4 class="icon1">대전TG이용</h4>
<ul class="icon3_list section1">
<li>톨게이트에서 시내방향 직진 &gt; 동부4가 우회전 &gt; 중리4가 정부청사방면 직진 &gt; 한밭대교 &gt; 사학연금사거리 좌회전 &gt; 홈플러스대전점 우회전 &gt; 아너스빌(녹색사업단)</li>
</ul>
<h4 class="icon1">유성TG이용</h4>
<ul class="icon3_list section1">
<li>돌게이트에서 우회전하여 직진 &gt; 충대앞 지하차도 &gt; 갑천대교 &gt; 청부청사방면 직진 &gt; 무지개아파트앞 지하차도 &gt; 사학연금사거리 우회전 &gt; 홈플러스대전점 우회전 &gt; 아너스빌(녹색사업단)</li>
<li>유성TG를 이용하는 것이 가장 가깝고 찾기 쉬움.</li>
</ul>
</div>