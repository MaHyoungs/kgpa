<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="open_mng"><dl class="state01"><dt><span>&nbsp;</span>일반현황</dt><dd>
<ul>
<li><a class="active" href="/attachfiles/open_mng/page01_tab01.html">&middot; 일반현황</a></li>
</ul>
</dd></dl><dl class="state02"><dt><span>&nbsp;</span>기관운영</dt><dd>
<ul>
<li><a href="/attachfiles/open_mng/page01_tab02.html">&middot; 임직원수</a></li>
<li><a href="/attachfiles/open_mng/page01_tab03.html">&middot; 임원현황</a></li>
<li><a href="/attachfiles/open_mng/page01_tab04.html">&middot; 신규채용현황 및 유연근무현황</a></li>
<li><a href="/attachfiles/open_mng/page01_tab05.html">&middot; 임원연봉</a></li>
<li><a href="/attachfiles/open_mng/page01_tab06.html">&middot; 직원평균보수</a></li>
<li><a href="/attachfiles/open_mng/page01_tab07.html">&middot; 기관장업무추진비</a></li>
<li><a href="/attachfiles/open_mng/page01_tab08.html">&middot; 복리후생비 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab09.html">&middot; 노동조합 관련현황</a></li>
</ul>
</dd></dl><dl class="state03"><dt><span>&nbsp;</span>주요사업 및 경영성과</dt><dd>
<ul>
<li><a href="/attachfiles/open_mng/page01_tab10.html">&middot; 요약대차대조표(또는 요약재무상태표)</a></li>
<li><a href="/attachfiles/open_mng/page01_tab11.html">&middot; 요약손익계산서(또는 포괄손익계산서)</a></li>
<li><a href="/attachfiles/open_mng/page01_tab12.html">&middot; 수입*지출현황</a></li>
<li><a href="/attachfiles/open_mng/page01_tab13.html">&middot; 주요사업 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab14.html">&middot; 공공기관 투자집행현황 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab15.html">&middot; 자본금 및 주주현황</a></li>
<li><a href="/attachfiles/open_mng/page01_tab16.html">&middot; 장단기 차입금현황 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab17.html">&middot; 투자 및 출자현황</a></li>
<li><a href="/attachfiles/open_mng/page01_tab18.html">&middot; 연간 출연 및 증여 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab19.html">&middot; 경영부담비용 추계 </a></li>
<li><a href="/attachfiles/open_mng/page01_tab20.html">&middot; 납세정보현황</a></li>
</ul>
</dd></dl><dl class="state04"><dt><span>&nbsp;</span>대내외 평가</dt><dd>
<ul>
<li><a href="/attachfiles/open_mng/page01_tab21.html">&middot; 경영평가 지적사항</a></li>
</ul>
</dd></dl><dl class="state05"><dt><span>&nbsp;</span>공공기관 정상화 추진</dt><dd>
<ul>
<li><a href="/attachfiles/open_mng/page01_tab22.html">&middot; 12개 주요기관의 상세 부채 정보</a></li>
<li><a href="/attachfiles/open_mng/page01_tab23.html">&middot; 복리후생 관련 8대항목</a></li>
</ul>
</dd></dl></div>
<div class="mng_box01">&nbsp;</div>