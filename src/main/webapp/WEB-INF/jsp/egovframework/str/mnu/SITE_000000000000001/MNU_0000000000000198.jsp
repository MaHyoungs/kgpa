<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h3 class="icon">녹색자금제도 도입배경</h3>
<ul class="icon3_list mB40">
<li>맑은 물, 깨끗한 공기 공급 등 산림의 환경기능에 대한 국민적 수요 증대</li>
<li>&lsquo;99. 2. 5 산림법 개정을 통하여 &ldquo;산림의 환경기능증진&rdquo;을 위한 &ldquo;녹색자금&rdquo;을 설치</li>
</ul>
<h3 class="icon">녹색자금 조성의 필요성</h3>
<ul class=" icon3_list mB40">
<li>산림은 당대뿐만 아니라 미래세대의 자산으로서 목재생산 등 경제적 기능과 깨끗한 물과 맑은 공기를 공급하고 국토를 보전하는 등 환경적&middot;공익적 기능이 매우 큰 자원임 <span class="red">- 산림의 공익적 기능은 66조원 (2007년 기준)</span></li>
<li>최근 산업화&middot;도시화에 따라 산림환경기능에 대한 수요가 급증하고 있으나 정부예산중 산림투자는 0.4%에 불과하고, 그나마 산림의 경제분야에 치중되어 환경기능증대를 위한 투자는 매우 미약한 실정임</li>
<li>&lsquo;92 리우선언에서도 수자원 보전, CO2흡수, 토양보전 등 산림환경 기능의 내부화(Internalization)를 통해 산주의 기회비용 보강을 위한 새로운 재원 확보를 권장</li>
</ul>
<h3 class="icon">도입경위</h3>
<div class="mB40">
<table class="chart2 " summary="녹색사업단 녹색자금 도입경위정보를 나타낸포로 각 날짜에 따른 내용을 나타낸표입니다"><caption>도입경위</caption><colgroup> <col width="20%" /> <col width="80%" /> </colgroup>
<thead>
<tr><th scope="col">날짜</th><th scope="col">내용</th></tr>
</thead>
<tbody>
<tr>
<td>1998. 3.20</td>
<td class="alL">국회 환경포럼에서 "산림의 친환경적 수자원 증대 및 재원확보 방안" 발표</td>
</tr>
<tr>
<td>1998. 7. 6</td>
<td class="alL">녹색복권도입을 위한 산림법개정안을 여야 의원 37인이 발의</td>
</tr>
<tr>
<td>1998. 9. 3</td>
<td class="alL">여론수렴 및 국민적 공감대 형성을 위한 공청회 개최</td>
</tr>
<tr>
<td>1998.12.29</td>
<td class="alL">국회에서 의결</td>
</tr>
<tr>
<td>1999. 2. 5</td>
<td class="alL">산림법 개정공포(법률 제5760호)</td>
</tr>
<tr>
<td>1999. 9. 9</td>
<td class="alL">녹색복권 발행</td>
</tr>
<tr>
<td>2002. 5. 1</td>
<td class="alL">인터넷녹색복권 발행</td>
</tr>
<tr>
<td>2002.12. 2</td>
<td class="alL">온라인연합복권 발행(7개 기관 연합발행 참여)</td>
</tr>
<tr>
<td>2004. 4. 1</td>
<td class="alL">국무총리 산하『복권위원회』로부터 복권위탁발행</td>
</tr>
<tr>
<td>2005. 8. 4</td>
<td class="alL">산림자원의조성 및 관리에 관한 법률 공포</td>
</tr>
<tr>
<td>2006. 9.19</td>
<td class="alL">녹색자금관리단 설립</td>
</tr>
</tbody>
</table>
</div>
<h3 class="icon">녹색자금의 조성재원</h3>
<ul class=" icon3_list mB40">
<li>정부 외의 자의 출연금</li>
<li>「복권 및 복권기금법」제23조제1항의 규정에 의하여 배분된 복권수익금</li>
<li>녹색자금의 운용으로 생기는 수익금 등</li>
</ul>
<h3 class="icon">녹색자금의 사용용도</h3>
<ul class=" icon3_list mB40">
<li>맑은 물 공급을 위한 산림환경 개선사업</li>
<li>공해방지 및 경관보전을 위한 산림 및 도시림 조성사업</li>
<li>청소년 등을 위한 산림체험활동시설의 설치&middot;운영 및 교육&middot;홍보사업</li>
<li>수목원&middot;휴양림&middot;수목장림(樹木葬林)의 조성&middot;운영사업</li>
<li>산림환경기능증진과 관련한 임업인의 교육 및 복지증진사업 등</li>
</ul>
<h3 class="icon">녹색자금 조성현황</h3>
<div class="alR ft11">(단위 : 백만원)</div>
<div class="mB40">
<table class="chart" summary="녹색사업단 녹색자금 조성현황정보를 나태낸표로 각 구분별 자금조성(A), 자금지원등(B), 사용잔액(A-B) 항목을 제공한 표입니다"><caption>녹색사업단 녹색자금 조성현황정보</caption><colgroup> <col width="25%" /> <col width="25%" /> <col width="25%" /> <col width="25%" /> </colgroup>
<thead>
<tr><th scope="col">구분</th><th scope="col">자금조성(A)</th><th scope="col">자금지원등(B)</th><th scope="col">사용잔액(A-B)</th></tr>
</thead>
<tbody>
<tr>
<td>합 계</td>
<td>223,440</td>
<td>193,209</td>
<td>30,231</td>
</tr>
<tr>
<td>&lsquo;99～'02</td>
<td>13,449</td>
<td>1,650</td>
<td>11,799</td>
</tr>
<tr>
<td>2003년</td>
<td>78,836</td>
<td>12,756</td>
<td>66,080</td>
</tr>
<tr>
<td>2004년</td>
<td>40,754</td>
<td>13,713</td>
<td>27,041</td>
</tr>
<tr>
<td>2005년</td>
<td>25,821</td>
<td>19,682</td>
<td>6,139</td>
</tr>
<tr>
<td>2006년</td>
<td>24,077</td>
<td>48,946</td>
<td>△24,869</td>
</tr>
<tr>
<td>2007년</td>
<td>23,109</td>
<td>23,806</td>
<td>△697</td>
</tr>
<tr>
<td>2008년</td>
<td>17,394</td>
<td>72,656</td>
<td>△55,262</td>
</tr>
</tbody>
</table>
</div>
<h3 class="icon">녹색자금사업 추진체계</h3>
<div><img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/page/m6/img_m6_01.gif" alt="녹색사업단(사업평가팀)은 복권위원회로부터 작성지침통보를 받고 녹색자금사업 시행자에게 사업신청안내후 사업신청서를 받아 녹색자금운용심의회에 사업계획 심의요청후 사업계획 심의확정받아  산림청에 사업계획 승인신청 후 사업계획 승인을 받게되면 복권위원회에  사업계획 승인신청후  사업계획승인을 받으면 녹색자금사업 시행자에게 사업확정통보 및 자금교부한다 후에 사업완료보고를 받고  1차 사업평가후 복권위원회에  1차 사업평가 결과보고를하고 사업완료보고를 시행한후 2차사업평가결과를 전달받는다" /></div>
<p><a class="cbtn" title="새창열림" onclick="" href="http://ago.kgpa.or.kr/resch/web/resch_Usr_List.php" target="_blank">녹색자금 사업추진실적 및 계획</a></p>