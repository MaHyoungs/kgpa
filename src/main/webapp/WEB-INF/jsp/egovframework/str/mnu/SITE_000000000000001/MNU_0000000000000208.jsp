<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<h3 class="icon">산림비전센터</h3>
	<div id="map" class="map">&nbsp;</div>

	<div class="trf02">
		<h3 class="icon">지하철</h3>
	</div>
	<div class="pbg mB30">
		<h4 class="icon1">지하철 9호선</h4>
		<ul class="icon3_list section1">
			<li>국회의사당역 6번 출구→삼환까뮤 본관 후문 쪽으로 도보</li>
		</ul>
		<h4 class="icon1">지하철 5호선 </h4>
		<ul class="icon3_list section1">
			<li>여의도역 3번 출구→여의도공원을 지나 KBS 쪽으로 도보</li>
		</ul>
	</div>



	<div class="trf01">
		<h3 class="icon">시내버스</h3>
	</div>
	<div class="pbg mB30">
		<h4 class="icon1">서울역 </h4>
		<ul class="icon3_list section1">
			<li>162, 505번(서울역 버스환승센터),</li>
			<li>261번(손기정 체육공원 입구), 262번(서계동)</li>
		</ul>
		<h4 class="icon1">서울고속버스터미널</h4>
		<ul class="icon3_list section1">
			<li>362번(샛강역 앙카라공원 정류장)</li>
		</ul>
	</div>


	<div class="trf03">
		<h3 class="icon">승용차(대전 → 서울방향)</h3>
	</div>
	<div class="pbg mB30">
		<ul class="icon3_list section1">
			<li>경부고속도로→올림픽대로→노량대교→영등포로터리, 수산시장, 여의도 방면 우측방향→여의대방로 길→의사당대로 1길</li>
		</ul>
	</div>

	<p>※문의처 : 산림비전센터(☎ : 02-782-2612, Fax : 02-782-4321)  </p>
