<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<h3 class="icon">칠곡나눔숲체험</h3>
		<div id="map" class="map">&nbsp;</div>

		<div class="trf03">
			<h3 class="icon">승용차</h3>
		</div>
		<div class="pbg mB30">
			<h4 class="icon1">경부고속도로- </h4>
			<ul class="icon3_list section1">
				<li><strong  class="green">남구미IC</strong><br />낙동강변로 고속도로 진출 후 859m 이동 - 임오삼거리 강변서로 산업단지 방면으로 좌회전 후 776m 이동 - 남구미로 3산업단지 방면으로 우측도로 1.2km 이동 - 장곡길 왜관 방면으로 우회전후 2.1km 이동 - 유학로 좌회전 후 167m 이동 - 도착</li>
			</ul>
			<h4 class="icon1">중앙고속도로</h4>
			<ul class="icon3_list section1">
				<li><strong  class="green"> 다부IC</strong><br />고속도로 진출 후 44m 이동 - 호국로 왜관 방면으로 좌회전 후 1.8km 이동 - 유학로 석적,중리(구미)방면으로 우회전 후 3.8km 이동 - 도착</li>
			</ul>
		</div>



		<div class="trf01">
			<h3 class="icon">대중교통</h3>
		</div>
		<div class="pbg mB30">
			<h4 class="mB20">시외버스, 열차 이용시 아래 시내버스로 환승하여, 석적읍 중리 부영아파트 인근에서 하차 후 택시 이용 바랍니다.</h4>

			<h3 class="icon">시외버스</h3>

			<div class="section">
				<ul class="icon3_list">
					<li>구미터미널에서 하차 후 시내버스 또는 택시 이용바랍니다.</li>
				</ul>
				<table class="chart2" summary="칠곡나눔숲체험 시외버스 이용 시스템 링크 제공">
				<caption>칠곡나눔숲체험 시외버스로 오시는길</caption>
					<colgroup>
						<col width="40%" />
						<col width="60%" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">전국시외버스통합 예약, 예매 시스템</th>
							<td>
								<a href="https://www.busterminal.or.kr/" target="_blank" title="새창으로 열립니다.">https://www.busterminal.or.kr/</a>
							</td>
						</tr>
						<tr>
							<th scope="row">전국고속버스운송사업조합 시스템</th>
							<td>
								<a href="http://kobus.co.kr/web/main/index.jsp" target="_blank" title="새창으로 열립니다.">http://kobus.co.kr/web/main/index.jsp</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h3 class="icon">시내버스</h3>
			<div class="section">
				<ul class="icon3_list">
					<li>시내버스 탑승 후 칠곡군 석적읍 중리 부영아파트 인근에서 하차 후 택시 이용 바랍니다.</li>
					<li>버스 운행시간은 칠곡 및 구미버스정보 시스템을 참고하시길 바랍니다.</li>
				</ul>
				<table class="chart2"  summary="칠곡나눔숲체험 시내버스 번호,  이용시스템 정보 링크 제공">
					<caption>칠곡나눔숲체험 시내버스로 오시는길</caption>
					<colgroup>
						<col width="40%" />
						<col width="60%" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">시내버스번호</th>
							<td>
								9번, 10번, 10-1번, 47번, 55번, 110번, 555번
							</td>
						</tr>
						<tr>
							<th scope="row">칠곡버스정보시스템</th>
							<td>
								<a title="새창으로 열립니다." target="_blank" href="http://bus.chilgok.go.kr/GCBIS/web/page/index.do">http://bus.chilgok.go.kr/GCBIS/web/page/index.do</a>
							</td>
						</tr>
						<tr>
							<th scope="row">구미버스정보시스템</th>
							<td>
								<a title="새창으로 열립니다." target="_blank" href="http://bis.gumi.go.kr/GCBIS/web/page/index.do">http://bis.gumi.go.kr/GCBIS/web/page/index.do</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="trf02">
			<h3 class="icon">열차</h3>
		</div>
		<div class="pbg mB30">
			<div class=" section">
				<ul class="icon3_list">
					<li>왜관역 또는 구미역 에서 하차 후 버스 및 택시 이용 바랍니다.</li>
				</ul>

				<table class="chart2">
					<colgroup>
						<col width="40%" />
						<col width="60%" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">코레일 홈페이지</th>
							<td>
								<a href="http://www.letskorail.com/" target="_blank" title="새창으로 열립니다.">http://www.letskorail.com/</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>

		<p>※ 문의처 : 칠곡나눔숲체원(☎ : 05-977-8772) </p>