<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<h3 class="icon">백두대간숲생태원</h3>
	<div id="map" class="map">&nbsp;</div>

	<div class="trf03">
		<h3 class="icon">승용차 - 각 IC에서 약 30분 내외 소요됩니다.</h3>
	</div>
	<div class="pbg mB30">
		<h4 class="icon1">중부내륙고속도로 </h4>
		<ul class="icon3_list section1">
			<li><strong class="green">남상주IC</strong><br />김천방면으로 우회전 - 약 10km 직진(3번국도) - 공성면 옥산리 - 황간/ 모동방면으로 우회전(68번지방도) - 백두대간 숲생태원</li>
			<li><strong class="green">선산 IC</strong><br />상주/무을방면으로 좌회전 - 약 17km 직진(68번국도) - 거창/김천방면으로 좌회전(3번국도) - 공성면 옥산리 - 황간/모동방면으로 우회전 - 백두대간 숲생태원</li>
		</ul>
		<h4 class="icon1">경부고속도로 </h4>
		<ul class="icon3_list section1">
			<li><strong class="green">김천 IC</strong><br />상주방면으로 좌회전(514번지방도) - 직지교 사거리에서 좌회전(김천시청방면) - 구례교차로에서 우회전 (추풍령/옥계리/구례 방면) - 좌회전(어모로) - 약 8km 직진 - 문경/상주방면으로 좌회전 - 황간/모동방면으로 좌회전 - 백두대간 숲생태원</li>
			<li><strong class="green">황간 IC</strong><br />김천/황간방면으로 우회전(4번국도) - 지하차도 - 황간교 삼거리- 상주방면으로 좌회전(49번지방도) - 우매삼거리 - 상주/모동방면으로 우회전(68번지방도) - 공성방면으로 좌회전(웅산로) - 백두대간 숲생태원</li>
		</ul>
	</div>



	<div class="trf01">
		<h3 class="icon">대중교통(2013.01.02 기준)</h3>
	</div>
	<div class="pbg mB30">
		<h4 class="mB20">시외버스, 열차 이용시 옥산에서 하차 후 시내버스(200번) 또는 택시 이용.</h4>

		<h3 class="icon">시외버스</h3>

		<div class="section">
			<table class="chart1" summary="백두대간숲생태원 대중교통 시외버스 이용 시간표를 확인하실 수 있습니다.">
				<caption>백두대간숲생태원 시외버스 시간표</caption>
				</colgroup>
				<thead>
					<tr>
						<th scope="col">출발지</th>
						<th>도착지</th>
						<th>첫차</th>
						<th>막차</th>
						<th>배차간격</th>
						<th>소요시간</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>상주터미널</td>
						<td>옥산</td>
						<td>07:10</td>
						<td>21:50</td>
						<td>20분</td>
						<td>약 25분</td>
					</tr>
					<tr>
						<td>김천터미널</td>
						<td>옥산</td>
						<td>06:30</td>
						<td>21:55</td>
						<td>30~40분</td>
						<td>약 20분</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h3 class="icon">시내버스</h3>
		<div class="section">
			<ul class="icon3_list">
				<li> 버스번호 : <span  class="green">200번</span></li>
				<li>노선 : 상주터미널 ↔ 옥산 ↔ 백두대간 숲생태원 ↔ 효곡.</li>
			</ul>


			<table  class="chart1" summary="백두대간숲생태원 대중교통 시내버스 이용 시간표를 확인하실 수 있습니다.">
				<caption>백두대간숲생태원 시내버스 시간표</caption>
				<thead>
					<tr>
						<th scope="col">출발지</th>
						<th scope="col">출발시간</th>
						<th scope="col">소요시간</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>상주터미널</td>
						<td>12시 10분, 18시 45분</td>
						<td>45분</td>
					</tr>
					<tr>
						<td>옥산</td>
						<td>12시 40분, 17시 15분</td>
						<td>15분</td>
					</tr>
					<tr>
						<td>효곡</td>
						<td>06시 40분, 13시 15분</td>
						<td>15분</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>

	<div class="trf02">
		<h3 class="icon">열차</h3>
	</div>
	<div class="pbg mB30">
		<div class=" section">

			<table class="chart1" summary="백두대간숲생태원 대중교통 이용 시간표를 확인하실 수 있습니다.">
				<caption>백두대간숲생태원 열차 시간표</caption>
				<thead>
					<tr>
						<th scope="col">행선지</th>
						<th scope="col">도착지</th>
						<th scope="col">첫차</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td rowspan="4">김천역 → 옥산역</td>
						<td>09:09</td>
						<td>09:30</td>
					</tr>
					<tr>
						<td>09:43</td>
						<td>10:03</td>
					</tr>
					<tr>
						<td>18:50</td>
						<td>19:10</td>
					</tr>
					<tr>
						<td>21:11</td>
						<td>21:31</td>
					</tr>
					<tr>
						<td rowspan="4">상주역 → 옥산역</td>
						<td>07:20</td>
						<td>07:36</td>
					</tr>
					<tr>
						<td>11:47</td>
						<td>12:05</td>
					</tr>
					<tr>
						<td>15:58</td>
						<td>16:13</td>
					</tr>
					<tr>
						<td>19:46</td>
						<td>20:02</td>
					</tr>
				</tbody>
			</table>


		</div>

	</div>

	<p>※ 문의처 : 산림비전센터(☎ : 02-782-2612, Fax : 02-782-4321) </p>