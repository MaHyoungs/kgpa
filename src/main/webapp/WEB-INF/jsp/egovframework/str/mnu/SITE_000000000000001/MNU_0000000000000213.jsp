<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="carbon_intro">
	<h3 class="icon mB30">산림탄소센터 소개</h3>


	<h4 class="icon1">설립근거 및 목적</h4>
	<ul class="icon3_list section1">
		<li>산림탄소센터는 산림의 탄소흡수기능 유지ㆍ증진을 통하여 기후변화에 대응하고 저탄소 사회를 구현하기 위해2013년 「탄소흡수원 유지 및 증진에 관한 법」 23조에 따라 설립되었습니다</li>
	</ul>


	<h4 class="icon1">주요업무</h4>
	<ul class="num_list mB50">
		<li><strong>01</strong> 산림탄소상쇄사업 계획서 검토 및 타당성 평가를 통한 사업 계획 등록</li>
		<li><strong>02</strong> 검증기관의 지정ㆍ운영</li>
		<li><strong>03</strong> 검증기관의 검증 결과에 대한 검토</li>
		<li><strong>04</strong> 한국임업진흥원이 인증한 산림탄소흡수량의 인증서 발급</li>
		<li><strong>05</strong> 등록부의 운영 및 관리에 관한 사항</li>
		<li><strong>06</strong> 기타 산림청장이 지원을 요청한 사항 등</li>
	</ul>

	<div>
		<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_vision.gif" alt="VISION 산림부문 자발적 탄소시장 주도 (해외 자발적 탄소 시장 연계, 탄소흡수원 확층, 산림탄소상쇄제도 활성화)" />
	</div>
</div>