<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="tab_box_sub">
				<div class="tabs2 ">
					<ul class="tab3 tab_sub">
						<li class="active"><a href="#tab_sub1">사업유형</a></li>
						<li><a href="#tab_sub2">사업체계도</a></li>
						<li><a href="#tab_sub3">사업등록현황</a></li>
					</ul>
				</div>

				<div class="tab_content_sub">
					<div id="tab_sub1" class="tab_cont_sub">
						<h3 class="icon">사업유형</h3>
						<table class="chart1" summary="산림탄소센터 사업유형 정보를 나타낸표로 유형별 사업내용 및 요건 정보를 나타낸표입니다">
							<caption>산림탄소센터 사업유형</caption>
							<colgroup>
								<col width="20%" />
								<col width="80%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">사업유형</th>
									<th scope="col">사업내용 및 요건</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>신규조림<br />재 조 림</th>
									<td class="alL">
										<strong> 산림이 아닌 지역에 인위적인 식재․파종 및 천연갱신 유도를 통해 산림을 조성하는 사업</strong>
										<ul class="icon3_list">
											<li>신규조림 대상지 : 최소한 과거 50년 동안 산림이 아니었던 토지</li>
											<li>재조림 대상지 : 본래 산림이었다가 다른 용도로 전용되어 1989.12.31일 이전까지 산림이 아니었던 토지<br />* 비거래형일 경우 사업 신청 당시 산림이 아닌 토지로 완화된 요건을 적용</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>산림경영</th>
									<td class="alL">
										<strong> 산림을 지속가능한 방식으로 경영함으로써 산림의 탄소흡수량을 증대시키는 사업</strong>
										<ul class="icon3_list">
											<li>대상지 : 산림인증 획득 산림 또는 산림경영계획이 작성된 산림<br /> 비거래형일 경우 사업 신청 당시 산림경영이 가능한 지역으로 완화된 요건 적용</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>식생복구</th>
									<td class="alL">
										<strong> 0.05ha 이상의 토지에 식생조성을 통해 탄소축적을 늘리는 인위적 활동으로 도시림, 생활림, 가로수를 조성하는 사업</strong>
										<ul class="icon3_list">
											<li>사업 요건 : 산림이 아닌 토지로 신규조림‧재조림 사업 대상지 요건에 해당되지 않는 토지</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>목제품이용</th>
									<td class="alL">
										<strong> 산림경영을 통해 수확된 원목이나 이를 가공하여 생산된 목제품을 이용하는 사업</strong>
										<ul class="icon3_list">
											<li>사업 요건 : 국내 산림에서 합법적인 절차에 의해 수확된 목재  <br />* 비거래형일 경우 수입재 사용이 가능하도록 완화된 요건 적용</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>산림바이오매스<br /> 에너지 이용</th>
									<td class="alL">
										<strong> 화석연료를 산림바이오매스 에너지로 대체함으로써 온실가스 배출량을 줄이는 사업</strong>
										<ul class="icon3_list">
											<li>사업 요건 : 국내에서 생산된 목재펠릿 등의 산림바이오매스 에너지<br />비거래형일 경우 수입재 사용이 가능하도록 완화된 요건 적용</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>산지전용<br />억    제</th>
									<td class="alL">
										<strong>산지관리법 제14조의 산지전용 허가 시 부여받은 산림존치 또는 녹지 조성 면적 이상으로 산림을 존치하거나 녹지를 조성하는 사업</strong>
										<ul class="icon3_list">
											<li>사업 요건 : 산지전용 억제 사업은 비거래형만 가능하며 산지관리법 제14조에 따라 산지전용 허가를 받은 토지</li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>복합형 사업</th>
									<td class="alL">
										<strong>두 가지 이상의 개별 사업을 연계하여 추진하는 사업 </strong>
										<ul class="icon3_list">
											<li>산림경영 등을 통해 발생한 임목부산물을 이용하여 산림바이오매스 에너지로 활용하는 사업</li>
											<li> 산림경영 등을 통해 발생한 임목을 이용하여 생산한 목제품을 활용하는 사업</li>
										</ul>
									</td>
								</tr>

							</tbody>
						</table>

					</div>


					<div id="tab_sub2" class="tab_cont_sub">

						<div class="line_green txt_bg05">
							<strong class="txt_style">사회공헌형 산림탄소상쇄제도란</strong>
							<p>산림을 이용한 업을 통하여 획득한 산림탄소흡수량을 자발적 시장에서 거래하거나,<br />거래 외의 홍보등으로 사용할 수 있는 제도입니다.사업 유형은 다음과 같고, 참여유형은<br />거래형과 비거래형으로 구분됩니다.</p>
						</div>

						<h3 class="icon mB40">사업체계도</h3>


						<div class="org">
							<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_st_org.gif" alt="산림탄소센터 사업체계는 사업계획(사업계획서 작성 , 사업계획서 접수) → 타당성 검토 및  등록 (타당성 검토 및 등록)→ 사업실행 및 모니터링(사업수행 및 모니터링 보고서 작성, 모니터링 보고서 검증요청) → 검증(머니터링결과 검증보고서 작성, 검증보고서 인증) → 인증서 발급(산림탄소흡수량 거래, 인증서 발급관리) 절차로 이루어지며
							사업자(산림탄소사업자)는 사업계획서를 작성하여 산림탄소센터(녹색사업단)에 접수 하면 산림탄소센터(녹색사업단)는 타당성 검토후 등록결과를 사업자에게 통보하면  사업자는  사업수행및 모니터링 보고서를 작성하여 다시 산림탄소센터에 모니터링 보고서 검증요청을 하고 제3의 검증기관에서 모니터링 결과 검증보고서를 작성하여 인증기관에 검증보고서 인증을 받는다. 산림탄소센터는   인증결과에따른 인정서 발급및 관리를 수행하며 사업자에게 산림탄소 흡수량거래 인증서를 발급한다
							" />
						</div>
					</div>

					<div id="tab_sub3" class="tab_cont_sub">
						<h3 class="icon">사업등록현황</h3>

						<table class="chart" summary="사업등록현황 정보를 나타낸표로 등록번호, 등록일, 사업자, 참여유형, 사업유형, 총예상 산림탄소흡수량 정보를 나타낸 표입니다">
							<caption>사업등록현황 정보</caption>
							<thead>
								<tr>
									<th scope="col">등록번호</th>
									<th scope="col">등록일</th>
									<th scope="col">사업자</th>
									<th scope="col">참여유형</th>
									<th scope="col">사업유형</th>
									<th scope="col">총 예상 산림탄소흡수량</th>
								</tr>
							</thead>
							<tfoot>
								 <tr>
									<td>합  계</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td>26,961tCO2</td>
								</tr>
							</tfoot>
							<tbody>
								<tr>
									<td>1</td>
									<td>&rsquo;13.08.16</td>
									<td>강원도</td>
									<td>거래형</td>
									<td>재조림</td>
									<td>1,683tCO2/30yr</td>
								</tr>
								<tr>
									<td>2</td>
									<td>&rsquo;13.09.30</td>
									<td>㈜이브자리</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>235tCO2/35yr</td>
								</tr>
								<tr>
									<td>3</td>
									<td>&rsquo;14.01.03</td>
									<td>한국예탁결제원</td>
									<td>거래형</td>
									<td>재조림</td>
									<td>756tCO2/30yr</td>
								</tr>
								<tr>
									<td>4</td>
									<td>&rsquo;14.02.11</td>
									<td>자연환경국민신탁</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>67tCO2/10yr</td>
								</tr>
								<tr>
									<td>5</td>
									<td>&rsquo;14.03.04</td>
									<td>㈜이브자리</td>
									<td>비거래형</td>
									<td>산림경영</td>
									<td>2,812tCO2/35yr</td>
								</tr>
								<tr>
									<td>6</td>
									<td>&rsquo;14.03.28</td>
									<td>서울특별시-㈜이브자리</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>200tCO2/30yr</td>
								</tr>
								<tr>
									<td>7</td>
									<td>&rsquo;14.05.01</td>
									<td>전라북도 진안군</td>
									<td>거래형</td>
									<td>신규조림</td>
									<td>704tCO2/50yr</td>
								</tr>
								<tr>
									<td>8</td>
									<td>&rsquo;14.05.08</td>
									<td>대구광역시</td>
									<td>거래형</td>
									<td>재조림</td>
									<td>1,338tCO2/50yr</td>
								</tr>
								<tr>
									<td>9</td>
									<td>&rsquo;14.05.08</td>
									<td>예금보험공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>58ttCO2/10yr</td>
								</tr>
								<tr>
									<td>10</td>
									<td>&rsquo;14.05.21</td>
									<td>강원도 화천군</td>
									<td>비거래형</td>
									<td>목제품 이용</td>
									<td>3tCO2/4yr</td>
								</tr>
								<tr>
									<td>11</td>
									<td>&rsquo;14.06.24</td>
									<td>전라북도 진안군(주천면)</td>
									<td>거래형</td>
									<td>신규조림</td>
									<td>3,516tCO2/50yr</td>
								</tr>
								<tr>
									<td>12</td>
									<td>&rsquo;14.06.27</td>
									<td>강원도 (춘천시)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>77tCO2/10yr</td>
								</tr>
								<tr>
									<td>13</td>
									<td>&rsquo;14.06.28</td>
									<td>강원도 (철원군)</td>
									<td>비거래형</td>
									<td>신규조림</td>
									<td>424tCO2/30yr</td>
								</tr>
								<tr>
									<td>14</td>
									<td>&rsquo;14.06.28</td>
									<td>강원도 (홍천군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>5,552tCO2/30yr</td>
								</tr>
								<tr>
									<td>15</td>
									<td>&lsquo;14.07.18</td>
									<td>강원도 양구군</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>646tCO2/30yr</td>
								</tr>
								<tr>
									<td>16</td>
									<td>&lsquo;14.08.17</td>
									<td>강원도 (화천군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>3,205tCO2/30yr</td>
								</tr>
								<tr>
									<td>17</td>
									<td>&lsquo;14.08.17</td>
									<td>강원도 인제군(1)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>64tCO2/10yr</td>
								</tr>
								<tr>
									<td>18</td>
									<td>&lsquo;14.08.17</td>
									<td>강원도 인제군(2)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>38tCO2/10yr</td>
								</tr>
								<tr>
									<td>19</td>
									<td>&lsquo;14.09.11</td>
									<td>강원도 고성군</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>54tCO2/10yr</td>
								</tr>
								<tr>
									<td>20</td>
									<td>&lsquo;14.09.11</td>
									<td>강원도 양양군</td>
									<td>비거레형</td>
									<td>재조림</td>
									<td>52tCO2/10yr</td>
								</tr>
								<tr>
									<td>21</td>
									<td>&lsquo;14.09.11</td>
									<td>한국도로공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>168tCO2/30yr</td>
								</tr>
								<tr>
									<td>22</td>
									<td>&lsquo;14.09.19</td>
									<td>강원도(춘천시)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>991tCO2/30yr</td>
								</tr>
								<tr>
									<td>23</td>
									<td>&lsquo;14.09.19</td>
									<td>강원도(화천군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>995tCO2/30yr</td>
								</tr>
								<tr>
									<td>24</td>
									<td>&lsquo;14.10.08</td>
									<td>강원도(영월군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>651tCO2/30yr</td>
								</tr>
								<tr>
									<td>25</td>
									<td>&lsquo;14.11.06</td>
									<td>한국도로공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>12tCO2/10yr</td>
								</tr>
								<tr>
									<td>26</td>
									<td>&lsquo;14.11.06</td>
									<td>한국도로공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>17tCO2/10yr</td>
								</tr>
								<tr>
									<td>27</td>
									<td>&lsquo;14.11.21</td>
									<td>강원도 철원군</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>123tCO2/30yr</td>
								</tr>
								<tr>
									<td>28</td>
									<td>&lsquo;14.11.25</td>
									<td>한국도로공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>242tCO2/30yr</td>
								</tr>
								<tr>
									<td>29</td>
									<td>&lsquo;14.11.25</td>
									<td>한국도로공사</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>251tCO2/30yr</td>
								</tr>
								<tr>
									<td>30</td>
									<td>&lsquo;14.11.26</td>
									<td>강원도(홍천군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>1,170tCO2/30yr</td>
								</tr>
								<tr>
									<td>31</td>
									<td>&lsquo;14.11.26</td>
									<td>강원도(평창군)</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>355tCO2/30yr</td>
								</tr>
								<tr>
									<td>32</td>
									<td>&rsquo;14.12.18</td>
									<td>㈜신세계</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>129tCO2/10yr</td>
								</tr>
								<tr>
									<td>33</td>
									<td>&rsquo;14.12.23</td>
									<td>서울특별시-
										한국환경산업기술원</td>
									<td>비거래형</td>
									<td>재조림</td>
									<td>329tCO2/30yr</td>
								</tr>
						   </tbody>
						</table>
					</div>
				</div>
			</div>