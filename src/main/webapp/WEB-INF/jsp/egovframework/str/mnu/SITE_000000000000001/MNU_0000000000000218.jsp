<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<div class="tab_box_sub">
		<div class="tabs2 ">
			<ul class="tab2 tab_sub">
				<li class="active"><a href="#tab_sub1">시설소개</a></li>
				<li><a href="#tab_sub2">프로그램 이용안내</a></li>
			</ul>
		</div>

		<div class="tab_content_sub">
			<div id="tab_sub1" class="tab_cont_sub">
				<h3 class="icon">칠곡나눔숲체원은?</h3>
				<p class="line">
					녹색사업단에서 운영하는 산림교육전문 휴양시설로써 2015년 2월에 개원 할 예정입니다. <br />
					산림청, 교육부, 환경부, 여성가족부, 녹색성장위원회 등 정부로부터 인증 받은 200여개의 프로그램을 운영하고 있으며, 산림에 대한 가치 제고와 산림을 통한 사회통합 등 다양한 기능을 수행하기 위하여 설립되었습니다.
				</p>
				<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_facility01.gif" alt="A 행정동, B 식당동, C 강당동, D숙박동(10인실), E숙박동(16인실), 01 매표 안내소, 02 주차장, 03 연리지쉼터, 04 외불포토존, 05 수번데크로드, 06 중앙광장, 07 숲속교실, 08 주차장, 09산책로, 10 야영장, 11취사장 화장실, 12운동장, 13 숲체험원, 14 숲속의집, 15등산로 " />

			</div>


			<div id="tab_sub2" class="tab_cont_sub">
				<h3 class="icon">칠곡나눔숲체원은?</h3>
				<p class="line">
					녹색사업단에서 운영하는 산림교육전문 휴양시설로써 2015년 2월에 개원 할 예정입니다. <br />
					산림청, 교육부, 환경부, 여성가족부, 녹색성장위원회 등 정부로부터 인증 받은 200여개의 프로그램을 운영하고 있으며, 산림에 대한 가치 제고와 산림을 통한 사회통합 등 다양한 기능을 수행하기 위하여 설립되었습니다.
				</p>

				<h3 class="icon1">프로그램 이용안내</h3>
				<ul class="section1 icon3_list mB40">
					<li>프로그램 1일 최대 교육인원은 120명으로 선착순으로 예약접수 받습니다. </li>
					<li>프로그램에 이용은 칠곡나눔숲체원(054-977-8772)으로 문의해 주시기 바랍니다</li>
				</ul>

				<ul class="prg_link">
					<li>
						<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo01.jpg" alt="숲놀이" />
						<span class="cont">
							<strong>숲놀이</strong>
							<span>영유아를 대상으로 체험과 놀이를 통해 숲에 대한 거부감을 없애고, 일 년 열두달 변화하는 숲에서 놀고, 스스로 꺠닫는 교육을 지향하는 프로그램입니다.</span>
							<a href="http://www.soop.or.kr/page.do?div=sub_edu&url=sub3" target="_blank">자세히 보기</a>
						</span>
					</li>
					<li>
						<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo02.jpg" alt="숲체험" />
						<span class="cont">
							<strong>숲체험</strong>
							<span>모든 감각기능을 통해 숲을 체험함으로써 술을 익히고, 창의력과 감성을 키워주는 유ㆍ소년 및 가족참가자들이 참여할 수 있는 다양한 숲 체험 프로그램입니다.</span>
							<a href="http://www.soop.or.kr/page.do?div=sub_edu&url=sub4" target="_blank">자세히 보기</a>
						</span>
					</li>
					<li>
						<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo03.jpg" alt="휴양ㆍ레저" />
						<span class="cont">
							<strong>휴양ㆍ레저</strong>
							<span>숲에서 다양한 활동을 통해 숲이 주는 여유로움과 교감으로 숲의 일부분이 됨으로써, 현대인들의 스트레스 감소, 심리적 안정 등을 위한 프로그램입니다.</span>
							<a href="http://www.soop.or.kr/page.do?div=sub_edu&url=sub5" target="_blank">자세히 보기</a>
						</span>
					</li>
					<li>
						<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo04.jpg" alt="특별" />
						<span class="cont">
							<strong>특별</strong>
							<span>유형 및 특성을 고려한 프로그램 기획하여 숲체험 교육 기회 <br />제공 및 다양한 문화와 사회통합을 위한 프로그램입니다. </span>
							<a href="http://www.soop.or.kr/page.do?div=sub_edu&url=sub2" target="_blank">자세히 보기</a>
						</span>
					</li>
				</ul>




			</div>

		</div>
	</div>