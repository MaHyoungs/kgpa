<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


			<div class="tab_box_sub">
				<div class="tabs2 ">
					<ul class="tab2 tab_sub">
						<li class="active"><a href="#tab_sub3">시설소개</a></li>
						<li><a href="#tab_sub4">프로그램 이용안내</a></li>
					</ul>
				</div>

				<div class="tab_content_sub">
					<div id="tab_sub3" class="tab_cont_sub">
						<h3 class="icon">백두대간숲생태원은?</h3>
						<p class="line">
							녹색사업단이 2010년 12월부터 상주시에 위탁받아 운영하고 있는 숲체험 교육시설로써, 백두대간의 역사‧문화‧생태 교류의 장을 제공하고 유‧소년 및 일반인의 숲체험, 생태교육을 수행하고 있습니다. 산림청, 교육부, 환경부, 여성가족부, 녹색성장위원회 등 정부로부터 인증 받은 200여개의 프로그램을 운영하고 있습니다.
						</p>

						<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_facility02.gif" alt="01전시실, 02 세미나실, 3-1 야외체험장(동편), 3-2야외체험장(서편),04 숨폭의 집, 05 다목적 숙소 " />

					</div>


					<div id="tab_sub4" class="tab_cont_sub">
						<h3 class="icon">백두대간숲생태원은?</h3>
						<p class="line">
							녹색사업단이 2010년 12월부터 상주시에 위탁받아 운영하고 있는 숲체험 교육시설로써, 백두대간의 역사‧문화‧생태 교류의 장을 제공하고 유‧소년 및 일반인의 숲체험, 생태교육을 수행하고 있습니다. 산림청, 교육부, 환경부, 여성가족부, 녹색성장위원회 등 정부로부터 인증 받은 200여개의 프로그램을 운영하고 있습니다.
						</p>

						<h3 class="icon1">프로그램 이용안내</h3>
						<ul class="section1 icon3_list mB40">
							<li>프로그램 이용은 백두대간숲생태원(054-536-0914)으로 문의해 주시기 바랍니다. </li>
						</ul>




						<ul class="prg_link">
							<li>
								<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo05.jpg" alt="숲체험 프로그램" />
								<span class="cont">
									<strong>숲체험 프로그램</strong>
									<span>유ㆍ소년 및 가족 참가자들이 참여할 수 있는 다양한 숲 체험프로그램들입니다.</span>
									<a href="https://www.foresteco.or.kr/cnt/prod/prod030101.html" target="_blank">자세히 보기</a>
								</span>
							</li>
							<li>
								<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo06.jpg" alt="생태교육프로그램" />
								<span class="cont">
									<strong>생태교육프로그램</strong>
									<span>생태에 관한 전반적인 지식을 습득하고, 현장에서의 실질적인 실습을 거처 능력 있는 전문인을 양성합니다.</span>
									<a href="https://www.foresteco.or.kr/cnt/prod/prod030201.html" target="_blank">자세히 보기</a>
								</span>
							</li>
							<li>
								<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo07.jpg" alt="특별프로그램" />
								<span class="cont">
									<strong>특별프로그램</strong>
									<span>백두대간 숲에 대한 기초지식을 이해하고 동ㆍ식물의 구조와 생태에 관한 이론을 실습하고 체험함으로써 학문적으로 접근 하는 방법을 배우는 방학기간 동안 진행되는 특별프로그램 입니다.</span>
									<a href="https://www.foresteco.or.kr/cnt/prod/prod030301.html" target="_blank">자세히 보기</a>
								</span>
							</li>
							<li>
								<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images//page/m5/img_prgo08.jpg" alt="단위프로그램" />
								<span class="cont">
									<strong>단위프로그램</strong>
									<span>숲 체험,생태교육, 대체 및 특별프로그램을 구성하는 70여개의 단위프로그램입니다.</span>
									<a href="https://www.foresteco.or.kr//cnt/prod/prod030401.html" target="_blank">자세히 보기</a>
								</span>
							</li>
						</ul>



					</div>

				</div>
			</div>
