<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%
	/*
		사이트맵 			: /msi/siteMap.do
		개인정보보호정책 	: /msi/indvdlInfoPolicy.do
		이용약관			: /msi/useStplat.do
		이메일수집거부		: /msi/emailColctPolicy.do
	*/
%>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${_WEB_FULL_PATH}/template/${siteInfo.lytTmplatId}/${TEMPLATE_PATH}/images"/>
<c:set var="_CSS" value="${_WEB_FULL_PATH}/template/${siteInfo.lytTmplatId}/${TEMPLATE_PATH}/css"/>
<c:set var="_JS" value="${_WEB_FULL_PATH}/template/${siteInfo.lytTmplatId}/${TEMPLATE_PATH}/js"/>
<c:set var="C_JS" value="${_WEB_FULL_PATH}/template/common/js"/>
<c:import url="/msi/tmplatHead.do" charEncoding="utf-8"/>

index

<script type="text/javascript">
	<c:forEach items="${popupList}" var="resultInfo" varStatus="status">
		if(fnGetCookiePopup('${resultInfo.popupId}') == null ){
		 	fn_egov_popupOpen_PopupManage('${resultInfo.popupId}',
		 			'${resultInfo.fileUrl}',
		 			'${resultInfo.popupWsize}',
	    	    	'${resultInfo.popupHsize}',
	    	    	'${resultInfo.popupHlc}',
	    	    	'${resultInfo.popupWlc}',
	    	    	'${resultInfo.stopVewAt}');
		}
	</c:forEach>
</script>


<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>