<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%
	/*
		사이트맵 			: /msi/siteMap.do
		개인정보보호정책 	: /msi/indvdlInfoPolicy.do
		이용약관			: /msi/useStplat.do
		이메일수집거부		: /msi/emailColctPolicy.do
	*/
%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images"/>

		
		<c:if test="${param.isMain ne 'Y'}">
				</div>
				</div>
				<!-- container end -->
		</c:if>
				
				<!-- footer start -->
				<div id="footer">	
				
					<div class="footer_cont">
						<div class="footer_gnb">
							<a href="${_WEB_FULL_PATH}/msi/indvdlInfoPolicy.do"><spring:message code="etc.link.indvdlInfoPolicy" /></a><span>|</span>
							<a href="${_WEB_FULL_PATH}/msi/useStplat.do"><spring:message code="etc.link.useStplat" /></a><span>|</span>
							<a href="${_WEB_FULL_PATH}/msi/emailColctPolicy.do"><spring:message code="etc.link.emailColctPolicy" /></a><span>|</span>
							<a href="#"><spring:message code="etc.link.viewer" /></a>
						</div>
						<h2 class="ft_logo"><img src="${SiteFileStoreWebPath}${siteInfo.siteId}/${siteInfo.lptLogoFileNm}" alt="<c:out value="${siteInfo.siteNm}"/>" /></h2>
						<p class="address">${siteInfo.adresReplcText}
						</p>
						<div class="other">
							<c:import url="/msi/ctn/linkSiteService.do" charEncoding="utf-8">
								<c:param name="tmplatCours" value="${_IMG}"/>
							</c:import>	
						</div>
					
					</div>
					
				</div>
				<!-- footer end -->
			</div>
		</body>
		</html>