<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<%
	/*
		사이트맵 			: /msi/siteMap.do
		개인정보보호정책 	: /msi/indvdlInfoPolicy.do
		이용약관			: /msi/useStplat.do
		이메일수집거부		: /msi/emailColctPolicy.do
		저작권신고서비스	: /msi/cpyrhtSttemntSvc.do
			
		메뉴 불러오기 : menuTarget => Main, Sub, menuType => FullDisplay, Simple2Depth
		<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
			<c:param name="menuTarget" value="Main"/>
			<c:param name="menuType" value="FullDisplay"/>
			<c:param name="cssClass" value="cont"/>
		</c:import>
		
		팝업창 불러오기
		<c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
	
		게시판 불러오기 : linkMenuId => 메뉴아이디, tableId => 게시판아이디
		<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
			<c:param name="tmplatCours" value="${_IMG}"/>
			<c:param name="linkMenuId" value="${result.menuId}" />
			<c:param name="viewType" value="data" />
			<c:param name="tableId" value="${result.progrmId}" />
			<c:param name="listTag" value="li" />
			<c:param name="itemCount" value="4" />
						
			<c:param name="colname" value="nttSj" />
			<c:param name="length" value="15" />
			<c:param name="tag" value="a" />
			<c:param name="cssClass" value="cont" />
			
			<c:param name="colname" value="frstRegisterPnttm" />
			<c:param name="length" value="-1" />
			<c:param name="tag" value="span" />
			<c:param name="cssClass" value="date" />
		</c:import>
		
		배너불러오기 : tableId => PopupZone, BannerZone, QuickZone, MainBannerType1, MainBannerType2, SubBanner
		<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
			<c:param name="tmplatCours" value="${_IMG}"/>
			<c:param name="tableId" value="BannerZone" />
			<c:param name="listTag" value="li" />
			
			<c:param name="colname" value="bannerImage" />
			<c:param name="length" value="-1" />
			<c:param name="tag" value="a" />
			<c:param name="cssClass" value="" />
			
			<c:param name="colname" value="bannerNm" />
			<c:param name="length" value="-1" />
			<c:param name="tag" value="a" />
			<c:param name="cssClass" value="" />
		</c:import>
		
		관련사이트 불러오기
		<c:import url="/msi/ctn/linkSiteService.do" charEncoding="utf-8">
			<c:param name="tmplatCours" value="${_IMG}"/>
		</c:import>	
	*/
%>
<c:set var="PUBLISH_APPEND_FREFIX"><%=Globals.getPublishAppendPrefix(request)%></c:set>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images"/>
<c:set var="_CSS" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}"/>
<c:set var="_JS" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}"/>
<c:set var="C_JS" value="${_WEB_FULL_PATH}/template/common/js"/>
<c:set var="CURR_URL" value="<%=helper.getOriginatingRequestUri(request) %>"/>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<script type="text/javascript" src="${C_JS}/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${C_JS}/common.js"></script>
	<script type="text/javascript" src="${C_JS}/over_tap.js"></script>

	<link charset="utf-8" href="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/style.css" type="text/css" rel="stylesheet"/>
	<script charset="utf-8" src="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/script.js" type="text/javascript"></script>
	<c:if test="${currMpm.htmlUseAt eq 'Y'}">
		<link charset="utf-8" href="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/style.css" type="text/css" rel="stylesheet"/>
		<script charset="utf-8" src="${MnuFileStoreWebPathByWebFile}${currMpm.siteId}/${currMpm.menuId}/script.js" type="text/javascript"></script>
	</c:if>	
	<c:if test="${not empty param.BBS_TMPLATID}">
	<link charset="utf-8" href="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/style.css" type="text/css" rel="stylesheet"/>
	<script type="text/javascript" src="${BbsFileStoreWebPathByWebFile}${param.BBS_TMPLATID}/script.js"></script>
	</c:if>
	<c:if test="${siteInfo.mouseScrtyApplcAt eq 'Y' }"><script type="text/javascript">$(document).ready(function(){$(document).bind("contextmenu",function(e){return false;});});</script></c:if>
	<c:if test="${siteInfo.kybrdScrtyApplcAt eq 'Y' }">
	<script type="text/javascript">$(document).ready(function(){$(document).bind('selectstart',function() {return false;});$(document).bind('dragstart',function(){return false;});$(document).keydown(function (e) {if(e.ctrlKey && e.which == 65) return false;if(e.ctrlKey && e.which == 67) return false;});});	</script>
	</c:if>
	<title>
		<c:out value="${siteInfo.siteNm}"/>
		<c:choose>
			<c:when test="${CURR_URL eq '/msi/indvdlInfoPolicy.do'}">&gt; <spring:message code="etc.link.indvdlInfoPolicy" /></c:when>
			<c:when test="${CURR_URL eq '/msi/useStplat.do'}">&gt; <spring:message code="etc.link.useStplat" /></c:when>
			<c:when test="${CURR_URL eq '/msi/emailColctPolicy.do'}">&gt; <spring:message code="etc.link.emailColctPolicy" /></c:when>							
			<c:when test="${CURR_URL eq '/msi/siteMap.do'}">&gt; <spring:message code="etc.link.sitemap" /></c:when>
			<c:otherwise>				
				<c:if test="${not empty currRootMpm.menuNm}">&gt;<c:out value="${currMpm.menuPathByName}"/></c:if>
			</c:otherwise>
		</c:choose>
	</title>	
</head>

<body>
	<div id="wrap">
		<!-- header start -->
		<div id="header">
			<div id="lng_line"></div>
			<div class="header">
				<div class="logo_top">
					<h1 class="logo"><a href="/"><img src="${SiteFileStoreWebPath}${siteInfo.siteId}/${siteInfo.upendLogoFileNm}" alt="<c:out value="${siteInfo.siteNm}"/>" /></a></h1>
					<div class="top_gnb">
					<a href="/">HOME</a><span>|</span>
					<c:choose>
						<c:when test="${empty USER_INFO.id}">
							<a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>">로그인</a><span>|</span>
							<a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>">회원가입</a><span>|</span>
						</c:when>
						<c:otherwise>
							<a href="<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>">로그아웃</a><span>|</span>
							<a href="<%=EgovUserDetailsHelper.getRedirectUserUpdateUrl()%>">정보수정</a><span>|</span>
						</c:otherwise>
					</c:choose>
					<a href="/msi/siteMap.do"><spring:message code="etc.link.sitemap" /></a>
					</div>
				</div>
				<ul class="main_gnb">
					<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
						<c:param name="menuTarget" value="Main"/>
						<c:param name="menuType" value="FullDisplay"/>
					</c:import>
				</ul>
			</div>
		</div>
		<!-- header end -->
		
	<c:choose>
		<c:when test="${param.isMain eq 'Y'}">
			<!-- main_slide_images start -->
			<div id="slide_images">
				<div class="image1"> </div>
				<div class="image2"> </div>
				<div class="image3"> </div>
			</div>
			<!--main_ slide_images end -->
			
			<!-- container start -->
			<div id="container">
			
				<div class="cont_collection">
					<div class="cont1">
						<div class="board">
							<div id="bbs_box" class="bbs_box">
								<h2 class="hdn">주요게시판</h2>
								<ul id="bbs">
									<c:forEach var="result" items="${siteInfo.mainContentsList}" varStatus="status">
									<li class="bbsCnt">
										<h3 id="hbbs${status.count}" class="bbsTit"><a href="#bbs${status.count}"><c:out value="${result.progrmNm}"/></a></h3>
										<div id="bbs${status.count}" class="bbs_list">
											<ul>
												<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
													<c:param name="tmplatCours" value="${_IMG}"/>
													<c:param name="linkMenuId" value="${result.menuId}" />
													<c:param name="viewType" value="data" />
													<c:param name="tableId" value="${result.progrmId}" />
													<c:param name="listTag" value="li" />
													<c:param name="itemCount" value="4" />
																
													<c:param name="colname" value="nttSj" />
													<c:param name="length" value="15" />
													<c:param name="tag" value="a" />
													<c:param name="cssClass" value="cont" />
													
													<c:param name="colname" value="frstRegisterPnttm" />
													<c:param name="length" value="-1" />
													<c:param name="tag" value="span" />
													<c:param name="cssClass" value="date" />
												</c:import>
											</ul>
										</div>
										<a href="<c:url value="/cop/bbs/selectBoardList.do?bbsId=${result.progrmId}&menuId=${result.menuId}"/>" id="bbsmore" class="tab_more"><img src="${_IMG}/main/more.gif" alt="<c:out value="${result.progrmNm}"/> 더보기" /></a>
									</li>
									</c:forEach>
								</ul>								
								<script type="text/javascript">
								
								$(document).ready(function(){									
									$("#bbs").tabToggle({
										tabClass : ".bbsCnt",
										titClass : ".bbsTit",
										bbsList : ".bbs_list",
										moreBtn : ".tab_more",
										height : 100,										
										top : 30, left : 0
									});
								});
								</script>
							</div>	
						</div>
						<div class="enter_qna"><a href="http://www.uway.com"><img src="${_IMG}/main/enter_qna.gif" alt="세종국제고등학교입학문의"/></a></div>
					</div>
	
					<div class="cont2">
						<div class="search">
							<h2 class="smart_search"><img src="${_IMG}/main/smart_search.gif" alt="SMART SEARCH" /></h2>
							<form action="#" name="">
								<input class="search_text" id="search_text" name="search_text" type="text" value="검색할 단어를 입력하세요." onfocus="this.value='';" onblur="if(this.value.length==0) {this.value='검색할 단어를 입력하세요.'}" title="검색어입력" />
								<input class="search_btn" id="search_btn" name="search_btn" type="image" alt="검색" src="${_IMG}/main/search_btn.png"/>
							</form>
						</div>
						<div class="movie">
							<a href="http://sji.sje.go.kr:8080/msi/cntntsService.do?menuId=MNU_0000000000000242"><img src="${_IMG}/main/movie.jpg" alt="세종국제고등학교홍보영상 : 세종국제고등학교 홈페이지 방문을 진심으로 환영합니다.저희 세종국제고등학교는 2012년 개교이래..자세히보기" /></a>
						</div>
					</div>
	
					<div class="popup">
						<h3 class="popupzone"><img src="${_IMG}/main/popupzone.gif" alt="POPUPZONE"/></h3>
						<div class="popup_btn">
							<input class="play_btn" type="image" onclick="rollingOn();return false;" src="${_IMG}/main/play.gif" alt="재생버튼" />
							<input class="stop_btn" type="image"  onclick="rollingOff();return false;"  src="${_IMG}/main/stop.gif" alt="정지버튼" />
						</div>
						<ul>
							<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
								<c:param name="tmplatCours" value="${_IMG}"/>
								<c:param name="tableId" value="PopupZone" />
								<c:param name="listTag" value="li" />
							</c:import>
						</ul>
					</div>
					<script type="text/javascript">
						//<![CDATA[
							var rId;
						    if(document.getElementById("pop1") != null) {
								rId = setInterval(rollingPanel, 5000);
								document.getElementById("pop1").className="on";
						    }						
							 
							function rollingOn() {
								rollingOff();
								rId = setInterval(rollingPanel, 5000);
							} 
							function rollingOff() {
								clearInterval(rId);
							}
						//]]>	
					</script>	
	
				</div>
				<div class="contents">
					<!-- student contents start -->
					 <div id="student">
					 	<div class="people">재학생</div>
					 	<div class="message">세종국제고등학교 방문을 환영합니다<br />재학생이 자주 찾는 메뉴입니다 <br />감사합니다</div>
							<div class="who_img"><img src="${_IMG}/main/student.png" alt=""/></div>
					 	<div class="who_img"></div>
					 	<ul class="people_cont">
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000849&menuId=MNU_0000000000000286"><img src="${_IMG}/main/notice_btn.png" alt="학교공지" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000861&menuId=MNU_0000000000000276"><img src="${_IMG}/main/plan_btn.png" alt="학사일정" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000887&menuId=MNU_0000000000000352"><img src="${_IMG}/main/life_btn.png" alt="생활관" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000846&menuId=MNU_0000000000000244"><img src="${_IMG}/main/international.png" alt="국제인" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000876&menuId=MNU_0000000000000324"><img src="${_IMG}/main/subject_btn.png" alt="교과자료실" /></a></li>
					 		<!--<li><a href="#"><img src="${_IMG}/main/circle_btn.png" alt="동아리활동" /></a></li>
					 		<li><a href="#"><img src="${_IMG}/main/homepage_btn.png" alt="학급홈페이지" /></a></li>-->
					 	</ul>
					 	<div class="arrow_stu"><img src="${_IMG}/main/arrow_top.png" alt="" /></div>
					 </div>
	
					 <!-- teacher contents start -->
					 <div id="teacher">
					 	<div class="people">교직원</div>
					 	<div class="message">세종국제고등학교 방문을 환영합니다<br />재학생이 자주 찾는 메뉴입니다 <br />감사합니다</div>
						<div class="who_img"><img src="${_IMG}/main/teacher.png" alt=""/></div>
					 	<div class="who_img"></div>
					 	<ul class="people_cont">
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000395"><img src="${_IMG}/main/icon/icon1.png" alt="신입생마당" /></a></li>
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000281"><img src="${_IMG}/main/icon/icon2.png" alt="이달의식단" /></a></li>
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000373"><img src="${_IMG}/main/icon/icon3.png" alt="행정실" /></a></li>
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000395"><img src="${_IMG}/main/icon/icon4.png" alt="신입생마당" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000844&menuId=MNU_0000000000000242"><img src="${_IMG}/main/icon/icon5.png" alt="홍보동영상" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000860&menuId=MNU_0000000000000273"><img src="${_IMG}/main/icon/icon6.png" alt="가정통신문" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000862&menuId=MNU_0000000000000277"><img src="${_IMG}/main/icon/icon7.png" alt="이달의행사" /></a></li>
					 	</ul>
					 	<div class="arrow_tea"><img src="${_IMG}/main/arrow_top.png" alt="" /></div>
					 </div>
	
					 <!-- normal contents start -->
					 <div id="normal">
					 	<div class="people">일반인</div>
					 	<div class="message">세종국제고등학교 방문을 환영합니다<br />재학생이 자주 찾는 메뉴입니다 <br />감사합니다</div>
						<div class="who_img"><img src="${_IMG}/main/normal.png" alt=""/></div> 
					 	<div class="who_img"></div>
					 	<ul class="people_cont">
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000370"><img src="${_IMG}/main/icon/icon8.png" alt="자유게시판" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000876&menuId=MNU_0000000000000324"><img src="${_IMG}/main/icon/icon9.png" alt="학급홈피" /></a></li>
					 		<!--<li><a href="#"><img src="${_IMG}/main/icon/icon10.png" alt="전산실" /></a></li>-->
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000846&menuId=MNU_0000000000000244"><img src="${_IMG}/main/icon/icon11.png" alt="국제인" /></a></li>
					 		<li><a href="/msi/cntntsService.do?menuId=MNU_0000000000000334"><img src="${_IMG}/main/icon/icon12.png" alt="진로정보" /></a></li>
					 		<!--<li><a href="#"><img src="${_IMG}/main/icon/icon13.png" alt="동아리활동" /></a></li>
					 		<li><a href="#"><img src="${_IMG}/main/icon/icon14.png" alt="경시대회" /></a></li>-->
					 	</ul>
					 	<div class="arrow_nor"><img src="${_IMG}/main/arrow_top.png" alt="" /></div>
					 </div>
					 <!-- parents contents start -->
					 <div id="parents">
					 	<div class="people">학부모</div>
					 	<div class="message">세종국제고등학교 방문을 환영합니다<br />재학생이 자주 찾는 메뉴입니다 <br />감사합니다</div>
						<div class="who_img"><img src="${_IMG}/main/parents.png" alt=""/></div>
					 	<div class="who_img"></div>
					 	<ul class="people_cont">
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000851&menuId=MNU_0000000000000253"><img src="${_IMG}/main/icon/icon15.png" alt="신입생마당" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000852&menuId=MNU_0000000000000257"><img src="${_IMG}/main/icon/icon16.png" alt="입시자료" /></a></li>
					 		<li><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000853&menuId=MNU_0000000000000258"><img src="${_IMG}/main/icon/icon17.png" alt="일반자료" /></a></li>
					 		<!--<li><a href="#"><img src="${_IMG}/main/icon/icon18.png" alt="기출문제" /></a></li>-->
					 		<!--<li><a href="#"><img src="${_IMG}/main/icon/icon19.png" alt="수학이야기"/></a></li>
					 		<li><a href="#"><img src="${_IMG}/main/icon/icon20.png" alt="평가자료"/></a></li>
					 		<li><a href="#"><img src="${_IMG}/main/icon/icon21.png" alt="미술교육"/></a></li>-->
					 	</ul>
					 	<div class="arrow_par"><img src="${_IMG}/main/arrow_top.png" alt="" /></div>
					 </div>
					 
					 <!-- tap start -->
					 <div class="tap_people">
						 <ul>
						 	<li><a href="#" id="stu" class="selected">재학생</a></li>
						 	<li><a href="#" id="tea" class="">교직원</a></li>
					 		<li><a href="#" id="nor" class="">일반인</a></li>
					 		<li><a href="#" id="par" class="">학부모</a></li>
					 	</ul>
				 	</div>
				</div>
				<div class="banner">
					<div class="control">
						<h4 class="bannerzone"><img src="${_IMG}/main/bannerzone.png" alt="Banner Zone"/></h4>
						<div class="banner_btn">
							<input id="bn_start" type="image" src="${_IMG}/main/play.gif" alt="재생버튼"/>
							<input id="bn_stop" type="image" src="${_IMG}/main/stop.gif" alt="정지버튼"/>
						</div>
					</div>
					<div class="slide_banner">
						<ul>
							<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
								<c:param name="tmplatCours" value="${_IMG}"/>
								<c:param name="tableId" value="BannerZone" />
								<c:param name="listTag" value="li" />
								
								<c:param name="colname" value="bannerImage" />
								<c:param name="length" value="-1" />
								<c:param name="tag" value="a" />
								<c:param name="cssClass" value="" />
							</c:import>
						</ul>
					</div>
					<script type="text/javascript">
					$(document).ready(function(){
						$(".slide_banner").jCarouselLite({
							visible: 6,
							speed: 2000,
							auto : 800,
							playBtn : "#bn_start",
							stopBtn : "#bn_stop"
						});
					});
					</script>
				</div>
				<!-- quick menu -->
				<div class="quick_wrap">
					<div class="quick">		
						<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
							<c:param name="tmplatCours" value="${_IMG}"/>
							<c:param name="tableId" value="QuickZone" />
							<c:param name="listTag" value="li" />
							
							<c:param name="colname" value="bannerImage" />
							<c:param name="length" value="-1" />
							<c:param name="tag" value="a" />
							<c:param name="cssClass" value="" />
						</c:import>	
					</div>
				</div>
				<!-- quick menu -->
			</div>
			<!-- container end -->	
			
			<!-- 팝업창 -->
			<c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
		</c:when>
		<c:otherwise>
			<!-- sub_main start -->
			<div id="sub_main">
				<div class="sub_main_img">
					
				</div>
			</div>
			<!-- sub_main end -->
	
			<!-- container start -->
			<div id="sub_container">
				<div id="sub_menu">
					<div class="sub_logo">
						<h2 class="school">
							<c:choose>
							<c:when test="${CURR_URL eq '/msi/indvdlInfoPolicy.do'}"><em class="etcLink1"><spring:message code="etc.link.indvdlInfoPolicy" /></em></c:when>
							<c:when test="${CURR_URL eq '/msi/useStplat.do'}"><em class="etcLink2"><spring:message code="etc.link.useStplat" /></em></c:when>
							<c:when test="${CURR_URL eq '/msi/emailColctPolicy.do'}"><em class="etcLink3"><spring:message code="etc.link.emailColctPolicy" /></em></c:when>	
							<c:when test="${CURR_URL eq '/msi/siteMap.do'}"><em class="etcLink4"><spring:message code="etc.link.sitemap" /></em></c:when>						
							<c:otherwise><c:out value="${currRootMpm.menuNm}"/></c:otherwise>
							</c:choose>							
						</h2>
						<img src="${_IMG}/sub/about_us.gif" alt="About Us" />
					</div>
					<c:if test="${not (CURR_URL eq '/msi/indvdlInfoPolicy.do' or CURR_URL eq '/msi/useStplat.do' or CURR_URL eq '/msi/emailColctPolicy.do' or CURR_URL eq '/msi/siteMap.do')}">
					<ul class="sub_gnb">
						<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
							<c:param name="menuTarget" value="Sub"/>
						</c:import>
					</ul>
					</c:if>
					<ul class="img_cont">
						<li class="movie"><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000844&menuId=MNU_0000000000000242"><img src="${_IMG}/sub/movie.png" alt="미리가보는학교학교홍보영상" /></a></li>
						<li class="data"><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000878&menuId=MNU_0000000000000328"><img src="${_IMG}/sub/datacolection.png" alt="입시정보가가득한대입자료실" /></a></li>
					</ul>
				</div>
				<div id="sub_top">
					<h2 class="teacher">
						<c:choose>
							<c:when test="${CURR_URL eq '/msi/indvdlInfoPolicy.do'}"><spring:message code="etc.link.indvdlInfoPolicy" /></c:when>
							<c:when test="${CURR_URL eq '/msi/useStplat.do'}"><spring:message code="etc.link.useStplat" /></c:when>
							<c:when test="${CURR_URL eq '/msi/emailColctPolicy.do'}"><spring:message code="etc.link.emailColctPolicy" /></c:when>	
							<c:when test="${CURR_URL eq '/msi/siteMap.do'}"><spring:message code="etc.link.sitemap" /></c:when>							
							<c:otherwise><c:out value="${currMpm.menuNm}"/></c:otherwise>
						</c:choose>
					</h2>
					<span class="course">
						<img src="${_IMG}/sub/H.gif" alt="H" />
						<c:choose>
							<c:when test="${CURR_URL eq '/msi/indvdlInfoPolicy.do'}"><spring:message code="etc.link.indvdlInfoPolicy" /></c:when>
							<c:when test="${CURR_URL eq '/msi/useStplat.do'}"><spring:message code="etc.link.useStplat" /></c:when>
							<c:when test="${CURR_URL eq '/msi/emailColctPolicy.do'}"><spring:message code="etc.link.emailColctPolicy" /></c:when>	
							<c:when test="${CURR_URL eq '/msi/siteMap.do'}"><spring:message code="etc.link.sitemap" /></c:when>							
							<c:otherwise><c:out value="${currMpm.menuPathByName}"/></c:otherwise>
						</c:choose>						
					</span>
				</div>
				
				<div id="sub_contnts">
					
					<c:if test="${currMpm.htmlUseAt eq 'Y'}">
						<c:catch var="ex">
							<c:import url="/EgovPageLink.do?link=${MnuFileStoreWebPathByJspFile}${currMpm.siteId}/${currMpm.menuId}${PUBLISH_APPEND_FREFIX}" charEncoding="utf-8"/>
						</c:catch>
						<c:if test="${ex != null}">publishing not found</c:if>
					</c:if>
					<c:if test="${currMpm.cntntsTyCode eq 'CTS05'}">
						<iframe id="cntnsIframe" src="${currMpm.url}" scrolling="no" frameborder="0" width="100%" height="1200"></iframe>
					</c:if>
					<c:if test="${currMpm.cntntsTyCode eq 'CTS06'}">
						<c:catch var="ex">
							<c:import url="${currMpm.url}" charEncoding="utf-8"/>
						</c:catch>
						<c:if test="${ex != null}">포틀릿을 가져오는데 문제가 발생하였습니다.</c:if>
					</c:if>
		</c:otherwise>
	</c:choose>