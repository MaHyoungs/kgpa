<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%-- head Import --%>
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:choose>
			<c:when test="${not empty brdMstrVO.tmplatId}">
				<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
					<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
				</c:import>
			</c:when>
			<c:otherwise>
				<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
					<c:param name="isMain" value="${isMain}"/>
				</c:import>
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="Content-Script-Type" content="text/javascript" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<link charset="utf-8" href="${_C_CSS}/default.css" type="text/css" rel="stylesheet"/>
			<script type="text/javascript" src="${_C_JS}/jquery/jquery-1.8.2.min.js"></script>
			<script type="text/javascript" src="${_C_JS}/common.js"></script>
		</head>
		<body>
	</c:otherwise>
</c:choose>
<%-- head Import --%>

<%-- Sub Import --%>
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatSub.do" charEncoding="utf-8" />
	</c:when>
	<c:otherwise>
		서브 페이지 설정을 하세요.
	</c:otherwise>
</c:choose>
<%-- Sub Import --%>

<%-- Bottom Import --%>
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}"/>
		</c:import>
	</c:when>
	<c:otherwise>
		</body>
		</html>
	</c:otherwise>
</c:choose>
<%-- Bottom Import --%>