//페이지 OnLoad 자동이벤트
$(function(){
	//행정구역 시.도, 시.군.구 OPNE API 이벤트
	if($('#area').val() != ""){
		$('#sido option:eq(0)').text($('#area').val().split(' ')[0]);
		$('#sidogungu option:eq(0)').text($('#area').val().split(' ')[1]);
		fnSidoOpenAPIAdd($('#sido'), 1);
	}else{
		fnSidoOpenAPIAdd($('#sido'), 1);
	}

	$('#sido').change(function(){
		$('#sidogungu option').remove();
		var opt = $('<option value="">시.군.구</option>');
		$('#sidogungu').append(opt);
		fnSidoOpenAPIAdd($('#sidogungu'), $('#sido').val());
		//제안요약성(기본정보) 자동등록
		$('#area_txt').text($("#sido option:selected").text());
		$('#area').val($("#sido option:selected").text());
	});
	$('#sidogungu').change(function(){
		//제안요약성(기본정보) 자동등록
		$('#area_txt').text($('#area_txt').text() + " " + $("#sidogungu option:selected").text());
		$('#area').val($('#area').val() + " " + $("#sidogungu option:selected").text());
	});

	//제안사업명 입력시 -> 사업명 자동 입력
	$('#biz_nm').change(function(){
		$('#biz_nm_txt').text($('#biz_nm').val());
	});
	//시설명 입력시 -> 기관명 자동 입력
	$('#instt_nm').change(function(){
		$('#instt_nm_txt').text($('#instt_nm').val() + "("+$('input:radio[name=fond_ty_cc]:checked').next().text()+")");
	});
	//설립유형 입력시 -> 기관명 자동 입력
	$('input:radio[name=fond_ty_cc]').change(function(){
		$('#instt_nm_txt').text($('#instt_nm').val() + "("+$('input:radio[name=fond_ty_cc]:checked').next().text()+")");
	});

	//재정자립도 순위 입력시 -> 재정자립도위 자동 입력
	$('#fnanc_idpdc_rank').change(function(){
		$('#fnanc_idpdc_rank_txt').text($('#fnanc_idpdc_rank').val());
	});

	//복지 시설 나눔숲 (특수교육시설) 조성면적 입력시 -> 토지소유자 및 조성면적 조성면적(㎡) 자동 입력
	$('#make_ar').change(function(){
		$('#make_ar_txt').text($('#make_ar').val());
	});

	//시설명 입력시 -> 기관명 자동 입력
	$('#biz_adres_detail, #biz_adres, #biz_zip_1, #biz_zip_2').change(function(){
		$('#biz_addr_txt').html('');
		$('#biz_addr_txt').append($('#biz_zip_1').val() + ' - ' + $('#biz_zip_2').val());
		$('#biz_addr_txt').append("<br />");
		$('#biz_addr_txt').append(fnClearXSS($('#biz_adres').val() + $('#biz_adres_detail').val()));
	});

	//단체연락처 입력시 -> 기관개요 자동 입력
	$('#adres_detail, #adres, #zip_1, #zip_2').change(function(){
		$('#adres_txt').html('');
		$('#adres_txt').append($('#zip_1').val() + ' - ' + $('#zip_2').val());
		$('#adres_txt').append("<br />");
		$('#adres_txt').append(fnClearXSS($('#adres').val() + ' ' + $('#adres_detail').val()));
	});

	//지상&옥상 녹화 입력시 -> 녹화유형 자동입력
	$('#ground_trplant_ar').change(function(){
		var str = "";
		if($('#ground_trplant_ar').val() != '') str = '지상녹화';
		if($('#rf_trplant_ar').val() != ""){
			if(str != '') str += "/옥상녹화";
			else str += "옥상녹화";
		}
		$('#green_type_txt').text(str);
		$('#make_ar').val(Number($('#ground_trplant_ar').val()) + Number($('#rf_trplant_ar').val()));
		$('#make_area_txt').text(Number($('#ground_trplant_ar').val()) + Number($('#rf_trplant_ar').val()));
	});
	$('#rf_trplant_ar').change(function(){
		var str = "";
		if($('#rf_trplant_ar').val() != '') str = '옥상녹화';
		if($('#ground_trplant_ar').val() != ""){
			if(str != '') str = "지상녹화/" + str;
			else str = "지상녹화";
		}
		$('#green_type_txt').text(str);
		$('#make_ar').val(Number($('#ground_trplant_ar').val()) + Number($('#rf_trplant_ar').val()));
		$('#make_area_txt').text(Number($('#ground_trplant_ar').val()) + Number($('#rf_trplant_ar').val()));
	});

	//제안요약서(녹색자금) 공사원가 작성시 -> 공사원가 계
	$('.gt_ct_val').change(function(){
		var gt_ct_tot = 0;
		$('.gt_ct_val').each(function(idx, el){
			gt_ct_tot += Number($(el).val());
		});
		$('#gf_ct_tot_txt').text(gt_ct_tot);
	});

	//제안요약서(녹색자금) 작성시 전체 합계 자동입력
	$('#gf_vals input[type=text]').change(function(){
		var gf_vals_tot = 0;
		$('#gf_vals input[type=text]').each(function(idx, el){
			gf_vals_tot += Number($(el).val());
		});
		$('.gf_vals_tot_txt').text(gf_vals_tot);
		var jabudam_vals_tot_txt = $('.jabudam_vals_tot_txt:eq(1)').text();
		var jabudam_vals_tot = 0;
		var biz_total = gf_vals_tot;
		if(jabudam_vals_tot_txt != ''){
			jabudam_vals_tot = Number(jabudam_vals_tot_txt);
			biz_total += jabudam_vals_tot;
		}
		$('#biz_total_txt').text(biz_total);
		$('.gf_vals_agv').text(roundXL(gf_vals_tot / biz_total * 100, 1));
		$('.jabudam_vals_agv').text(roundXL(jabudam_vals_tot / biz_total * 100, 1));
	});

	//제안요약서(자부담) 공사원가 작성시 -> 공사원가 계
	$('.jabudam_sp_val').change(function(){
		var jabudam_sp_tot = 0;
		$('.jabudam_sp_val').each(function(idx, el){
			jabudam_sp_tot += Number($(el).val());
		});
		$('#jabudam_sp_tot_txt').text(jabudam_sp_tot);
	});

	//제안요약서(자부담) 작성시 전체 합계 자동입력
	$('#jabudam_vals input[type=text]').change(function(){
		var jabudam_vals_tot = 0;
		$('#jabudam_vals input[type=text]').each(function(idx, el){
			jabudam_vals_tot += Number($(el).val());
		});
		$('.jabudam_vals_tot_txt').text(jabudam_vals_tot);
		var gf_vals_tot_txt = $('.gf_vals_tot_txt:eq(1)').text();
		var gf_vals_tot = 0;
		var biz_total = jabudam_vals_tot;
		if(gf_vals_tot_txt != ''){
			gf_vals_tot = Number(gf_vals_tot_txt);
			biz_total += gf_vals_tot;
		}
		$('#biz_total_txt').text(biz_total);
		$('.jabudam_vals_agv').text(roundXL(jabudam_vals_tot / biz_total * 100, 1));
		$('.gf_vals_agv').text(roundXL(gf_vals_tot / biz_total * 100, 1));
	});

	//수용정원 작성시 자동입력 -> 수용정원
	$('#aceptnc_psncpa').change(function(){
		$('#aceptnc_psncpa_txt').text($('#aceptnc_psncpa').val());
	});

	//이용자수,관리자수 작성시 자동입력 -> 이용자수
	$('#user_qy, #mngr_qy').change(function(){
		$('#user_qy_txt').text(Number($('#user_qy').val()) + Number($('#mngr_qy').val()));
	});

	//사업내용(식재공사비,기반조성비,시설물공사비,기타비용) 자동합계 및 퍼센트 계산
	$('#biz_ct input[type=text]').change(function(){
		var biz_ct_tot = 0;
		$('#biz_ct input[type=text]').each(function(idx, el){
			biz_ct_tot += Number($(el).val());
		});
		$('#biz_ct_tot').text(biz_ct_tot);
		$('#biz_ct input[type=text]').each(function(idx, el){
			if(biz_ct_tot == 0 || biz_ct_tot == '') {
				$(el).next().text(0);
			} else{
				$(el).next().text(roundXL(Number($(el).val()) / biz_ct_tot * 100, 1));
			}
		});
	});

	//교육장소 자동입력
	$('#edc_place').change(function(){
		$('#biz_addr_txt').text($('#edc_place').val());
	});

	//대표자 자동입력
	$('#rprsntv').change(function(){
		$('#rprsntv_txt').text($('#rprsntv').val());
	});

	//소외계층 대상 이벤트 -> 다문화,저소득,장애인 readonly true false
	$('#ud_co_ck').change(function(){
		if($('#ud_co_ck').prop('checked') == true) $('.ud_co_vals').prop('disabled' , false);
		else $('.ud_co_vals').prop('disabled' , true);
		fnStTargetCal();
	});

	//일반청소년 대상 -> 아동,고등학생 readonly true false
	$('#ut_co_ck').change(function(){
		if($('#ut_co_ck').prop('checked') == true) $('.ut_co_vals').prop('disabled' , false);
		else $('.ut_co_vals').prop('disabled' , true);
		fnStTargetCal();
	});

	//일반인 대상 -> 대학생,일반인,산주 readonly true false
	$('#at_co_ck').change(function(){
		if($('#at_co_ck').prop('checked') == true) $('.at_co_vals').prop('disabled' , false);
		else $('.at_co_vals').prop('disabled' , true);
		fnStTargetCal();
	});

	///연인원, 소외계층 비율 자동 입력
	$('.ud_co_vals, .ut_co_vals, .at_co_vals').change(function(){
		fnStTargetCal();
	});

	//1인당 교육비 자동 입력
	$('#tot_wct').change(function(){
		var co_tot_cnt = 0;
		$('.ud_co_vals, .ut_co_vals, .at_co_vals').each(function(idx, el){
			co_tot_cnt += Number($(el).val());
		});
		if(co_tot_cnt != 0){
			//$('#one_edc_mny').text(roundXL(Number($('#tot_wct').val()) / co_tot_cnt * 1000, 1));
			$('#one_edc_mny').text(fnNumberWithCommas(roundXL(Number($('#tot_wct').val()) / co_tot_cnt * 1000, 1)));
		} else {
			$('#one_edc_mny').text('');
		}
		fnBizOpePgmTotalfnAutoStatistics();
	});

	//사업내용 -> 문화·공연 체크시 관련 Input readonly 이벤트
	$('#cs_ck').change(function(){
		if($('#cs_ck').prop('checked') == true) $('.cs_vals').prop('disabled' , false);
		else $('.cs_vals').prop('disabled' , true);
		fnBizTypeInnerHtml();
	});

	//사업내용 -> (체험)박람회 체크시 관련 Input readonly 이벤트
	$('#ef_ck').change(function(){
		if($('#ef_ck').prop('checked') == true){
			$('.ef_vals').prop('disabled' , false);
		}else{
			$('.ef_vals').prop('disabled' , true);
		}
		fnBizTypeInnerHtml();
	});

	//사업내용 -> 산림레저활동(걷기,등산,산악스키등) 체크시 관련 Input readonly 이벤트
	$('#fl_ck').change(function(){
		if($('#fl_ck').prop('checked') == true){
			$('.fl_vals').prop('disabled' , false);
		}else{
			$('.fl_vals').prop('disabled' , true);
		}
		fnBizTypeInnerHtml();
	});

	//사업내용 -> 기타 체크시 관련 Input readonly 이벤트
	$('#etc_ck').change(function(){
		if($('#etc_ck').prop('checked') == true){
			$('.etc_vals').prop('disabled' , false);
		}else{
			$('.etc_vals').prop('disabled' , true);
			$('.etc_vals:eq(0)').val('');
			$('.etc_vals:gt(0)');
		}
	});

	//제안요약서(사업비 구성) 합계 자동입력 // 인건비,직접비,일반관리비 합계 및 평균
	$('#gf_ct_vals input[type=text]').change(function(){
		var gf_ct_tot = 0;
		var gf_ct_age_1 = 0;
		var gf_ct_age_2 = 0;
		var gf_ct_age_3 = 0;
		$('#gf_ct_vals input[type=text]').each(function(idx, el){
			gf_ct_tot += Number($(el).val());
			if(idx >= 0 && idx <= 2)
				gf_ct_age_1 += Number($(el).val());
			if(idx >= 3 && idx <= 13)
				gf_ct_age_2 += Number($(el).val());
			if(idx == 14)
				gf_ct_age_3 += Number($(el).val());
		});
		$('#gf_ct_vals_tot_txt').text(gf_ct_tot);
		$('#gf_ct_vals_age td:eq(0)').text(gf_ct_tot);
		$('#gf_ct_vals_age td:eq(1)').text(gf_ct_age_1);
		$('#gf_ct_vals_age td:eq(2)').text(gf_ct_age_2);
		$('#gf_ct_vals_age td:eq(3)').text(gf_ct_age_3);
	});

	//숲유형 자동입력 이벤트
	$('select[name=frt_ty_spcl]').change(function(){
		$('#frt_ty_spcl_txt').text($('select[name=frt_ty_spcl] option:selected').text());
	});

	//사업기간 시작일 자동입력 이벤트
	$('#biz_bgnde').change(function(){
		$('.biz_bgnde_txt').text($('#biz_bgnde').val());
	});

	//사업기간 종료일 자동입력 이벤트
	$('#biz_endde').change(function(){
		$('.biz_bgnde_txt').append(' ~ ' + $('#biz_endde').val());
	});

	var nowDate1 = new Date();
	var minDate = new Date((nowDate1.getUTCFullYear()+1), 0, 1);

	//달력 이벤트 로드
	fnDatepickerOptionAdd();
	$("#biz_bgnde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date()});
	$("#biz_endde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date()});

	$("#prtnfx_dc_dt").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$("#prtnfx_dsgn_bgnde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$("#prtnfx_dsgn_endde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$("#prtnfx_cntrwk_bgnde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$("#prtnfx_cntrwk_endde").datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_bizprpare_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_bizprpare_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_rcritpblanc_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_rcritpblanc_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_bizprogrs_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_bizprogrs_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_evlreport_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#prtnfx_evlreport_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#fclty_pudn_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#fclty_pudn_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#construction_bgnde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});
	$('#construction_endde').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true, minDate : new Date(minDate)});

	//기관정보 자동수정
	$('#zip_1, #zip_2').change(function(){
		$('#zip').val($('#zip_1').val() + "" + $('#zip_2').val());
	});
	$('#email1, #email2').change(function(){
		$('#email_adres').val($('#email1').val() + "@" + $('#email2').val());
	});
	$('#tel1, #tel2, #tel3').change(function(){
		$('#tlphon_no').val($('#tel1').val() + "-" + $('#tel2').val() +  "-" + $('#tel3').val());
	});
	$('#fax1, #fax2, #fax3').change(function(){
		$('#faxphon_no').val($('#fax1').val() + "-" + $('#fax2').val() +  "-" + $('#fax3').val());
	});
	$('#phone1, #phone2, #phone3').change(function(){
		$('#moblphon_no').val($('#phone1').val() + "-" + $('#phone2').val() +  "-" + $('#phone3').val());
	});

});

//교육대상 인원 체크 등
function fnStTargetCal(){
	var co_tot_cnt = 0;
	var ud_co_tot_cnt = 0;
	$('.ud_co_vals, .ut_co_vals, .at_co_vals').each(function(idx, el){
		if(!$(el).prop('disabled'))
			co_tot_cnt += Number($(el).val());
	});
	$('.ud_co_vals').each(function(idx, el){
		if(!$(el).prop('disabled'))
			ud_co_tot_cnt += Number($(el).val());
	});
	if(ud_co_tot_cnt != 0){
		$('.ud_co_tot_cnt_txt').text(roundXL(ud_co_tot_cnt / co_tot_cnt * 100, 1));
	} else {
		$('.ud_co_tot_cnt_txt').text('0');
	}
	$('.ud_ut_at_tot_txt').text(co_tot_cnt);
	if($('#tot_wct').val() != '' && co_tot_cnt != 0){
		//$('#one_edc_mny').text(roundXL(Number($('#tot_wct').val()) / co_tot_cnt * 1000, 1));
		$('#one_edc_mny').text(fnNumberWithCommas(roundXL(Number($('#tot_wct').val()) / co_tot_cnt * 1000, 1)));
	} else {
		$('#one_edc_mny').text('');
	}
}

//이메일 주소 선택시 자동 입력
function onEmailSelected(){
	$('#email2').val($('#email_choice').val());
	$('#email_adres').val($('#email1').val() + "@" + $('#email2').val());
}

//사업유형 자동입력 이벤트
function fnBizTypeInnerHtml(){
	var biz_str = '';
	$('#biz_addr_txt').html('');
	$('#cs_ck, #ef_ck, #fl_ck').each(function(idx, el){
		if(idx == 0 && $(el).prop('checked') == true){
			biz_str = '문화공연행사';
		}else if(idx == 1 && $(el).prop('checked') == true){
			if(biz_str != '') biz_str += ', 체험형 박람회';
			else biz_str += '체험형 박람회';
		}else if(idx == 2 && $(el).prop('checked') == true){
			if(biz_str != '') biz_str += ', 산림레포츠';
			else biz_str += '산림레포츠';
		}
	});
	$('#biz_addr_txt').html(biz_str);
}

//다음 우편번호 API
function fnDaumPostCodeSearchAPI(zip, zip1, zip2, adr1, adr2){
	new daum.Postcode({
		oncomplete: function(data) {
			zip.val(data.postcode1+""+data.postcode2);
			zip1.val(data.postcode1);
			zip2.val(data.postcode2);
			adr1.val(data.address);
			adr2.focus();
		}
	}).open();
}

//사업연속성, 운영프로그램, 사업규모 등록시 이름 치환시키기
function fnInputNameChange(){
	$('.bizCotnSeq tr').each(function(b_idx, item){
		$(item).find('input, select').each(function(idx, el){
			$(el).attr('name' , 'bcslist['+ (b_idx - 1) + '].' + $(el).attr('name'));
		});
	});
	$('.prg').each(function(b_idx, item){
		$(item).find('input, textarea, select').each(function(idx, el){
			$(el).attr('name' , 'boplist['+ b_idx + '].' + $(el).attr('name'));
		});
	});
	$('.bizSize tr').each(function(b_idx, item){
		$(item).find('input').each(function(idx, el){
			$(el).attr('name' , 'bbslist['+ (b_idx - 1) + '].' + $(el).attr('name'));
		});
	});
}

var bizSize_basic_tr = null;
$(function(){
	//사업규모 1ROW 복사
	bizSize_basic_tr = $('.bizSize tr:eq(1)').clone();

	//사업유형 자동입력
	if($('#biz_ty_code').val() == 'BTC06'){
		fnBizTypeInnerHtml();
	}
});

//사업규모 추가
function fnbizSizeAdd(){
	var el = bizSize_basic_tr.clone();
	el.find('input').val('');
	$('.bizSize').append(el);
	if($('.bizSize tr').length != 1){
		$('.bizSize_0').hide();
	}
}

//사업규모 삭제
function fnbizSizeDelete(el){
	if(confirm("(주의)선택하신 사업규모을 삭제 하시겠습니까?")){
		var pk = el.parent().parent().parent().find('input[name=biz_scale_id]');
		if(pk.val() != ''){
			var url = '/gfund/biz/bassinfo/ajaxDeleteBusinessScale.do';
			var data = [];
			data = {biz_scale_id : pk.val(), biz_id : $('#biz_id').val()};
			var successFn = function(json){
				if(Number(json.rs) > 0){
					alert('삭제되었습니다.');
					el.parent().parent().parent().remove();
					if($('.bizSize tr').length == 1){
						$('.bizSize_0').show();
					}else{
						$('.bizSize_0').hide();
					}
				}
			};
			fn_ajax_json(url, data, successFn, null);
		}else{
			el.parent().parent().parent().remove();
			if($('.bizSize tr').length == 1){
				$('.bizSize_0').show();
			}else{
				$('.bizSize_0').hide();
			}
		}
	}
}

var bizCotnSeq_basic_tr = null;
$(function(){
	//사업 연속성 1ROW 복사
	bizCotnSeq_basic_tr = $('.bizCotnSeq tr:eq(1)').clone();
});

//사업 연속성 추가
function fnBizCotnSeqAdd(){
	var el = bizCotnSeq_basic_tr.clone();
	el.find('input').val('');
	$('.bizCotnSeq').append(el);
	if($('.bizCotnSeq tr').length != 1){
		$('.bizCotnSeq_0').hide();
	}
}

//사업 연속성 삭제
function fnBizCotnSeqDelete(el){
	if(confirm("(주의)선택하신 사업연속성을 삭제 하시겠습니까?")){
		var pk = el.parent().parent().parent().find('input[name=biz_ctnu_id]');
		if(pk.val() != ''){
			var url = '/gfund/biz/bassinfo/ajaxDeleteBusinessContentsSequence.do';
			var data = [];
			data = {biz_ctnu_id : pk.val(), biz_id : $('#biz_id').val()};
			var successFn = function(json){
				if(Number(json.rs) > 0){
					alert('삭제되었습니다.');
					el.parent().parent().parent().remove();
					if($('.bizCotnSeq tr').length == 1){
						$('.bizCotnSeq_0').show();
					}else{
						$('.bizCotnSeq_0').hide();
					}
				}
			};
			fn_ajax_json(url, data, successFn, null);
		}else{
			el.parent().parent().parent().remove();
			if($('.bizCotnSeq tr').length == 1){
				$('.bizCotnSeq_0').show();
			}else{
				$('.bizCotnSeq_0').hide();
			}
		}
	}
}

var bizPgm_basic_tr = null;
$(function(){
	//운영프로그램 1ROW 복사
	bizPgm_basic_tr = $('.bizOpePgm tr:eq(1)').clone();
	//달력 이벤트
	$('input[name=bgnde]').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
	$('input[name=endde]').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
});

//회당,총 인원 소계 자동 입력
function fnAutoStatistics(el, cl, el_1, el_2, el_3){
	var one_pers = $(el).parent().parent().parent().find(cl);
	var one_pers_val_1 = $(el).parent().parent().parent().find(el_1).val();
	var one_pers_val_2 = $(el).parent().parent().parent().find(el_2).val();
	var one_pers_val_3 = $(el).parent().parent().parent().find(el_3).val();
	one_pers.text(Number(one_pers_val_1) + Number(one_pers_val_2) + Number(one_pers_val_3));
	fnAutoYearTotal(el);
	fnBizOpePgmTotalfnAutoStatistics();
}

//연인원 자동 입력
function fnAutoYearTotal(el){
	var parent_el = el.parent().parent().parent();
	var tot_co = parent_el.find('input[name=tot_co]').val();
	var edc_co = parent_el.find('input[name=edc_co]').val();
	var edc_trget_01_co = parent_el.find('input[name=edc_trget_01_co]').val();
	parent_el.find('input[name=edc_trget_01_co]').parent().next().text(Number(edc_trget_01_co) * Number(edc_co) * Number(tot_co));
	var edc_trget_03_co = parent_el.find('input[name=edc_trget_03_co]').val();
	parent_el.find('input[name=edc_trget_03_co]').parent().next().text(Number(edc_trget_03_co) * Number(edc_co) * Number(tot_co));
	var edc_trget_05_co = parent_el.find('input[name=edc_trget_05_co]').val();
	parent_el.find('input[name=edc_trget_05_co]').parent().next().text(Number(edc_trget_05_co) * Number(edc_co) * Number(tot_co));
	parent_el.find('.year_pers_tot_txt').text(Number(edc_trget_01_co) * Number(edc_co) * Number(tot_co) + Number(edc_trget_03_co) * Number(edc_co) * Number(tot_co) + Number(edc_trget_05_co) * Number(edc_co) * Number(tot_co));
	fnBizOpePgmTotalfnAutoStatistics();
}

//운영프로그램 합계 자동입력
function fnBizOpePgmTotalfnAutoStatistics(){
	var edc_co = 0;
	$('input[name=edc_co]').each(function(idx, el){ edc_co += Number($(el).val());}); $('.edc_co_tot_txt').text(edc_co);

	var round_edcde_co = 0;
	$('input[name=round_edcde_co]').each(function(idx, el){ round_edcde_co += Number($(el).val()); }); $('.round_edcde_co_tot_txt').text(round_edcde_co);

	var edc_trget_01_co = 0;
	$('input[name=edc_trget_01_co]').each(function(idx, el){ edc_trget_01_co += Number($(el).val()); }); $('.edc_trget_01_co_tot_txt').text(edc_trget_01_co);

	//var edc_trget_02_co = 0;
	//$('input[name=edc_trget_02_co]').each(function(idx, el){ edc_trget_02_co += Number($(el).val()); }); $('.edc_trget_02_co_tot_txt').text(edc_trget_02_co);

	var edc_trget_03_co = 0;
	$('input[name=edc_trget_03_co]').each(function(idx, el){ edc_trget_03_co += Number($(el).val()); }); $('.edc_trget_03_co_tot_txt').text(edc_trget_03_co);

	//var edc_trget_04_co = 0;
	//$('input[name=edc_trget_04_co]').each(function(idx, el){ edc_trget_04_co += Number($(el).val()); }); $('.edc_trget_04_co_tot_txt').text(edc_trget_04_co);

	var edc_trget_05_co = 0;
	$('input[name=edc_trget_05_co]').each(function(idx, el){ edc_trget_05_co += Number($(el).val()); }); $('.edc_trget_05_co_tot_txt').text(edc_trget_05_co);

	//var edc_trget_06_co = 0;
	//$('input[name=edc_trget_06_co]').each(function(idx, el){ edc_trget_06_co += Number($(el).val()); }); $('.edc_trget_06_co_tot_txt').text(edc_trget_06_co);

	var edc_trget_co_tot_txt_1 = 0;
	$('.edc_trget_co_tot_txt_1').each(function(idx, el){ edc_trget_co_tot_txt_1 += Number($(el).text()); }); $('.edc_trget_co_tot_txt_1_txt').text(edc_trget_co_tot_txt_1);

	var edc_trget_co_tot_txt_2 = 0;
	$('.edc_trget_co_tot_txt_2').each(function(idx, el){ edc_trget_co_tot_txt_2 += Number($(el).text()); }); $('.edc_trget_co_tot_txt_2_txt').text(edc_trget_co_tot_txt_2);

	var edc_trget_co_tot_txt_3 = 0;
	$('.edc_trget_co_tot_txt_3').each(function(idx, el){ edc_trget_co_tot_txt_3 += Number($(el).text()); }); $('.edc_trget_co_tot_txt_3_txt').text(edc_trget_co_tot_txt_3);

	$('.edc_trget_135_tot_txt').text(edc_trget_01_co + edc_trget_03_co + edc_trget_05_co);
	//$('.edc_trget_246_tot_txt').text(edc_trget_02_co + edc_trget_04_co + edc_trget_06_co);
	$('.edc_trget_135_year_tot_txt').text(edc_trget_co_tot_txt_1 + edc_trget_co_tot_txt_2 + edc_trget_co_tot_txt_3);
	$('.one_edu_money').text(roundXL(Number($('#tot_wct').val())/(edc_trget_co_tot_txt_1 + edc_trget_co_tot_txt_2 + edc_trget_co_tot_txt_3) * 1000, 1));
}

//운영프로그램 추가
function fnBizOpePgmAdd(){
	var el = bizPgm_basic_tr.clone();
	el.find('input, textarea').val('');
	el.find('input[name=bgnde]').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
	el.find('input[name=endde]').datepicker({ dateFormat : "yy-mm-dd", changeYear : true, changeMonth : true });
	$('.bizOpePgm').append(el);
	$('.program_cnt').text($('.bizOpePgm > tr').length - 1);
	if($('.bizOpePgm > tr').length != 1){
		$('.BizOpePgm_0').hide();
	}
}

//운영프로그램 삭제
function fnBizOpePgmDelete(el){
	if(confirm("(주의)선택하신 운영프로그램을 삭제 하시겠습니까?")){
		var pk = el.parent().parent().parent().parent().parent().parent().parent().parent().find('input[name=biz_progrm_id]');
		if(pk.val() != ''){
			var url = '/gfund/biz/bassinfo/ajaxDeleteBusinessOperationalProgramme.do';
			var data = [];
			data = {biz_progrm_id : pk.val(), biz_id : $('#biz_id').val()};
			var successFn = function(json){
				if(Number(json.rs) > 0){
					alert('삭제되었습니다.');
					el.parent().parent().parent().parent().parent().parent().parent().parent().remove();
					$('.program_cnt').text($('.bizOpePgm > tr').length - 1);
					if($('.bizOpePgm > tr').length == 1){
						$('.BizOpePgm_0').show();
					}else{
						$('.BizOpePgm_0').hide();
					}
					fnBizOpePgmTotalfnAutoStatistics();
				}
			};
			fn_ajax_json(url, data, successFn, null);
		}else{
			el.parent().parent().parent().parent().parent().parent().parent().parent().remove();
			$('.program_cnt').text($('.bizOpePgm > tr').length - 1);
			if($('.bizOpePgm > tr').length == 1){
				$('.BizOpePgm_0').show();
			}else{
				$('.BizOpePgm_0').hide();
			}
		}
	}
}