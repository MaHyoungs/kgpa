<%@ page language = "java" contentType = "text/html; charset=UTF-8"%>

<%
	/********************************************************************************************************************************************
	 NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

	 서비스명 : 가상주민번호서비스 (IPIN) 서비스
	 페이지명 : 가상주민번호서비스 (IPIN) 사용자 인증 정보 처리 페이지

	 수신받은 데이터(인증결과)를 메인화면으로 되돌려주고, close를 하는 역활을 합니다.
	 *********************************************************************************************************************************************/

	// 사용자 정보 및 CP 요청번호를 암호화한 데이타입니다. (ipin_main.jsp 페이지에서 암호화된 데이타와는 다릅니다.)
	String sResponseData = request.getParameter("enc_data");

	// ipin_main.jsp 페이지에서 설정한 데이타가 있다면, 아래와 같이 확인가능합니다.
	String sReservedParam1 = request.getParameter("param_r1");
	String sReservedParam2 = request.getParameter("param_r2");
	String sReservedParam3 = request.getParameter("param_r3");

	// 암호화된 사용자 정보가 존재하는 경우
	if (!sResponseData.equals("") && sResponseData != null)
	{

%>

<html  lang="ko">
<head>
	<title>NICE신용평가정보 가상주민번호 서비스</title>
	<script language='javascript'>
		function fnLoad()
		{
			document.vnoform.submit();

		}
	</script>
</head>
<body onLoad="fnLoad()">
	<!-- 가상주민번호 서비스 팝업 페이지에서 사용자가 인증을 받으면 암호화된 사용자 정보는 해당 팝업창으로 받게됩니다.
	 따라서 부모 페이지로 이동하기 위해서는 다음과 같은 form이 필요합니다. -->
	<form name="vnoform" method="post" action="ipin_result.jsp">
		<input type="hidden" name="enc_data" value="<%= sResponseData %>" />								<!-- 인증받은 사용자 정보 암호화 데이타입니다. -->
		
		<!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
	    	 해당 파라미터는 추가하실 수 없습니다. -->
	    <input type="hidden" name="param_r1" value="<%= sReservedParam1 %>" />
	    <input type="hidden" name="param_r2" value="<%= sReservedParam2 %>" />
	    <input type="hidden" name="param_r3" value="<%= sReservedParam3 %>" />
	</form>
	
<%
	} else {
%>

<html>
<head>
	<title>NICE신용평가정보 가상주민번호 서비스</title>
	<body onLoad="self.close()">

<%
	}
%>

</body>
</html>