<%@ page language = "java" contentType = "text/html; charset=UTF-8"%>
<%@ page language="java" import="Kisinfo.Check.IPINClient" %>

<%
	/********************************************************************************************************************************************
	 NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

	 서비스명 : 가상주민번호서비스 (IPIN) 서비스
	 페이지명 : 가상주민번호서비스 (IPIN) 결과 페이지
	 *********************************************************************************************************************************************/

	String sSiteCode				= "H079";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
	String sSitePw					= "35416143";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)



	// 사용자 정보 및 CP 요청번호를 암호화한 데이타입니다.
	String sResponseData = request.getParameter("enc_data");

	// CP 요청번호 : ipin_main.jsp 에서 세션 처리한 데이타
	String sCPRequest = (String)session.getAttribute("CPREQUEST");


	// 객체 생성
	IPINClient pClient = new IPINClient();
	
	
	/*
	┌ 복호화 함수 설명  ──────────────────────────────────────────────────────────
		Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
		
		fnResponse 함수는 결과 데이타를 복호화 하는 함수이며,
		'sCPRequest'값을 추가로 보내시면 CP요청번호 일치여부도 확인하는 함수입니다. (세션에 넣은 sCPRequest 데이타로 검증)
		
		따라서 귀사에서 원하는 함수로 이용하시기 바랍니다.
	└────────────────────────────────────────────────────────────────────
	*/
	int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData);
	//int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData, sCPRequest);

	String sRtnMsg				= "";							// 처리결과 메세지
	String sVNumber				= pClient.getVNumber();			// 가상주민번호 (13자리이며, 숫자 또는 문자 포함)
	String sName				= pClient.getName();			// 이름
	String sDupInfo				= pClient.getDupInfo();			// 중복가입 확인값 (DI - 64 byte 고유값)
	String sAgeCode				= pClient.getAgeCode();			// 연령대 코드 (개발 가이드 참조)
	String sGenderCode			= pClient.getGenderCode();		// 성별 코드 (개발 가이드 참조)
	String sBirthDate			= pClient.getBirthDate();		// 생년월일 (YYYYMMDD)
	String sNationalInfo		= pClient.getNationalInfo();	// 내/외국인 정보 (개발 가이드 참조)
	String sCPRequestNum		= pClient.getCPRequestNO();		// CP 요청번호

	// Method 결과값에 따른 처리사항
	if (iRtn == 1)
	{
		/*
			다음과 같이 사용자 정보를 추출할 수 있습니다.
			사용자에게 보여주는 정보는, '이름' 데이타만 노출 가능합니다.
		
			사용자 정보를 다른 페이지에서 이용하실 경우에는
			보안을 위하여 암호화 데이타(sResponseData)를 통신하여 복호화 후 이용하실것을 권장합니다. (현재 페이지와 같은 처리방식)
			
			만약, 복호화된 정보를 통신해야 하는 경우엔 데이타가 유출되지 않도록 주의해 주세요. (세션처리 권장)
			form 태그의 hidden 처리는 데이타 유출 위험이 높으므로 권장하지 않습니다.
		*/

		// 사용자 인증정보에 대한 변수
		
		/*
		out.println("가상주민번호 : " + sVNumber + "<br/>");
		out.println("이름 : " + sName + "<br/>");
		out.println("중복가입 확인값 (DI) : " + sDupInfo + "<br/>");
		out.println("연령대 코드 : " + sAgeCode + "<br/>");
		out.println("성별 코드 : " + sGenderCode + "<br/>");
		out.println("생년월일 : " + sBirthDate + "<br/>");
		out.println("내/외국인 정보 : " + sNationalInfo + "<br/>");
		out.println("CP 요청번호 : " + sCPRequestNum + "<br/>");
		out.println("***** 복호화 된 정보가 정상인지 확인해 주시기 바랍니다.<br/><br/><br/><br/>");
		*/

		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "vNumber", sVNumber);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "dupInfo", sDupInfo);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "realName", sName);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "birthDate", sBirthDate);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "sex", sGenderCode);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "foreigner", sNationalInfo);

		sRtnMsg = "정상 처리되었습니다.";

	}
	else if (iRtn == -1 || iRtn == -4)
	{
		sRtnMsg =	"iRtn 값, 서버 환경정보를 정확히 확인하여 주시기 바랍니다.";
	}
	else if (iRtn == -6)
	{
		sRtnMsg =	"당사는 한글 charset 정보를 euc-kr 로 처리하고 있으니, euc-kr 에 대해서 허용해 주시기 바랍니다.<br/>" +
				"한글 charset 정보가 명확하다면 ..<br/><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
	}
	else if (iRtn == -9)
	{
		sRtnMsg = "입력값 오류 : fnResponse 함수 처리시, 필요한 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	}
	else if (iRtn == -12)
	{
		sRtnMsg = "CP 비밀번호 불일치 : IPIN 서비스 사이트 패스워드를 확인해 주시기 바랍니다.";
	}
	else if (iRtn == -13)
	{
		sRtnMsg = "CP 요청번호 불일치 : 세션에 넣은 sCPRequest 데이타를 확인해 주시기 바랍니다.";
	}
	else
	{
		sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 개발 담당자에게 문의해 주세요.";
	}

%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<script src="/template/common/js/jquery/jquery-1.7.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="/template/member/css/login.css" type="text/css" charset="utf-8" />
   <title>NICE신용평가정보 가상주민번호 서비스</title>
   <script language="javascript">
	$(document).ready(function(){
		$("#btn_ok").click(function(){
			window.close();
		});
	});
	function realNameSubmit(){
		<% if(iRtn == 1) { %>
		opener.checkReturnCallFn("<%=sVNumber%>", "<%=sName%>", "<%=sDupInfo%>");
		<% } %>
		//self.close();
	}
	</script>
<style type="text/css">
html, body{background:#fff}
.gbox{background:#efefef;text-align:center;padding:20px 30px;margin:15px 0;border-radius:10px;}
</style>
</head>
<body onload="realNameSubmit()">

<div class="pop_tit">
	<h1>본인인증 확인</h1>
</div>

<div class="pop_con">
	<form name="checkForm" action="#" method="post">
		<fieldset>
			<legend>본인인증 확인</legend>
				<div class="gbox">
					<span class="orange"><%= sRtnMsg %></span>
				</div>
				<div class="btn_c">
					<span class="btn_s"><a id="btn_ok" href="javascript:checkReturnCallFn('<%=sVNumber%>', '<%=sName%>', '<%=sDupInfo%>');" target="authWin">확인</a></span>
				</div>
		</fieldset>
	</form>
</div>

<button class="pop_mclose" onclick="window.close(); return false;">닫기</button>

</body>
</html>