<%@ page language="java" contentType="text/html;charset=euc-kr" %>

<%
	NiceID.Check.CPClient niceCheck = new NiceID.Check.CPClient();

	String sEncodeData = requestReplace(request.getParameter("EncodeData"), "encodeData");
	String sReserved1 = requestReplace(request.getParameter("param_r1"), "");
	String sReserved2 = requestReplace(request.getParameter("param_r2"), "");
	String sReserved3 = requestReplace(request.getParameter("param_r3"), "");

	String sSiteCode = "G4576";                   // NICE로부터 부여받은 사이트 코드
	String sSitePassword = "NFTNGQXLLC2L";             // NICE로부터 부여받은 사이트 패스워드

	String sCipherTime = "";                 // 복호화한 시간
	String sRequestNumber = "";             // 요청 번호
	String sResponseNumber = "";         // 인증 고유번호
	String sAuthType = "";                   // 인증 수단
	String sName = "";                             // 성명
	String sDupInfo = "";                         // 중복가입 확인값 (DI_64 byte)
	String sConnInfo = "";                     // 연계정보 확인값 (CI_88 byte)
	String sBirthDate = "";                     // 생일
	String sGender = "";                         // 성별
	String sNationalInfo = "";       // 내/외국인정보 (개발가이드 참조)
	String sMessage = "";
	String sPlainData = "";

	int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

	if(iReturn == 0){
		sPlainData = niceCheck.getPlainData();
		sCipherTime = niceCheck.getCipherDateTime();

		// 데이타를 추출합니다.
		java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

		sRequestNumber = (String)mapresult.get("REQ_SEQ");
		sResponseNumber = (String)mapresult.get("RES_SEQ");
		sAuthType = (String)mapresult.get("AUTH_TYPE");
		sName = (String)mapresult.get("NAME");
		sBirthDate = (String)mapresult.get("BIRTHDATE");
		sGender = (String)mapresult.get("GENDER");
		sNationalInfo = (String)mapresult.get("NATIONALINFO");
		sDupInfo = (String)mapresult.get("DI");
		sConnInfo = (String)mapresult.get("CI");
		// 휴대폰 번호 : MOBILE_NO => (String)mapresult.get("MOBILE_NO");
		// 이통사 정보 : MOBILE_CO => (String)mapresult.get("MOBILE_CO");
		// checkplus_success 페이지에서 결과값 null 일 경우, 관련 문의는 관리담당자에게 하시기 바랍니다.
		String session_sRequestNumber = (String)session.getAttribute("REQ_SEQ");

		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "dupInfo", sDupInfo);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "realName", sName);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "birthDate", sBirthDate);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "sex", sGender);
		egovframework.com.utl.cas.service.EgovSessionCookieUtil.setSessionAttribute(request, "foreigner", sNationalInfo);

		System.out.println(sName);
		System.out.println(sBirthDate);
		System.out.println(session_sRequestNumber);
		System.out.println(sResponseNumber);

		sMessage = "정상 처리되었습니다.";
		if(!sRequestNumber.equals(session_sRequestNumber)){
			sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";

			sResponseNumber = "";
			sAuthType = "";
		}
	}else if(iReturn == -1){
		sMessage = "복호화 시스템 에러입니다.";
	}else if(iReturn == -4){
		sMessage = "복호화 처리오류입니다.";
	}else if(iReturn == -5){
		sMessage = "복호화 해쉬 오류입니다.";
	}else if(iReturn == -6){
		sMessage = "복호화 데이터 오류입니다.";
	}else if(iReturn == -9){
		sMessage = "입력 데이터 오류입니다.";
	}else if(iReturn == -12){
		sMessage = "사이트 패스워드 오류입니다.";
	}else{
		sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
	}

%>
<%!
	public static String requestReplace(String paramValue, String gubun){
		String result = "";

		if(paramValue != null){

			paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

			paramValue = paramValue.replaceAll("\\*", "");
			paramValue = paramValue.replaceAll("\\?", "");
			paramValue = paramValue.replaceAll("\\[", "");
			paramValue = paramValue.replaceAll("\\{", "");
			paramValue = paramValue.replaceAll("\\(", "");
			paramValue = paramValue.replaceAll("\\)", "");
			paramValue = paramValue.replaceAll("\\^", "");
			paramValue = paramValue.replaceAll("\\$", "");
			paramValue = paramValue.replaceAll("'", "");
			paramValue = paramValue.replaceAll("@", "");
			paramValue = paramValue.replaceAll("%", "");
			paramValue = paramValue.replaceAll(";", "");
			paramValue = paramValue.replaceAll(":", "");
			paramValue = paramValue.replaceAll("-", "");
			paramValue = paramValue.replaceAll("#", "");
			paramValue = paramValue.replaceAll("--", "");
			paramValue = paramValue.replaceAll("-", "");
			paramValue = paramValue.replaceAll(",", "");

			if(gubun != "encodeData"){
				paramValue = paramValue.replaceAll("\\+", "");
				paramValue = paramValue.replaceAll("/", "");
				paramValue = paramValue.replaceAll("=", "");
			}

			result = paramValue;

		}
		return result;
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<script src="/template/common/js/jquery/jquery-1.7.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="/template/member/css/login.css" type="text/css" charset="utf-8" />
   <title>NICE신용평가정보 가상주민번호 서비스</title>
   <script language="javascript">
	$(document).ready(function(){
		$("#btn_ok").click(function(){

			realNameSubmit();
			window.close();
		});
	});
	function realNameSubmit(){
		<% if(iReturn == 0) { %>
		opener.checkReturnCallFn("<%=sResponseNumber%>", "<%=sName%>", "<%=sDupInfo%>");
		<% } %>
		//self.close();
	}
	</script>
<style type="text/css">
html, body{ background:#fff }
.gbox{ background:#efefef; text-align:center; padding:20px 30px; margin:15px 0; border-radius:10px; }
</style>
</head>
<body>

<div class="pop_tit">
	<h1>본인인증 확인</h1>
</div>

<div class="pop_con">
	<form name="checkForm" action="#" method="post">
		<fieldset>
			<legend>본인인증 확인</legend>
				<div class="gbox">
					<span class="orange"><%= sMessage %></span>
				</div>
				<div class="btn_c">
					<span class="btn_s"><a id="btn_ok" href="javascript:checkReturnCallFn('<%=sResponseNumber%>', '<%=sName%>', '<%=sDupInfo%>');" target="authWin">확인</a></span>
				</div>
		</fieldset>
	</form>
</div>

<button class="pop_mclose" onclick="window.close(); return false;">닫기</button>

</body>
</html>