<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>" />
<c:import url="/msi/sch/tmplatHead.do" charEncoding="utf-8">
	<c:param name="conKeep" value="Y"/>
	<c:param name="schTitle" value="Y" />
</c:import>

<link rel="stylesheet" href="${_CSS}/page.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="${_C_JS}/jquery/jquery.form.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript">
		
<c:if test='${not empty message}'>
alert("${message}");
</c:if>

<%--
<c:if test='${empty USER_INFO.userSeCode}'>
location.href="/index.do";
</c:if>
--%>


/* 게시판용 하단 1depth 메뉴 on/off */
$(function(){
	$('.menu_all .open').click(function(){
		$('.menu_all').animate({"bottom": 0}, 1000);
		$('.menu_all a.open').css({'display':'none'});
		$('.menu_all a.close').css({'display':'block'});
	});
	$('.menu_all .close').click(function(){
		$('.menu_all').animate({"bottom": -95}, 1000);
		$('.menu_all a.open').css({'display':'block'});
		$('.menu_all a.close').css({'display':'none'});
	});
	
});
</script>	
<%
	request.setCharacterEncoding("utf-8"); 

	String keywd = request.getParameter("qt");
%>
	<div class="mt100"> </div>
	<div id="bbs_wrap">
		<!-- 컨텐츠 부분 -->
		<iframe name="cntntsIframe" id="cntntsIframe" src="/RSA/front/Search.jsp?qt=<%=keywd%>&userSe=${USER_INFO.userSeCode}" frameborder="0" width="100%" height="900px"  scrolling="no" ></iframe> 
		<!-- 컨텐츠 부분 -->
		
	</div> <!-- #bbsWrap end -->
			
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="conKeep" value="Y"/>
</c:import>		