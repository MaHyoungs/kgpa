function removeHtml(text)

{
	text = text.replace(/<br>/ig, "\n"); // <br>을 엔터로 변경

	text = text.replace(/&nbsp;/ig, " "); // 공백

	// HTML 태그제거

	text = text.replace(/<(\/)?([a-zA-Z]*)(\s[a-zA-Z]*=[^>]*)?(\s)*(\/)?>/ig, "");

	// shkim.add.

	text = text.replace(/<(no)?script[^>]*>.*?<\/(no)?script>/ig, "");

	text = text.replace(/<style[^>]*>.*<\/style>/ig, "");

	text = text.replace(/<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>/ig, "");

	text = text.replace(/<\\w+\\s+[^<]*\\s*>/ig, "");

	text = text.replace(/&[^;]+;/ig, "");

	text = text.replace(/\\s\\s+/ig, "");



	return text;

}



$(function(){

	var url = 'https://apis.daum.net/blog/info/blog.do?blogName=kgpa&output=json';

	jQuery.ajax({

		url : url,

		type     : 'GET',

		dataType : 'jsonp',

		timeout  : 2000, //ms

		jsonp : 'callback', //오픈API 제공하는 콜백함수의 key 값을 쓴다.

		success : function(json) {

			if ("200" == json.channel.status) {

				// nickname, title, profileThumbnailImageUrl, url=블로그URL,  visitorCount=방문자수

				var e = json.channel;

				var logo = jQuery("<img>").attr({'src':e.profileThumbnailImageUrl});

				/*jQuery('#div_blog_logo').append(logo);*/

/*
				var title = e.title.substr(0,6) + '<br/>' + e.title.substr(6,12);
*/

				jQuery('#div_blog_tit').append(e.title);

				jQuery('#span_blog_visit').text(e.visitorCount);

			}

		}

	});

});



/* 다음의 시간을 화면표시용으로 포멧팅한다. */

function fnParseDaumBlogDate(date) {

	if (undefined == date || '' == date) return '';



	var strDate = date.substr(0,16);

	var tmpArr1 = strDate.split(' ');

	var tmpArr2 = tmpArr1[0].split('-');

	tmpArr1 = tmpArr1[1].split(':');

	strDate = tmpArr2[0] + '년 ' + parseInt(tmpArr2[1]) + '월 ' + parseInt(tmpArr2[2]) + '일' + parseInt(tmpArr1[0]) + '시 ' + parseInt(tmpArr1[1]) + '분';



	return strDate;

}



$(function(){

	var url = 'https://apis.daum.net/blog/post/list.do?blogName=kgpa&result=5&output=json&viewContent=Y';

	jQuery.ajax({

		url : url,

		type     : 'GET',

		dataType : 'jsonp',

		timeout  : 2000, //ms

		jsonp : 'callback', //오픈API 제공하는 콜백함수의 key 값을 쓴다.

		success : function(json) {

			if ("200" == json.channel.status) {

				var divGroup = jQuery('#div_blog_group');



				for (var k in json.channel.item) {



					var e = json.channel.item[k];



					if ('녹색사업단' == e.nickname) e.nickname = e.nickname + '(Korea Green Promotion Agency)';



					// Cen, Img, Comment div 클래스의 반복

					var divCen = jQuery("<div>").attr({'class': 'Cen'});

					var divOrg = jQuery("<div>").attr({'class': 'org'});



					var divOrgP = "";

					var divOrgPImg = ""; // mycon, 작성자별로 이미지가 없어서 그냥 본문이미지 썸네일로 대체

					var contentImg = ""; // 본문용



					if (undefined != e.images && undefined != e.images.image) {

						var tmpImg = e.images.image[0];



						var tmpArr = tmpImg.filenameUTF8.split(".");

						var divOrgP = jQuery("<p>");

						var divOrgPImg = jQuery("<img>").attr({alt:e.title, src:tmpImg.thumbnailImageUrl, width:'60px', height:'60px'});

						contentImg = jQuery("<img>").attr({alt:e.title, src:tmpImg.imageUrl, width:contentImg_w, height:contentImg_h});



						var maxWidth = 190;

						var contentImg_w = contentImg.get(0).width;

						var contentImg_h = contentImg.get(0).height;



						if(contentImg_w > maxWidth){

							var wP = maxWidth / contentImg_w * 100;

							contentImg_h = contentImg_h * wP / 100;

							contentImg_w = maxWidth;

							contentImg.attr({width: contentImg_w, height: contentImg_h});

						}



						divOrgP.append(divOrgPImg);

						divOrg.append(divOrgP);

					} else {

						var divOrgP = jQuery("<p>");

						var divOrgPImg = jQuery("<img>").attr({alt:'녹색사업단', src:'https://graph.facebook.com/1507222112844133/picture', width:'60px', height:'60px'});

						divOrgP.append(divOrgPImg);

						divOrg.append(divOrgP);

					}




					var divOrgDl = jQuery("<dl>");

					var divOrgDlDt = jQuery("<dt>");

					divOrgDlDt.html('<a href="' + e.url + '" target="_blank" alt="' + e.nickname + '">' + e.nickname + '</a>');



					var divOrgDlDd = jQuery("<dd>");

					var divOrgDlDdSpan = jQuery("<span>");



					// var strDivOrgDlDdSpan = "<img class=\"pd\" src=\"\" alt=\"\">";

					var strDivOrgDlDdSpan = "";



					if (undefined != e.date) {

						strDivOrgDlDdSpan = e.date.substr(0,16);

						var tmpArr1 = strDivOrgDlDdSpan.split(' ');

						var tmpArr2 = tmpArr1[0].split('-');

						tmpArr1 = tmpArr1[1].split(':');

						//strDivOrgDlDdSpan = tmpArr2[0] + '년 ' + parseInt(tmpArr2[1]) + '월 ' + parseInt(tmpArr2[2]) + '일' + parseInt(tmpArr1[0]) + '시 ' + parseInt(tmpArr1[1]) + '분';
						strDivOrgDlDdSpan = parseInt(tmpArr2[1]) + '월 ' + parseInt(tmpArr2[2]) + '일' + parseInt(tmpArr1[0]) + '시 ' + parseInt(tmpArr1[1]) + '분';

					}



					divOrgDlDdSpan.text(strDivOrgDlDdSpan);

					divOrgDlDd.append(divOrgDlDdSpan);

					divOrgDl.append(divOrgDlDt);

					divOrgDl.append(divOrgDlDd);

					divOrg.append(divOrgDl);



					var aTitle = jQuery("<a>").attr({'href': e.url, 'alt' : e.title});

					aTitle.append(e.title);

					var divText = jQuery("<div>").attr({'class': 'text'});

					divText.html('<a href="' + e.url + '">' + removeHtml(e.content) + '</a>');



					var divComment = jQuery("<div>").attr({'class': 'Comment'});

					var divCommentHidden = jQuery("<input>").attr({'class': 'daumBlogCommentHidden', 'type': 'hidden', 'value':e.postId});

					var divComGood = jQuery("<div>").attr({'class': 'com_good'}).text("댓글 : " + e.comments + " 건");

					var divComGro = jQuery("<div>").attr({'class': 'com_gro'});

					var divComMore = jQuery("<div>").attr({'class': 'com_more'});

					divComment.append(divCommentHidden);

					divComment.append(divComGood);

					divComment.append(divComGro);

					divComment.append(divComMore);



					divCen.append(divOrg);

					divCen.append(divText);

					divGroup.append(divCen);



					if ("" != contentImg) {

						var divImg = jQuery("<div>").attr({'class': 'Img'});

						divImg.append(contentImg);

						divGroup.append(divImg);

					}



					divGroup.append(divComment);

					// alert(divCen.html());

				}



				// alert(divGroup.html());

			}



			// 댓글목록 조회

			fnFetchDaumBlogComment();

		}

	});



	// 댓글목록 조회 함수

	var fnFetchDaumBlogComment = function() {

		var tmpUrl = 'https://apis.daum.net/blog/comment/list.do?blogName=kgpa&output=json&postId=';

		jQuery('input[class=daumBlogCommentHidden]').each(function(){

			var postId = jQuery(this).val();

			var divComment = jQuery(this).parent();

			var url = tmpUrl + postId;



			jQuery.ajax({

				url : url,

				type     : 'GET',

				dataType : 'jsonp',

				timeout  : 2000, //ms

				jsonp : 'callback', //오픈API 제공하는 콜백함수의 key 값을 쓴다.

				success : function(json) {

					if ("200" == json.channel.status && undefined != json.channel.item) {

						for (var i=0; i<json.channel.item.length; ++i) {

							var e = json.channel.item[i];

							var sHtml = '<span>' + e.name + '</span>' + e.content + ' <span class="co_go">' + fnParseDaumBlogDate(e.date) + '</span>';



							var divComGro = divComment.find('.com_gro');



							// 다음 블로그 댓글은 댓글작성자의 사진이 없어서 내용만 넣는다.

							jQuery('<div>').attr({'class':'gro'}).append(sHtml).appendTo(divComGro);

						}

					}

				}

			});

		});

	}

});