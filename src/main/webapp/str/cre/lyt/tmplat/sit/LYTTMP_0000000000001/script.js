/* 서브 페이지 2차 메뉴 이벤트 */
$(function(){
	//2차메뉴 클릭시 3차 메뉴 자동 열림
	$('ul.depth02 > li > a').click(function(){
		if($(this).parent().find('ul').hasClass('depth03')){
			$(this).parent().find('.depth03').toggle();
			return false;
		}
	});
	//현재 메뉴 자동 펼침
	$('ul.depth02 > li > a.on').parent().find('.depth03').show();
});

/* popupzone */
function popupzone(param, btn, obj, auto, f, s, p, h){
	var param = $(param);
	var btn = param.find(btn);
	var obj = param.find(obj);

	var stop = btn.find("button.mn_stop");
	var play = btn.find("button.mn_play");

	var returnNodes; // 버튼 이벤트를 위해 반복 명령 받아두기
	var elem = 0;
	var fade = f;
	var speed = s;

	// setup
	obj.hide().eq(0).show();
	obj.css({"position": "absolute", "top": "0", "left": "0"});

	// 초단위 반복
	function rotate(){
		returnNodes = setInterval(function(){
			obj.eq(elem).stop(true, true).fadeOut(fade);
			if(p == true) pbtn.removeClass("active");

			if(elem < obj.length - 1){
				elem++;
			}else{
				elem = 0;
			}

			obj.eq(elem).stop(true, true).fadeIn(fade);
			if(p == true) pbtn.eq(elem).addClass("active");
		}, speed);
	}

	//페이징
	if(p == true){
		var target = param.find(".paginate");

		var pbtn = target.find("a");

		pbtn.removeClass("active");
		pbtn.eq(elem).addClass("active");

		pbtn.click(function(){
			var t = $(this);
			clearInterval(returnNodes);
			pbtn.eq(elem).removeClass("active");
			obj.eq(elem).stop(false, true).fadeOut(f / 2);
			elem = t.index();
			pbtn.eq(elem).addClass("active");
			obj.eq(elem).stop(false, true).fadeIn(f / 2);
			return false;
		});
	}

	// href false
	btn.click(function(){
		return false;
	});

	// 팝업 갯수가 하나면 실행하지않습니다.
	if(obj.size() <= 1){
		btn.hide();
		return false;
	}

	// 멈춤
	btn.find("button.m_stop").click(function(){
		clearInterval(returnNodes);
	});

	// 시작
	btn.find("button.m_play").click(function(){
		clearInterval(returnNodes);
		rotate();
	});

	// 이전
	btn.find("button.m_prev").click(function(){
		obj.eq(elem).stop(false, true).fadeOut(f / 2);
		if(p == true) pbtn.eq(elem).removeClass("active");

		if(elem != 0){
			elem--;
		}else{
			elem = obj.length - 1;
		}

		obj.eq(elem).stop(false, true).fadeIn(f / 2);
		if(p == true) pbtn.eq(elem).addClass("active");
		if(play.css("display") == "none"){
			stop.hide();
			play.show();
		}
		clearInterval(returnNodes);
		rotate();
	});

	// 다음
	btn.find("button.m_next").click(function(){
		obj.eq(elem).stop(false, true).fadeOut(f / 2);
		if(p == true) pbtn.eq(elem).removeClass("active");

		if(elem < obj.length - 1){
			elem++;
		}else{
			elem = 0;
		}

		obj.eq(elem).stop(false, true).fadeIn(f / 2);
		if(p == true) pbtn.eq(elem).addClass("active");
		if(play.css("display") == "none"){
			stop.hide();
			play.show();
		}
		clearInterval(returnNodes);
		rotate();
	});

	if(auto != false){
		rotate();
	}

	if(h == true){
		play.hide();

		stop.click(function(){
			var t = $(this);
			t.hide();
			play.show();

			return false;
		});

		play.click(function(){
			var t = $(this);
			t.hide();
			stop.show();

			return false;
		});
	}
}

$(document).ready(function(){

	/* lnb */
	$("#lnb > ul > li > a").bind("mouseenter focusin", function(){
		$("#lnb > ul > li > a").removeClass('active');
		$("#lnb >ul > li ul").hide();
		$(this).addClass('active');
		$(this).next().show();
	});

	$("#header, #lnb").bind("mouseleave focusout", function(){
		$("#lnb >ul > li ul").hide();
		$("#lnb > ul > li > a").removeClass('active');
	});



	/* main_visul  */
	$(".visual_list li").eq(0).css("display", "block").siblings("li").css("display", "none");

	$(".thumb_img a").bind("mouseover focusin click", function(){
		var mvIdx1 = $(".thumb_img a").index(this);
		$(this).find("span").addClass("on").parent("a").siblings("a").find("span").removeClass("on");
		$(".visual_list li").eq(mvIdx1).fadeIn(1300).siblings("li").fadeOut(1300);
	});

	var mainVar1 = 0;
	var mainTimer1 = setInterval(rollBan, 5000);

	function rollBan(){
		if(mainVar1 < 3){
			mainVar1++;
			$(".thumb_img a").eq(mainVar1).find("span").addClass("on").parent("a").siblings("a").find("span").removeClass("on");
			$(".visual_list li").eq(mainVar1).fadeIn(1300).siblings("li").fadeOut(1300);
		}else{
			mainVar1 = 0;
			$(".thumb_img a").eq(mainVar1).find("span").addClass("on").parent("a").siblings("a").find("span").removeClass("on");
			$(".visual_list li").eq(mainVar1).fadeIn(1300).siblings("li").fadeOut(1300);
		}
	}
	$(".stop").click(function(){
		clearInterval(mainTimer1);
	});
	$(".play").click(function(){
		if(mainVar1 < 5){
		} else{
			mainTimer1 = setInterval(rollBan, 5000);
		}

	});

	$(".thumb_img a").hover(function(){
		clearInterval(mainTimer1);
	}, function(){
		mainTimer1 = setInterval(rollBan, 5000);
	});

	/* notice */
	var notice = (function(){
		var $btnPrev = $('.ctl .nprev'), $btnNext = $('.ctl .nnext'), $cur = 0;

		$('.ul_news').each(function(){
			var $this = $(this), $size = $this.find('li').size(), $target = $this.siblings('.npage').find('.total');
			$target.text($size);
		});

		$btnPrev.bind('click', function(){
			$(this).viewNews('left');
			return false;
		});

		$btnNext.bind('click', function(){
			$(this).viewNews('right');
			return false;
		});

		$.fn.viewNews = function(direct){
			this.each(function(){
				var $this = $(this), $btnParent = $this.closest('div.ctl'), $targetParent = $btnParent.siblings('.ul_news'), $target = $targetParent.find('li'), $curTxt = $btnParent.siblings('.npage').find('.current'), $size = $target.size();
				switch(direct){
					case 'left':
						$cur--;
						break;
					case 'right':
						$cur++;
						break;
				}
				if($cur < 0){
					$cur = $size - 1;
				}else if($cur > $size - 1){
					$cur = 0;
				}
				$curTxt.text($cur + 1);
				$target.removeClass('on');
				$target.eq($cur).addClass('on');
			})
		}
	}());


	//메인 페이지 팝업존 이벤트
	if($('.evt_zone').size() > 0 ){
		popupzone('.evt_zone', '.position', '.popupzone>li', true, 1000, 5000, true, true);
	}

	/* tab */
	$('.tab_box > .tabs > li.tabCts').click(function(){
		$('.tabCts').removeClass('active');
		$(this).addClass('active');
		$('.tab_cont').hide();
		$('.tab_cont:eq('+$(this).index()+')').show();
		return false;
	});

	/* tab2 */
	$('.tab_box_sub > .tab_content_sub').children().css('display', 'none');
	$('.tab_box_sub > .tab_content_sub div:first-child').css('display', 'block');
	$('.tab_box_sub .tab_sub li:first-child').addClass('active');

	$('.tab_box_sub').delegate('.tab_sub> li', 'click', function() {
		$(this).siblings().removeClass();
		$(this).addClass('active');
		$(this).parent().parent().next('.tab_content_sub').children().hide();
		var activeTab = $(this).find("a").attr("href");
		$(activeTab).show() ;
		return false;
	});

	/* 공지사항 본문 HTML 태그 삭제 */
	$('ul.ul_news li').each(function(idx, el){
		var p_str = $(el).find('p').text();
		var pattern = /<[^>]+>/g;
		p_str = p_str.replace(pattern, '');
		$(el).find('p').html(p_str);
	});

	/* 20150113 main_tab */
	$('.bbs_list_box .mtab_box:first').addClass('on')
	$('.bbs_list_box h3 a').click(function(){
		$('.bbs_list_box .mtab_box').removeClass('on');
		$(this).parent().parent().addClass('on');
		return false;
	});

	$('.sns_list_box .mtab_box:first').addClass('on')
	/*$('.sns_list_box h3 a').click(function(){
		$('.sns_list_box .mtab_box').removeClass('on');
		$(this).parent().parent().addClass('on');
		return false;
	});*/
});


/**
 * 녹색자료실 > 2006~2009 팝업 레이어
 * @param el
 * @param target
 * @param gn_title
 * @param gn_img
 * @param gn_file
 * @param gn_down
 * @returns {boolean}
 */
function fnLayerOpen(el, target, gn_title, gn_img, gn_file, gn_down){
	var temp = $('#' + target);
	var bg = temp.prev().hasClass('bg');
	var gn_sort = $(el).parent().parent().parent().prev().text();

	if(bg){
		$('.layer').fadeIn();
		temp.fadeIn();
	}else{
		temp.fadeIn();
	}
	$(temp).focus();

	if (Number(temp.outerHeight()) < Number($(document).height())){
		temp.css('margin-top', '-'+temp.outerHeight()/2+'px');
	}else{
		temp.css('top', '0px');
	}
	if(Number(temp.outerWidth()) < Number($(document).width())){
		temp.css('margin-left', '-'+temp.outerWidth()/2+'px');
	}else{
		temp.css('left', '0px');
	}

	$('.pclose, .layer .bg').click(function(e){
		if(bg){
			$('.layer').fadeOut();
			$(el).focus();
		}else{
			temp.fadeOut();
			$(el).focus();
		}
		e.preventDefault();
	});

	$('.gn_sort').text(gn_sort);
	$('.gn_title').text(gn_title);
	$('.gn_img').attr('src', gn_img);
	$('.gn_img').attr('alt', gn_title + ' 표지');
	$('.gn_down').text(gn_down);
	if(gn_down.toLowerCase().indexOf('pdf') > -1){
		$('.gn_down').attr('href', gn_file);
	}else{
		$('.gn_down').attr('href', '/attachfiles/gds/'+gn_down);
	}
	return false;
}