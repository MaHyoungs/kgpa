$(function(){
	//Menu 1Control
	$('#lnb > ul > li > a').bind("mouseenter focusin", function(e) {
		$('#lnb ul li div.smnu').hide();
		$('#lnb > ul > li > a').removeClass('active');
		$('div.smnu a').removeClass('active');	
		$(this).addClass('active');
		
		if($(this).parent().find('div.smnu').size() > 0){
			$("#header").stop().animate({"height":108},400);
			$(this).parent().find('div.smnu').show();
		}else{
			$("#header").stop().animate({"height":60},400);
		}
	});

    $('#lnb > ul > li > div.smnu ul > li > a').each(function(idx, el){
        if($(el).hasClass('active')){
            $("#header").stop().css({"height":108},400);
            $('div.smnu').prev().addClass('active');
            $('div.smnu').show();
        }
    });

	//top_move
	$('.btn_top button').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	
	//tab
	$('.tab_step li:last-child, .tab2 li:last-child').addClass('last');

	//area focus
	$('.area_box .ftxt').one('click focus', function(){
		$(this).hide();
		$(this).parent().find('textarea').focus();
	})
});



/* popupzone */
function popupzone(param,btn,obj,auto,f,s,p,h){
    var param = $(param);
    var btn = param.find(btn);
    var obj = param.find(obj);

    var stop = btn.find("button.mn_stop");
    var play = btn.find("button.mn_play");

    var returnNodes; // 버튼 이벤트를 위해 반복 명령 받아두기
    var elem = 0;
    var fade = f;
    var speed = s;

    // setup
    obj.hide().eq(0).show();
    obj.css({"position":"absolute","top":"0","left":"0"});
    
    // 초단위 반복
    function rotate(){
        returnNodes = setInterval(function(){
            obj.eq(elem).stop(true,true).fadeOut(fade);
            if(p==true) pbtn.removeClass("active");
    
            if(elem<obj.length-1){
                elem++;
            }else{
                elem = 0;
            }

            obj.eq(elem).stop(true,true).fadeIn(fade);
            if(p==true) pbtn.eq(elem).addClass("active");
        },speed);
    }

    //페이징
    if(p==true){
        var target = param.find(".paginate");

        var pbtn = target.find("a");

        pbtn.removeClass("active");
        pbtn.eq(elem).addClass("active");

        pbtn.click(function(){
            var t = $(this);
            clearInterval(returnNodes);
            pbtn.eq(elem).removeClass("active");
            obj.eq(elem).stop(false,true).fadeOut(f/2);
            elem = t.index();
            pbtn.eq(elem).addClass("active");
            obj.eq(elem).stop(false,true).fadeIn(f/2);
            return false;
        });
    }
    
    // href false
    btn.click(function(){return false;});

    // 팝업 갯수가 하나면 실행하지않습니다.
    if(obj.size() <= 1 ){
        btn.hide();
        return false;
    }
    
    // 멈춤
    btn.find("button.m_stop").click(function(){
        clearInterval(returnNodes);
    });
    
    // 시작
    btn.find("button.m_play").click(function(){
        clearInterval(returnNodes);
        rotate();
    });
    
    // 이전
    btn.find("button.m_prev").click(function(){
        obj.eq(elem).stop(false,true).fadeOut(f/2);
        if(p==true) pbtn.eq(elem).removeClass("active");

        if(elem != 0){
            elem--;
        }else{
            elem = obj.length-1;    
        }

        obj.eq(elem).stop(false,true).fadeIn(f/2);
        if(p==true) pbtn.eq(elem).addClass("active");
        if(play.css("display") == "none"){
            stop.hide();
            play.show();
        }
        clearInterval(returnNodes);
        rotate();
    });
    
    // 다음
    btn.find("button.m_next").click(function(){
        obj.eq(elem).stop(false,true).fadeOut(f/2);
        if(p==true) pbtn.eq(elem).removeClass("active");
        
        if(elem<obj.length-1){
            elem++;
        }else{
            elem = 0;
        }
        
        obj.eq(elem).stop(false,true).fadeIn(f/2);
        if(p==true) pbtn.eq(elem).addClass("active");
        if(play.css("display") == "none"){
            stop.hide();
            play.show();
        }
        clearInterval(returnNodes);
        rotate();
    });

    if(auto != false){
        rotate();
    }
    
    if(h==true){
        play.hide();

        stop.click(function(){
        var t = $(this);
        t.hide();
        play.show();

        return false;
        });

        play.click(function(){
        var t = $(this);
        t.hide();
        stop.show();

        return false;
        });
    }
}