$(function(){	
	/* 1차 탭 */
	$('.open_mng a').click(function(){
		$('.open_mng dt').removeClass('on');
		$('.open_mng a').removeClass('on');
		$(this).parent().parent().parent().parent().children('dt').addClass('on');
		$(this).addClass('on');
		var onTab = $(this).attr("href");

		$(".mng_box01").load(onTab, function(){
			/* 2차 탭 */
			$('.tab_box > .tab_content').children().css('display', 'none');
			$('.tab_box > .tab_content div:first-child').css('display', 'block');
			$('.tab_box .tab li:first-child').addClass('active');
			$('.tab_box').delegate('.tab > li', 'click', function() {
				$(this).siblings().removeClass();
				$(this).addClass('active');
				$(this).parent().next('.tab_content').children().hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).show() ;
				return false;
			});


			if(onTab == "/attachfiles/open_mng/page01_tab08.html"){
				$(".mng_box02").load("/attachfiles/open_mng/page01_tab08_01.html", function(){
					mng_detail();		
				});
			}
			if(onTab == "/attachfiles/open_mng/page01_tab23.html"){
				$(".mng_box02").load("/attachfiles/open_mng/page01_tab23_01.html", function(){
					mng_detail();
				});
			}

		
			$('.open_mng_tab  a').click(function(){
				$('.open_mng_tab li').removeClass('active');
				$(this).parent().addClass('active');
				var onTab2 = $(this).attr("href");
				$(".mng_box02").load(onTab2, function(){
					mng_detail();		
				});
				return false;
			});
		});
		return false;
	});
	
	/* 복리후생비, 복리후생비8대항목 탭기능  */
	mng_detail= function() {
		$('.mng_detail_cont > div').hide();
		$('.mng_detail_link a').click(function(){
			$('.mng_detail_cont > div').hide();
			var onTab2 = $(this).attr("href");
			$(onTab2).show();
			return false;
		});
	}
});