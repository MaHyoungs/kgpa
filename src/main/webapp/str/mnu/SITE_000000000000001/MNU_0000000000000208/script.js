var map     = null;
var marker  = null;
$(function(){
	var geocoder    = new google.maps.Geocoder();
	var latlng      = new google.maps.LatLng(36.3568303,127.3883565);
	var mapOptions = {
		zoom        : 17,
		center      : latlng,
		mapTypeId   : google.maps.MapTypeId.ROADMAP
	};
	map     = new google.maps.Map(document.getElementById('map'), mapOptions);
	marker  = new google.maps.Marker({
		map         : map,
		position    : latlng,
		animation   : google.maps.Animation.BOUNCE,
		title       : '대전광역시 서구 둔산북로 121, 2층 산림청 녹색사업단'
	});
	google.maps.event.addListener(marker, 'click', fnInfoWindowOpen);
});

function fnInfoWindowOpen(){
	var contents    = '대전광역시 서구 둔산북로 121, 2층 산림청 녹색사업단';
	var infowindow  = new google.maps.InfoWindow({
		content         : contents ,
		maxWidth        : 180
	});
	infowindow.open(map, marker);
}