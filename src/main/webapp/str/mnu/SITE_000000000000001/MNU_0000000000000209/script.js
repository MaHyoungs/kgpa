var map     = null;
var marker  = null;
$(function(){
	var geocoder    = new google.maps.Geocoder();
	var latlng      = new google.maps.LatLng(36.06372,128.46659);
	var mapOptions = {
		zoom        : 17,
		center      : latlng,
		mapTypeId   : google.maps.MapTypeId.ROADMAP
	};
	map     = new google.maps.Map(document.getElementById('map'), mapOptions);
	marker  = new google.maps.Marker({
		map         : map,
		position    : latlng,
		animation   : google.maps.Animation.BOUNCE,
		title       : '경상북도 칠곡군 석적읍 성곡리 유학로 532길'
	});
	google.maps.event.addListener(marker, 'click', fnInfoWindowOpen);
});

function fnInfoWindowOpen(){
	var contents    = '경상북도 칠곡군 석적읍 성곡리 유학로 532길';
	var infowindow  = new google.maps.InfoWindow({
		content         : contents ,
		maxWidth        : 180
	});
	infowindow.open(map, marker);
}