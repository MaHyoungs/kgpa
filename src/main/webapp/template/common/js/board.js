var file_upload_size = 0;

ctgryObj = function(upperCtgryId, ctgryId, ctgryNm, ctgryLevel){
	this.upperCtgryId = upperCtgryId;
	this.ctgryId = ctgryId;
	this.ctgryNm = ctgryNm;
	this.ctgryLevel = ctgryLevel;
};

function fnFindctgryObj(ctgryId){
	for(var i = 0; i < boardCateList.length; i++){
		var cate = boardCateList[i];
		if(cate.ctgryId == ctgryId){
			return cate;
		}
	}

	return null;
}

function fnCtgryChange(ctgryLevel){
	var actObj = document.getElementById("ctgry" + ctgryLevel);
	var targetObj = document.getElementById("ctgry" + (ctgryLevel + 1));
	if(targetObj != null){
		for(var cmIdx = ctgryLevel + 1; cmIdx <= boardCateLevel; cmIdx++){
			var cmObj = document.getElementById("ctgry" + cmIdx);
			if(cmObj != null){
				for(var i = cmObj.length - 1; i > 0; i--){
					cmObj.options.remove(i);
				}
			}
		}

		var currVal = actObj.options[actObj.selectedIndex].value;
		var currCtgryObj = fnFindctgryObj(currVal);
		if(currCtgryObj != null){
			var pos = 1;
			for(var i = 0; i < boardCateList.length; i++){
				var cate = boardCateList[i];
				if(cate.upperCtgryId == currCtgryObj.ctgryId && cate.ctgryLevel == ctgryLevel + 1){
					targetObj.options[pos] = new Option(cate.ctgryNm, cate.ctgryId);
					pos++;
				}
			}
		}
	}
}

function fnCtgryInit(searchCateList){
	var arr = searchCateList.replace('[', '').replace(']', '').split(",");
	for(var i = 0; i < arr.length; i++){
		if(arr[i].trim() != ''){
			var cmObj = document.getElementById("ctgry" + (i + 1));
			if(cmObj != null){
				for(var y = 0; y < cmObj.length; y++){
					if(cmObj[y].value == arr[i].trim()){
						cmObj[y].selected = true;
						fnCtgryChange(i + 1);
						break;
					}
				}
			}
		}
	}
}

String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g, "");
};

function fn_egov_SelectBoxValue(sbName){
	var FValue = "";
	for(var i = 0; i < document.getElementById(sbName).length; i++){
		if(document.getElementById(sbName).options[i].selected == true){

			FValue = document.getElementById(sbName).options[i].value;
		}
	}

	return  FValue;
}

function fn_egov_bbs_basic_regist(frm){

	return validateBoard(frm);
}

function fn_egov_bbs_editor(adfile_config){
	var context = ""; //"/cait";
	$('#' + adfile_config.editorId).tinymce({
		script_url                       : "/lib/tiny_mce/tiny_mce.js",
		language                         : "ko",
		theme                            : "advanced",
		skin                             : "o2k7",
		skin_variant                     : "silver",
		plugins                          : "autolink,lists,table,advhr,advimage,advlink,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,advlist,advfile",
		//theme_advanced_buttons1 : "img,|,attach",//"image,file,media",
		theme_advanced_buttons2          : "code,|,fullscreen,|,preview,|,print,|,newdocument,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,link,unlink,cleanup,|,tablecontrols",
		theme_advanced_buttons3          : "fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,outdent,indent,|,charmap,hr,removeformat,visualaid,|,sub,sup,|,ltr,rtl",
		theme_advanced_toolbar_location  : "top",
		theme_advanced_toolbar_align     : "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing          : true,
		theme_advanced_fonts             : "굴림=굴림;굴림체=굴림체;궁서=궁서;궁서체=궁서체;돋움=돋움;돋움체=돋움체;바탕=바탕;바탕체=바탕체;Arial=Arial; Comic Sans MS='Comic Sans MS';Courier New='Courier New';Tahoma=Tahoma;Times New Roman='Times New Roman';Verdana=Verdana",
		content_css                      : context + "/template/manage/css/default.css",
		template_external_list_url       : context + "/template/manage/docTemplate/mpm/template_list.js",
		convert_urls                     : false,
		forced_root_block                : false,
		adfile_external_param            : adfile_config,
		setup                            : function(ed){
			ed.addButton('img', {
				title  : '이미지 삽입/편집',
				image  : context + "/lib/tiny_mce/img/icon_01.gif",
				onclick: function(){
					ed.execCommand('mceAdvImage');
				}
			});
			ed.addButton('attach', {
				title  : '파일 첨부',
				image  : context + "/lib/tiny_mce/img/icon_03.gif",
				onclick: function(){
					ed.execCommand('mceAdvFile');
				}
			});
		}
	});

	$('#btnAddFile_' + adfile_config.editorId).show();
	$('#btnAddFile_' + adfile_config.editorId).click(function(){
		tinyMCE.activeEditor.windowManager.open({
			file  : context + "/lib/tiny_mce/plugins/advfile/file.htm",
			width : 500 + parseInt(tinyMCE.activeEditor.getLang('advfile.delta_width', 0)),
			height: 460 + parseInt(tinyMCE.activeEditor.getLang('advfile.delta_height', 0)),
			inline: 1
		}, {
			plugin_url : context + "/lib/tiny_mce/plugins/advfile",
			window     : window,
			insert_html: false
		});
		return false;
	});

	fn_egov_file_clean_action(adfile_config.editorId);

	$('#nttSj').focus();
}

/**
 * 파일 업로드 기본 세팅
 */
$(function(){
	$('#lblCurrCount_nttCn').text($('#fileCurrCount_nttCn').val());
	$('#currCount').val($('#fileCurrCount_nttCn').val());
	$('#currSize').val(Number($('#fileCurrSize_nttCn').val()));
});

/**
 * 파일 업로드 완료 fn
 * @param json
 */
function fnAjaxFileUploadComplete(json){
	var tr = $('<tr id="' + json.fileVo.atchFileId + '_' + json.fileVo.fileSn + '" class="db"></tr>');
	var td_1 = $('<td><img src="/template/manage/images/ico_file.gif" alt="파일"> ' + json.fileVo.orignlFileNm + '</td>');
	var td_2 = $('<td class="size">' + json.fileVo.fileMgByByteConvert + '</td>');
	var td_3 = $('<td class="del"></td>');
	var delete_a = $('<a href="" onclick="fnAjaxTmprFileDel(\'' + json.fileVo.tmprFileId + '\', this);return false;"><img src="/template/manage/images/btn_sdelete.gif"></a>');

	td_3.append(delete_a);
	tr.append(td_1);
	tr.append(td_2);
	tr.append(td_3);

	if($('.file_list_chart tbody tr').size() == 1){
		$('.file_list_chart tbody tr:eq(0)').hide();
	}
	$('.file_list_chart tbody').append(tr);

	setTimeout(function(){
		$('.progress').hide();
		$('.progress').width("0%");
		$('.bar').html("0%");
	}, 1000);

	$('#fileCurrCount_nttCn').val(json.totalFileCount);
	$('#fileCurrSize_nttCn').val(json.totalFileMg);
	$('#lblCurrCount_nttCn').text(json.totalFileCount);
	$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalFileMg) / 1024 / 1024, 1) + "MB");
	$('#fileGroupId').val(json.fileVo.atchFileId);
	$('#currCount').val(json.totalFileCount);
	$('#currSize').val(json.totalFileMg);
}

/**
 * 파일 그룹 아이디 키값이 없을때 자동으로 불러오기
 */
$(function(){
	$('#lblMaxCount_nttCn').text($('#posblAtchFileNumber_nttCn').val());
	$('#lblCurrSize_nttCn').text(roundXL(Number($('#fileCurrSize_nttCn').val()) / 1024 / 1024, 1) + "MB");
	$('#lblMaxSize_nttCn').text($('#posblAtchFileSize_nttCn').val() / 1024 / 1024 + "MB");
	$('#maxCount').val($('#posblAtchFileNumber_nttCn').val());
	$('#maxSize').val($('#posblAtchFileSize_nttCn').val());
});

/**
 * 파일 업로드 이벤트
 */
function fnAjaxFileUploadChangeEvent(){
	if($('#boardFileAjaxForm .file_upload_size').val() == ""){
		$('#boardFileAjaxForm .file_upload_size').val(0);
	}
	try{
		$('#gatchFileId').val($('#fileGroupId').val());
	}catch(exception){
		alert(exception);
	}finally{
		$('#boardFileAjaxForm').submit();
	}
}

/**
 * 파일 업로드 클릭
 */
function fnAjaxFileUploadClick(){
	var progress = $('.progress');
	var bar = $('.bar');
	progress.hide();
	progress.width("0%");
	bar.html("0%");
	$('#uploadfile').click();
}

/**
 * 파일 삭제
 * @param pk
 */
function fnAjaxTmprFileDel(pk, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = '/cmm/fms/ajaxBoardFileDelete.do';
		var data = [];
		data = {tmprFileId: pk};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount_nttCn').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}

function fnAjaxFileDel(atchFileId, fileSn, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = $(el).attr('href');
		var data = [];
		data = {atchFileId: atchFileId, fileSn: fileSn};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount_nttCn').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}