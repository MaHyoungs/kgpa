/**
 * 파일 업로드 기본 세팅
 */
$(function(){
	$('#lblMaxCount').text($('#maxCount').val());
	$('#lblCurrCount').text($('#fileCurrCount').val());
	$('#lblCurrSize').text(roundXL(Number($('#fileCurrSize').val()) / 1024 / 1024, 1) + "MB");
	$('#lblMaxSize').text(Number($('#maxSize').val()) / 1024 / 1024 + "MB");
	$('#currCount').val($('#fileCurrCount').val());
	$('#currSize').val(Number($('#fileCurrSize').val()));
});

/**
 * 파일 업로드 완료 fn
 * @param json
 */
function fnAjaxFileUploadComplete(json){
	var tr = $('<tr id="' + json.fileVo.atchFileId + '_' + json.fileVo.fileSn + '" class="db"></tr>');
	var td_1 = $('<td><img src="/template/manage/images/ico_file.gif" alt="파일"> ' + json.fileVo.orignlFileNm + '</td>');
	var td_2 = $('<td class="size">' + json.fileVo.fileMgByByteConvert + '</td>');
	var td_3 = $('<td class="del"></td>');
	var delete_a = $('<a href="" onclick="fnAjaxTmprFileDel(\'' + json.fileVo.tmprFileId + '\', this);return false;"><img src="/template/manage/images/btn_sdelete.gif"></a>');

	td_3.append(delete_a);
	tr.append(td_1);
	tr.append(td_2);
	tr.append(td_3);

	if($('.file_list_chart tbody tr').size() == 1){
		$('.file_list_chart tbody tr:eq(0)').hide();
	}
	$('.file_list_chart tbody').append(tr);

	setTimeout(function(){
		$('.progress').hide();
		$('.progress').width("0%");
		$('.bar').html("0%");
	}, 1000);

	$('#fileCurrCount').val(json.totalFileCount);
	$('#fileCurrSize').val(json.totalFileMg);
	$('#lblCurrCount').text(json.totalFileCount);
	$('#lblCurrSize').text(roundXL(Number(json.totalFileMg) / 1024 / 1024, 1) + "MB");
	$('#atch_file_id').val(json.fileVo.atchFileId);
	$('#currCount').val(json.totalFileCount);
	$('#currSize').val(json.totalFileMg);
}

/**
 * 파일 업로드 이벤트
 */
function fnAjaxFileUploadChangeEvent(){
	if($('#boardFileAjaxForm .file_upload_size').val() == ""){
		$('#boardFileAjaxForm .file_upload_size').val(0);
	}
	try{
		$('#gatchFileId').val($('#atch_file_id').val());
	}catch(exception){
		alert(exception);
	}finally{
		$('#boardFileAjaxForm').submit();
	}
}

/**
 * 파일 업로드 클릭
 */
function fnAjaxFileUploadClick(){
	var progress = $('.progress');
	var bar = $('.bar');
	progress.hide();
	progress.width("0%");
	bar.html("0%");
	$('#uploadfile').click();
}

/**
 * 파일 삭제
 * @param pk
 */
function fnAjaxTmprFileDel(pk, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = '/cmm/fms/ajaxBoardFileDelete.do';
		var data = [];
		data = {tmprFileId: pk};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount').text(json.totalInfoVO.totalFileCount);
			$('#fileCurrCount').val(json.totalInfoVO.totalFileCount);
			$('#currCount').val(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}

function fnAjaxFileDel(atchFileId, fileSn, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = $(el).attr('href');
		var data = [];
		data = {atchFileId: atchFileId, fileSn: fileSn};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount').text(json.totalInfoVO.totalFileCount);
			$('#currCount').val(json.totalInfoVO.totalFileCount);
			$('#fileCurrCount').val(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}