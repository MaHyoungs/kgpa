function signguArr() {
	var signguArr = [ 
	                 	{ctprvn:'강원도', signgu:'강릉시'}
					  , {ctprvn:'강원도', signgu:'고성군'}
					  , {ctprvn:'강원도', signgu:'동해시'}
					  , {ctprvn:'강원도', signgu:'삼척시'}
					  , {ctprvn:'강원도', signgu:'속초시'}
					  , {ctprvn:'강원도', signgu:'양구군'}
					  , {ctprvn:'강원도', signgu:'양양군'}
					  , {ctprvn:'강원도', signgu:'영월군'}
					  , {ctprvn:'강원도', signgu:'원주시'}
					  , {ctprvn:'강원도', signgu:'인제군'}
					  , {ctprvn:'강원도', signgu:'정선군'}
					  , {ctprvn:'강원도', signgu:'철원군'}
					  , {ctprvn:'강원도', signgu:'춘천시'}
					  , {ctprvn:'강원도', signgu:'태백시'}
					  , {ctprvn:'강원도', signgu:'평창군'}
					  , {ctprvn:'강원도', signgu:'홍천군'}
					  , {ctprvn:'강원도', signgu:'화천군'}
					  , {ctprvn:'강원도', signgu:'횡성군'}
					  
					  , {ctprvn:'경기도', signgu:'가평군'}
					  , {ctprvn:'경기도', signgu:'고양시덕양구'}
					  , {ctprvn:'경기도', signgu:'고양시일산동구'}
					  , {ctprvn:'경기도', signgu:'고양시일산서구'}
					  , {ctprvn:'경기도', signgu:'과천시'}
					  , {ctprvn:'경기도', signgu:'광명시'}
					  , {ctprvn:'경기도', signgu:'광주시'}
					  , {ctprvn:'경기도', signgu:'구리시'}
					  , {ctprvn:'경기도', signgu:'군포시'}
					  , {ctprvn:'경기도', signgu:'김포시'}
					  , {ctprvn:'경기도', signgu:'남양주시'}
					  , {ctprvn:'경기도', signgu:'동두천시'}
					  , {ctprvn:'경기도', signgu:'부천시소사구'}
					  , {ctprvn:'경기도', signgu:'부천시오정구'}
					  , {ctprvn:'경기도', signgu:'부천시원미구'}
					  , {ctprvn:'경기도', signgu:'성남시분당구'}
					  , {ctprvn:'경기도', signgu:'성남시수정구'}
					  , {ctprvn:'경기도', signgu:'성남시중원구'}
					  , {ctprvn:'경기도', signgu:'수원시권선구'}
					  , {ctprvn:'경기도', signgu:'수원시영통구'}
					  , {ctprvn:'경기도', signgu:'수원시장안구'}
					  , {ctprvn:'경기도', signgu:'수원시팔달구'}
					  , {ctprvn:'경기도', signgu:'시흥시'}
					  , {ctprvn:'경기도', signgu:'안산시단원구'}
					  , {ctprvn:'경기도', signgu:'안산시상록구'}
					  , {ctprvn:'경기도', signgu:'안성시'}
					  , {ctprvn:'경기도', signgu:'안양시동안구'}
					  , {ctprvn:'경기도', signgu:'안양시만안구'}
					  , {ctprvn:'경기도', signgu:'양주시'}
					  , {ctprvn:'경기도', signgu:'양평군'}
					  , {ctprvn:'경기도', signgu:'여주시'}
					  , {ctprvn:'경기도', signgu:'연천군'}
					  , {ctprvn:'경기도', signgu:'오산시'}
					  , {ctprvn:'경기도', signgu:'용인시기흥구'}
					  , {ctprvn:'경기도', signgu:'용인시수지구'}
					  , {ctprvn:'경기도', signgu:'용인시처인구'}
					  , {ctprvn:'경기도', signgu:'의왕시'}
					  , {ctprvn:'경기도', signgu:'의정부시'}
					  , {ctprvn:'경기도', signgu:'이천시'}
					  , {ctprvn:'경기도', signgu:'파주시'}
					  , {ctprvn:'경기도', signgu:'평택시'}
					  , {ctprvn:'경기도', signgu:'포천시'}
					  , {ctprvn:'경기도', signgu:'하남시'}
					  , {ctprvn:'경기도', signgu:'화성시'}
					  
					 ,{ctprvn:'경상남도',signgu:'거제시'}
					 ,{ctprvn:'경상남도',signgu:'거창군'}
					 ,{ctprvn:'경상남도',signgu:'고성군'}
					 ,{ctprvn:'경상남도',signgu:'김해시'}
					 ,{ctprvn:'경상남도',signgu:'남해군'}
					 ,{ctprvn:'경상남도',signgu:'밀양시'}
					 ,{ctprvn:'경상남도',signgu:'사천시'}
					 ,{ctprvn:'경상남도',signgu:'산청군'}
					 ,{ctprvn:'경상남도',signgu:'양산시'}
					 ,{ctprvn:'경상남도',signgu:'의령군'}
					 ,{ctprvn:'경상남도',signgu:'진주시'}
					 ,{ctprvn:'경상남도',signgu:'창녕군'}
					 ,{ctprvn:'경상남도',signgu:'창원시마산합포구'}
					 ,{ctprvn:'경상남도',signgu:'창원시마산회원구'}
					 ,{ctprvn:'경상남도',signgu:'창원시성산구'}
					 ,{ctprvn:'경상남도',signgu:'창원시의창구'}
					 ,{ctprvn:'경상남도',signgu:'창원시진해구'}
					 ,{ctprvn:'경상남도',signgu:'통영시'}
					 ,{ctprvn:'경상남도',signgu:'하동군'}
					 ,{ctprvn:'경상남도',signgu:'함안군'}
					 ,{ctprvn:'경상남도',signgu:'함양군'}
					 ,{ctprvn:'경상남도',signgu:'합천군'}
					 
					 ,{ctprvn:'경상북도',signgu:'경산시'}
					 ,{ctprvn:'경상북도',signgu:'경주시'}
					 ,{ctprvn:'경상북도',signgu:'고령군'}
					 ,{ctprvn:'경상북도',signgu:'구미시'}
					 ,{ctprvn:'경상북도',signgu:'군위군'}
					 ,{ctprvn:'경상북도',signgu:'김천시'}
					 ,{ctprvn:'경상북도',signgu:'문경시'}
					 ,{ctprvn:'경상북도',signgu:'봉화군'}
					 ,{ctprvn:'경상북도',signgu:'상주시'}
					 ,{ctprvn:'경상북도',signgu:'성주군'}
					 ,{ctprvn:'경상북도',signgu:'안동시'}
					 ,{ctprvn:'경상북도',signgu:'영덕군'}
					 ,{ctprvn:'경상북도',signgu:'영양군'}
					 ,{ctprvn:'경상북도',signgu:'영주시'}
					 ,{ctprvn:'경상북도',signgu:'영천시'}
					 ,{ctprvn:'경상북도',signgu:'예천군'}
					 ,{ctprvn:'경상북도',signgu:'울릉군'}
					 ,{ctprvn:'경상북도',signgu:'울진군'}
					 ,{ctprvn:'경상북도',signgu:'의성군'}
					 ,{ctprvn:'경상북도',signgu:'청도군'}
					 ,{ctprvn:'경상북도',signgu:'청송군'}
					 ,{ctprvn:'경상북도',signgu:'칠곡군'}
					 ,{ctprvn:'경상북도',signgu:'포항시남구'}
					 ,{ctprvn:'경상북도',signgu:'포항시북구'}
                     
					 ,{ctprvn:'광주광역시',signgu:'광산구'}
					 ,{ctprvn:'광주광역시',signgu:'남구'}
					 ,{ctprvn:'광주광역시',signgu:'동구'}
					 ,{ctprvn:'광주광역시',signgu:'북구'}
					 ,{ctprvn:'광주광역시',signgu:'서구'}
					 
					 ,{ctprvn:'대구광역시',signgu:'남구'}
					 ,{ctprvn:'대구광역시',signgu:'달서구'}
					 ,{ctprvn:'대구광역시',signgu:'달성군'}
					 ,{ctprvn:'대구광역시',signgu:'동구'}
					 ,{ctprvn:'대구광역시',signgu:'북구'}
					 ,{ctprvn:'대구광역시',signgu:'서구'}
					 ,{ctprvn:'대구광역시',signgu:'수성구'}
					 ,{ctprvn:'대구광역시',signgu:'중구'}
					 
					 ,{ctprvn:'대전광역시',signgu:'대덕구'}
					 ,{ctprvn:'대전광역시',signgu:'동구'}
					 ,{ctprvn:'대전광역시',signgu:'서구'}
					 ,{ctprvn:'대전광역시',signgu:'유성구'}
					 ,{ctprvn:'대전광역시',signgu:'중구'}
					 
					 ,{ctprvn:'부산광역시',signgu:'강서구'}
					 ,{ctprvn:'부산광역시',signgu:'금정구'}
					 ,{ctprvn:'부산광역시',signgu:'기장군'}
					 ,{ctprvn:'부산광역시',signgu:'남구'}
					 ,{ctprvn:'부산광역시',signgu:'동구'}
					 ,{ctprvn:'부산광역시',signgu:'동래구'}
					 ,{ctprvn:'부산광역시',signgu:'부산진구'}
					 ,{ctprvn:'부산광역시',signgu:'북구'}
					 ,{ctprvn:'부산광역시',signgu:'사상구'}
					 ,{ctprvn:'부산광역시',signgu:'사하구'}
					 ,{ctprvn:'부산광역시',signgu:'서구'}
					 ,{ctprvn:'부산광역시',signgu:'수영구'}
					 ,{ctprvn:'부산광역시',signgu:'연제구'}
					 ,{ctprvn:'부산광역시',signgu:'영도구'}
					 ,{ctprvn:'부산광역시',signgu:'중구'}
					 ,{ctprvn:'부산광역시',signgu:'해운대구'}
					 
					 ,{ctprvn:'서울특별시',signgu:'강남구'}
					 ,{ctprvn:'서울특별시',signgu:'강동구'}
					 ,{ctprvn:'서울특별시',signgu:'강북구'}
					 ,{ctprvn:'서울특별시',signgu:'강서구'}
					 ,{ctprvn:'서울특별시',signgu:'관악구'}
					 ,{ctprvn:'서울특별시',signgu:'광진구'}
					 ,{ctprvn:'서울특별시',signgu:'구로구'}
					 ,{ctprvn:'서울특별시',signgu:'금천구'}
					 ,{ctprvn:'서울특별시',signgu:'노원구'}
					 ,{ctprvn:'서울특별시',signgu:'도봉구'}
					 ,{ctprvn:'서울특별시',signgu:'동대문구'}
					 ,{ctprvn:'서울특별시',signgu:'동작구'}
					 ,{ctprvn:'서울특별시',signgu:'마포구'}
					 ,{ctprvn:'서울특별시',signgu:'서대문구'}
					 ,{ctprvn:'서울특별시',signgu:'서초구'}
					 ,{ctprvn:'서울특별시',signgu:'성동구'}
					 ,{ctprvn:'서울특별시',signgu:'성북구'}
					 ,{ctprvn:'서울특별시',signgu:'송파구'}
					 ,{ctprvn:'서울특별시',signgu:'양천구'}
					 ,{ctprvn:'서울특별시',signgu:'영등포구'}
					 ,{ctprvn:'서울특별시',signgu:'용산구'}
					 ,{ctprvn:'서울특별시',signgu:'은평구'}
					 ,{ctprvn:'서울특별시',signgu:'종로구'}
					 ,{ctprvn:'서울특별시',signgu:'중구'}
					 ,{ctprvn:'서울특별시',signgu:'중랑구'}
					 
					 ,{ctprvn:'세종특별자치시',signgu:''}
					 
					 ,{ctprvn:'울산광역시',signgu:'남구'}
					 ,{ctprvn:'울산광역시',signgu:'동구'}
					 ,{ctprvn:'울산광역시',signgu:'북구'}
					 ,{ctprvn:'울산광역시',signgu:'울주군'}
					 ,{ctprvn:'울산광역시',signgu:'중구'}
					 
					 ,{ctprvn:'인천광역시',signgu:'강화군'}
					 ,{ctprvn:'인천광역시',signgu:'계양구'}
					 ,{ctprvn:'인천광역시',signgu:'남구'}
					 ,{ctprvn:'인천광역시',signgu:'남동구'}
					 ,{ctprvn:'인천광역시',signgu:'동구'}
					 ,{ctprvn:'인천광역시',signgu:'부평구'}
					 ,{ctprvn:'인천광역시',signgu:'서구'}
					 ,{ctprvn:'인천광역시',signgu:'연수구'}
					 ,{ctprvn:'인천광역시',signgu:'옹진군'}
					 ,{ctprvn:'인천광역시',signgu:'중구'}
					 
					 ,{ctprvn:'전라남도',signgu:'강진군'}
					 ,{ctprvn:'전라남도',signgu:'고흥군'}
					 ,{ctprvn:'전라남도',signgu:'곡성군'}
					 ,{ctprvn:'전라남도',signgu:'광양시'}
					 ,{ctprvn:'전라남도',signgu:'구례군'}
					 ,{ctprvn:'전라남도',signgu:'나주시'}
					 ,{ctprvn:'전라남도',signgu:'담양군'}
					 ,{ctprvn:'전라남도',signgu:'목포시'}
					 ,{ctprvn:'전라남도',signgu:'무안군'}
					 ,{ctprvn:'전라남도',signgu:'보성군'}
					 ,{ctprvn:'전라남도',signgu:'순천시'}
					 ,{ctprvn:'전라남도',signgu:'신안군'}
					 ,{ctprvn:'전라남도',signgu:'여수시'}
					 ,{ctprvn:'전라남도',signgu:'영광군'}
					 ,{ctprvn:'전라남도',signgu:'영암군'}
					 ,{ctprvn:'전라남도',signgu:'완도군'}
					 ,{ctprvn:'전라남도',signgu:'장성군'}
					 ,{ctprvn:'전라남도',signgu:'장흥군'}
					 ,{ctprvn:'전라남도',signgu:'진도군'}
					 ,{ctprvn:'전라남도',signgu:'함평군'}
					 ,{ctprvn:'전라남도',signgu:'해남군'}
					 ,{ctprvn:'전라남도',signgu:'화순군'}
					 
					 ,{ctprvn:'전라북도',signgu:'고창군'}
					 ,{ctprvn:'전라북도',signgu:'군산시'}
					 ,{ctprvn:'전라북도',signgu:'김제시'}
					 ,{ctprvn:'전라북도',signgu:'남원시'}
					 ,{ctprvn:'전라북도',signgu:'무주군'}
					 ,{ctprvn:'전라북도',signgu:'부안군'}
					 ,{ctprvn:'전라북도',signgu:'순창군'}
					 ,{ctprvn:'전라북도',signgu:'완주군'}
					 ,{ctprvn:'전라북도',signgu:'익산시'}
					 ,{ctprvn:'전라북도',signgu:'임실군'}
					 ,{ctprvn:'전라북도',signgu:'장수군'}
					 ,{ctprvn:'전라북도',signgu:'전주시덕진구'}
					 ,{ctprvn:'전라북도',signgu:'전주시완산구'}
					 ,{ctprvn:'전라북도',signgu:'정읍시'}
					 ,{ctprvn:'전라북도',signgu:'진안군'}
					 
					 ,{ctprvn:'제주특별자치도',signgu:'서귀포시'}
					 ,{ctprvn:'제주특별자치도',signgu:'제주시'}
					 
					 ,{ctprvn:'충청남도',signgu:'계룡시'}
					 ,{ctprvn:'충청남도',signgu:'공주시'}
					 ,{ctprvn:'충청남도',signgu:'금산군'}
					 ,{ctprvn:'충청남도',signgu:'논산시'}
					 ,{ctprvn:'충청남도',signgu:'당진시'}
					 ,{ctprvn:'충청남도',signgu:'보령시'}
					 ,{ctprvn:'충청남도',signgu:'부여군'}
					 ,{ctprvn:'충청남도',signgu:'서산시'}
					 ,{ctprvn:'충청남도',signgu:'서천군'}
					 ,{ctprvn:'충청남도',signgu:'아산시'}
					 ,{ctprvn:'충청남도',signgu:'예산군'}
					 ,{ctprvn:'충청남도',signgu:'천안시동남구'}
					 ,{ctprvn:'충청남도',signgu:'천안시서북구'}
					 ,{ctprvn:'충청남도',signgu:'청양군'}
					 ,{ctprvn:'충청남도',signgu:'태안군'}
					 ,{ctprvn:'충청남도',signgu:'홍성군'}
					 
					 ,{ctprvn:'충청북도',signgu:'괴산군'}
					 ,{ctprvn:'충청북도',signgu:'단양군'}
					 ,{ctprvn:'충청북도',signgu:'보은군'}
					 ,{ctprvn:'충청북도',signgu:'영동군'}
					 ,{ctprvn:'충청북도',signgu:'옥천군'}
					 ,{ctprvn:'충청북도',signgu:'음성군'}
					 ,{ctprvn:'충청북도',signgu:'제천시'}
					 ,{ctprvn:'충청북도',signgu:'증평군'}
					 ,{ctprvn:'충청북도',signgu:'진천군'}
					 ,{ctprvn:'충청북도',signgu:'청원군'}
					 ,{ctprvn:'충청북도',signgu:'청주시상당구'}
					 ,{ctprvn:'충청북도',signgu:'청주시흥덕구'}
					 ,{ctprvn:'충청북도',signgu:'충주시'}
			  	];
	
	
	return signguArr;
}


