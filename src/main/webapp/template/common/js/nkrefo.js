var file_upload_size = 0;

String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g, "");
};

/**
 * 파일 업로드 기본 세팅
 */
$(function(){
	$('#currCount').val($('#fileCurrCount_nttCn').val());
	$('#currSize').val(Number($('#fileCurrSize_nttCn').val()));
});


/**
 * 파일 업로드 완료 fn
 * @param json
 */
function fnAjaxFileUploadComplete(json){
	var tr = $('<tr id="' + json.fileVo.atchFileId + '_' + json.fileVo.fileSn + '" class="db"></tr>');
	var td_1 = $('<td><img src="/template/manage/images/ico_file.gif" alt="파일"> ' + json.fileVo.orignlFileNm + '</td>');
	var td_2 = $('<td class="size">' + json.fileVo.fileMgByByteConvert + '</td>');
	var td_3 = $('<td class="del"></td>');
	var td_4 = $('<td><input type="radio" name="dyn' + json.fileVo.fileSn + '" id="dyn' + json.fileVo.fileSn + '_yes" value="Y" cssClass="cho" onclick="fnAjaxTempFileDyn(\'' + json.fileVo.atchFileId + '\',\'' + json.fileVo.fileSn + '\',\'Y\',this);return false;"/> <label for="dyn' + json.fileVo.fileSn + '_yes">예</label> <input type="radio" name="dyn' + json.fileVo.fileSn + '" id="dyn' + json.fileVo.fileSn + '_no" value="N" cssClass="cho" checked="checked" onclick="fnAjaxTempFileDyn(\'' + json.fileVo.atchFileId + '\',\'' + json.fileVo.fileSn + '\',\'N\',this);return false;"/> <label for="dyn' + json.fileVo.fileSn + '_no">아니오</label></td>');
	var delete_a = $('<a href="" onclick="fnAjaxTmprFileDel(\'' + json.fileVo.tmprFileId + '\', this);return false;"><img src="/template/manage/images/btn_sdelete.gif"></a>');
	
	td_3.append(delete_a);
	tr.append(td_1);
	tr.append(td_2);
	tr.append(td_3);
	tr.append(td_4);

	if($('.file_list_chart tbody tr').size() == 1){
		$('.file_list_chart tbody tr:eq(0)').hide();
	}
	$('.file_list_chart tbody').append(tr);

	setTimeout(function(){
		$('.progress').hide();
		$('.progress').width("0%");
		$('.bar').html("0%");
	}, 1000);

	$('#fileCurrCount_nttCn').val(json.totalFileCount);
	$('#fileCurrSize_nttCn').val(json.totalFileMg);
	$('#lblCurrCount_nttCn').text(json.totalFileCount);
	$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalFileMg) / 1024 / 1024, 1) + "MB");
	$('#fileGroupId').val(json.fileVo.atchFileId);
	$('#currCount').val(json.totalFileCount);
	$('#currSize').val(json.totalFileMg);
}

/**
 * 파일 그룹 아이디 키값이 없을때 자동으로 불러오기
 */
$(function(){
	$('#lblMaxCount_nttCn').text($('#posblAtchFileNumber_nttCn').val());
	$('#lblMaxSize_nttCn').text($('#posblAtchFileSize_nttCn').val() / 1024 / 1024 + "MB");
	$('#lblCurrSize_nttCn').text(roundXL(Number($('#fileCurrSize_nttCn').val()) / 1024 / 1024, 1) + "MB");
	$('#lblCurrCount_nttCn').text($('#fileCurrCount_nttCn').val());
	
	$('#maxCount').val($('#posblAtchFileNumber_nttCn').val());
	$('#maxSize').val($('#posblAtchFileSize_nttCn').val());
});

/**
 * 파일 업로드 이벤트
 */
function fnAjaxFileUploadChangeEvent(){
	if($('#nkrefoFileAjaxForm .file_upload_size').val() == ""){
		$('#nkrefoFileAjaxForm .file_upload_size').val(0);
	}
	try{
		$('#gatchFileId').val($('#fileGroupId').val());
	}catch(exception){
		alert(exception);
	}finally{
		$('#nkrefoFileAjaxForm').submit();
	}
}

/**
 * 파일 업로드 클릭
 */
function fnAjaxFileUploadClick(){
	var progress = $('.progress');
	var bar = $('.bar');
	progress.hide();
	progress.width("0%");
	bar.html("0%");
	$('#uploadfile').click();
}

/**
 * 파일 삭제
 * @param pk
 */
function fnAjaxTmprFileDel(pk, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = '/cmm/fms/ajaxBoardFileDelete.do';
		var data = [];
		data = {tmprFileId: pk};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount_nttCn').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}

function fnAjaxFileDel(atchFileId, fileSn, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = $(el).attr('href');
		var data = [];
		data = {atchFileId: atchFileId, fileSn: fileSn};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart tbody tr').size() == 1){
				$('.file_list_chart tbody tr:eq(0)').show();
			}
			$('#lblCurrCount_nttCn').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize_nttCn').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
		};
		fn_ajax_json(url, data, successFn, null);
	}
}

function fnAjaxFileDyn(atchFileId, fileSn, downloadYn, el){
	var url = '/cmm/fms/updateFileInfDynByAjax.do';
	var data = [];
	data = {atchFileId: atchFileId, fileSn: fileSn, downloadYn: downloadYn};
	var successFn = function(json){
		$(el).prop('checked', true);
	};
	fn_ajax_json(url, data, successFn, null);
}

function fnAjaxTempFileDyn(atchFileId, fileSn, downloadYn, el){
	var url = '/cmm/fms/updateTempFileInfDynByAjax.do';
	var data = [];
	data = {atchFileId: atchFileId, fileSn: fileSn, downloadYn: downloadYn};
	var successFn = function(json){
		$(el).prop('checked', true);
	};
	fn_ajax_json(url, data, successFn, null);
}