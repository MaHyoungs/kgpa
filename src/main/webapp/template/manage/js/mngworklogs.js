$(function(){
	fnAjaxManagerWorkLogInsert();
	fnAjaxManagerWorkLogBoardListInsert();
	fnAjaxManagerWorkLogBoardViewInsert();
});


/**
 * 관리자 작업로그 목록 조회 Insert Ajax Function
 */
function fnAjaxManagerWorkLogInsert(){
	var menu_1st_nm = $('#mainMenu ul li a.slt').text();
	var menu_2nd_nm = "";
	var menu_3rd_nm = "";
	var menu_4th_nm = "";
	var menu_5th_nm = "";

	$('#leftMenu ul li a.slt').each(function(idx, el){
		switch(idx){
			case 0 :
				menu_2nd_nm = $(el).text();
				break;
			case 1 :
				menu_3rd_nm = $(el).text();
				break;
			case 2 :
				menu_4th_nm = $(el).text();
				break;
			case 3 :
				menu_5th_nm = $(el).text();
				break;
		}
	});

	var url = location.href;
	var work_ty = "MWC01";
	var action = '/mng/sym/log/ajaxInsertMngWorkLog.do';
	var data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
	var errorFn = function(xhr, status, error){};
	var successFn = function(json){};
	var currUrl = location.href.toLowerCase();

	//Action에 List 있을때
	if(
		currUrl.indexOf('list') > -1 &&
		currUrl.indexOf('selectboardlist') < 0 &&
		currUrl.indexOf('gfundnoticelist') < 0 &&
		currUrl.indexOf('gfundqnalist') < 0 &&
		currUrl.indexOf('selectopenboardlist') < 0 &&
		currUrl.indexOf('egovphotoinfolist') < 0 &&
		currUrl.indexOf('egovchcklistupdtview') < 0 &&
		currUrl.indexOf('egovprufpaperslist') < 0
	){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//회원관리 목록 조회
	if(currUrl.indexOf('egovmbermanage') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//게시물통계 목록 조회
	if(currUrl.indexOf('selectbbsstats') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//한반도생태복원자료실관리 목록 조회
	if(currUrl.indexOf('egovnkrefomanage') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//사후관리 목록 조회
	if(currUrl.indexOf('gfundpostmanagement') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//게시판관리 목록 조회
	if(currUrl.indexOf('selectbbsmasterinfs') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//일/월/년간 접속 > 홈페이지 목록 조회
	if(currUrl.indexOf('selectscrinstats') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//사이트관리, 템플릿관리, 레이아웃관리, 공통코드관리, 메뉴관리, 관리자관리, 카테고리관리, 게시판템플릿관리, 게시판소스관리, 녹색자료실06~09, 통합캘린더관리, 팝업존관리, 팝업관리, 사업공고 상세 조회
	if(
		currUrl.indexOf('selectsiteinfo') > -1 ||
		(currUrl.indexOf('selectlyttemplate') > -1 && currUrl.indexOf('selectlyttemplatelist') < 0) ||
		(currUrl.indexOf('selectlytsourc') > -1 && currUrl.indexOf('selectlytsourclist') < 0) ||
		(currUrl.indexOf('selectbbstemplate') > -1 && currUrl.indexOf('selectbbstemplatelist') < 0) ||
		(currUrl.indexOf('selectbbssourc') > -1 && currUrl.indexOf('selectbbssourclist') < 0) ||
		currUrl.indexOf('egovccmcmmncodemodify') > -1 ||
		currUrl.indexOf('egovuserselectupdtview') > -1 ||
		currUrl.indexOf('selectbbsctgrymaster') > -1 ||
		currUrl.indexOf('selectcomtngninfo') > -1 ||
		currUrl.indexOf('updatecomtnschdulinfoview') > -1 ||
		currUrl.indexOf('getbanner') > -1 ||
		currUrl.indexOf('updtpopup') > -1 ||
		currUrl.indexOf('announcementform') > -1 ||
		currUrl.indexOf('egovnkrefoselectview') > -1 ||
		currUrl.indexOf('forupdatempm') > -1
	){
		work_ty = "MWC02";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}


	//선정전형 상세조회
	else if(
		currUrl.indexOf('/proposalview') > -1 ||
		currUrl.indexOf('selection/finalproposalview') > -1 ||
		currUrl.indexOf('selectionstepform') > -1
	){
		menu_3rd_nm = $('.tab_step ul li.active a').text();
		work_ty = "MWC02";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}


	//사업관리 > 사진정보, 관련증빙서 목록조회
	else if(
		currUrl.indexOf('egovphotoinfolist') > -1 ||
		currUrl.indexOf('egovprufpaperslist') > -1
	){
		menu_3rd_nm = $('.tab2 ul li.active a').text();
		work_ty = "MWC01";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//사업관리 > 기본정보, 사업현황, 지도점검 상세조회
	else if(
		currUrl.indexOf('businessmng/finalproposalview') > -1 ||
		currUrl.indexOf('egovplanacmsltupdtview') > -1 ||
		currUrl.indexOf('egovchcklistupdtview') > -1 ||
		currUrl.indexOf('selectionstepform') > -1
	){
		menu_3rd_nm = $('.tab2 ul li.active a').text();
		work_ty = "MWC02";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}


	//결과산출 > 최종서류제출
	else if(
		currUrl.indexOf('egovlastpaperslist') > -1
	){
		menu_3rd_nm = $('.tab2 ul li.active a').text();
		work_ty = "MWC01";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}

	//결과산출 > 최종정산, 평가결과
	else if(
		currUrl.indexOf('egovexcclcaddview') > -1 ||
		currUrl.indexOf('egovevaluationresultupdtview') > -1
	){
		menu_3rd_nm = $('.tab2 ul li.active a').text();
		work_ty = "MWC02";
		data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, menu_3rd_nm : menu_3rd_nm, menu_4th_nm : menu_4th_nm, menu_5th_nm : menu_5th_nm, url : url, work_ty : work_ty};
		fn_ajax_json(action, data, successFn, errorFn);
	}
}


/**
 * 관리자 작업로그 게시판 목록 조회 Insert Ajax Function
 */
function fnAjaxManagerWorkLogBoardListInsert(){
	var menu_1st_nm = "게시판관리";
	var menu_2nd_nm = "";

	var menu_2_str_arr = $('.naviTit').text().split(" > ");

	if(menu_2_str_arr.length != 1){
		for(var i =0; i<menu_2_str_arr.length; i++){
			if(menu_2_str_arr[i] == "정부3.0 정보공개"){
				menu_1st_nm = "정보공개관리";
				menu_2nd_nm = "정보목록";
			}else if(i == (menu_2_str_arr.length - 1)){
				menu_2nd_nm = menu_2_str_arr[i];
			}
		}
	}

	if(menu_2nd_nm == ""){
		menu_1st_nm = "녹색자금통합관리";
		menu_2nd_nm = $('.naviTit').text();
	}

	var url = location.href;
	var work_ty = "MWC01";
	var action = '/mng/sym/log/ajaxInsertMngWorkLog.do';
	var data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, url : url, work_ty : work_ty};
	var errorFn = function(xhr, status, error){};
	var successFn = function(json){};
	var currUrl = location.href.toLowerCase();

	if(currUrl.indexOf('selectboardlist') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}
}


/**
 * 관리자 작업로그 게시판 상세조회 Insert Ajax Function
 */
function fnAjaxManagerWorkLogBoardViewInsert(){
	var menu_1st_nm = "게시판관리";
	var menu_2nd_nm = "";

	var menu_2_str_arr = $('.naviTit').text().split(" > ");

	if(menu_2_str_arr.length != 1){
		for(var i =0; i<menu_2_str_arr.length; i++){
			if(menu_2_str_arr[i] == "정부3.0 정보공개"){
				menu_1st_nm = "정보공개관리";
				menu_2nd_nm = "정보목록";
			}else if(i == (menu_2_str_arr.length - 1)){
				menu_2nd_nm = menu_2_str_arr[i];
			}
		}
	}

	if(menu_2nd_nm == ""){
		menu_1st_nm = "녹색자금통합관리";
		menu_2nd_nm = $('.naviTit').text();
	}

	var url = location.href;
	var work_ty = "MWC02";
	var action = '/mng/sym/log/ajaxInsertMngWorkLog.do';
	var data = {menu_1st_nm : menu_1st_nm, menu_2nd_nm : menu_2nd_nm, url : url, work_ty : work_ty};
	var errorFn = function(xhr, status, error){};
	var successFn = function(json){};
	var currUrl = location.href.toLowerCase();

	if(currUrl.indexOf('selectboardarticle') > -1){
		fn_ajax_json(action, data, successFn, errorFn);
	}
}

