/*
 * 파일 업로드 기본 세팅
 */
$(function(){
	$('#lblMaxCount2').text($('#maxCount2').val());
	$('#lblCurrCount2').text($('#fileCurrCount2').val());
	$('#lblCurrSize2').text(roundXL(Number($('#fileCurrSize2').val()) / 1024 / 1024, 1) + "MB");
	$('#lblMaxSize2').text(Number($('#maxSize2').val()) / 1024 / 1024 + "MB");
	$('#currCount').val($('#fileCurrCount').val());
	$('#currSize').val(Number($('#fileCurrSize').val()));
	$('#currCount2').val($('#fileCurrCount2').val()); // 지도점검표는 1개의 파일만 업로드 가능
	$('#currSize2').val(Number($('#fileCurrSize2').val())); // 지도점검표는 1개의 파일만 업로드 가능
});

/*
 * 파일 업로드 완료 fn
 * @param json
 */
function fnAjaxFileUploadComplete2(json){
	var tr = $('<tr id="' + json.fileVo.atchFileId + '_' + json.fileVo.fileSn + '" class="db"></tr>');
	var td_1 = $('<td><img src="/template/manage/images/ico_file.gif" alt="파일"> ' + json.fileVo.orignlFileNm + '</td>');
	var td_2 = $('<td class="size">' + json.fileVo.fileMgByByteConvert + '</td>');
	var td_3 = $('<td class="del"></td>');
	var delete_a = $('<a href="" onclick="fnAjaxTmprFileDel2(\'' + json.fileVo.tmprFileId + '\', this);return false;"><img src="/template/manage/images/btn_sdelete.gif"></a>');

	td_3.append(delete_a);
	tr.append(td_1);
	tr.append(td_2);
	tr.append(td_3);

	if($('.file_list_chart2 tbody tr').size() == 1){
		$('.file_list_chart2 tbody tr:eq(0)').hide();
	}
	$('.file_list_chart2 tbody').append(tr);
	
	setTimeout(function(){
		$('.progress2').hide();
		$('.progress2').width("0%");
		$('.bar2').html("0%");
	}, 1000);
	
	$('#fileCurrCount2').val(json.totalFileCount);
	$('#fileCurrSize2').val(json.totalFileMg);
	$('#lblCurrCount2').text(json.totalFileCount);
	$('#lblCurrSize2').text(roundXL(Number(json.totalFileMg) / 1024 / 1024, 1) + "MB");
	$('#ctt_atch_file_id').val(json.fileVo.atchFileId);
	$('#currCount2').val(json.totalFileCount);
	$('#currSize2').val(json.totalFileMg);
}

/*
 * 파일 업로드 이벤트
 */
function fnAjaxFileUploadChangeEvent2(){
	if($('#chckFileAjaxForm .file_upload_size').val() == ""){
		$('#chckFileAjaxForm .file_upload_size').val(0);
	}
	try{
		$('#gcttAtchFileId').val($('#ctt_atch_file_id').val());
	}catch(exception){
		alert(exception);
	}finally{
		$('#chckFileAjaxForm').submit();
	}
}

/*
 * 파일 삭제
 * @param pk
 */
function fnAjaxTmprFileDel2(pk, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = '/cmm/fms/ajaxBoardFileDelete.do';
		var data = [];
		data = {tmprFileId: pk};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart2 tbody tr').size() == 1){
				$('.file_list_chart2 tbody tr:eq(0)').show();
			}
			$('#lblCurrCount2').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize2').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
			$('#currCount2').val(json.totalInfoVO.totalFileCount);
			$('#currSize2').val(json.totalInfoVO.totalFileMg);
		};
		fn_ajax_json(url, data, successFn, null);
	}
}

function fnAjaxFileDel2(atchFileId, fileSn, el){
	if(confirm("해당 파일을 삭제 하시겠습니까?")){
		var url = $(el).attr('href');
		var data = [];
		data = {atchFileId: atchFileId, fileSn: fileSn};
		var successFn = function(json){
			$(el).parent().parent().remove();
			if($('.file_list_chart2 tbody tr').size() == 1){
				$('.file_list_chart2 tbody tr:eq(0)').show();
			}
			$('#lblCurrCount2').text(json.totalInfoVO.totalFileCount);
			$('#lblCurrSize2').text(roundXL(Number(json.totalInfoVO.totalFileMg) / 1024 / 1024, 1) + "MB");
			$('#currCount2').val(json.totalInfoVO.totalFileCount);
			$('#currSize2').val(json.totalInfoVO.totalFileMg);
		};
		fn_ajax_json(url, data, successFn, null);
	}
}
