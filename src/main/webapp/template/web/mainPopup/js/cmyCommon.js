

function fn_egov_deleteUser(url){
	if(confirm('커뮤니티에서 탈퇴 하시겠습니까?')) {
		location.href=url;
		//window.open(url, "deleteUser", "width=320px, height=200px;");
		return true;
	}else {
		return false;	
	}
}

function fn_egov_registUser(url){
	if(confirm('커뮤니티에 가입 하시겠습니까?')) {
		location.href=url;
		//window.open(url, "registUser", "width=320px, height=200px;");
		return true;
	}else {
		return false;	
	}
}

function fn_cmmnty_closing(url){
	if(confirm('커뮤니티 폐쇄 알림\n\n커뮤니티 폐쇄 신청이 이루어지면 모든 자료가 삭제 처리됩니다.\n삭제가 완료된 후에는 자료복구를 할 수 없으므로 신중한 판단을 부탁드립니다.\n\n폐쇄 안내를 확인하였으며 확인을 누르시면 폐쇄됩니다.')) {
		location.href=url;
		//window.open(url, "registUser", "width=320px, height=200px;");
		return true;
	}else {
		return false;	
	}
}
