var pageIndex = 0; 
var nextExists = true;

function getSmartIcon(actPageIndex) {
	$.getJSON(reqUrl + groupTy + '&pageIndex=' + actPageIndex + '&recordCountPerPage=' + recordCountPerPage, function(data) {
	  	if(data.length == recordCountPerPage && recordCountPerPage > 0) {
	  		nextExists = true;
	  	} else {
	  		nextExists = false;
	  	}
	  	
	  	pageIndex = actPageIndex;
	  
	  	$('#smart_icon_box').empty();
		$.each(data, function(index, entry) {
			var html = "<dl>";
				html += "<dt><a href='" + entry.menuWebPath + "' " + (entry.nwdAt == 'Y' ? "target='_blank'" : "") + "><img src='" + iconBaseUrl + entry.siteId + "/" + entry.imageFileNm + "' width='66' height='67' alt='" + entry.menuNm + "' /></a></dt>";
				html += "<dd>" + entry.menuNm + "</dd>";
				html += "</dl>";
	       	$('#smart_icon_box').append(html);
	    });
		
	});
}

$(document).ready(function() {
	getSmartIcon(pageIndex + 1);
	
	$('#btnSmartIconPre').click(function() {
		if(pageIndex > 1) {
			getSmartIcon(pageIndex - 1);
		}
	});
	
	$('#btnSmartIconNext').click(function() {
		if(nextExists) {
			getSmartIcon(pageIndex + 1);
		}
	});
});
